<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

switch (ENVIRONMENT) {
	case 'development':
		$config['sso_address'] = 'http://127.0.0.1/berkatcorp-master/sso/server';
		$config['rice_address'] = 'http://127.0.0.1/berkatcorp-rice/';
		$config['transport_address'] = 'http://127.0.0.1/berkatcorp-transportasi';
		break;
	case 'production':
		$config['sso_address'] = 'http://master.berkatcorp.com/sso/server';
		$config['rice_address'] = 'http://rice.berkatcorp.com/';
		$config['rice_address'] = 'http://127.0.0.1/berkatcorp-rice/';
		break;
}