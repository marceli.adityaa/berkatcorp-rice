<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('kas_model', 'kas');
        $this->load->model('pembelian_lain_model', 'pembelian_lain');
        $this->load->model('pembelian_lain_detail_model', 'pembelian_lain_detail');
        $this->load->model('pembelian_lain_pembayaran_model', 'pembelian_lain_pembayaran');
        $this->load->model('pendapatan_model', 'pendapatan'); //
        $this->load->model('beban_model', 'beban'); //
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->id_potongan_pembelian = $this->config->item('id_potongan_pembelian'); //
        $this->id_biaya_tambahan = $this->config->item('id_biaya_tambahan'); //
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian_lain';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_lain->get($id);
        echo json_encode($response);
    }

    public function get_sisa_tempo()
    {
        $id = $this->input->post('id');
        if(!empty($id)){

            $pembelian = $this->pembelian_lain->get_data_pembayaran_tempo($id)[0]; 
            $detail = $this->pembelian_lain_pembayaran->get_detail_pembayaran($id);
            $sisa_tempo = $pembelian['grandtotal'];
            $pembayaran_verif       = $this->pembelian_lain_pembayaran->get_data_pembayaran_verified($id, '');
            if($pembayaran_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_pembayaran = 0;
                foreach($pembayaran_verif as $row){
                    $total_pembayaran += $row['jumlah_bayar'];
                }
                $sisa_tempo -= $total_pembayaran;
            }
        }else{
            $sisa_tempo = 0;
        }
        echo json_encode($sisa_tempo);
    }

    public function get_detail_trx_pembayaran()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_lain_pembayaran->get_data($id);
        echo json_encode($response);
    }

    public function json_get_detail_pembayaran(){
        $id = $this->input->post('id');
        $response = $this->kas->join($this->pembelian_lain_pembayaran->get_detail_pembayaran_approved($id), 'kas', 'id_kas');
        echo json_encode($response);
    }

    public function delete_data_pembayaran()
    {
        $id = $this->input->post('id');
        $cek = $this->pembelian_lain_pembayaran->is_deleteable($id);
        if ($cek) {
            $response = $this->pembelian_lain_pembayaran->delete($id);
            if ($response) {
                $this->message('Sukses menghapus data', 'success');
            } else {
                $this->message('Gagal menghapus data.', 'error');
            }
        } else {
            $response = true;
            $this->message('Gagal', 'error');
        }
        echo json_encode($response);
    }

    public function setuju()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 1,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
           
            # Update status di tabel pembayaran pembelian lain
            $id_pembelian = $this->pembelian_lain_pembayaran->get_data($id)['id_pembelian'];
            $pernah_bayar = $this->pembelian_lain_pembayaran->jumlah_pembayaran($id_pembelian);
            $result = $this->pembelian_lain_pembayaran->update($id, $update);
            $detail = $this->pembelian_lain_pembayaran->get_data($id);
            # Update status di tabel pembelian lain
            $result2 = $this->pembelian_lain->update($detail['id_pembelian'], array('is_tempo_dibayar' => 1, 'is_lunas' => $detail['is_paid_off'], 'tgl_bayar' => $detail['tgl_pembayaran'], 'id_kas' => $detail['id_kas'], 'jenis_pembayaran' => $detail['jenis_pembayaran']));
            $trx = $this->pembelian_lain->get($detail['id_pembelian']);
            
            $pembelian = $this->pembelian_lain->get_data($detail['id_pembelian']);

            if(!$pernah_bayar){
                # Jika belum ada transaksi pembayaran, maka melakukan init transaksi potongan / tambahan jika ada dan arus kas dengan penyesuaian jumlah bayar

                # Insert transaksi di buku kas 
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tgl_pembayaran'];
                $arus_kas['arus'] = "out";
                $arus_kas['sumber'] = "pembelian-lain-bayar-tempo";
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = 'Bayar Tempo '.$trx['kode'];
                $arus_kas['deskripsi'] = 'Jumlah bayar : '.$detail['jumlah_bayar'];
                $arus_kas['nominal'] = $detail['jumlah_bayar'] + $pembelian['potongan'] - $pembelian['biaya_tambahan'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $response = json_decode(xcurl($param_curl)['response']);

                if($pembelian['potongan'] > 0){
                    # Jika terdapat potongan pembelian, maka insert ke pendapatan lain (auto acc) dan buku kas
                    # Insert ke pendapatan
                    $pendapatan['id_kategori'] = $this->id_potongan_pembelian;
                    $pendapatan['tgl_transaksi'] = $pembelian['tgl_bayar'];
                    $pendapatan['nominal'] = $pembelian['potongan'];
                    $pendapatan['catatan'] = 'Potongan pembelian '.$pembelian['kode'];
                    $pendapatan['jenis_pembayaran'] = $pembelian['jenis_pembayaran'];
                    $pendapatan['id_kas'] = $pembelian['id_kas'];
                    $pendapatan['is_verifikasi'] = 1;
                    $pendapatan['input_by'] = get_session('auth')['id'];
                    $pendapatan['timestamp_input'] = setNewDateTime();
                    $pendapatan['verifikasi_by'] = get_session('auth')['id'];
                    $pendapatan['timestamp_verifikasi'] = setNewDateTime();
                    $result = $this->pendapatan->insert($pendapatan);

                    # Insert ke buku kas
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $pembelian['id_kas'];
                    $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = "pendapatan-pembelian-bahan";
                    $arus_kas['id_sumber'] = $pembelian['id'];
                    $arus_kas['keterangan'] = 'Potongan pembelian '.$pembelian['kode'];
                    $arus_kas['deskripsi'] = '';
                    $arus_kas['nominal'] = $pembelian['potongan'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
    
                if($pembelian['biaya_tambahan'] > 0){
                    # Jika terdapat biaya tambahan pembelian, maka insert ke beban pembelian (auto acc) dan buku kas
                    # Insert ke pendapatan
                    $beban['id_beban'] = $this->id_biaya_tambahan;
                    $beban['tgl_transaksi'] = $pembelian['tgl_bayar'];
                    $beban['nominal'] = $pembelian['biaya_tambahan'];
                    $beban['catatan'] = 'Biaya tambahan pembelian '.$pembelian['kode'];
                    $beban['jenis_pembayaran'] = $pembelian['jenis_pembayaran'];
                    $beban['id_kas'] = $pembelian['id_kas'];
                    $beban['is_verifikasi'] = 1;
                    $beban['input_by'] = get_session('auth')['id'];
                    $beban['timestamp_input'] = setNewDateTime();
                    $beban['verifikasi_by'] = get_session('auth')['id'];
                    $beban['timestamp_verifikasi'] = setNewDateTime();
                    $result = $this->beban->insert($beban);

                    # Insert ke buku kas
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $pembelian['id_kas'];
                    $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = "beban-pembelian-bahan";
                    $arus_kas['id_sumber'] = $pembelian['id'];
                    $arus_kas['keterangan'] = 'Biaya tambahan pembelian '.$pembelian['kode'];
                    $arus_kas['deskripsi'] = '';
                    $arus_kas['nominal'] = $pembelian['biaya_tambahan'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
            }else{
                # Jika sudah pernah transaksi pembayaran, maka jumlah bayar tidak disesuaikan
                # Insert transaksi di buku kas 
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tgl_pembayaran'];
                $arus_kas['arus'] = "out";
                $arus_kas['sumber'] = "pembelian-lain-bayar-tempo";
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = 'Bayar Tempo '.$trx['kode'];
                $arus_kas['nominal'] = $detail['jumlah_bayar'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $response = json_decode(xcurl($param_curl)['response']);
            }

        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menyetujui pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

    public function tolak()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 2,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
            $response = $this->pembelian_lain_pembayaran->update($id, $update);
            
        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menolak pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

}