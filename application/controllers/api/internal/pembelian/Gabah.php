<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gabah extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->load->model('stok_gabah_model', 'stok_gabah');
        $this->load->model('stok_gabah_trx_model', 'stok_gabah_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

	public function insert_data_timbangan()
    {
        $post = $this->input->post();
        # Insert Statement
        $post['input_by'] = get_session('auth')['id'];
        $result = $this->pembelian_gabah_batch->insert($post);
        echo json_encode($result);
    }

    public function update_data_timbangan()
    {
        $post = $this->input->post('data');
        // dump($post);
        if(!empty($post)){
            # Update Statement
            foreach($post as $row)
            {
                $data = array(
                    'batch' => $row['batch'],
                    'batch_label' => $row['batch_label'],
                    'berat_kotor' => $row['berat_kotor'],
                    'berat_kosong' => $row['berat_kosong'],
                    'potongan_timbangan' => $row['potongan_timbangan'],
                    'berat_netto' => $row['berat_netto'],
                    'is_manual' => $row['is_manual'],
                    'input_by' => get_session('auth')['id']
                );
                $result = $this->pembelian_gabah_batch->update($row['id'], $data);
            }
            
            echo json_encode($result);
        }else{
            echo json_encode(false);
        }
    }

    public function update_potongan_sak()
    {
        $post = $this->input->post();
        if(!empty($post['data']))
        {
            foreach($post['data'] as $row)
            {
                $result = $this->pembelian_gabah_jenis->update_hampa(array('potong_sak' => $row['potong_sak']), array('id_batch' => $row['id_batch'], 'jenis_gabah' => $row['jenis_gabah'], 'id_jenis_padi' => $row['jenis_padi']));
            }
        }
        if(!empty($post['trx_sak']))
        {
            foreach($post['trx_sak'] as $row)
            {
                $result = $this->pembelian_gabah_batch->update($row['id_batch'], array('id_jenis_sak' => $row['id_jenis_sak'], 'setor_sak' => $row['setor_sak'], 'is_record_supplier' => $row['is_record_supplier'], 'is_record_primary' => $row['is_record_primary']));
            }
        }

        if($result == true)
        {
            $this->message('Sukses mengubah data.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function submit_qc_pembelian()
    {
        $id = $this->input->post('id');
        $data['pembelian'] = $this->pembelian_gabah->get_data($id);
        $data['batch'] = $this->user->join($this->pembelian_gabah_batch->get_data($id), 'username_input', 'input_by');
        $is_exist = $this->transaksi_sak->is_exist($this->src, $id);
        // var_dump($is_exist);die();
        $primary = $this->customer->get_primary()[0];
        if($is_exist == false){
            # insert statement
            $trx['kode_transaksi'] = $this->generate_kode_sak($data['pembelian']['tanggal_pembelian']);
            $trx['tgl_transaksi'] = $data['pembelian']['tanggal_pembelian'];
            $trx['source'] = $this->src;
            $trx['id_source'] = $id;
            $trx['id_customer_from'] = $data['pembelian']['id_supplier'];
            $trx['id_customer_to'] = $primary['id'];
            $trx['is_verifikasi'] = 0;
            $trx['input_by'] = get_session('auth')['id'];
            $trx['timestamp_input'] = setNewDateTime();
            
            $result_trx = $this->transaksi_sak->insert($trx);
            if($result_trx){
                $id_trx = $this->db->insert_id();
                foreach($data['batch'] as $row){
                    if($row['id_jenis_sak'] != 0){
                        $trx_detail['id_transaksi'] = $id_trx;
                        $trx_detail['jumlah'] = $row['setor_sak'];
                        if($row['is_record_supplier'] == 1){
                            $trx_detail['id_customer'] = $data['pembelian']['id_supplier'];
                            $trx_detail['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail['is_record'] = 1;
                            $trx_detail['arus'] = 'in';
                            $this->transaksi_sak_detail->insert($trx_detail);
                        }

                        if($row['is_record_primary'] == 1){
                            $trx_detail['id_customer'] = $primary['id'];
                            $trx_detail['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail['is_record'] = 1;
                            $trx_detail['arus'] = 'in';
                            $this->transaksi_sak_detail->insert($trx_detail);
                        }
                    }
                }
            }
        }else{
            # update statement
            $trx = $this->transaksi_sak->get_by($this->src, $id);
            if($trx['is_verifikasi'] != 1){
                $reset = $this->transaksi_sak_detail->reset_detail($trx['id']);
                if($reset){
                    foreach($data['batch'] as $row){
                        if($row['id_jenis_sak'] != 0){
                            $trx_detail['id_transaksi'] = $trx['id'];
                            $trx_detail['jumlah'] = $row['setor_sak'];
                            if($row['is_record_supplier'] == 1){
                                $trx_detail['id_customer'] = $data['pembelian']['id_supplier'];
                                $trx_detail['id_jenis_sak'] = $row['id_jenis_sak'];
                                $trx_detail['is_record'] = 1;
                                $trx_detail['arus'] = 'in';
                                $this->transaksi_sak_detail->insert($trx_detail);
                            }
    
                            if($row['is_record_primary'] == 1){
                                $trx_detail['id_customer'] = $primary['id'];
                                $trx_detail['id_jenis_sak'] = $row['id_jenis_sak'];
                                $trx_detail['is_record'] = 1;
                                $trx_detail['arus'] = 'in';
                                $this->transaksi_sak_detail->insert($trx_detail);
                            }
                        }
                    }
                }
            }
        }
        $response = $this->pembelian_gabah->update($id, array('status_muatan' => 1));
        $redirect = "";
        if($response == true)
        {
            $this->message('Sukses mengirim data.', 'success');
            $redirect = base_url('pembelian/gabah/quality_control');
        }else{
            $this->message('Gagal mengirim data', 'error');
            $redirect = base_url('pembelian/gabah/batch/'.$id);
        }
        echo json_encode(array('redirect' => $redirect));
    }

    public function submit_input_harga()
    {
        $post = $this->input->post();
        // dump($post);
        if(!empty($post['data_ks'])){
            foreach($post['data_ks'] as $row){
                $id = $row['id_jenis'];
                unset($row['id_jenis']);
                $this->pembelian_gabah_jenis->update($id, $row);
            }
        }
        
        if(!empty($post['data_kg'])){
            foreach($post['data_kg'] as $row){
                $id = $row['id_jenis'];
                unset($row['id_jenis']);
                $this->pembelian_gabah_jenis->update($id, $row);
            }
        }

        $response = $this->pembelian_gabah->update($post['id_pembelian'], $post['data_pembayaran'][0]);
        if ($response) {
            $this->message('Sukses mengubah data', 'success');
        } else {
            $this->message('Gagal menghapus data karena terdapat data transaksi.', 'error');
        }
        echo json_encode($response);
    }

	public function pembelian_gabah_tolak(){
		$id = $this->input->post('id');
		if(!empty($id)){
			$update = array(
				'status_persetujuan' => 2,
				'verifikasi_oleh' => get_session('auth')['id'],
				'timestamp_verifikasi' => setNewDateTime()
			);

			$result = $this->pembelian_gabah->update($id, $update);
		}else{
			$result = false;
		}
		echo json_encode($result);
	}

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah->as_array()->get($id);
        echo json_encode($response);
    }

    public function json_get_detail_batch()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah_batch->as_array()->get($id);
        echo json_encode($response);
    }

    public function delete_data_batch()
    {
        $id = $this->input->post('id');
        $cek = $this->pembelian_gabah_batch->is_deleteable($id);
        if ($cek) {
            $response = $this->pembelian_gabah_batch->delete($id);
            if ($response) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal menghapus data karena terdapat data transaksi.', 'error');
            }
        } else {
            $response = true;
            $this->message('Gagal', 'error');
        }
        echo json_encode($response);
    }

    public function delete_data_jenis()
    {
        $id = $this->input->post('id');
        $sap = $this->input->post('sap');
        $response = $this->pembelian_gabah_detail->delete_by_jenis_sap($id, $sap);
        $cek = $this->pembelian_gabah_detail->is_available_detail_by_jenis($id);
        if(!$cek){
            $response = $this->pembelian_gabah_jenis->delete($id);
        }
        if ($response) {
            $this->message('Sukses mengubah data', 'success');
        } else {
            $this->message('Gagal menghapus data karena terdapat data transaksi.', 'error');
        }
        
        echo json_encode($response);
    }
    public function delete_data_jenis_kecil()
    {
        $id = $this->input->post('id');
        // $sap = $this->input->post('sap');
        $response = $this->pembelian_gabah_detail->delete_by_jenis($id);
        $cek = $this->pembelian_gabah_detail->is_available_detail_by_jenis($id);
        // dump($response);
        if(!$cek){
            $response = $this->pembelian_gabah_jenis->delete($id);
        }
        if ($response) {
            $this->message('Sukses mengubah data', 'success');
        } else {
            $this->message('Gagal menghapus data karena terdapat data transaksi.', 'error');
        }
        
        echo json_encode($response);
    }
    public function delete_data_pembayaran_tempo()
    {
        $id = $this->input->post('id');
        $cek = $this->pembelian_gabah_pembayaran->is_deleteable($id);
        if ($cek) {
            $response = $this->pembelian_gabah_pembayaran->delete($id);
            if ($response) {
                $this->message('Sukses menghapus data', 'success');
            } else {
                $this->message('Gagal menghapus data.', 'error');
            }
        } else {
            $response = true;
            $this->message('Gagal', 'error');
        }
        echo json_encode($response);
    }


    public function insert_qc_form()
    {
        $post = $this->input->post();
        // dump($post);
        $id = '';
        foreach ($post['data'] as $row) {
			$res = $this->pembelian_gabah_jenis->is_available($row['id_batch'], $row['jenis_gabah'], $row['jenis_padi']);
            if ($res == false) {
				$insert = array(
					'id_batch' => $row['id_batch'],
                    'jenis_gabah' => $row['jenis_gabah'],
                    'id_jenis_padi' => $row['jenis_padi'],
                    'pengeringan' => $row['pengeringan'],
                    'proses' => $row['proses_kg'],
                );
                $this->pembelian_gabah_jenis->insert($insert);
                $id = $this->db->insert_id();
            } else {
				$id = $res[0]['id'];
            }

            $detail = array(
                'sap' => $row['sap'],
                'id_jenis' => $id,
                'kadar_air' => $row['kadar_air'],
                'jumlah_sak' => $row['jumlah_sak'],
                'is_default' => ($row['default'] == 'true') ? 1 : 0,
                'input_by' => get_session('auth')['id'],
                'timestamp' => setNewDateTime(),
            );
            $result = $this->pembelian_gabah_detail->insert($detail);
            if ($result) {
                $this->message('Sukses menambah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
		}
        if ($post['new_sap'] == 'true') {
            echo json_encode(array('redirect' => base_url('pembelian/gabah/quality_control/form/' . $post['batch'] . '?sap=' . ($post['sap'] + 1))));
        } else {
            echo json_encode(array('redirect' => base_url('pembelian/gabah/quality_control/transaksi/' . $post['batch'])));
        }

    }

    public function update_qc_form()
    {
        $post = $this->input->post();
        # Delete data lama
        $old = $this->pembelian_gabah_jenis->get_data_by($post['batch'], $post['sap']);
        if(!empty($old)){
            foreach($old as $row){
                $this->pembelian_gabah_jenis->delete($row['id']);
                $this->pembelian_gabah_detail->delete_by_jenis($row['id']);
            }
        }
        // dump($old);
        $id = '';
        foreach ($post['data'] as $row) {
            $res = $this->pembelian_gabah_jenis->is_available($row['id_batch'], $row['jenis_gabah'], $row['jenis_padi'], $row['sap']);
            if ($res == false) {
				$insert = array(
					'id_batch' => $row['id_batch'],
                    'jenis_gabah' => $row['jenis_gabah'],
                    'id_jenis_padi' => $row['jenis_padi'],
                    'pengeringan' => $row['pengeringan'],
                    'proses' => $row['proses_kg'],
                );
                $this->pembelian_gabah_jenis->insert($insert);
                $id = $this->db->insert_id();
            } else {
				$id = $res[0]['id'];
            }

            $detail = array(
                'sap' => $row['sap'],
                'id_jenis' => $id,
                'kadar_air' => $row['kadar_air'],
                'jumlah_sak' => $row['jumlah_sak'],
                'is_default' => ($row['default'] == 'true') ? 1 : 0,
                'input_by' => get_session('auth')['id'],
                'timestamp' => setNewDateTime(),
            );
            $result = $this->pembelian_gabah_detail->insert($detail);
            if ($result) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
		}
        echo json_encode(array('redirect' => base_url('pembelian/gabah/quality_control/transaksi/' . $post['batch'])));

    }

    public function submit_qc_form_manual()
    {
        $post = $this->input->post();
        $jumlah_sap = array();
        if(!empty($post['jumlah_sap'])){
            foreach($post['jumlah_sap'] as $row){
                if(!empty($row['value'])){
                    array_push($jumlah_sap, $row['value']);
                }
            }
        }
        if(!empty($post['id_batch'])){
            # Jika data timbangan kecil sudah ada
            $old = $this->pembelian_gabah_jenis->get_data_by($post['id_batch']);
            if(!empty($old)){
                # Hapus semua data timbangan dibatch ini untuk kemudian ditimpa data baru
                foreach($old as $row){
                    $this->pembelian_gabah_jenis->delete($row['id']);
                    $this->pembelian_gabah_detail->delete_by_jenis($row['id']);
                }
            }
            $id_batch = $post['id_batch'];
        }else{
            # Create data timbangan (batch) 
            $create = array(
                'id_pembelian' => $post['id_pembelian'],
                'batch' => $post['batch'],
                'batch_label' => 'Timbangan Kecil',
                'is_manual' => 1,
                'input_by' => get_session('auth')['id']
            );
            $this->pembelian_gabah_batch->insert($create);
            $id_batch = $this->db->insert_id();
        }
        
        $berat_total = 0;
        foreach($post['data'] as $row){
            $jenis = array();
            if(!isset($row['pengeringan'])){
                $row['pengeringan'] = '';
            }
            if(!isset($row['proses'])){
                $row['proses'] = '';
            }
            foreach($row['berat'] as $row2){
                $cek = $this->pembelian_gabah_jenis->is_available($id_batch, $row['jenis_gabah'], $row['jenis_padi'], '', $row['pengeringan'], $row['proses']);
                if($cek != false){
                    # ada data jenisnya, insert data detail dengan id jenis yang sudah dipilih
                    // dump($cek);
                        $detail = array(
                            'sap' => 1,
                            'id_jenis' => $cek[0]['id'],
                            'kadar_air' => $row['kadar_air'],
                            'jumlah_sak' => 1,
                            'berat_basah' => $row2,
                            'is_default' => 0,
                            'input_by' => get_session('auth')['id'],
                            'timestamp' => setNewDateTime()
                        );
                        $berat_total += $row2;
                        $this->pembelian_gabah_detail->insert($detail);
                    
                }else{
                    # tidak ada data jenisnya, insert data jenis kemudian insert data detail
                    $jenis['id_batch'] = $id_batch;
                    $jenis['jenis_gabah'] = strtolower($row['jenis_gabah']); 
                    $jenis['id_jenis_padi'] = $row['jenis_padi'];
                    $jenis['pengeringan'] = $row['pengeringan'];
                    $jenis['proses'] = $row['proses'];
                    $jenis['potong_sak'] = 0;
                    $this->pembelian_gabah_jenis->insert($jenis);
                    $id_jenis = $this->db->insert_id();
                    $detail = array(
                        'sap' => 1,
                        'id_jenis' => $id_jenis,
                        'kadar_air' => $row['kadar_air'],
                        'jumlah_sak' => 1,
                        'berat_basah' => $row2,
                        'is_default' => 0,
                        'input_by' => get_session('auth')['id'],
                        'timestamp' => setNewDateTime()
                    );
                    
                    $berat_total += $row2;
                    $this->pembelian_gabah_detail->insert($detail);
                }
            }
        }

        $this->pembelian_gabah_batch->update($id_batch, array('berat_netto' => $berat_total, 'sap_manual' => json_encode($jumlah_sap)));
        $this->pembelian_gabah_batch->update($post['primary_batch'], array('berat_timbang_kecil' => $berat_total));
        $this->message('Sukses menyimpan data', 'success');
        echo json_encode(array('redirect' => base_url('pembelian/gabah/quality_control/transaksi/' . $post['primary_batch'])));
    }

    public function get_data_timbangan()
    {
        $id = $this->input->post('id');
        $response['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
        $response['timbangan'] = $this->user->join($this->pembelian_gabah_batch->get_data($id), 'username_input', 'input_by');
        $response['batch_idx'] = $this->pembelian_gabah_batch->get_data_last($id);
        $response['sisa_bruto'] = $response['pembelian']['berat_bruto'];
        if ($response['batch_idx'] > 1) {
            foreach ($response['timbangan'] as $row) {
                // $response['sisa_bruto'] -= $row['berat_netto'];
                $response['sisa_bruto'] -= ($row['berat_kotor'] - $row['berat_kosong']);
            }
        }
        echo json_encode($response);
       
    }

    public function pengajuan_persetujuan()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            # Hapus data stok gabah trx
            $list_stok = $this->stok_gabah->get_data_source('pembelian-gabah', $id);
            if($list_stok != false){
                foreach($list_stok as $list){
                    $this->stok_gabah_trx->hapus_trx_stok($list->id);
                }
            }
            # Hapus data pembelian di tabel stok_gabah
            $reset = $this->stok_gabah->hapus_pembelian($id);

            if($reset){
                # Inisialisasi utk stok
                $jenis      = array();
                $total_sak  = array();
                $stok       = array();
                $jml_berat  = array();
                
                $pembelian = $this->pembelian_gabah->get_data_cetak_nota($id);
                $data['jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id);
                $data['detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id);
                $data['total_sak'] = $this->pembelian_gabah_detail->get_total_sak($id);
                # Transpose data jenis -> harga satuan disesuaikan
                foreach($data['jenis'] as $val){
                    $jenis[$val['id_jenis']] = $val;
                    if(!isset($jml_berat[$val['id_batch']])){
                        $jml_berat[$val['id_batch']] = 0;
                    }
                    $jml_berat[$val['id_batch']] += $val['berat_beras_netto'];
                }

                # Transpose data total sak
                foreach($data['total_sak'] as $val){
                    $total_sak[$val['id_batch']] = $val;
                }
                # Transpose data pembelian gabah menjadi data stok  
                # Parameter pembeda : jenis_gabah, jenis_padi, pengeringan
                foreach($data['detail'] as $key => $row){
                    $berat = round(($row['jumlah_sak'] * $jml_berat[$row['id_batch']] / $total_sak[$row['id_batch']]['jumlah']));
                    $hargaSatuan = round($jenis[$row['id_jenis']]['subtotal'] / $jenis[$row['id_jenis']]['berat_beras_netto']);
                    if(empty($stok)){
                        $d['tanggal'] = $pembelian['tanggal_pembelian'];
                        $d['sumber'] = 'pembelian-gabah';
                        $d['id_sumber'] = $pembelian['id'];
                        $d['id_supplier'] = $pembelian['id_supplier'];
                        $d['input_by'] = get_session('auth')['id'];
                        $d['timestamp_input'] = setNewDateTime();
                        $d['jenis_gabah'] = $row['jenis_gabah'];
                        $d['id_jenis_padi'] = $row['id_jenis_padi'];
                        // $d['padi'] = $jenis[$row['id_jenis']]['padi'];
                        $d['stok'] = $berat;
                        $d['stok_sisa'] = $berat;
                        $d['harga_satuan'] = $hargaSatuan;
                        if($row['jenis_gabah'] == 'ks'){
                            $d['jenis_pengeringan'] = $row['pengeringan'];
                            $d['jenis_proses'] = '';
                        }else{
                            $d['jenis_pengeringan'] = '';
                            $d['jenis_proses'] = $row['proses'];
                        }
                        array_push($stok, $d);
                    }else{
                        $newItem = true;
                        foreach($stok as $key => $st){
                            if($st['jenis_gabah'] == $row['jenis_gabah'] && $st['id_jenis_padi'] == $row['id_jenis_padi'] && $st['jenis_pengeringan'] == $row['pengeringan']){
                                $hargaTotal = ($st['stok'] * $st['harga_satuan']) + ($berat * $hargaSatuan); 
                                $stok[$key]['stok'] += $berat;
                                $stok[$key]['stok_sisa'] += $berat;
                                $hargaBaru = round($hargaTotal / $stok[$key]['stok']);
                                $stok[$key]['harga_satuan'] = $hargaBaru;
                                $newItem = false;
                                break;
                            }
                        }
                        if($newItem){
                            $d['tanggal'] = $pembelian['tanggal_pembelian'];
                            $d['sumber'] = 'pembelian-gabah';
                            $d['id_sumber'] = $pembelian['id'];
                            $d['id_supplier'] = $pembelian['id_supplier'];
                            $d['input_by'] = get_session('auth')['id'];
                            $d['timestamp_input'] = setNewDateTime();
                            $d['jenis_gabah'] = $row['jenis_gabah'];
                            $d['id_jenis_padi'] = $row['id_jenis_padi'];
                            // $d['padi'] = $jenis[$row['id_jenis']]['padi'];
                            $d['stok'] = $berat;
                            $d['stok_sisa'] = $berat;
                            $d['harga_satuan'] = $hargaSatuan;
                            if($row['jenis_gabah'] == 'ks'){
                                $d['jenis_pengeringan'] = $row['pengeringan'];
                                $d['jenis_proses'] = '';
                            }else{
                                $d['jenis_pengeringan'] = '';
                                $d['jenis_proses'] = $row['proses'];
                            }
                            array_push($stok, $d);
                        }
                    }
                }
               
                # Insert data stok ke database
                foreach($stok as $st){
                    $this->stok_gabah->insert($st);
                    $trx = array(
                        'id_stok' => $this->db->insert_id(),
                        'tgl' => $pembelian['tanggal_pembelian'],
                        'arus' => 'in',
                        'kuantitas' => $st['stok'],
                        'harga' => $st['harga_satuan'],
                        'sumber' => 'pembelian-gabah',
                        'timestamp' => setNewDateTime()
                    );
                    $this->stok_gabah_trx->insert($trx);
                }

                $result = $this->pembelian_gabah->update($id, array('status_selesai' => 1, 'diajukan_oleh' => get_session('auth')['id'], 'timestamp_pengajuan' => setNewDateTime()));
                $this->message('Pengajuan data berhasil.', 'success');
            }else{
                $result = false;
                $this->message('Gagal menambahkan stok gabah', 'error');
            }
        }
        echo json_encode($result);
    }

    public function pembatalan_persetujuan()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $pembelian = $this->pembelian_gabah->get_data($id);
            if($pembelian['status_persetujuan'] != 1){
                $result = $this->pembelian_gabah->update($id, array('status_selesai' => 0));
            }else{
                $this->message('Pembatalan gagal! Data sudah disetujui.', 'error');
            }
        }
        if($result){
            $this->message('Pembatalan berhasil.', 'success');
        } else {
            $this->message('Pembatalan gagal!', 'error');
        }
        echo json_encode($result);
    }

    public function get_sisa_tempo()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $pembelian = $this->pembelian_gabah->get_data_pembayaran_tempo($id);
            $sisa_tempo = $pembelian['grandtotal'] - $pembelian['bayar_dp'];
            $pembayaran_verif = $this->pembelian_gabah_pembayaran->get_data_pembayaran_verified($id, '');
            if($pembayaran_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_pembayaran = 0;
                foreach($pembayaran_verif as $row){
                    $total_pembayaran += $row['jumlah_bayar'];
                }
                $sisa_tempo -= $total_pembayaran;
            }
        }else{
            $sisa_tempo = 0;
        }
        echo json_encode($sisa_tempo);
    }

    public function get_detail_trx_pembayaran_tempo()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah_pembayaran->get_data($id);
        echo json_encode($response);
    }

    public function otorisasi_pembelian_gabah_setuju()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 1,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
            $result = $this->pembelian_gabah_pembayaran->update($id, $update);
            $detail = $this->pembelian_gabah_pembayaran->get_data($id);
            $result2 = $this->pembelian_gabah->update($detail['id_pembelian'], array('is_tempo_dibayar' => $detail['is_paid_off'], 'tgl_bayar' => $detail['tgl_pembayaran']));
            $arus_kas['id_perusahaan'] = $this->id_perusahaan;
            $arus_kas['id_kas'] = $detail['id_kas'];
            $arus_kas['tgl_transaksi'] = $detail['tgl_pembayaran'];
            $arus_kas['arus'] = "out";
            $arus_kas['sumber'] = "pembelian-gabah-bayar-tempo";
            $arus_kas['id_sumber'] = $id;
            $arus_kas['keterangan'] = 'Bayar Tempo Pembelian Gabah';
            $arus_kas['nominal'] = $detail['jumlah_bayar'];
            $arus_kas['input_by'] = get_session('auth')['id'];
            $arus_kas['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
            $response = json_decode(xcurl($param_curl)['response']);
        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menyetujui pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

    public function otorisasi_pembelian_gabah_tolak()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 2,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
            $response = $this->pembelian_gabah_pembayaran->update($id, $update);
            
        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menolak pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

    public function delete_data_pembelian(){
        $id = $this->input->post('id');
        $batch = $this->pembelian_gabah_batch->get_data($id);
        $jenis = $this->pembelian_gabah_jenis->get_data_jenis_pembelian($id);
        if(!empty($jenis)){
            foreach($jenis as $row){
                $this->pembelian_gabah_detail->delete_by_jenis($row['id_jenis']);
            }
        }
        if(!empty($batch)){
            foreach($batch as $row){
                $this->pembelian_gabah_jenis->delete_by_batch($row['id']);
            }
        }
        $this->pembelian_gabah_batch->delete_pembelian($id);
        $response = $this->pembelian_gabah->delete($id);
        if ($response) {
			$this->message('Berhasil menghapus data', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

    public function get_data_label(){
        $id_batch = $this->input->post('id_batch');
        $detail = $this->pembelian_gabah_jenis->get_label_muatan($id_batch);
        dump($detail);
    }
    
    public function info_resi()
    {
        $resi = $this->input->post('resi');
        dump($resi);
    }

	public function generate_kode_sak($tgl)
    {
        $last = $this->transaksi_sak->get_last_kode($tgl);
        $index = 1;
        // $last = '1-191001-001';
        if ($last != false) {
            $temp = explode('.', $last);
            $index = intval($temp[2]) + 1;
        }
        $kode = 'SAK.'. date("ymd", strtotime($tgl)) . '.' . zerofy($index,3);
        return $kode;
    }
	
}