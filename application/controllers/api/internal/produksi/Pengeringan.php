<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengeringan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('stok_gabah_model', 'stok_gabah');
        $this->load->model('stok_gabah_trx_model', 'stok_gabah_trx');
        $this->load->model('pengeringan_model', 'pengeringan');
        $this->load->model('pengeringan_detail_model', 'pengeringan_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pengeringan';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->pengeringan->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->pengeringan->get($id);
        if($detail->persetujuan < 2){
            # Delete detail pengeringan
            $this->pengeringan_detail->delete_transaksi($id);
            # Delete pengeringan
            $result = $this->pengeringan->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function submit_detail_tugas(){
        $post = $this->input->post();
        $insert = array();
        foreach($post['data'] as $row){
            if(!isset($row['id_dryer'])){
                $row['id_dryer'] = 0;
            }
            if(!isset($row['id_jemur'])){
                $row['id_jemur'] = 0;
            }
            $baris = array(
                'id_pengeringan' => $post['id_pengeringan'],
                'id_stok' => $row['id_stok'],
                'kuantitas' => $row['kuantitas'],
                'harga_satuan' => $row['harga_satuan'],
                'id_dryer' => $row['id_dryer'],
                'id_jemur' => $row['id_jemur'],
            );
            array_push($insert, $baris);
        }
        $result = $this->pengeringan_detail->batch_insert($insert);
        # Insert statement
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_detail()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $result = $this->pengeringan_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        
        echo json_encode($result);
    }

    public function submit_detail_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $detail = $this->pengeringan_detail->get_data_detail($id);
        $hpp = 0;
        $qty = 0;
        $nilai = 0;
        foreach($detail as $row){
            $qty += $row['kuantitas'];
            $nilai += ($row['kuantitas'] * $row['harga_satuan']);
        }

        $update = array(
            'kuantitas' => $qty,
            'hpp_beli' => ($nilai / $qty)
        );

        $result = $this->pengeringan->update($id, $update);
        if($result){
            $this->message('Sukses menyimpan data.', 'success');
            $redirect = base_url('produksi/pengeringan/kelola');
        }else{
            $this->message('Gagal menyimpan data', 'error');
            $redirect = base_url('produksi/pengeringan/detail/'.$id);
        }
        
        echo json_encode(array('redirect' => $redirect, 'result' => $result));
    }

    public function batal_pengajuan_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'kuantitas' => 0,
            'hpp_beli' => 0
        );

        $result = $this->pengeringan->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function approve_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $tugas = $this->pengeringan->get_data($id)[0];
        $detail = $this->pengeringan_detail->get_data_detail($id);
        # Ubah Stok Gabah
        ## Kurangi Stok
        $stok_berkurang = array();
        foreach($detail as $row){
            $list = array(
                'id_stok' => $row['id_stok'],
                'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                'arus' => 'out',
                'kuantitas' => $row['kuantitas'],
                'harga' => $row['harga_satuan'],
                'sumber' => 'hasil-'.$tugas['jenis_pengeringan'],
                'id_sumber' => $id,
                'timestamp' => setNewDateTime()
            );
            $currentStok = $this->stok_gabah->get_data($row['id_stok']);
            $this->stok_gabah->update($row['id_stok'], array('stok_sisa' => ($currentStok['stok_sisa'] - $row['kuantitas'])));
            array_push($stok_berkurang, $list);
        }
        # catat log transaksi stok
        $this->stok_gabah_trx->batch_insert($stok_berkurang);

        ## Tambah Stok 
        $stok_bertambah = array(
            'tanggal' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
            'sumber' => 'hasil-'.$tugas['jenis_pengeringan'],
            'id_sumber' => $id,
            'jenis_gabah' => 'kg',
            'id_jenis_padi' => $tugas['id_jenis_padi'],
            'jenis_pengeringan' => '',
            'jenis_proses' => $tugas['jenis_proses'],
            'stok' => $tugas['berat_kering'],
            'stok_sisa' => $tugas['berat_kering'],
            'harga_satuan' => round(($tugas['kuantitas'] * $tugas['hpp_beli'])/ $tugas['berat_kering']),
            'input_by' => $tugas['input_by'],
            'timestamp_input' => setNewDateTime()
        );
        $this->stok_gabah->insert($stok_bertambah);

        # catat log transaksi stok
        $log = array(
            'id_stok' => $this->db->insert_id(),
            'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
            'arus' => 'in',
            'kuantitas' => $tugas['berat_kering'],
            'harga' => round(($tugas['kuantitas'] * $tugas['hpp_beli'])/ $tugas['berat_kering']),
            'sumber' => 'hasil-'.$tugas['jenis_pengeringan'],
            'id_sumber' => $id,
            'timestamp' => setNewDateTime()
        );
        $this->stok_gabah_trx->insert($log);

        # Update status pengeringan
        $update = array(
            'hpp_kering' => round(($tugas['kuantitas'] * $tugas['hpp_beli'])/ $tugas['berat_kering']),
            'persetujuan' => 2,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->pengeringan->update($id, $update);
        if($result){
            $this->message('Berhasil menyetujui transaksi.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function reject_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 3,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->pengeringan->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
	
}