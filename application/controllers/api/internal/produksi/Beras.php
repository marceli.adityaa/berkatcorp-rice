<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beras extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->load->model('produksi_model', 'produksi');
        $this->load->model('produksi_proses_model', 'produksi_proses');
        $this->load->model('produksi_bahan_model', 'produksi_bahan');
        $this->load->model('produksi_hasil_model', 'produksi_hasil');
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->load->model('stok_beras_model', 'stok_beras');
        $this->load->model('stok_beras_trx_model', 'stok_beras_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'produksi';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->produksi->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->produksi->get($id);
        if($detail->persetujuan != 1){
            # Delete detail produksi
            $this->produksi_bahan->delete_transaksi($id);
            $this->produksi_hasil->delete_transaksi($id);
            # Delete produksi
            $result = $this->produksi->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_bahan()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $result = $this->produksi_bahan->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_hasil()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $result = $this->produksi_hasil->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function submit_detail_tugas(){
        $post = $this->input->post();
        $insert = array();
        foreach($post['data'] as $row){
            $baris = array(
                'id_produksi' => $post['id_produksi'],
                'id_stok' => $row['id_stok'],
                'kuantitas' => $row['kuantitas'],
                'harga_satuan' => $row['harga_satuan'],
            );
            array_push($insert, $baris);
        }
        $result = $this->produksi_detail->batch_insert($insert);
        # Insert statement
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_detail()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $result = $this->produksi_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        
        echo json_encode($result);
    }

    public function submit_pengajuan_produksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $update = array(
            'status' => 1
        );

        $result = $this->produksi->update($id, $update);
        if($result){
            $this->message('Sukses mengajukan data.', 'success');
            $redirect = base_url('produksi/beras/kelola');
        }else{
            $this->message('Gagal menyimpan data', 'error');
            $redirect = base_url('produksi/beras/detail/'.$id);
        }
        
        echo json_encode(array('redirect' => $redirect, 'result' => $result));
    }

    public function batal_pengajuan_produksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status' => 0
        );

        $result = $this->produksi->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function reject_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 2,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->produksi->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
    
    public function approve_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        # stok bahan berkurang jika ada bahan yg dipakai
        $bahan_dipakai = $this->produksi_bahan->get_bahan_dipakai($id);
        $detail_hasil = $this->produksi_hasil->get_data_hasil($id);
        $valid = true;
        $terpakai = array();
        $kuantitas_hasil = 0;
        $bahan['kuantitas'] = 0;
        $bahan['kuantitas_sisa'] = 0;
        $bahan['nilai_stok'] = 0;
        $bahan['nilai_stok_sisa'] = 0;
        if(!empty($detail_hasil)){
            foreach($detail_hasil as $row){
                $kuantitas_hasil += ($row['kuantitas'] * $row['kemasan']);
            }
        }
        if(!empty($bahan_dipakai)){
            foreach($bahan_dipakai as $row){
                # dikalikan dengan jumlah_cor
                if($row['is_multiply_cor']){
                    $stok_dipakai = $row['kuantitas'] * $row['jumlah_cor'];
                    $bahan['kuantitas'] += $row['kuantitas'] * $row['jumlah_cor'];
                    $bahan['nilai_stok'] += ($row['kuantitas'] * $row['jumlah_cor'] * $row['harga']);
                }else{
                    $stok_dipakai = $row['kuantitas'];
                    $bahan['kuantitas'] += $row['kuantitas'];
                    $bahan['nilai_stok'] += ($row['kuantitas'] * $row['harga']);
                    
                }              

                $stok_bahan = $this->stok_bahan->get_stok_bahan_ready($row['id_bahan']);
                if(!empty($stok_bahan)){
                    foreach($stok_bahan as $stok_ready){
                        if($stok_dipakai > $stok_ready['sisa']){
                            # Jika pemakaian lebih dari jmlh stoknya
                            $stok_dipakai -= $stok_ready['sisa'];
                            $log = array(
                                'tgl' => $row['tanggal'],
                                'id_stok' => $stok_ready['id'],
                                'arus' => 'out',
                                'kuantitas' => $stok_ready['sisa'],
                                'harga' => $row['harga'],
                                'sumber' => 'produksi',
                                'id_sumber' => $id,
                                'timestamp' => setNewDateTime(),
                            );
                            array_push($terpakai, $log);
                        }else{
                            # Jika pemakaian kurang dari jmlh stoknya
                            $log = array(
                                'tgl' => $row['tanggal'],
                                'id_stok' => $stok_ready['id'],
                                'arus' => 'out',
                                'kuantitas' => $stok_dipakai,
                                'harga' => $row['harga'],
                                'sumber' => 'produksi',
                                'id_sumber' => $id,
                                'timestamp' => setNewDateTime(),
                            );
                            array_push($terpakai, $log);
                            $stok_dipakai = 0;
                            break;
                        }
                    }
                }else{
                    $valid = false;
                }
            }
            if($stok_dipakai > 0){
                # Stok bahan tidak mencukupi
                $valid = false;
            }else{
                # Stok bahan mencukupi, ubah stok
                if(!empty($terpakai)){
                    foreach($terpakai as $row){
                        $this->stok_bahan_trx->insert($row);
                        $dt = $this->stok_bahan->get_data($row['id_stok']);
                        $this->stok_bahan->update($row['id_stok'], array('stok_sisa' => ($dt['stok_sisa'] - $row['kuantitas'])));
                    }
                }
            }
        }
        # stok bahan bertambah jika ada keluaran produksi
        if($valid){
            $bahan_keluar = $this->produksi_bahan->get_bahan_keluaran($id);
            if(!empty($bahan_keluar)){
                foreach($bahan_keluar as $row){
                    $bahan['kuantitas'] -= $row['kuantitas'];
                    $bahan['nilai_stok'] -= ($row['kuantitas'] * $row['harga']);
    
                    $tambah_stok = array(
                        'tanggal' => $row['tanggal'],
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'id_jenis_bahan' => $row['id_bahan'],
                        'stok' => $row['kuantitas'],
                        'stok_sisa' => $row['kuantitas'],
                        'harga_satuan' => $row['harga'],
                        'input_by' => get_session('auth')['id'],
                        'timestamp_input' => setNewDateTime(),
                    );
                    $this->stok_bahan->insert($tambah_stok);

                    $log = array(
                        'tgl' => $row['tanggal'],
                        'id_stok' => $this->db->insert_id(),
                        'arus' => 'in',
                        'kuantitas' => $row['kuantitas'],
                        'harga' => $row['harga'],
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'timestamp' => setNewDateTime(),
                    );
                    $this->stok_bahan_trx->insert($log);
                   
                }
            }
        }
        # stok bahan bertambah jika ada sisa produksi 
        if($valid){
            $bahan_sisa = $this->produksi_bahan->get_bahan_sisa_produksi($id);
            if(!empty($bahan_sisa)){
                foreach($bahan_sisa as $row){
                    $bahan['kuantitas_sisa'] += $row['kuantitas'];
                    $bahan['nilai_stok_sisa'] += ($row['kuantitas'] * $row['harga']);
                    
                    $tambah_stok = array(
                        'tanggal' => $row['tanggal'],
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'id_jenis_bahan' => $row['id_bahan'],
                        'stok' => $row['kuantitas'],
                        'stok_sisa' => $row['kuantitas'],
                        'harga_satuan' => $row['harga'],
                        'input_by' => get_session('auth')['id'],
                        'timestamp_input' => setNewDateTime(),
                    );
                    $this->stok_bahan->insert($tambah_stok);
    
                    $log = array(
                        'tgl' => $row['tanggal'],
                        'id_stok' => $this->db->insert_id(),
                        'arus' => 'in',
                        'kuantitas' => $row['kuantitas'],
                        'harga' => $row['harga'],
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'timestamp' => setNewDateTime(),
                    );
                    $this->stok_bahan_trx->insert($log);
                }
            }
        }

        # stok beras bertambah sesuai hasil produksi
        if($valid){
            $hasil_produksi = $this->produksi_hasil->get_data_hasil($id);
            $hpp = round(($bahan['nilai_stok'] - $bahan['nilai_stok_sisa']) / $kuantitas_hasil);
            $susut = ($bahan['kuantitas'] - ($kuantitas_hasil + $bahan['kuantitas_sisa'])) / $bahan['kuantitas']*100;
            if(!empty($hasil_produksi)){
                foreach($hasil_produksi as $row){
                    $harga_pokok = ($hpp + $row['biaya_sak'] + $row['biaya_transport']);
                    $merk_kemasan = $this->merk_beras_kemasan->get_data_by($row['id_merk'], $row['kemasan']);
                    if($merk_kemasan == false){
                        $id_kemasan = $this->merk_beras_kemasan->create_data_kemasan($row['id_merk'], $row['kemasan']);
                    }else{
                        $id_kemasan = $merk_kemasan['id'];
                    }
                    $tambah_stok_beras = array(
                        'tanggal' => $row['tanggal'],
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'id_kemasan' => $id_kemasan,
                        'stok' => $row['kuantitas'],
                        'stok_sisa' => $row['kuantitas'],
                        'harga_pokok' => $harga_pokok,
                        'input_by' => get_session('auth')['id'],
                        'timestamp_input' => setNewDateTime(),
                    );
                    $this->stok_beras->insert($tambah_stok_beras);
    
                    $log = array(
                        'tgl' => $row['tanggal'],
                        'id_stok' => $this->db->insert_id(),
                        'arus' => 'in',
                        'kuantitas' => $row['kuantitas'],
                        'harga' => $harga_pokok,
                        'sumber' => 'produksi',
                        'id_sumber' => $id,
                        'timestamp' => setNewDateTime(),
                    );
                    $this->stok_beras_trx->insert($log);

                    $update_hpp = array(
                        'harga_pokok' => $harga_pokok
                    );
                    $this->produksi_hasil->update($row['id'], $update_hpp);
                }
            }
        }
        if($valid){
            $this->produksi->update($id, array('persetujuan' => 1));
            $this->message('Berhasil menyetujui data pengajuan.', 'success');
        }else{
            $this->message('Gagal menyetujui, harap cek ketersediaan stok.', 'error');
        }
        echo json_encode($valid);
    }
}