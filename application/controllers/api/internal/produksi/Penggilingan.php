<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penggilingan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('stok_gabah_model', 'stok_gabah');
        $this->load->model('stok_gabah_trx_model', 'stok_gabah_trx');
        $this->load->model('penggilingan_model', 'penggilingan');
        $this->load->model('penggilingan_detail_model', 'penggilingan_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penggilingan';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penggilingan->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->penggilingan->get($id);
        if($detail->persetujuan < 2){
            # Delete detail penggilingan
            $this->penggilingan_detail->delete_transaksi($id);
            # Delete penggilingan
            $result = $this->penggilingan->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function submit_detail_tugas(){
        $post = $this->input->post();
        $insert = array();
        foreach($post['data'] as $row){
            $baris = array(
                'id_penggilingan' => $post['id_penggilingan'],
                'id_stok' => $row['id_stok'],
                'kuantitas' => $row['kuantitas'],
                'harga_satuan' => $row['harga_satuan'],
            );
            array_push($insert, $baris);
        }
        $result = $this->penggilingan_detail->batch_insert($insert);
        # Insert statement
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_detail()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $result = $this->penggilingan_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        
        echo json_encode($result);
    }

    public function submit_detail_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $detail = $this->penggilingan_detail->get_data_detail($id);
        $hpp = 0;
        $qty = 0;
        $nilai = 0;
        foreach($detail as $row){
            $qty += $row['kuantitas'];
            $nilai += ($row['kuantitas'] * $row['harga_satuan']);
        }

        $update = array(
            'kuantitas' => $qty,
            'hpp_awal' => ($nilai / $qty),
            'persetujuan' => 1
        );

        $result = $this->penggilingan->update($id, $update);
        if($result){
            $this->message('Sukses menyimpan data.', 'success');
            $redirect = base_url('produksi/penggilingan/kelola');
        }else{
            $this->message('Gagal menyimpan data', 'error');
            $redirect = base_url('produksi/penggilingan/detail/'.$id);
        }
        
        echo json_encode(array('redirect' => $redirect, 'result' => $result));
    }

    public function batal_pengajuan_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 0
        );

        $result = $this->penggilingan->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function reject_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 3,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->penggilingan->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
	
}