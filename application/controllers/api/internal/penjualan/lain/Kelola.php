<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelola extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_lain_model', 'penjualan_lain');
        $this->load->model('penjualan_lain_detail_model', 'penjualan_lain_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penjualan_lain';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    
    public function json_get_trx()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_lain->get($id);
        echo json_encode($response);
    }

    public function batal_pengajuan_order()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status' => 0
        );

        $result = $this->penjualan_lain->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan transaksi.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
    
    public function delete_data_detail()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $result = $this->penjualan_lain_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
    
        echo json_encode($result);
    }

    public function delete_trx()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $this->penjualan_lain_detail->delete_transaksi($id);
        $result = $this->penjualan_lain->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
    
        echo json_encode($result);
    }

    public function simpan_draft(){
        $post = $this->input->post();
        if($post['pembayaran']['jenis_pembayaran'] == 'cash'){
            $kas = $post['pembayaran']['id_kas'];
        }else if($post['pembayaran']['jenis_pembayaran'] == 'transfer'){
            $kas = $post['pembayaran']['id_rek_pengirim'];
        }else{
            $kas = 0;
        }
        $data = array(
            'is_tempo' => $post['pembayaran']['is_tempo'],
            'tgl_tempo' => $post['pembayaran']['tgl_tempo'],
            'jenis_pembayaran' => $post['pembayaran']['jenis_pembayaran'],
            'id_kas' => $kas,
            'grandtotal' => $post['pembayaran']['grandtotal'],
            'potongan' => $post['pembayaran']['potongan'],
            'biaya_tambahan' => $post['pembayaran']['biaya_tambahan']
        );
        $result = $this->penjualan_lain->update($post['id'], $data);
        # Insert statement
        if ($result) {
            $this->message('Berhasil menyimpan draft.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function simpan_data(){
        $post = $this->input->post();
        if($post['pembayaran']['jenis_pembayaran'] == 'cash'){
            $kas = $post['pembayaran']['id_kas'];
        }else if($post['pembayaran']['jenis_pembayaran'] == 'transfer'){
            $kas = $post['pembayaran']['id_rek_pengirim'];
        }else{
            $kas = 0;
        }
        $data = array(
            'is_tempo' => $post['pembayaran']['is_tempo'],
            'tgl_tempo' => $post['pembayaran']['tgl_tempo'],
            'jenis_pembayaran' => $post['pembayaran']['jenis_pembayaran'],
            'id_kas' => $kas,
            'grandtotal' => $post['pembayaran']['grandtotal'],
            'potongan' => $post['pembayaran']['potongan'],
            'biaya_tambahan' => $post['pembayaran']['biaya_tambahan'],
            'status' => 1,
        );
        $result = $this->penjualan_lain->update($post['id'], $data);
        # Insert statement
        if ($result) {
            $this->message('Berhasil mengajukan transaksi.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function json_get_detail_trx(){
        $id = $this->input->post('id');
        $response = $this->penjualan_lain->get($id);
        echo json_encode($response);
    }

    public function penjualan_lain_tolak(){
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 2
        );

        $result = $this->penjualan_lain->update($id, $update);
        if($result){
            $this->message('Berhasil menolak pengajuan transaksi', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
}