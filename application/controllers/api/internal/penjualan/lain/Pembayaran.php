<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('kas_model', 'kas');
        $this->load->model('penjualan_lain_model', 'penjualan_lain');
        $this->load->model('penjualan_lain_detail_model', 'penjualan_lain_detail');
        $this->load->model('penjualan_lain_pembayaran_model', 'penjualan_lain_pembayaran');
        $this->load->model('pendapatan_model', 'pendapatan'); //
        $this->load->model('beban_model', 'beban'); //
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->id_potongan_penjualan = $this->config->item('id_potongan_penjualan'); //
        $this->id_biaya_penjualan = $this->config->item('id_biaya_penjualan'); // 
        $this->src = 'penjualan_lain';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_lain->get($id);
        echo json_encode($response);
    }

    public function get_sisa_tempo()
    {
        $id = $this->input->post('id');
        if(!empty($id)){

            $penjualan = $this->penjualan_lain->get_data_pembayaran_tempo($id)[0]; 
            $detail = $this->penjualan_lain_pembayaran->get_detail_pembayaran($id);
            $sisa_tempo = $penjualan['grandtotal'];
            $pembayaran_verif       = $this->penjualan_lain_pembayaran->get_data_pembayaran_verified($id, '');
            if($pembayaran_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_pembayaran = 0;
                foreach($pembayaran_verif as $row){
                    $total_pembayaran += $row['jumlah_bayar'];
                }
                $sisa_tempo -= $total_pembayaran;
            }
        }else{
            $sisa_tempo = 0;
        }
        echo json_encode($sisa_tempo);
    }

    public function get_detail_trx_pembayaran()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_lain_pembayaran->get_data($id);
        echo json_encode($response);
    }

    public function json_get_detail_pembayaran(){
        $id = $this->input->post('id');
        $response = $this->kas->join($this->penjualan_lain_pembayaran->get_detail_pembayaran_approved($id), 'kas', 'id_kas');
        echo json_encode($response);
    }

    public function delete_data_pembayaran()
    {
        $id = $this->input->post('id');
        $cek = $this->penjualan_lain_pembayaran->is_deleteable($id);
        if ($cek) {
            $response = $this->penjualan_lain_pembayaran->delete($id);
            if ($response) {
                $this->message('Sukses menghapus data', 'success');
            } else {
                $this->message('Gagal menghapus data.', 'error');
            }
        } else {
            $response = true;
            $this->message('Gagal', 'error');
        }
        echo json_encode($response);
    }

    public function setuju()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 1,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
            
            # Update status di tabel pembayaran penjualan lain
            $id_penjualan = $this->penjualan_lain_pembayaran->get_data($id)['id_penjualan'];
            $pernah_bayar = $this->penjualan_lain_pembayaran->jumlah_pembayaran($id_penjualan);
            $result = $this->penjualan_lain_pembayaran->update($id, $update);
            $detail = $this->penjualan_lain_pembayaran->get_data($id);
            
            # Update status di tabel penjualan lain
            $result2 = $this->penjualan_lain->update($detail['id_penjualan'], array('is_tempo_dibayar' => 1, 'is_lunas' => $detail['is_paid_off'], 'tgl_bayar' => $detail['tgl_pembayaran'], 'id_kas' => $detail['id_kas'], 'jenis_pembayaran' => $detail['jenis_pembayaran']));
            
            $penjualan = $this->penjualan_lain->get_data($detail['id_penjualan']);

            if(!$pernah_bayar){
                # Jika belum ada transaksi pembayaran, maka melakukan init transaksi potongan / tambahan jika ada dan arus kas dengan penyesuaian jumlah bayar

                # Insert transaksi di buku kas 
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tgl_pembayaran'];
                $arus_kas['arus'] = "in";
                $arus_kas['sumber'] = "penjualan-lain-bayar-tempo";
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = 'Bayar Tempo '.$penjualan['kode'];
                $arus_kas['deskripsi'] = 'Jumlah bayar : '.$detail['jumlah_bayar'];
                $arus_kas['nominal'] = $detail['jumlah_bayar'] + $penjualan['potongan'] - $penjualan['biaya_tambahan'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $response = json_decode(xcurl($param_curl)['response']);

                if($penjualan['potongan'] > 0){
                    # Jika terdapat potongan penjualan, maka insert ke pendapatan lain (auto acc) dan buku kas
                    # Insert ke beban
                    $beban['id_beban'] = $this->id_potongan_penjualan;
                    $beban['tgl_transaksi'] = $penjualan['tgl_bayar'];
                    $beban['nominal'] = $penjualan['potongan'];
                    $beban['catatan'] = 'Potongan penjualan lain '.$penjualan['kode'];
                    $beban['jenis_pembayaran'] = $penjualan['jenis_pembayaran'];
                    $beban['id_kas'] = $penjualan['id_kas'];
                    $beban['is_verifikasi'] = 1;
                    $beban['input_by'] = get_session('auth')['id'];
                    $beban['timestamp_input'] = setNewDateTime();
                    $beban['verifikasi_by'] = get_session('auth')['id'];
                    $beban['timestamp_verifikasi'] = setNewDateTime();
                    $result = $this->beban->insert($beban);

                    # Insert ke buku kas
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $penjualan['id_kas'];
                    $arus_kas['tgl_transaksi'] = $penjualan['tgl_bayar'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = "beban-penjualan-lain";
                    $arus_kas['id_sumber'] = $penjualan['id'];
                    $arus_kas['keterangan'] = 'Potongan penjualan lain '.$penjualan['kode'];
                    $arus_kas['deskripsi'] = '';
                    $arus_kas['nominal'] = $penjualan['potongan'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
    
                if($penjualan['biaya_tambahan'] > 0){
                    # Jika terdapat biaya tambahan penjualan, maka insert ke beban penjualan (auto acc) dan buku kas
                    # Insert ke pendapatan
                    $pendapatan['id_kategori'] = $this->id_biaya_penjualan;
                    $pendapatan['tgl_transaksi'] = $penjualan['tgl_bayar'];
                    $pendapatan['nominal'] = $penjualan['biaya_tambahan'];
                    $pendapatan['catatan'] = 'Biaya tambahan penjualan lain '.$penjualan['kode'];
                    $pendapatan['jenis_pembayaran'] = $penjualan['jenis_pembayaran'];
                    $pendapatan['id_kas'] = $penjualan['id_kas'];
                    $pendapatan['is_verifikasi'] = 1;
                    $pendapatan['input_by'] = get_session('auth')['id'];
                    $pendapatan['timestamp_input'] = setNewDateTime();
                    $pendapatan['verifikasi_by'] = get_session('auth')['id'];
                    $pendapatan['timestamp_verifikasi'] = setNewDateTime();
                    $result = $this->pendapatan->insert($pendapatan);

                    # Insert ke buku kas
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $penjualan['id_kas'];
                    $arus_kas['tgl_transaksi'] = $penjualan['tgl_bayar'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = "pendapatan-penjualan-lain";
                    $arus_kas['id_sumber'] = $penjualan['id'];
                    $arus_kas['keterangan'] = 'Biaya tambahan penjualan lain '.$penjualan['kode'];
                    $arus_kas['deskripsi'] = '';
                    $arus_kas['nominal'] = $penjualan['biaya_tambahan'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
            }else{
                # Jika sudah pernah transaksi pembayaran, maka jumlah bayar tidak disesuaikan
                # Insert transaksi di buku kas 
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tgl_pembayaran'];
                $arus_kas['arus'] = "in";
                $arus_kas['sumber'] = "penjualan-lain-bayar-tempo";
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = 'Bayar Tempo '.$penjualan['kode'];
                $arus_kas['deskripsi'] = $detail['catatan'];
                $arus_kas['nominal'] = $detail['jumlah_bayar'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $response = json_decode(xcurl($param_curl)['response']);
            }

        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menyetujui pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

    public function tolak()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $update = array(
                'status_persetujuan' => 2,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime()
            );
            $response = $this->penjualan_lain_pembayaran->update($id, $update);
            
        }else{
            $response = false;
        }

        if ($response) {
			$this->message('Berhasil menolak pengajuan', 'success');
		} else {
			$this->message('Gagal', 'error');
        }
        
        echo json_encode($response);
    }

}