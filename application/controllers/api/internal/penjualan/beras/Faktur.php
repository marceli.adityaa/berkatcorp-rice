<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faktur extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penjualan_po';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po->get($id);
        echo json_encode($response);
    }

    public function json_get_detail_trx()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->penjualan_po->get($id);
        if($detail['status_po'] == 0){
            # Delete detail penjualan_po
            $this->penjualan_po_detail->delete_transaksi($id);
            # Delete penjualan_po
            $result = $this->penjualan_po->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function simpan_data(){
        $post = $this->input->post();
        foreach($post['data_harga'] as $row){
            $update = array(
                'harga_satuan' => $row['harga_satuan'],
            );
            $this->penjualan_po_detail->update($row['id'], $update);
        }
        if($post['pembayaran']['jenis_pembayaran'] == 'cash'){
            $kas = $post['pembayaran']['id_kas'];
        }else if($post['pembayaran']['jenis_pembayaran'] == 'transfer'){
            $kas = $post['pembayaran']['id_rek_pengirim'];
        }else{
            $kas = 0;
        }
        $tgl_kirim = $post['tgl_kirim'];
        $kode = $this->generate_kode_sj($tgl_kirim);
        $data = array(
            'tgl_kirim' => $post['tgl_kirim'],
            'kode_surat_jalan' => $kode,
            'no_resi' => $post['no_resi'],
            'is_tempo' => $post['pembayaran']['is_tempo'],
            'tgl_tempo' => $post['pembayaran']['tgl_tempo'],
            'jenis_pembayaran' => $post['pembayaran']['jenis_pembayaran'],
            'id_kas' => $kas,
            'potongan' => $post['pembayaran']['potongan'],
            'biaya_tambahan' => $post['pembayaran']['tambahan'],
            'grandtotal' => $post['pembayaran']['grandtotal'],
            'jumlah_bayar' => $post['pembayaran']['grandtotal']
        );
        $result = $this->penjualan_po->update($post['id'], $data);
        # Insert statement
        if ($result) {
            $this->message('Berhasil menyimpan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function proses_faktur()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_selesai' => 1
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Berhasil menyelesaikan faktur penjualan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function batal_pengajuan_faktur()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_selesai' => 0
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan faktur penjualan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function penjualan_beras_tolak(){
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'persetujuan' => 2
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Berhasil menolak pengajuan transaksi', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
    public function generate_kode_sj($tgl)
    {
        $last = $this->penjualan_po->get_kode_sj($tgl);
        $index = 1;
        // $last = 'SJ-001/04/20 ';
        if ($last != false) {
            $temp = explode('-', $last);
            $temp2 = explode('/', $temp[1]);
            $index = intval($temp2[0]) + 1;
        }
        $kode = 'SJ-' . zerofy($index, 3) . '/' . date("m", strtotime($tgl)) . '/' . date("y", strtotime($tgl));
        return $kode;
    }
	
}