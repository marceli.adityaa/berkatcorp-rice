<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->url_transport = $this->config->item('url_transport');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'ekspedisi';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_po()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po_detail->get_data_detail($id);
        echo json_encode($response);
    }

    public function json_get_detail_ekspedisi(){
        $resi = $this->input->post('resi');
        $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_detail_ekspedisi?token='.$this->session->auth['token'].'&resi='.$resi); 
        $response = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
        echo json_encode($response);
    }

    public function reset_resi_pengiriman(){
        $id = $this->input->post('id');
        $resi = $this->input->post('resi');
        $result = $this->penjualan_po->update($id, array('no_resi' => '', 'status_pengiriman' => 0));
        $list = $this->penjualan_po->get_muatan_ekspedisi($resi);

        $muatan = 0;
        if(!empty($list)){
            foreach($list as $row){
                $muatan += $row['tonase'];
            }
        }
        $param_update    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/update_muatan_client?token='.$this->session->auth['token'].'&resi='.$resi.'&muatan='.$muatan);
        xcurl($param_update);
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function proses_kiriman(){
        $resi = $this->input->post('resi');
        $param_update    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/proses_kiriman?token='.$this->session->auth['token'].'&resi='.$resi);
        $result = json_decode(xcurl($param_update)['response']);
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function batal_kiriman(){
        $resi = $this->input->post('resi');
        $param_update    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/batal_kiriman?token='.$this->session->auth['token'].'&resi='.$resi);
        $result = json_decode(xcurl($param_update)['response']);
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }
	
}