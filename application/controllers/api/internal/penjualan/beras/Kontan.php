<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kontan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('penjualan_po_kontan_model', 'penjualan_po_kontan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penjualan_po';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po->get($id);
        echo json_encode($response);
    }

    public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po_kontan->get($id);
        echo json_encode($response);
    }

    public function simpan_data(){
        $post = $this->input->post();
        
        if($post['pembayaran']['jenis_pembayaran'] == 'cash'){
            $kas = $post['pembayaran']['id_kas'];
        }else if($post['pembayaran']['jenis_pembayaran'] == 'transfer'){
            $kas = $post['pembayaran']['id_rek_pengirim'];
        }else{
            $kas = 0;
        }
        $data = array(
            'jenis_pembayaran' => $post['pembayaran']['jenis_pembayaran'],
            'id_kas' => $kas,
            'grandtotal' => $post['pembayaran']['grandtotal']
        );
        $result = $this->penjualan_po->update($post['id'], $data);
        # Insert statement
        if ($result) {
            $this->message('Berhasil menyimpan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function proses_faktur()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_selesai' => 1
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Berhasil menyelesaikan faktur penjualan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function batal_pengajuan_faktur()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_selesai' => 0
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan faktur penjualan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
    
    public function delete_data_detail()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $result = $this->penjualan_po_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
    
        echo json_encode($result);
    }

    public function delete_data_detail_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $this->penjualan_po_detail->delete_detail_kontan($id);
        $result = $this->penjualan_po_kontan->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
    
        echo json_encode($result);
    }
}