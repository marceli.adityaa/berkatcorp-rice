<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Surat_pemesanan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penjualan_po';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_po->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->penjualan_po->get($id);
        if($detail['status_po'] == 0){
            # Delete detail penjualan_po
            $this->penjualan_po_detail->delete_transaksi($id);
            # Delete penjualan_po
            $result = $this->penjualan_po->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function simpan_data_pemesanan(){
        $post = $this->input->post();
        $insert = array();
        foreach($post['data'] as $row){
            $baris = array(
                'id_po' => $post['id'],
                'id_kemasan' => $row['id_kemasan'],
                'kuantitas' => $row['kuantitas'],
            );
            array_push($insert, $baris);
        }
        $this->penjualan_po_detail->delete_transaksi($post['id']);
        $result = $this->penjualan_po_detail->batch_insert($insert);
        # Insert statement
        if ($result) {
            $this->message('Berhasil menyimpan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function proses_order()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_po' => 1
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Sukses memproses pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function batal_pengajuan_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status_po' => 0
        );

        $result = $this->penjualan_po->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    // public function reject_transaksi()
    // {
    //     $token = $this->input->post('token');
    //     $id = $this->input->post('id');
    //     # Autentikasi Token
    //     $this->validateToken($token);

    //     $update = array(
    //         'persetujuan' => 3,
    //         'verif_by' => get_session('auth')['id'],
    //         'timestamp_verif' => setNewDateTime()
    //     );

    //     $result = $this->penjualan_po->update($id, $update);
    //     if($result){
    //         $this->message('Sukses membatalkan pengajuan.', 'success');
    //     }else{
    //         $this->message('Gagal mengubah data', 'error');
    //     }
        
    //     echo json_encode($result);
    // }
	
}