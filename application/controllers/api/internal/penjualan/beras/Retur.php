<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_retur_model', 'penjualan_retur');
        $this->load->model('penjualan_retur_detail_model', 'penjualan_retur_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'penjualan_retur';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_retur->get($id);
        echo json_encode($response);
    }

    public function delete_data_transaksi()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->penjualan_retur->get($id);
        if($detail['status'] == 0){
            # Delete detail penjualan_retur
            $this->penjualan_retur_detail->delete_transaksi($id);
            # Delete penjualan_retur
            $result = $this->penjualan_retur->delete($id);
            if($result){
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function simpan_data()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status' => 1
        );

        $result = $this->penjualan_retur->update($id, $update);
        if($result){
            $this->message('Sukses menyimpan data.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function batal_pengajuan_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        $update = array(
            'status' => 0
        );

        $result = $this->penjualan_retur->update($id, $update);
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
	
}