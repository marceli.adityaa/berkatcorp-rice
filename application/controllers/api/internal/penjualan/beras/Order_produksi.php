<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_produksi extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('penjualan_produksi_model', 'penjualan_produksi');
        $this->load->model('penjualan_produksi_detail_model', 'penjualan_produksi_detail');
        $this->load->model('penjualan_produksi_proses_model', 'penjualan_produksi_proses');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'order_produksi';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

    public function json_get_detail_tugas()
    {
        $id = $this->input->post('id');
        $response = $this->penjualan_produksi->get($id);
        echo json_encode($response);
    }

    public function delete_data_tugas()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        $detail = $this->penjualan_produksi->get($id);
        $list_po = $this->penjualan_produksi_detail->get_data_po($id);
        if($detail['status'] == 0){
            # Delete detail penjualan_produksi
            $this->penjualan_produksi_detail->delete_transaksi($id);
            # Delete penjualan_produksi
            $result = $this->penjualan_produksi->delete($id);
            if($result){
                if(!empty($list_po)){
                    foreach($list_po as $po){
                        $this->penjualan_po->update($po['id_po'], array('status_produksi' => 0));
                    }
                }
                $this->message('Sukses menghapus data.', 'success');
            }else{
                $this->message('Gagal menghapus data', 'error');
            }
        }else{
            $result = false;
            $this->message('Tidak diijinkan menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function delete_data_po()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        $detail = $this->penjualan_produksi_detail->get($id);
        $this->penjualan_po->update($detail->id_po, array('status_produksi' => 0));
        $result = $this->penjualan_produksi_detail->delete($id);
        if($result){
            $this->message('Sukses menghapus data.', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
    
        echo json_encode($result);
    }

    public function simpan_data_pemesanan(){
        $post = $this->input->post();
        $this->penjualan_produksi_proses->delete_transaksi($post['id']);
        $result = $this->penjualan_produksi_proses->batch_insert($post['data']);
        # Insert statement
        if ($result) {
            $this->message('Berhasil menyimpan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        echo json_encode($result);
    }

    public function proses_order()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);
        
        # Update status data PO
        $data = $this->penjualan_produksi_detail->get_data_po($id);
        foreach($data as $row){
            $this->penjualan_po->update($row['id_po'], array('status_produksi' => 1));
        }

        $result = $this->penjualan_produksi->update($id, array('status' => 1));
        if($result){
            $this->message('Sukses memproses pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }

    public function batal_pengajuan_proses()
    {
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        # Autentikasi Token
        $this->validateToken($token);

        # Update status data PO
        $data = $this->penjualan_produksi_detail->get_data_po($id);
        // foreach($data as $row){
        //     $this->penjualan_po->update($row['id_po'], array('status_produksi' => 0));
        // }

        $result = $this->penjualan_produksi->update($id, array('status' => 0));
        if($result){
            $this->message('Sukses membatalkan pengajuan.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        
        echo json_encode($result);
    }
	
}