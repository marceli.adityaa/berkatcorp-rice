<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bahan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'stok-bahan';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

	public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $result = $this->stok_bahan->get_data($id);
        echo json_encode($result);
    }	

    public function delete_data_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $this->stok_bahan_trx->hapus_trx_stok($id);
            $result = $this->stok_bahan->delete($id);
        }else{
            $result = false;
        }

        if ($result) {
            $this->message('Berhasil menghapus data.', 'success');
        } else {
            $this->message('Gagal menghapus data', 'error');
        }

        echo json_encode($result);
    }

    public function json_get_detail_transaksi_stok()
    {
        $post = $this->input->post();
        $response = $this->stok_bahan_trx->get_detail_rekap_stok($post);
        echo json_encode($response);
    }
}