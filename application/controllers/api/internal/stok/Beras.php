<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beras extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('stok_beras_model', 'stok_beras');
        $this->load->model('stok_beras_trx_model', 'stok_beras_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'stok-bahan';
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
	}

	public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $result = $this->stok_beras->get_data($id);
        echo json_encode($result);
    }	

    public function delete_data_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $this->stok_beras_trx->hapus_trx_stok($id);
            $result = $this->stok_beras->delete($id);
        }else{
            $result = false;
        }

        if ($result) {
            $this->message('Berhasil menghapus data.', 'success');
        } else {
            $this->message('Gagal menghapus data', 'error');
        }

        echo json_encode($result);
    }

    public function json_get_detail_transaksi_stok()
    {
        $post = $this->input->post();
        $response = $this->stok_beras_trx->get_detail_rekap_stok($post);
        echo json_encode($response);
    }
}