<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merk_beras extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('merk_beras_model', 'merk_beras');
		$this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
		$this->load->model('merk_beras_harga_model', 'merk_beras_harga');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function example($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
    }
    
    public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->merk_beras->as_array()->get($id);
		echo json_encode($response);
	}

	public function json_get_detail_kemasan()
    {
        $id = $this->input->post('id');
        $result = $this->merk_beras_kemasan->get_data($id);
        echo json_encode($result);
    }	

    public function json_get_riwayat_kemasan()
    {
        $id = $this->input->post('id');
        $result = $this->merk_beras_harga->get_data_riwayat($id);
        echo json_encode($result);
    }	

    public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->merk_beras->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->merk_beras->update($id, array('status' => 0));
		echo json_encode($response);
    }
    
    public function set_kemasan_aktif(){
		$id = $this->input->post('id');
		$response = $this->merk_beras_kemasan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_kemasan_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->merk_beras_kemasan->update($id, array('status' => 0));
		echo json_encode($response);
	}
}