<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends MY_Controller_api
{
    protected $auth_type = 'external';
    
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Menu_model','model');
    }
    
    public function get_all($token = '')
    {
        // $this->auth($token, true);
        die(json_encode($this->model->get_data()));
    }

    public function get_active($token = ''){
        // $this->auth($token, true);
        die(json_encode($this->model->get_active()));
    }
}