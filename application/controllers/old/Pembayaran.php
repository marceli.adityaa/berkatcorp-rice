<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('customer_model', 'customer');
		$this->load->model('user_model', 'user');
		$this->load->model('kas_model', 'kas');
		$this->load->model('rekening_model', 'rekening');
		$this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function pembayaran()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pby-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['pembelian'] = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_pembayaran_tempo(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu", 2 => "Lunas");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');

            $data['pic'] = $this->penanggung_jawab->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembayaran Tempo' => 'active', 'Pembelian Gabah' => 'active'));
            set_session('title', 'Pembayaran Tempo - Pembelian Gabah');
            set_activemenu('sub-pembayaran', 'menu-pembayaran-pembelian-gabah');
            $this->render('pembayaran/v-pembayaran-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }
}
