<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Gabah extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    // public function kelola()
    // {
    //     $hak = json_decode($this->session->auth['hak_akses']);
    //     if (in_array('pbl-gbh', $hak->bas) || $hak->bas[0] == '*') {
    //         $data['supplier'] = $this->customer->get_active_supplier();
    //         if (empty($this->input->get())) {
    //             $_GET['status'] = 2;
    //         }
    //         $data['pembelian']  = $this->user->join($this->pembelian_gabah->get_data(), 'username_input', 'input_by');
    //         $data['role'] = $this->role;
    //         $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_active?token='.$this->session->auth['token']);        
    //         $data['ekspedisi']  = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
    //         $data['resi'] = array("123", "456");
    //         $data['status'] = array(1 => "Bongkar Muatan", 2 => "Menunggu Input Harga", 3 => "Selesai");
    //         set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active'));
    //         set_session('title', 'Pembelian Gabah - Kelola Transaksi');
    //         set_activemenu('sub-pembelian', 'menu-pembelian-gabah');
    //         $this->render('pembelian/gabah/v-pembelian-gabah', $data);
    //     } else {
    //         show_error("Anda tidak memiliki hak akses untuk halaman ini.");
    //     }
    // }

    public function quality_control()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['supplier'] = $this->customer->get_active_supplier();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc(), 'username_input', 'input_by');
            $data['status'] = array(1 => "Bongkar Muatan", 2 => "Menunggu Input Harga", 3 => "Selesai");
            $data['role'] = $this->role;
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Quality Control' => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function batch($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['jenis_sak'] = $this->jenis_sak->get_active();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
            $data['jenis'] = $this->pembelian_gabah_jenis->get_data_jenis_pembelian($id);
            $data['manual'] = $this->pembelian_gabah_jenis->get_data_jenis_pembelian($id, 1);
            $data['batch'] = $this->user->join($this->pembelian_gabah_batch->get_data($id), 'username_input', 'input_by');
            $data['batch_idx'] = $this->pembelian_gabah_batch->get_data_last($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => 'active', $data['pembelian']['kode_pembelian'] => 'active'));
            $data['sisa_bruto'] = $data['pembelian']['berat_bruto'];
            $data['role'] = $this->role;
            if ($data['batch_idx'] > 1) {
                foreach ($data['batch'] as $row) {
                    $data['sisa_bruto'] -= $row['berat_netto'];
                }
            }
            if(!empty($data['batch'])){
                foreach($data['batch'] as $key => $row){
                    $sak = 0;
                    if(!empty($data['jenis'])){
                        foreach($data['jenis'] as $row2){
                            if($row['id'] == $row2['id_batch']){
                                $sak += $row2['jumlah_sak'];
                            }
                        }
                    }
                    $data['batch'][$key]['sak'] = $sak;
                }
            }
            set_session('title', 'Quality Control Gabah - Timbangan');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-batch', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function qc_transaksi($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['role'] = $this->role;
            if($data['pembelian']['is_manual'] == 1){
                # Jika Timbang Manual
                $data['kadar_air'] = $this->kadar_air->get_active();
                $data['jenis_gabah'] = $this->jenis_gabah->get_active();
                $data['jenis_padi'] = $this->jenis_padi->get_active();
                $data['detail'] = $this->pembelian_gabah_jenis->get_detail_muatan($id, false);
                // dump($data['detail']);
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Quality Control Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => 'active'));
                set_session('title', 'Quality Control Gabah');
                set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
                $this->render('pembelian/gabah/v-pembelian-gabah-qc-manual', $data);
            }else{
                # Jika Timbang Standard
                $data['sap'] = $this->pembelian_gabah_jenis->get_index_sap($data['pembelian']['id_pembelian']);
                $data['detail_sap'] = $this->pembelian_gabah_jenis->get_detail_sap($id);
                $data['detail'] = $this->user->join($this->pembelian_gabah_jenis->get_detail_muatan($id, true), 'username_input', 'input_by');
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Quality Control Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => 'active'));
                set_session('title', 'Quality Control Gabah');
                set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
                $this->render('pembelian/gabah/v-pembelian-gabah-qc-transaksi', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function qc_form($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['kadar_air'] = $this->kadar_air->get_active();
            $data['jenis_gabah'] = $this->jenis_gabah->get_active();
            $data['jenis_padi'] = $this->jenis_padi->get_active();
            $data['role'] = $this->role;
            if (!empty($sap = $this->input->get('sap'))) {
                $data['sap'] = $sap;
            } else {
                $data['sap'] = 1;
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Quality Control Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => base_url('pembelian/gabah/qc_transaksi/' . $id), 'Form' => 'active'));
            set_session('title', 'Quality Control Gabah - Form Input');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-form', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function edit_form($id){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $sap = $this->input->get('sap');
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['kadar_air'] = $this->kadar_air->get_active();
            $data['jenis_gabah'] = $this->jenis_gabah->get_active();
            $data['jenis_padi'] = $this->jenis_padi->get_active();
            $data['jenis_ks'] = $this->pembelian_gabah_jenis->get_data_jenis_ks($id);
            $data['jenis_kg'] = $this->pembelian_gabah_jenis->get_data_jenis_kg($id);
            $data['detail'] = $this->pembelian_gabah_detail->get_data_detail($id, $sap);
            $data['role'] = $this->role;
            $jumlah_sak = 0;
            $sap = 0;
            foreach($data['detail'] as $row){
                $sap = $row['sap'];
                $jumlah_sak += $row['jumlah_sak'];
            }
            $data['sap'] = $sap;
            $data['jumlah_sak'] = $jumlah_sak;
            $data['id_batch'] = $id;
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Quality Control Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => base_url('pembelian/gabah/qc_transaksi/' . $id), 'Edit Data' => 'active'));
            set_session('title', 'Quality Control Gabah - Form Input');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-form-edit', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function input_harga($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-gbh', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;

            $data['pic'] = $this->penanggung_jawab->get_active();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
            $data['ks_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'ks');
            $data['ks_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'ks');
            $data['kg_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'kg');
            $data['kg_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'kg');
            $data['total_sak'] = $this->pembelian_gabah_detail->get_total_sak($id);
            # Berat KS setelah dikurangi kadar air
            if(!empty($data['ks_detail'])){
                foreach($data['ks_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['ks_total_berat_standar'][$a['id_batch']])){
                                    $data['ks_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['ks_total_berat_manual'][$a['id_batch']])){
                                    $data['ks_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }

            # Berat KG setelah dikurangi kadar air
            if(!empty($data['kg_detail'])){
                foreach($data['kg_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['kg_total_berat_standar'][$a['id_batch']])){
                                    $data['kg_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['kg_total_berat_manual'][$a['id_batch']])){
                                    $data['kg_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }
    
    
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/kelola'), 'Input Harga' => 'active'));
            set_session('title', 'Pembelian Gabah - Form Input Harga');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah-input-harga', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function pembayaran_tempo()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pby-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['pembelian'] = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_pembayaran_tempo(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu", 2 => "Lunas");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');

            $data['pic'] = $this->penanggung_jawab->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Pembayaran Tempo' => 'active'));
            set_session('title', 'Pembayaran Tempo - Pembelian Gabah');
            set_activemenu('sub-pembelian', 'menu-pembayaran-pembelian-gabah');
            $this->render('pembayaran/v-pembayaran-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_pembayaran_tempo($id = null)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pby-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $data['kas']        = json_decode(xcurl($param_kas)['response']);
                $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
                $data['pembayaran'] = array('cash', 'transfer');
                $data['role'] = $this->role;
                $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_pembayaran_tempo($id), 'submit_by', 'diajukan_oleh');
                $data['detail'] = $this->user->join($this->kas->join($this->user->join($this->pembelian_gabah_pembayaran->get_detail_pembayaran($id), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'acc', 'acc_by');
                $data['sisa_tempo'] = $data['pembelian']['grandtotal'] - $data['pembelian']['bayar_dp'];
                $pembayaran_verif = $this->pembelian_gabah_pembayaran->get_data_pembayaran_verified($id, '');
                if($pembayaran_verif != false){
                    # Ada riwayat transaksi yg terverifikasi
                    $total_pembayaran = 0;
                    foreach($pembayaran_verif as $row){
                        $total_pembayaran += $row['jumlah_bayar'];
                    }
                    $data['sisa_tempo'] -= $total_pembayaran;
                }
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/pembayaran_tempo'), 'Pembayaran Tempo' => base_url('pembelian/gabah/pembayaran_tempo'), 'Detail Pembayaran' => 'active'));
                set_session('title', 'Kelola Piutang');
                set_activemenu('sub-pembelian', 'menu-pembayaran-pembelian-gabah');
                $this->render('pembelian/gabah/v-pembelian-gabah-pembayaran-detail', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-pbl-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
			$data['pembelian'] = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_persetujuan(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['pic'] = $this->penanggung_jawab->get_active();
			// dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Pembelian Gabah - Persetujuan Transaksi');
            set_activemenu('sub-pembelian', 'menu-persetujuan-pembelian-gabah');
            $this->render('pembelian/gabah/v-persetujuan-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }
    
    public function otorisasi_pembayaran()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-pby-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
			$data['pembayaran'] = $this->user->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah_pembayaran->get_data_otorisasi_pembayaran(), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'acc', 'acc_by');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Otorisasi Pembelian' => 'active'));
            set_session('title', 'Pembelian Gabah - Otorisasi Pembayaran');
            set_activemenu('sub-pembelian', 'menu-persetujuan-pembayaran-gabah');
			// dump($data);
            $this->render('pembelian/gabah/v-otorisasi-pembayaran-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    // public function submit_pembelian_gabah()
    // {
    //     $post = $this->input->post();
    //     $url = $post['url'];
    //     unset($post['url']);
    //     if (!$post['id']) {
    //         # Insert Statement
    //         $post['kode_pembelian'] = $this->generate_kode($post['tanggal_pembelian'], $post['id_supplier']);
    //         $post['status_muatan'] = 0;
    //         $post['status_selesai'] = 0;
    //         $post['input_by'] = get_session('auth')['id'];
    //         $post['timestamp_input'] = setNewDateTime();
    //         $result = $this->pembelian_gabah->insert($post);
    //         if ($result) {
    //             $this->message('Sukses memasukkan data', 'success');
    //         } else {
    //             $this->message('Gagal', 'error');
    //         }
    //     } else {
    //         # Update Statement
    //         $id = $post['id'];
    //         unset($post['id']);
    //         $result = $this->pembelian_gabah->update($id, $post);
    //         if ($result) {
    //             $this->message('Sukses mengubah data', 'success');
    //         } else {
    //             $this->message('Gagal', 'error');
    //         }
    //     }
    //     $this->go('pembelian/gabah/kelola?'.$url);
    // }

    // public function submit_pembelian_gabah_batch()
    // {
    //     $post = $this->input->post();
    //     if(!empty($post['is_manual'])){
    //         $post['is_manual'] = 1;
    //     }else{
    //         $post['is_manual'] = 0;
    //     }
    //     // dump($post);
    //     if (!$post['id']) {
    //         # Insert Statement
    //         $post['input_by'] = get_session('auth')['id'];
    //         $result = $this->pembelian_gabah_batch->insert($post);
    //         if ($result) {
    //             $this->message('Sukses memasukkan data', 'success');
    //             $this->go('pembelian/gabah/qc_transaksi/' . $this->db->insert_id());
    //         } else {
    //             $this->message('Gagal', 'error');
    //             $this->go('pembelian/gabah/batch/' . $post['id_pembelian']);
    //         }
    //     } else {
    //         # Update Statement
    //         $id = $post['id'];
    //         unset($post['id']);
    //         $result = $this->pembelian_gabah_batch->update($id, $post);
    //         if ($result) {
    //             $this->message('Sukses mengubah data', 'success');
    //         } else {
    //             $this->message('Gagal', 'error');
    //         }
    //         $this->go('pembelian/gabah/batch/' . $post['id_pembelian']);
    //     }
    // }

    

    public function submit_pembayaran_tempo(){
        $post = $this->input->post();
        unset($post['sisa_bayar_val']);
        unset($post['sisa_bayar']);
        if($post['jenis_pembayaran'] == 'cash'){
            $post['id_kas'] = $post['kas'];
        }else{
            $post['id_kas'] = $post['rekening'];
        }
        unset($post['kas']);
        unset($post['rekening']);
        $post['jumlah_bayar'] = str_replace(",", "", $post['jumlah_bayar']);
        $id_pembelian = $post['id_pembelian'];

        if(empty($post['id'])){
            # Insert Statement
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->pembelian_gabah_pembayaran->insert($post);
            if ($result) {
                $this->message('Berhasil mengajukan pembayaran', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }else{
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->pembelian_gabah_pembayaran->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('pembelian/gabah/detail_pembayaran_tempo/'.$id_pembelian);
    }

    public function pembelian_gabah_submit_approval(){
        $post = $this->input->post();
		if(!empty($post['id'])){
			$update = array(
				'is_tempo' => $post['is_tempo'],
				'tgl_tempo' => $post['tgl_tempo'],
				'jenis_pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $post['id_kas'],
				'id_rek_pengirim' => $post['id_rek_pengirim'],
				'id_rek_penerima' => $post['id_rek_penerima'],
                'bayar_dp' => str_replace(",", "", $post['bayar_dp']),
                'jumlah_bayar' => str_replace(",", "", $post['jumlah_bayar']),
				'dibayar_oleh' => $post['dibayar'],
				'checker' => $post['checker'],
				'tgl_bayar' => $post['tgl_bayar'],
				'status_persetujuan' => 1,
				'verifikasi_oleh' => get_session('auth')['id'],
				'timestamp_verifikasi' => setNewDateTime(),
			);

			$result = $this->pembelian_gabah->update($post['id'], $update);
			
			if($result){
				$pembelian = $this->pembelian_gabah->get_data($post['id']);
				if($post['is_tempo'] == 1){
					# Jika bayar tempo
					if($post['bayar_dp'] > 0){
						$arus_kas['id_perusahaan'] = $this->id_perusahaan;
						if($pembelian['jenis_pembayaran'] == 'cash'){
							$arus_kas['id_kas'] = $pembelian['id_kas'];
						}else if($pembelian['jenis_pembayaran'] == 'transfer'){
							$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
						}
						$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
						$arus_kas['arus'] = "out";
						$arus_kas['sumber'] = "pembelian-gabah";
						$arus_kas['id_sumber'] = $post['id'];
						$arus_kas['keterangan'] = 'DP Pembelian Gabah - '.$pembelian['kode_pembelian'];
                        $arus_kas['deskripsi'] = 'Bayar Tempo, Total : '.$post['jumlah_bayar'];
						$arus_kas['nominal'] = str_replace(",", "", $post['bayar_dp']);
						$arus_kas['input_by'] = get_session('auth')['id'];
						$arus_kas['input_timestamp'] = setNewDateTime();
						$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
						$result = json_decode(xcurl($param_curl)['response']);
					}
				}else{
					# Jika bayar lunas
					$arus_kas['id_perusahaan'] = $this->id_perusahaan;
					if($pembelian['jenis_pembayaran'] == 'cash'){
						$arus_kas['id_kas'] = $pembelian['id_kas'];
					}else if($pembelian['jenis_pembayaran'] == 'transfer'){
						$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
					}
					$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
					$arus_kas['arus'] = "out";
					$arus_kas['sumber'] = "pembelian-gabah";
					$arus_kas['id_sumber'] = $post['id'];
					$arus_kas['keterangan'] = 'Pembelian Gabah - '.$pembelian['kode_pembelian'];
                    $arus_kas['deskripsi'] = 'Bayar Lunas';
					$arus_kas['nominal'] = str_replace(",", "", $post['jumlah_bayar']);
					$arus_kas['input_by'] = get_session('auth')['id'];
					$arus_kas['input_timestamp'] = setNewDateTime();
					$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
					$result = json_decode(xcurl($param_curl)['response']);
				}
			}
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('pembelian/gabah/persetujuan?'.$post['url']);
    }
    
    public function generate_kode($tgl, $supplier)
    {
        $last = $this->pembelian_gabah->get_kode_pembelian($tgl, $supplier);
        $index = 1;
        // $last = '1-191001-001';
        if ($last != false) {
            $temp = explode('.', $last);
            $index = intval($temp[2]) + 1;
        }
        $kode = 'PB.' . zerofy($supplier, 3) . '-' . date("ymd", strtotime($tgl)) . '.' . $index;
        return $kode;
    }

    

    public function cetak_nota($id)
    {
        $get = $this->input->get();
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_cetak_nota($id), 'username_input', 'input_by');
        $data['tgl_pembelian'] = tanggal_indo($data['pembelian']['tanggal_pembelian']);
        
        $detail = $this->pembelian_gabah->get_data_nota($id, $get);
        $data['detail'] = array();
        $idx = 0;
        foreach($detail as $key => $row){
            if($key == 0){
                $temp2[$idx][$row['jenis_gabah']] = array();
                $temp['batch'] = $row['batch'];
                $temp['jenis_gabah'] = $row['jenis_gabah'];
                $temp['padi'] = $row['padi'];
            }
            if($row['batch'] == $temp['batch'] && $row['jenis_gabah'] == $temp['jenis_gabah']){
                $data['detail'][$idx]['batch'] = $row['batch'];
                $data['detail'][$idx]['jenis_gabah'] = $row['jenis_gabah'];
                $data['detail'][$idx]['batch_label'] = $row['batch_label'];
                if($row['padi'] != $temp['padi']){
                    if(in_array($row['padi'], $temp2[$idx][$row['jenis_gabah']]) == false){
                        $data['detail'][$idx]['berat_bruto'] += $row['berat_bruto'];
                        $data['detail'][$idx]['berat_netto'] += $row['berat_beras_netto'];
                        array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
                    }
                }else{
                    if(in_array($row['padi'], $temp2[$idx][$row['jenis_gabah']]) == false){
                        if(!isset($data['detail'][$idx]['berat_bruto'])){
                            $data['detail'][$idx]['berat_bruto'] = 0;
                        }
                        if(!isset($data['detail'][$idx]['berat_netto'])){
                            $data['detail'][$idx]['berat_netto'] = 0;
                        }
                        
                        $data['detail'][$idx]['berat_bruto'] += $row['berat_bruto'];
                        $data['detail'][$idx]['berat_netto'] += $row['berat_beras_netto'];
                        array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
                    }
                }
                if($key == 0){
                    $data['detail'][$idx]['jumlah_sak'] = $row['jumlah_sak'];
                }else{
                    $data['detail'][$idx]['jumlah_sak'] += $row['jumlah_sak'];
                }
                $data['detail'][$idx]['harga_nol_gabuk'] = $row['harga_nol_gabuk'];
                $data['detail'][$idx]['harga_kg'] = $row['harga_kg'];
            }else{
                
                $temp['batch'] = $row['batch'];
                $temp['jenis_gabah'] = $row['jenis_gabah'];
                $temp['padi'] = $row['padi'];
                $idx++;
                $data['detail'][$idx]['batch'] = $row['batch'];
                $data['detail'][$idx]['jenis_gabah'] = $row['jenis_gabah'];
                $data['detail'][$idx]['batch_label'] = $row['batch_label'];
                $data['detail'][$idx]['berat_bruto'] = $row['berat_bruto'];
                $data['detail'][$idx]['berat_netto'] = $row['berat_beras_netto'];
                $data['detail'][$idx]['jumlah_sak'] = $row['jumlah_sak'];
                $data['detail'][$idx]['harga_nol_gabuk'] = $row['harga_nol_gabuk'];
                $data['detail'][$idx]['harga_kg'] = $row['harga_kg'];
                $temp2[$idx][$row['jenis_gabah']] = array();
                if($row['berat_beras_netto'] != 0){
                    array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
                }
            }
        }
        // dump($data['detail']);
        $html = $this->load->view('modules/pembelian/gabah/v-nota', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("PT. BAS - Invoice Pembelian Gabah");
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);

        $mpdf->Output();
    }
}