<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Persetujuan extends MY_Controller_admin
{
	public $bypass_auth = true;

	public function __construct()
	{
		parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('customer_model', 'customer');
		$this->load->model('user_model', 'user');
		$this->load->model('kas_model', 'kas');
		$this->load->model('rekening_model', 'rekening');
		$this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
	}

	public function index()
	{
		show_404();
    }
    
    public function pembelian_gabah()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-pbl-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
			$data['pembelian'] = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_persetujuan(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');

            $data['pic'] = $this->penanggung_jawab->get_active();
			// dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan - Pembelian Gabah');
            set_activemenu('sub-persetujuan', 'menu-persetujuan-pembelian-gabah');
            $this->render('persetujuan/v-persetujuan-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
	}

	public function pembelian_gabah_submit_approval(){
		$post = $this->input->post();
		// dump($post);
		if(!empty($post['id'])){
			$update = array(
				'is_tempo' => $post['is_tempo'],
				'tgl_tempo' => $post['tgl_tempo'],
				'jenis_pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $post['id_kas'],
				'id_rek_pengirim' => $post['id_rek_pengirim'],
				'id_rek_penerima' => $post['id_rek_penerima'],
				'bayar_dp' => $post['bayar_dp'],
				'dibayar_oleh' => $post['dibayar'],
				'checker' => $post['checker'],
				'tgl_bayar' => $post['tgl_bayar'],
				'status_persetujuan' => 1,
				'verifikasi_oleh' => get_session('auth')['id'],
				'timestamp_verifikasi' => setNewDateTime(),
			);

			$result = $this->pembelian_gabah->update($post['id'], $update);
			
			if($result){
				$pembelian = $this->pembelian_gabah->get_data($post['id']);
				if($post['is_tempo'] == 1){
					# Jika bayar tempo
					if($post['bayar_dp'] > 0){
						$arus_kas['id_perusahaan'] = $this->id_perusahaan;
						if($pembelian['jenis_pembayaran'] == 'cash'){
							$arus_kas['id_kas'] = $pembelian['id_kas'];
						}else if($pembelian['jenis_pembayaran'] == 'transfer'){
							$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
						}
						$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
						$arus_kas['arus'] = "out";
						$arus_kas['sumber'] = "pembelian-gabah";
						$arus_kas['id_sumber'] = $post['id'];
						$arus_kas['keterangan'] = 'DP Pembelian Gabah - '.$pembelian['kode_pembelian'];
						$arus_kas['deskripsi'] = 'Bayar Tempo';
						$arus_kas['nominal'] = str_replace(",", "", $post['bayar_dp']);
						$arus_kas['input_by'] = get_session('auth')['id'];
						$arus_kas['input_timestamp'] = setNewDateTime();
						$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
						$result = json_decode(xcurl($param_curl)['response']);
					}
				}else{
					# Jika bayar lunas
					$arus_kas['id_perusahaan'] = $this->id_perusahaan;
					if($pembelian['jenis_pembayaran'] == 'cash'){
						$arus_kas['id_kas'] = $pembelian['id_kas'];
					}else if($pembelian['jenis_pembayaran'] == 'transfer'){
						$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
					}
					$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
					$arus_kas['arus'] = "out";
					$arus_kas['sumber'] = "pembelian-gabah";
					$arus_kas['id_sumber'] = $post['id'];
					$arus_kas['keterangan'] = $pembelian['kode_pembelian'];
					$arus_kas['deskripsi'] = 'Bayar Lunas';
					$arus_kas['nominal'] = str_replace(",", "", $post['pembelian_grandtotal']);
					$arus_kas['input_by'] = get_session('auth')['id'];
					$arus_kas['input_timestamp'] = setNewDateTime();
					$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
					$result = json_decode(xcurl($param_curl)['response']);
				}
			}
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('persetujuan/pembelian_gabah?'.$post['url']);
	}

	public function pembelian_gabah_tolak(){
		$id = $this->input->post('id');
		if(!empty($id)){
			$update = array(
				'status_persetujuan' => 2,
				'verifikasi_oleh' => get_session('auth')['id'],
				'timestamp_verifikasi' => setNewDateTime()
			);

			$result = $this->pembelian_gabah->update($id, $update);
		}else{
			$result = false;
		}
		echo json_encode($result);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->penanggung_jawab->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->penanggung_jawab->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('pic');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
