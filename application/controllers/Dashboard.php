<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller_admin
{
	public $class_id = 'dsb';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// dump($this->session->auth);
		set_session('breadcrumb', array('Dashboard' => 'active'));
		set_activemenu('', 'menu-dashboard');
		// dump($this->session->auth["token"]);
		$this->render('dashboard/index');
	}

	public function tes()
	{
		$link = parse_url('https://berkatcorp.com/beras');
		echo $link['host'];
	}

}
