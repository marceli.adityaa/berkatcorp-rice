<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Panduan extends MY_Controller
{
	// public $class_id = 'pnd';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// dump($this->session->auth);
		// set_session('breadcrumb', array('Dashboard' => 'active'));
		// set_activemenu('', 'menu-dashboard');
		// dump($this->session->auth["token"]);
		$this->render('panduan/v_panduan');
	}
}