<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Pengeringan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('stok_gabah_model', 'stok_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('pengeringan_model', 'pengeringan');
        $this->load->model('pengeringan_detail_model', 'pengeringan_detail');
        $this->load->model('master_dryer_model', 'master_dryer');
        $this->load->model('master_jemur_model', 'master_jemur');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-kll', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Pengeringan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->pengeringan->get_data(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pengeringan' => 'active', 'Kelola Pengeringan' => 'active'));
            set_session('title', 'Kelola Pengeringan');
            set_activemenu('sub-pengeringan', 'menu-kelola-pengeringan');
            $this->render('produksi/pengeringan/v-kelola-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-kll', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->pengeringan->get_data($id)[0];
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Pengeringan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['detail_tugas'] = $this->pengeringan_detail->get_data_detail($data['detail']['id']);
            $data['gabah'] = $this->stok_gabah->get_stok_pengeringan($data['detail']['jenis_pengeringan']);
            $waiting = $this->pengeringan_detail->get_data_detail_waiting_exclude($data['detail']['id']);

            $data['list_dryer'] = $this->master_dryer->get_active();
            $data['list_jemur'] = $this->master_jemur->get_active();
            foreach($data['list_dryer'] as $row){
                $data['lokasi_dryer'][$row['id']] = $row;
            }
            foreach($data['list_jemur'] as $row){
                $data['lokasi_jemur'][$row['id']] = $row;
            }
            # Init data gabah yg bisa dipakai
            if(!empty($data['detail_tugas'])){
                foreach($data['detail_tugas'] as $dt){
                    foreach($data['gabah'] as $key => $gb){
                        if($gb['id'] == $dt['id_stok']){
                            $data['gabah'][$key]['stok_sisa'] -= $dt['kuantitas'];
                        break;
                        }
                    }
                }
            }

            if(!empty($waiting)){
                foreach($waiting as $w){
                    foreach($data['gabah'] as $key => $gb){
                        if($gb['id'] == $w['id_stok']){
                            $data['gabah'][$key]['stok_sisa'] -= $w['kuantitas'];
                        break;
                        }
                    }
                }
            }

            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Kelola Pengeringan' => base_url('produksi/pengeringan/kelola'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Pengeringan ');
            set_activemenu('sub-pengeringan', 'menu-kelola-pengeringan');
            $this->render('produksi/pengeringan/v-detail-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function hasil()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-res', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(2 => "Proses Pengeringan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->pengeringan->get_data_hasil_pengeringan(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pengeringan' => 'active', 'Hasil Pengeringan' => 'active'));
            set_session('title', 'Hasil Pengeringan');
            set_activemenu('sub-pengeringan', 'menu-hasil-pengeringan');
            $this->render('produksi/pengeringan/v-hasil-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_hasil($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-res', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->pengeringan->get_data($id)[0];
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Pengeringan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['detail_tugas'] = $this->pengeringan_detail->get_data_detail($data['detail']['id']);
            $data['gabah'] = $this->stok_gabah->get_stok_pengeringan($data['detail']['jenis_pengeringan']);
            $data['list_dryer'] = $this->master_dryer->get_active();
            $data['list_jemur'] = $this->master_jemur->get_active();
            foreach($data['list_dryer'] as $row){
                $data['lokasi_dryer'][$row['id']] = $row;
            }
            foreach($data['list_jemur'] as $row){
                $data['lokasi_jemur'][$row['id']] = $row;
            }
           
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Hasil Pengeringan' => base_url('produksi/pengeringan/hasil'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Pengeringan ');
            set_activemenu('sub-pengeringan', 'menu-hasil-pengeringan');
            $this->render('produksi/pengeringan/v-detail-hasil-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(3 => "Menunggu Persetujuan", 4 => "Disetujui", 5 => "Ditolak");
            $data['tugas'] = $this->user->join($this->pengeringan->get_data_persetujuan_pengeringan(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pengeringan' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Pengeringan');
            set_activemenu('sub-pengeringan', 'menu-persetujuan-pengeringan');
            $this->render('produksi/pengeringan/v-persetujuan-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_pengeringan($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-acc', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->pengeringan->get_data($id)[0];
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Pengeringan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['detail_tugas'] = $this->pengeringan_detail->get_data_detail($data['detail']['id']);
            $data['gabah'] = $this->stok_gabah->get_stok_pengeringan($data['detail']['jenis_pengeringan']);
            $data['list_dryer'] = $this->master_dryer->get_active();
            $data['list_jemur'] = $this->master_jemur->get_active();
            foreach($data['list_dryer'] as $row){
                $data['lokasi_dryer'][$row['id']] = $row;
            }
            foreach($data['list_jemur'] as $row){
                $data['lokasi_jemur'][$row['id']] = $row;
            }
           
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Persetujuan Pengeringan' => base_url('produksi/pengeringan/persetujuan'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Pengeringan ');
            set_activemenu('sub-pengeringan', 'menu-hasil-pengeringan');
            $this->render('produksi/pengeringan/v-detail-hasil-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_hasil_pengeringan($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-acc', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->pengeringan->get_data($id)[0];
            $data['padi'] = $this->jenis_padi->get_active();
            $data['detail_tugas'] = $this->pengeringan_detail->get_data_detail($data['detail']['id']);
            $data['gabah'] = $this->stok_gabah->get_stok_pengeringan($data['detail']['jenis_pengeringan']);
            $data['list_dryer'] = $this->master_dryer->get_active();
            $data['list_jemur'] = $this->master_jemur->get_active();
            foreach($data['list_dryer'] as $row){
                $data['lokasi_dryer'][$row['id']] = $row;
            }
            foreach($data['list_jemur'] as $row){
                $data['lokasi_jemur'][$row['id']] = $row;
            }
           
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Persetujuan Pengeringan' => base_url('produksi/pengeringan/persetujuan'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Pengeringan ');
            set_activemenu('sub-pengeringan', 'menu-persetujuan-pengeringan');
            $this->render('produksi/pengeringan/v-detail-hasil-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('dry-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['padi'] = $this->jenis_padi->get_active();
            $data['pengeringan'] = array("dryer", "jemur");
            $data['tugas'] = $this->user->join($this->pengeringan->get_data_rekap_pengeringan(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pengeringan' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Rekap Pengeringan');
            set_activemenu('sub-pengeringan', 'menu-rekap-pengeringan');
            $this->render('produksi/pengeringan/v-rekap-pengeringan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }


    public function submit_tambah_tugas()
    {
        $post = $this->input->post();
        $post['kode'] = $this->generate_kode_produksi($post);
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->pengeringan->insert($post);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/pengeringan/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->pengeringan->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/pengeringan/kelola?'.$url);
        }

    }

    public function submit_hasil_pengeringan()
    {
        $post = $this->input->post();
        $post['persetujuan'] = 1;
    
        $url = $post['url'];
        unset($post['url']);
       
        # Update statement
        $id = $post['id'];
        unset($post['id']);
        $result = $this->pengeringan->update($id, $post);
        if ($result) {
            $this->message('Berhasil mengubah data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }

        $this->go('produksi/pengeringan/hasil?'.$url);
    }

    public function generate_kode_produksi($param)
    {
        $kode = "";
        $idx = $this->pengeringan->get_data_generate_kode($param['tanggal_mulai']);
        if($param['jenis_pengeringan'] == 'dryer'){
            $kode = "DRY-".date('Ymd', strtotime($param['tanggal_mulai']))."-".zerofy($idx, 3);
        }else{
            $kode = "JMR-".date('Ymd', strtotime($param['tanggal_mulai']))."-".zerofy($idx, 3);
        }

        return $kode;
    }

    public function export(){
        $get                = $this->input->get();
        $data['transaksi']  = $this->user->join($this->pengeringan->get_data_rekap_pengeringan(), 'submit_by', 'input_by');
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        $total_gabah = 0;
        $total_kering = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Hasil Pengeringan');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        $nilai_sisa = 0;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Mulai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Selesai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pengeringan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Padi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Gabah KS');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Gabah KS');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Gabah KG');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Gabah KG');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Susut (%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Petugas');
        $baris++;
                
        if(!empty($data['transaksi'])){
            foreach($data['transaksi'] as $d){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kode']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_mulai']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_selesai']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jenis_pengeringan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['padi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hpp_beli']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['berat_kering']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hpp_kering']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, round((($d['kuantitas'] - $d['berat_kering'])/$d['kuantitas'] * 100), 1));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['petugas']);
                $baris++;
                $total_gabah += $d['kuantitas'];
                $total_kering += $d['berat_kering'];
            }
        }

        # Set subtotal
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Gabah Dikeringkan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_gabah));

        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Hasil Pengeringan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_kering));

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;

        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_pengeringan.xlsx"');
		$writer->save("php://output");
    }

   
}
