<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Beras extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->load->model('jenis_bahan_model', 'jenis_bahan');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('produksi_model', 'produksi');
        $this->load->model('produksi_proses_model', 'produksi_proses');
        $this->load->model('produksi_bahan_model', 'produksi_bahan');
        $this->load->model('produksi_hasil_model', 'produksi_hasil');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-kll', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['merk'] = $this->merk_beras->get_active();
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Produksi", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->produksi->get_data(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => 'active', 'Kelola' => 'active'));
            set_session('title', 'Kelola Produksi');
            set_activemenu('sub-produksi', 'menu-kelola-produksi');
            $this->render('produksi/beras/v-kelola-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-kll', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['tugas'] = $this->produksi->get($id);
            $data['detail_bahan'] = $this->produksi_bahan->get_data_produksi($id);
            $data['detail_sisa'] = $this->produksi_bahan->get_data_sisa($id);
            $data['detail_hasil'] = $this->produksi_hasil->get_data_hasil($id);
            
            $data['kuantitas_hasil'] = 0;
            if(!empty($data['detail_hasil'])){
                foreach($data['detail_hasil'] as $row){
                    $data['kuantitas_hasil'] += ($row['kuantitas'] * $row['kemasan']);
                }
            }
            // dump($data['kuantitas_hasil']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => base_url('produksi/beras/kelola'), 'Detail' => 'active', $data['tugas']->kode => 'active', ucwords($data['tugas']->merk) => 'active'));
            set_session('title', 'Detail Produksi ');
            set_activemenu('sub-produksi', 'menu-kelola-produksi');
            $this->render('produksi/beras/v-detail-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;

            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['merk'] = $this->merk_beras->get_active();
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses Produksi", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->produksi->get_data(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Produksi');
            set_activemenu('sub-produksi', 'menu-persetujuan-produksi');
            $this->render('produksi/beras/v-persetujuan-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_hasil($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-acc', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['tugas'] = $this->produksi->get($id);
            $data['detail_bahan'] = $this->produksi_bahan->get_data_produksi($id);
            $data['detail_sisa'] = $this->produksi_bahan->get_data_sisa($id);
            $data['detail_hasil'] = $this->produksi_hasil->get_data_hasil($id);
            
            $data['kuantitas_hasil'] = 0;
            if(!empty($data['detail_hasil'])){
                foreach($data['detail_hasil'] as $row){
                    $data['kuantitas_hasil'] += ($row['kuantitas'] * $row['kemasan']);
                }
            }
            // dump($data['kuantitas_hasil']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => base_url('produksi/beras/persetujuan'), 'Detail' => 'active', $data['tugas']->kode => 'active', ucwords($data['tugas']->merk) => 'active'));
            set_session('title', 'Detail Produksi ');
            set_activemenu('sub-produksi', 'menu-persetujuan-produksi');
            $this->render('produksi/beras/v-detail-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_produksi($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['tugas'] = $this->produksi->get($id);
            $data['detail_bahan'] = $this->produksi_bahan->get_data_produksi($id);
            $data['detail_sisa'] = $this->produksi_bahan->get_data_sisa($id);
            $data['detail_hasil'] = $this->produksi_hasil->get_data_hasil($id);
            
            $data['kuantitas_hasil'] = 0;
            if(!empty($data['detail_hasil'])){
                foreach($data['detail_hasil'] as $row){
                    $data['kuantitas_hasil'] += ($row['kuantitas'] * $row['kemasan']);
                }
            }
            // dump($data['kuantitas_hasil']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => base_url('produksi/beras/rekap'), 'Detail' => 'active', $data['tugas']->kode => 'active', ucwords($data['tugas']->merk) => 'active'));
            set_session('title', 'Detail Produksi ');
            set_activemenu('sub-produksi', 'menu-persetujuan-produksi');
            $this->render('produksi/beras/v-detail-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('prd-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['proses'] = $this->produksi_proses->get_active();
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['merk'] = $this->merk_beras->get_active();
            $data['tugas'] = $this->user->join($this->produksi->get_data_rekap(), 'submit_by', 'input_by');
            $data['hasil'] = array();
            $hasil = $this->produksi_hasil->get_data_rekap_hasil();
            if(!empty($hasil)){
                foreach($hasil as $row){
                    $data['hasil'][$row['id']] = $row;
                }
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Produksi' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Rekap Produksi');
            set_activemenu('sub-produksi', 'menu-rekap-produksi');
            $this->render('produksi/beras/v-rekap-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }


    public function submit_tambah_tugas()
    {
        $post = $this->input->post();
        $post['kode'] = $this->generate_kode_produksi($post);
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->produksi->insert($post);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/beras/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->produksi->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/beras/kelola?'.$url);
        }
    }

    public function submit_data_bahan()
    {
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp'] = setNewDateTime();
        $result = $this->produksi_bahan->insert($post);
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }

        $this->go('produksi/beras/detail/'.$post['id_produksi']);
    }

    public function submit_data_hasil(){
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp'] = setNewDateTime();
        $result = $this->produksi_hasil->insert($post);
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }

        $this->go('produksi/beras/detail/'.$post['id_produksi']);
    }

    public function approve_transaksi(){
        $post = $this->input->post();
        $tugas = $this->penggilingan->get_data($post['id'])[0];
        $detail = $this->penggilingan_detail->get_data_detail($post['id']);
        $bahan = array(
            'hasil_beras' => 1,
            'hasil_menir' => 2,
            'hasil_sekam' => 3, 
            'hasil_katul' => 4
        );
        
        # Ubah Stok Gabah
        ## Kurangi Stok
        $stok_berkurang = array();
        foreach($detail as $row){
            $list = array(
                'id_stok' => $row['id_stok'],
                'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                'arus' => 'out',
                'kuantitas' => $row['kuantitas'],
                'harga' => $row['harga_satuan'],
                'sumber' => 'hasil-giling',
                'id_sumber' => $post['id'],
                'timestamp' => setNewDateTime()
            );
            $currentStok = $this->stok_gabah->get_data($row['id_stok']);
            $this->stok_gabah->update($row['id_stok'], array('stok_sisa' => ($currentStok['stok_sisa'] - $row['kuantitas'])));
            array_push($stok_berkurang, $list);
        }
        # catat log transaksi stok
        $this->stok_gabah_trx->batch_insert($stok_berkurang);

        ## Tambah Stok Bahan
        foreach($bahan as $key => $val){
            if(!empty($tugas[$key]) && $tugas[$key] > 0){
                if($key == 'hasil_beras'){
                    # Jika beras medium
                    $stok_bertambah = array(
                        'tanggal' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'id_jenis_bahan' => $val,
                        'stok' => $tugas[$key],
                        'stok_sisa' => $tugas[$key],
                        'harga_satuan' => $post['harga_medium'],
                        'broken' => $post['broken'],
                        'harga_kepala' => $post['harga_kepala'],
                        'harga_broken' => $post['harga_broken'],
                        'input_by' => $tugas['input_by'],
                        'timestamp_input' => setNewDateTime()
                    );
                    $this->stok_bahan->insert($stok_bertambah);
        
                    # catat log transaksi stok
                    $log = array(
                        'id_stok' => $this->db->insert_id(),
                        'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'arus' => 'in',
                        'kuantitas' => $tugas[$key],
                        'harga' => $post['harga_medium'],
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'timestamp' => setNewDateTime()
                    );
                    $this->stok_bahan_trx->insert($log);
                }else{
                    # Selain beras medium
                    $stok_bertambah = array(
                        'tanggal' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'id_jenis_bahan' => $val,
                        'stok' => $tugas[$key],
                        'stok_sisa' => $tugas[$key],
                        'harga_satuan' => 0,
                        'broken' => 0,
                        'harga_kepala' => 0,
                        'harga_broken' => 0,
                        'input_by' => $tugas['input_by'],
                        'timestamp_input' => setNewDateTime()
                    );
                    $this->stok_bahan->insert($stok_bertambah);
        
                    # catat log transaksi stok
                    $log = array(
                        'id_stok' => $this->db->insert_id(),
                        'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'arus' => 'in',
                        'kuantitas' => $tugas[$key],
                        'harga' => 0,
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'timestamp' => setNewDateTime()
                    );
                    $this->stok_bahan_trx->insert($log);
                }
            }
        }

        # Update status pengeringan
        $update = array(
            'broken' => $post['broken'],
            'harga_medium' => $post['harga_medium'],
            'harga_kepala' => $post['harga_kepala'],
            'harga_broken' => $post['harga_broken'],
            'persetujuan' => 2,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->penggilingan->update($post['id'], $update);
        if($result){
            $this->message('Berhasil menyetujui transaksi.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        $this->go('produksi/penggilingan/persetujuan?'.$post['url']);
    }

    public function generate_kode_produksi($param)
    {
        $kode = "";
        $idx = $this->produksi->get_data_generate_kode($param['tanggal']);
        $kode = "PRD-".date('Ymd', strtotime($param['tanggal']))."-".zerofy($idx, 3);

        return $kode;
    }

    public function export(){
        $get                    = $this->input->get();
        $data['rekap_trx']      = $this->user->join($this->produksi->get_data_rekap(), 'submit_by', 'input_by');
        $data['detail_trx']     = $this->produksi_hasil->get_data_rekap_detail();
        $data['hasil'] = array();
        $hasil = $this->produksi_hasil->get_data_rekap_hasil();
        if(!empty($hasil)){
            foreach($hasil as $row){
                $data['hasil'][$row['id']] = $row;
            }
        }
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        $total_bahan = 0;
        $total_produksi = 0;
        $total_sisa = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Produksi Beras');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Merk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Cor');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kuantitas Naik');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Hasil Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Susut (%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input By');
        $baris++;
        if(!empty($data['rekap_trx'])){
            foreach($data['rekap_trx'] as $d){
                $kolom = 1;
                $susut = ($d['total_kuantitas'] - ($data['hasil'][$d['id']]['tonase_hasil'] + $d['sisa_produksi'])) / $d['total_kuantitas']*100;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kode']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['merk']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jumlah_cor']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['total_kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['sisa_produksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $data['hasil'][$d['id']]['tonase_hasil']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, round($susut,2));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['keterangan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['submit_by']);
                $baris++;
                $total_bahan += $d['total_kuantitas'];
                $total_produksi += $data['hasil'][$d['id']]['tonase_hasil'];
                $total_sisa += $d['sisa_produksi'];
            }
        }

        # Set subtotal
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Bahan Dipakai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_bahan));

        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Hasil Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_produksi));

        $avg_susut = ($total_bahan - ($total_produksi + $total_sisa)) / $total_bahan * 100;
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Rata-Rata Susut (%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, round($avg_susut, 2));

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;

        # New Sheet
        $worksheet = new Worksheet($spreadsheet, 'Detail Transaksi');
        $spreadsheet->addSheet($worksheet, $idx);
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Merk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kemasan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kuantitas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tonase (Kg)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Biaya Sak');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Biaya Transport');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nilai Produksi');
        $baris++;
        
        if(!empty($data['detail_trx'])){
            foreach($data['detail_trx'] as $dt){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, date("Y-m-d", strtotime($dt['tanggal'])));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kode']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($dt['merk']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kemasan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kemasan']*$dt['kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['biaya_sak']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['biaya_transport']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['harga_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($dt['kemasan']*$dt['kuantitas']*$dt['harga_pokok']));
                $baris++;
            }
        }

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_produksi_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }

   
}
