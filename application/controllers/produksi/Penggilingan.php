<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Penggilingan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('stok_gabah_model', 'stok_gabah');
        $this->load->model('stok_gabah_trx_model', 'stok_gabah_trx');
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->load->model('jenis_bahan_model', 'jenis_bahan');
        $this->load->model('penggilingan_model', 'penggilingan');
        $this->load->model('penggilingan_detail_model', 'penggilingan_detail');
        $this->load->model('master_dryer_model', 'master_dryer');
        $this->load->model('master_jemur_model', 'master_jemur');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-kll', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['bahan'] = $this->jenis_bahan->get_active();
            // $data['penggilingan'] = array("dryer", "jemur");
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses penggilingan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->penggilingan->get_data(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penggilingan' => 'active', 'Kelola Penggilingan' => 'active'));
            set_session('title', 'Kelola Penggilingan');
            set_activemenu('sub-penggilingan', 'menu-kelola-penggilingan');
            $this->render('produksi/penggilingan/v-kelola-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-kll', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->penggilingan->get_data($id)[0];
            $data['gabah'] = $this->stok_gabah->get_bahan_gilingan();
            $data['status'] = array(1 => "Menunggu Input Data", 2 => "Proses penggilingan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['detail_tugas'] = $this->penggilingan_detail->get_data_detail($data['detail']['id']);
            // dump($data);
            # Init data gabah yg bisa dipakai
            if(!empty($data['detail_tugas'])){
                foreach($data['detail_tugas'] as $dt){
                    foreach($data['gabah'] as $key => $gb){
                        if($gb['id'] == $dt['id_stok']){
                            $data['gabah'][$key]['stok_sisa'] -= $dt['kuantitas'];
                        break;
                        }
                    }
                }
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Kelola Penggilingan' => base_url('produksi/penggilingan/kelola'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Penggilingan ');
            set_activemenu('sub-penggilingan', 'menu-kelola-penggilingan');
            $this->render('produksi/penggilingan/v-detail-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function hasil()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-res', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['gabah'] = $this->stok_gabah->get_bahan_gilingan();
            $data['status'] = array(2 => "Proses penggilingan", 3 => "Menunggu Persetujuan", 4 => "Selesai");
            $data['tugas'] = $this->user->join($this->penggilingan->get_data_hasil_penggilingan(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'penggilingan' => 'active', 'Hasil Penggilingan' => 'active'));
            set_session('title', 'Hasil Penggilingan');
            set_activemenu('sub-penggilingan', 'menu-hasil-penggilingan');
            $this->render('produksi/penggilingan/v-hasil-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_hasil($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-res', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->penggilingan->get_data($id)[0];
            $data['gabah'] = $this->stok_gabah->get_bahan_gilingan();
            $data['detail_tugas'] = $this->penggilingan_detail->get_data_detail($data['detail']['id']);
            $data['admin'] = false;
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Hasil Penggilingan' => base_url('produksi/penggilingan/hasil'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Penggilingan ');
            set_activemenu('sub-penggilingan', 'menu-hasil-penggilingan');
            $this->render('produksi/penggilingan/v-detail-hasil-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['status'] = array(3 => "Menunggu Persetujuan", 4 => "Disetujui", 5 => "Ditolak");
            $data['tugas'] = $this->user->join($this->penggilingan->get_data_persetujuan_penggilingan(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'penggilingan' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan penggilingan');
            set_activemenu('sub-penggilingan', 'menu-persetujuan-penggilingan');
            $this->render('produksi/penggilingan/v-persetujuan-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_hasil_penggilingan($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-res', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['detail'] = $this->penggilingan->get_data($id)[0];
            $data['gabah'] = $this->stok_gabah->get_bahan_gilingan();
            $data['detail_tugas'] = $this->penggilingan_detail->get_data_detail($data['detail']['id']);
            $data['admin'] = true;
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Persetujuan Penggilingan' => base_url('produksi/penggilingan/persetujuan'), 'Detail Tugas' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Tugas Penggilingan ');
            set_activemenu('sub-penggilingan', 'menu-persetujuan-penggilingan');
            $this->render('produksi/penggilingan/v-detail-hasil-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('grnd-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['penggilingan'] = array("dryer", "jemur");
            $data['tugas'] = $this->user->join($this->penggilingan->get_data_rekap_penggilingan(), 'submit_by', 'input_by');
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penggilingan' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Rekap Penggilingan');
            set_activemenu('sub-penggilingan', 'menu-rekap-penggilingan');
            $this->render('produksi/penggilingan/v-rekap-penggilingan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }


    public function submit_tambah_tugas()
    {
        $post = $this->input->post();
        $post['kode'] = $this->generate_kode_produksi($post);
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penggilingan->insert($post);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/penggilingan/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penggilingan->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('produksi/penggilingan/kelola?'.$url);
        }
    }

    public function submit_hasil_penggilingan()
    {
        $post = $this->input->post();
        $post['persetujuan'] = 1;
    
        $url = $post['url'];
        unset($post['url']);
       
        # Update statement
        $id = $post['id'];
        unset($post['id']);
        $result = $this->penggilingan->update($id, $post);
        if ($result) {
            $this->message('Berhasil mengubah data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }

        $this->go('produksi/penggilingan/hasil?'.$url);
    }

    public function approve_transaksi(){
        $post = $this->input->post();
        $tugas = $this->penggilingan->get_data($post['id'])[0];
        $detail = $this->penggilingan_detail->get_data_detail($post['id']);
        $bahan = array(
            'hasil_beras' => 1,
            'hasil_menir' => 2,
            'hasil_sekam' => 3, 
            'hasil_katul' => 4
        );
        
        # Ubah Stok Gabah
        ## Kurangi Stok
        $stok_berkurang = array();
        foreach($detail as $row){
            $list = array(
                'id_stok' => $row['id_stok'],
                'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                'arus' => 'out',
                'kuantitas' => $row['kuantitas'],
                'harga' => $row['harga_satuan'],
                'sumber' => 'hasil-giling',
                'id_sumber' => $post['id'],
                'timestamp' => setNewDateTime()
            );
            $currentStok = $this->stok_gabah->get_data($row['id_stok']);
            $this->stok_gabah->update($row['id_stok'], array('stok_sisa' => ($currentStok['stok_sisa'] - $row['kuantitas'])));
            array_push($stok_berkurang, $list);
        }
        # catat log transaksi stok
        $this->stok_gabah_trx->batch_insert($stok_berkurang);

        ## Tambah Stok Bahan
        foreach($bahan as $key => $val){
            if(!empty($tugas[$key]) && $tugas[$key] > 0){
                if($key == 'hasil_beras'){
                    # Jika beras medium
                    $stok_bertambah = array(
                        'tanggal' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'id_jenis_bahan' => $val,
                        'stok' => $tugas[$key],
                        'stok_sisa' => $tugas[$key],
                        'harga_satuan' => $post['harga_medium'],
                        'broken' => $post['broken'],
                        'harga_kepala' => $post['harga_kepala'],
                        'harga_broken' => $post['harga_broken'],
                        'input_by' => $tugas['input_by'],
                        'timestamp_input' => setNewDateTime()
                    );
                    $this->stok_bahan->insert($stok_bertambah);
        
                    # catat log transaksi stok
                    $log = array(
                        'id_stok' => $this->db->insert_id(),
                        'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'arus' => 'in',
                        'kuantitas' => $tugas[$key],
                        'harga' => $post['harga_medium'],
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'timestamp' => setNewDateTime()
                    );
                    $this->stok_bahan_trx->insert($log);
                }else{
                    # Selain beras medium
                    $stok_bertambah = array(
                        'tanggal' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'id_jenis_bahan' => $val,
                        'stok' => $tugas[$key],
                        'stok_sisa' => $tugas[$key],
                        'harga_satuan' => 0,
                        'broken' => 0,
                        'harga_kepala' => 0,
                        'harga_broken' => 0,
                        'input_by' => $tugas['input_by'],
                        'timestamp_input' => setNewDateTime()
                    );
                    $this->stok_bahan->insert($stok_bertambah);
        
                    # catat log transaksi stok
                    $log = array(
                        'id_stok' => $this->db->insert_id(),
                        'tgl' => date("Y-m-d", strtotime($tugas['tanggal_selesai'])),
                        'arus' => 'in',
                        'kuantitas' => $tugas[$key],
                        'harga' => 0,
                        'sumber' => 'hasil-giling',
                        'id_sumber' => $post['id'],
                        'timestamp' => setNewDateTime()
                    );
                    $this->stok_bahan_trx->insert($log);
                }
            }
        }

        # Update status pengeringan
        $update = array(
            'broken' => $post['broken'],
            'harga_medium' => $post['harga_medium'],
            'harga_kepala' => $post['harga_kepala'],
            'harga_broken' => $post['harga_broken'],
            'persetujuan' => 2,
            'verif_by' => get_session('auth')['id'],
            'timestamp_verif' => setNewDateTime()
        );

        $result = $this->penggilingan->update($post['id'], $update);
        if($result){
            $this->message('Berhasil menyetujui transaksi.', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        $this->go('produksi/penggilingan/persetujuan?'.$post['url']);
    }

    public function generate_kode_produksi($param)
    {
        $kode = "";
        $idx = $this->penggilingan->get_data_generate_kode($param['tanggal_mulai']);
        $kode = "GIL-".date('Ymd', strtotime($param['tanggal_mulai']))."-".zerofy($idx, 3);

        return $kode;
    }

    public function export(){
        $get                = $this->input->get();
        $data['transaksi']  = $this->user->join($this->penggilingan->get_data_rekap_penggilingan(), 'submit_by', 'input_by');
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        $total_gabah = 0;
        $total_giling = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Hasil Penggilingan');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        $nilai_sisa = 0;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Produksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Mulai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Selesai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Gabah KG');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Beras Medium');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Medium');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Menir');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Katul');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sekam');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Susut');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Broken (%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Rendemen KG(%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Kepala');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Kepala');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Broken');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Broken');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Petugas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $baris++;
                
        if(!empty($data['transaksi'])){
            foreach($data['transaksi'] as $d){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kode']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_mulai']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_selesai']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hasil_beras']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['harga_medium']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hasil_menir']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hasil_katul']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hasil_sekam']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, round(($d['kuantitas'] - $d['hasil_beras'] - $d['hasil_menir'] - $d['hasil_katul'] - $d['hasil_sekam']), 1));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['broken']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, round(($d['hasil_beras']/$d['kuantitas'] * 100), 1));
                if($d['harga_kepala'] > 0){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, round((100 - $d['broken']) / 100 * $d['hasil_beras'], 1));
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['harga_kepala']);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, round((100 - $d['broken']) / 100 * $d['hasil_beras'], 1)); 
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);

                }
                if($d['harga_broken'] > 0){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, round(($d['broken']) / 100 * $d['hasil_beras'], 1));
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['harga_broken']);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, round(($d['broken']) / 100 * $d['hasil_beras'], 1));
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hasil_beras']);
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['petugas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['keterangan']);
                $baris++;
                $total_gabah += $d['kuantitas'];
                $total_giling += $d['hasil_beras'];
            }
        }

        # Set subtotal
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Gabah Digiling');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_gabah));

        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Hasil Penggilingan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_giling));

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;

        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_penggilingan.xlsx"');
		$writer->save("php://output");
    }

   
}
