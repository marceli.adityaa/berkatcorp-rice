<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_produksi extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('penjualan_produksi_model', 'penjualan_produksi');
        $this->load->model('penjualan_produksi_detail_model', 'penjualan_produksi_detail');
        // $this->load->model('penjualan_produksi_proses_model', 'penjualan_produksi_proses');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-op', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['area'] = $this->area->get_active();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Input data", 2 => "Selesai");
            $data['po'] = $this->user->join($this->penjualan_po->get_data_proses_po(), 'submit_by', 'input_by');
            $data['order'] = $this->user->join($this->penjualan_produksi->get_data_trx(), 'submit_by', 'input_by');

            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Order Produksi' => 'active'));
            set_session('title', 'Penjualan Beras - Order Produksi');
            set_activemenu('sub-penjualan', 'menu-pj-op');
            $this->render('penjualan/beras/v-order-produksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-op', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['detail'] = $this->penjualan_produksi->get($id);
            $data['list_po'] = $this->penjualan_po->get_data_proses_po();
            $data['exist'] = $this->penjualan_produksi_detail->get_data_po($id);
            $data['detail_po'] = $this->penjualan_produksi_detail->get_data_detail($id);
            $data['summary_po'] = $this->penjualan_produksi_detail->get_data_summary($id);
            $data['tonase'] = 0;
            
            # init list untuk PO
            if(!empty($data['list_po']) && !empty($data['exist'])){
                foreach($data['list_po'] as $key => $val){
                    foreach($data['exist'] as $row){
                        if($val['id'] == $row['id_po']){
                            unset($data['list_po'][$key]);
                        }
                    }
                }
            }
            
            if(!empty($data['detail_po'])){
                foreach($data['detail_po'] as $row){
                    $data['tonase'] += $row['tonase'];
                }
            }
            
           
            // dump($data);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Order Produksi' => base_url('penjualan/beras/order_produksi'), 'Detail Pemesanan' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Detail Order Produksi ');
            set_activemenu('sub-penjualan', 'menu-pj-op');
            $this->render('penjualan/beras/v-order-produksi-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_tambah_do()
    {
        $post = $this->input->post();
        $post['kode'] = $this->generate_kode($post['tanggal']);
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        // dump($post);
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_produksi->insert($post);
            if ($result) {
                $this->message('Berhasil membuat data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/order_produksi/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_produksi->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/order_produksi/index?'.$url);
        }
    }

    public function submit_tambah_po()
    {
        $post = $this->input->post();
        if(isset($post['check']) && !empty($post['check'])){
            $insert = array();
            foreach($post['check'] as $row){
                $baris = array(
                    'id_produksi' => $post['id_produksi'],
                    'id_po' => $row
                );
                array_push($insert, $baris);
                $this->penjualan_po->update($row, array('status_produksi' => 1));
            }
            $result = $this->penjualan_produksi_detail->batch_insert($insert);
            if($result){
                $this->message('Berhasil memasukkan data.', 'success');
            }else{
                $this->message('Gagal memasukkan data.', 'error');
            }
        }else{
            $this->message('No data', 'info');
        }
        $this->go('penjualan/beras/order_produksi/detail/'.$post['id_produksi']);
    }

    public function generate_kode($tgl)
    {
        $last = $this->penjualan_produksi->get_kode_order($tgl);
        $index = 1;
        // $last = 'PO-001/04/20 ';
        if ($last != false) {
            $temp = explode('-', $last);
            $temp2 = explode('/', $temp[1]);
            $index = intval($temp2[0]) + 1;
        }
        $kode = 'OP-' . zerofy($index, 3) . '/' . date("m", strtotime($tgl)) . '/' . date("y", strtotime($tgl));
        return $kode;
    }


}
