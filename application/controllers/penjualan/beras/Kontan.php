<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Kontan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_kontan_model', 'penjualan_po_kontan');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-kt', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Input Data", 2 => "Menunggu Persetujuan", 3 => 'Disetujui', 4 => "Ditolak");
            $data['transaksi'] = $this->user->join($this->penjualan_po->get_data_trx_kontan(), 'submit_by', 'input_by');
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Kontan' => 'active'));
            set_session('title', 'Penjualan Beras - Kontan');
            set_activemenu('sub-penjualan', 'menu-pj-kt');
            $this->render('penjualan/beras/v-penjualan-kontan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function transaksi($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-kt', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['detail'] = $this->penjualan_po->get($id);
            $data['transaksi'] = $this->user->join($this->penjualan_po_kontan->get_data_transaksi($id), 'submit_by', 'input_by');;
            $data['tonase'] = 0;
            $data['list_kemasan'] = array();
            
            // Init kemasan
            if(!empty($data['kemasan'])){
                foreach($data['kemasan'] as $row){
                    $data['list_kemasan'][$row['id']] = $row;
                }
                
            }
            // dump($data);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Kontan' => base_url('penjualan/beras/kontan'), 'Transaksi' => 'active'));
            set_session('title', 'Penjualan Beras - Transaksi Kontan');
            set_activemenu('sub-penjualan', 'menu-pj-kt');
            $this->render('penjualan/beras/v-penjualan-kontan-transaksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($trx)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-kt', $hak->bas) || $hak->bas[0] == '*') {
            
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['trx'] = $this->penjualan_po_kontan->get($trx);
            $data['detail'] = $this->penjualan_po->get($data['trx']['id_po']);
            $data['detail_po'] = $this->user->join($this->penjualan_po_detail->get_data_detail_kontan($data['trx']['id']), 'submit_by', 'input_by');;
            $data['tonase'] = 0;
            $data['list_kemasan'] = array();
            // Init kemasan
            if(!empty($data['kemasan'])){
                foreach($data['kemasan'] as $row){
                    $data['list_kemasan'][$row['id']] = $row;
                }
                
            }
            // dump($data['detail']);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Kontan' => base_url('penjualan/beras/kontan'), 'Transaksi' => base_url('penjualan/beras/kontan/transaksi/'.$data['trx']['id_po']), 'Detail Penjualan' => 'active'));
            set_session('title', 'Penjualan Beras - Detail Transaksi Kontan');
            set_activemenu('sub-penjualan', 'menu-pj-kt');
            $this->render('penjualan/beras/v-penjualan-kontan-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_tambah_trx()
    {
        $post = $this->input->post();
        $post['kode'] = $this->generate_kode($post['tanggal']);
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $post['status_kontan'] = 1;
        $post['jenis_pembayaran'] = 'cash';
        $post['is_tempo'] = 0;
        $post['tgl_kirim'] = $post['tanggal'];
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $exist = $this->penjualan_po->cek_transaksi_kontan($post['tanggal'], $post['id_sales']);
            if($exist != false){
                # Redirect ke detail transaksi
                $this->go('penjualan/kontan/detail/'.$exist['id']);
            }else{
                $result = $this->penjualan_po->insert($post);
                if ($result) {
                    $this->message('Berhasil membuat data.', 'success');
                } else {
                    $this->message('Gagal.', 'error');
                }
                $this->go('penjualan/beras/kontan/transaksi/'.$this->db->insert_id());
            }
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_po->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/kontan/index?'.$url);      
        }
    }

    public function submit_tambah_transaksi(){
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $po = $post['id_po'];
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_po_kontan->insert($post);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/kontan/detail/'.$this->db->insert_id());
        }else{
            $id = $post['id'];
            unset($post['id']);
            # Update statement
            $result = $this->penjualan_po_kontan->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal mengubah data.', 'error');
            }
            $this->go('penjualan/beras/kontan/transaksi/'.$po);
        }
    }

    public function submit_tambah_detail_trx(){
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $id = $post['id_kontan'];
        $result = $this->penjualan_po_detail->insert($post);
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        $this->go('penjualan/beras/kontan/detail/'.$id);
    }

    public function cetak_sj($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get($id), 'username_input', 'input_by');
        $data['detail_sj'] = $this->penjualan_po_detail->get_data_detail($id);
        // dump($data);
        $data['tonase'] = 0;
        if(!empty($data['detail_po'])){
            foreach($data['detail_po'] as $row){
                $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
            }
        }
        // dump($data);
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-surat-jalan', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Surat Jalan #".$data['detail']['kode_surat_jalan']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $kode = str_replace('/', '-', $data['detail']['kode']);
        $file_name = "Penjualan_Kontan_Nota_".$kode.'.pdf';
        $mpdf->WriteHTML($html);

        $mpdf->Output($file_name, 'I');
    }

    public function cetak_faktur($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get($id), 'username_input', 'input_by');
        $data['detail_sj'] = $this->penjualan_po_detail->get_data_detail($id);
        $kode = $data['detail']['kode'];
        $data['tonase'] = 0;
        if(!empty($data['detail_po'])){
            foreach($data['detail_po'] as $row){
                $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
            }
        }
        // dump($data);
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-faktur', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Faktur Penjualan #".$data['detail']['kode']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $kode = str_replace('/', '-', $data['detail']['kode']);
        $file_name = "Penjualan_Beras_Nota_".$kode.'.pdf';

        $mpdf->WriteHTML($html);

        $mpdf->Output($file_name, 'I');
    }

    public function cetak_faktur_transaksi($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get_data_kontan($id), 'username_input', 'input_by');
        $data['detail_sj'] = $this->penjualan_po_detail->get_data_detail_faktur_kontan($id);
        $data['id'] = $id;
        $kode = $data['detail']['kode'];
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 7,
            'margin_right' => 7,
            'margin_top' => 4,
            'margin_bottom' => 4,
            'margin_header' => 0,
            'margin_footer' => 0,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-faktur-transaksi', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Faktur Penjualan #".$data['detail']['kode'].'-'.zerofy($id,5));
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $kode = str_replace('/', '-', $data['detail']['kode']);
        $file_name = "Penjualan_Kontan_Nota_Transaksi_".$kode.'.pdf';
        $mpdf->WriteHTML($html);

        $mpdf->Output($file_name, 'I');
    }

    public function generate_kode($tgl)
    {
        $last = $this->penjualan_po->get_kode_po($tgl);
        $index = 1;
        // $last = 'PO-001/04/20 ';
        if ($last != false) {
            $temp = explode('-', $last);
            $temp2 = explode('/', $temp[1]);
            $index = intval($temp2[0]) + 1;
        }
        $kode = 'BK-' . 'BAS-' . date("Ym", strtotime($tgl)) . zerofy($index, 3);
        return $kode;
    }
}
