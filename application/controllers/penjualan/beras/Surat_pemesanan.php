<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Surat_pemesanan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->load->model('generate_model', 'generate'); // model generate kode //
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-po', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['area'] = $this->area->get_active();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Belum Diisi", 2 => "Input Harga", 3 => 'Atur Pengiriman', 4 => "Selesai");
            if(!empty($this->input->get())){
                $data['transaksi'] = $this->user->join($this->penjualan_po->get_data_trx(), 'submit_by', 'input_by');
            }else{
                $data['transaksi'] = array();
            }
            // dump($data['transaksi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Surat Pemesanan' => 'active'));
            set_session('title', 'Penjualan Beras - Surat Pemesanan');
            set_activemenu('sub-penjualan', 'menu-pj-po');
            $this->render('penjualan/beras/v-purchase-order', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-po', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['detail'] = $this->penjualan_po->get($id);
            $cust = $data['detail']['id_customer'];
            $data['detail_po'] = $this->penjualan_po_detail->get_data_detail($id);
            $data['tonase'] = 0;
            $data['jml'] = $this->penjualan_po->get_jumlah_tempo($cust, $id);
            $data['po'] = $data['jml'][0]['jml_tempo'];
            if(!empty($data['detail_po'])){
                foreach($data['detail_po'] as $row){
                    $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
                }
            }
            // dump($data['po']);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Surat Pemesanan' => base_url('penjualan/beras/surat_pemesanan'), 'Detail Pemesanan' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Penjualan Beras - Detail Pemesanan');
            set_activemenu('sub-penjualan', 'menu-pj-po');
            $this->render('penjualan/beras/v-purchase-order-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_tambah_po()
    {
        $post = $this->input->post();
        ## generate kode ##
        $fitur = 'JB';
        $tgl = $post['tanggal'];
        $last = $this->penjualan_po->get_kode_po($tgl);
        $cust = $this->generate->get_customer_init($post['id_customer']);
        $post['kode'] = $this->generate->generate_kode($fitur, $tgl, $cust, $last);
        ## end generate kode ##
        dump($post['kode']);
        $post['input_by'] = get_session('auth')['id'];
        $get_kode = explode('-', $post['kode']);
        $get_kode_cust = $get_kode[1];
        $get_kode_index = $get_kode[2];
        $post['kode_surat_jalan'] = 'SJ-'.$get_kode_cust. '-' .$get_kode_index;
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_po->insert($post);
            if ($result) {
                $this->message('Berhasil membuat data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/surat_pemesanan/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_po->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/surat_pemesanan/index?'.$url);
        }
    }

    public function cetak_po($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get($id), 'username_input', 'input_by');
        $data['detail_po'] = $this->penjualan_po_detail->get_data_detail($id);
        $data['merk'] = $this->merk_beras->get_active();
        $data['kemasan'] = $this->merk_beras_kemasan->get_data();
        $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
        $data['tonase'] = 0;
        if(!empty($data['detail_po'])){
            foreach($data['detail_po'] as $row){
                $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
            }
        }
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-surat-pemesanan', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Surat Pemesanan #".$data['detail']['kode']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $kode = str_replace('/', '-', $data['detail']['kode']);
        // dump($data);

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        $file_name = "Surat Pemesanan ".$kode.'.pdf';

        $mpdf->Output($file_name, 'I');
    }

}
