<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Retur extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_retur_model', 'penjualan_retur');
        $this->load->model('penjualan_retur_detail_model', 'penjualan_retur_detail');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-rtr', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['area'] = $this->area->get_active();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Belum Diisi", 2 => "Selesai");
            if(!empty($this->input->get())){
                $data['transaksi'] = $this->user->join($this->penjualan_retur->get_data_trx(), 'submit_by', 'input_by');
            }else{
                $data['transaksi'] = array();
            }
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Retur' => 'active'));
            set_session('title', 'Penjualan Beras - Retur Penjualan');
            set_activemenu('sub-penjualan', 'menu-penjualan-retur');
            $this->render('penjualan/beras/v-retur-penjualan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-rtr', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['trx'] = $this->penjualan_retur->get($id);
            $data['detail'] = $this->user->join($this->penjualan_retur_detail->get_data_detail($id), 'username_input', 'input_by');
            $data['tonase'] = 0;
            if(!empty($data['detail'])){
                foreach($data['detail'] as $row){
                    $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
                }
            }
            // dump($data);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Retur Penjualan' => base_url('penjualan/beras/retur'), 'Detail Transaksi' => 'active'));
            set_session('title', 'Penjualan Beras - Detail Retur Penjualan');
            set_activemenu('sub-penjualan', 'menu-penjualan-retur');
            $this->render('penjualan/beras/v-retur-penjualan-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_transaksi()
    {
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_retur->insert($post);
            if ($result) {
                $this->message('Berhasil membuat data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/retur/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_retur->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/retur/index?'.$url);
        }
    }

    public function submit_detail_transaksi()
    {
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        $id_retur = $post['id_retur'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_retur_detail->insert($post);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/retur/detail/'.$id_retur);
        }
    }

    public function export()
    {
        $transaksi = $this->user->join($this->penjualan_retur->get_data_rekap(), 'username_input', 'input_by');
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Area');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Customer');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Status');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Merk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kemasan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kuantitas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tonase (KG)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Satuan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['area']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tanggal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['customer']);
                if($row['status'] == 1){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Selesai');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Belum Diisi');
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['keterangan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['merk']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kemasan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kuantitas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kuantitas']*$row['kemasan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['harga_satuan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kuantitas'] * $row['harga_satuan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['username_input']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        // dump($transaksi);

        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_retur_penjualan_beras.xlsx"');
		$writer->save("php://output");
    }

}
