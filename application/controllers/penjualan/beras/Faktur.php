<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Faktur extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('customer_model', 'customer');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->url_transport = $this->config->item('url_transport');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-np', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 2;
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['area'] = $this->area->get_active();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(2 => "Input Harga", 3 => "Atur Pengiriman", 4 => "Selesai");
            $data['pembayaran'] = array(1 => "Belum diisi", 2 => "Tempo", 3 => "Lunas");
            if(!empty($this->input->get())){
                $data['transaksi'] = $this->user->join($this->penjualan_po->get_data_nota(), 'submit_by', 'input_by');
            }else{
                $data['transaksi'] = array();
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Faktur Penjualan' => 'active'));
            set_session('title', 'Faktur Penjualan');
            set_activemenu('sub-penjualan', 'menu-pj-fp');
            $this->render('penjualan/beras/v-faktur', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-po', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            $data['merk'] = $this->merk_beras->get_active();
            $data['kemasan'] = $this->merk_beras_kemasan->get_data();
            $data['jenis_kemasan'] = $this->merk_beras_kemasan->get_jenis_kemasan();
            $data['detail'] = $this->penjualan_po->get($id);
            $data['detail_po'] = $this->penjualan_po_detail->get_data_detail($id);
            $cust = $data['detail']['id_customer'];
            $data['jml'] = $this->penjualan_po->get_jumlah_tempo($cust);
            $data['po'] = $data['jml'][0]['jml_tempo'];
            $data['tonase'] = 0;
            $data['list_kemasan'] = array();
            
            // Init kemasan
            if(!empty($data['kemasan'])){
                foreach($data['kemasan'] as $row){
                    $data['list_kemasan'][$row['id']] = $row;
                }
                
            }

            if(!empty($data['detail_po'])){
                foreach($data['detail_po'] as $row){
                    $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
                }
            }
            if (empty($this->input->get())) {
                $_GET['status'] = '1';
                $_GET['kendaraan'] = 'all';
                $_GET['start'] = '';
                $_GET['end'] = '';
            }
            $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_active?token='.$this->session->auth['token'].'&start='.$_GET['start'].'&end='.$_GET['end'].'&kendaraan='.$_GET['kendaraan'].'&status='.$_GET['status']); 
            $data['ekspedisi']  = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
            $param_kendaraan    = array(CURLOPT_URL => $this->url_transport.'api/external/kendaraan/get?token='.$this->session->auth['token']); 
            $data['kendaraan']  = json_decode(xcurl($param_kendaraan)['response']);
            // dump($data['ekspedisi']);


            // dump($data);
                
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Faktur Penjualan' => base_url('penjualan/beras/faktur'), 'Detail Penjualan' => 'active', $data['detail']['kode'] => 'active'));
            set_session('title', 'Faktur Penjualan');
            set_activemenu('sub-penjualan', 'menu-pj-fp');
            $this->render('penjualan/beras/v-faktur-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_tambah_po()
    {
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();

        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_po->insert($post);
            if ($result) {
                $this->message('Berhasil membuat data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/surat_pemesanan/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_po->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/beras/surat_pemesanan/index?'.$url);
        }
    }

    public function cetak_sj($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get($id), 'username_input', 'input_by');
        $data['detail_sj'] = $this->penjualan_po_detail->get_data_detail($id);
        // dump($data);
        $data['tonase'] = 0;
        if(!empty($data['detail_po'])){
            foreach($data['detail_po'] as $row){
                $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
            }
        }
        // dump($data);
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-surat-jalan', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Surat Jalan #".$data['detail']['kode_surat_jalan']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $kode = str_replace('/', '-', $data['detail']['kode_surat_jalan']);
        $file_name = "Penjualan_Beras_Surat_Jalan_".$kode.'.pdf';

        $mpdf->WriteHTML($html);
        


        $mpdf->Output($file_name, 'I');
    }

    public function cetak_faktur($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_po->get($id), 'username_input', 'input_by');
        $data['detail_sj'] = $this->penjualan_po_detail->get_data_detail($id);
        // dump($data);
        $data['tonase'] = 0;
        if(!empty($data['detail_po'])){
            foreach($data['detail_po'] as $row){
                $data['tonase'] += ($row['kemasan'] * $row['kuantitas']);
            }
        }

        if(count($data['detail_sj']) > 9){
            $margin_footer = 0;
        }else{
            $margin_footer = 160;
        }
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => $margin_footer,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/beras/v-cetak-faktur', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Nota Penjualan #".$data['detail']['kode']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $kode = str_replace('/', '-', $data['detail']['kode']);
        $file_name = "Penjualan_Beras_Nota_".$kode.'.pdf';

        $mpdf->WriteHTML($html);

        $mpdf->Output($file_name, 'I');
    }

}
