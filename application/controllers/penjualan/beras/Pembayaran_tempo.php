<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Pembayaran_tempo extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('penjualan_po_pembayaran_model', 'penjualan_po_pembayaran');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->load->model('stok_beras_model', 'stok_beras');
        $this->load->model('stok_beras_trx_model', 'stok_beras_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-tmp', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 2;
            }
            $data['penjualan'] = $this->user->join($this->penjualan_po->get_data_pembayaran_tempo(), 'submit_by', 'diajukan_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Jatuh tempo", 2 => "Sedang Berlangsung", 3 => "Lunas");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);     
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['area']       = $this->area->get_active();
            $data['sales']      = $this->master_sales->get_active();
            $data['customer']   = $this->customer->get_customer_penjualan_beras();
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Beras' => 'active', 'Pembayaran Tempo' => 'active'));
            set_session('title', 'Penjualan Beras - Pembayaran Tempo');
            set_activemenu('sub-penjualan', 'menu-pj-tempo');
            $this->render('penjualan/beras/v-pembayaran-tempo', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-tmp', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $param_kas              = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $param_rek_penerima     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
                $data['kas']            = json_decode(xcurl($param_kas)['response']);
                $data['rekening']       = json_decode(xcurl($param_rek_penerima)['response']);
                $data['pembayaran']     = array('cash', 'transfer');
                $data['penjualan']      = $this->user->join($this->penjualan_po->get_data_pembayaran_tempo($id), 'submit_by', 'diajukan_oleh')[0]; 
                $data['detail']         = $this->user->join($this->kas->join($this->user->join($this->penjualan_po_pembayaran->get_detail_pembayaran($id), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'acc', 'acc_by');
                $data['sisa_tempo']     = $data['penjualan']['grandtotal'];
                $pembayaran_verif       = $this->penjualan_po_pembayaran->get_data_pembayaran_verified($id, '');
                if($pembayaran_verif != false){
                    # Ada riwayat transaksi yg terverifikasi
                    $total_pembayaran = 0;
                    foreach($pembayaran_verif as $row){
                        $total_pembayaran += $row['jumlah_bayar'];
                    }
                    $data['sisa_tempo'] -= $total_pembayaran;
                }
                // dump($data);
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Beras' => base_url('penjualan/beras/pembayaran_tempo'), 'Pembayaran Tempo' => base_url('penjualan/beras/pembayaran_tempo'), 'Detail Pembayaran' => 'active'));
                set_session('title', 'Penjualan Beras - Pembayaran Tempo');
                set_activemenu('sub-penjualan', 'menu-pj-tempo');
                $this->render('penjualan/beras/v-pembayaran-tempo-detail', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan(){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-tmp-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['customer']   = $this->customer->get_customer_penjualan_beras();                  
			$data['pembayaran'] = $this->user->join($this->kas->join($this->user->join($this->penjualan_po_pembayaran->get_data_otorisasi_pembayaran(), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'acc', 'acc_by');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Beras' => 'active', 'Persetujuan Pembayaran' => 'active'));
            set_session('title', 'Penjualan Beras - Persetujuan Pembayaran');
            set_activemenu('sub-penjualan', 'menu-pj-tempo-acc');
			// dump($data);
            $this->render('penjualan/beras/v-otorisasi-pembayaran', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_pembayaran()
    {
        $post = $this->input->post();
        unset($post['sisa_bayar_val']);
        unset($post['sisa_bayar']);
        if($post['jenis_pembayaran'] == 'cash'){
            $post['id_kas'] = $post['kas'];
        }else{
            $post['id_kas'] = $post['rekening'];
        }
        unset($post['kas']);
        unset($post['rekening']);
        $post['jumlah_bayar'] = str_replace(",", "", $post['jumlah_bayar']);
        $id_penjualan = $post['id_penjualan'];

        if(empty($post['id'])){
            # Insert Statement
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->penjualan_po_pembayaran->insert($post);
            if ($result) {
                $this->message('Berhasil mengajukan pembayaran', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }else{
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->penjualan_po_pembayaran->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('penjualan/beras/pembayaran_tempo/detail/'.$id_penjualan);
    }
  
}
