<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('area_model', 'area');
        $this->load->model('master_sales_model', 'master_sales');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_detail_model', 'penjualan_po_detail');
        $this->load->model('penjualan_produksi_model', 'penjualan_produksi');
        $this->load->model('penjualan_produksi_detail_model', 'penjualan_produksi_detail');
        $this->load->model('merk_beras_model', 'merk_beras');
        $this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->url_transport = $this->config->item('url_transport');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-xpd', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = '1';
                $_GET['kendaraan'] = 'all';
                $_GET['start'] = '';
                $_GET['end'] = '';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['area'] = $this->area->get_active();
            $data['sales'] = $this->master_sales->get_active();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Menunggu input", 2 => "Diproses", 3 => "Selesai");
            $data['po'] = $this->penjualan_po->get_data_input_ekspedisi();
            $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_active?token='.$this->session->auth['token'].'&start='.$_GET['start'].'&end='.$_GET['end'].'&kendaraan='.$_GET['kendaraan'].'&status='.$_GET['status']); 
            $data['ekspedisi']  = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
            $param_kendaraan    = array(CURLOPT_URL => $this->url_transport.'api/external/kendaraan/get?token='.$this->session->auth['token']); 
            $data['kendaraan']  = json_decode(xcurl($param_kendaraan)['response']);
            // dump($data['ekspedisi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan' => 'active', 'Ekspedisi' => 'active'));
            set_session('title', 'Penjualan Beras - Ekspedisi');
            set_activemenu('sub-penjualan', 'menu-pj-ekspedisi');
            $this->render('penjualan/beras/v-ekspedisi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($resi)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pj-xpd', $hak->bas) || $hak->bas[0] == '*') {
            $data['list']       = $this->penjualan_po->get_muatan_ekspedisi($resi);
            $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_detail_ekspedisi?token='.$this->session->auth['token'].'&resi='.$resi); 
            $data['detail']  = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
            // dump($data);
            
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Ekspedisi' => base_url('penjualan/beras/ekspedisi'), 'Detail' => 'active', $resi => 'active'));
            set_session('title', 'Penjualan Beras - Ekspedisi Detail');
            set_activemenu('sub-penjualan', 'menu-pj-ekspedisi');
            $this->render('penjualan/beras/v-ekspedisi-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_resi(){
        $post = $this->input->post();
        # Update data muatan
        $result = $this->penjualan_po->update($post['id_po'], array('no_resi' => $post['no_resi'], 'status_pengiriman' => 1));
        $list = $this->penjualan_po->get_muatan_ekspedisi($post['no_resi']);
        $muatan = 0;
        if(!empty($list)){
            foreach($list as $row){
                $muatan += $row['tonase'];
            }
        }
        $param_update    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/update_muatan?token='.$this->session->auth['token'].'&resi='.$post['no_resi'].'&muatan='.$muatan); 
        xcurl($param_update);
        # Update status pengiriman
        if ($result) {
            $this->message('Berhasil menyimpan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        $this->go('penjualan/beras/ekspedisi');
    }


}
