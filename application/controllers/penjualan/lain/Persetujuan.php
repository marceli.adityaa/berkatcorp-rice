<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Persetujuan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_lain_model', 'penjualan_lain');
        $this->load->model('penjualan_lain_detail_model', 'penjualan_lain_detail');
        $this->load->model('master_penjualan_lain_model', 'master_penjualan_lain');
        $this->load->model('pendapatan_model', 'pendapatan'); //
        $this->load->model('beban_model', 'beban'); //
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->id_potongan_penjualan = $this->config->item('id_potongan_penjualan'); //
        $this->id_biaya_penjualan = $this->config->item('id_biaya_penjualan'); // 
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pjl-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_penerima'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            $data['pembayaran'] = array('cash', 'transfer');
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(2 => "Menunggu Persetujuan", 3 => 'Disetujui', 4 => "Ditolak");
            $data['transaksi'] = $this->user->join($this->penjualan_lain->get_data_persetujuan(), 'submit_by', 'input_by');
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Lain' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Penjualan Lain - Persetujuan');
            set_activemenu('sub-penjualan-lain', 'menu-pjl-acc');
            $this->render('penjualan/lain/v-penjualan-persetujuan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pjl-acc', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            $data['kategori'] = $this->master_penjualan_lain->get_active();
            $data['trx'] = $this->penjualan_lain->get($id);
            $data['detail'] = $this->user->join($this->penjualan_lain_detail->get_data_detail($id), 'submit_by', 'input_by');;
            // dump($data);
            set_session('breadcrumb', array('Penjualan Lain' => 'active', 'Persetujuan' => base_url('penjualan/lain/persetujuan'), 'Detail Penjualan' => 'active'));
            set_session('title', 'Penjualan Lain - Detail Transaksi');
            set_activemenu('sub-penjualan-lain', 'menu-pjl-kll');
            $this->render('penjualan/lain/v-penjualan-lain-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_approval(){
        $post = $this->input->post();
        $id = $post['id'];
        $jual = $this->penjualan_lain->get_data($id);
        if(!empty($id)){
            if(isset($post['jenis_pembayaran'])){
                if($post['jenis_pembayaran'] == 'cash'){
                    $kas = $post['id_kas'];
                }else{
                    $kas = $post['id_rek_penerima'];
                }
            }else{
                $post['jenis_pembayaran'] = 'tbd';
                $kas = 0;
            }
            if($post['is_tempo'] == 0){
                $isLunas = 1;
            }else{
                $isLunas = 0;
            }

			$update = array(
				'is_tempo' => $post['is_tempo'],
				'tgl_tempo' => $post['tgl_tempo'],
				'jenis_pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $kas,
                'jumlah_bayar' => $jual['grandtotal'],
				'tgl_bayar' => $post['tgl_bayar'],
                'persetujuan' => 1,
                'is_lunas' => $isLunas,
				'verif_by' => get_session('auth')['id'],
				'timestamp_verif' => setNewDateTime(),
			);

            $result = $this->penjualan_lain->update($id, $update);
            // $result = true;
            if(!$post['is_tempo']){
                $penjualan = $this->penjualan_lain->get_data($id);
                $subtotal = $penjualan['grandtotal'] + $penjualan['potongan'] - $penjualan['biaya_tambahan']; 
                if($result){
                    # Jika lunas 
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $penjualan['id_kas'];
                    $arus_kas['tgl_transaksi'] = $penjualan['tgl_bayar'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = "penjualan-lain";
                    $arus_kas['id_sumber'] = $post['id'];
                    $arus_kas['keterangan'] = 'Penjualan lain '.$penjualan['kode'];
                    $arus_kas['deskripsi'] = 'Bayar Lunas';
                    $arus_kas['nominal'] = $subtotal;
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                    if($penjualan['potongan'] > 0){
                        # Jika terdapat potongan penjualan, maka insert ke pendapatan lain (auto acc) dan buku kas
                        # Insert ke beban
                        $beban['id_beban'] = $this->id_potongan_penjualan;
                        $beban['tgl_transaksi'] = $penjualan['tgl_bayar'];
                        $beban['nominal'] = $penjualan['potongan'];
                        $beban['catatan'] = 'Potongan penjualan lain '.$penjualan['kode'];
                        $beban['jenis_pembayaran'] = $penjualan['jenis_pembayaran'];
                        $beban['id_kas'] = $penjualan['id_kas'];
                        $beban['is_verifikasi'] = 1;
                        $beban['input_by'] = get_session('auth')['id'];
                        $beban['timestamp_input'] = setNewDateTime();
                        $beban['verifikasi_by'] = get_session('auth')['id'];
                        $beban['timestamp_verifikasi'] = setNewDateTime();
                        $result = $this->beban->insert($beban);

                        # Insert ke buku kas
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $penjualan['id_kas'];
                        $arus_kas['tgl_transaksi'] = $penjualan['tgl_bayar'];
                        $arus_kas['arus'] = "out";
                        $arus_kas['sumber'] = "beban-penjualan-lain";
                        $arus_kas['id_sumber'] = $penjualan['id'];
                        $arus_kas['keterangan'] = 'Potongan penjualan lain '.$penjualan['kode'];
                        $arus_kas['deskripsi'] = '';
                        $arus_kas['nominal'] = $penjualan['potongan'];
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }

                    if($penjualan['biaya_tambahan'] > 0){
                        # Jika terdapat biaya tambahan penjualan, maka insert ke beban penjualan (auto acc) dan buku kas
                        # Insert ke pendapatan
                        $pendapatan['id_kategori'] = $this->id_biaya_penjualan;
                        $pendapatan['tgl_transaksi'] = $penjualan['tgl_bayar'];
                        $pendapatan['nominal'] = $penjualan['biaya_tambahan'];
                        $pendapatan['catatan'] = 'Biaya tambahan penjualan lain '.$penjualan['kode'];
                        $pendapatan['jenis_pembayaran'] = $penjualan['jenis_pembayaran'];
                        $pendapatan['id_kas'] = $penjualan['id_kas'];
                        $pendapatan['is_verifikasi'] = 1;
                        $pendapatan['input_by'] = get_session('auth')['id'];
                        $pendapatan['timestamp_input'] = setNewDateTime();
                        $pendapatan['verifikasi_by'] = get_session('auth')['id'];
                        $pendapatan['timestamp_verifikasi'] = setNewDateTime();
                        $result = $this->pendapatan->insert($pendapatan);

                        # Insert ke buku kas
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $penjualan['id_kas'];
                        $arus_kas['tgl_transaksi'] = $penjualan['tgl_bayar'];
                        $arus_kas['arus'] = "in";
                        $arus_kas['sumber'] = "pendapatan-penjualan-lain";
                        $arus_kas['id_sumber'] = $penjualan['id'];
                        $arus_kas['keterangan'] = 'Biaya tambahan penjualan lain '.$penjualan['kode'];
                        $arus_kas['deskripsi'] = '';
                        $arus_kas['nominal'] = $penjualan['biaya_tambahan'];
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }
                    
                    
                }
            }
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('penjualan/lain/persetujuan?'.$post['url']);
    }
}
