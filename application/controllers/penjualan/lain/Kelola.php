<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Kelola extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_lain_model', 'penjualan_lain');
        $this->load->model('penjualan_lain_detail_model', 'penjualan_lain_detail');
        $this->load->model('master_penjualan_lain_model', 'master_penjualan_lain');
        $this->load->model('generate_model', 'generate'); // model generate kode //
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pjl-kll', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_customer();
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            $data['status'] = array(1 => "Input Data", 2 => "Menunggu Persetujuan", 3 => 'Disetujui', 4 => "Ditolak");
            $data['transaksi'] = $this->user->join($this->penjualan_lain->get_data_trx(), 'submit_by', 'input_by');
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Lain' => 'active'));
            set_session('title', 'Penjualan Lain');
            set_activemenu('sub-penjualan-lain', 'menu-pjl-kll');
            $this->render('penjualan/lain/v-penjualan-lain', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pjl-kll', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            $data['kategori'] = $this->master_penjualan_lain->get_active();
            $data['trx'] = $this->penjualan_lain->get($id);
            $data['detail'] = $this->user->join($this->penjualan_lain_detail->get_data_detail($id), 'submit_by', 'input_by');;
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Lain' => base_url('penjualan/lain/kelola'), 'Detail Penjualan' => 'active'));
            set_session('title', 'Penjualan Lain - Detail Transaksi');
            set_activemenu('sub-penjualan-lain', 'menu-pjl-kll');
            $this->render('penjualan/lain/v-penjualan-lain-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_tambah_trx()
    {
        $post = $this->input->post();
        ## generate kode ##
        $fitur = 'JL';
        $tgl = $post['tanggal'];
        $cust = $this->generate->get_customer_init($post['id_customer']);
        $last = $this->penjualan_lain->get_kode($tgl);
        $post['kode'] = $this->generate->generate_kode($fitur, $tgl, $cust, $last);
        ## end generate kode ##
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $post['jenis_pembayaran'] = 'tbd';
        $post['is_tempo'] = 0;
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->penjualan_lain->insert($post);
            if ($result) {
                $this->message('Berhasil membuat data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/lain/kelola/detail/'.$this->db->insert_id());
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->penjualan_lain->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
            $this->go('penjualan/lain/kelola/index?'.$url);      
        }
    }

    public function submit_tambah_detail_trx(){
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $id = $post['id_penjualan'];
        $result = $this->penjualan_lain_detail->insert($post);
        if ($result) {
            $this->message('Berhasil memasukkan data.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }
        $this->go('penjualan/lain/kelola/detail/'.$id);
    }

    public function cetak_faktur($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_lain->get($id), 'username_input', 'input_by');
        $data['detail_trx'] = $this->penjualan_lain_detail->get_data_detail($id);
        $data['id'] = $id;
        $kode = $data['detail']['kode'];

        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/lain/v-cetak-faktur', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Faktur Penjualan #".$data['detail']['kode'].'-'.zerofy($id,5));
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        $file_name = "Penjualan_Lain_Nota_".$kode;
        $mpdf->Output($file_name, 'I');
    }

    public function cetak_faktur_kecil($id)
    {
        $data['detail'] = $this->user->join($this->penjualan_lain->get($id), 'username_input', 'input_by');
        $data['detail_trx'] = $this->penjualan_lain_detail->get_data_detail($id);
        $data['id'] = $id;

        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 7,
            'margin_right' => 7,
            'margin_top' => 4,
            'margin_bottom' => 4,
            'margin_header' => 0,
            'margin_footer' => 0,
            'format' => 'A4'
            ]);
        
        $html = $this->load->view('modules/penjualan/lain/v-cetak-faktur-kecil', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Faktur Penjualan #".$data['detail']['kode'].'-'.zerofy($id,5));
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);

        $mpdf->Output();
    }

  
}
