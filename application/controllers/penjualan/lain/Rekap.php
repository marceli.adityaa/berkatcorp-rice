<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Rekap extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('kas_model', 'kas');
        $this->load->model('customer_model', 'customer');
        $this->load->model('penjualan_lain_model', 'penjualan_lain');
        $this->load->model('penjualan_lain_detail_model', 'penjualan_lain_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pjl-rkp', $hak->bas) || $hak->bas[0] == '*') {
            if (!empty($this->input->get())) {
                // $_GET['status'] = 'all';
                $data['transaksi'] = $this->user->join($this->penjualan_lain->get_data_rekap(), 'submit_by', 'diajukan_oleh');
            }else{
                $data['transaksi'] = array();
            }
            $data['role'] = $this->role;
            $data['area']       = $this->area->get_active();
            $data['customer']   = $this->customer->get_active();
            $data['status']     = array(1 => "Lunas", 2 => "Belum Lunas");
            $data['tempo']      = array(1 => "Berlangsung", 2 => "Jatuh Tempo");

            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);     
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Penjualan Lain' => 'active', 'Rekap Transaksi' => 'active'));
            set_session('title', 'Penjualan Lain - Rekap Transaksi');
            set_activemenu('sub-penjualan-lain', 'menu-pjl-rkp');
            $this->render('penjualan/lain/v-rekap-transaksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function export()
    {
        $transaksi = $this->user->join($this->kas->join($this->user->join($this->penjualan_lain->get_data_rekap(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'username_input', 'input_by');
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Area');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Customer');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Transaksi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Status');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tempo');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pembayaran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Tagihan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Dibayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Tagihan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['area']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tanggal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['customer']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kode']);
                if($row['is_lunas'] == 1){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Lunas');
                }else{
                    if($row['tgl_tempo'] <= $now = date('Y-m-d')){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jatuh Tempo');
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sedang Berlangsung');
                    }
                }
                if($row['is_tempo']){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Ya');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                if($row['jenis_pembayaran'] == 'tbd'){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_pembayaran']);
                }
                if(isset($row['kas_is_rekening'])){
                    if($row['kas_is_rekening'] == 1){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kas_bank'].'-'.$row['kas_no_rek']));
                        
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kas_kas']);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['grandtotal']);
                if($row['is_tempo']==0){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['grandtotal']);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jumlah_bayar']);
                }
                if($row['is_lunas']){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['grandtotal'] - $row['jumlah_bayar']));
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_bayar']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['username_input']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        // dump($transaksi);

        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_penjualan_lain.xlsx"');
		$writer->save("php://output");
    }
}
