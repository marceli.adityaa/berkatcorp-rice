<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item extends MY_Controller_admin
{
	public $bypass_auth = true;

	public function __construct()
	{
		parent::__construct();
        $this->load->model('master_pembelian_lain_model', 'master_pembelian_lain');
        $this->load->model('master_penjualan_lain_model', 'master_penjualan_lain');
	}
    
    public function pembelian_lain()
    {
		$data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_pembelian_lain->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Item Pembelian Lain' => 'active'));
        set_session('title', 'Master Data - Item Pembelian Lain');
        set_activemenu('sub-master', 'menu-item-pembelian-lain');
		$this->render('master/v-item-pembelian-lain', $data);
    }

    public function penjualan_lain()
    {
        $data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_penjualan_lain->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Item Penjualan Lain' => 'active'));
        set_session('title', 'Master Data - Item Penjualan Lain');
        set_activemenu('sub-master', 'menu-item-penjualan-lain');
		$this->render('master/v-item-penjualan-lain', $data);
    }

	public function submit_form_pembelian_lain(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_pembelian_lain->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_pembelian_lain->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/item/pembelian_lain');
	}

	public function submit_form_penjualan_lain(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_penjualan_lain->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_penjualan_lain->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/item/penjualan_lain');
    }
    
    public function json_get_pembelian_lain_detail(){
		$id = $this->input->post('id');
		$response = $this->master_pembelian_lain->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_pembelian_lain_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_pembelian_lain->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_pembelian_lain_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_pembelian_lain->update($id, array('status' => 0));
		echo json_encode($response);
    }

    public function json_get_penjualan_lain_detail(){
		$id = $this->input->post('id');
		$response = $this->master_penjualan_lain->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_penjualan_lain_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_penjualan_lain->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_penjualan_lain_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_penjualan_lain->update($id, array('status' => 0));
		echo json_encode($response);
    }
}
