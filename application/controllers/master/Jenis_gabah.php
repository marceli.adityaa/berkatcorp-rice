<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_gabah extends MY_Controller_admin
{
	public $class_id = 'jn-gbh';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('jenis_gabah_model', 'jenis_gabah');
	}

	public function index()
	{
		$data['jenis_gabah'] = $this->jenis_gabah->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Jenis Gabah' => 'active'));
        set_session('title', 'Master Jenis Gabah');
        set_activemenu('sub-master', 'menu-jenis-gabah');
		$this->render('master/v-jenis-gabah', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->jenis_gabah->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->jenis_gabah->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/jenis_gabah');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->jenis_gabah->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_gabah->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_gabah->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
