<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proses_produksi extends MY_Controller_admin
{
	public $class_id = 'm-prd';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produksi_proses_model', 'produksi_proses');
	}

	public function index()
	{
		$data['list'] = $this->produksi_proses->order_by(array('status' => 'desc', 'urutan' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Proses Produksi' => 'active'));
        set_session('title', 'Master Proses Produksi');
        set_activemenu('sub-master', 'menu-proses-produksi');
		$this->render('master/v-proses-produksi', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->produksi_proses->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->produksi_proses->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/proses_produksi');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->produksi_proses->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->produksi_proses->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->produksi_proses->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
