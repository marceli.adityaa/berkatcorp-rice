<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_padi extends MY_Controller_admin
{
	public $class_id = 'jn-pdi';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_padi_model', 'jenis_padi');
	}

	public function index()
	{
		$data['jenis_padi'] = $this->jenis_padi->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Jenis Padi' => 'active'));
        set_session('title', 'Master Jenis Padi');
        set_activemenu('sub-master', 'menu-jenis-padi');
		$this->render('master/v-jenis-padi', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->jenis_padi->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->jenis_padi->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/jenis_padi');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->jenis_padi->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_padi->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_padi->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
