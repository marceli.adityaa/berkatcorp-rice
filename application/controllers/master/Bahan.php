<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bahan extends MY_Controller_admin
{
	public $class_id = 'bhn';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bahan_model', 'bahan');
	}

	public function index()
	{
		$data['bahan'] = $this->bahan->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Bahan' => 'active'));
		set_session('title', 'Master Bahan');
		set_activemenu('sub-master', 'menu-bahan');
		$this->render('master/v-bahan', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->bahan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->bahan->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/bahan');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->bahan->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->bahan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->bahan->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
