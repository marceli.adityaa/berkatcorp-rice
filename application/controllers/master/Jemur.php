<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jemur extends MY_Controller_admin
{
	public $class_id = 'm-jemur';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Master_jemur_model', 'master_jemur');
	}

	public function index()
	{
		$data['list'] = $this->master_jemur->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Dryer' => 'active'));
        set_session('title', 'Master Jemur');
        set_activemenu('sub-master', 'menu-master-jemur');
		$this->render('master/v-master-jemur', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_jemur->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_jemur->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/jemur');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->master_jemur->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_jemur->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_jemur->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
