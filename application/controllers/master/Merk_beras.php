<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merk_beras extends MY_Controller_admin
{
	public $bypass_auth = true;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('merk_beras_model', 'merk_beras');
		$this->load->model('merk_beras_kemasan_model', 'merk_beras_kemasan');
		$this->load->model('merk_beras_harga_model', 'merk_beras_harga');
	}

	public function index()
	{
		$data['list'] = $this->merk_beras->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Merk Beras' => 'active'));
        set_session('title', 'Merk Beras');
        set_activemenu('sub-master', 'menu-merk-beras');
		$this->render('master/v-merk-beras', $data);
	}

	public function kemasan()
	{
		$data['merk'] = $this->merk_beras->order_by(array('status' => '1', 'nama' => 'asc'))->as_array()->get_all();
		$data['status'] 	= array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['kemasan'] = $this->merk_beras_kemasan->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Merk Beras' => 'active', 'Kemasan' => 'active'));
        set_session('title', 'Kemasan Beras');
        set_activemenu('sub-master', 'menu-kemasan-beras');
		$this->render('master/v-merk-beras-kemasan', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->merk_beras->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->merk_beras->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/merk_beras');
	}

	public function submit_kemasan_beras(){
		$post = $this->input->post();
		$post['timestamp_update'] = setNewDateTime();
		$check = $this->merk_beras_kemasan->get_data_by($post['id_merk'], $post['kemasan']);
		if(!$post['id'] && !$check){
			# Insert Statement
			$result = $this->merk_beras_kemasan->insert($post);
			$kemasan = $this->db->insert_id();
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			if(!empty($post['id'])){
				$id = $post['id'];
			}else{
				$id = $check['id'];
			}
			unset($post['id']);
			$result = $this->merk_beras_kemasan->update($id, $post);
			$kemasan = $id;
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$log = array(
			'id_kemasan' => $kemasan,
			'tanggal' => $post['last_update'],
			'harga' => $post['harga'],
			'timestamp_update' => $post['timestamp_update'],
			'update_by' => get_session('auth')['id']
		);
		$this->merk_beras_harga->insert($log);
		$this->go('master/merk_beras/kemasan');
	}

	

	
}
