<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_sak extends MY_Controller_admin
{
	public $class_id = 'jn-sak';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_sak_model', 'jenis_sak');
	}

	public function index()
	{
		$data['jenis_sak'] = $this->jenis_sak->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Jenis Sak' => 'active'));
        set_session('title', 'Master Jenis Sak');
        set_activemenu('sub-master', 'menu-jenis-sak');
		$this->render('master/v-jenis-sak', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->jenis_sak->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->jenis_sak->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/jenis_sak');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->jenis_sak->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_sak->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_sak->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
