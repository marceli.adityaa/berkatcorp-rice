<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dryer extends MY_Controller_admin
{
	public $class_id = 'm-dry';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Master_dryer_model', 'master_dryer');
	}

	public function index()
	{
		$data['list'] = $this->master_dryer->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Dryer' => 'active'));
        set_session('title', 'Master Dryer');
        set_activemenu('sub-master', 'menu-master-dryer');
		$this->render('master/v-master-dryer', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_dryer->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_dryer->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/dryer');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->master_dryer->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_dryer->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_dryer->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
