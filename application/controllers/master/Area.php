<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends MY_Controller_admin
{
	public $class_id = 'area';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('area_model', 'area');
	}

	public function index()
	{
		$data['area'] = $this->area->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Area' => 'active'));
		set_session('title', 'Master Area');
		set_activemenu('sub-master', 'menu-area');
		$this->render('master/v-area', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->area->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->area->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/area');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->area->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->area->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->area->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
