<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_bahan extends MY_Controller_admin
{
	public $class_id = 'jn-bhn';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_bahan_model', 'jenis_bahan');
	}

	public function index()
	{
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('jn-bhn', $hak->bas) || $hak->bas[0] == '*') {
            $data['jenis_bahan'] = $this->jenis_bahan->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Jenis Bahan' => 'active'));
            set_session('title', 'Master Jenis jenis-bahan');
            set_activemenu('sub-master', 'menu-jenis-bahan');
            $this->render('master/v-jenis-bahan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }		
	}

	public function submit_form(){
        $post = $this->input->post();
        if(!isset($post['gilingan'])){
            $post['gilingan'] = 0;
        }
        if(!isset($post['kebian'])){
            $post['kebian'] = 0;
        }

		if(!$post['id']){
			# Insert Statement
			$result = $this->jenis_bahan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->jenis_bahan->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/jenis_bahan');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->jenis_bahan->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_bahan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->jenis_bahan->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
