<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kadar_air extends MY_Controller_admin
{
	public $class_id = 'kda';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kadar_air_model', 'kadar_air');
	}

	public function index()
	{
		$data['kadar_air'] = $this->kadar_air->order_by(array('status' => 'desc', 'kadar_air' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Kadar Air' => 'active'));
        set_session('title', 'Master Kadar Air');
        set_activemenu('sub-master', 'menu-kadar-air');
		$this->render('master/v-kadar-air', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->kadar_air->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->kadar_air->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/kadar_air');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->kadar_air->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->kadar_air->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->kadar_air->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
