<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends MY_Controller_admin
{
	public $class_id = 'cust';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_model', 'customer');
		$this->load->model('area_model', 'area');
	}

	public function index()
	{
		$data['area'] 		= $this->area->get_active();
		$data['customer'] 	= $this->customer->get_data();
		$data['jenis'] 		= array(2 => 'Supplier', 3 => 'Customer');
		$data['status'] 	= array(1 => 'Aktif', 2 => 'Nonaktif');
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Customer' => 'active'));
        set_session('title', 'Master Customer');
        set_activemenu('sub-master', 'menu-customer');
		$this->render('master/v-customer', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!isset($post['supply_beras'])){
			$post['supply_beras'] = 0;
		}
		if(!isset($post['supply_gabah'])){
			$post['supply_gabah'] = 0;
		}
		if(!isset($post['is_supplier'])){
			$post['is_supplier'] = 0;
		}
		if(!isset($post['is_customer'])){
			$post['is_customer'] = 0;
		}
        $post['saldo_bon'] = intval(preg_replace('/[^\d.]/', '', $post['saldo_bon']));
        $post['limit_bon'] = intval(preg_replace('/[^\d.]/', '', $post['limit_bon']));
		if(!$post['id']){
			# Insert Statement
			$post['status'] = 1;
            $post['input_by'] = get_session('auth')['id'];
			$post['timestamp'] = setNewDateTime();
			$result = $this->customer->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->customer->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/customer');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->customer->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->customer->update($id, array('status' => 1));
		if($response){
			$this->message('Sukses mengubah data', 'success');
		}else{
			$this->message('Gagal', 'error');
		}
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->customer->update($id, array('status' => 0));
		if($response){
			$this->message('Sukses mengubah data', 'success');
		}else{
			$this->message('Gagal', 'error');
		}
		echo json_encode($response);
	}
}
