<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends MY_Controller_admin
{
	public $class_id = 'sales';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Master_sales_model', 'master_sales');
	}

	public function index()
	{
		$data['sales'] = $this->master_sales->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Sales' => 'active'));
		set_session('title', 'Master Sales');
		set_activemenu('sub-master', 'menu-sales');
		$this->render('master/v-master-sales', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_sales->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_sales->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/sales');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->master_sales->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_sales->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_sales->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
