<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pic extends MY_Controller_admin
{
	public $class_id = 'pic';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('penanggung_jawab_model', 'penanggung_jawab');
	}

	public function index()
	{
		$data['penanggung_jawab'] = $this->penanggung_jawab->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Penanggung Jawab' => 'active'));
        set_session('title', 'Master Penanggung Jawab');
        set_activemenu('sub-master', 'menu-penanggung-jawab');
		$this->render('master/v-penanggung-jawab', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->penanggung_jawab->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->penanggung_jawab->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/pic');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->penanggung_jawab->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
