<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Akun extends MY_Controller_admin
{
	public $bypass_auth = true;

	public function __construct()
	{
		parent::__construct();
        $this->load->model('master_akun_biaya_model', 'master_akun_biaya');
		$this->load->model('master_akun_biaya_detail_model', 'master_akun_biaya_detail');
        $this->load->model('master_akun_pendapatan_model', 'master_akun_pendapatan');
	}
    
    public function biaya()
    {
		$data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_akun_biaya->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Akun Biaya' => 'active'));
        set_session('title', 'Master Data - Akun Biaya');
        set_activemenu('sub-master', 'menu-akun-biaya');
		$this->render('master/v-akun-biaya', $data);
    }

	public function biaya_detail($id)
    {
		$data['biaya'] = $this->master_akun_biaya->get_data($id);
		$data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_akun_biaya_detail->get_data_detail($id);
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Akun Biaya' => base_url('master/akun/biaya'), $data['biaya']['nama'] => 'active'));
        set_session('title', 'Master Data - Detail Akun Biaya');
        set_activemenu('sub-master', 'menu-akun-biaya');
		$this->render('master/v-akun-biaya-detail', $data);
    }

    public function pendapatan()
    {
        $data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_akun_pendapatan->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Akun Pendapatan' => 'active'));
        set_session('title', 'Master Data - Akun Pendapatan');
        set_activemenu('sub-master', 'menu-akun-pendapatan');
		$this->render('master/v-akun-pendapatan', $data);
    }

	public function submit_form_biaya(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_akun_biaya->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_akun_biaya->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/akun/biaya');
	}

	public function submit_form_biaya_detail(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_akun_biaya_detail->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_akun_biaya_detail->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/akun/biaya_detail/'.$post['id_biaya']);
	}

	public function submit_form_pendapatan(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_akun_pendapatan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_akun_pendapatan->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/akun/pendapatan');
    }
    
    public function json_get_biaya_detail(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_biaya_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_biaya_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya->update($id, array('status' => 0));
		echo json_encode($response);
    }

	public function json_get_biayadetail(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya_detail->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_biayadetail_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya_detail->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_biayadetail_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_biaya_detail->update($id, array('status' => 0));
		echo json_encode($response);
    }
    
    public function json_get_pendapatan_detail(){
		$id = $this->input->post('id');
		$response = $this->master_akun_pendapatan->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_pendapatan_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_pendapatan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_pendapatan_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_akun_pendapatan->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
