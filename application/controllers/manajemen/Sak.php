<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Sak extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('kll-sak', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['transaksi'] = $this->user->join($this->transaksi_sak->get_data_sak(), 'submit_by', 'input_by');
            $data['arus'] = array("in", "out");
            $data['jenis_sak'] = $this->jenis_sak->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Sak' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Kelola Sak');
            set_activemenu('sub-manajemen-sak', 'menu-kelola-sak');
            $this->render('manajemen/sak/v-kelola-transaksi-sak', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-sak', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            $data['transaksi'] = $this->user->join($this->user->join($this->transaksi_sak->get_data_pengajuan(), 'submit_by', 'input_by'), 'acc_by', 'acc_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Sak' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Transaksi Sak');
            set_activemenu('sub-manajemen-sak', 'menu-persetujuan-sak');
            $this->render('manajemen/sak/v-persetujuan-transaksi-sak', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function stok()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rkp-sak', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_primary();
            if (empty($this->input->get())) {
                if(empty($_GET['akun'])){
                    $_GET['akun'] = $data['customer'][0]['id'];
                }
            }
            $data['stok'] = $this->transaksi_sak->get_data_stok($_GET['akun']);
            $data['jenis_sak'] = $this->jenis_sak->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Sak' => 'active', 'Stok Sak' => 'active'));
            set_session('title', 'Stok Sak');
            set_activemenu('sub-manajemen-sak', 'menu-stok-sak');
            $this->render('manajemen/sak/v-stok-sak', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function kartu()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('krt-sak', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_non_primary();
            if (empty($this->input->get())) {
                if(empty($_GET['akun'])){
                    $_GET['akun'] = $data['customer'][0]['id'];
                }
            }
            $data['stok'] = $this->transaksi_sak->get_data_stok($_GET['akun']);
            $data['jenis_sak'] = $this->jenis_sak->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Sak' => 'active', 'Kartu Sak Supplier' => 'active'));
            set_session('title', 'Kartu Sak Supplier');
            set_activemenu('sub-manajemen-sak', 'menu-kartu-sak');
            $this->render('manajemen/sak/v-kartu-sak', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_transaksi_sak()
    {
        $post = $this->input->post('data');
        $id = $this->input->post('id_transaksi');
        // dump($post);
        if(empty($id)){
            # Transaksi baru
            $trx['kode_transaksi'] = $this->generate_kode_sak($post['tgl_transaksi']);
            $trx['tgl_transaksi'] = $post['tgl_transaksi'];
            $trx['source'] = 'master-transaksi';
            $trx['id_source'] = 0;
            $trx['id_customer_from'] = $post['id_customer_from'];
            $trx['id_customer_to'] = $post['id_customer_to'];
            $trx['is_record_from'] = $post['is_record_from'];
            $trx['is_record_to'] = $post['is_record_to'];
            $trx['arus'] = $post['arus'];
            $trx['input_by'] = get_session('auth')['id'];
            $trx['timestamp_input'] = setNewDateTime();
            $result_trx = $this->transaksi_sak->insert($trx);
            if($result_trx){
                $id_trx = $this->db->insert_id();
                $cust_from = $this->customer->get_data($post['id_customer_from']);
                $cust_to = $this->customer->get_data($post['id_customer_to']);
                foreach($post['jenis_sak'] as $row){
                    if($post['arus'] == 'in'){
                        # Barang masuk
                        $trx_detail_from['id_transaksi'] = $id_trx;
                        $trx_detail_from['id_jenis_sak'] = $row['id_jenis_sak'];
                        $trx_detail_from['jumlah'] = $row['jumlah'];
                        $trx_detail_from['status_acc'] = 2;
                        $trx_detail_from['id_customer'] = $post['id_customer_from'];
                        $trx_detail_from['is_record'] = $post['is_record_from'];
                        if($cust_from['is_primary'] == 1){
                            $trx_detail_from['arus'] = 'out';
                        }else{
                            $trx_detail_from['arus'] = 'in';
                        }
                        $this->transaksi_sak_detail->insert($trx_detail_from);

                        if($post['id_customer_from'] != $post['id_customer_to']){
                            $trx_detail_to['id_transaksi'] = $id_trx;
                            $trx_detail_to['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail_to['jumlah'] = $row['jumlah'];
                            $trx_detail_to['status_acc'] = 2;
                            $trx_detail_to['id_customer'] = $post['id_customer_from'];
                            $trx_detail_to['is_record'] = $post['is_record_from'];
                            $trx_detail_to['id_customer'] = $post['id_customer_to'];
                            $trx_detail_to['is_record'] = $post['is_record_to'];
                            $trx_detail_to['arus'] = 'in';
                            $this->transaksi_sak_detail->insert($trx_detail_to);
                        }
                        
                    }else{
                        # Barang keluar
                        $trx_detail_from['id_transaksi'] = $id_trx;
                        $trx_detail_from['id_jenis_sak'] = $row['id_jenis_sak'];
                        $trx_detail_from['jumlah'] = $row['jumlah'];
                        $trx_detail_from['status_acc'] = 2;
                        $trx_detail_from['id_customer'] = $post['id_customer_from'];
                        $trx_detail_from['is_record'] = $post['is_record_from'];
                        $trx_detail_from['arus'] = 'out';
                        
                        $this->transaksi_sak_detail->insert($trx_detail_from);

                        if($post['id_customer_from'] != $post['id_customer_to']){
                            $trx_detail_to['id_transaksi'] = $id_trx;
                            $trx_detail_to['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail_to['jumlah'] = $row['jumlah'];
                            $trx_detail_to['status_acc'] = 2;
                            $trx_detail_to['id_customer'] = $post['id_customer_from'];
                            $trx_detail_to['is_record'] = $post['is_record_from'];
                            $trx_detail_to['id_customer'] = $post['id_customer_to'];
                            $trx_detail_to['is_record'] = $post['is_record_to'];
                            if($cust_to['is_primary'] == 1){
                                $trx_detail_from['arus'] = 'in';
                            }else{
                                $trx_detail_to['arus'] = 'out';
                            }
                            $this->transaksi_sak_detail->insert($trx_detail_to);
                        }
                    }
                }
            }
            if ($result_trx == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            echo json_encode($result_trx);
        }else{
            # Update transaksi
            $trx['tgl_transaksi'] = $post['tgl_transaksi'];
            $trx['id_customer_from'] = $post['id_customer_from'];
            $trx['id_customer_to'] = $post['id_customer_to'];
            $trx['is_record_from'] = $post['is_record_from'];
            $trx['is_record_to'] = $post['is_record_to'];
            $trx['arus'] = $post['arus'];
            $trx['input_by'] = get_session('auth')['id'];
            $trx['timestamp_input'] = setNewDateTime();
            $update = $this->transaksi_sak->update($id, $trx);
            if($update){
                $id_trx = $id;
                $cust_from = $this->customer->get_data($post['id_customer_from']);
                $cust_to = $this->customer->get_data($post['id_customer_to']);
                $delete = $this->transaksi_sak_detail->delete_by_transaksi($id_trx);
                if($delete){
                    foreach($post['jenis_sak'] as $row){
                        if($post['arus'] == 'in'){
                            # Barang masuk
                            $trx_detail_from['id_transaksi'] = $id_trx;
                            $trx_detail_from['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail_from['jumlah'] = $row['jumlah'];
                            $trx_detail_from['status_acc'] = 2;
                            $trx_detail_from['id_customer'] = $post['id_customer_from'];
                            $trx_detail_from['is_record'] = $post['is_record_from'];
                            if($cust_from['is_primary'] == 1){
                                $trx_detail_from['arus'] = 'out';
                            }else{
                                $trx_detail_from['arus'] = 'in';
                            }
                            $this->transaksi_sak_detail->insert($trx_detail_from);

                            if($post['id_customer_from'] != $post['id_customer_to']){
                                $trx_detail_to['id_transaksi'] = $id_trx;
                                $trx_detail_to['id_jenis_sak'] = $row['id_jenis_sak'];
                                $trx_detail_to['jumlah'] = $row['jumlah'];
                                $trx_detail_to['status_acc'] = 2;
                                $trx_detail_to['id_customer'] = $post['id_customer_from'];
                                $trx_detail_to['is_record'] = $post['is_record_from'];
                                $trx_detail_to['id_customer'] = $post['id_customer_to'];
                                $trx_detail_to['is_record'] = $post['is_record_to'];
                                $trx_detail_to['arus'] = 'in';
                                $this->transaksi_sak_detail->insert($trx_detail_to);
                            }
                            
                        }else{
                            # Barang keluar
                            $trx_detail_from['id_transaksi'] = $id_trx;
                            $trx_detail_from['id_jenis_sak'] = $row['id_jenis_sak'];
                            $trx_detail_from['jumlah'] = $row['jumlah'];
                            $trx_detail_from['status_acc'] = 2;
                            $trx_detail_from['id_customer'] = $post['id_customer_from'];
                            $trx_detail_from['is_record'] = $post['is_record_from'];
                            $trx_detail_from['arus'] = 'out';
                            
                            $this->transaksi_sak_detail->insert($trx_detail_from);

                            if($post['id_customer_from'] != $post['id_customer_to']){
                                $trx_detail_to['id_transaksi'] = $id_trx;
                                $trx_detail_to['id_jenis_sak'] = $row['id_jenis_sak'];
                                $trx_detail_to['jumlah'] = $row['jumlah'];
                                $trx_detail_to['status_acc'] = 2;
                                $trx_detail_to['id_customer'] = $post['id_customer_from'];
                                $trx_detail_to['is_record'] = $post['is_record_from'];
                                $trx_detail_to['id_customer'] = $post['id_customer_to'];
                                $trx_detail_to['is_record'] = $post['is_record_to'];
                                if($cust_to['is_primary'] == 1){
                                    $trx_detail_from['arus'] = 'in';
                                }else{
                                    $trx_detail_to['arus'] = 'out';
                                }
                                $this->transaksi_sak_detail->insert($trx_detail_to);
                            }
                        }
                    }
                
                }
            }
            if ($update == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            echo json_encode($update);
        }
        
    }

    public function delete_data_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $result = $this->transaksi_sak_detail->delete_by_transaksi($id);
            if($result){
                $result = $this->transaksi_sak->delete($id);
            }
        }

        if ($result) {
            $this->message('Berhasil menghapus data.', 'success');
        } else {
            $this->message('Gagal menghapus data', 'error');
        }
        echo json_encode($result);
    }

    public function approve_transaksi_sak()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'status_acc' => 2,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime(),
            );
            $response = $this->transaksi_sak_detail->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi disetujui.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function decline_transaksi_sak()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'status_acc' => 3,
                'acc_by' => get_session('auth')['id'],
                'timestamp_acc' => setNewDateTime(),
            );
            $response = $this->transaksi_sak_detail->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $data['trx'] = $this->transaksi_sak->get_data($id);
            $data['detail'] = $this->transaksi_sak_detail->get_data_edit_transaksi($id);
            echo json_encode($data);
        }else{
            echo json_encode(false);
        }
    }

    public function generate_kode_sak($tgl)
    {
        $last = $this->transaksi_sak->get_last_kode($tgl);
        $index = 1;
        // $last = '1-191001-001';
        if ($last != false) {
            $temp = explode('.', $last);
            $index = intval($temp[2]) + 1;
        }
        $kode = 'SAK.' . date("ymd", strtotime($tgl)) . '.' . zerofy($index, 3);
        return $kode;
    }

    public function export_transaksi()
    {
        $transaksi = $this->user->join($this->transaksi_sak->get_data_sak(), 'submit_by', 'input_by');
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nama');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Sak');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Masuk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Keluar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kode_transaksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_transaksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['source']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['customer']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_sak']);
                if($row['arus'] == 'in'){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jumlah']);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jumlah']);
                }
                
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_transaksi_sak_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }
}
