<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Hutang extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
        $this->load->model('customer_model', 'customer');
        $this->load->model('hutang_model', 'hutang');
        $this->load->model('hutang_detail_model', 'hutang_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'master-transaksi-hutang';
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('htg-kll', $hak->bas) || $hak->bas[0] == '*') {
            if(empty($_GET['status'])){
                $_GET['status'] = 0;
            }
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role']       = $this->role;
            $data['customer']   = $this->customer->get_active();
            $data['exist']      = $this->hutang->get_data();
            if(!empty($data['exist'])){
                foreach($data['exist'] as $row){
                    foreach($data['customer'] as $key => $val){
                        if($row['id_customer'] == $val['id']){
                            unset($data['customer'][$key]);
                            break;
                        }
                    }
                }
            }

            if (!empty($this->input->get())) {
                $data['hutang'] = $this->user->join($this->hutang->get_kelola_hutang(), 'submit_by', 'input_by');
            }else{
                $data['hutang'] = array();
            }
            $data['status'] = array( 1 => "Belum Lunas", 2 => "Lunas");
            // dump($data['hutang']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Hutang' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Kelola Hutang');
            set_activemenu('sub-hutang', 'menu-hutang-kelola');
            $this->render('manajemen/hutang/v-kelola-transaksi-hutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('htg-kll', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $data['kas']        = json_decode(xcurl($param_kas)['response']);
                $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
                $data['pembayaran'] = array('cash', 'transfer', 'tbd');
                $data['role'] = $this->role;
                $data['customer'] = $this->customer->get_active();
                $data['hutang'] = $this->user->join($this->hutang->get_kelola_hutang($id), 'submit_by', 'input_by');
                $data['detail'] = $this->kas->join($this->user->join($this->hutang_detail->get_hutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
                // dump($data['hutang']);
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Kelola Transaksi' => base_url('manajemen/hutang/kelola'), 'Detail hutang' => 'active'));
                set_session('title', 'Kelola hutang');
                set_activemenu('sub-hutang', 'menu-hutang-kelola');
                $this->render('manajemen/hutang/v-detail-transaksi-hutang', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
        
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('htg-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['detail'] = $this->kas->join($this->user->join($this->hutang_detail->get_persetujuan_hutang(), 'submit_by', 'input_by'), 'kas', 'id_kas');
            $data['status'] = array(1 => "Menunggu", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Hutang' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Transaksi');
            set_activemenu('sub-hutang', 'menu-hutang-persetujuan');
            $this->render('manajemen/hutang/v-persetujuan-hutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('htg-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['hutang'] = $this->user->join($this->hutang->get_rekap_hutang()  , 'submit_by', 'input_by');
            $data['status'] = array( 1 => "Belum Lunas", 2 => "Lunas");
            // dump($data['hutang']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Hutang' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Rekap Hutang');
            set_activemenu('sub-hutang', 'menu-hutang-rekap');
            $this->render('manajemen/hutang/v-rekap-transaksi-hutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_rekap($id)
    {
        # BELOM SELESAI 
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('htg-rkp', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $data['hutang'] = $this->user->join($this->hutang->get_kelola_hutang($id), 'submit_by', 'input_by');
                $data['detail'] = $this->kas->join($this->user->join($this->hutang_detail->get_rekap_hutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Rekap Hutang' => base_url('manajemen/hutang/rekap'), 'Detail Transaksi' => 'active'));
                set_session('title', 'Detail Rekap Hutang');
                set_activemenu('sub-hutang', 'menu-hutang-rekap');
                $this->render('manajemen/hutang/v-rekap-transaksi-hutang-detail', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
        
    }

    public function submit_hutang()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert Statement
            $hutang['id_customer'] = $post['id_customer'];
            $hutang['tgl_hutang'] = $post['tgl_hutang'];
            $hutang['nominal_hutang'] = str_replace(",", "", $post['nominal']);
            $hutang['keterangan'] = $post['keterangan'];
            $hutang['input_by'] = get_session('auth')['id'];
            $hutang['timestamp_input'] = setNewDateTime();
            $result = $this->hutang->insert($hutang);
            if($result){
                $id = $this->db->insert_id();
                $detail['id_hutang'] = $id;
                $detail['source'] = $this->source;
                $detail['tgl_transaksi'] = $post['tgl_hutang'];
                $detail['tambah_hutang'] = str_replace(",", "", $post['nominal']);
                $detail['sisa_pokok'] = str_replace(",", "", $post['nominal']);
                $detail['remark'] = 'transaksi awal';
                $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
                if($post['jenis_pembayaran'] == 'cash'){
                    $detail['id_kas'] = $post['id_kas'];
                }else if($post['jenis_pembayaran'] == 'transfer'){
                    $detail['id_kas'] = $post['id_rekening'];
                }else{
                    $detail['id_kas'] = 0;
                }
                $detail['input_by'] = get_session('auth')['id'];
                $detail['timestamp_input'] = setNewDateTime();
                $result = $this->hutang_detail->insert($detail);
            }
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            $this->go('manajemen/hutang/detail/'.$id);
        }else{
            # Update Statement
            $hutang['id_customer'] = $post['id_customer'];
            $hutang['tgl_hutang'] = $post['tgl_hutang'];
            $hutang['nominal_hutang'] = str_replace(",", "", $post['nominal']);
            $hutang['keterangan'] = $post['keterangan'];
            $result = $this->hutang->update($post['id'], $hutang);
            if($result){
                $detail['tgl_transaksi'] = $post['tgl_hutang'];
                $detail['tambah_hutang'] = str_replace(",", "", $post['nominal']);
                $detail['sisa_pokok'] = str_replace(",", "", $post['nominal']);
                $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
                if($post['jenis_pembayaran'] == 'cash'){
                    $detail['id_kas'] = $post['id_kas'];
                }else if($post['jenis_pembayaran'] == 'transfer'){
                    $detail['id_kas'] = $post['id_rekening'];
                }else{
                    $detail['id_kas'] = 0;
                }
                $detail['input_by'] = get_session('auth')['id'];
                $detail['timestamp_input'] = setNewDateTime();
                $result = $this->hutang_detail->update($post['id_detail'], $detail);
            }
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            $this->go('manajemen/hutang/kelola?'.$url);
        }        
    }

    public function approve_transaksi_hutang()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $detail = $this->hutang_detail->get_data($id);
            $hutang_verif = $this->hutang_detail->get_data_hutang_verified($detail['id_hutang'], $id);
            if($hutang_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_hutang = 0;
                $bayar_pokok = 0;
                foreach($hutang_verif as $row){
                    $total_hutang += $row['tambah_hutang'];
                    $bayar_pokok += $row['bayar_pokok'];
                }
                $sisa = $total_hutang - $bayar_pokok;
                $update['pokok_awal'] = $sisa;
                $update['sisa_pokok'] = $sisa - $detail['bayar_pokok'] + $detail['tambah_hutang'];
            }else{
                # Belum ada riwayat transaksi yg terverifikasi
                $total_hutang = 0;
                $update['pokok_awal'] = 0;
                $update['sisa_pokok'] = $detail['tambah_hutang'] - $detail['bayar_pokok'];
            }
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->hutang_detail->update($id, $update);
            if($response){
                # record ke arus kas
                if($detail['tambah_hutang'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Tambah hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['tambah_hutang'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_pokok'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Pokok Hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_pokok'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_bunga'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Bunga Hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_bunga'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        echo json_encode($response);
    }

    public function submit_form_approval(){
        $post = $this->input->post();
        $id = $post['id'];
        $url = $post['url'];
        if(!empty($id)){
            # Update statement
            $update_detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $update_detail['tambah_hutang'] = str_replace(",", "", $post['tambah_hutang']);
            $update_detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $update_detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $update_detail['remark'] = $post['remark'];
            $update_detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $update_detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $update_detail['id_kas'] = $post['id_rekening'];
            }else{
                $update_detail['id_kas'] = 0;
            }
            $update_detail['input_by'] = get_session('auth')['id'];
            $update_detail['timestamp_input'] = setNewDateTime();
            $result = $this->hutang_detail->update($id, $update_detail);

            # Get new data
            $detail = $this->hutang_detail->get_data($id);
            $hutang_verif = $this->hutang_detail->get_data_hutang_verified($detail['id_hutang'], $id);
            if($hutang_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_hutang = 0;
                $bayar_pokok = 0;
                foreach($hutang_verif as $row){
                    $total_hutang += $row['tambah_hutang'];
                    $bayar_pokok += $row['bayar_pokok'];
                }
                $sisa = $total_hutang - $bayar_pokok;
                $update['pokok_awal'] = $sisa;
                $update['sisa_pokok'] = $sisa - $detail['bayar_pokok'] + $detail['tambah_hutang'];
            }else{
                # Belum ada riwayat transaksi yg terverifikasi
                $total_hutang = 0;
                $update['pokok_awal'] = 0;
                $update['sisa_pokok'] = $detail['tambah_hutang'] - $detail['bayar_pokok'];
            }
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->hutang_detail->update($id, $update);

             # cek status
             $cek = $this->hutang->get_kelola_hutang($detail['id_hutang']);
             if($cek['sisa_pokok'] == 0){
                 $this->hutang->update($detail['id_hutang'], array('is_paid_off' => 1));
             }else{
                 $this->hutang->update($detail['id_hutang'], array('is_paid_off' => 0));
             }

            if($response){
                # record ke arus kas
                if($detail['tambah_hutang'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Tambah hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['tambah_hutang'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_pokok'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Pokok Hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_pokok'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_bunga'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Bunga Hutang';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_bunga'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        $this->go('manajemen/hutang/persetujuan?'.$url);
    }

    public function decline_transaksi_hutang()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'is_verifikasi' => 2,
                'verifikasi_by' => get_session('auth')['id'],
                'timestamp_verifikasi' => setNewDateTime(),
            );
            $response = $this->hutang_detail->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function submit_angsuran()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert statement
            $detail['source'] = $this->source;
            $detail['id_hutang'] = $post['id_hutang'];
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->hutang_detail->insert($detail);
            if ($result) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update statement
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->hutang_detail->update($post['id'], $detail);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }
        $this->go('manajemen/hutang/detail/'.$post['id_hutang']);
    }

    public function submit_tambah_hutang()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert statement
            $detail['source'] = $this->source;
            $detail['id_hutang'] = $post['id_hutang'];
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['tambah_hutang'] = str_replace(",", "", $post['tambah_hutang']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->hutang_detail->insert($detail);
            if ($result) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update statement
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['tambah_hutang'] = str_replace(",", "", $post['tambah_hutang']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->hutang_detail->update($post['id'], $detail);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }
        $this->go('manajemen/hutang/detail/'.$post['id_hutang']);
    }

    public function get_sisa_pokok()
    {
        $id = $this->input->post('id');
        $hutang_verif = $this->hutang_detail->get_data_hutang_verified($id, '');
        $sisa_pokok = 0;
        if($hutang_verif != false){
            # Ada riwayat transaksi yg terverifikasi
            $total_hutang = 0;
            $bayar_pokok = 0;
            foreach($hutang_verif as $row){
                $total_hutang += $row['tambah_hutang'];
                $bayar_pokok += $row['bayar_pokok'];
            }
            $sisa_pokok = $total_hutang - $bayar_pokok;
        }

        echo json_encode($sisa_pokok);
    }

    public function json_get_data_hutang()
    {
        $id = $this->input->post('id');
        $response = $this->hutang->get_data_hutang_awal($id);
        echo json_encode($response);
    }

    public function get_detail_hutang()
    {
        $id = $this->input->post('id');
        $response = $this->hutang_detail->get_data($id);
        echo json_encode($response);
    }

    public function delete_data_hutang()
    {
        $id = $this->input->post('id');
        $is_verif = $this->hutang_detail->cek_data_validasi($id);
        if($is_verif){
            # Decline action
            $this->message('Gagal menghapus data. Data transaksi sudah disetujui.', 'warning');
        }else{
            # Delete transaksi
            $result = $this->hutang_detail->delete_by_transaksi($id);
            if($result){
                $this->hutang->delete($id);
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }
    }

    public function delete_data_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $result = $this->hutang_detail->delete($id);
            if($result){
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }else{
            $result = false;
            $this->message('Gagal.', 'danger');
        }
        echo json_encode($result);
    }

    public function export_rekap()
    {
        $transaksi = $this->user->join($this->hutang->get_rekap_hutang(), 'submit_by', 'input_by'); 
        // dump($transaksi);
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pemberi Hutang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Status');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Dibuat');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Terakhir Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Hutang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Angsuran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kreditur']);
                if($row['is_paid_off'] == 1){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Lunas');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Belum Lunas');
                    
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_hutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_terakhir_bayar']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['total_hutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['sisa_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['angsuran']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['keterangan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_hutang_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }

    public function export_detail($id)
    {
        $transaksi = $this->kas->join($this->user->join($this->hutang_detail->get_rekap_hutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
        $client = "";
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pemberi Hutang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pembayaran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tambah Hutang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bayar Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bayar Bunga');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pokok Awal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Catatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $client = preg_replace('/\s+/', '', $row['kreditur']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kreditur']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_transaksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['source']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_pembayaran']);
                if(isset($row['kas_is_rekening'])){
                    if($row['kas_is_rekening'] == 1){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kas_bank'].'-'.$row['kas_no_rek']));
                        
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kas_kas']);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tambah_hutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['bayar_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['bayar_bunga']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['pokok_awal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['sisa_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['remark']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_hutang_'.$client.'_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }
}
