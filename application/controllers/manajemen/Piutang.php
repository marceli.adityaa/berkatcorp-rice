<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Piutang extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
        $this->load->model('customer_model', 'customer');
        $this->load->model('piutang_model', 'piutang');
        $this->load->model('piutang_detail_model', 'piutang_detail');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'master-transaksi-uangmuka';
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('piut-kll', $hak->bas) || $hak->bas[0] == '*') {
            if(empty($_GET['status'])){
                $_GET['status'] = 0;
            }

            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['exist'] = $this->piutang->get_data();
            if(!empty($data['exist'])){
                foreach($data['exist'] as $row){
                    foreach($data['customer'] as $key => $val){
                        if($row['id_customer'] == $val['id']){
                            unset($data['customer'][$key]);
                            break;
                        }
                    }
                }
            }

            if(!empty($this->input->get())){
                $data['piutang'] = $this->user->join($this->piutang->get_kelola_piutang(), 'submit_by', 'input_by');
            }else{
                $data['piutang'] = array();
            }
            $data['jenis_piutang'] = array('panjang', 'pendek');
            $data['status'] = array( 1 => "Belum Lunas", 2 => "Lunas");
            // dump($data['piutang']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Uang Muka' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Kelola Uang Muka / Bon');
            set_activemenu('sub-piutang', 'menu-piutang-kelola');
            $this->render('manajemen/piutang/v-kelola-transaksi-piutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('piut-kll', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $data['kas']        = json_decode(xcurl($param_kas)['response']);
                $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
                $data['pembayaran'] = array('cash', 'transfer', 'tbd');
                $data['role'] = $this->role;
                $data['jenis_piutang'] = array('panjang', 'pendek');
                $data['customer'] = $this->customer->get_active();
                $data['piutang'] = $this->user->join($this->piutang->get_kelola_piutang($id), 'submit_by', 'input_by');
                $data['detail'] = $this->kas->join($this->user->join($this->piutang_detail->get_piutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
                // dump($data['piutang']);
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Kelola Transaksi' => base_url('manajemen/piutang/kelola'), 'Detail Uang Muka' => 'active'));
                set_session('title', 'Kelola Uang Muka / Bon');
                set_activemenu('sub-piutang', 'menu-piutang-kelola');
                $this->render('manajemen/piutang/v-detail-transaksi-piutang', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
        
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('piut-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            $data['jenis_piutang'] = array('panjang', 'pendek');
            $data['customer'] = $this->customer->get_active();
            $data['detail'] = $this->kas->join($this->user->join($this->piutang_detail->get_persetujuan_piutang(), 'submit_by', 'input_by'), 'kas', 'id_kas');
            // dump($data['detail']);
            $data['status'] = array(1 => "Menunggu", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Uang Muka' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Transaksi');
            set_activemenu('sub-piutang', 'menu-piutang-persetujuan');
            $this->render('manajemen/piutang/v-persetujuan-piutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('piut-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            $data['piutang'] = $this->user->join($this->piutang->get_rekap_piutang(), 'submit_by', 'input_by');
            $data['jenis_piutang'] = array('panjang', 'pendek');
            $data['status'] = array( 1 => "Belum Lunas", 2 => "Lunas");
            // dump($data['piutang']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Uang Muka' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Rekap Uang Muka');
            set_activemenu('sub-piutang', 'menu-piutang-rekap');
            $this->render('manajemen/piutang/v-rekap-transaksi-piutang', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_rekap($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('piut-rkp', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $data['piutang'] = $this->user->join($this->piutang->get_kelola_piutang($id), 'submit_by', 'input_by');
                $data['detail'] = $this->kas->join($this->user->join($this->piutang_detail->get_rekap_piutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Rekap Uang Muka' => base_url('manajemen/piutang/rekap'), 'Detail Transaksi' => 'active'));
                set_session('title', 'Detail Rekap Uang Muka');
                set_activemenu('sub-piutang', 'menu-piutang-rekap');
                $this->render('manajemen/piutang/v-rekap-transaksi-piutang-detail', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
        
    }

    public function submit_piutang()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert Statement
            $piutang['id_customer'] = $post['id_customer'];
            $piutang['tgl_piutang'] = $post['tgl_piutang'];
            // $piutang['jenis_piutang'] = $post['jenis_piutang'];
            $piutang['nominal_piutang'] = str_replace(",", "", $post['nominal']);
            $piutang['keterangan'] = $post['keterangan'];
            $piutang['input_by'] = get_session('auth')['id'];
            $piutang['timestamp_input'] = setNewDateTime();
            $result = $this->piutang->insert($piutang);
            if($result){
                $id = $this->db->insert_id();
                $detail['id_piutang'] = $id;
                $detail['source'] = $this->source;
                $detail['tgl_transaksi'] = $post['tgl_piutang'];
                $detail['tambah_piutang'] = str_replace(",", "", $post['nominal']);
                // $detail['sisa_pokok'] = str_replace(",", "", $post['nominal']);
                $detail['remark'] = $post['keterangan'].' - transaksi awal';
                $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
                if($post['jenis_pembayaran'] == 'cash'){
                    $detail['id_kas'] = $post['id_kas'];
                }else if($post['jenis_pembayaran'] == 'transfer'){
                    $detail['id_kas'] = $post['id_rekening'];
                }else{
                    $detail['id_kas'] = 0;
                }
                $detail['input_by'] = get_session('auth')['id'];
                $detail['timestamp_input'] = setNewDateTime();
                $result = $this->piutang_detail->insert($detail);
            }
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            $this->go('manajemen/piutang/detail/'.$id);
        }else{
            # Update Statement
            $piutang['id_customer'] = $post['id_customer'];
            $piutang['tgl_piutang'] = $post['tgl_piutang'];
            $piutang['jenis_piutang'] = $post['jenis_piutang'];
            $piutang['nominal_piutang'] = str_replace(",", "", $post['nominal']);
            $piutang['keterangan'] = $post['keterangan'];
            $result = $this->piutang->update($post['id'], $piutang);
            if($result){
                $detail['tgl_transaksi'] = $post['tgl_piutang'];
                $detail['tambah_piutang'] = str_replace(",", "", $post['nominal']);
                // $detail['sisa_pokok'] = str_replace(",", "", $post['nominal']);
                $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
                if($post['jenis_pembayaran'] == 'cash'){
                    $detail['id_kas'] = $post['id_kas'];
                }else if($post['jenis_pembayaran'] == 'transfer'){
                    $detail['id_kas'] = $post['id_rekening'];
                }else{
                    $detail['id_kas'] = 0;
                }
                $detail['input_by'] = get_session('auth')['id'];
                $detail['timestamp_input'] = setNewDateTime();
                $result = $this->piutang_detail->update($post['id_detail'], $detail);
            }
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
            $this->go('manajemen/piutang/kelola?'.$url);
        }        
    }

    public function approve_transaksi_piutang()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $detail = $this->piutang_detail->get_data($id);
            $piutang_verif = $this->piutang_detail->get_data_piutang_verified($detail['id_piutang'], $id);
            if($piutang_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_piutang = 0;
                $bayar_pokok = 0;
                foreach($piutang_verif as $row){
                    $total_piutang += $row['tambah_piutang'];
                    $bayar_pokok += $row['bayar_pokok'];
                }
                $sisa = $total_piutang - $bayar_pokok;
                $update['pokok_awal'] = $sisa;
                $update['sisa_pokok'] = $sisa - $detail['bayar_pokok'] + $detail['tambah_piutang'];
            }else{
                # Belum ada riwayat transaksi yg terverifikasi
                $total_piutang = 0;
                $update['pokok_awal'] = 0;
                $update['sisa_pokok'] = $detail['tambah_piutang'] - $detail['bayar_pokok'];
            }
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->piutang_detail->update($id, $update);

            if($response){
                # record ke arus kas
                if($detail['tambah_piutang'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Tambah Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['tambah_piutang'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_pokok'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Pokok Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_pokok'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_bunga'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Bunga Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_bunga'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        echo json_encode($response);
    }

    public function submit_form_approval()
    {
        $post = $this->input->post();
        $id = $post['id'];
        $url = $post['url'];
        if(!empty($id)){
            # Update statement
            $update_detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $update_detail['tambah_piutang'] = str_replace(",", "", $post['tambah_piutang']);
            $update_detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $update_detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $update_detail['remark'] = $post['remark'];
            $update_detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $update_detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $update_detail['id_kas'] = $post['id_rekening'];
            }else{
                $update_detail['id_kas'] = 0;
            }
            $update_detail['input_by'] = get_session('auth')['id'];
            $update_detail['timestamp_input'] = setNewDateTime();
            $result = $this->piutang_detail->update($id, $update_detail);


            # Get new data
            $detail = $this->piutang_detail->get_data($id);
            $piutang_verif = $this->piutang_detail->get_data_piutang_verified($detail['id_piutang'], $id);
            if($piutang_verif != false){
                # Ada riwayat transaksi yg terverifikasi
                $total_piutang = 0;
                $bayar_pokok = 0;
                foreach($piutang_verif as $row){
                    $total_piutang += $row['tambah_piutang'];
                    $bayar_pokok += $row['bayar_pokok'];
                }
                $sisa = $total_piutang - $bayar_pokok;
                $update['pokok_awal'] = $sisa;
                $update['sisa_pokok'] = $sisa - $detail['bayar_pokok'] + $detail['tambah_piutang'];
            }else{
                # Belum ada riwayat transaksi yg terverifikasi
                $total_piutang = 0;
                $update['pokok_awal'] = 0;
                $update['sisa_pokok'] = $detail['tambah_piutang'] - $detail['bayar_pokok'];
            }
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->piutang_detail->update($id, $update);

            # cek status
            $cek = $this->piutang->get_kelola_piutang($detail['id_piutang']);
            if($cek['sisa_pokok'] == 0){
                $this->piutang->update($detail['id_piutang'], array('is_paid_off' => 1));
            }else{
                $this->piutang->update($detail['id_piutang'], array('is_paid_off' => 0));
            }
            
            if($response){
                # record ke arus kas
                if($detail['tambah_piutang'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "out";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Tambah Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['tambah_piutang'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_pokok'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Pokok Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_pokok'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }

                if($detail['bayar_bunga'] != 0){
                    $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                    $arus_kas['id_kas'] = $detail['id_kas'];
                    $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                    $arus_kas['arus'] = "in";
                    $arus_kas['sumber'] = $detail['source'];
                    $arus_kas['id_sumber'] = $id;
                    $arus_kas['keterangan'] = 'Bayar Bunga Uang Muka';
                    $arus_kas['deskripsi'] = $detail['jenis_pembayaran'].' - '.$detail['remark'];
                    $arus_kas['nominal'] = $detail['bayar_bunga'];
                    $arus_kas['input_by'] = get_session('auth')['id'];
                    $arus_kas['input_timestamp'] = setNewDateTime();
                    $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                    $result = json_decode(xcurl($param_curl)['response']);
                }
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        $this->go('manajemen/piutang/persetujuan?'.$url);
    }

    public function decline_transaksi_piutang()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'is_verifikasi' => 2,
                'verifikasi_by' => get_session('auth')['id'],
                'timestamp_verifikasi' => setNewDateTime(),
            );
            $response = $this->piutang_detail->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function submit_angsuran()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert statement
            $detail['source'] = $this->source;
            $detail['id_piutang'] = $post['id_piutang'];
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->piutang_detail->insert($detail);
            if ($result) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update statement
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['bayar_pokok'] = str_replace(",", "", $post['bayar_pokok']);
            $detail['bayar_bunga'] = str_replace(",", "", $post['bayar_bunga']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->piutang_detail->update($post['id'], $detail);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }
        $this->go('manajemen/piutang/detail/'.$post['id_piutang']);
    }

    public function submit_tambah_piutang()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert statement
            $detail['source'] = $this->source;
            $detail['id_piutang'] = $post['id_piutang'];
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['tambah_piutang'] = str_replace(",", "", $post['tambah_piutang']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->piutang_detail->insert($detail);
            if ($result) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update statement
            $detail['tgl_transaksi'] = $post['tgl_transaksi'];
            $detail['tambah_piutang'] = str_replace(",", "", $post['tambah_piutang']);
            $detail['remark'] = $post['keterangan'];
            $detail['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $detail['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $detail['id_kas'] = $post['id_rekening'];
            }else{
                $detail['id_kas'] = 0;
            }
            $detail['input_by'] = get_session('auth')['id'];
            $detail['timestamp_input'] = setNewDateTime();
            $result = $this->piutang_detail->update($post['id'], $detail);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }
        $this->go('manajemen/piutang/detail/'.$post['id_piutang']);
    }

    public function get_sisa_pokok()
    {
        $id = $this->input->post('id');
        $piutang_verif = $this->piutang_detail->get_data_piutang_verified($id, '');
        $sisa_pokok = 0;
        if($piutang_verif != false){
            # Ada riwayat transaksi yg terverifikasi
            $total_piutang = 0;
            $bayar_pokok = 0;
            foreach($piutang_verif as $row){
                $total_piutang += $row['tambah_piutang'];
                $bayar_pokok += $row['bayar_pokok'];
            }
            $sisa_pokok = $total_piutang - $bayar_pokok;
        }

        echo json_encode($sisa_pokok);
    }

    public function json_get_data_piutang()
    {
        $id = $this->input->post('id');
        $response = $this->piutang->get_data_piutang_awal($id);
        echo json_encode($response);
    }

    public function get_detail_piutang()
    {
        $id = $this->input->post('id');
        $response = $this->piutang_detail->get_data($id);
        echo json_encode($response);
    }


    public function delete_data_piutang()
    {
        $id = $this->input->post('id');
        $is_verif = $this->piutang_detail->cek_data_validasi($id);
        if($is_verif){
            # Decline action
            $this->message('Gagal menghapus data. Data transaksi sudah disetujui.', 'warning');
        }else{
            # Delete transaksi
            $result = $this->piutang_detail->delete_by_transaksi($id);
            if($result){
                $this->piutang->delete($id);
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }
    }

    public function delete_data_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $result = $this->piutang_detail->delete($id);
            if($result){
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }else{
            $result = false;
            $this->message('Gagal.', 'danger');
        }
        echo json_encode($result);
    }

    public function export_rekap()
    {
        $transaksi = $this->user->join($this->piutang->get_rekap_piutang(), 'submit_by', 'input_by');
        # INIT
        // dump($transaksi);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supplier');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Status');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Bon');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Terakhir Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Uang Muka');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jmlh Pembayaran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['debitur']);
                // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_piutang']);
                if($row['is_paid_off'] == 1){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Lunas');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Belum Lunas');
                    
                }
                // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_piutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_terakhir_bayar']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['total_piutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['sisa_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['angsuran']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['keterangan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_uangmuka_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }

    public function export_detail($id)
    {
        $transaksi = $this->kas->join($this->user->join($this->piutang_detail->get_rekap_piutang_detail($id), 'submit_by', 'input_by'), 'kas', 'id_kas');
        $client = "";
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supplier');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pembayaran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tambah Uang Muka');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bayar Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bayar Bunga');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pokok Awal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sisa Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Catatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp input');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $client = preg_replace('/\s+/', '', $row['debitur']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['debitur']);
                // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_piutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_transaksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['source']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['jenis_pembayaran']);
                if(isset($row['kas_is_rekening'])){
                    if($row['kas_is_rekening'] == 1){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kas_bank'].'-'.$row['kas_no_rek']));
                        
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kas_kas']);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tambah_piutang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['bayar_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['bayar_bunga']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['pokok_awal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['sisa_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['remark']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $baris++;
            }
        }
        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_uangmuka_'.$client.'_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }
}
