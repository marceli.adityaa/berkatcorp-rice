<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Beban extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
        $this->load->model('master_akun_biaya_model', 'akun_biaya');
        $this->load->model('master_akun_biaya_detail_model', 'akun_biaya_detail');
        $this->load->model('beban_model', 'beban');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'master-transaksi-beban';
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('bbn-kll', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            $data['kategori'] = $this->akun_biaya->get_active();
            $data['kategori_detail'] = $this->akun_biaya_detail->get_active();
            // dump($data['kategori_detail']);
            if (!empty($this->input->get())) {
                $data['beban'] = $this->kas->join($this->user->join($this->user->join($this->beban->get_kelola_beban(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            }else{
                $data['beban'] = array();
            }
            $data['status'] = array( 1 => "Menunggu Verifikasi", 2 => "Disetujui", 3 => "Ditolak");
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Beban' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Kelola Beban');
            set_activemenu('sub-beban', 'menu-beban-kelola');
            $this->render('manajemen/beban/v-kelola-transaksi-beban', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('bbn-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['role'] = $this->role;
            $data['kategori'] = $this->akun_biaya->get_active();
            $data['kategori_detail'] = $this->akun_biaya_detail->get_active();
            $data['beban'] = $this->kas->join($this->user->join($this->user->join($this->beban->get_persetujuan_beban(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            $data['status'] = array( 1 => "Menunggu Verifikasi", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Beban' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Transaksi');
            set_activemenu('sub-beban', 'menu-beban-persetujuan');
            $this->render('manajemen/beban/v-persetujuan-beban', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('bbn-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['role'] = $this->role;
            $data['kategori'] = $this->akun_biaya->get_active();
            $data['kategori_detail'] = $this->akun_biaya_detail->get_active();
            if (!empty($this->input->get())) {
                $data['beban'] = $this->kas->join($this->user->join($this->user->join($this->beban->get_rekap_beban(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            }else{
                $data['beban'] = array();
            }
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Beban' => 'active', 'Rekap Transaksi' => 'active'));
            set_session('title', 'Rekap Beban');
            set_activemenu('sub-beban', 'menu-beban-rekap');
            $this->render('manajemen/beban/v-rekap-transaksi-beban', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function export()
    {
        $transaksi = $this->kas->join($this->user->join($this->user->join($this->beban->get_rekap_beban(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kategori');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Akun Biaya');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Catatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nominal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Input');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Verif by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Verif');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tgl_transaksi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['kategori']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['biaya']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['catatan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['nominal']);
                
                if(isset($row['kas_is_rekening'])){
                    if($row['kas_is_rekening'] == 1){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kas_bank'].'-'.$row['kas_no_rek']));
                        
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kas_kas']);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['acc_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_verifikasi']);
                $baris++;
            }
        }
        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_transaksi_beban_'.strtotime(setNewDateTime()).'.xlsx"');
		$writer->save("php://output");
    }

    public function submit_beban()
    {
        $post = $this->input->post();
        $url = $post['url'];
        // dump($post);
        if(empty($post['id'])){
            # Insert Statement
            $beban['id_beban'] = $post['id_beban'];
            $beban['tgl_transaksi'] = $post['tgl_transaksi'];
            $beban['nominal'] = str_replace(",", "", $post['nominal']);
            $beban['catatan'] = $post['catatan'];
            $beban['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $beban['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $beban['id_kas'] = $post['id_rekening'];
            }else{
                $beban['id_kas'] = 0;
            }
            $beban['input_by'] = get_session('auth')['id'];
            $beban['timestamp_input'] = setNewDateTime();
            $result = $this->beban->insert($beban);
            
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update Statement
            $beban['id_beban'] = $post['id_beban'];
            $beban['tgl_transaksi'] = $post['tgl_transaksi'];
            $beban['nominal'] = str_replace(",", "", $post['nominal']);
            $beban['catatan'] = $post['catatan'];
            $beban['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $beban['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $beban['id_kas'] = $post['id_rekening'];
            }else{
                $beban['id_kas'] = 0;
            }
            $beban['input_by'] = get_session('auth')['id'];
            $beban['timestamp_input'] = setNewDateTime();
            $result = $this->beban->update($post['id'], $beban);
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }        
        if(empty($url)){
            $url = "?kategori=all";
        }
        $this->go('manajemen/beban/kelola?'.$url);
    }

    public function approve_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $detail = $this->beban->get_data_beban($id);
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->beban->update($id, $update);
            if($response){
                # record ke arus kas
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tgl_transaksi'];
                $arus_kas['arus'] = "out";
                $arus_kas['sumber'] = $this->source;
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = $detail['kategori'];
                $arus_kas['deskripsi'] = $detail['catatan'];
                $arus_kas['nominal'] = $detail['nominal'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $result = json_decode(xcurl($param_curl)['response']);
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        echo json_encode($response);
    }

    public function decline_transaksi()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'is_verifikasi' => 2,
                'verifikasi_by' => get_session('auth')['id'],
                'timestamp_verifikasi' => setNewDateTime(),
            );
            $response = $this->beban->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function json_get_data_beban()
    {
        $id = $this->input->post('id');
        $response = $this->beban->get_data($id);
        echo json_encode($response);
    }

    public function delete_data_beban()
    {
        $id = $this->input->post('id');
        $detail = $this->beban->get_data($id);
        if($detail['is_verifikasi'] == 1){
            # Decline action
            $this->message('Gagal menghapus data. Data transaksi sudah disetujui.', 'warning');
        }else{
            # Delete transaksi
            $result = $this->beban->delete($id);
            if($result){
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }
        echo json_encode($detail);
    }
}
