<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Retur extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
        $this->load->model('master_akun_biaya_model', 'akun_biaya');
        $this->load->model('master_akun_biaya_detail_model', 'akun_biaya_detail');
        $this->load->model('retur_model', 'retur');
        $this->load->model('beban_model', 'beban');
        $this->load->model('penjualan_po_model', 'penjualan_po');
        $this->load->model('penjualan_po_pembayaran_model', 'penjualan_po_pembayaran');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'retur-penjualan-beras';
        $this->id_retur = $this->config->item('id_retur');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rtp-kll', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            // $data['retur'] = $this->input->get();
            // $data['kategori_detail'] = $this->akun_biaya_detail->get_active();
            // dump($data['kategori_detail']);
            if (!empty($this->input->get())) {
                $data['retur'] = $this->kas->join($this->user->join($this->user->join($this->retur->get_kelola_retur(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            }else{
                $data['retur'] = array();
            }
            $data['status'] = array( 1 => "Menunggu Verifikasi", 2 => "Disetujui", 3 => "Ditolak");
            // $data['coba'] = $this->retur->get_data();
            // dump($data['retur']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Retur Penjualan' => 'active', 'Kelola Retur' => 'active'));
            set_session('title', 'Kelola retur');
            set_activemenu('sub-retur', 'menu-retur-kelola');
            $this->render('manajemen/retur/v-kelola-transaksi-retur', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }
    public function tambah_retur()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rtp-kll', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            // $data['area']       = $this->area->get_active();
            // $data['sales']      = $this->master_sales->get_active();
            $data['transaksi']   = $this->penjualan_po->get_transaksi_penjualan_beras();
            
            // dump($data['transaksi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Retur Penjualan' => 'active', 'Kelola Retur' => base_url('manajemen/retur/kelola'), 'Tambah Retur' => 'active'));
            set_session('title', 'Kelola Retur - Tambah Retur');
            set_activemenu('sub-retur', 'menu-retur-kelola');
            $this->render('manajemen/retur/v-tambah-transaksi', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rtp-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['role'] = $this->role;
            $data['retur'] = $this->kas->join($this->user->join($this->user->join($this->retur->get_persetujuan_retur(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            $data['status'] = array( 1 => "Menunggu Verifikasi", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Retur' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Transaksi');
            set_activemenu('sub-retur', 'menu-retur-persetujuan');
            $this->render('manajemen/retur/v-persetujuan-retur', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('bbn-rkp', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['role'] = $this->role;

            $data['retur'] = $this->kas->join($this->user->join($this->user->join($this->retur->get_rekap_retur(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Retur' => 'active', 'Rekap Transaksi Retur' => 'active'));
            set_session('title', 'Rekap Retur');
            set_activemenu('sub-retur', 'menu-retur-rekap');
            $this->render('manajemen/retur/v-rekap-transaksi-retur', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function export()
    {
        $transaksi = $this->kas->join($this->user->join($this->user->join($this->retur->get_rekap_retur(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
        # INIT
        // dump($transaksi);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Penjualan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nama Customer');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Item yang Diretur');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Alasan Retur');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Tonase');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Bayar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Input');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Verif by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Verif');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tanggal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['kode_penjualan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['customer']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['item_retur']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['keterangan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['total_tonase']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['jumlah_bayar']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['kas_label']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['acc_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_verifikasi']);
                $baris++;
            }
        }
        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_transaksi_retur_penjualan'.strtotime(setNewDateTime()).'.xlsx"');
		$writer->save("php://output");
    }

    public function submit_retur()
    {
        $post = $this->input->post();
        $url = $post['url'];
        // dump($post);
        if(empty($post['id'])){
            # Insert Statement
            $retur['id'] = $post['id'];
            $retur['tanggal'] = $post['tgl_transaksi'];
            $retur['id_penjualan'] = $post['id_penjualan'];
            $retur['item_retur'] = $post['item'];
            $retur['keterangan'] = $post['keterangan'];
            
            $retur['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $retur['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $retur['id_kas'] = $post['id_rekening'];
            }else{
                $retur['id_kas'] = 0;
            }
            $retur['status'] = 1;
            $retur['total_tonase'] = $post['total_tonase'];
            $retur['jumlah_bayar'] = str_replace(",", "", $post['jumlah_bayar']);
            $retur['input_by'] = get_session('auth')['id'];
            $retur['timestamp_input'] = setNewDateTime();
            $result = $this->retur->insert($retur);
            
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update Statement
            $retur['id'] = $post['id'];
            $retur['tanggal'] = $post['tgl_transaksi'];
            $retur['id_penjualan'] = $post['id_penjualan'];
            $retur['item_retur'] = $post['item'];
            $retur['keterangan'] = $post['keterangan'];
            
            $retur['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $retur['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $retur['id_kas'] = $post['id_rekening'];
            }else{
                $retur['id_kas'] = 0;
            }
            $retur['status'] = 1;
            $retur['total_tonase'] = $post['total_tonase'];
            $retur['jumlah_bayar'] = str_replace(",", "", $post['jumlah_bayar']);
            $retur['input_by'] = get_session('auth')['id'];
            $retur['timestamp_input'] = setNewDateTime();
            $result = $this->retur->update($post['id'], $retur);
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }        
        if(empty($url)){
            $url = "?status=all&start=&end=";
        }
        $this->go('manajemen/retur/kelola?'.$url);
    }

    public function approve_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $detail = $this->retur->get_data_retur($id);
            $update['persetujuan'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->retur->update($id, $update);
            $cek_tempo = $this->penjualan_po->get_data($detail['id_penjualan']);
            $tempo = $cek_tempo['is_tempo'];

            if($response){
                #cek tempo
                
            if ($tempo == 0){
                    #jika lunas / tidak tempo
                # record ke arus kas
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tanggal'];
                $arus_kas['arus'] = "out";
                $arus_kas['sumber'] = $this->source;
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = "Beban retur penjualan ".$cek_tempo['kode'];
                $arus_kas['deskripsi'] = "Retur ".$detail['item_retur'];
                $arus_kas['nominal'] = $detail['jumlah_bayar'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $result = json_decode(xcurl($param_curl)['response']);
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }


                $beban['id_beban'] = $this->id_retur;
                $beban['tgl_transaksi'] = $detail['tanggal'];
                $beban['nominal'] = str_replace(",", "", $detail['jumlah_bayar']);
                $beban['catatan'] = "Retur ".$detail['item_retur'];
                $beban['jenis_pembayaran'] = $detail['jenis_pembayaran'];
                $beban['id_kas'] = $detail['id_kas'];
                $beban['is_verifikasi'] = 1;
                $beban['verifikasi_by'] = get_session('auth')['id'];
                $beban['input_by'] = get_session('auth')['id'];
                $beban['timestamp_input'] = setNewDateTime();
                $beban['timestamp_verifikasi'] = setNewDateTime();
                $result_beban = $this->beban->insert($beban);
                if($result_beban){
                    $this->message('Transaksi berhasil disimpan.', 'success');
                }else{
                    $this->message('Gagal record data ke beban / biaya', 'warning');
                }

            }else{
                #jika tempo
                $tempo_in['id_penjualan'] = $detail['id_penjualan'];
                $tempo_in['tgl_pembayaran'] = $detail['tanggal'];
                $tempo_in['jenis_pembayaran'] = $detail['jenis_pembayaran'];
                $tempo_in['id_kas'] = $detail['id_kas'];
                $tempo_in['catatan'] = "Bayar tempo untuk retur ".$detail['item_retur'];
                if ($cek_tempo['grandtotal'] > $detail['jumlah_bayar']){
                    $tempo_in['is_paid_off'] = 0;
                }else if ($cek_tempo['grandtotal'] = $detail['jumlah_bayar']){
                    $tempo_in['is_paid_off'] = 1;
                }else{
                    $tempo_in['is_paid_off'] = 0;
                }
                $tempo_in['jumlah_bayar'] = $detail['jumlah_bayar'];
                $tempo_in['status_persetujuan'] = 1;
                $tempo_in['timestamp_acc'] = setNewDateTime();
                $tempo_in['acc_by'] = get_session('auth')['id'];
                $tempo_in['input_by'] = get_session('auth')['id'];
                $tempo_in['timestamp_input'] = setNewDateTime();
                // dump($tempo_in);
                $result_tempo = $this->penjualan_po_pembayaran->insert($tempo_in);
                if ($result_tempo) {
                    $this->message('Berhasil membayar ke tempo', 'success');
                     
                } else {
                    $this->message('Gagal membayar ke tempo', 'error');
                }

               
                
                $beban['id_beban'] = $this->id_retur;
                $beban['tgl_transaksi'] = $detail['tanggal'];
                $beban['nominal'] = str_replace(",", "", $detail['jumlah_bayar']);
                $beban['catatan'] = "Pembayaran tempo untuk retur ".$detail['item_retur'];
                $beban['jenis_pembayaran'] = $detail['jenis_pembayaran'];
                $beban['id_kas'] = $detail['id_kas'];
                $beban['is_verifikasi'] = 1;
                $beban['verifikasi_by'] = get_session('auth')['id'];
                $beban['input_by'] = get_session('auth')['id'];
                $beban['timestamp_input'] = setNewDateTime();
                $beban['timestamp_verifikasi'] = setNewDateTime();
                
                $result_beban = $this->beban->insert($beban);
                if($result_beban){
                    $this->message('Transaksi berhasil disimpan.', 'success');
                }else{
                    $this->message('Gagal record data ke beban / biaya', 'warning');
                }

                 # record ke arus kas
                 $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                 $arus_kas['id_kas'] = $detail['id_kas'];
                 $arus_kas['tgl_transaksi'] = $detail['tanggal'];
                 $arus_kas['arus'] = "in";
                 $arus_kas['sumber'] = "retur-penjualan-beras-tempo";
                 $arus_kas['id_sumber'] = $id;
                 $arus_kas['keterangan'] = "Pembayaran tempo retur ".$cek_tempo['kode'];
                 $arus_kas['deskripsi'] = "Retur ".$detail['item_retur'];
                 $arus_kas['nominal'] = $detail['jumlah_bayar'];
                 $arus_kas['input_by'] = get_session('auth')['id'];
                 $arus_kas['input_timestamp'] = setNewDateTime();
                 $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                 $result = json_decode(xcurl($param_curl)['response']);
                 $response = $result;
                 if($response){
                     $this->message('Transaksi berhasil disetujui.', 'success');
                 }else{
                     $this->message('Gagal record data ke arus kas', 'warning');
                 }

            }
            

            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        echo json_encode($response);
    }

    public function decline_transaksi()
    {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'persetujuan' => 2,
                'verifikasi_by' => get_session('auth')['id'],
                'timestamp_verifikasi' => setNewDateTime(),
            );
            $response = $this->retur->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function json_get_data_retur()
    {
        $id = $this->input->post('id');
        $response = $this->retur->get_kelola_retur($id);
        echo json_encode($response);
    }

    public function delete_data_retur()
    {
        $id = $this->input->post('id');
        $detail = $this->retur->get_data($id);
        // dump($detail);
        if($detail['persetujuan'] == 1){
            # Decline action
            $this->message('Gagal menghapus data. Data transaksi sudah disetujui.', 'warning');
        }else{
            # Delete transaksi
            $result = $this->retur->delete($id);
            if($result){
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }
        echo json_encode($detail);
    }
}
