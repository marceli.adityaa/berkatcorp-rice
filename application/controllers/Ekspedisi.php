<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi extends MY_Controller_admin
{
	public $bypass_auth = TRUE;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ekspedisi_model', 'ekspedisi');
	}

	public function index()
	{
		$data['supplier'] 	= $this->supplier->get_data();
		// dump($this->session->auth);
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Supplier' => 'active'));
        set_session('title', 'Master Supplier');
        set_activemenu('sub-master', 'menu-supplier');
		$this->render('supplier/v-supplier', $data);
    }
    
    public function pengambilan()
	{   
		// dump($this->session->auth['token']);
        $param_area = array(
            CURLOPT_URL => $this->config->item('url_transport').'api/external/biaya/get?token='.$this->session->auth['token'].'&is_disabled=0'
        );
        $data['area'] 	= json_decode(xcurl($param_area)['response'])->rows;

        $param_kendaraan = array(
            CURLOPT_URL => $this->config->item('url_transport').'api/external/kendaraan/get?token='.$this->session->auth['token'].'&is_disabled=0'
        );
		$data['kendaraan'] 	= json_decode(xcurl($param_kendaraan)['response'])->rows;
		$param_supir = array(
            CURLOPT_URL => $this->config->item('url_master').'api/external/pegawai/get?token='.$this->session->auth['token'].'&is_disabled=0&kelompok_id=2&sort=nama'
        );
		$data['supir'] 	= json_decode(xcurl($param_supir)['response']);
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Ekspedisi - Pengambilan' => 'active'));
        set_session('title', 'Ekspedisi - Pengambilan');
        set_activemenu('sub-ekspedisi', 'menu-pengambilan');
		$this->render('ekspedisi/v-pengambilan', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!isset($post['supply_beras'])){
			$post['supply_beras'] = 0;
		}
		if(!isset($post['supply_gabah'])){
			$post['supply_gabah'] = 0;
		}
        $post['saldo_bon'] = intval(preg_replace('/[^\d.]/', '', $post['saldo_bon']));
        $post['limit_bon'] = intval(preg_replace('/[^\d.]/', '', $post['limit_bon']));
		if(!$post['id']){
			# Insert Statement
			$post['status'] = 1;
            $post['input_by'] = get_session('auth')['id'];
			$post['timestamp'] = setNewDateTime();
			$result = $this->supplier->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->supplier->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('supplier');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->supplier->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->supplier->update($id, array('status' => 1));
		if($response){
			$this->message('Sukses mengubah data', 'success');
		}else{
			$this->message('Gagal', 'error');
		}
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->supplier->update($id, array('status' => 0));
		if($response){
			$this->message('Sukses mengubah data', 'success');
		}else{
			$this->message('Gagal', 'error');
		}
		echo json_encode($response);
	}
}
