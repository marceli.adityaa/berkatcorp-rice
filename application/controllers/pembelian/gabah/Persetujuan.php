<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Persetujuan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('piutang_model', 'piutang');
		$this->load->model('piutang_detail_model', 'piutang_detail');
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-pbl-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
			$data['pembelian'] = $this->user->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_persetujuan(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            // $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			// $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
			// dump($data['rek_penerima']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['pic'] = $this->penanggung_jawab->get_active();
			// dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Pembelian Gabah - Persetujuan Transaksi');
            set_activemenu('sub-pembelian', 'menu-persetujuan-pembelian-gabah');
            $this->render('pembelian/gabah/v-persetujuan-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_approval(){
		$post = $this->input->post();
		$supplier = $post['supplier'];
		$pembelian = $this->pembelian_gabah->get_data($post['id']);
		$muatan = $this->pembelian_gabah->get_data_muatan($post['id']);
		$potong_piutang = str_replace(",", "", $post['potong_piutang']);
		$jumlah_bayar = str_replace(",", "", $post['jumlah_bayar']);
		// $bayar_dp = str_replace(",", "", $post['bayar_dp']);
		
		# send data berat muatan ke ekspedisi
		if(!empty($pembelian['resi_pengiriman'])){
			$param_update_muatan    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/update_muatan_client?token='.$this->session->auth['token'].'&resi='.$pembelian['resi_pengiriman'].'&muatan='.$muatan['berat_muatan']);
        	xcurl($param_update_muatan);
		}
		
		if(!empty($post['id'])){
			$update = array(
				'is_tempo' => $post['is_tempo'],
				'tgl_tempo' => $post['tgl_tempo'],
				'jenis_pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $post['id_kas'],
				'id_rek_pengirim' => $post['id_rek_pengirim'],
				// 'id_rek_penerima' => $post['id_rek_penerima'],
				// 'bayar_dp' => $bayar_dp,
				'potong_piutang' => $potong_piutang,
                'jumlah_bayar' => $jumlah_bayar,
				'dibayar_oleh' => $post['dibayar'],
				'checker' => $post['checker'],
				'tgl_bayar' => $post['tgl_bayar'],
				'status_persetujuan' => 1,
				'verifikasi_oleh' => get_session('auth')['id'],
				'timestamp_verifikasi' => setNewDateTime(),
			);

			$result = $this->pembelian_gabah->update($post['id'], $update);
			
			# Dengan potong piutang
			if($potong_piutang > 0){
				$piutang = $this->piutang->get_data_by_customer($supplier);
				if($piutang != false){
					# Ada data piutang
					if(!empty($post['id_kas'])){
						$kas_sumber = $post['id_kas'];
					}
					if(!empty($post['id_rek_pengirim'])){
						$kas_sumber = $post['id_rek_pengirim'];
					}
					$insert_detail_piutang = array(
						'id_piutang' => $piutang['id'],
						'source' => 'pembelian-gabah',
						'id_source' => $post['id'],
						'tgl_transaksi' => $post['tgl_bayar'],
						'bayar_pokok' => str_replace(",", "", $post['potong_piutang']),
						'remark' => 'Potong Bon - '.$pembelian['kode_pembelian'],
						'jenis_pembayaran' => $post['jenis_pembayaran'],
						'id_kas' => $kas_sumber,
						'input_by' => get_session('auth')['id'],
						'timestamp_input' => setNewDateTime(),
					);
					$this->piutang_detail->insert($insert_detail_piutang);
				}
			}
			
			if($result){
				$pembelian = $this->pembelian_gabah->get_data($post['id']);
				if($post['is_tempo'] == 1){
					# Jika bayar tempo
					// if($post['bayar_dp'] > 0){
					// 	$arus_kas['id_perusahaan'] = $this->id_perusahaan;
					// 	if($pembelian['jenis_pembayaran'] == 'cash'){
					// 		$arus_kas['id_kas'] = $pembelian['id_kas'];
					// 	}else if($pembelian['jenis_pembayaran'] == 'transfer'){
					// 		$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
					// 	}
					// 	$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
					// 	$arus_kas['arus'] = "out";
					// 	$arus_kas['sumber'] = "pembelian-gabah";
					// 	$arus_kas['id_sumber'] = $post['id'];
					// 	$arus_kas['keterangan'] = 'DP Pembelian Gabah - '.$pembelian['kode_pembelian'];
					// 	$arus_kas['deskripsi'] = 'Total : '.$jumlah_bayar;
					// 	$arus_kas['nominal'] = $bayar_dp;
					// 	$arus_kas['input_by'] = get_session('auth')['id'];
					// 	$arus_kas['input_timestamp'] = setNewDateTime();
					// 	$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
					// 	$result = json_decode(xcurl($param_curl)['response']);
					// }
				}else{
					# Jika bayar lunas
					if($potong_piutang > 0 && $jumlah_bayar >= $potong_piutang){
						
						$arus_kas['id_perusahaan'] = $this->id_perusahaan;
						if($pembelian['jenis_pembayaran'] == 'cash'){
							$arus_kas['id_kas'] = $pembelian['id_kas'];
						}else if($pembelian['jenis_pembayaran'] == 'transfer'){
							$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
						}
						$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
						$arus_kas['arus'] = "out";
						$arus_kas['sumber'] = "pembelian-gabah";
						$arus_kas['id_sumber'] = $post['id'];
						$arus_kas['keterangan'] = 'Pembelian Gabah '.$pembelian['kode_pembelian'];
						$arus_kas['deskripsi'] = 'Potong Bon : '.$potong_piutang.', Kurang Bayar : '.($jumlah_bayar - $potong_piutang);
						$arus_kas['nominal'] = $jumlah_bayar;
						$arus_kas['input_by'] = get_session('auth')['id'];
						$arus_kas['input_timestamp'] = setNewDateTime();
						$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
						$result = json_decode(xcurl($param_curl)['response']);
					}else{
						$arus_kas['id_perusahaan'] = $this->id_perusahaan;
						if($pembelian['jenis_pembayaran'] == 'cash'){
							$arus_kas['id_kas'] = $pembelian['id_kas'];
						}else if($pembelian['jenis_pembayaran'] == 'transfer'){
							$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
						}
						$arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
						$arus_kas['arus'] = "out";
						$arus_kas['sumber'] = "pembelian-gabah";
						$arus_kas['id_sumber'] = $post['id'];
						$arus_kas['keterangan'] = 'Pembelian Gabah '.$pembelian['kode_pembelian'];
						$arus_kas['deskripsi'] = 'Bayar Lunas';
						$arus_kas['nominal'] = $jumlah_bayar;
						$arus_kas['input_by'] = get_session('auth')['id'];
						$arus_kas['input_timestamp'] = setNewDateTime();
						$param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
						$result = json_decode(xcurl($param_curl)['response']);
					}

					// $arus_kas['id_perusahaan'] = $this->id_perusahaan;
					// if($pembelian['jenis_pembayaran'] == 'cash'){
					// 	$arus_kas['id_kas'] = $pembelian['id_kas'];
					// }else if($pembelian['jenis_pembayaran'] == 'transfer'){
					// 	$arus_kas['id_kas'] = $pembelian['id_rek_pengirim'];
					// }
					// $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
					// $arus_kas['arus'] = "out";
					// $arus_kas['sumber'] = "pembelian-gabah";
					// $arus_kas['id_sumber'] = $post['id'];
					// $arus_kas['keterangan'] = 'Pembelian Gabah '.$pembelian['kode_pembelian'];
					// if($potong_piutang > 0){
					// 	if($jumlah_bayar > $potong_piutang){
					// 		$arus_kas['deskripsi'] = 'Potong Bon : '.$potong_piutang.', Kurang Bayar : '.($jumlah_bayar - $potong_piutang);
					// 	}else{
					// 		$arus_kas['deskripsi'] = 'Potong Bon : '.$potong_piutang.', Sisa Bon : '.($potong_piutang - $jumlah_bayar);
					// 	}
					// }
					// $arus_kas['nominal'] = $jumlah_bayar;
					// $arus_kas['input_by'] = get_session('auth')['id'];
					// $arus_kas['input_timestamp'] = setNewDateTime();
					// $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
					// $result = json_decode(xcurl($param_curl)['response']);
				}
			}
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('pembelian/gabah/persetujuan?'.$post['url']);
	}
	
}