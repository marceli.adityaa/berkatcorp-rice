<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Kelola extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('generate_model', 'generate'); // model generate kode //
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-gbh', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['pembelian']  = $this->user->join($this->pembelian_gabah->get_data(), 'username_input', 'input_by');
            $data['role'] = $this->role;
            $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_active?token='.$this->session->auth['token'].'&status='.$_GET['status']);        
            $data['ekspedisi']  = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
            $data['status'] = array(1 => "Bongkar Muatan", 2 => "Menunggu Input Harga", 3 => "Persetujuan");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active'));
            set_session('title', 'Pembelian Gabah - Kelola Transaksi');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_pembelian_gabah()
    {
        $post = $this->input->post();
         ## generate kode ##
        $fitur = 'BG';
        $tgl = $post['tanggal_pembelian'];
        $cust = $this->generate->get_customer_init($post['id_supplier']);
        $last = $this->pembelian_gabah->get_kode_pembelian($tgl);
        ## end generate kode ##
        $url = $post['url'];
        unset($post['url']);
        if (!$post['id']) {
            # Insert Statement
            $post['kode_pembelian'] = $this->generate->generate_kode($fitur, $tgl, $cust, $last);
            $post['status_muatan'] = 0;
            $post['status_selesai'] = 0;
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->pembelian_gabah->insert($post);
            if ($result) {
                $this->message('Sukses memasukkan data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->pembelian_gabah->update($id, $post);
            if ($result) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('pembelian/gabah/kelola?'.$url);
    }

    public function input_harga($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-gbh', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;

            $data['pic'] = $this->penanggung_jawab->get_active();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
            $data['ks_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'ks');
            $data['ks_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'ks');
            $data['kg_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'kg');
            $data['kg_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'kg');
            $data['total_sak'] = $this->pembelian_gabah_detail->get_total_sak($id);
            # Berat KS setelah dikurangi kadar air
            if(!empty($data['ks_detail'])){
                foreach($data['ks_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['ks_total_berat_standar'][$a['id_batch']])){
                                    $data['ks_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['ks_total_berat_manual'][$a['id_batch']])){
                                    $data['ks_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }

            # Berat KG setelah dikurangi kadar air
            if(!empty($data['kg_detail'])){
                foreach($data['kg_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['kg_total_berat_standar'][$a['id_batch']])){
                                    $data['kg_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['kg_total_berat_manual'][$a['id_batch']])){
                                    $data['kg_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }
    
    
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/kelola'), 'Input Harga' => 'active'));
            set_session('title', 'Pembelian Gabah - Form Input Harga');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah-input-harga', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_transaksi($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-gbh', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;

            $data['pic'] = $this->penanggung_jawab->get_active();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
            $data['ks_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'ks');
            $data['ks_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'ks');
            $data['kg_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'kg');
            $data['kg_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'kg');
            $data['total_sak'] = $this->pembelian_gabah_detail->get_total_sak($id);
            # Berat KS setelah dikurangi kadar air
            if(!empty($data['ks_detail'])){
                foreach($data['ks_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['ks_total_berat_standar'][$a['id_batch']])){
                                    $data['ks_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['ks_total_berat_manual'][$a['id_batch']])){
                                    $data['ks_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['ks_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['ks_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['ks_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }

            # Berat KG setelah dikurangi kadar air
            if(!empty($data['kg_detail'])){
                foreach($data['kg_detail'] as $a){
                    foreach($data['total_sak'] as $b){
                        if($b['id_batch'] == $a['id_batch']){
                            if($a['is_manual'] == 0){
                                if(empty($data['kg_total_berat_standar'][$a['id_batch']])){
                                    $data['kg_total_berat_standar'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_standar'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_standar'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_standar'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }else{
                                if(empty($data['kg_total_berat_manual'][$a['id_batch']])){
                                    $data['kg_total_berat_manual'][$a['id_batch']]['value'] = 0;
                                    $data['kg_total_berat_manual'][$a['id_batch']]['sak'] = 0;
                                }
                                $data['kg_total_berat_manual'][$a['id_batch']]['value'] += ($a['jumlah_sak'] / $b['jumlah']*((100-$a['kadar_air'])/100) * $a['berat_netto']);
                                $data['kg_total_berat_manual'][$a['id_batch']]['sak'] += $a['jumlah_sak'];
                            }
                        }
                    }
                }
            }
    
    
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/rekap'), 'Detail' => 'active'));
            set_session('title', 'Pembelian Gabah - Detail Transaksi');
            set_activemenu('sub-pembelian', 'menu-rekap-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah-input-harga', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }


    public function cetak_nota($id)
    {
        $get = $this->input->get();
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 10,
            'margin_right' => 7,
            'margin_top' => 30,
            'margin_bottom' => 10,
            'margin_header' => 10,
            'margin_footer' => 160,
            'format' => 'A4'
            ]);
        $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_cetak_nota($id), 'username_input', 'input_by');
        $kode = $data['pembelian']['kode_pembelian'];
        $data['tgl_pembelian'] = tanggal_indo($data['pembelian']['tanggal_pembelian']);
        if(!empty($data['pembelian']['resi_pengiriman'])){
            $param_ekspedisi    = array(CURLOPT_URL => $this->url_transport.'api/external/ekspedisi/get_detail_ekspedisi?token='.$this->session->auth['token'].'&resi='.$data['pembelian']['resi_pengiriman']); 
            $data['ekspedisi'] = $this->pegawai->join_object(json_decode(xcurl($param_ekspedisi)['response']), 'sopir', 'pegawai_id');
            // dump($data['ekspedisi']); 
        }else{
            $data['ekspedisi'] = "";
        }
        $data['detail'] = array();
        $data['ks_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'ks');
        $data['ks_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'ks');
        $data['kg_jenis'] = $this->pembelian_gabah_jenis->get_data_input_harga($id, 'kg');
        $data['kg_detail'] = $this->pembelian_gabah_jenis->get_detail_input_harga($id, 'kg');
        $data['total_sak'] = $this->pembelian_gabah_detail->get_total_sak($id);
        // dump($data['ks_jenis']);
        $data['data_ks'] = array();
        if(!empty($data['ks_jenis']) && !empty($data['ks_detail'])){
            foreach($data['ks_jenis'] as $a){
                $index = 0;
                foreach($data['ks_detail'] as $b){
                    if($a['id_jenis'] == $b['id_jenis']){
                        $data['data_ks'][$a['id_jenis']][$index++] = array('kadar_air' => $b['kadar_air'], 'jumlah_sak' => $b['jumlah_sak']);
                    }
                }
            }
        }

        $data['data_kg'] = array();
        if(!empty($data['kg_jenis']) && !empty($data['kg_detail'])){
            foreach($data['kg_jenis'] as $a){
                $index = 0;
                foreach($data['kg_detail'] as $b){
                    if($a['id_jenis'] == $b['id_jenis']){
                        $data['data_kg'][$a['id_jenis']][$index++] = array('kadar_air' => $b['kadar_air'], 'jumlah_sak' => $b['jumlah_sak']);
                    }
                }
            }
        }
// dump($data);
        $html = $this->load->view('modules/pembelian/gabah/v-nota', $data, true);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("PT. BAS - Invoice Pembelian Gabah ".$kode);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        $mpdf->SetDisplayMode('fullpage');




        $file_name = "Pembelian_Gabah_Nota_".$kode.'.pdf';
        
        $mpdf->WriteHTML($html);

        $mpdf->Output($file_name, 'I');
    }

    // public function cetak_nota($id)
    // {
    //     $get = $this->input->get();
        
    //     $mpdf = new \Mpdf\Mpdf([
    //         'margin_left' => 10,
    //         'margin_right' => 7,
    //         'margin_top' => 30,
    //         'margin_bottom' => 10,
    //         'margin_header' => 10,
    //         'margin_footer' => 160,
    //         'format' => 'A4'
    //         ]);
    //     $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_cetak_nota($id), 'username_input', 'input_by');
    //     $data['tgl_pembelian'] = tanggal_indo($data['pembelian']['tanggal_pembelian']);
        
    //     $detail = $this->pembelian_gabah->get_data_nota($id, $get);
    //     // dump($detail);
    //     $data['detail'] = array();
    //     $idx = 0;
    //     foreach($detail as $key => $row){
    //         if($key == 0){
    //             $temp2[$idx][$row['jenis_gabah']] = array();
    //             $temp['batch'] = $row['batch'];
    //             $temp['jenis_gabah'] = $row['jenis_gabah'];
    //             $temp['padi'] = $row['padi'];
    //         }
    //         if($row['batch'] == $temp['batch'] && $row['jenis_gabah'] == $temp['jenis_gabah']){
    //             $data['detail'][$idx]['batch'] = $row['batch'];
    //             $data['detail'][$idx]['jenis_gabah'] = $row['jenis_gabah'];
    //             $data['detail'][$idx]['batch_label'] = $row['batch_label'];
    //             if($row['padi'] != $temp['padi']){
    //                 if(in_array($row['padi'], $temp2[$idx][$row['jenis_gabah']]) == false){
    //                     $data['detail'][$idx]['berat_bruto'] += $row['berat_bruto'];
    //                     $data['detail'][$idx]['berat_netto'] += $row['berat_beras_netto'];
    //                     array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
    //                 }
    //             }else{
    //                 if(in_array($row['padi'], $temp2[$idx][$row['jenis_gabah']]) == false){
    //                     if(!isset($data['detail'][$idx]['berat_bruto'])){
    //                         $data['detail'][$idx]['berat_bruto'] = 0;
    //                     }
    //                     if(!isset($data['detail'][$idx]['berat_netto'])){
    //                         $data['detail'][$idx]['berat_netto'] = 0;
    //                     }
                        
    //                     $data['detail'][$idx]['berat_bruto'] += $row['berat_bruto'];
    //                     $data['detail'][$idx]['berat_netto'] += $row['berat_beras_netto'];
    //                     array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
    //                 }
    //             }
    //             if($key == 0){
    //                 $data['detail'][$idx]['jumlah_sak'] = $row['jumlah_sak'];
    //             }else{
    //                 $data['detail'][$idx]['jumlah_sak'] += $row['jumlah_sak'];
    //             }
    //             $data['detail'][$idx]['harga_nol_gabuk'] = $row['harga_nol_gabuk'];
    //             $data['detail'][$idx]['harga_kg'] = $row['harga_kg'];
    //         }else{
                
    //             $temp['batch'] = $row['batch'];
    //             $temp['jenis_gabah'] = $row['jenis_gabah'];
    //             $temp['padi'] = $row['padi'];
    //             $idx++;
    //             $data['detail'][$idx]['batch'] = $row['batch'];
    //             $data['detail'][$idx]['jenis_gabah'] = $row['jenis_gabah'];
    //             $data['detail'][$idx]['batch_label'] = $row['batch_label'];
    //             $data['detail'][$idx]['berat_bruto'] = $row['berat_bruto'];
    //             $data['detail'][$idx]['berat_netto'] = $row['berat_beras_netto'];
    //             $data['detail'][$idx]['jumlah_sak'] = $row['jumlah_sak'];
    //             $data['detail'][$idx]['harga_nol_gabuk'] = $row['harga_nol_gabuk'];
    //             $data['detail'][$idx]['harga_kg'] = $row['harga_kg'];
    //             $temp2[$idx][$row['jenis_gabah']] = array();
    //             if($row['berat_beras_netto'] != 0){
    //                 array_push($temp2[$idx][$row['jenis_gabah']], $row['padi']);
    //             }
    //         }
    //     }
    //     // dump($data['detail']);
    //     $html = $this->load->view('modules/pembelian/gabah/v-nota', $data, true);
    //     $mpdf->SetProtection(array('print'));
    //     $mpdf->SetTitle("PT. BAS - Invoice Pembelian Gabah");
    //     $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
    //     // $mpdf->SetWatermarkText("Paid");
    //     // $mpdf->showWatermarkText = true;
    //     // $mpdf->watermark_font = 'DejaVuSansCondensed';
    //     // $mpdf->watermarkTextAlpha = 0.1;
    //     $mpdf->SetDisplayMode('fullpage');

    //     $mpdf->WriteHTML($html);

    //     $mpdf->Output();
    // }
}