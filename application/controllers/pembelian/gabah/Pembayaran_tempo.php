<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Pembayaran_tempo extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pby-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['pembelian'] = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_pembayaran_tempo(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu", 2 => "Lunas");
			$param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/rekening/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);        
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
			$data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');

            $data['pic'] = $this->penanggung_jawab->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Pembayaran Tempo' => 'active'));
            set_session('title', 'Pembayaran Tempo - Pembelian Gabah');
            set_activemenu('sub-pembelian', 'menu-pembayaran-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah-pembayaran', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id = null)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pby-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if(!empty($id)){
                $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
                $data['kas']        = json_decode(xcurl($param_kas)['response']);
                $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
                $data['pembayaran'] = array('cash', 'transfer');
                $data['role'] = $this->role;
                $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_pembayaran_tempo($id), 'submit_by', 'diajukan_oleh');
                $data['detail'] = $this->user->join($this->kas->join($this->user->join($this->pembelian_gabah_pembayaran->get_detail_pembayaran($id), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'acc', 'acc_by');
                $data['sisa_tempo'] = $data['pembelian']['grandtotal'] - $data['pembelian']['bayar_dp'];
                $pembayaran_verif = $this->pembelian_gabah_pembayaran->get_data_pembayaran_verified($id, '');
                if($pembayaran_verif != false){
                    # Ada riwayat transaksi yg terverifikasi
                    $total_pembayaran = 0;
                    foreach($pembayaran_verif as $row){
                        $total_pembayaran += $row['jumlah_bayar'];
                    }
                    $data['sisa_tempo'] -= $total_pembayaran;
                }
                set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/pembayaran_tempo'), 'Pembayaran Tempo' => base_url('pembelian/gabah/pembayaran_tempo'), 'Detail Pembayaran' => 'active'));
                set_session('title', 'Kelola Piutang');
                set_activemenu('sub-pembelian', 'menu-pembayaran-pembelian-gabah');
                $this->render('pembelian/gabah/v-pembelian-gabah-pembayaran-detail', $data);
            }
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-pby-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
			$data['pembayaran'] = $this->user->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah_pembayaran->get_data_otorisasi_pembayaran(), 'submit_by', 'input_by'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'acc', 'acc_by');
            $data['role'] = $this->role;
			$data['status'] = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Otorisasi Pembayaran' => 'active'));
            set_session('title', 'Pembelian Gabah - Otorisasi Pembayaran');
            set_activemenu('sub-pembelian', 'menu-persetujuan-pembayaran-gabah');
			// dump($data);
            $this->render('pembelian/gabah/v-otorisasi-pembayaran-pembelian-gabah', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_pembayaran(){
        $post = $this->input->post();
        unset($post['sisa_bayar_val']);
        unset($post['sisa_bayar']);
        if($post['jenis_pembayaran'] == 'cash'){
            $post['id_kas'] = $post['kas'];
        }else{
            $post['id_kas'] = $post['rekening'];
        }
        unset($post['kas']);
        unset($post['rekening']);
        $post['jumlah_bayar'] = str_replace(",", "", $post['jumlah_bayar']);
        $id_pembelian = $post['id_pembelian'];

        if(empty($post['id'])){
            # Insert Statement
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->pembelian_gabah_pembayaran->insert($post);
            if ($result) {
                $this->message('Berhasil mengajukan pembayaran', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }else{
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $post['input_by'] = get_session('auth')['id'];
            $post['timestamp_input'] = setNewDateTime();
            $result = $this->pembelian_gabah_pembayaran->update($id, $post);
            if ($result) {
                $this->message('Berhasil mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('pembelian/gabah/pembayaran_tempo/detail/'.$id_pembelian);
    }

    
}