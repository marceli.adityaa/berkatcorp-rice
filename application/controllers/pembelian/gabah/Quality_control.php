<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Quality_control extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('pembelian_gabah_tes_model', 'pembelian_gabah_tes');
        $this->load->model('pembelian_gabah_tes_detail_model', 'pembelian_gabah_tes_detail');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $data['supplier'] = $this->customer->get_active_supplier();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc(), 'username_input', 'input_by');
            $data['status'] = array(1 => "Bongkar Muatan", 2 => "Menunggu Input Harga", 3 => "Selesai");
            $data['role'] = $this->role;
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Quality Control' => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    } 

    public function batch($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['jenis_sak'] = $this->jenis_sak->get_active();
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data($id), 'username_input', 'input_by');
            $data['jenis'] = $this->pembelian_gabah_jenis->get_data_jenis_pembelian($id);
            $data['manual'] = $this->pembelian_gabah_jenis->get_data_jenis_pembelian($id, 1);
            $data['batch'] = $this->user->join($this->pembelian_gabah_batch->get_data($id), 'username_input', 'input_by');
            // dump($data['batch']);
            $data['batch_idx'] = $this->pembelian_gabah_batch->get_data_last($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/quality_control'), 'Quality Control' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => 'active', $data['pembelian']['kode_pembelian'] => 'active'));
            $data['sisa_bruto'] = $data['pembelian']['berat_bruto'];
            $data['role'] = $this->role;
            if ($data['batch_idx'] > 1) {
                foreach ($data['batch'] as $row) {
                    // $data['sisa_bruto'] -= $row['berat_netto'];
                    $data['sisa_bruto'] -= ($row['berat_kotor'] - $row['berat_kosong']);
                }
            }
            if(!empty($data['batch'])){
                foreach($data['batch'] as $key => $row){
                    $sak = 0;
                    if(!empty($data['jenis'])){
                        foreach($data['jenis'] as $row2){
                            if($row['id'] == $row2['id_batch']){
                                $sak += $row2['jumlah_sak'];
                            }
                        }
                    }
                    $data['batch'][$key]['sak'] = $sak;
                }
            }
            set_session('title', 'Quality Control Gabah - Timbangan');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-batch', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function transaksi($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['role'] = $this->role;
            $data['sap'] = $this->pembelian_gabah_jenis->get_index_sap($data['pembelian']['id_pembelian']);
            $data['detail_sap'] = $this->pembelian_gabah_jenis->get_detail_sap($id);
            $data['detail'] = $this->user->join($this->pembelian_gabah_jenis->get_detail_muatan($id, true), 'username_input', 'input_by');         
            $data['timbang_kecil'] = $this->pembelian_gabah->get_data_qc_detail_manual($data['pembelian']['id_pembelian'], $data['pembelian']['batch']);
            // dump($data['pembelian']);
            if(!empty($data['timbang_kecil'])){
                $data['timbang_kecil'] = $data['timbang_kecil'][0];
                $data['detail_kecil'] = $this->pembelian_gabah_jenis->get_data_jenis_timbang_kecil($data['timbang_kecil']['id_batch'], false);
            }else{
                $data['detail_kecil'] = array();
            }

            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/quality_control'), 'Quality Control' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/quality_control/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-transaksi', $data);          
            
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function form_manual($id){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['timbangan'] = $this->pembelian_gabah->get_data_qc_detail_manual($data['pembelian']['id_pembelian'], $data['pembelian']['batch']);
            $data['role'] = $this->role;
            $data['kadar_air'] = $this->kadar_air->get_active();
            $data['jenis_gabah'] = $this->jenis_gabah->get_active();
            $data['jenis_padi'] = $this->jenis_padi->get_active();
            if(!empty($data['timbangan'])){
                $data['timbangan'] = $data['timbangan'][0];
                $data['detail'] = $this->pembelian_gabah_jenis->get_detail_muatan($data['timbangan']['id_batch'], false);
            }else{
                $data['timbangan']['id_batch'] = '';
                $data['detail'] = array();
            }
            // dump($data['timbangan']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/quality_control'), 'Quality Control' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/quality_control/batch/' . $data['pembelian']['id_pembelian']), 'Detail' => base_url('pembelian/gabah/quality_control/transaksi/' . $id), $data['pembelian']['kode_pembelian'] => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-manual', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
        
    }

    public function form($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['kadar_air'] = $this->kadar_air->get_active();
            $data['jenis_gabah'] = $this->jenis_gabah->get_active();
            $data['jenis_padi'] = $this->jenis_padi->get_active();
            $data['role'] = $this->role;
            if (!empty($sap = $this->input->get('sap'))) {
                $data['sap'] = $sap;
            } else {
                $data['sap'] = 1;
            }

            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => base_url('pembelian/gabah/quality_control'), 'Quality Control' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/quality_control/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => base_url('pembelian/gabah/quality_control/transaksi/' . $id), 'Form' => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control Form');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-form', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function edit_form($id){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('qc', $hak->bas) || $hak->bas[0] == '*') {
            $sap = $this->input->get('sap');
            $data['pembelian'] = $this->user->join($this->pembelian_gabah->get_data_qc_detail($id), 'username_input', 'input_by')[0];
            $data['kadar_air'] = $this->kadar_air->get_active();
            $data['jenis_gabah'] = $this->jenis_gabah->get_active();
            $data['jenis_padi'] = $this->jenis_padi->get_active();
            $data['jenis_ks'] = $this->pembelian_gabah_jenis->get_data_jenis_ks($id);
            $data['jenis_kg'] = $this->pembelian_gabah_jenis->get_data_jenis_kg($id);
            $data['detail'] = $this->pembelian_gabah_detail->get_data_detail($id, $sap);
            $data['role'] = $this->role;
            // dump($data);
            $jumlah_sak = 0;
            $sap = 0;
            foreach($data['detail'] as $row){
                $sap = $row['sap'];
                $jumlah_sak += $row['jumlah_sak'];
            }
            $data['sap'] = $sap;
            $data['jumlah_sak'] = $jumlah_sak;
            $data['id_batch'] = $id;
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Quality Control Gabah' => base_url('pembelian/gabah/quality_control'), 'Timbangan' => base_url('pembelian/gabah/quality_control/batch/' . $data['pembelian']['id_pembelian']), $data['pembelian']['kode_pembelian'] => base_url('pembelian/gabah/quality_control/transaksi/' . $id), 'Edit Data' => 'active'));
            set_session('title', 'Pembelian Gabah - Quality Control Form');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-qc');
            $this->render('pembelian/gabah/v-pembelian-gabah-qc-form-edit', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }
    
    public function submit_pembelian_gabah_batch()
    {
        $post = $this->input->post();
        if(!empty($post['is_manual'])){
            $post['is_manual'] = 1;
        }else{
            $post['is_manual'] = 0;
        }
        if (!$post['id']) {
            # Insert Statement
            $post['input_by'] = get_session('auth')['id'];
            $result = $this->pembelian_gabah_batch->insert($post);
            if ($result) {
                $this->message('Sukses memasukkan data', 'success');
                $this->go('pembelian/gabah/quality_control/transaksi/' . $this->db->insert_id());
            } else {
                $this->message('Gagal', 'error');
                $this->go('pembelian/gabah/quality_control/batch/' . $post['id_pembelian']);
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->pembelian_gabah_batch->update($id, $post);
            if ($result) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
            $this->go('pembelian/gabah/quality_control/batch/' . $post['id_pembelian']);
        }
    }

    public function buku_tes(){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('ts-gbh', $hak->bas) || $hak->bas[0] == '*') {
            if(empty($_GET['status'])){
                $_GET['status'] = 1;
            }
            $data['supplier'] = $this->customer->get_active_supplier();
            $data['pembelian'] = $this->pembelian_gabah->get_data_tes_gabah();
            $data['tes'] = $this->pembelian_gabah_tes->get_data_tes();
            $data['status'] = array(1 => "Menunggu update", 2 => "Selesai", 3 => 'Dibatalkan');
            
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Quality Control' => 'active', 'Buku Tes' => 'active'));
            set_session('title', 'Pembelian Gabah - Buku Tes');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-tes');
            $this->render('pembelian/gabah/v-pembelian-gabah-tes', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail_tes($id){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('ts-gbh', $hak->bas) || $hak->bas[0] == '*') {
            $data['tes'] = $this->pembelian_gabah_tes->get_data_tes_detail($id)[0];
            $data['detail'] = $this->pembelian_gabah_tes_detail->get_detail($id);
            $data['status'] = $this->pembelian_gabah_tes->get_data_tes($id);
            // dump($data);
            
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Quality Control' => 'active', 'Buku Tes' => base_url('pembelian/gabah/quality_control/buku_tes'), 'Detail' => 'active'));
            set_session('title', 'Pembelian Gabah - Detail Buku Tes');
            set_activemenu('sub-pembelian', 'menu-pembelian-gabah-tes');
            $this->render('pembelian/gabah/v-pembelian-gabah-tes-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function init_data_tes(){
        $id = $this->input->get('id');
        $list = $this->pembelian_gabah->get_data_tes_gabah($id);
        if(!empty($list)){
            $insert = array();
            foreach($list as $row){
                $log = array(
                    'id_pembelian' => $row['id'],
                    'id_batch' => $row['id_batch'],
                    'batch' => $row['batch'],
                    'id_supplier' => $row['id_supplier'],
                    'jenis_gabah' => $row['jenis_gabah'],
                    'id_jenis_padi' => $row['id_jenis_padi'],
                    'input_by' => get_session('auth')['id'],
                    'timestamp' => setNewDateTime()
                );
                array_push($insert, $log);
            }
            $result = $this->pembelian_gabah_tes->batch_insert($insert);
            if($result){
                $this->pembelian_gabah->update($id, array('status_tes' => 1));
                $this->message('Sukses menambahkan data tes', 'success');
            }else{
                $this->message('Data tes gagal ditambahkan', 'error');
            }
        }else{
            $this->message('Internal error', 'error');
        }
        $this->go('pembelian/gabah/quality_control/buku_tes');

    }

    public function submit_form_detail(){
        $post = $this->input->post();
        $id = $post['id_tes'];
        unset($post['id_tes']);
        $result = $this->pembelian_gabah_tes->update($id, $post);
        $pembelian = $this->pembelian_gabah_tes->get_data_tes($id)[0];
        if($pembelian['status_persetujuan'] == 0){
            # update data hampa
            $jenis = $this->pembelian_gabah_batch->get_data_jenis_bukutes($pembelian['id_batch'], $pembelian['id_pembelian'], $pembelian['jenis_gabah'], $pembelian['id_jenis_padi']);
            if(!empty($jenis)){
                foreach($jenis as $row){
                    $this->pembelian_gabah_jenis->update($row['id'], array('hampa' => $post['hasil_akhir']));
                }
            }
            
            if($result){
                $this->message('Sukses menyimpan data', 'success');
            }else{
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            $this->message('Gagal mengubah hasil akhir karena data sudah disetujui.', 'error');
        }
        $this->go('pembelian/gabah/quality_control/detail_tes/'.$id);
    }

    public function submit_tes(){
        $post = $this->input->post();
        $id = $post['id_tes'];
        $result = $this->pembelian_gabah_tes_detail->insert($post);
        if($result){
            $this->message('Sukses menambahkan data tes', 'success');
        }else{
            $this->message('Data tes gagal ditambahkan', 'error');
        }
        $this->go('pembelian/gabah/quality_control/detail_tes/'.$id);
    }

    public function hapus_detail_tes(){
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah_tes_detail->delete($id);
        if($response){
            $this->message('Sukses menghapus data', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        echo json_encode($response);
    }

    public function hapus_data_tesgabah(){
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah_tes->update($id, array('is_deleted' => 1));
        if($response){
            $this->message('Sukses menghapus data', 'success');
        }else{
            $this->message('Gagal menghapus data', 'error');
        }
        echo json_encode($response);
    }

    public function aktivasi_data_tesgabah(){
        $id = $this->input->post('id');
        $response = $this->pembelian_gabah_tes->update($id, array('is_deleted' => 0));
        if($response){
            $this->message('Sukses mengaktifkan data transaksi', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($response);
    }

    public function cetak_label($id)
    {
        $data['detail'] = $this->pembelian_gabah_jenis->get_data_label($id);
        $jenis = $data['detail']['jenis_gabah'];
        $kode = str_replace('/', '-', $data['detail']['kode_pembelian']);
        $padi = $data['detail']['padi'];    
        // dump($data);
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 3,
            'margin_right' => 3,
            'margin_top' => 5,
            'margin_bottom' => 3,
            // 'margin_header' => 10,
            // 'margin_right' => 132,
            // 'margin_footer' => 197,
            'format' => 'A8-L'
            ]);
        
        $html = $this->load->view('modules/pembelian/gabah/v-cetak-label-qc', $data, true);
        // $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Label QC #".$data['detail']['kode_pembelian']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        // $mpdf->SetDisplayMode('fullpage');
        
        
        $file_name = "Label Timbangan ".strtoupper($jenis).'-'.$padi.' '.$kode.'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($file_name, 'D');
        // $mpdf->Output("Label-".strtotime(setNewDateTime()).".pdf", "D")->setContentType('application/pdf');
        // $mpdf->Output('my-file.pdf', 'D'); 
        
    }

    public function cetak_label_portrait($id)
    {
        $data['detail'] = $this->pembelian_gabah_jenis->get_data_label($id);
        $jenis = $data['detail']['jenis_gabah'];
        $kode = str_replace('/', '-', $data['detail']['kode_pembelian']);
        $padi = $data['detail']['padi'];    
        // dump($data);
        
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 2,
            'margin_right' => 2,
            'margin_top' => 5,
            'margin_bottom' => 3,
            'margin_header' => 10,
            // 'margin_right' => 132,
            // 'margin_footer' => 197,
            'format' => 'A7'
            ]);
        
        $html = $this->load->view('modules/pembelian/gabah/v-cetak-label-qc', $data, true);
        $mpdf->SetTitle("Label QC #".$data['detail']['kode_pembelian']);
        $mpdf->SetAuthor("PT. Berkat Anoegerah Sejahtera");


        $file_name = "Label Timbangan ".strtoupper($jenis).'-'.$padi.' '.$kode.'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->Output($file_name, 'D');

        // $mpdf->WriteHTML($html);
        // $mpdf->SetDisplayMode('fullpage');
        // $mpdf->Output();
        
    }

}