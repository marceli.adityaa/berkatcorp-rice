<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Rekap extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_gabah_model', 'pembelian_gabah');
        $this->load->model('pembelian_gabah_batch_model', 'pembelian_gabah_batch');
        $this->load->model('pembelian_gabah_jenis_model', 'pembelian_gabah_jenis');
        $this->load->model('pembelian_gabah_detail_model', 'pembelian_gabah_detail');
        $this->load->model('pembelian_gabah_pembayaran_model', 'pembelian_gabah_pembayaran');
        $this->load->model('customer_model', 'customer');
        $this->load->model('kadar_air_model', 'kadar_air');
        $this->load->model('user_model', 'user');
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('jenis_gabah_model', 'jenis_gabah');
        $this->load->model('jenis_padi_model', 'jenis_padi');
        $this->load->model('rekening_model', 'rekening');
        $this->load->model('kas_model', 'kas');
        $this->load->model('penanggung_jawab_model', 'penanggung_jawab');
        $this->load->model('jenis_sak_model', 'jenis_sak');
        $this->load->model('transaksi_sak_model', 'transaksi_sak');
        $this->load->model('transaksi_sak_detail_model', 'transaksi_sak_detail');
        $this->url_master = $this->config->item('url_master');
        $this->url_transport = $this->config->item('url_transport');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
        $this->src = 'pembelian-gabah';
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rkp-pb-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['supplier'] = $this->customer->get_active_supplier();
            if(!empty($_GET)){
                $data['pembelian']  = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_rekap(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');
            }else{
                $data['pembelian'] = array();
            }
            $data['role'] = $this->role;
            $data['status'] = array(1 => "Bongkar Muatan", 2 => "Menunggu Input Harga", 3 => "Persetujuan");
            
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Gabah' => 'active', 'Rekap' => 'active'));
            set_session('title', 'Pembelian Gabah - Rekap Transaksi');
            set_activemenu('sub-pembelian', 'menu-rekap-pembelian-gabah');
            $this->render('pembelian/gabah/v-pembelian-gabah-rekap', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function export(){
        $get                    = $this->input->get();
        $data['pembelian']      = $this->user->join($this->rekening->join($this->kas->join($this->kas->join($this->user->join($this->pembelian_gabah->get_data_rekap(), 'submit_by', 'diajukan_oleh'), 'kas', 'id_kas'), 'rek_pengirim', 'id_rek_pengirim'), 'rek_penerima', 'id_rek_penerima'), 'verifikasi', 'verifikasi_oleh');

        $data['detail_trx'] = $this->pembelian_gabah->get_data_rekap_detail();
        $data['total_sak'] = $this->pembelian_gabah->get_rekap_total_sak();
        $total_sak = array();
        if(!empty($data['total_sak'])){
            foreach($data['total_sak'] as $rowi){
                $total_sak[$rowi['id_batch']] = $rowi;
            }
        }
        // dump($data['total_sak']);
        

        $spreadsheet = new Spreadsheet();
        $idx = 0;
        $total_netto = 0;
        $grandtotal = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Pembelian Gabah');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Pembelian');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supplier');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Plat Nomor');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Netto');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tempo');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Tempo');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Pembayaran');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Dari');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kepada');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Grandtotal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input By');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp');
        $baris++;
        if(!empty($data['pembelian'])){
            foreach($data['pembelian'] as $d){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kode_pembelian']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_pembelian']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['supplier']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['plat_kendaraan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['total_netto']);
                if($d['is_tempo']){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 1);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tgl_tempo']);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, 0);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }
                if($d['jenis_pembayaran'] != 'tbd'){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jenis_pembayaran']);
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                if($d['jenis_pembayaran'] == 'transfer'){
                    $from = ucwords($d['rek_pengirim_bank']).'-'.$d['rek_pengirim_no_rek'];
                    // $to = ucwords($d['rek_penerima_bank']).'-'.$d['rek_penerima_no'];
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $from);
                    // $sheet->setCellValueByColumnAndRow($kolom++, $baris);
                }else{
                    if(!empty($d['kas_kas'])){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($d['kas_kas']));
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                    }
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }     
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['grandtotal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['submit_by']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['timestamp_input']);
                $baris++;
                $total_netto += $d['total_netto'];
                $grandtotal += $d['grandtotal'];
            }
        }

        # Set subtotal
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Berat Netto');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_netto);

        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Pembelian');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $grandtotal);

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;


        # New Sheet
        $worksheet = new Worksheet($spreadsheet, 'Detail Transaksi');
        $spreadsheet->addSheet($worksheet, $idx);
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kode Pembelian');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supplier');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Plat Kendaraan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timbangan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Gabah');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Padi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jumlah Sak');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Potong Sak');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Potong Lain');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Hampa (%)');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Muatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Gabah');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Netto');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Nol Gabuk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga KG');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tambahan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total');
        $baris++;
        
        if(!empty($data['detail_trx'])){
            foreach($data['detail_trx'] as $d){
                $kolom = 1;
                $berat_muatan = round($d['sak'] / $total_sak[$d['id_batch']]['jumlah'] * $d['total_netto']);
                $berat_gabah = round($d['sak'] / $total_sak[$d['id_batch']]['jumlah']*((100-$d['kadar_air'])/100) * $d['total_netto']);
                $berat_netto = round(($berat_gabah - (($d['potong_sak'] * $d['sak']) + $d['potong_lain'])) * ((100-$d['hampa'])/100));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kode_pembelian']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_pembelian']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['supplier']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['plat_kendaraan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['batch'].' - '.$d['batch_label']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, strtoupper($d['jenis_gabah']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['padi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['sak']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['potong_sak']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['potong_lain']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['hampa']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $berat_muatan);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $berat_gabah);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $berat_netto);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['harga_nol_gabuk']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['harga_kg']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tambahan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['subtotal']);
                $baris++;
            }
        }

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        

        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_pembelian_gabah_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }

    
}