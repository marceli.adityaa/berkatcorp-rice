<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'vendor/autoload.php';
class Persetujuan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('area_model', 'area');
        $this->load->model('pendapatan_model', 'pendapatan'); //
        $this->load->model('beban_model', 'beban'); //
        $this->load->model('piutang_model', 'piutang');
        $this->load->model('piutang_detail_model', 'piutang_detail');
        $this->load->model('customer_model', 'customer');
        $this->load->model('pembelian_lain_model', 'pembelian_lain');
        $this->load->model('pembelian_lain_detail_model', 'pembelian_lain_detail');
        $this->load->model('master_pembelian_lain_model', 'master_pembelian_lain');
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->id_potongan_pembelian = $this->config->item('id_potongan_pembelian'); //
        $this->id_biaya_tambahan = $this->config->item('id_biaya_tambahan'); // 
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-acc', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 2;
            }
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_penerima'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active_supplier();
            
            $data['pembayaran'] = array('cash', 'transfer');
            foreach($data['customer'] as $row){
                $data['list_customer'][$row['id']] = $row;
            }
            // $data['detail'] = $this->pembelian_lain_detail->get_data_detail();
            $data['status'] = array(2 => "Menunggu Persetujuan", 3 => 'Disetujui', 4 => "Ditolak");
            $data['transaksi'] = $this->user->join($this->pembelian_lain->get_data_persetujuan(), 'submit_by', 'input_by');
            // $data['saldo_piutang'] = $this->piutang->get_active_balance($data['transaksi'][0]['id_customer']);
            // dump($data['saldo_piutang']['balance']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Bahan' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Pembelian Bahan - Persetujuan');
            set_activemenu('sub-pembelian-lain', 'menu-pbl-acc');
            $this->render('pembelian/lain/v-pembelian-persetujuan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function detail($id)
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('pbl-acc', $hak->bas) || $hak->bas[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            
            $data['role'] = $this->role;
            $data['kategori'] = $this->master_pembelian_lain->get_active();
            $data['trx'] = $this->pembelian_lain->get($id);
            $data['detail'] = $this->user->join($this->pembelian_lain_detail->get_data_detail($id), 'submit_by', 'input_by');
            // dump($data);
            set_session('breadcrumb', array('Pembelian Bahan' => 'active', 'Persetujuan' => base_url('pembelian/lain/persetujuan'), 'Detail Pembelian' => 'active'));
            set_session('title', 'Pembelian Bahan - Detail Transaksi');
            set_activemenu('sub-pembelian-lain', 'menu-pbl-kll');
            $this->render('pembelian/lain/v-pembelian-lain-detail', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_approval(){
        $post = $this->input->post();
        $post['sumber'] = 'pembelian-bahan';
        $id = $post['id'];
        $potong_piutang = str_replace(",", "", $post['potong_piutang']);
        $data['detail'] = $this->user->join($this->pembelian_lain_detail->get_data_detail($id), 'submit_by', 'input_by');
        $beli = $this->pembelian_lain->get_data($post['id']);
        $subtotal = $beli['grandtotal'] + $beli['potongan'] - $beli['biaya_tambahan']; 
        $datadetail = $data['detail'];
        if(!empty($id)){
            if(isset($post['jenis_pembayaran'])){
                if($post['jenis_pembayaran'] == 'cash'){
                    $kas = $post['id_kas'];
                }else{
                    $kas = $post['id_rek_penerima'];
                }
            }else{
                $post['jenis_pembayaran'] = 'tbd';
                $kas = 0;
            }
            if($post['is_tempo'] == 0){
                $isLunas = 1;
            }else{
                $isLunas = 0;
            }

			$update = array(
				'is_tempo' => $post['is_tempo'],
				'tgl_tempo' => $post['tgl_tempo'],
				'jenis_pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $kas,
                'jumlah_bayar' => str_replace(",", "", $beli['grandtotal']),
				'tgl_bayar' => $post['tgl_bayar'],
                'persetujuan' => 1,
                'is_lunas' => $isLunas,
				'verif_by' => get_session('auth')['id'],
				'timestamp_verif' => setNewDateTime(),
			);
            $result = $this->pembelian_lain->update($id, $update);
            
            if(!$post['is_tempo']){
                $supplier = $post['customer'];
                if($potong_piutang > 0){
                    $piutang = $this->piutang->get_data_by_customer($supplier);
                    if($piutang != false){
                        # Ada data piutang
                        $insert_detail_piutang = array(
                            'id_piutang' => $piutang['id'],
                            'source' => $post['sumber'],
                            'id_source' => $post['id'],
                            'tgl_transaksi' => $post['tgl_bayar'],
                            'bayar_pokok' => str_replace(",", "", $post['potong_piutang']),
                            'remark' => 'Potong UM - '.$beli['kode'],
                            'jenis_pembayaran' => $post['jenis_pembayaran'],
                            'id_kas' => $kas,
                            'input_by' => get_session('auth')['id'],
                            'timestamp_input' => setNewDateTime(),
                        );
                        $this->piutang_detail->insert($insert_detail_piutang);
                    }
                }

                $pembelian = $this->pembelian_lain->get_data($id);
                if($result){
                    
                    if($potong_piutang > 0 && $pembelian['grandtotal'] > $potong_piutang){
                        # Jika masih ada kurang bayar 
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $pembelian['id_kas'];
                        $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $arus_kas['arus'] = "out";
                        $arus_kas['sumber'] = "pembelian-bahan";
                        $arus_kas['id_sumber'] = $post['id'];
                        $arus_kas['keterangan'] = 'Pembelian Bahan '.$pembelian['kode'];
                        $arus_kas['deskripsi'] = 'Potong UM : '.$potong_piutang.', Kurang Bayar : '.($beli['grandtotal'] - $potong_piutang);
                        $arus_kas['nominal'] = $subtotal;
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }else{
                        # Jika lunas 
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $pembelian['id_kas'];
                        $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $arus_kas['arus'] = "out";
                        $arus_kas['sumber'] = "pembelian-bahan";
                        $arus_kas['id_sumber'] = $post['id'];
                        $arus_kas['keterangan'] = 'Pembelian Bahan '.$pembelian['kode'];
                        $arus_kas['deskripsi'] = 'Bayar Lunas';
                        $arus_kas['nominal'] = $subtotal;
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }

                    if($pembelian['potongan'] > 0){
                        # Jika terdapat potongan pembelian, maka insert ke pendapatan lain (auto acc) dan buku kas
                        # Insert ke pendapatan
                        $pendapatan['id_kategori'] = $this->id_potongan_pembelian;
                        $pendapatan['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $pendapatan['nominal'] = $pembelian['potongan'];
                        $pendapatan['catatan'] = 'Potongan pembelian '.$pembelian['kode'];
                        $pendapatan['jenis_pembayaran'] = $pembelian['jenis_pembayaran'];
                        $pendapatan['id_kas'] = $pembelian['id_kas'];
                        $pendapatan['is_verifikasi'] = 1;
                        $pendapatan['input_by'] = get_session('auth')['id'];
                        $pendapatan['timestamp_input'] = setNewDateTime();
                        $pendapatan['verifikasi_by'] = get_session('auth')['id'];
                        $pendapatan['timestamp_verifikasi'] = setNewDateTime();
                        $result = $this->pendapatan->insert($pendapatan);

                        # Insert ke buku kas
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $pembelian['id_kas'];
                        $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $arus_kas['arus'] = "in";
                        $arus_kas['sumber'] = "pendapatan-pembelian-bahan";
                        $arus_kas['id_sumber'] = $pembelian['id'];
                        $arus_kas['keterangan'] = 'Potongan pembelian '.$pembelian['kode'];
                        $arus_kas['deskripsi'] = '';
                        $arus_kas['nominal'] = $pembelian['potongan'];
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }

                    if($pembelian['biaya_tambahan'] > 0){
                        # Jika terdapat biaya tambahan pembelian, maka insert ke beban pembelian (auto acc) dan buku kas
                        # Insert ke pendapatan
                        $beban['id_beban'] = $this->id_biaya_tambahan;
                        $beban['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $beban['nominal'] = $pembelian['biaya_tambahan'];
                        $beban['catatan'] = 'Biaya tambahan pembelian '.$pembelian['kode'];
                        $beban['jenis_pembayaran'] = $pembelian['jenis_pembayaran'];
                        $beban['id_kas'] = $pembelian['id_kas'];
                        $beban['is_verifikasi'] = 1;
                        $beban['input_by'] = get_session('auth')['id'];
                        $beban['timestamp_input'] = setNewDateTime();
                        $beban['verifikasi_by'] = get_session('auth')['id'];
                        $beban['timestamp_verifikasi'] = setNewDateTime();
                        $result = $this->beban->insert($beban);

                        # Insert ke buku kas
                        $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                        $arus_kas['id_kas'] = $pembelian['id_kas'];
                        $arus_kas['tgl_transaksi'] = $pembelian['tgl_bayar'];
                        $arus_kas['arus'] = "out";
                        $arus_kas['sumber'] = "beban-pembelian-bahan";
                        $arus_kas['id_sumber'] = $pembelian['id'];
                        $arus_kas['keterangan'] = 'Biaya tambahan pembelian '.$pembelian['kode'];
                        $arus_kas['deskripsi'] = '';
                        $arus_kas['nominal'] = $pembelian['biaya_tambahan'];
                        $arus_kas['input_by'] = get_session('auth')['id'];
                        $arus_kas['input_timestamp'] = setNewDateTime();
                        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                        $result = json_decode(xcurl($param_curl)['response']);
                    }
                    
                    # Tambah stok dari pembelian bahan
                    foreach($datadetail as $key => $row) {
                        $stok_bertambah = array(
                            'tanggal' => $row['timestamp_input'],
                            'sumber' => $post['sumber'],
                            'id_sumber' => $post['id'],
                            'id_jenis_bahan' => $row['id_jenis_bahan'],
                            'id_supplier' => $post['customer'],
                            'stok' => $row['kuantitas'],
                            'stok_sisa' => $row['kuantitas'],
                            'harga_satuan' => $row['harga_satuan'],
                            'input_by' => $row['input_by'],
                            'timestamp_input' => setNewDateTime()
                        );
                        
                        $this->stok_bahan->insert($stok_bertambah);
                        
                        #tambah log di transaksi stok bahan
                    
                        $trx = array(
                            'id_stok' => $this->db->insert_id(),
                            'tgl' => $row['timestamp_input'],
                            'arus' => 'in',
                            'kuantitas' => $row['kuantitas'],
                            'harga' => $row['harga_satuan'],
                            'sumber' => $post['sumber'],
                            'timestamp' => setNewDateTime()
                        );
                        $this->stok_bahan_trx->insert($trx);
                    }
                }
            }
            
			
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('pembelian/lain/persetujuan?'.$post['url']);
    }
}
