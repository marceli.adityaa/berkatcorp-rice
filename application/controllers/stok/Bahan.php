<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Bahan extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('customer_model', 'customer');
        $this->load->model('stok_bahan_model', 'stok_bahan');
        $this->load->model('stok_bahan_trx_model', 'stok_bahan_trx');
        $this->load->model('jenis_bahan_model', 'jenis_bahan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->role = json_decode($this->session->auth['role'])->bas;
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('kll-stk-gb', $hak->bas) || $hak->bas[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 'all';
            }
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['bahan'] = $this->jenis_bahan->get_active();
            $data['status'] = array(1 => "Stok tersedia", 2 => "Stok Kosong");
            $data['pengeringan'] = array("dryer", "jemur");
            $data['proses'] = array("jemur", "tapel", "giling");
            $data['transaksi'] = $this->user->join($this->stok_bahan->get_data_trx(), 'submit_by', 'input_by');
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Stok Bahan' => 'active', 'Kelola Stok' => 'active'));
            set_session('title', 'Kelola Stok Bahan');
            set_activemenu('sub-stok-bahan', 'menu-kelola-stok-bahan');
            $this->render('stok/bahan/v-kelola-stok', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rkp-stk-gb', $hak->bas) || $hak->bas[0] == '*') {
            $data['role'] = $this->role;
            $data['customer'] = $this->customer->get_active();
            foreach($data['customer'] as $row){
                $data['list_supplier'][$row['id']] = $row;
            }
            $data['bahan'] = $this->jenis_bahan->get_active();
            if(!empty($this->input->get())){
                $data['stok'] = $this->user->join($this->stok_bahan->get_data_rekap(), 'submit_by', 'input_by');
                // dump($data['stok']);
            }else{
                $data['stok'] = array();
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Stok Bahan' => 'active', 'Rekap Stok' => 'active'));
            set_session('title', 'Rekap Stok Bahan');
            set_activemenu('sub-stok-bahan', 'menu-rekap-stok-bahan');
            $this->render('stok/bahan/v-rekap-stok', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }


    public function submit_stok_bahan(){
        $post = $this->input->post();
        $post['sumber'] = 'master-transaksi';
        $post['input_by'] = get_session('auth')['id'];
        $post['timestamp_input'] = setNewDateTime();
        $url = $post['url'];
        unset($post['url']);
        if(empty($post['id'])){
            # Insert statement
            $result = $this->stok_bahan->insert($post);
            $trx = array(
                'id_stok' => $this->db->insert_id(),
                'tgl' => $post['tanggal'],
                'arus' => 'in',
                'kuantitas' => $post['stok'],
                'harga' => $post['harga_satuan'],
                'sumber' => 'master-transaksi',
                'timestamp' => setNewDateTime()
            );
            $this->stok_bahan_trx->insert($trx);
            if ($result) {
                $this->message('Berhasil memasukkan data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
        }else{
            # Update statement
            $id = $post['id'];
            unset($post['id']);
            # Hapus data stok bahan trx
            $this->stok_bahan_trx->hapus_trx_stok($id);
            $result = $this->stok_bahan->update($id, $post);
            $trx = array(
                'id_stok' => $id,
                'tgl' => $post['tanggal'],
                'arus' => 'in',
                'kuantitas' => $post['stok'],
                'harga' => $post['harga_satuan'],
                'sumber' => 'master-transaksi',
                'timestamp' => setNewDateTime()
            );
            $this->stok_bahan_trx->insert($trx);
            if ($result) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
        }

        $this->go('stok/bahan/kelola?'.$url);
    }

    public function export(){
        $get                = $this->input->get();
        $data['transaksi']  = $this->user->join($this->stok_bahan->get_data_rekap(), 'submit_by', 'input_by');
        $data['detail']     = $this->stok_bahan_trx->get_detail_rekap_stok($get);
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Stok Bahan');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        $nilai_sisa = 0;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Bahan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Awal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Bertambah');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Berkurang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Sisa');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nilai Sisa Stok');
        $baris++;
                
        if(!empty($data['transaksi'])){
            foreach($data['transaksi'] as $d){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($d['bahan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['avg_harga_pokok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jml_stok_awal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jml_stok_bertambah']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($d['jml_stok_terpakai']*-1));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jml_sisa_stok']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($d['jml_sisa_stok']*$d['avg_harga_pokok']));
                $baris++;
                $nilai_sisa += ($d['jml_sisa_stok']*$d['avg_harga_pokok']);
            }
        }

        # Set subtotal
        $baris++;
        $kolom = 1;
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($nilai_sisa));

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;

        # New Sheet
        $worksheet = new Worksheet($spreadsheet, 'Detail Transaksi');
        $spreadsheet->addSheet($worksheet, $idx);
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bahan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Bertambah');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Berkurang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
        $baris++;
        
        if(!empty($data['detail'])){
            foreach($data['detail'] as $dt){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, date("Y-m-d", strtotime($dt['tgl'])));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['sumber']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($dt['bahan']));
                if($dt['arus'] == 'in'){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kuantitas']);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($dt['kuantitas']*-1));
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['harga']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($dt['kuantitas']*$dt['harga']));
                $baris++;
            }
        }

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        # SAVE
        $filename = date("Ymd", strtotime($get['start'])).'_'.date("Ymd", strtotime($get['end']));
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_stok_bahan_'.$filename.'.xlsx"');
		$writer->save("php://output");
    }

    public function export_detail(){
        $get                = $this->input->get();
        $data['detail']     = $this->stok_bahan_trx->get_detail_rekap_stok($get);
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Daftar Transaksi');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        $nilai_sisa = 0;
       
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supplier');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Bahan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Bertambah');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Stok Berkurang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Harga Pokok');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
        $baris++;
        
        if(!empty($data['detail'])){
            foreach($data['detail'] as $dt){
                $kolom = 1;
                $supplier = "master";
                if(!empty($dt['supplier'])){
                    $supplier = $dt['supplier']; 
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, date("Y-m-d", strtotime($dt['tgl'])));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['sumber']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $supplier);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($dt['bahan']));
                if($dt['arus'] == 'in'){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['kuantitas']);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($dt['kuantitas']*-1));
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $dt['harga']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($dt['kuantitas']*$dt['harga']));
                $baris++;
            }
        }

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        # SAVE
        $filename = date("Ymd", strtotime($get['start'])).'_'.date("Ymd", strtotime($get['end']));
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="detail_trx_stok_bahan_'.$filename.'.xlsx"');
		$writer->save("php://output");
    }
}
