@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/beras/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                        
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Merk</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="merk" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($merk as $key => $row){
                                        if(isset($get['merk']) && $get['merk'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button> 
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Produksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_produksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Merk</th>
                    <th data-sortable="true">Jumlah Cor</th>
                    <th data-sortable="true">Kuantitas Naik</th>
                    <th data-sortable="true">Sisa Produksi</th>
                    <th data-sortable="true">Hasil Produksi</th>
                    <th data-sortable="true">Susut</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('produksi/beras/detail_produksi/'.$row['id'])."'target='_blank'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Tugas'><i class='fa fa-sign-in'></i></button></a> ";
                            if($row['persetujuan'] != 1){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit tugas' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['total_kuantitas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Input Data</label></td>";
                            }else if($row['total_kuantitas'] > 0 && $row['status'] == 0 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-warning'>Proses Produksi</label></td>";
                            }else if($row['total_kuantitas'] > 0 && $row['status'] == 1 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-primary'>Menunggu Persetujuan</label></td>";
                            }else if($row['status'] == 1 && $row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td>".date("Y-m-d", strtotime($row['tanggal']))."<br></td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['merk'])."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['jumlah_cor']."</label></td>";
                            echo "<td>".monefy($row['total_kuantitas'], false)."</td>";
                            echo "<td>".monefy($row['sisa_produksi'], false)."</td>";
                            if (array_key_exists($row['id'], $hasil)){
                                echo "<td>".monefy($hasil[$row['id']]['tonase_hasil'], false)."</td>";
                                $susut = ($row['total_kuantitas'] - ($hasil[$row['id']]['tonase_hasil'] + $row['sisa_produksi']))/$row['total_kuantitas'] * 100;
                                echo "<td><label class='badge badge-danger'>".round($susut*-1,2)."%</label></td>";
                                
                            }else{
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                            }
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data produksi</p>
        @endif
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_produksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let merk = $("#form-filter [name=merk]").val();
    // if (start != '' && end != '') {
        var url = "<?= base_url('produksi/beras/export?')?>"+'start='+start+'&end='+end+'&merk='+merk;
        window.open(url, '_blank');
    // } else {
        // Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}


</script>
@end