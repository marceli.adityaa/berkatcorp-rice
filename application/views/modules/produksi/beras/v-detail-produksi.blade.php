@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi Produksi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$tugas->tanggal}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Merk</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{ucwords($tugas->merk)}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jumlah Cor</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{ucwords($tugas->jumlah_cor)}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Keterangan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$tugas->keterangan}}" readonly>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($tugas->status == 0)
                            <button type="button" class="btn btn-primary btn-success" id="proses" data-id="{{$tugas->id}}"><i class="fa fa-send mg-r-10"></i>Selesai</button>
                            @elseif($tugas->status == 1 && $tugas->persetujuan != 1)
                            <button type="button" class="btn btn-danger btn-success" id="batal" data-id="{{$tugas->id}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Bahan Produksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($tugas->status == 0)
            <button class="btn btn-primary" id="tambah_bahan">+ Tambah Bahan</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_bahan">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th>Proses</th>
                    <th>Bahan</th>
                    <th>Kuantitas Cor</th>
                    <th>Kuantitas Total</th>
                    <th>Harga</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $jumlah_cor = $tugas->jumlah_cor;
                    $bahan['kuantitas'] = 0;
                    $bahan['kuantitas_sisa'] = 0;
                    $bahan['nilai_stok'] = 0;
                    $bahan['nilai_stok_sisa'] = 0;
                    if(!empty($detail_bahan)){
                        foreach($detail_bahan as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($tugas->status == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_bahan(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['arus'] == 'in'){
                                echo "<td><label class='badge badge-light'>".ucwords($row['proses'])."</label><br><label class='badge badge-primary'>Bahan Naik</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".ucwords($row['proses'])."</label><br><label class='badge badge-warning'>Bahan Keluar</label></td>";
                            }
                            if($row['is_return']){
                                echo "<td><label class='badge badge-secondary'>".ucwords($row['nama_return'])."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".ucwords($row['bahan'])."</label></td>";
                            }

                            if($row['arus'] == 'in'){
                                echo "<td>".monefy($row['kuantitas'], false)."</td>";
                                if($row['is_multiply_cor']){
                                    $row['kuantitas'] *= $jumlah_cor;
                                }
                                echo "<td>".monefy($row['kuantitas'], false)."</td>";
                                echo "<td>".monefy($row['harga'], false)."</td>";
                                echo "<td>".monefy(($row['kuantitas'] * $row['harga']), false)."</td>";
                                
                                $bahan['kuantitas'] += $row['kuantitas'];
                                $bahan['nilai_stok'] += ($row['kuantitas'] * $row['harga']);
                            }else{
                                echo "<td><label class='tx-danger'>".monefy($row['kuantitas']*-1, false)."</label></td>";
                                if($row['is_multiply_cor']){
                                    $row['kuantitas'] *= $jumlah_cor;
                                }
                                echo "<td><label class='tx-danger'>".monefy($row['kuantitas']*-1, false)."</label></td>";
                                echo "<td>".monefy($row['harga'], false)."</td>";
                                echo "<td><label class='tx-danger'>".monefy(($row['kuantitas'] * $row['harga'] * -1), false)."</label></td>";
                                
                                $bahan['kuantitas'] -= $row['kuantitas'];
                                $bahan['nilai_stok'] -= ($row['kuantitas'] * $row['harga']);
                                
                            }
                            echo "</tr>";
                            
                        }
                    }
                    if(!empty($detail_sisa)){
                        foreach($detail_sisa as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($tugas->status == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_bahan(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['proses'])."</label><br><label class='badge badge-secondary'>Sisa Produksi</label></td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['bahan'])."</label></td>";
                   
                            echo "<td><label class='tx-danger'>".monefy($row['kuantitas']*-1, false)."</label></td>";

                            echo "<td><label class='tx-danger'>".monefy($row['kuantitas']*-1, false)."</label></td>";
                            echo "<td>".monefy($row['harga'], false)."</td>";
                            echo "<td><label class='tx-danger'>".monefy(($row['kuantitas'] * $row['harga'] * -1), false)."</label></td>";
                            if($row['is_sisa_produksi']){
                                $bahan['kuantitas_sisa'] += $row['kuantitas'];
                                $bahan['nilai_stok_sisa'] += ($row['kuantitas'] * $row['harga']);
                            }else{
                                $bahan['kuantitas'] -= $row['kuantitas'];
                                $bahan['nilai_stok'] -= ($row['kuantitas'] * $row['harga']);
                            }
                            echo "</tr>";
                            
                        }
                    }
				?>
                @if($bahan['kuantitas'] > 0)
                <tr class="tx-18">
                    <td colspan="7"><small>Total Kuantitas Naik</small></td>
                    <td>
                        <b>{{monefy($bahan['kuantitas'], false)}}</b> <small>Kg</small>
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="7"><small>Total Nilai Bahan Naik</small></td>
                    <td>
                        <b>{{monefy($bahan['nilai_stok'], false)}}</b> 
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="7"><small>Total Sisa Produksi</small></td>
                    <td>
                        <b>{{monefy($bahan['kuantitas_sisa'], false)}}</b> <small>Kg</small>
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="7"><small>Total Nilai Sisa Produksi</small></td>
                    <td>
                        <b>{{monefy($bahan['nilai_stok_sisa'], false)}}</b> 
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="7"><small>Harga Pokok Bahan</small></td>
                    <td>
                        <b>{{monefy($bahan['nilai_stok'] / $bahan['kuantitas'], false)}}</b> 
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Hasil Produksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($tugas->status == 0)
            <button class="btn btn-primary" id="tambah_hasil">+ Tambah Data</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_hasil">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Merk</th>
                    <th data-sortable="true">Kemasan</th>
                    <th data-sortable="true">Biaya Sak</th>
                    <th data-sortable="true">Biaya Transport</th>
                    <th data-sortable="true">Kuantitas (sak)</th>
                    <th data-sortable="true">Tonase (kg)</th>
                    <th data-sortable="true">Harga Pokok Produk</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($kuantitas_hasil)){
                        // dump($bahan);
                        $hasil['hpp'] = ($bahan['nilai_stok'] - $bahan['nilai_stok_sisa']) / $kuantitas_hasil;
                        // dump($hasil['hpp']);
                    }else{
                        $hasil['hpp'] = 0;
                    }
                    if(!empty($detail_hasil)){
                        foreach($detail_hasil as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($tugas->status == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_hasil(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            echo "<td>".ucwords($tugas->merk)."</td>";
                            echo "<td><label class='badge badge-light'>@".ucwords($row['kemasan'])." KG</label></td>";
                            echo "<td>".monefy($row['biaya_sak'], false)."</td>";
                            echo "<td>".monefy($row['biaya_transport'], false)."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>".monefy(($row['kuantitas'] * $row['kemasan']), false)."</td>";
                            echo "<td>".monefy(($hasil['hpp'] + $row['biaya_sak'] + $row['biaya_transport']), false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                @if($kuantitas_hasil > 0)
                <tr class="tx-18">
                    <td colspan="8"><small>Total Kuantitas Produksi</small></td>
                    <td>
                        <b>{{monefy($kuantitas_hasil, false)}}</b> <small>Kg</small><br>
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="8"><small>Harga Pokok Bahan</small></td>
                    <td>
                        <b>{{monefy($hasil['hpp'], false)}}</b> 
                    </td>
                </tr>
                <tr class="tx-18">
                    <td colspan="8"><small>Susut</small></td>
                    <td>
                        <b>{{monefy(($bahan['kuantitas'] - ($kuantitas_hasil + $bahan['kuantitas_sisa'])) / $bahan['kuantitas']*100, 2)}}%</b> 
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_bahan" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Bahan Produksi</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-bahan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/beras/submit_data_bahan')}}" id="form-bahan" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_produksi" value="{{$tugas->id}}">
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Nama Proses <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_proses" class="form-control reset-form select2-bahan" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($proses as $row)
                                                <option value="{{$row['id']}}" data-return="{{$row['is_return']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10 not-return">
                                    <label class="col-sm-4 form-control-label">Jenis Bahan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_bahan" class="form-control reset-form select2-bahan">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($bahan as $row)
                                                @if(!empty($row['nama']))
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10 return">
                                    <label class="col-sm-4 form-control-label">Nama Bahan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="nama_return"> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kuantitas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="kuantitas" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Harga <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="number" class="form-control" name="harga" required=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label">Subtotal <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <label class="form-control" name="subtotal"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_hasil" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Hasil Produksi</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-bahan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/beras/submit_data_hasil')}}" id="form-hasil" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_produksi" value="{{$tugas->id}}">
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Nama Merk <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <label class="form-control">{{ucwords($tugas->merk)}}</label>
                                    </div>
                                </div>

                                <div class="row mg-t-10 not-return">
                                    <label class="col-sm-4 form-control-label">Kemasan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="number" class="form-control" name="kemasan" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Biaya Sak <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="number" class="form-control" name="biaya_sak" required=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Biaya Transport <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="number" class="form-control" name="biaya_transport" required=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kuantitas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="kuantitas" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">sak</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label">Tonase <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group">
                                            <label class="form-control" name="tonase"></label> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {

    $('.select2').select2({
    });

    $('.select2-bahan').select2({
        dropdownParent: $('#modal_bahan')
    });
    $('.penggilingan, .proses').hide();
    bahan_return();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah_bahan').click(function() {
    $('#form-bahan').trigger('reset');
    $('[name=id_produksi]').val({{$tugas->id}});
    $("#modal_bahan").modal('show');
});

$('#tambah_hasil').click(function() {
    $('#form-hasil').trigger('reset');
    $('[name=id_produksi]').val({{$tugas->id}});
    $("#modal_hasil").modal('show');
});

$('#form-bahan [name=id_proses]').change(function(){
    bahan_return();
});

$('#form-bahan [name=kuantitas], #form-bahan [name=harga]').on('change keyup', function(){
    let bahan_qty = getNum(parseInt($('#form-bahan [name=kuantitas]').val()));
    let bahan_harga = getNum(parseInt($('#form-bahan [name=harga]').val()));
    let bahan_subtotal = bahan_qty * bahan_harga;
    
    $('#form-bahan [name=subtotal]').empty().html(bahan_subtotal.toLocaleString());
});

$('#form-hasil [name=kuantitas], #form-hasil [name=kemasan]').on('change keyup', function(){
    let hasil_qty = getNum(parseInt($('#form-hasil [name=kuantitas]').val()));
    let hasil_kemasan = getNum(parseInt($('#form-hasil [name=kemasan]').val()));
    let hasil_subtotal = hasil_qty * hasil_kemasan;
    
    $('#form-hasil [name=tonase]').empty().html(hasil_subtotal.toLocaleString());
});

function bahan_return(){
    let retur = $('#form-bahan [name=id_proses] option:selected').data('return');
    if(retur){
        $('.return').show();
        $('.not-return').hide();
    }else{
        $('.return').hide();
        $('.not-return').show();
    }
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/submit_pengajuan_produksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    window.location.href = result.redirect;
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan pengajuan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/batal_pengajuan_produksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})


function hapus_bahan(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/delete_data_bahan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function hapus_hasil(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/delete_data_hasil')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end