@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/beras/kelola?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                        
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Merk</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="merk" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($merk as $key => $row){
                                        if(isset($get['merk']) && $get['merk'] == $key){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas Produksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_produksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Merk</th>
                    <th data-sortable="true">Jumlah Cor</th>
                    <th data-sortable="true">Kuantitas Naik</th>
                    <th data-sortable="true">Sisa Produksi</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('produksi/beras/detail_hasil/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Tugas'><i class='fa fa-sign-in'></i></button></a> ";
                            if($row['persetujuan'] == 0){
                                # Setuju
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }else if($row['persetujuan'] == 2){
                                # Tidak setuju
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "</td>";
                            if($row['total_kuantitas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Input Data</label></td>";
                            }else if($row['total_kuantitas'] > 0 && $row['status'] == 0 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-warning'>Proses Produksi</label></td>";
                            }else if($row['total_kuantitas'] > 0 && $row['status'] == 1 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-primary'>Menunggu Persetujuan</label></td>";
                            }else if($row['status'] == 1 && $row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td>".date("Y-m-d", strtotime($row['tanggal']))."<br></td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['merk'])."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['jumlah_cor']."</label></td>";
                            echo "<td>".monefy($row['total_kuantitas'], false)."</td>";
                            echo "<td>".monefy($row['sisa_produksi'], false)."</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data produksi</p>
        @endif
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_produksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});


function setuju(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Stok akan berubah sesuai data produksi, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/approve_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/beras/reject_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end