@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/penggilingan/persetujuan?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas Penggilingan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_penggilingan">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal Mulai</th>
                    <th data-sortable="true">Tanggal Selesai</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Beras Medium</th>
                    <th data-sortable="true">Menir</th>
                    <th data-sortable="true">Katul</th>
                    <th data-sortable="true">Sekam</th>
                    <th data-sortable="true">Susut</th>
                    <th data-sortable="true">Broken</th>
                    <th data-sortable="true">Rendemen KG</th>
                    <th data-sortable="true">Berat Kepala</th>
                    <th data-sortable="true">Berat Broken</th>
                    <th data-sortable="true">Petugas</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['persetujuan'] == 1){
                                # Setuju
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }else if($row['persetujuan'] == 3){
                                # Tidak setuju
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "<a href='".base_url('produksi/penggilingan/detail_hasil_penggilingan/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Tugas'><i class='fa fa-sign-in'></i></button></a> ";
                            echo "</td>";
                            if($row['kuantitas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Input data</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-info'>Proses Penggilingan</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-primary'>Menunggu Persetujuan</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".date("Y-m-d H:i", strtotime($row['tanggal_mulai']))."</label></td>";
                            if($row['hasil_beras'] != 0){
                            echo "<td><label class='badge badge-dark'>".date("Y-m-d H:i", strtotime($row['tanggal_selesai']))."<label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'><i>Menunggu update</i></label></td>";
                            }
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            if($row['hasil_beras'] != 0){
                                if($row['harga_medium'] > 0){
                                    echo "<td>".monefy($row['hasil_beras'], false)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_medium'], false)."</label></td>";

                                }else{
                                    echo "<td>".monefy($row['hasil_beras'], false)."</td>";
                                }
                                echo "<td>".monefy($row['hasil_menir'], false)."</td>";
                                echo "<td>".monefy($row['hasil_katul'], false)."</td>";
                                echo "<td>".monefy($row['hasil_sekam'], false)."</td>";
                                echo "<td><label class='tx-warning'>-".monefy(($row['kuantitas'] - $row['hasil_beras'] - $row['hasil_menir'] - $row['hasil_katul'] - $row['hasil_sekam']), false)." Kg</label></td>";
                                echo "<td>".$row['broken']." %</td>";
                                echo "<td><label class='badge badge-warning'>".round(($row['hasil_beras']/$row['kuantitas'] * 100), 1)." %</label></td>";
                            }else{
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                            }
                            if($row['harga_kepala'] > 0){
                                echo "<td>".round((100 - $row['broken']) / 100 * $row['hasil_beras'], 1)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_kepala'], false)."</label></td>";
                            }else{
                                echo "<td>".round((100 - $row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            }
                            if($row['harga_broken'] > 0){
                                echo "<td>".round(($row['broken']) / 100 * $row['hasil_beras'], 1)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_broken'], false)."</label></td>";
                            }else{
                                echo "<td>".round(($row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            }
                            echo "<td><label class='badge badge-light'>".ucwords($row['petugas'])."</label></td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data penggilingan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Penggilingan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/penggilingan/approve_transaksi')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-3 form-control-label">Tanggal Mulai <span class="tx-danger"></span></label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="tanggal_mulai"></label>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Tanggal Selesai <span class="tx-danger"></span></label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="tanggal_selesai"></label>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Petugas <span class="tx-danger"></span></label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="petugas"></label>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="keterangan"></label>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Gabah KG</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="kuantitas"></label> 
                                    </div>
                                    <label class="col-sm-3 form-control-label">Broken</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="broken"></label>
                                        <input type="hidden" name="broken"> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Hasil Beras Medium</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="hasil_beras"></label>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Hasil Menir</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="hasil_menir"></label>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Hasil Katul</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="hasil_katul"></label>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Hasil Sekam</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="hasil_sekam"></label>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Berat Beras Kepala</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="berat_kepala"></label>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Berat Beras Broken</label>
                                    <div class="col-sm-3">
                                        <label class="form-control" id="berat_broken"></label>
                                    </div>
                                </div>
                                <hr>
                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Harga Medium</label>
                                    <div class="col-sm-9">
                                    <input type="number" class="form-control" name="harga_medium" id="harga_medium" readonly="" autocomplete="off"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Harga Kepala <span class="tx-danger"></span></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="harga_kepala" autocomplete="off"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-3 form-control-label">Harga Broken <span class="tx-danger"></span></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="harga_broken" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <label class="col-sm-3 form-control-label"></label>
                                    <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                                        <button type="button" class="btn btn-info" onclick="konfirmasi(this)"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                                <hr>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_penggilingan').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

$('[name=harga_kepala]').on('change, keyup', function(e){
    var hrg_medium = parseInt($('#harga_medium').val());
    var hrg_kepala = parseInt($('[name=harga_kepala]').val());
    var broken = parseInt($('[name=broken]').val());
    var hrg_broken = Math.round(((hrg_medium - ((100 - broken) / 100 * hrg_kepala)) / broken) * 100);
    $('[name=harga_broken').val(hrg_broken);
})

$('[name=harga_broken]').on('change, keyup', function(e){
    var hrg_medium = parseInt($('#harga_medium').val());
    var hrg_broken = parseInt($('[name=harga_broken]').val());
    var broken = parseInt($('[name=broken]').val());
    var hrg_kepala = Math.round((hrg_medium - (broken / 100 * hrg_broken)) / (100-broken) * 100);
    $('[name=harga_kepala').val(hrg_kepala);
})


function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/penggilingan/delete_data_tugas')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function setuju(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/produksi/penggilingan/json_get_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                if(result.keterangan == ""){
                    result.keterangan = "-";
                }
                var medium = Math.round(parseInt(result.kuantitas) * parseInt(result.hpp_awal) / parseInt(result.hasil_beras));
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi #tanggal_mulai').empty().html(result.tanggal_mulai);
                $('#form-transaksi #petugas').empty().html(result.petugas);
                $('#form-transaksi #keterangan').empty().html(result.keterangan);
                $('#form-transaksi #kuantitas').empty().html(result.kuantitas);
                if(result.tanggal_selesai == '0000-00-00 00:00:00'){
                    $('#form-transaksi #tanggal_selesai').empty();
                }else{
                    $('#form-transaksi #tanggal_selesai').empty().html(result.tanggal_selesai);
                }
                $('#form-transaksi #hasil_beras').empty().html(result.hasil_beras);
                $('#form-transaksi #hasil_menir').empty().html(result.hasil_menir);
                $('#form-transaksi #hasil_katul').empty().html(result.hasil_katul);
                $('#form-transaksi #hasil_sekam').empty().html(result.hasil_sekam);
                $('#form-transaksi #broken').empty().html(result.broken+" %");
                $('#form-transaksi [name=broken]').empty().val(result.broken);
                $('#form-transaksi #berat_kepala').empty().html(round(((100-result.broken)/100 * result.hasil_beras),1));
                $('#form-transaksi #berat_broken').empty().html(round(((result.broken)/100 * result.hasil_beras),1));
                $('#form-transaksi #harga_medium').empty().val(medium);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function konfirmasi(el){
    if($('[name=harga_kepala').val() != '' && $('[name=harga_broken]').val() != ''){
        Swal.fire({
            title: 'Konfirmasi',
            text: "Data yang diinput tidak dapat diubah, lanjutkan?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $('#form-transaksi').submit();
            }
        })
    }else{
        swal.fire('Perhatian','Mohon isi data harga terlebih dahulu');
    }
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/penggilingan/reject_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}
</script>
@end