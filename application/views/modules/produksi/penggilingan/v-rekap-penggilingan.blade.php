@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/penggilingan/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal penggilingan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-2 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas Penggilingan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
        <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal Mulai</th>
                    <th data-sortable="true">Tanggal Selesai</th>
                    <th data-sortable="true">Gabah KG</th>
                    <th data-sortable="true">Beras Medium</th>
                    <th data-sortable="true">Menir</th>
                    <th data-sortable="true">Katul</th>
                    <th data-sortable="true">Sekam</th>
                    <th data-sortable="true">Susut</th>
                    <th data-sortable="true">Broken</th>
                    <th data-sortable="true">Rendemen KG</th>
                    <th data-sortable="true">Berat Kepala</th>
                    <th data-sortable="true">Berat Broken</th>
                    <th data-sortable="true">Petugas</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total_gabah = 0;
                    $total_giling = 0;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".date("Y-m-d H:i", strtotime($row['tanggal_mulai']))."</label></td>";
                            if($row['hasil_beras'] != 0){
                            echo "<td><label class='badge badge-dark'>".date("Y-m-d H:i", strtotime($row['tanggal_selesai']))."<label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'><i>Menunggu update</i></label></td>";
                            }
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            if($row['hasil_beras'] != 0){
                                if($row['harga_medium'] > 0){
                                    echo "<td>".monefy($row['hasil_beras'], false)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_medium'], false)."</label></td>";

                                }else{
                                    echo "<td>".monefy($row['hasil_beras'], false)."</td>";
                                }
                                echo "<td>".monefy($row['hasil_menir'], false)."</td>";
                                echo "<td>".monefy($row['hasil_katul'], false)."</td>";
                                echo "<td>".monefy($row['hasil_sekam'], false)."</td>";
                                echo "<td><label class='tx-warning'>-".monefy(($row['kuantitas'] - $row['hasil_beras'] - $row['hasil_menir'] - $row['hasil_katul'] - $row['hasil_sekam']), false)." Kg</label></td>";
                                echo "<td>".$row['broken']." %</td>";
                                echo "<td><label class='badge badge-warning'>".round(($row['hasil_beras']/$row['kuantitas'] * 100), 1)." %</label></td>";
                            }else{
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                            }
                            if($row['harga_kepala'] > 0){
                                echo "<td>".round((100 - $row['broken']) / 100 * $row['hasil_beras'], 1)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_kepala'], false)."</label></td>";
                            }else{
                                echo "<td>".round((100 - $row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            }
                            if($row['harga_broken'] > 0){
                                echo "<td>".round(($row['broken']) / 100 * $row['hasil_beras'], 1)."<br><label class='badge badge-primary'>Rp ".monefy($row['harga_broken'], false)."</label></td>";
                            }else{
                                echo "<td>".round(($row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            }
                            echo "<td><label class='badge badge-light'>".ucwords($row['petugas'])."</label></td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                            $total_gabah += $row['kuantitas'];
                            $total_giling += $row['hasil_beras'];
                        }
                    }
				?>
            </tbody>
        </table>
        <table class="table table-striped mg-t-10 table-white">
            <tr class="tx-bold tx-16">
                <td>Total Gabah Digiling</td>
                <td>{{monefy($total_gabah, false)}}</td>
            </tr>
            <tr class="tx-bold tx-16">
                <td>Total Hasil Giling</td>
                <td>{{monefy($total_giling, false)}}</td>
            </tr>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data penggilingan</p>
        @endif
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    // if (start != '' && end != '') {
        var url = "<?= base_url('produksi/penggilingan/export?')?>"+'start='+start+'&end='+end;
        window.open(url, '_blank');
    // } else {
        // Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}

</script>
@end