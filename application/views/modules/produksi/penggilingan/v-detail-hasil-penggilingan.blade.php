@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi Tugas Penggilingan
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{$detail['tanggal_mulai']}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Tanggal Selesai</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{$detail['tanggal_selesai']}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Petugas</label>
                        <div class="col-sm-3">
                        <label class="form-control">{{ucwords($detail['petugas'])}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Keterangan</label>
                        <div class="col-sm-3">
                            @if(!empty($detail['keterangan']))
                            <label class="form-control">{{ucwords($detail['keterangan'])}} </label>
                            @else
                            <label class="form-control">-</label>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Total Gabah</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['kuantitas'], false)}} Kg</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Penyusutan</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy(($detail['kuantitas'] - $detail['hasil_beras'] - $detail['hasil_menir'] - $detail['hasil_katul'] - $detail['hasil_sekam']), false)}} Kg</label>
                        </div>
                       
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Hasil Beras Medium</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['hasil_beras'], false)}} Kg</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Hasil Menir</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['hasil_menir'], false)}} Kg</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Hasil Katul</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['hasil_katul'], false)}} Kg</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Hasil Sekam</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['hasil_sekam'], false)}} Kg</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Broken</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{$detail['broken']}} %</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Rendemen KG</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{round(($detail['hasil_beras']/$detail['kuantitas'] * 100), 1)}} %</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Berat Beras Kepala</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy(round((100 - $detail['broken']) / 100 * $detail['hasil_beras'], 1), false)}} Kg</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Berat Beras Broken</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy(round(($detail['broken']) / 100 * $detail['hasil_beras'], 1), false)}} Kg</label>
                        </div>
                    </div>
                    
                    @if ($admin)
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Harga Medium</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['kuantitas'] * $detail['hpp_awal'] / $detail['hasil_beras'], false)}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Nilai Stok</label>
                        <div class="col-sm-3">
                            <label class="form-control tx-18"><b>{{monefy($detail['harga_medium'] * $detail['hasil_beras'], false)}}</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Harga Kepala</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['harga_kepala'], false)}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Harga Broken</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['harga_broken'], false)}}</label>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Penggilingan Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($detail['kuantitas'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Gabah</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Penyimpanan</th>
                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Kuantitas</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    if(!empty($detail_tugas)){
                        foreach($detail_tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($detail['kuantitas'] == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            echo "<td>".strtoupper($row['tanggal'])."</td>";
                            if($row['id_supplier'] != 0){
                                echo "<td><label class='badge badge-light'>".$row['sumber']."</label><br><label class='badge badge-info'>".ucwords($list_supplier[$row['id_supplier']]['nama'])."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['sumber']."</label></td>";
                            }
                            if($row['jenis_proses'] == 'giling'){
                                echo "<td><label class='badge badge-warning'>".strtoupper($row['jenis_proses'])."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-primary'>".strtoupper($row['jenis_proses'])."</label></td>";
                            }
                            echo "<td>".strtoupper($row['jenis_gabah'])."</td>";
                            echo "<td>".strtoupper($row['padi'])."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            
                            echo "</tr>";
                            $total += $row['kuantitas'];
                        }
                    }
				?>
                <tr class="tx-18">
                    <td colspan="7"><small>Total Berat Gabah</small></td>
                    <td>
                        <b>{{monefy($total, false)}}</b> <small>Kg</small>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('#tabel-gabah').bootstrapTable({
        pagination: true,
        search: true,
        // toolbar: '#toolbar2'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});
</script>
@end