@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/penggilingan/hasil?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas penggilingan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal Mulai</th>
                    <th data-sortable="true">Tanggal Selesai</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Beras Medium</th>
                    <th data-sortable="true">Menir</th>
                    <th data-sortable="true">Katul</th>
                    <th data-sortable="true">Sekam</th>
                    <th data-sortable="true">Susut</th>
                    <th data-sortable="true">Broken</th>
                    <th data-sortable="true">Rendemen KG</th>
                    <th data-sortable="true">Berat Kepala</th>
                    <th data-sortable="true">Berat Broken</th>
                    <th data-sortable="true">Petugas</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('produksi/penggilingan/detail_hasil/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Tugas'><i class='fa fa-sign-in'></i></button></a> ";
                            if($row['persetujuan'] < 2){
                                echo "<button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Update Hasil Penggilingan' onclick='update(this)' data-id='".$row['id']."'><i class='fa fa-file-text-o'></i> Update</button> ";
                            }
                            echo "</td>";
                            if($row['kuantitas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Input data</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-info'>Proses Penggilingan</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-primary'>Menunggu Persetujuan</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".date("Y-m-d H:i", strtotime($row['tanggal_mulai']))."</label></td>";
                            if($row['hasil_beras'] != 0){
                            echo "<td><label class='badge badge-dark'>".date("Y-m-d H:i", strtotime($row['tanggal_selesai']))."<label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'><i>Menunggu update</i></label></td>";
                            }
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            if($row['hasil_beras'] != 0){
                                echo "<td>".monefy($row['hasil_beras'], false)."</td>";
                                echo "<td>".monefy($row['hasil_menir'], false)."</td>";
                                echo "<td>".monefy($row['hasil_katul'], false)."</td>";
                                echo "<td>".monefy($row['hasil_sekam'], false)."</td>";
                                echo "<td><label class='tx-warning'>-".monefy(($row['kuantitas'] - $row['hasil_beras'] - $row['hasil_menir'] - $row['hasil_katul'] - $row['hasil_sekam']), false)." Kg</label></td>";
                                echo "<td>".$row['broken']." %</td>";
                                echo "<td><label class='badge badge-warning'>".round(($row['hasil_beras']/$row['kuantitas'] * 100), 1)." %</label></td>";
                            }else{
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                                echo "<td>-</td>";
                            }
                            echo "<td>".round((100 - $row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            echo "<td>".round(($row['broken']) / 100 * $row['hasil_beras'], 1)."</td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['petugas'])."</label></td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data penggilingan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Update Tugas penggilingan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/penggilingan/submit_hasil_penggilingan')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Mulai <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datetime" name="tanggal_mulai" disabled=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Petugas <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="petugas" disabled=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off" disabled> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kuantitas <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="kuantitas" autocomplete="off" disabled> 
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Selesai <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control datetime" name="tanggal_selesai" required="" autocomplete="off"> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Hasil Beras <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="hasil_beras" autocomplete="off" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Kg</span>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Hasil Menir <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="hasil_menir" autocomplete="off"> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Hasil Katul <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="hasil_katul" autocomplete="off"> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Hasil Sekam <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="hasil_sekam" autocomplete="off"> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Broken <span class="tx-danger"></span></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="broken" autocomplete="off" step="0.1" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function update(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/produksi/penggilingan/json_get_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tanggal_mulai]').val(result.tanggal_mulai);
                $('#form-transaksi [name=petugas]').val(result.petugas);
                $('#form-transaksi [name=keterangan]').val(result.keterangan);
                $('#form-transaksi [name=kuantitas]').val(result.kuantitas);
                if(result.tanggal_selesai == '0000-00-00 00:00:00'){
                    $('#form-transaksi [name=tanggal_selesai]').val('');
                }else{
                    $('#form-transaksi [name=tanggal_selesai]').val(result.tanggal_selesai);
                }
                $('#form-transaksi [name=hasil_beras]').val(result.hasil_beras);
                $('#form-transaksi [name=hasil_menir]').val(result.hasil_menir);
                $('#form-transaksi [name=hasil_katul]').val(result.hasil_katul);
                $('#form-transaksi [name=hasil_sekam]').val(result.hasil_sekam);
                $('#form-transaksi [name=broken]').val(result.broken);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/penggilingan/delete_data_tugas')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end