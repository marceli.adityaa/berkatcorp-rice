@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/pengeringan/kelola?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Pengeringan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-2 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jenis Padi</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="padi" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($padi as $row){
                                        if(isset($get['padi']) && $get['padi'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Pengeringan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="pengeringan" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($pengeringan as $row){
                                        if(isset($get['pengeringan']) && $get['pengeringan'] == $row){
                                            echo '<option value="'.$row.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas Pengeringan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Tugas</button>
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal Mulai</th>
                    <th data-sortable="true">Pengeringan</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Petugas</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('produksi/pengeringan/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Tugas'><i class='fa fa-sign-in'></i></button></a> ";
                            if($row['persetujuan'] < 2){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit tugas' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['kuantitas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Input data</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-info'>Proses Pengeringan</label></td>";
                            }else if($row['kuantitas'] > 0 && $row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-primary'>Menunggu Persetujuan</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td>".date("Y-m-d H:i", strtotime($row['tanggal_mulai']))."<br></td>";
                            if($row['jenis_pengeringan'] == 'dryer'){
                                echo "<td><label class='badge badge-primary'>Dryer</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>Jemur</label></td>";
                            }
                            echo "<td>".$row['padi']."</td>";
                            echo "<td>".monefy($row['total_kuantitas'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['petugas'])."</label></td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pengeringan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Tugas Pengeringan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/pengeringan/submit_tambah_tugas')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Pengeringan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control datetime" name="tanggal_mulai" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jenis Padi <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_jenis_padi" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($padi as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Pengeringan<span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="jemur-1" class="custom-control-input" name="jenis_pengeringan" value="jemur" required="">
                                            <label class="custom-control-label" for="jemur-1">Jemur</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="dryer-1" class="custom-control-input" name="jenis_pengeringan" value="dryer" required="">
                                            <label class="custom-control-label" for="dryer-1">Dryer</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Petugas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="petugas" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/produksi/pengeringan/json_get_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tanggal_mulai]').val(result.tanggal_mulai);
                $('#form-transaksi [name=id_jenis_padi]').val(result.id_jenis_padi).change();
                if(result.jenis_pengeringan != ''){
                    $('#form-transaksi [name=jenis_pengeringan][value='+result.jenis_pengeringan+']').attr('checked', true);
                }
                $('#form-transaksi [name=petugas]').val(result.petugas);
                $('#form-transaksi [name=keterangan]').val(result.keterangan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/delete_data_tugas')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end