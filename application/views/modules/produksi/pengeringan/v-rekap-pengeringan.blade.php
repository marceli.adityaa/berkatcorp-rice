@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('produksi/pengeringan/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Pengeringan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-2 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Pengeringan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="pengeringan" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($pengeringan as $row){
                                        if(isset($get['pengeringan']) && $get['pengeringan'] == $row){
                                            echo '<option value="'.$row.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tugas Pengeringan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-sortable="true">Kode Produksi</th>
                    <th data-sortable="true">Tanggal Mulai</th>
                    <th data-sortable="true">Tanggal Selesai</th>
                    <th data-sortable="true">Pengeringan</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Jumlah KS</th>
                    <th data-sortable="true">Harga KS</th>
                    <th data-sortable="true">Hasil KG</th>
                    <th data-sortable="true">Harga KG</th>
                    <th data-sortable="true">Susut</th>
                    <th data-sortable="true">Petugas</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total_ks = 0;
                    $total_kg = 0;
                    if(!empty($tugas)){
                        foreach($tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".date("Y-m-d H:i", strtotime($row['tanggal_mulai']))."</label></td>";
                            echo "<td><label class='badge badge-dark'>".date("Y-m-d H:i", strtotime($row['tanggal_selesai']))."<label></td>";
                            echo "<td>";
                            if($row['jenis_pengeringan'] == 'dryer'){
                                echo "<label class='badge badge-primary'>Dryer</label>";
                            }else{
                                echo "<label class='badge badge-warning'>Jemur</label>";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['padi']."</label></td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>".monefy($row['hpp_beli'], false)."</td>";
                            echo "<td>".monefy($row['berat_kering'], false)."</td>";
                            echo "<td>".monefy($row['hpp_kering'], false)."</td>";
                            echo "<td><label class='badge badge-danger'>".round((($row['kuantitas'] - $row['berat_kering'])/$row['kuantitas'] * 100), 1)." %</label></td>";
                            
                            echo "<td><label class='badge badge-light'>".ucwords($row['petugas'])."</label></td>";
                            echo "</tr>";
                            $total_ks += $row['kuantitas'];
                            $total_kg += $row['berat_kering'];
                        }
                    }
				?>
                
            </tbody>
        </table>
        <table class="table table-striped mg-t-10 table-white">
            <tr class="tx-bold tx-16">
                <td>Total Gabah Dikeringkan</td>
                <td>{{monefy($total_ks, false)}}</td>
            </tr>
            <tr class="tx-bold tx-16">
                <td>Total Hasil Pengeringan</td>
                <td>{{monefy($total_kg, false)}}</td>
            </tr>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pengeringan</p>
        @endif
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let pengeringan = $("#form-filter [name=pengeringan]").val();
    // if (start != '' && end != '') {
        var url = "<?= base_url('produksi/pengeringan/export?')?>"+'start='+start+'&end='+end+'&pengeringan='+pengeringan;
        window.open(url, '_blank');
    // } else {
        // Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}

</script>
@end