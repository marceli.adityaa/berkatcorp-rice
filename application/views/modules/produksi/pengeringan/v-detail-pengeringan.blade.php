@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi Tugas Pengeringan
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail['tanggal_mulai']}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jenis Pengeringan / Padi </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{ucwords($detail['jenis_pengeringan']).' / '.$detail['padi']}}" readonly>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Petugas</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{ucwords($detail['petugas'])}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Keterangan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail['keterangan']}}" readonly>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($detail['kuantitas'] == 0)
                            <button type="button" class="btn btn-primary btn-success" id="proses" data-id="{{$detail['id']}}"><i class="fa fa-send mg-r-10"></i>Simpan</button>
                            @elseif($detail['persetujuan'] != 2)
                            <button type="button" class="btn btn-danger btn-success" id="batal" data-id="{{$detail['id']}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pengeringan Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($detail['kuantitas'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Gabah</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Pengeringan</th>

                    @if ($detail['jenis_pengeringan'] == 'dryer')
                    <th data-sortable="true">Lokasi Dryer</th>
                    @else
                    <th data-sortable="true">Lokasi Jemur</th>
                    @endif

                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Supplier</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    if(!empty($detail_tugas)){
                        foreach($detail_tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($detail['kuantitas'] == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['jenis_pengeringan'] == 'dryer'){
                                echo "<td><label class='badge badge-primary'>Dryer</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>Jemur</label></td>";
                            }

                            if($detail['jenis_pengeringan'] == 'dryer'){
                                if($row['id_dryer'] != 0){
                                    echo "<td><label class='badge badge-light'>".ucwords($lokasi_dryer[$row['id_dryer']]['nama'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                            }else{
                                if($row['id_jemur'] != 0){
                                    echo "<td><label class='badge badge-light'>".ucwords($lokasi_jemur[$row['id_jemur']]['nama'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                            }

                            echo "<td>".strtoupper($row['jenis_gabah'])."</td>";
                            echo "<td>".strtoupper($row['padi'])."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            if($row['id_supplier'] != 0){
                                echo "<td><label class='badge badge-light'>".ucwords($list_supplier[$row['id_supplier']]['nama'])."</label></td>";
                            }else{
                                echo "<td>-</td>";
                            }
                            
                            echo "</tr>";
                            $total += $row['kuantitas'];
                        }
                    }
				?>
            </tbody>
        </table>
        <h4 class="float-right tx-black mg-r-10 text-right"><small>Total Berat Gabah : </small><b>{{monefy($total, false)}}</b> <small>Kg</small></h4>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Gabah</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('produksi/pengeringan/submit_detail_tugas')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_pengeringan" value="{{$detail['id']}}">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <table class="table table-stripped" id="tabel-gabah">
                                    <thead>
                                        <tr>
                                            <th data-searchable="false">Pilih</th>
                                            <th data-sortable="true">Tanggal</th>
                                            <th data-sortable="true">Supplier</th>
                                            <th data-sortable="true">Gabah</th>
                                            <th data-sortable="true">Padi</th>
                                            <th data-sortable="true">Stok Sisa</th>
                                            <th>Ambil Stok</th>
                                            <th>Lokasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($gabah as $key => $row)
                                        @if($row['stok_sisa'] > 0)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input check" name="check[]" value="{{$row['id']}}" id="customCheck-{{$key}}">
                                                    <input type="hidden" name="id_stok" value="{{$row['id']}}">
                                                    <input type="hidden" name="harga_satuan" value="{{$row['harga_satuan']}}">
                                                    <label class="custom-control-label" for="customCheck-{{$key}}"></label>
                                                </div>
                                            </td>    
                                            <td>
                                                <label class="badge badge-light">{{$row['tanggal']}}</label>
                                            </td>
                                            <td>
                                                @if($row['id_supplier'] != 0)
                                                    <label class="badge badge-light">{{$list_supplier[$row['id_supplier']]['nama']}}</label>
                                                @else
                                                    <label>-</label>
                                                @endif
                                            </td>
                                            <td>
                                                <label class="badge badge-light">{{strtoupper($row['jenis_gabah'])}}</label>
                                            </td>
                                            <td>
                                                <label class="badge badge-light">{{strtoupper($row['padi'])}}</label>
                                            </td>
                                            <td>
                                                <label class='sisa'>{{strtoupper($row['stok_sisa'])}}</label>
                                            </td>
                                            <td>
                                                <input type="number" min="0" max="{{$row['stok_sisa']}}" class="form-control kuantitas" readonly="" name="kuantitas[]">
                                            </td>
                                            <td>
                                                @if($detail['jenis_pengeringan'] == 'dryer')
                                                    <select name="id_dryer" class="form-control lokasi" readonly="">
                                                    <option value=""></option>
                                                    @foreach ($list_dryer as $row)
                                                        <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                                    @endforeach
                                                    </select>
                                                @else
                                                    <select name="id_jemur" class="form-control lokasi" readonly="">
                                                    <option value=""></option>
                                                    @foreach ($list_jemur as $row)
                                                        <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                                    @endforeach
                                                    </select>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="button" class="btn btn-info" id="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('#tabel-gabah').bootstrapTable({
        pagination: true,
        search: true,
        // toolbar: '#toolbar2'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$(document).on('change keyup', '.check', function(){
    if($(this).is(':checked')){
        var sisa = $(this).closest('tr').find('.sisa').text();
        $(this).closest('tr').find('.kuantitas, .lokasi').prop('readonly', false).val(sisa);
    }else{
        $(this).closest('tr').find('.kuantitas, .lokasi').prop('readonly', true).val('');
    }
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data dan lanjut ke proses pengeringan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/submit_detail_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    window.location.href = result.redirect;
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan pengajuan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/batal_pengajuan_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#submit').click(function(){
    var data = [];
    var exist = false;
    $('#tabel-gabah tr').each(function(a, b) {
        if (a > 0) {
            exist = true;
            if($(b).find('.check').is(':checked')){
                data.push({
                    id_stok: $(b).find("[name=id_stok]").val(),
                    kuantitas: $(b).find(".kuantitas").val(),
                    harga_satuan: $(b).find("[name=harga_satuan]").val(),
                    id_dryer : $(b).find("[name=id_dryer] option:selected").val(),
                    id_jemur : $(b).find("[name=id_jemur] option:selected").val(),
                });
            
            }
        }
    });
    if(exist){
        $.ajax({
            url: "<?= base_url('api/internal/produksi/pengeringan/submit_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id_pengeringan' : {{$detail['id']}},
                'data': data,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }else{
        swal.fire("Perhatian", "Harap memilih data gabah yang akan dikeringkan", "warning");
    }
});

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/delete_data_detail')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end