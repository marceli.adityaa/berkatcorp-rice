@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi Tugas Pengeringan
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{$detail['tanggal_mulai']}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Tanggal Selesai</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{$detail['tanggal_selesai']}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3 form-control-label">Jenis Pengeringan </label>
                        <div class="col-sm-3">
                            <label class="form-control">{{ucwords($detail['jenis_pengeringan'])}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Jenis Padi </label>
                        <div class="col-sm-3">
                            <label class="form-control">{{ucwords($detail['padi'])}}</label>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 form-control-label">Petugas</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{ucwords($detail['petugas'])}}</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Keterangan</label>
                        <div class="col-sm-3">
                            @if(!empty($detail['keterangan']))
                            <label class="form-control">{{ucwords($detail['keterangan'])}} </label>
                            @else
                            <label class="form-control">-</label>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 form-control-label">Total Gabah</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['kuantitas'], false)}} Kg</label>
                        </div>
                        <label class="col-sm-3 form-control-label">Hasil Kering</label>
                        <div class="col-sm-3">
                            <label class="form-control">{{monefy($detail['berat_kering'], false)}} Kg</label>
                            @if (!empty($detail['berat_kering']))
                            <p class="mt-2 tx-16 tx-danger"><small>Penyusutan : </small> <b>{{round((($detail['kuantitas'] - $detail['berat_kering'])/$detail['kuantitas'] * 100), 1)}} %</b> </p>
                            @endif
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pengeringan Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Pengeringan</th>

                    @if ($detail['jenis_pengeringan'] == 'dryer')
                    <th data-sortable="true">Lokasi Dryer</th>
                    @else
                    <th data-sortable="true">Lokasi Jemur</th>
                    @endif

                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Supplier</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    if(!empty($detail_tugas)){
                        foreach($detail_tugas as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            if($row['jenis_pengeringan'] == 'dryer'){
                                echo "<td><label class='badge badge-primary'>Dryer</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>Jemur</label></td>";
                            }

                            if($detail['jenis_pengeringan'] == 'dryer'){
                                if($row['id_dryer'] != 0){
                                    echo "<td><label class='badge badge-light'>".ucwords($lokasi_dryer[$row['id_dryer']]['nama'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                            }else{
                                if($row['id_jemur'] != 0){
                                    echo "<td><label class='badge badge-light'>".ucwords($lokasi_jemur[$row['id_jemur']]['nama'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                            }

                            echo "<td>".strtoupper($row['jenis_gabah'])."</td>";
                            echo "<td>".strtoupper($row['padi'])."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            if($row['id_supplier'] != 0){
                                echo "<td><label class='badge badge-light'>".ucwords($list_supplier[$row['id_supplier']]['nama'])."</label></td>";
                            }else{
                                echo "<td>-</td>";
                            }
                            
                            echo "</tr>";
                            $total += $row['kuantitas'];
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('#tabel-gabah').bootstrapTable({
        pagination: true,
        search: true,
        // toolbar: '#toolbar2'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$(document).on('change keyup', '.check', function(){
    if($(this).is(':checked')){
        var sisa = $(this).closest('tr').find('.sisa').text();
        $(this).closest('tr').find('.kuantitas, .lokasi').prop('readonly', false).val(sisa);
    }else{
        $(this).closest('tr').find('.kuantitas, .lokasi').prop('readonly', true).val('');
    }
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data dan lanjut ke proses pengeringan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/submit_detail_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    window.location.href = result.redirect;
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan pengajuan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/batal_pengajuan_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#submit').click(function(){
    var data = [];
    var exist = false;
    $('#tabel-gabah tr').each(function(a, b) {
        if (a > 0) {
            exist = true;
            if($(b).find('.check').is(':checked')){
                data.push({
                    id_stok: $(b).find("[name=id_stok]").val(),
                    kuantitas: $(b).find(".kuantitas").val(),
                    harga_satuan: $(b).find("[name=harga_satuan]").val(),
                    id_dryer : $(b).find("[name=id_dryer] option:selected").val(),
                    id_jemur : $(b).find("[name=id_jemur] option:selected").val(),
                });
            
            }
        }
    });
    if(exist){
        $.ajax({
            url: "<?= base_url('api/internal/produksi/pengeringan/submit_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id_pengeringan' : {{$detail['id']}},
                'data': data,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }else{
        swal.fire("Perhatian", "Harap memilih data gabah yang akan dikeringkan", "warning");
    }
});

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/produksi/pengeringan/delete_data_detail')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end