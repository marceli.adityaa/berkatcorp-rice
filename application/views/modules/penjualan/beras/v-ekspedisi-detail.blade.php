@layout('commons/index')

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    No Resi {{$detail->no_resi}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Plat Kendaraan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail->no_pol}}" readonly>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sopir</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail->sopir}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Kirim</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail->tanggal_jalan}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Total Tonase (Kg)</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <h3 id="tonase">{{monefy($detail->berat_muatan, false)}}</h3>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($detail->status_muatan == 0)
                            <button type="button" class="btn btn-success btn-primary" id="proses" data-resi="{{$detail->no_resi}}"><i class="fa fa-send mg-r-10"></i>Selesai</button>
                            @else 
                            <button type="button" class="btn btn-success btn-danger" id="batal" data-resi="{{$detail->no_resi}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Muatan</h6>
    </div>
    <div class="card-body">
        <div class="float-right mg-b-10">
        </div>
        <table class="table table-striped table-white" id="tabel-pemesanan">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Aksi</th>
                    <th data-searchable="false">Kode</th>
                    <th>Customer</th>
                    <th>Sales</th>
                    <th data-searchable="false" class="text-center">Tonase (Kg)</th>
                    <th data-searchable="false" class="text-center">Subtotal (Rp)</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
                @if (!empty($list))
                    @foreach($list as $row)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>
                            @if($detail->status_muatan == 0)
                                <button type="button" class="btn btn-danger" onclick="hapus_data(this)" data-id="{{$row['id']}}" data-resi="{{$row['no_resi']}}" title="Hapus data resi"><i class="fa fa-trash"></i></button>
                            @endif
                                <a href="{{base_url('penjualan/beras/faktur/detail/'.$row['id'])}}" target="_blank" class="btn btn-info" title="detail transaksi"><i class="fa fa-search"></i></a>
                            </td>
                            <td>{{ucwords($row['kode'])}}</td>
                            <td>{{ucwords($row['customer'])}}</td>
                            <td>{{ucwords($row['sales'])}}</td>
                            <td><label class="tx-primary tx-bold sub-tonase">{{monefy($row['tonase'], false)}}</label></td>
                            <td><label class="tx-primary tx-bold sub-tonase">{{monefy($row['total'], false)}}</label></td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@end


@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-po').bootstrapTable({
        pagination: true,
        // search: true,
        toolbar: '#toolbar2'
    });

    $('#tabel-pemesanan').bootstrapTable({
        // pagination: true,
        // search: true,
        toolbar: '#toolbar'
    });
    
});

$('#tambah').click(function() {
    $("#modal_form").modal('show');
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

function isEmpty(str) {
    return (!str || 0 === str.length);
}

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengiriman akan diupdate, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/ekspedisi/proses_kiriman')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'resi': $(this).data().resi
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    });
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan pengajuan, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/ekspedisi/batal_kiriman')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'resi': $(this).data().resi
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    });
})

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus resi pengiriman data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/ekspedisi/reset_resi_pengiriman')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id,
                    'resi' : $(e).data().resi
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end