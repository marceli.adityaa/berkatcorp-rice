<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .items td {
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }

    table thead td {
        /* background-color: #EEEEEE; */
        text-align: center;
        border: 0.1mm solid #000000;
        /* font-variant: small-caps; */
        font-weight:bold;
    }

    /* table tbody td{
        border-bottom: 0.05mm solid #aaa;
    } */

    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        font-weight:bold;
        text-align:center;
    }

    .items td.totals {
        text-align: right;
        border: 0.1mm solid #000000;
    }

    .items td.cost {
        text-align: "."center;
    }

    .ttd{
        font-size:8pt;
        text-align:center;
    }
    .text-center{
        text-align:center;
    }
    .text-left{
        text-align:left;
    }
    </style>
</head>

<body>
<!--mpdf
<htmlpageheader name="myheader">
<table width="100%">
    <tr>
        <td width="34%" style="color:#000;padding-top:6px">
            <span style="font-weight: bold; font-size: 8pt;">PT. Berkat Anoegerah Sejahtera</span><br />
            <span style="font-size:7pt">Krajan, Plerean, Kec. Sumberjambe</span><br />
            <span style="font-size:7pt">Kab. Jember - Jawa Timur</span><br />
            <span style="font-family:dejavusanscondensed; font-size:7pt">&#9742;(0331) 566130</span> 
        </td>
        <td width="33%" style="text-align:center;line-height:1.5;">
            <span style="font-weight: bold; font-size: 10pt;">SURAT PEMESANAN</span><br>
            <span style="font-size:8pt;"><?= tanggal_indo($detail['tanggal'])?></span><br>
            <span style="font-size:8pt"><?= $detail['kode']?></span>

        </td>
        <td width="33%" style="text-align: right;">
            <img src="assets/img/logo/bas-black.png" style="width:115px">
        </td>
    </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
    <div style="border-top: 0.1mm solid #000000; font-size: 8pt; text-align: center; padding-top: 1mm; ">
        <table width="100%">
            <tr>
                <td width="50%" style="color:#444;text-align:left; font-size:7pt">
                    <b>Input by : <?= ucwords($detail['username_input'])?>, <?= $detail['timestamp_input']?></b>
                </td>
                <td width="50%" style="color:#444;text-align:right; font-size:7pt">
                    <b>Printed by : <?= ucwords($this->session->auth['nama'])?>, <?= date('Y-m-d H:i:s')?></b>
                </td>
            <tr>
        </table>
    </div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
    <hr style="margin-top:0px;height:0.1mm;color:#000;">
    <div style="text-align: left;">
        <span style="font-size:7pt">Kepada Yth : <b><?= ucwords($detail['customer'])?></b> <br> <?= ucwords($detail['customer_alamat'])?> | <?= $detail['customer_telp']?></span></div>
    
    <br />
    

    <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse; " cellpadding="4">
            <thead>
                <tr>
                    <td class="text-center">No.</td>
                    <td style="text-align:left;">Merk</td>
                    <?php foreach ($jenis_kemasan as $row) {
                    echo '<td class="text-center">@'.$row['kemasan'].'KG</td>';
                    }
                    ?>
                    <td class="text-center">Tonase (Kg)</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $tonase = 0;
                foreach($merk as $mk){
                    $subtonase = 0;
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td>'.ucwords($mk['nama']).'</td>';
                    foreach($jenis_kemasan as $jk){
                        $found = false;
                        foreach ($kemasan as $kms){
                            if($mk['id'] == $kms['id_merk'] && $kms['kemasan'] == $jk['kemasan']){
                                $found = true;
                                $found2 = false;
                                if(!empty($detail_po)){
                                    foreach($detail_po as $d){
                                        if($kms['id'] == $d['id_kemasan']){
                                            echo "<td class='text-center'><label>".$d['kuantitas']."</label></td>";
                                            $found2 = true;
                                            $subtonase += ($d['kuantitas'] * $kms['kemasan']);
                                        break;
                                        }
                                    }
                                }
                                if(!$found2){
                                    echo "<td class='text-center'></td>";
                                }
                            break;
                            }
                        }
                        if(!$found){
                            echo "<td></td>";
                        }
                    }
                    if($subtonase > 0){
                        echo '<td class="text-center"><label class="tx-primary tx-bold">'.$subtonase.'</label></td>';
                    }else{
                        echo '<td></td>';
                    }
                    echo '</tr>';
                    $tonase += $subtonase;
                }
                ?>
                <tr>
                    <td class="blanktotal" colspan="<?= count($jenis_kemasan)+2?>">Total</td>
                    <td class="blanktotal"><?=$tonase?></td>
                </tr>
            </tbody>
        </table>

    <table class="" width="100%" style="font-size: 8pt; border-collapse: collapse; margin-top:5px" cellpadding="20">
        <tr>    
            <td class="ttd">
                Customer
            </td>
            
            <td class="ttd">
                Sales
            </td>
        </tr>
        <tr>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
            
            <td class="ttd">
                <b><?php
                    if(!empty($detail['sales'])){
                        echo "( ".ucwords($detail['sales'])." )";
                    }else{
                        echo "( _________________ )";
                    }
                ?></b>
            </td>
        </tr>
    </table>
    <!-- <div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div> -->
</body>

</html>