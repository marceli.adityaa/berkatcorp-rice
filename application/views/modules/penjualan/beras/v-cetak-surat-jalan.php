<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .items td {
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }

    table thead td {
        /* background-color: #EEEEEE; */
        text-align: center;
        border: 0.1mm solid #000000;
        /* font-variant: small-caps; */
        font-weight:bold;
    }

    /* table tbody td{
        border-bottom: 0.05mm solid #aaa;
    } */

    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        font-weight:bold;
        text-align:center;
    }

    .items td.totals {
        /* text-align: right; */
        border: 0.1mm solid #000000;
    }

    .items td.cost {
        text-align: "."center;
    }

    .ttd{
        font-size:8pt;
        text-align:center;
    }
    .text-center{
        text-align:center;
    }
    .text-left{
        text-align:left;
    }
    </style>
</head>

<body>
<!--mpdf
<htmlpageheader name="myheader">
<table width="100%">
    <tr>
        <td width="34%" style="color:#000;padding-top:6px">
            <span style="font-weight: bold; font-size: 8pt;">PT. Berkat Anoegerah Sejahtera</span><br />
            <span style="font-size:7pt">Krajan, Plerean, Kec. Sumberjambe</span><br />
            <span style="font-size:7pt">Kab. Jember - Jawa Timur</span><br />
            <span style="font-family:dejavusanscondensed; font-size:7pt">&#9742;(0331) 566130</span> 
        </td>
        <td width="33%" style="text-align:center;line-height:1.5">
            <span style="font-weight: bold; font-size: 10pt;">SURAT JALAN</span><br>
            <span style="font-size:8pt"><?= $detail['kode_surat_jalan']?></span><br>
            <span style="font-size:8pt;"><?= tanggal_indo($detail['tgl_kirim'])?></span><br>
            <span style="font-size:8pt;">No. Pemesanan : <?= $detail['kode']?></span><br>
        </td>
        <td width="33%" style="text-align: right;">
            <img src="assets/img/logo/bas-black.png" style="width:115px">
        </td>
    </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
    <div style="border-top: 0.1mm solid #000000; font-size: 8pt; text-align: center; padding-top: 1mm; ">
        <table width="100%">
            <tr>
                <td width="50%" style="color:#444;text-align:left; font-size:7pt">
                    <b>Input by : <?= ucwords($detail['username_input'])?>, <?= $detail['timestamp_input']?></b>
                </td>
                <td width="50%" style="color:#444;text-align:right; font-size:7pt">
                    <b>Printed by : <?= ucwords($this->session->auth['nama'])?>, <?= date('Y-m-d H:i:s')?></b>
                </td>
            <tr>
        </table>
    </div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
    <hr style="margin-top:0px;height:0.1mm;color:#000;">
    <div style="text-align: left;">
        <span style="font-size:7pt">Kepada Yth : <b><?= ucwords($detail['customer'])?></b> <br> <?= ucwords($detail['customer_alamat'])?> | <?= $detail['customer_telp']?></span></div>
    
    <br />
    

    <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse; " cellpadding="4">
            <thead>
                <tr>
                    <td class="text-center">No.</td>
                    <td style="text-align:left;">Nama Barang</td>
                    <td class="text-center">Kuantitas</td>
                    <td class="text-center">Tonase (Kg)</td>
                    <td class="text-center">Keterangan</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $total = 0;
                foreach($detail_sj as $row){
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td class="text-left">'.ucwords($row['merk']).' @'.$row['kemasan'].' KG</td>';
                    echo '<td class="text-center">'.$row['kuantitas'].'</td>';
                    echo '<td class="text-center">'.($row['kuantitas']*$row['kemasan']).'</td>';
                    echo '<td class="text-center"></td>';
                    echo '</tr>';
                    $total += ($row['kuantitas']*$row['kemasan']);
                }
                
                ?>
                <tr>
                    <td class="totals text-center" colspan="3"><b>Total</b></td>
                    <td class="totals text-center"><b><?= $total?></b></td>
                    <td class="totals"></td>
                </tr>
            </tbody>
        </table>

    <table class="" width="100%" style="font-size: 8pt; border-collapse: collapse; margin-top:0px" cellpadding="20">
        <tr>
            <td class="ttd">
                Kepala Gudang
            </td>    
            <td class="ttd">
                Pengirim
            </td>   
            <td class="ttd">
                Sopir
            </td>
            
            <td class="ttd">
                Penerima
            </td>
        </tr>
        <tr>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
        </tr>
    </table>
    <!-- <div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div> -->
</body>

</html>