@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Delivery Order No. {{$detail['kode']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal DO</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail['tanggal']}}" readonly>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Catatan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$detail['catatan']}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Total Tonase (Kg)</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <h3 id="tonase">{{$tonase}}</h3>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($detail['status'] == 0)
                            <button type="button" class="btn btn-success btn-primary" id="proses" data-id="{{$detail['id']}}"><i class="fa fa-send mg-r-10"></i>Proses</button>
                            @elseif($detail['status'] != 2)
                            <button type="button" class="btn btn-danger btn-success" id="batal" data-id="{{$detail['id']}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>

<div id="accordion2" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header bg-danger" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="tx-gray-800 transition">
                    Data Surat Pemesanan
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card">
                <div class="card-body">
                    <div id="toolbar2" class="mg-b-10">
                        @if($detail['status'] == 0)
                        <button class="btn btn-primary" id="tambah">+ Tambah Surat Pemesanan</button>
                        @endif
                    </div>
                    <table class="table table-striped mg-t-10 table-white" id="tabel-po">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Aksi</th>
                                <th data-sortable="true">Kode</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Area</th>
                                <th data-sortable="true">Customer</th>
                                <th data-sortable="true">Sales</th>
                                <th data-sortable="true">Tonase</th>
                                <th data-sortable="true">Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1;
                                if(!empty($exist)){
                                    foreach($exist as $row){
                                        echo "<tr>";
                                        echo "<td class='text-center'>".$no++."</td>";
                                        if($detail['status'] == 0){
                                            echo "<td><button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button></td>";
                                        }else{
                                            echo "<td></td>";
                                        }
                                        echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                                        echo "<td>".$row['tanggal']."</td>";
                                        echo "<td><label class='badge badge-warning'>".ucwords($row['area'])."</label></td>";
                                        echo "<td>".$row['customer']."</td>";
                                        echo "<td><label class='badge badge-light'>".ucwords($row['sales'])."</label></td>";
                                        echo "<td>".monefy($row['tonase'], false)."</td>";
                                        echo "<td>".$row['catatan']."</td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pemesanan</h6>
    </div>
    <div class="card-body">
        <div class="float-right mg-b-10">
            @if($detail['status'] == 0)
            <!-- <button type="button" class="btn btn-primary btn-primary" id="simpan" data-id="{{$detail['id']}}"><i class="fa fa-save mg-r-10"></i>Simpan</button> -->
            @endif
        </div>
        <table class="table table-striped table-white" id="tabel-pemesanan">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Merk</th>
                    @foreach($jenis_kemasan as $row)
                    <th data-searchable="false" class="text-center">@{{$row['kemasan']}}KG</th>
                    @endforeach
                    <th data-searchable="false" class="text-center">Tonase (Kg)</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
                @foreach($merk as $mk)
                    <?php $subtonase = 0;?>
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{ucwords($mk['nama'])}}</td>
                        @foreach($jenis_kemasan as $jk)
                            <?php 
                                $found = false;
                                foreach ($kemasan as $kms){
                                    if($mk['id'] == $kms['id_merk'] && $kms['kemasan'] == $jk['kemasan']){
                                        $found = true;
                                        $found2 = false;
                                        if(!empty($detail_po)){

                                            foreach($summary_po as $d){
                                                if($kms['id'] == $d['id_kemasan']){
                                                    echo "<td>
                                                    <input type='number' name='id_kemasan' class='form-control kemasan text-center' data-produk='".$kms['id']."' data-qty='".$kms['kemasan']."' min='0' value='".$d['qty']."' readonly=''>
                                                    </td>";
                                                    $found2 = true;
                                                    $subtonase += ($d['qty'] * $kms['kemasan']);
                                                break;
                                                }
                                            }
                                        }
                                        if(!$found2){
                                            echo "<td>
                                                    <input type='number' name='id_kemasan' class='form-control kemasan text-center' data-produk='".$kms['id']."' data-qty='".$kms['kemasan']."' min='0' readonly=''>
                                                </td>";
                                        }
                                    break;
                                    }
                                }
                                if(!$found){
                                    echo "<td></td>";
                                }
                                ?>
                          
                        @endforeach
                        <td><label class="tx-primary tx-bold sub-tonase" data-merk="{{$mk['id']}}">{{$subtonase}}</label></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Purchase Order</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('penjualan/beras/order_produksi/submit_tambah_po')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_produksi" value="{{$detail['id']}}">

                                <table class="table" id="table-list-po">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th data-searchable="true">Tanggal</th>
                                            <th data-searchable="true">Kode</th>
                                            <th data-searchable="true">Customer</th>
                                            <th data-searchable="true">Tonase</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($list_po as $key => $row)
                                        <tr>
                                            <td class="text-center">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input check" name="check[]" value="{{$row['id']}}" id="customCheck-{{$key}}">
                                                    <label class="custom-control-label" for="customCheck-{{$key}}"></label>
                                                </div>
                                            </td>
                                            @if($row['tanggal'] == $detail['tanggal'])
                                                <td><label class="badge badge-primary">{{$row['tanggal']}}</label></td>
                                            @else
                                                <td><label class="badge badge-light">{{$row['tanggal']}}</label></td>
                                            @endif
                                            <td><label class="badge badge-light">{{$row['kode']}}</label></td>
                                            <td><label class="badge badge-light">{{$row['customer']}}</label></td>
                                            <td><label class="form-control">{{$row['tonase']}}</label></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info" id="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-po').bootstrapTable({
        pagination: true,
        // search: true,
        toolbar: '#toolbar2'
    });

    $('#tabel-pemesanan').bootstrapTable({
        // pagination: true,
        // search: true,
        toolbar: '#toolbar'
    });

    $('#tabel-list-po').bootstrapTable();

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$('#tambah').click(function() {
    $("#modal_form").modal('show');
});

$(document).on('change keyup', '.kemasan', function(){
    var total_tonase = 0;
    $('#tabel-pemesanan .kemasan').each(function(a, b) {
        if(!isEmpty($(b).val())){
            total_tonase += (parseInt($(b).val()) * parseInt($(b).data('qty')));
        }
    });
    $('#tonase').empty().html(total_tonase);
    $('#tabel-pemesanan tr').each(function(a, b) {
        var subtonase = 0;
        if (a > 0) {
            $('.kemasan', b).each(function(c,d){
                if(!isEmpty($(d).val())){
                    subtonase += (parseInt($(d).val()) * parseInt($(d).data('qty')));
                }
            })
        }
        $(b).find('.sub-tonase').empty().html(subtonase);
    });
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

function isEmpty(str) {
    return (!str || 0 === str.length);
}

$('#simpan').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            $('#tabel-pemesanan tr').each(function(a, b) {
                if (a > 0) {
                    data.push(
                        {
                            "id_produksi" : <?= $detail['id']?>,
                            "id_merk" : $(b).find('.sub-tonase').data('merk'), 
                            "tonase_po" : $(b).find('.sub-tonase').html(), 
                            "tonase_ready" : $(b).find('[name=tonase_ready]').val(), 
                        }
                    );
                }
            });
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/order_produksi/simpan_data_pemesanan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id,
                    'data' : data
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan proses order?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/order_produksi/batal_pengajuan_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Proses order?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            var id = $(this).data('id');
            $('#tabel-pemesanan tr').each(function(a, b) {
                if (a > 0) {
                    data.push(
                        {
                            "id_produksi" : <?= $detail['id']?>,
                            "id_merk" : $(b).find('.sub-tonase').data('merk'), 
                            "tonase_po" : $(b).find('.sub-tonase').html(), 
                            "tonase_ready" : $(b).find('[name=tonase_ready]').val(), 
                        }
                    );
                }
            });
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/order_produksi/simpan_data_pemesanan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id,
                    'data' : data
                },
                success: function(result) {
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                    $.ajax({
                        url: "<?= base_url('api/internal/penjualan/beras/order_produksi/proses_order')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'token' : "{{$this->session->auth['token']}}",
                            'id': id,
                        },
                        success: function(result) {
                            reload_page();
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                }
            });
        }
    });
})

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/order_produksi/delete_data_po')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end