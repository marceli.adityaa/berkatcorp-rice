@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Surat Pemesanan No. {{$detail['kode']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row">
                        <div class="col-6">
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Tanggal Pemesanan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{$detail['tanggal']}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Customer</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{ucwords($detail['customer'])}}" readonly>
                                    @if ($po == 0)
                                    @else
                                    <p class="help-text mg-t-3 tx-danger" style="font-weight: bold; padding-top:5px" id="tmp" value="{{$po}}">Customer ini memiliki {{$po}} Nota penjualan yang belum lunas</b></p>
                                    @endif
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Sales</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{ucwords($detail['sales'])}}" readonly>
                                </div>
                            </div>

                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Tanggal Kirim</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" class="form-control selesai" name="tgl_kirim" value="{{$detail['tgl_kirim']}}">
                                </div>
                            </div>

                            
                                <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Resi Pengiriman</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="no_resi" class="form-control select2 selesai">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($ekspedisi as $row)
                                    @if($detail['no_resi'] == $row->no_resi)
                                        <option value="{{$row->no_resi}}" selected="">{{$row->no_resi.' ( '.$row->no_pol.' - '.strtoupper($row->sopir)}} )</option>
                                        @else
                                        <option value="{{$row->no_resi}}">{{$row->no_resi.' ( '.$row->no_pol.' - '.strtoupper($row->sopir)}} ) </option>
                                        @endif
                                    @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                           
                            
                            <div class="row mg-t-10">
                                <!-- <label class="col-sm-4 form-control-label"></label> -->
                                <div class="col-sm-12 mg-t-10 mg-sm-t-0 tx-right">
                                    @if($detail['status_selesai'] == 0)
                                    <button type="button" class="btn btn-success" id="proses" data-id="{{$detail['id']}}"><i class="fa fa-send mg-r-10"></i>Selesai</button>
                                    <button type="button" class="btn btn-primary" id="simpan" data-id="{{$detail['id']}}"><i class="fa fa-save mg-r-10"></i>Simpan</button>
                                    <a href="{{base_url('penjualan/beras/surat_pemesanan/cetak_po/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-info"data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>PO</button></a>
                                    @elseif($detail['status_selesai'] == 1)
                                        @if($detail['persetujuan'] != 1)
                                        <button type="button" class="btn btn-danger" id="batal" data-id="{{$detail['id']}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                                        @endif
                                    <a href="{{base_url('penjualan/beras/surat_pemesanan/cetak_po/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-info"data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>PO</button></a>
                                    <a href="{{base_url('penjualan/beras/faktur/cetak_sj/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-warning"data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>Surat Jalan</button></a>
                                    <a href="{{base_url('penjualan/beras/faktur/cetak_faktur/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-dark" id="print" data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>Nota</button></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($detail['is_tempo'] == 1)
                                        <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input selesai" checked="">
                                        @else
                                        <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input selesai">
                                        @endif
                                        <label class="custom-control-label" for="tempo-1">Tempo</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($detail['is_tempo'] == 0)
                                        <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input selesai" checked="">
                                        @else
                                        <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input selesai">
                                        @endif
                                        <label class="custom-control-label" for="tempo-2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-12 my-3 tempo">
                                <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" name="tgl_tempo" class="form-control tempo selesai" value="{{$detail['tgl_tempo']}}">
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo">
                                <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @foreach ($pembayaran as $key => $row)
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($detail['jenis_pembayaran'] == $row)
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai" checked>
                                        @else
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai">
                                        @endif

                                        @if($row == 'tbd')
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                        @else
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo cash">
                                <label class="col-sm-4 form-control-label">Sumber Kas</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_kas" class="form-control selesai select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($kas as $row)
                                            @if($detail['id_kas'] == $row->id)
                                            <option value="{{$row->id}}" selected="">{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo transfer">
                                <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_rek_pengirim" class="form-control selesai select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($rek_pengirim as $row)
                                            @if($detail['id_kas'] == $row->id)
                                            <option value="{{$row->id}}" selected>{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-12 mg-t-10">
                                <label class="col-sm-4 form-control-label">Potongan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control selesai" name="potongan" value="{{$detail['potongan']}}">
                                </div>
                            </div>

                            <div class="row col-12 mg-t-10">
                                <label class="col-sm-4 form-control-label">Biaya Tambahan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control selesai" name="biaya_tambahan" value="{{$detail['biaya_tambahan']}}">
                                </div>
                            </div>

                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                        </div>
                                        <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal">{{monefy($detail['jumlah_bayar'], false)}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pemesanan</h6>
    </div>
    <div class="card-body">
        <div class="float-right mg-b-10">
            @if($detail['status_po'] == 0)
            <button type="button" class="btn btn-primary btn-primary" id="simpan" data-id="{{$detail['id']}}"><i class="fa fa-save mg-r-10"></i>Simpan</button>
            @endif
        </div>
        <table class="table table-striped table-white" id="tabel-po">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th>Merk</th>
                    <th class="text-center">Kemasan (Kg)</th>
                    <th class="text-center">Kuantitas</th>
                    <th class="text-center">Tonase (Kg)</th>
                    <th class="text-center">Harga Satuan (Kg)</th>
                    <th class="text-center">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $total = 0;
                $grandtotal = 0;
                foreach($detail_po as $row){
                    $subtotal = 0;
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td class="text-left">'.ucwords($row['merk']).'</td>';
                    echo '<td class="text-center">'.$row['kemasan'].'</td>';
                    echo '<td class="text-center">'.$row['kuantitas'].'</td>';
                    echo '<td class="text-center"><label class="qty">'.($row[ 'kuantitas']*$row['kemasan']).'</label></td>';
                    if($row['harga_satuan'] != 0){
                        echo '<td class="text-center"><input type="number" class="form-control text-center selesai" name="harga_satuan" value="'.$row['harga_satuan'].'" data-id="'.$row['id'].'"></td>';
                        echo '<td class="text-center"><label class="subtotal">'.monefy(($row['kuantitas'] * $row['kemasan'] * $row['harga_satuan']),false).'</label>';
                        $subtotal += $row['kuantitas'] * $row['kemasan'] * $row['harga_satuan'];
                        echo '<input type="hidden" name="subtotal" value="'.($row['kuantitas'] * $row['kemasan'] * $row['harga_satuan']).'"></td>';
                    }else{
                        echo '<td class="text-center"><input type="number" class="form-control text-center selesai" name="harga_satuan" value="'.$list_kemasan[$row['id_kemasan']]['harga'].'" data-id="'.$row['id'].'"></td>';
                        echo '<td class="text-center"><label class="subtotal">'.monefy(($row['kuantitas'] * $row['kemasan'] * $list_kemasan[$row['id_kemasan']]['harga']),false).'</label>';
                        $subtotal += $row['kuantitas'] * $row['kemasan'] * $list_kemasan[$row['id_kemasan']]['harga'];
                        echo '<input type="hidden" name="subtotal" value="'.($row['kuantitas'] * $row['kemasan'] * $list_kemasan[$row['id_kemasan']]['harga']).'"></td>';
                    }
                    echo '</tr>';
                    $total += ($row['kuantitas']*$row['kemasan']);
                    $grandtotal += $subtotal;
                }
                
                ?>
                <tr>
                    <td class="totals text-center" colspan="4"><b class="form-control">Total Kuantitas</b></td>
                    <td class="totals text-center"><label class="form-control"><b><?= monefy($total, false)?></b></label></td>
                    <td class="totals text-center"><b class="form-control">Total Penjualan</b></td>
                    <td class="totals text-center"><label class="form-control tx-subtotal"><b><?= monefy($grandtotal, false)?></b></label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-po').bootstrapTable({
        // pagination: true,
        // search: true,
        toolbar: '#toolbar'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    cek_tempo();
    cek_metode_pembayaran();
    hitung_grandtotal();
    let selesai = <?= $detail['status_selesai']?>;
    if(selesai){
        $('.selesai').attr('disabled', true);
    }
});

$(document).on('change keyup', '[name=harga_satuan], [name=potongan], [name=biaya_tambahan]', function(){
    let sum_subtotal = 0;
    $('#tabel-po tr').each(function(a, b) {
        let subtotal = 0;
        if (a > 0) {
            subtotal =  parseInt($(b).find('.qty').html()) * parseInt($(b).find('[name=harga_satuan]').val());
            $(b).find('.subtotal').empty().html(subtotal.toLocaleString());
            $(b).find('[name=subtotal]').val(subtotal);
            sum_subtotal += subtotal;
        }
    });
    $('.tx-subtotal').empty().html(sum_subtotal.toLocaleString());
    hitung_grandtotal();
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});


function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function hitung_grandtotal(){
    let total = 0;
    let potongan = parseInt($('[name=potongan]').val());
    let tambahan = parseInt($('[name=biaya_tambahan]').val());
    $('#tabel-po tr').each(function(a, b) {
        if (a > 0) {
            total += parseInt(getNum($(b).find('[name=subtotal]').val()));
        }
    });
    total = total + tambahan - potongan;
    $('#grandtotal, .tx-grandtotal').empty().html(total.toLocaleString());
}

function convert_to_number(input){
    return Number(input.replace(/[^0-9.-]+/g,""));
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

$('#simpan').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let data = [];
            let data_pembayaran = [];

            $('#tabel-po tr').each(function(a, b) {
                if (a > 0) {
                    data.push({
                        "id" : $(b).find('[name=harga_satuan]').data('id'), 
                        "harga_satuan" : $(b).find('[name=harga_satuan]').val()
                    });
                }
            });

            data_pembayaran = {
                is_tempo : $('[name=is_tempo]:checked').val(),
                tgl_tempo : $('[name=tgl_tempo]').val(),
                jenis_pembayaran : $('[name=jenis_pembayaran]:checked').val(),
                id_kas : $('[name=id_kas] option:selected').val(),
                id_rek_pengirim : $('[name=id_rek_pengirim]').find('option:selected').val(),
                potongan : $('[name=potongan]').val(),
                tambahan : $('[name=biaya_tambahan]').val(),
                grandtotal :convert_to_number($('#grandtotal').html())
            };

            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/faktur/simpan_data')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id,
                    'tgl_kirim' : $('[name=tgl_kirim]').val(),
                    'no_resi' : $('[name=no_resi]').val(),
                    'pembayaran' : data_pembayaran,
                    'data_harga' : data
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan proses order?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/faktur/batal_pengajuan_faktur')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#proses').click(function(){
    let vv = {{$po}};
    console.log(vv);
if (vv == 0){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Proses nota penjualan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let data = [];
            let data_pembayaran = [];
            let id = $(this).data().id;

            $('#tabel-po tr').each(function(a, b) {
                if (a > 0) {
                    data.push({
                        "id" : $(b).find('[name=harga_satuan]').data('id'), 
                        "harga_satuan" : $(b).find('[name=harga_satuan]').val()
                    });
                }
            });

            data_pembayaran = {
                is_tempo : $('[name=is_tempo]:checked').val(),
                tgl_tempo : $('[name=tgl_tempo]').val(),
                jenis_pembayaran : $('[name=jenis_pembayaran]:checked').val(),
                id_kas : $('[name=id_kas] option:selected').val(),
                id_rek_pengirim : $('[name=id_rek_pengirim]').find('option:selected').val(),
                potongan : $('[name=potongan]').val(),
                tambahan : $('[name=biaya_tambahan]').val(),
                grandtotal :convert_to_number($('#grandtotal').html())
            };

            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/faktur/simpan_data')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id,
                    'tgl_kirim' : $('[name=tgl_kirim]').val(),
                    'no_resi' : $('[name=no_resi]').val(),
                    'pembayaran' : data_pembayaran,
                    'data_harga' : data
                },
                success: function(result) {
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                    $.ajax({
                        url: "<?= base_url('api/internal/penjualan/beras/faktur/proses_faktur')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'token' : "{{$this->session->auth['token']}}",
                            'id': id,
                        },
                        success: function(result) {
                            reload_page();
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                }
            });
        }
    });
}else{
    Swal.fire({
        title: 'Customer masih memiliki Tagihan',
        text: "Customer masih memiliki Nota penjualan yang belum lunas. Lanjutkan proses order?",
        type: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let data = [];
            let data_pembayaran = [];
            let id = $(this).data().id;

            $('#tabel-po tr').each(function(a, b) {
                if (a > 0) {
                    data.push({
                        "id" : $(b).find('[name=harga_satuan]').data('id'), 
                        "harga_satuan" : $(b).find('[name=harga_satuan]').val()
                    });
                }
            });

            data_pembayaran = {
                is_tempo : $('[name=is_tempo]:checked').val(),
                tgl_tempo : $('[name=tgl_tempo]').val(),
                jenis_pembayaran : $('[name=jenis_pembayaran]:checked').val(),
                id_kas : $('[name=id_kas] option:selected').val(),
                id_rek_pengirim : $('[name=id_rek_pengirim]').find('option:selected').val(),
                potongan : $('[name=potongan]').val(),
                tambahan : $('[name=biaya_tambahan]').val(),
                grandtotal :convert_to_number($('#grandtotal').html())
            };

            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/faktur/simpan_data')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id,
                    'tgl_kirim' : $('[name=tgl_kirim]').val(),
                    'no_resi' : $('[name=no_resi]').val(),
                    'pembayaran' : data_pembayaran,
                    'data_harga' : data
                },
                success: function(result) {
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                    $.ajax({
                        url: "<?= base_url('api/internal/penjualan/beras/faktur/proses_faktur')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'token' : "{{$this->session->auth['token']}}",
                            'id': id,
                        },
                        success: function(result) {
                            reload_page();
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                }
            });
        }
    });
    }
})
</script>
@end