@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row">
                        <div class="col-6">
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Tanggal Pemesanan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{$detail['tanggal']}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Customer</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{ucwords($detail['customer'])}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Sales</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" value="{{ucwords($detail['sales'])}}" readonly>
                                </div>
                            </div>
                            
                            <div class="row mg-t-10">
                                <!-- <label class="col-sm-4 form-control-label"></label> -->
                                <div class="col-sm-12 mg-t-10 mg-sm-t-0 tx-right">
                                    @if($detail['status_selesai'] == 0)
                                    <button type="button" class="btn btn-success" id="proses" data-id="{{$detail['id']}}"><i class="fa fa-send mg-r-10"></i>Selesai</button>
                                    @elseif($detail['status_selesai'] == 1)
                                        @if($detail['persetujuan'] != 1)
                                        <button type="button" class="btn btn-danger" id="batal" data-id="{{$detail['id']}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                                        @endif
                                    @endif
                                    <a href="{{base_url('penjualan/beras/surat_pemesanan/cetak_po/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-info"data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>Pemesanan</button></a>
                                    <a href="{{base_url('penjualan/beras/faktur/cetak_faktur/'.$detail['id'])}}" target="_blank"><button type="button" class="btn btn-dark" id="print" data-id="{{$detail['id']}}"><i class="fa fa-print mg-r-10"></i>Rekap</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row col-12 my-3 langsung">
                                <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @foreach ($pembayaran as $key => $row)
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($detail['jenis_pembayaran'] == $row)
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai" checked>
                                        @else
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai">
                                        @endif

                                        @if($row == 'tbd')
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                        @else
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row col-12 my-3 langsung cash">
                                <label class="col-sm-4 form-control-label">Sumber Kas</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_kas" class="form-control selesai select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($kas as $row)
                                            @if($detail['id_kas'] == $row->id)
                                            <option value="{{$row->id}}" selected="">{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-12 my-3 langsung transfer">
                                <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_rek_pengirim" class="form-control selesai select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($rek_pengirim as $row)
                                            @if($detail['id_kas'] == $row->id)
                                            <option value="{{$row->id}}" selected>{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                        </div>
                                        <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal">{{monefy($detail['jumlah_bayar'], false)}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi Penjualan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        @if($detail['status_selesai'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Transaksi</button>
        @endif
        </div>
        <table class="table table-striped table-white" id="tabel-po">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Aksi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Customer</th>
                    <th class="text-center">Subtotal</th>
                    <th>Catatan</th>
                    <th>Input</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $total = 0;
                $grandtotal = 0;
                if(!empty($transaksi)){
                    foreach($transaksi as $row){
                        echo '<tr>';
                        echo '<td class="text-center">'.$no++.'</td>';
                        echo "<td class='text-nowrap'>";
                        echo "<a href='".base_url('penjualan/beras/kontan/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Transaksi'><i class='fa fa-sign-in'></i></button></a> ";
                        if($detail['status_selesai'] == 0){
                            echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                        }
                        if($row['subtotal'] == 0){
                            echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                        }else{
                            echo "<a href='".base_url('penjualan/beras/kontan/cetak_faktur_transaksi/'.$row['id'])."' target='_blank'><button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Cetak Nota'><i class='fa fa-print'></i></button></a> ";
                        }
                        echo "</td>";
                        echo '<td class="text-center">'.$detail['kode'].'-'.zerofy($row['id'],5).'</td>';
                        echo '<td class="text-center">'.$row['nama'].'</td>';
                        echo '<td class="text-center"><label class="subtotal">'.monefy($row['subtotal'], false).'</label>';
                        echo '<input type="hidden" name="subtotal" value="'.$row['subtotal'].'"></td>';
                        echo '<td>'.$row['catatan'].'</td>';
                        echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                        echo '</tr>';
                        $grandtotal += $row['subtotal'];
                    }
                }
                ?>
                <tr>
                    <td class="totals text-center" colspan="4"><b class="form-control">Total Penjualan</b></td>
                    <td class="totals text-center"><label class="form-control tx-grandtotal"><b><?= monefy($grandtotal, false)?></b></label></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Transaksi</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('penjualan/beras/kontan/submit_tambah_transaksi')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_po" value="{{$detail['id']}}">
                                <input type="hidden" name="id" value="">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Nama Customer <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="nama" autocomplete="off" value="ECERAN" placeholder="Masukkan nama customer" required=""> 
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="catatan" autocomplete="off" value="" placeholder="Masukkan catatan jika ada"> 
                                    </div>
                                </div>

                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-po').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    $('.penggilingan, .proses').hide();
    cek_tempo();
    cek_metode_pembayaran();
    hitung_grandtotal();
    let selesai = <?= $detail['status_selesai']?>;
    if(selesai){
        $('.selesai').attr('disabled', true);
    }
});

$(document).on('change keyup', '[name=harga_satuan]', function(){
    $('#tabel-po tr').each(function(a, b) {
        let subtotal = 0;
        if (a > 0) {
            subtotal =  parseInt($(b).find('.qty').html()) * parseInt($(b).find('[name=harga_satuan]').val());
            $(b).find('.subtotal').empty().html(subtotal.toLocaleString());
            $(b).find('[name=subtotal]').val(subtotal);
        }
    });
    hitung_grandtotal();
});

$('#merk').change(function() {
    let merk = $(this).val();
    $('#kemasan').val('').change();
    $('#tonase, #subtotal').empty().html('0');
    if(merk != ""){
        $('#kemasan').children('option').each(function () {
            if($(this).data('merk') == merk){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    }else{
        $('#kemasan').children('option').show();
    }
    
});

$('#kemasan').change(function(){
    let harga = $(this).find('option:selected').data('harga');
    $('#harga').empty().val(harga);
    hitung_subtotal();
})

$('#kuantitas, #harga').on('change keyup', function(){
    hitung_subtotal();
})

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});


function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function hitung_subtotal(){
    let kemasan = parseInt($('#kemasan').find('option:selected').data('kemasan'));
    let harga = parseInt($('#harga').val());
    let kuantitas = parseInt($('#kuantitas').val());

    $('#tonase').empty().html((kemasan * kuantitas).toLocaleString());
    $('#subtotal').empty().html((kemasan * kuantitas * harga).toLocaleString());
}

function hitung_grandtotal(){
    let total = 0;
    $('#tabel-po tr').each(function(a, b) {
        if (a > 0) {
            total += parseInt(getNum($(b).find('[name=subtotal]').val()));
        }
    });
    $('#grandtotal, .tx-grandtotal').empty().html(total.toLocaleString());
}

function convert_to_number(input){
    return Number(input.replace(/[^0-9.-]+/g,""));
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/penjualan/beras/kontan/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=nama]').val(result.nama);
                $('#form-transaksi [name=catatan]').val(result.catatan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(el) {
    var id = $(el).data().id;
    Swal.fire({
        title: 'Konfirmasi',
        text: "Hapus data transaksi?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/kontan/delete_data_detail_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan proses order?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/kontan/batal_pengajuan_faktur')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Proses nota penjualan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let data_pembayaran = [];
            let id = $(this).data().id;

            data_pembayaran = {
                jenis_pembayaran : $('[name=jenis_pembayaran]:checked').val(),
                id_kas : $('[name=id_kas] option:selected').val(),
                id_rek_pengirim : $('[name=id_rek_pengirim]').find('option:selected').val(),
                grandtotal :convert_to_number($('#grandtotal').html())
            };

            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/kontan/simpan_data')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id,
                    'pembayaran' : data_pembayaran,
                },
                success: function(result) {
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                    $.ajax({
                        url: "<?= base_url('api/internal/penjualan/beras/kontan/proses_faktur')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'token' : "{{$this->session->auth['token']}}",
                            'id': id,
                        },
                        success: function(result) {
                            reload_page();
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                }
            });
        }
    });
})
</script>
@end