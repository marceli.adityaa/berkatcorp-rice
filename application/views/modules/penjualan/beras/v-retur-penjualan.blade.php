@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('penjualan/beras/retur/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Customer</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                    @foreach ($area as $ar)
                                        <optgroup label="{{ucwords($ar['nama'])}}">
                                        @foreach ($customer as $c)
                                            @if($ar['id'] == $c['id_area'])
                                                @if (isset($get['customer']) && $get['customer'] == $c['id'])
                                                    <option value="{{$c['id']}}" selected>{{ucwords($c['nama'])}}</option>
                                                @else
                                                    <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                        </optgroup>
                                    @endforeach
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Area</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="area" class="form-control select2">
                                <option value="all">All</option>
                                    @foreach($area as $row){
                                        @if (isset($get['area']) && $get['area'] == $row['id'])
                                            <option value="{{$row['id']}}" selected>{{strtoupper($row['nama'])}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{strtoupper($row['nama'])}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-20">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            @if(!empty($this->input->get()))
                                <a href="{{base_url('penjualan/beras/retur')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-10"></i>Reset</button></a>
                                <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Retur Penjualan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_transaksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Area</th>
                    <th data-sortable="true">Customer</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Tonase (Kg)</th>
                    <th data-sortable="true">Nominal (Rp)</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('penjualan/beras/retur/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Transaksi'><i class='fa fa-sign-in'></i></button></a> ";
                            echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit tugas' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                            if($row['status'] == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['status'] == 0){
                                echo "<td><label class='badge badge-secondary'>Belum diisi</label></td>";
                            }else{
                                echo "<td><label class='badge badge-success'>Selesai</label></td>";
                            }
                            echo "<td>".$row['tanggal']."</td>";
                            echo "<td><label class='badge badge-warning'>".ucwords($row['area'])."</label></td>";
                            echo "<td>".$row['customer']."</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td class='text-center'>".monefy($row['tonase'], false)."</td>";
                            echo "<td class='text-center'>".monefy($row['nominal'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data retur penjualan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Retur Penjualan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('penjualan/beras/retur/submit_transaksi')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Retur <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="date" class="form-control" name="tanggal" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Customer <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_customer" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($area as $ar)
                                                <optgroup label="{{ucwords($ar['nama'])}}">
                                                @foreach ($customer as $c)
                                                    @if($ar['id'] == $c['id_area'])
                                                        <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                    @endif
                                                @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off" required=""> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_transaksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/penjualan/beras/retur/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tanggal]').val(result.tanggal);
                $('#form-transaksi [name=id_customer]').val(result.id_customer).change();
                $('#form-transaksi [name=keterangan]').val(result.keterangan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/retur/delete_data_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let customer = $("#form-filter [name=customer]").val();
    let area = $("#form-filter [name=area]").val();
    let status = $("#form-filter [name=status]").val();
    var url = "<?= base_url('penjualan/beras/retur/export?')?>"+'start='+start+'&end='+end+'&customer='+customer+'&area='+area+'&status='+status;
    window.open(url, '_blank');
}
</script>
@end