@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('penjualan/beras/surat_pemesanan/index?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Customer</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                    @foreach ($area as $ar)
                                        <optgroup label="{{ucwords($ar['nama'])}}">
                                        @foreach ($customer as $c)
                                            @if($ar['id'] == $c['id_area'])
                                                @if (isset($get['customer']) && $get['customer'] == $c['id'])
                                                    <option value="{{$c['id']}}" selected>{{ucwords($c['nama'])}}</option>
                                                @else
                                                    <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                        </optgroup>
                                    @endforeach
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Area</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="area" class="form-control select2">
                                <option value="all">All</option>
                                    @foreach($area as $row){
                                        @if (isset($get['area']) && $get['area'] == $row['id'])
                                            <option value="{{$row['id']}}" selected>{{strtoupper($row['nama'])}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{strtoupper($row['nama'])}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Sales</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="sales" class="form-control select2">
                                <option value="all">All</option>
                                    @foreach($sales as $row){
                                        @if (isset($get['sales']) && $get['sales'] == $row['id'])
                                            <option value="{{$row['id']}}" selected>{{strtoupper($row['nama'])}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{strtoupper($row['nama'])}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Surat Pemesanan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Pemesanan</button>
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Area</th>
                    <th data-sortable="true">Customer</th>
                    <th data-sortable="true">Sales</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('penjualan/beras/surat_pemesanan/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Pemesanan'><i class='fa fa-sign-in'></i></button></a> ";
                            echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit tugas' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                            if($row['status_po'] == 0){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }else{
                                echo "<a href='".base_url('penjualan/beras/surat_pemesanan/cetak_po/'.$row['id'])."' target='_blank'><button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Cetak Surat Pemesanan'><i class='fa fa-print'></i></button></a> ";
                            }
                            echo "</td>";
                            if($row['status_po'] == 0){
                                echo "<td><label class='badge badge-secondary'>Belum diisi</label></td>";
                            }else if($row['status_po'] == 1 && $row['status_pengiriman'] == 0){
                                echo "<td><label class='badge badge-info'>Input harga</label></td>";
                            }else if($row['status_po'] == 1 && $row['status_pengiriman'] == 1 && $row['status_selesai'] == 0){
                                echo "<td><label class='badge badge-dark'>Atur Pengiriman</label></td>";
                            }else if($row['status_po'] == 1 && $row['status_pengiriman'] == 1 && $row['status_selesai'] == 1){
                                echo "<td><label class='badge badge-success'>Selesai</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td>".$row['tanggal']."</td>";
                            echo "<td><label class='badge badge-warning'>".ucwords($row['area'])."</label></td>";
                            echo "<td>".$row['customer']."</td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['sales'])."</label></td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pemesanan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Pemesanan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('penjualan/beras/surat_pemesanan/submit_tambah_po')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Pemesanan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="date" class="form-control" name="tanggal" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Customer <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_customer" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($area as $ar)
                                                <optgroup label="{{ucwords($ar['nama'])}}">
                                                @foreach ($customer as $c)
                                                    @if($ar['id'] == $c['id_area'])
                                                        <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                    @endif
                                                @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sales <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_sales" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                                @foreach ($sales as $row)
                                                    <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="catatan" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/penjualan/beras/surat_pemesanan/json_get_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tanggal]').val(result.tanggal);
                $('#form-transaksi [name=id_customer]').val(result.id_customer).change();
                $('#form-transaksi [name=id_sales]').val(result.id_sales).change();
                $('#form-transaksi [name=catatan]').val(result.catatan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/surat_pemesanan/delete_data_tugas')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end