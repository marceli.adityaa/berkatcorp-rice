<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .page{
        height:200mm;
        width:64mm;
        page-break-after:always;
    }

    .items td {
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }

    table thead td {
        /* background-color: #EEEEEE; */
        text-align: center;
        border: 0.1mm solid #000000;
        /* font-variant: small-caps; */
        font-weight:bold;
    }

    /* table tbody td{
        border-bottom: 0.05mm solid #aaa;
    } */

    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        font-weight:bold;
        text-align:center;
    }

    .items td.totals {
        /* text-align: right; */
        border: 0.1mm solid #000000;
    }

    .items td.cost {
        text-align: "."center;
    }

    .ttd{
        font-size:9pt;
        text-align:center;
    }
    .text-center{
        text-align:center;
    }
    .text-left{
        text-align:left;
    }
    .text-right{
        text-align:right;
    }
    </style>
</head>

<body>
<!--mpdf
<htmlpageheader name="myheader">

</htmlpageheader>

<htmlpagefooter name="myfooter">
    
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<div class="page">
    <table width="100%">
        <tr>
            <td width="100%" style="color:#000;padding-top:10px;text-align:center;line-height:1.4;">
                <span style="font-weight: bold; font-size: 10.5pt;">PT. Berkat Anoegerah Sejahtera</span><br />
                <span style="font-size:9pt">Krajan, Plerean, Kec. Sumberjambe</span><br />
                <span style="font-size:9pt">Kab. Jember - Jawa Timur</span><br />
                <span style="font-family:dejavusanscondensed; font-size:9pt">&#9742;(0331) 566130</span> 
            </td>
        </tr>
        <hr style="margin-top:10px;margin-bottom:5px;height:0.1mm;color:#000;">
        <tr>
            <td width="100%" style="text-align:center;line-height:1.4;">
                <span style="font-weight: bold; font-size: 10.5pt;">NOTA PENJUALAN</span><br>
                <span style="font-size:9pt;">Nomor : <?=$detail['kode']?>-<?=zerofy($id,5)?></span><br>
                <span style="font-size:9pt;">Customer : <b><?=ucwords($detail['nama'])?></b></span><br>
                <span style="font-size:9pt;"><?= $detail['timestamp_input']?> | <?= ucwords($detail['username_input'])?>
            </td>
        </tr>
    </table>
    <hr style="margin-top:5px;height:0.1mm;color:#000;">
    <table width="100%" style="font-size: 9.5pt; border:none;margin-top:5px;" cellpadding="2">
        <?php 
        $total = 0;
        $item = 0;
        foreach($detail_sj as $row){
            echo '<tr style="padding-top:5px;">';
            echo '<td class="text-left" colspan="5">'.ucwords($row['merk']).' @'.$row['kemasan'].' KG</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td class="text-center">'.monefy($row['kuantitas'],false).' sak</td>';
            echo '<td class="text-center">x</td>';
            echo '<td class="text-right">'.monefy($row['harga_satuan'] * $row['kemasan'],false).'</td>';
            echo '<td class="text-center">=</td>';
            echo '<td class="text-right">'.monefy($row['kuantitas']*$row['kemasan']*$row['harga_satuan'],false).'</td>';
            echo '</tr>';
            $total += ($row['kuantitas']*$row['kemasan']*$row['harga_satuan']);
            $item += $row['kuantitas'];
        }
        
        
        ?>
            <tr>
                <td colspan="5">
                    <hr style="margin-top:5px;margin-bottom:0px;height:0.1mm;color:#000;">
                </td>
            </tr>
            <tr>
                <td class="totals text-left">(<?= $item?> item)</td>
                <td class="totals text-center" colspan="3"><b>Total</b></td>
                <td class="totals text-right"><b><?= monefy($total, false)?></b></td>
            </tr>
            <tr>
                <td colspan="5">
                    <hr style="margin-top:0px;margin-bottom:0px;height:0.1mm;color:#000;">
                </td>
            </tr>
        </table>

        <table class="" width="100%" style="font-size: 8pt; border-collapse: collapse; margin-top:0px" cellpadding="20">
            <tr>
                <td class="ttd">
                    Diperiksa
                </td>    
                <td class="ttd">
                    Dibuat Oleh
                </td>   
                
            </tr>
            <tr>
                <td class="ttd">
                    <b>( ___________ )</b>
                </td>
                <td class="ttd">
                    <b>( ___________ )</b>
                </td>

            </tr>
        </table>
        <hr style="margin-top:0px;margin-bottom:10px;height:0.1mm;color:#000;">
        <div style="text-align: center; font-size:9pt;">- Terima kasih atas kepercayaan Anda -</div>
    </div>
</body>

</html>