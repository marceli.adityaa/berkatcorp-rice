@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('penjualan/beras/pembayaran_tempo?')?>">
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Customer</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="customer" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$customer as $row){
                                            if(!empty($this->input->get('customer')) && $this->input->get('customer') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Area</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="area" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$area as $row){
                                            if(!empty($this->input->get('area')) && $this->input->get('area') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Sales</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="sales" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$sales as $row){
                                            if(!empty($this->input->get('sales')) && $this->input->get('sales') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Status</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="status" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$status as $key => $val){
                                            if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                                echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                            }else{
                                                echo '<option value="'.$key.'">'.$val.'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                                <a href="{{base_url('penjualan/beras/pembayaran_tempo')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-10"></i>Refresh</button></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tempo Penjualan Beras</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Area</th>
                    <th data-sortable="true">Sales</th>
                    <th data-sortable="true">Customer</th>
                    <th data-sortable="true">Kode Transaksi</th>
                    <th data-sortable="true">Tgl Transaksi</th>
                    <th data-sortable="true">Tgl Jatuh Tempo</th>
                    <th data-sortable="true">Umur (Hari)</th>
                    <th data-sortable="true">Jumlah Tagihan</th>
                    <th data-sortable="true">Sisa Tagihan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($penjualan)){
                        foreach($penjualan as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('penjualan/beras/pembayaran_tempo/detail/'.$row['id'])."' class='btn btn-info active'><i class='fa fa-sign-in'>&nbsp;</i> Detail</a> ";
                            echo "</td>";
                            if($row['is_lunas'] == 0 && $row['tgl_tempo'] <= date('Y-m-d')){
                                echo "<td><label class='badge badge-danger'>Jatuh Tempo</label></td>";
                            }else if($row['is_lunas'] == 0 && $row['tgl_tempo'] > date('Y-m-d')){
                                echo "<td><label class='badge badge-primary'>Berlangsung</label></td>";                               
                            }else if ($row['is_lunas'] == 1){
                                echo "<td><label class='badge badge-success'>Lunas</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['area']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['sales']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['customer']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['tanggal']."</label></td>";
                            echo "<td>";
                            if($row['tgl_tempo'] <= $now = date('Y-m-d') ){
                                echo "<label class='badge badge-danger'>".$row['tgl_tempo']."</label>";
                                echo " <label class='badge badge-danger' title='melewati tanggal'><i class='fa fa-bell-o'></i></label>";
                            }else{
                                echo "<label class='badge badge-primary'>".$row['tgl_tempo']."</label>";
                            }
                            echo "</td>";
                            echo "<td class='text-center'>";
                            if($row['is_lunas'] == 1){
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($row['tgl_bayar']));
                            }else{
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($now));
                            }
                            if($row['tgl_tempo'] <= $now = date('Y-m-d') ){
                                echo "<label class='tx-danger'>".$interval->days."</label>";
                            }else{
                                echo "<label class='tx-primary'>- ".$interval->days."</label>";
                            }
                            echo "</td>";
                            echo "<td class='text-center'>".monefy($row['grandtotal'], false)."</td>";
                            echo "<td class='text-center'>".monefy(($row['grandtotal']-$row['total_bayar']), false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data penjualan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('persetujuan/pembelian_gabah_submit_approval')?>" id="form-pembelian">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Pembelian | <label id="kode_pembelian" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <!-- <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Kode Pembelian</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="kode_pembelian" class="form-control" value="" readonly="">
                            </div>
                        </div> -->
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tanggal_pembelian" class="form-control" value="" readonly="">
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="supplier" class="form-control" readonly="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($supplier as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-1">Tempo</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-2">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row col-12 my-3 tempo">
                            <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_tempo" class="form-control tempo" value="">
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung cash">
                            <label class="col-sm-4 form-control-label">Sumber Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung transfer">
                            <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_pengirim" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_pengirim as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3 langsung transfer">
                            <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_penerima" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_penerima as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label">Dibayar oleh</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="dibayar">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label">Checker </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="checker">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Bayar</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_bayar" class="form-control" value="" required>
                            </div>
                        </div>
                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp</span>
                                    </div>
                                    <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal"></label>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
    
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function setuju(el) {
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            console.log(result);
            $('#kode_pembelian').html(result.kode_pembelian);
            $('[name=tanggal_pembelian]').val(result.tanggal_pembelian);
            $('[name=id]').val(result.id);
            $('#form-pembelian [name=supplier]').val(result.id_supplier).change();
            if(result.is_tempo == 1){
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val(result.tgl_tempo);
            }else{
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val('');
            }
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
            if(result.jenis_pembayaran == 'cash'){
                $('[name=id_kas]').val(result.id_kas).change();
                $('[name=id_rek_pengirim]').val('').change();
                $('[name=id_rek_penerima]').val('').change();
            }else if(result.jenis_pembayaran == 'transfer'){
                $('[name=id_kas]').val('').change();
                $('[name=id_rek_pengirim]').val(result.id_rek_pengirim).change();
                $('[name=id_rek_penerima]').val(result.id_rek_penerima).change();
            }
            if(result.dibayar_oleh == "0"){
                $('[name=dibayar]').val('').change();
            }else{
                $('[name=dibayar]').val(result.dibayar_oleh).change();
            }
            if(result.checker == "0"){
                $('[name=checker]').val('').change();
            }else{
                $('[name=checker]').val(result.checker).change();
            }
            if(result.tgl_bayar != "0000-00-00"){
                $('[name=tgl_bayar]').val(result.tgl_bayar);
            }
            $('#grandtotal').html(parseInt(result.grandtotal).toLocaleString());
            $("#modal_form").modal('show');
            cek_tempo();
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/pembelian_gabah_tolak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end