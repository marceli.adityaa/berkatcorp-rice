@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Data Retur Penjualan
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form>
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Retur</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$trx['tanggal']}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Customer</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{ucwords($trx['customer'])}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Keterangan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$trx['keterangan']}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Total Tonase (Kg)</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <h3 id="tonase">{{$tonase}}</h3>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($trx['status'] == 0)
                            <button type="button" class="btn btn-success btn-primary" id="proses" data-id="{{$trx['id']}}"><i class="fa fa-send mg-r-10"></i>Selesai</button>
                            @elseif($trx['status'] == 1)
                            <button type="button" class="btn btn-danger btn-success" id="batal" data-id="{{$trx['id']}}"><i class="fa fa-times mg-r-10"></i>Batalkan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi Retur</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        @if($trx['status'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        @endif
        </div>
        <table class="table table-striped table-white" id="tabel-detail">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th class="text-left">Aksi</th>
                    <th class="text-left">Merk </th>
                    <th class="text-center">Kemasan</th>
                    <th class="text-center">Kuantitas</th>
                    <th class="text-center">Harga Satuan</th>
                    <th class="text-center">Subtotal</th>
                    <th>Catatan</th>
                    <th>Input</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $total = 0;
                $grandtotal = 0;
                if(!empty($detail)){
                    foreach($detail as $row){
                        $subtotal = 0;
                        echo '<tr>';
                        echo '<td class="text-center">'.$no++.'</td>';
                        echo '<td>';
                        if($trx['status'] != 1){
                            echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                        }
                        echo '</td>';
                        echo '<td class="text-left">'.$row['merk'].'</td>';
                        echo '<td class="text-center">@'.$row['kemasan'].'KG</td>';
                        echo '<td class="text-center"><label class="qty">'.$row[ 'kuantitas'].'</label></td>';
                        echo '<td class="text-center"><input type="hidden" class="form-control text-center selesai" name="harga_satuan" value="'.$row['harga_satuan'].'" data-id="'.$row['id'].'">'.monefy($row['harga_satuan'],false).'</td>';
                        echo '<td class="text-center"><label class="subtotal">'.monefy(($row['kuantitas'] * $row['harga_satuan']),false).'</label>';
                        $subtotal += $row['kuantitas'] * $row['harga_satuan'];
                        echo '<input type="hidden" name="subtotal" value="'.($row['kuantitas'] * $row['harga_satuan']).'"></td>';
                        echo '<td>'.$row['catatan'].'</td>';
                        echo "<td><label class='badge badge-light'>".$row['username_input']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                        echo '</tr>';
                        $grandtotal += $subtotal;
                    }
                }
                ?>
                <tr>
                    <td class="totals text-center" colspan="6"><b class="form-control">Total Penjualan</b></td>
                    <td class="totals text-center"><label class="form-control tx-grandtotal"><b><?= monefy($grandtotal, false)?></b></label></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Tambah Data</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('penjualan/beras/retur/submit_detail_transaksi')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_retur" value="{{$trx['id']}}">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Merk <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_kemasan" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Pilih Salah Satu -</option>
                                            @foreach ($merk as $mrk)
                                                <optgroup label="{{ucwords($mrk['nama'])}}">
                                                @foreach ($kemasan as $km)
                                                    @if($mrk['id'] == $km['id_merk'])
                                                        <option value="{{$km['id']}}">{{ucwords($mrk['nama']).' @'.$km['kemasan']}}</option>
                                                    @endif
                                                @endforeach
                                                </optgroup>
                                            @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Harga Satuan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="number" class="form-control" name="harga_satuan" id="harga" autocomplete="off" value="0" required="" min="0"> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kuantitas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="number" class="form-control" name="kuantitas" id="kuantitas" autocomplete="off" value="0" required="" min="0"> 
                                    </div>
                                </div>

                                
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="catatan" autocomplete="off" required=""> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Subtotal <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <b><label class="form-control tx-20" id="subtotal">0</label></b>
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info" id="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-po').bootstrapTable({
        // pagination: true,
        // search: true,
        toolbar: '#toolbar'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.penggilingan, .proses').hide();
    
});

$(document).on('change keyup', '.kemasan', function(){
    var total_tonase = 0;
    $('#tabel-po .kemasan').each(function(a, b) {
        if(!isEmpty($(b).val())){
            total_tonase += (parseInt($(b).val()) * parseInt($(b).data('qty')));
        }
    });
    $('#tonase').empty().html(total_tonase);
    $('#tabel-po tr').each(function(a, b) {
        var subtonase = 0;
        if (a > 0) {
            $('.kemasan', b).each(function(c,d){
                if(!isEmpty($(d).val())){
                    subtonase += (parseInt($(d).val()) * parseInt($(d).data('qty')));
                }
            })
        }
        $(b).find('.sub-tonase').empty().html(subtonase);
    });
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


$('#kuantitas, #harga').on('change keyup', function(){
    hitung_subtotal();
})

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

function hitung_subtotal(){
    let harga = parseInt($('#harga').val());
    let kuantitas = parseInt($('#kuantitas').val());

    $('#subtotal').empty().html((kuantitas * harga).toLocaleString());
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

$('#simpan').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            $('#tabel-po tr').each(function(a, b) {
                if (a > 0) {
                    $('.kemasan', b).each(function(c,d){
                        if(!isEmpty($(d).val())){
                            data.push(
                                {
                                    "id_kemasan" : $(d).data('produk'), 
                                    "kemasan" : $(d).data('qty'),
                                    "kuantitas" : $(d).val(),
                                }
                            );
                        }
                    })
                    
                }
            });
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/surat_pemesanan/simpan_data_pemesanan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id,
                    'data' : data
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#batal').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Batalkan proses order?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/retur/batal_pengajuan_proses')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': $(this).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
})

$('#proses').click(function(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            var id = $(this).data('id');
            
            $.ajax({
                url: "<?= base_url('api/internal/penjualan/beras/retur/simpan_data')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'token' : "{{$this->session->auth['token']}}",
                    'id': id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
        
    });
})
</script>
@end