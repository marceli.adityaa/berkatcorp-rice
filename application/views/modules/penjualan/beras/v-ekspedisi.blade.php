@layout('commons/index')

@section('content')
<div id="accordion2" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header bg-danger" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="tx-gray-800 transition">
                    Atur Ekspedisi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card">
                <div class="card-body">
                    <div id="toolbar" class="mg-b-10">
                    </div>
                    @if(!empty($this->input->get()))    
                    <table class="table table-striped mg-t-10 table-white" id="tabel_po">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Aksi</th>
                                <th data-sortable="true">Kode</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Area</th>
                                <th data-sortable="true">Customer</th>
                                <th data-sortable="true">Sales</th>
                                <th data-sortable="true">Tonase</th>
                                <th data-sortable="true">Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1;
                                if(!empty($po)){
                                    foreach($po as $row){
                                        echo "<tr>";
                                        echo "<td class='text-center'>".$no++."</td>";
                                        echo "<td class='text-nowrap'>
                                        <button type='button' class='btn btn-primary' title='detail' data-toggle='tooltip' data-placement='top' onclick='detail_po(this)' data-id='".$row['id']."'><i class='fa fa-info'></i></button>
                                        <button type='button' class='btn btn-warning' title='update resi' data-toggle='tooltip' data-placement='top' onclick='update_resi(this)' data-id='".$row['id']."'><i class='fa fa-truck'></i></button>
                                        </td>";
                                        echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                                        echo "<td>".$row['tanggal']."</td>";
                                        echo "<td><label class='badge badge-warning'>".ucwords($row['area'])."</label></td>";
                                        echo "<td>".$row['customer']."</td>";
                                        echo "<td><label class='badge badge-light'>".ucwords($row['sales'])."</label></td>";
                                        echo "<td>".monefy($row['tonase'], false)."</td>";
                                        echo "<td>".$row['catatan']."</td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                    @else
                    <p class="text-center">Klik filter untuk menampilkan data PO</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('penjualan/beras/ekspedisi/index?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Kendaraan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="kendaraan" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($kendaraan as $key => $row){
                                        if(isset($get['kendaraan']) && $get['kendaraan'] == $row->id){
                                            echo '<option value="'.$row->id.'" selected>'.strtoupper($row->no_pol).'</option>';
                                        }else{
                                            echo '<option value="'.$row->id.'">'.strtoupper($row->no_pol).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Ekspedisi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_ekspedisi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">No Resi</th>
                    <th data-sortable="true">Tanggal Kirim</th>
                    <th data-sortable="true">Plat Kendaraan</th>
                    <th data-sortable="true">Sopir</th>
                    <th data-sortable="true">Tujuan</th>
                    <th data-sortable="true">Berat Muatan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($ekspedisi)){
                        foreach($ekspedisi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('penjualan/beras/ekspedisi/detail/'.$row->no_resi)."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Ekspedisi'><i class='fa fa-sign-in'></i></button></a> ";
                            echo "</td>";
                            if($row->is_done == 0 && $row->status_muatan == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu input</label></td>";
                            }else if($row->is_done == 0 && $row->status_muatan == 1){
                                echo "<td><label class='badge badge-warning'>Diproses</label></td>";
                            }else if($row->is_done == 1){
                                echo "<td><label class='badge badge-success'>Selesai</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row->no_resi."</label></td>";
                            echo "<td>".$row->tanggal_jalan."</td>";
                            echo "<td>".$row->no_pol."</td>";
                            echo "<td>".$row->sopir."</td>";
                            echo "<td>".$row->tujuan."</td>";
                            echo "<td>".monefy($row->berat_muatan,false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Pesanan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="form-layout form-layout-4">
                            <table class="table table-stripped table-white" id="tabel-info">
                                <thead>
                                    <th>Merk</th>
                                    <th>Kemasan</th>
                                    <th>Kuantitas</th>
                                    <th>Tonase</th>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>                            
                            
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_update_resi" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Update Resi</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form method="post" id="form-resi" action="{{base_url('penjualan/beras/ekspedisi/submit_resi')}}">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_po" value="">
                                <div class="row col-12 my-3">
                                    <label class="col-sm-4 form-control-label">Nomor Resi</label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                <select name="no_resi" class="form-control select2" require="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($ekspedisi as $row)
                                    @if($detail['no_resi'] == $row->no_resi)
                                        <option value="{{$row->no_resi}}" selected="">{{$row->no_resi.' ( '.$row->no_pol.' - '.strtoupper($row->sopir)}} )</option>
                                        @else
                                        <option value="{{$row->no_resi}}">{{$row->no_resi.' ( '.$row->no_pol.' - '.strtoupper($row->sopir)}} ) </option>
                                        @endif
                                    @endforeach

                                            </select>
                                        <!-- <span><button class="btn btn-info" type="button" title="cek resi" onclick="show_detail(this)"><i class="fa fa-question-circle"></i></button></span> -->

                                            <!-- <input type="text" name="no_resi" class="form-control" value="" required="" autocomplete="off"> -->
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-primary" id="btn-submit"><i class="fa fa-send mr-2"></i>Submit</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_po').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('#tabel_ekspedisi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('tabel-info').bootstrapTable();

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function detail_po(el) {
    let id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/penjualan/beras/ekspedisi/json_get_detail_po')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                let subtotal = 0;
                $('#tabel-info').find('tbody').empty();
                $(result).each(function(a,b){
                    $('#tabel-info').find('tbody').append('\
                        <tr>\
                        <td>'+b.merk+'</td>\
                        <td>'+b.kemasan+'</td>\
                        <td>'+b.kuantitas+'</td>\
                        <td>'+(b.kemasan * b.kuantitas)+'</td>\
                        </tr>\
                    ');
                    subtotal += (b.kemasan * b.kuantitas);
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function update_resi(el) {
    let id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/penjualan/beras/faktur/json_get_detail_tugas')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('[name=id_po]').val(id);
                $('[name=no_resi]').val(result.no_resi);
                $('.result, #btn-submit').show();
                $('#modal_update_resi').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

// function show_detail(el){
//     let resi = $('[name=no_resi]').val();
//     if (resi != '') {
//         $.ajax({
//             url: "<?= base_url('api/internal/penjualan/beras/ekspedisi/json_get_detail_ekspedisi')?>",
//             type: "POST",
//             dataType: "json",
//             data: {
//                 'resi': resi,
//             },
//             success: function(result) {
//                 if(result != false){
//                     $('#plat').html(result.no_pol);
//                     $('#sopir').html(result.sopir);
//                     $('#tgl_jalan').html(result.tanggal_jalan);
//                     $('.result, #btn-submit').show(300);
//                 }else{
//                     swal.fire("Perhatian", "Mohon maaf data resi tidak ditemukan.", "error");
//                 }
//             },
//             error: function(e) {
//                 console.log(e);
//             },
//             complete: function(e) {}
//         });
//     }
// }
</script>
@end