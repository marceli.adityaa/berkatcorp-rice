@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Ekspedisi - Pengambilan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table w-100 table-striped mg-t-10 table-white" id="tabel_supplier">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Lokasi</th>
                    <th data-sortable="true">Kendaraan</th>
                    <th data-sortable="true">Sopir</th>
                    <th data-sortable="true">Jenis Barang</th>
                    <th data-sortable="true">Tanggal Transaksi</th>
                    <th data-sortable="true">Jenis Tarif</th>
                    <th data-sortable="true">Tarif</th>
                    <th data-sortable="true">Gabah</th>
                    <th data-sortable="true">Beras</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($supplier)){
					foreach($supplier as $row){
						echo "<tr>";
						echo "<td class='text-center'>".$no++."</td>";
						echo "<td class='text-nowrap'>";
						echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
						if($row['status'] == 1){
							echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set Nonaktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button>";
						}else{
							echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set Aktif' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
						}
						
						echo "</td>";
                        echo "<td>".$row['nama']."</td>";
                        echo "<td>".$row['area']."</td>";
                        echo "<td>".$row['alamat']."</td>";
                        echo "<td>".$row['telp']."</td>";
                        echo "<td>".$row['saldo_sak']."</td>";
                        echo "<td>Rp ".monefy($row['saldo_bon'], false)."</td>";
                        echo "<td>Rp ".monefy($row['limit_bon'], false)."</td>";
                        if($row['supply_gabah'] == 1){
							echo "<td><label class='badge badge-success'>Ya</label></td>";
						}else{
							echo "<td><label class='badge badge-secondary'>Tidak</label></td>";
                        }
                        if($row['supply_beras'] == 1){
							echo "<td><label class='badge badge-success'>Ya</label></td>";
						}else{
							echo "<td><label class='badge badge-secondary'>Tidak</label></td>";
						}
						if($row['status'] == 1){
							echo "<td><label class='badge badge-success'>Aktif</label></td>";
						}else{
							echo "<td><label class='badge badge-secondary'>Nonaktif</label></td>";
						}
						echo "</tr>";
                    }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form id="form">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Ekspedisi Pengambilan</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="token" value="{{$this->session->auth['token']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="lokasi" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($area as $row)
                                    <option value="{{$row->id}}" data-rit="{{$row->per_rit}}" data-ton="{{$row->per_ton}}">{{$row->kota}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Kendaraan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="kendaraan_id" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kendaraan as $row)
                                    <option value="{{$row->id}}">{{$row->no_pol}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Supir <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="sopir_id" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($supir as $row)
                                    <option value="{{$row->id}}">{{ucwords($row->nama)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jenis Barang <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="jenis_barang" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tgl Transaksi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="tanggal_transaksi" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jenis Tarif <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="jenis_tarif" class="custom-control-input" value="rit">
                                    <label class="custom-control-label" for="customRadioInline1">RIT</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="jenis_tarif" class="custom-control-input" value="tonase">
                                    <label class="custom-control-label" for="customRadioInline2">TONASE</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tarif <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control numeric" name="tarif" autocomplete="off" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="save()"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_supplier').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[name=saldo_bon], [name=limit_bon]').autoNumeric('init', {
        'mDec': 0
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});

function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        // Save
        var data = $('#form').serializeArray();
        $.ajax({
            url: "{{$this->config->item('url_transport').'api/external/ekspedisi/submit'}}",
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('#tabel_supplier').bootstrapTable('refresh');
                    $('#modal_form').modal('hide');
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});

$('[name=jenis_tarif]').change(function(){
    var tarif = 0;
    if($(this).val() == 'rit' && $('[name=lokasi]').val() != ''){
        tarif = $('[name=lokasi]').find(':selected').data().rit;
    }
    if($(this).val() == 'tonase' && $('[name=lokasi]').val() != ''){
        tarif = $('[name=lokasi]').find(':selected').data().ton;
    }
    $('[name=tarif]').val(tarif);
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('supplier/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=id_area]').val(result.id_area).change();
                $('[name=alamat]').val(result.alamat);
                $('[name=telp]').val(result.telp);
                $('[name=saldo_sak]').val(result.saldo_sak);
                $('[name=saldo_bon]').val(result.saldo_bon);
                $('[name=limit_bon]').val(result.limit_bon);
                $('[name=saldo_bon], [name=limit_bon]').autoNumeric('destroy');
                $('[name=saldo_bon], [name=limit_bon]').autoNumeric('init', {
                    'mDec': 0
                });
                if (result.supply_beras == 1) {
                    $('[name=supply_beras]').prop('checked', true);
                }
                if (result.supply_gabah == 1) {
                    $('[name=supply_gabah]').prop('checked', true);
                }
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('supplier/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('supplier/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end