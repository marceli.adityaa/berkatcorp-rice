@layout('commons/index')

@section('content')
<style>
    .nowrap{
        white-space:nowrap;
    }
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/piutang/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="client" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array) $customer as $row){
                                        if($row['is_primary'] == 0){
                                            if(!empty($get['client']) && $get['client'] == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div> -->
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Saldo Uang Muka Supplier</h6>
    </div>
    <div class="card-body">
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="false">Aksi</th>
                    <th data-sortable="true">Supplier</th>
                    <!-- <th data-sortable="true">Jenis</th> -->
                    <th data-sortable="true">Status</th>
                    <!-- <th data-sortable="true">Tanggal Dibuat</th> -->
                    <th data-sortable="true">Terakhir Bayar</th>
                    <th data-sortable="true">Total Piutang</th>
                    <th data-sortable="true">Sisa Pokok</th>
                    <th data-sortable="true">Jmlh Pembayaran</th>
                    <th data-sortable="true">Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    if(!empty($piutang)){
                        foreach($piutang as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='nowrap'><a href='".base_url('manajemen/piutang/detail_rekap/'.$row['id'])."' target='_blank' class='btn btn-info' title='Detail Transaksi'><i class='fa fa-info'></i></a> 
                            <a href='".base_url('manajemen/piutang/export_detail/'.$row['id'])."' target='_blank' class='btn btn-success' title='Export Data'><i class='fa fa-file-excel-o'></i></a>
                            </td>";

                            echo "<td><label class='badge badge-light'>".ucwords($row['debitur'])."</label></td>";
                            // echo "<td><label class='badge badge-light'>".ucwords($row['jenis_piutang'])."</label></td>";
                            
                            if($row['is_paid_off'] != 1){
                                echo "<td><label class='badge badge-secondary'>Belum Lunas</label>";
                                if($row['total_piutang'] == 0){
                                    echo "<br><label class='badge badge-warning'>Menunggu Verifikasi</label>";
                                }
                                echo "</td>";
                            }else{
                                echo "<td><label class='badge badge-success'>Lunas</label></td>";
                            }
                            // echo "<td>".$row['tgl_piutang']."</td>";
                            echo "<td>".$row['tgl_terakhir_bayar']."</td>";
                            echo "<td>".monefy($row['total_piutang'], false)."</td>";
                            echo "<td>".monefy($row['sisa_pokok'], false)."</td>";
                            echo "<td>".$row['angsuran']."x</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "</tr>";

                            if($row['is_paid_off'] != 1){
                                $total += $row['sisa_pokok'];
                            }
                        }
                    }
				?>
            </tbody>
        </table>
        <table class="table table-striped mg-t-10 table-white">
            <tr class="tx-bold tx-16">
                <td>Total Sisa Pokok</td>
                <td>{{monefy($total, false)}}</td>
            </tr>
        </table>
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();
});


function export_data(el){
    let start = '';
    let end = '';
    let client = $("#form-filter [name=client]").val();
    let status = $("#form-filter [name=status]").val();
    var url = "<?= base_url('manajemen/piutang/export_rekap?')?>"+'start='+start+'&end='+end+'&client='+client+'&status='+status;
    window.open(url, '_blank');
}

</script>
@end