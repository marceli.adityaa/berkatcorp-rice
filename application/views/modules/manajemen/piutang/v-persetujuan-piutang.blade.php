@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/piutang/persetujuan?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="debitur" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$customer as $row){
                                        if(!empty($this->input->get('debitur')) && $this->input->get('debitur') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div> -->
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pengajuan Uang Muka</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
        <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Customer</th>
                    <th data-sortable="true">Tgl Bayar</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Tambah Uang Muka</th>
                    <th data-sortable="true">Bayar Pokok</th>
                    <th data-sortable="true">Bayar Bunga</th>
                    <th data-sortable="true">Pokok Awal</th>
                    <th data-sortable="true">Sisa Pokok</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($detail)){
                        foreach($detail as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['is_verifikasi'] == 0){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }else if($row['is_verifikasi'] == 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "</td>";
                            if($row['is_verifikasi'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";
                            }else if($row['is_verifikasi'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td>".ucwords($row['debitur'])."</td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            if($row['kas_is_rekening'] == 0){
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_kas'])."</label></td>";
                            }else{
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_bank'])." | ".$row['kas_no_rek']."</label></td>";
                            }
                            
                            echo "<td><label class='badge badge-light'>".monefy($row['tambah_piutang'], false)."</label></td>";
                            
                            echo "<td>".monefy($row['bayar_pokok'], false)."</td>";
                            echo "<td>".monefy($row['bayar_bunga'], false)."</td>";
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['pokok_awal'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['sisa_pokok'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            echo "<td>".$row['remark']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data transaksi</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('manajemen/piutang/submit_form_approval')?>" id="form-pembayaran">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Transaksi </h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Transaksi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tgl_transaksi" class="form-control" value="" readonly="">
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Customer</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_customer" class="form-control" disabled="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($customer as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="row col-12 my-3 langsung">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung cash">
                            <label class="col-sm-4 form-control-label">Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung transfer">
                            <label class="col-sm-4 form-control-label">Rekening</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rekening" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rekening as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tambah Bon</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tambah_piutang" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah tambah piutang">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Bayar Pokok</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="bayar_pokok" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah bayar pokok">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Bayar Bunga</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="bayar_bunga" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah bayar bunga">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="remark" class="form-control" value="" autocomplete="off" placeholder="Masukkan catatan">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        // dropdownParent: $('#modal_form')
    });
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});

function remark(e){
    $.ajax({
        url: "<?= base_url('manajemen/piutang/get_detail_piutang')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': $(e).data().id
        },
        success: function(result) {
            Swal.fire('Catatan', result.remark, 'info');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

// function setuju_lama(el) {
//     Swal.fire({
//         title: 'Konfirmasi',
//         text: "Data transaksi akan disetujui, lanjutkan?",
//         type: 'info',
//         showCancelButton: true,
//         confirmButtonColor: '#3085d6',
//         cancelButtonColor: '#d33',
//         confirmButtonText: 'Ya',
//         cancelButtonText: 'Batal',
//         reverseButtons: true
//     }).then((result) => {
//         if (result.value) {
//             $.ajax({
//                 url: "<?= base_url('manajemen/piutang/approve_transaksi_piutang')?>",
//                 type: "POST",
//                 dataType: "json",
//                 data: {
//                     'id' : $(el).data().id,
//                 },
//                 success: function(result) {
//                     reload_page();
//                 },
//                 error: function(e) {
//                     console.log(e);
//                 },
//                 complete: function(e) {
//                 }
//             });
//         }
//     })
// }

$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});


function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function setuju(el){
    $.ajax({
        url: "<?= base_url('manajemen/piutang/get_detail_piutang')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            $('[name=tgl_transaksi]').val(result.tgl_transaksi);
            $('[name=id]').val(result.id);
            $('#form-pembayaran [name=id_customer]').val(result.id_customer).change();
            
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
            if(result.jenis_pembayaran == 'cash'){
                $('[name=id_kas]').val(result.id_kas).change();
                $('[name=id_rekening]').val('').change();
            }else if(result.jenis_pembayaran == 'transfer'){
                $('[name=id_kas]').val('').change();
                $('[name=id_rekening]').val(result.id_kas).change();
            }
            
            $('[name=tambah_piutang]').val(result.tambah_piutang);
            $('[name=bayar_pokok]').val(result.bayar_pokok);
            $('[name=bayar_bunga]').val(result.bayar_bunga);
            $('[name=remark]').val(result.remark);
            $("#modal_form").modal('show');
            $('.autonumeric').autoNumeric('destroy');
            $('.autonumeric').autoNumeric('init', {
                'mDec': 0
            });
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/piutang/decline_transaksi_piutang')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end