@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/piutang/kelola?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input class="form-control" value="{{$piutang['debitur']}}" readonly>
                        </div>
                    </div>

                    <!-- <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jenis Piutang</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input class="form-control" value="Jangka {{ucwords($piutang['jenis_piutang'])}}" readonly>
                        </div>
                    </div> -->

                    <!-- <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Dibuat</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$piutang['tgl_piutang']}}" readonly>
                        </div>
                    </div> -->
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sisa Pokok</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                </div>
                                <input type="text" class="form-control" value="{{monefy($piutang['sisa_pokok'], false)}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($piutang['is_paid_off'] != 1)
                                @if($piutang['total_piutang'] == 0)
                                    <label class='badge badge-warning'>Menunggu Verifikasi</label>
                                @else
                                    <label class='badge badge-secondary'>Belum Lunas</label>
                                @endif
                            @else
                                <label class='badge badge-success'>Lunas</label>
                            @endif
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <a href={{base_url('manajemen/piutang/export_detail/'.$piutang['id'])}} target='_blank' class='btn btn-success'><i class='fa fa-file-excel-o'></i> Export Data</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Detail Transaksi Uang Muka</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_transaksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Tgl Bayar</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Tambah Uang Muka</th>
                    <th data-sortable="true">Bayar Pokok</th>
                    <th data-sortable="true">Bayar Bunga</th>
                    <th data-sortable="true">Pokok Awal</th>
                    <th data-sortable="true">Sisa Pokok</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($detail)){
                        foreach($detail as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['source']."</label></td>";
                            if($row['kas_is_rekening'] == 0){
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_kas'])."</label></td>";
                            }else{
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_bank'])." | ".$row['kas_no_rek']."</label></td>";
                            }
                            
                            echo "<td>".monefy($row['tambah_piutang'], false)."</td>";
                            
                            echo "<td>".monefy($row['bayar_pokok'], false)."</td>";
                            echo "<td>".monefy($row['bayar_bunga'], false)."</td>";
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['pokok_awal'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['sisa_pokok'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            echo "<td>".$row['remark']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
       
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_transaksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();
});

</script>
@end