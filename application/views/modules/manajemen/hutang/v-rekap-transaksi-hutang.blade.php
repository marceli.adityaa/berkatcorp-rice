@layout('commons/index')

@section('content')
<style>
    .nowrap{
        white-space:nowrap;
    }
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/hutang/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Pemberi Hutang</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="client" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$customer as $row){
                                        if($row['is_primary'] == 0){
                                            if(!empty($this->input->get('client')) && $this->input->get('client') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Saldo Hutang Perusahaan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_hutang">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Pemberi Hutang</th>
                    <th data-sortable="true">Tanggal Dibuat</th>
                    <th data-sortable="true">Terakhir Bayar</th>
                    <th data-sortable="true">Total hutang</th>
                    <th data-sortable="true">Sisa Pokok</th>
                    <th data-sortable="true">Jmlh Pembayaran</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    if(!empty($hutang)){
                        foreach($hutang as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='nowrap'><a href='".base_url('manajemen/hutang/detail_rekap/'.$row['id'])."' target='_blank' class='btn btn-info' title='Detail Transaksi'><i class='fa fa-info'></i></a> 
                            <a href='".base_url('manajemen/hutang/export_detail/'.$row['id'])."' target='_blank' class='btn btn-success' title='Export Data'><i class='fa fa-file-excel-o'></i></a>
                            </td>";
                            if($row['is_paid_off'] != 1){
                                echo "<td><label class='badge badge-secondary'>Belum Lunas</label>";
                                if($row['total_hutang'] == 0){
                                    echo "<br><label class='badge badge-warning'>Menunggu Verifikasi</label>";
                                }
                                echo "</td>";
                            }else{
                                echo "<td><label class='badge badge-success'>Lunas</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".ucwords($row['kreditur'])."</label></td>";
                            echo "<td>".$row['tgl_hutang']."</td>";
                            echo "<td>".$row['tgl_terakhir_bayar']."</td>";
                            
                            echo "<td>".monefy($row['total_hutang'], false)."</td>";
                            echo "<td>".monefy($row['sisa_pokok'], false)."</td>";
                            echo "<td class='text-center'>".$row['angsuran']."x</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                            if($row['is_paid_off'] != 1){
                                $total += $row['sisa_pokok'];
                            }
                        }
                    }
				?>
            </tbody>
        </table>
        <table class="table table-striped mg-t-10 table-white">
            <tr class="tx-bold tx-16">
                <td>Total Hutang Belum Dibayar</td>
                <td>{{monefy($total, false)}}</td>
            </tr>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Transaksi Hutang</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('manajemen/hutang/submit_hutang')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_detail" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_hutang" required=""> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kreditur <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <select name="id_customer" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Metode Pembayaran</label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">
                                            @if($row == 'tbd')
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                            @else
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="row mg-t-10 tbd">
                                    <label class="col-4 form-control-label">Sumber Kas<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="id_kas" class="form-control cash">
                                            @foreach ($kas as $row)
                                                <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                            @endforeach
                                        </select>

                                        <select name="id_rekening" class="form-control transfer">
                                            @foreach ($rekening as $row)
                                                <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Nominal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="nominal" required="" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="keterangan"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_hutang').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let client = $("#form-filter [name=client]").val();
    let status = $("#form-filter [name=status]").val();
    var url = "<?= base_url('manajemen/hutang/export_rekap?')?>"+'start='+start+'&end='+end+'&client='+client+'&status='+status;
    window.open(url, '_blank');
}
</script>
@end