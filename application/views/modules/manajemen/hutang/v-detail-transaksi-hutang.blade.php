@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/hutang/kelola?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Pemberi Hutang</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input class="form-control" value="{{$hutang['kreditur']}}" readonly>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Hutang</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" value="{{$hutang['tgl_hutang']}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sisa Pokok</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                </div>
                                <input type="text" class="form-control" value="{{monefy($hutang['sisa_pokok'], false)}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($hutang['is_paid_off'] != 1)
                                @if($hutang['total_hutang'] == 0)
                                    <label class='badge badge-warning'>Menunggu Verifikasi</label>
                                @else
                                    <label class='badge badge-secondary'>Belum Lunas</label>
                                @endif
                            @else
                                <label class='badge badge-success'>Lunas</label>
                            @endif
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Detail Transaksi Hutang</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah_angsuran">+ Bayar Hutang</button>
            <button class="btn btn-dark" id="tambah_hutang">+ Tambah Hutang</button>
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Tgl Bayar</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Tambah hutang</th>
                    <th data-sortable="true">Bayar Pokok</th>
                    <th data-sortable="true">Bayar Bunga</th>
                    <th data-sortable="true">Pokok Awal</th>
                    <th data-sortable="true">Sisa Pokok</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($detail)){
                        foreach($detail as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['is_verifikasi'] != 1){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit transaksi' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['is_verifikasi'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Verifikasi</label></td>";
                            }else if($row['is_verifikasi'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            
                            echo "<td><label class='badge badge-light'>".$row['source']."</label></td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            if($row['kas_is_rekening'] == 0){
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_kas'])."</label></td>";
                            }else{
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-info'>".ucwords($row['kas_bank'])." | ".$row['kas_no_rek']."</label></td>";
                            }
                            
                            echo "<td>".monefy($row['tambah_hutang'], false)."</td>";
                            
                            echo "<td>".monefy($row['bayar_pokok'], false)."</td>";
                            echo "<td>".monefy($row['bayar_bunga'], false)."</td>";
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['pokok_awal'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            if($row['is_verifikasi'] == 1){
                                echo "<td>".monefy($row['sisa_pokok'], false)."</td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>?</label></td>";
                            }
                            echo "<td><p>".$row['remark']."</p></td>";
                            // echo "<td><button class='btn btn-info' type='button' data-id='".$row['id']."' onclick='remark(this)'><i class='fa fa-list'></i></button></td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
       
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_angsuran" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Angsuran</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('manajemen/hutang/submit_angsuran')}}" id="form-angsuran" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_hutang" value="{{$hutang['id']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_transaksi" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Metode Pembayaran</label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">
                                            @if($row == 'tbd')
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                            @else
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="row mg-t-10 tbd">
                                    <label class="col-4 form-control-label">Sumber Kas<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="id_kas" class="form-control cash">
                                            @foreach ($kas as $row)
                                                <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                            @endforeach
                                        </select>

                                        <select name="id_rekening" class="form-control transfer">
                                            @foreach ($rekening as $row)
                                                <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Bayar Pokok <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="bayar_pokok" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Bayar Bunga <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="bayar_bunga" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sisa Pokok <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="hidden" name="sisa_pokok_val" value="">
                                            <input type="text" class="form-control" name="sisa_pokok" readonly=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Total Bayar <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control" name="total_bayar" readonly=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_hutang" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah hutang</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('manajemen/hutang/submit_tambah_hutang')}}" id="form-hutang" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_hutang" value="{{$hutang['id']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_transaksi" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Metode Pembayaran</label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p-radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">
                                            @if($row == 'tbd')
                                                <label class="custom-control-label" for="p-radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                            @else
                                                <label class="custom-control-label" for="p-radio-{{$key}}">{{ucwords($row)}}</label>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="row mg-t-10 tbd">
                                    <label class="col-4 form-control-label">Sumber Kas<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="id_kas" class="form-control cash">
                                            @foreach ($kas as $row)
                                                <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                            @endforeach
                                        </select>

                                        <select name="id_rekening" class="form-control transfer">
                                            @foreach ($rekening as $row)
                                                <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Tambah hutang <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="tambah_hutang" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sisa Pokok <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="hidden" name="sisa_pokok_val" value="">
                                            <input type="text" class="form-control" name="sisa_pokok" readonly=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_metode_pembayaran();
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah_angsuran').click(function() {
    $.ajax({
        url: "<?= base_url('manajemen/hutang/get_sisa_pokok')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': {{$hutang['id']}}
        },
        success: function(result) {
            $('#form-angsuran [name=sisa_pokok]').val(result);
            $('#form-angsuran [name=sisa_pokok_val]').val(result);
            $('#form-angsuran [name=sisa_pokok]').autoNumeric('init', {
                'mDec': 0
            });
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    $('#form-angsuran').trigger('reset');
    $("#modal_angsuran").modal('show');
});

$('#tambah_hutang').click(function() {
    $.ajax({
        url: "<?= base_url('manajemen/hutang/get_sisa_pokok')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': {{$hutang['id']}}
        },
        success: function(result) {
            $('#form-hutang [name=sisa_pokok]').val(result);
            $('#form-hutang [name=sisa_pokok_val]').val(result);
            $('#form-hutang [name=sisa_pokok]').autoNumeric('init', {
                'mDec': 0
            });
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    $('#form-angsuran').trigger('reset');
    $("#modal_hutang").modal('show');
});

$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});

$('#form-angsuran [name=bayar_pokok]').on('keyup change', function(){
    hitung_sisa_pokok();
});

$('#form-angsuran [name=bayar_pokok], #form-angsuran [name=bayar_bunga]').on('keyup change', function(){
    hitung_total_bayar();
});

$('#form-hutang [name=tambah_hutang]').on('keyup change', function(){
    hitung_sisa_pokok_tambah();
});

function remark(e){
    $.ajax({
        url: "<?= base_url('manajemen/hutang/get_detail_hutang')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': $(e).data().id
        },
        success: function(result) {
            Swal.fire('Catatan', result.remark, 'info');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.tbd').show();
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.tbd').show();
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
        $('.tbd').hide();
    }
}

function hitung_sisa_pokok_tambah(){
    let hutang = getNum(parseInt($('#form-hutang [name=sisa_pokok_val]').val()));
    let tambah = getNum(parseInt($('#form-hutang [name=tambah_hutang]').autoNumeric('get')));
    let sisa = hutang + tambah;
    $('#form-hutang [name=sisa_pokok]').val(sisa);
    $('#form-hutang [name=sisa_pokok]').autoNumeric('destroy');
    $('#form-hutang [name=sisa_pokok]').autoNumeric('init', {
        'mDec': 0
    });
}

function hitung_sisa_pokok(){
    let hutang = getNum(parseInt($('#form-angsuran [name=sisa_pokok_val]').val()));
    let bayar_pokok = getNum(parseInt($('#form-angsuran [name=bayar_pokok]').autoNumeric('get')));
    let sisa = hutang - bayar_pokok;
    $('#form-angsuran [name=sisa_pokok]').val(sisa);
    $('#form-angsuran [name=sisa_pokok]').autoNumeric('destroy');
    $('#form-angsuran [name=sisa_pokok]').autoNumeric('init', {
        'mDec': 0
    });
}

function hitung_total_bayar(){
    let bayar_pokok = getNum(parseInt($('[name=bayar_pokok]').autoNumeric('get')));
    let bayar_bunga = getNum(parseInt($('[name=bayar_bunga]').autoNumeric('get')));
    let total_bayar = bayar_pokok + bayar_bunga;
    $('[name=total_bayar]').val(total_bayar);
    $('#form-angsuran [name=total_bayar]').autoNumeric('destroy');
    $('#form-angsuran [name=total_bayar]').autoNumeric('init', {
        'mDec': 0
    });
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
        }
    })
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('manajemen/hutang/get_detail_hutang')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {               

                if(result.tambah_hutang != 0){
                    // Tambah hutang
                    $.ajax({
                        url: "<?= base_url('manajemen/hutang/get_sisa_pokok')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': {{$hutang['id']}}
                        },
                        success: function(sisa_pokok) {
                            $('#form-hutang').trigger('reset');
                            $('#form-hutang [name=id]').val(result.id);
                            $('#form-hutang [name=id_hutang]').val(result.id_hutang);
                            $('#form-hutang [name=tgl_transaksi]').val(result.tgl_transaksi);
                            $('#form-hutang [name=jenis_pembayaran][value='+result.jenis_pembayaran+']').attr('checked', true);
                            if(result.jenis_pembayaran == 'cash'){
                                $('#form-hutang [name=id_kas]').val(result.id_kas).change();
                            }else if(result.jenis_pembayaran == 'transfer'){
                                $('#form-hutang [name=id_rekening]').val(result.id_kas).change();
                            }
                            $('#form-hutang [name=tambah_hutang]').val(result.tambah_hutang);
                            var sisa = parseInt(sisa_pokok) + parseInt(result.tambah_hutang);
                            $('#form-hutang [name=sisa_pokok]').val(sisa);
                            $('#form-hutang [name=sisa_pokok_val]').val(sisa_pokok);
                            $('#form-hutang [name=tambah_hutang], [name=sisa_pokok]').autoNumeric('destroy');
                            $('#form-hutang [name=tambah_hutang], [name=sisa_pokok]').autoNumeric('init', {
                                'mDec': 0
                            });
                            $('#form-hutang [name=keterangan]').val(result.remark);
                            cek_metode_pembayaran();
                            $('#modal_hutang').modal('show');
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                    
                }else{
                    // Angsuran
                    $.ajax({
                        url: "<?= base_url('manajemen/hutang/get_sisa_pokok')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': {{$hutang['id']}}
                        },
                        success: function(sisa_pokok) {
                            $('#form-angsuran').trigger('reset');
                            $('#form-angsuran [name=id]').val(result.id);
                            $('#form-angsuran [name=id_angsuran]').val(result.id_angsuran);
                            $('#form-angsuran [name=tgl_transaksi]').val(result.tgl_transaksi);
                            $('#form-angsuran [name=jenis_pembayaran][value='+result.jenis_pembayaran+']').attr('checked', true);
                            if(result.jenis_pembayaran == 'cash'){
                                $('#form-angsuran [name=id_kas]').val(result.id_kas).change();
                            }else if(result.jenis_pembayaran == 'transfer'){
                                $('#form-angsuran [name=id_rekening]').val(result.id_kas).change();
                            }
                            $('#form-angsuran [name=bayar_pokok]').val(result.bayar_pokok);
                            $('#form-angsuran [name=bayar_bunga]').val(result.bayar_bunga);
                            var sisa = parseInt(sisa_pokok) - parseInt(result.bayar_pokok);
                            var total_bayar = parseInt(result.bayar_pokok) + parseInt(result.bayar_bunga);
                            $('#form-angsuran [name=total_bayar]').val(total_bayar);
                            $('#form-angsuran [name=sisa_pokok]').val(sisa);
                            $('#form-angsuran [name=sisa_pokok_val]').val(sisa_pokok);
                            $('#form-angsuran [name=bayar_pokok], [name=bayar_bunga], [name=total_bayar], [name=sisa_pokok]').autoNumeric('destroy');
                            $('#form-angsuran [name=bayar_pokok], [name=bayar_bunga], [name=total_bayar], [name=sisa_pokok]').autoNumeric('init', {
                                'mDec': 0
                            });
                            $('#form-angsuran [name=keterangan]').val(result.remark);
                            cek_metode_pembayaran();
                            $('#modal_angsuran').modal('show');
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                }
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/hutang/delete_data_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}
</script>
@end