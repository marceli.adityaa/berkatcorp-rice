@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/sak/kelola?')?>" id="form-filter">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Klien</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="client" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$customer as $row){
                                        if(!empty($this->input->get('client')) && $this->input->get('client') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Arus</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="arus" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$arus as $row){
                                        if(!empty($this->input->get('arus')) && $this->input->get('arus') == $row){
                                            echo '<option value="'.$row.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button> 
                            <a href="{{base_url('manajemen/sak/kelola')}}"> <button type="button" class="btn btn-warning btn-submit"><i class="fa fa-refresh mg-r-10"></i>Reset Filter</button></a> 
                            <button type="button" class='btn btn-success' onclick="export_data(this)"><i class='fa fa-file-excel-o'></i> Export Data</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Sak</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Transaksi</button>
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode Trx</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Nama</th>
                    <th data-sortable="true">Arus</th>
                    <th data-sortable="true">Jenis Sak</th>
                    <th data-sortable="true">Jumlah</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            if($row['is_record'] == 1){
                                echo "<tr>";
                                echo "<td class='text-center'>".$no++."</td>";
                                echo "<td class='text-nowrap'>";
                                if($row['source'] == 'master-transaksi'){
                                    echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit transaksi' onclick='edit(this)' data-id='".$row['id_transaksi']."'><i class='fa fa-edit'></i></button> ";
                                    echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id_transaksi']."'><i class='fa fa-trash'></i></button> ";
                                }
                                echo "</td>";
                                echo "<td><label class='badge badge-light'>".$row['kode_transaksi']."</label></td>";
                                echo "<td><label class='badge badge-light'>".$row['source']."</label></td>";
                                echo "<td>".$row['tgl_transaksi']."</td>";
                                echo "<td>".$row['customer']."</td>";
                                if($row['arus'] == 'in'){
                                    echo "<td><label class='badge badge-primary'>in</label></td>";
                                }else{
                                    echo "<td><label class='badge badge-warning'>out</label></td>";
                                }
                                echo "<td>".$row['jenis_sak']."</td>";
                                echo "<td>".$row['jumlah']."</td>";
    
                                echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                                echo "</tr>";
                            }
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Transaksi Sak</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="#" id="form-transaksi">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_transaksi" value="">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_transaksi"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Dari <span class="tx-danger">*</span></label>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <select name="id_customer_from" class="form-control reset-form select2-modal">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="is_record_from">
                                            <label class="custom-control-label" for="is_record_from">Tidak Ubah Saldo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kepada<span class="tx-danger">*</span></label>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <select name="id_customer_to" class="form-control reset-form select2-modal">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="is_record_to">
                                            <label class="custom-control-label" for="is_record_to">Tidak Ubah Saldo</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Jenis Transaksi<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="arus" class="form-control">
                                            <option value="in">Barang Masuk</option>
                                            <option value="out">Barang Keluar</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class=" row mg-t-20">
                                    <button type="button" class="btn btn-primary" id="tambah_jenis">+ Tambah Jenis</button>
                                    <table class="table table-bordered table-hover mg-t-10" id="tabel_jenis_sak">
                                        <thead class="bg-info">
                                            <th>Jenis Sak</th>
                                            <th>Jumlah</th>
                                            <th>Aksi</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="button" class="btn btn-info" onclick="submit_trx()"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
    add_row_jenis_sak('', '');
});

$('#tambah_jenis').click(function() {
    add_row_jenis_sak('', '');
});

function add_row_jenis_sak(id_jenis_sak, jumlah){
    let jenis_sak = {{json_encode($jenis_sak)}};
    let opt_sak = '<option value="">- Pilih Jenis Sak -</option>';

    $(jenis_sak).each(function(a,b){
        if(id_jenis_sak == b.id){
            opt_sak += ' <option value="'+b.id+'" selected>'+b.nama+'</option>'
        }else{
            opt_sak += ' <option value="'+b.id+'">'+b.nama+'</option>'
        }
    });

    var element = ' <tr>\
                        <td>\
                            <select class="form-control select2-append" name="id_jenis_sak">'+opt_sak+'</select>    \
                        </td>    \
                        <td>\
                            <input type="number" class="form-control" name="jumlah" value="'+jumlah+'">\
                        </td>\
                        <td nowrap>\
                            <button type="button" class="btn btn-danger" onclick="hapus(this)"><i class="fa fa-trash"></i></button>\
                        </td>\
                    </tr>';
    $("#tabel_jenis_sak tbody").append(element);
}

function submit_trx(){
    let data = {};
    let jenis_sak = [];
    data.tgl_transaksi = $('#form-transaksi [name=tgl_transaksi]').val();
    data.id_customer_from = $('#form-transaksi [name=id_customer_from').val();
    data.id_customer_to = $('#form-transaksi [name=id_customer_to]').val();
    data.arus = $('#form-transaksi [name=arus] option:selected').val();
    if($("#is_record_from").is(":checked")){
        data.is_record_from = 0;
    }else{
        data.is_record_from = 1;
    }

    if($("#is_record_to").is(":checked")){
        data.is_record_to = 0;
    }else{
        data.is_record_to = 1;
    }

    $('#tabel_jenis_sak tr').each(function(a, b) {
        if (a > 0) {
            jenis_sak.push({
                id_jenis_sak: $(b).find("[name=id_jenis_sak] option:selected").val(),
                jumlah: $(b).find("[name=jumlah]").val(),
            });
            
        }
    });

    data.jenis_sak = jenis_sak;
    // console.log(data);

    $.ajax({
        url: "<?= base_url('manajemen/sak/submit_transaksi_sak')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id_transaksi' : $('#form-transaksi [name=id_transaksi]').val(),
            'data': data,
        },
        success: function(result) {
            reload_page();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
        }
    })
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('manajemen/sak/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $("#tabel_jenis_sak tbody").empty();
                $('#form-transaksi [name=id_transaksi]').val(result.trx.id);
                $('#form-transaksi [name=tgl_transaksi]').val(result.trx.tgl_transaksi);
                $('#form-transaksi [name=id_customer_from]').val(result.trx.id_customer_from).change();
                $('#form-transaksi [name=id_customer_to]').val(result.trx.id_customer_to).change();
                if(result.trx.is_record_from == 0){
                    $('#is_record_from').prop('checked', 'true');
                }
                if(result.trx.is_record_to == 0){
                    $('#is_record_to').prop('checked', 'true');
                }
                if(result.trx.arus != ''){
                    $('#form-transaksi [name=arus]').val(result.trx.arus).change();
                }
                $(result.detail).each(function(a, b){
                    add_row_jenis_sak(b.id_jenis_sak, b.jumlah);
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/sak/delete_data_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let client = $("#form-filter [name=client]").val();
    let arus = $("#form-filter [name=arus]").val();
    var url = "<?= base_url('manajemen/sak/export_transaksi?')?>"+'start='+start+'&end='+end+'&client='+client+'&arus='+arus;
    window.open(url, '_blank');
}
</script>
@end