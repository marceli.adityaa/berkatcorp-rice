@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/sak/kartu?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Akun</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="akun" class="form-control select2">
                                <?php
                                    foreach((array)$customer as $row){
                                        if(!empty($this->input->get('akun')) && $this->input->get('akun') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>                
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <a href="{{base_url('manajemen/sak/stok')}}"> <button type="button" class="btn btn-warning btn-submit"><i class="fa fa-refresh mg-r-10"></i>Reset Filter</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Stok Sak</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <a href="{{base_url('manajemen/sak/kelola')}}"><button class="btn btn-primary" type="button">+ Tambah Transaksi</button></a>
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Jenis Sak</th>
                    @if(!empty($_GET['start']))
                    <th data-sortable="true"class="text-center">Stok Awal</th>
                    @endif
                    <th data-sortable="true"class="text-center">Barang Masuk</th>
                    <th data-sortable="true"class="text-center">Barang Keluar</th>
                    <th data-sortable="true"class="text-center">Sisa Stok</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($stok)){
                        foreach($stok as $row){
                            
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['nama']."</td>";
                            if(!empty($_GET['start'])){
                            echo "<td>".monefy($row['stok_awal'], false)."</td>";
                            }
                            if($row['total_masuk'] > 0){
                                echo "<td><label class='tx-success'>+ ".monefy($row['total_masuk'], false)."</label></td>";
                            }else{
                                echo "<td>-</td>";
                            }
                            if($row['total_keluar'] > 0){
                                echo "<td><label class='tx-danger'>- ".monefy($row['total_keluar'], false)."</label></td>";
                            }else{
                                echo "<td>-</td>";
                            }
                            if(!empty($_GET['start'])){
                                echo "<td>".($row['stok_awal'] + $row['total_masuk'] - $row['total_keluar'])."</td>";
                            }else{
                                echo "<td>".($row['total_masuk'] - $row['total_keluar'])."</td>";
                            }
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Transaksi Sak</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="#" id="form-transaksi">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id_transaksi" value="">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_transaksi"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Dari <span class="tx-danger">*</span></label>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <select name="id_customer_from" class="form-control reset-form select2-modal">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="is_record_from">
                                            <label class="custom-control-label" for="is_record_from">Tidak Ubah Saldo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kepada<span class="tx-danger">*</span></label>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <select name="id_customer_to" class="form-control reset-form select2-modal">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="is_record_to">
                                            <label class="custom-control-label" for="is_record_to">Tidak Ubah Saldo</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Jenis Transaksi<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="arus" class="form-control">
                                            <option value="in">Barang Masuk</option>
                                            <option value="out">Barang Keluar</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class=" row mg-t-20">
                                    <button type="button" class="btn btn-primary" id="tambah_jenis">+ Tambah Jenis</button>
                                    <table class="table table-bordered table-hover mg-t-10" id="tabel_jenis_sak">
                                        <thead class="bg-info">
                                            <th>Jenis Sak</th>
                                            <th>Jumlah</th>
                                            <th>Aksi</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="button" class="btn btn-info" onclick="submit_trx()"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
    add_row_jenis_sak('', '');
});

$('#tambah_jenis').click(function() {
    add_row_jenis_sak('', '');
});

function add_row_jenis_sak(id_jenis_sak, jumlah){
    let jenis_sak = {{json_encode($jenis_sak)}};
    let opt_sak = '<option value="">- Pilih Jenis Sak -</option>';

    $(jenis_sak).each(function(a,b){
        if(id_jenis_sak == b.id){
            opt_sak += ' <option value="'+b.id+'" selected>'+b.nama+'</option>'
        }else{
            opt_sak += ' <option value="'+b.id+'">'+b.nama+'</option>'
        }
    });

    var element = ' <tr>\
                        <td>\
                            <select class="form-control select2-append" name="id_jenis_sak">'+opt_sak+'</select>    \
                        </td>    \
                        <td>\
                            <input type="number" class="form-control" name="jumlah" value="'+jumlah+'">\
                        </td>\
                        <td nowrap>\
                            <button type="button" class="btn btn-danger" onclick="hapus(this)"><i class="fa fa-trash"></i></button>\
                        </td>\
                    </tr>';
    $("#tabel_jenis_sak tbody").append(element);
}

function submit_trx(){
    let data = {};
    let jenis_sak = [];
    data.tgl_transaksi = $('#form-transaksi [name=tgl_transaksi]').val();
    data.id_customer_from = $('#form-transaksi [name=id_customer_from').val();
    data.id_customer_to = $('#form-transaksi [name=id_customer_to]').val();
    data.arus = $('#form-transaksi [name=arus] option:selected').val();
    if($("#is_record_from").is(":checked")){
        data.is_record_from = 0;
    }else{
        data.is_record_from = 1;
    }

    if($("#is_record_to").is(":checked")){
        data.is_record_to = 0;
    }else{
        data.is_record_to = 1;
    }

    $('#tabel_jenis_sak tr').each(function(a, b) {
        if (a > 0) {
            jenis_sak.push({
                id_jenis_sak: $(b).find("[name=id_jenis_sak] option:selected").val(),
                jumlah: $(b).find("[name=jumlah]").val(),
            });
            
        }
    });

    data.jenis_sak = jenis_sak;
    $.ajax({
        url: "<?= base_url('manajemen/sak/submit_transaksi_sak')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id_transaksi' : $('#form-transaksi [name=id_transaksi]').val(),
            'data': data,
        },
        success: function(result) {
            reload_page();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
        }
    })
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('manajemen/sak/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $("#tabel_jenis_sak tbody").empty();
                $('#form-transaksi [name=id_transaksi]').val(result.trx.id);
                $('#form-transaksi [name=tgl_transaksi]').val(result.trx.tgl_transaksi);
                $('#form-transaksi [name=id_customer_from]').val(result.trx.id_customer_from).change();
                $('#form-transaksi [name=id_customer_to]').val(result.trx.id_customer_to).change();
                if(result.trx.is_record_from == 0){
                    $('#is_record_from').prop('checked', 'true');
                }
                if(result.trx.is_record_to == 0){
                    $('#is_record_to').prop('checked', 'true');
                }
                if(result.trx.arus != ''){
                    $('#form-transaksi [name=arus]').val(result.trx.arus).change();
                }
                $(result.detail).each(function(a, b){
                    add_row_jenis_sak(b.id_jenis_sak, b.jumlah);
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/sak/delete_data_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end