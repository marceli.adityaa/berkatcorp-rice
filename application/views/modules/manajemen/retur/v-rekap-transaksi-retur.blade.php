@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/retur/rekap?')?>" id="form-filter">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            @if(!empty($this->input->get()))
                            <a href="{{base_url('manajemen/retur/rekap')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-10"></i>Reset</button></a>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi Retur Penjualan </h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_beban">
            <thead>
                <tr>
                <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kode Penjualan</th>
                    <th data-sortable="true">Nama Customer</th>
                    <th data-sortable="true">Item yang Diretur</th>
                    <th data-sortable="true">Alasan Retur</th>
                    <th data-sortable="true">Total Tonase</th>
                    <th data-sortable="true">Jumlah Bayar</th>
                    <th data-sortable="true">Sumber Kas</th>
                    <th data-sortable="true">Input by</th>
                    <th data-sortable="true">Verif by</th>

                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total = 0;
                    $totalTonase = 0;
                    if(!empty($retur)){
                        foreach($retur as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "</td>";
                            if($row['persetujuan'] == 1){
                                echo "<td class='text-center'><label class='badge badge-success'>Selesai</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td class='text-center'><label class='badge badge-danger'>Gagal</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-secondary'>Menunggu Verifikasi</label></td>";
                            }
                            echo "<td>".$row['tanggal']."</td>";
                            // echo "<td><label class='badge badge-info'>".ucwords($row['kategori'])."</label></td>";
                            // echo "<td><label class='badge badge-light'>".ucwords($row['biaya'])."</label></td>";
                            echo "<td>".$row['kode_penjualan']."</td>";
                            echo "<td>".$row['customer']."</td>";
                            echo "<td>".$row['item_retur']."</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td>".monefy($row['total_tonase'], false)."</td>";
                            echo "<td>".monefy($row['jumlah_bayar'], false)."</td>";
                            if($row['id_kas'] != 0){
                                if($row['kas_is_rekening'] == 1){
                                    echo "<td><label class='badge badge-info'>".$row['kas_bank']." | ".$row['kas_no_rek']."</label></td>";
                                }else{
                                    echo "<td><label class='badge badge-primary'>".$row['kas_kas']."</label></td>";
                                }
                            }else{
                                echo "<td>-</td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            if($row['persetujuan'] != 1){
                                echo "<td>-</td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['acc_by']."</label><br><label class='badge badge-light'>".$row['timestamp_verifikasi']."</label></td>";
                            }
                            echo "</tr>";
                            $total+=$row['jumlah_bayar'];
                            $totalTonase+=$row['total_tonase'];
                        }
                    }
				?>
                <tr class="tx-bold tx-16">
                    <td></td>
                    <td class = "text-center" colspan="7">Total</td>
                    <td><?= monefy($totalTonase,false)?></td>
                    <td>Rp. <?= monefy($total,false)?></td>
                    <td colspan="3"></td>
                </tr>
            </tbody>
        </table>
        
        @else
        <p class="text-center">Klik filter untuk menampilkan data transaksi biaya / beban</p>
        @endif
    </div>
</div>
@end


@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_beban').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_metode_pembayaran();
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});


$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.tbd').show();
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.tbd').show();
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
        $('.tbd').hide();
    }
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
        }
    })
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('manajemen/beban/json_get_data_beban')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tgl_transaksi]').val(result.tgl_transaksi);
                $('#form-transaksi [name=id_beban]').val(result.id_beban).change();
                $('#form-transaksi [name=jenis_beban]').val(result.jenis_beban).change();
                $('#form-transaksi [name=jenis_pembayaran][value='+result.jenis_pembayaran+']').attr('checked', 'true');
                $('#form-transaksi [name=nominal]').val(result.nominal);
                if(result.jenis_pembayaran == 'cash'){
                    $('#form-transaksi [name=id_kas]').val(result.id_kas).change();
                }else if(result.jenis_pembayaran == 'transfer'){
                    $('#form-transaksi [name=id_rekening]').val(result.id_kas).change();
                }
                $('#form-transaksi [name=catatan]').val(result.catatan);
                cek_metode_pembayaran();
                $('.autonumeric').autoNumeric('destroy');
                $('.autonumeric').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/beban/delete_data_beban')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}


function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    // if (start != '' && end != '') {
    var url = "<?= base_url('manajemen/retur/export?')?>"+'start='+start+'&end='+end;
    window.open(url, '_blank');
    // } else {
    //     Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}
</script>
@end