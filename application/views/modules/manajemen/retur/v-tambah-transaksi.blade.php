@layout('commons/index')

@section('content')

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pilih Transaksi Penjualan Beras untuk Retur</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
        <h7 class="mg-b-0" style="color: red; font-weight: bold">Cari kode transaksi disini :</h7>
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="true">Kode Penjualan</th>
                    <th data-sortable="true">Kode Surat Jalan</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Customer</th>
                    <th data-sortable="true">Total Tonase</th>
                    <th data-sortable="true">Grandtotal</th>
                    <!-- <th data-sortable="true">Umur (Hari)</th> -->
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $now = date('Y-m-d');
                    $total = 0;
                    $sisa = 0;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                                echo "<button type='button' class='btn btn-info' onclick='ajukan(this)' data-id='".$row['id']."'>Ajukan Retur</button> ";
                            echo "</td>";
                            
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['kode_surat_jalan']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['tanggal']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['customer']."</label></td>";
                            echo "<td class='text-center'>".monefy($row['tonase'], false)."</td>";
                            echo "<td class='text-center'>Rp. ".monefy($row['grandtotal'], false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Transaksi Retur</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('manajemen/retur/submit_retur')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_penjualan" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Kode Penjualan</label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" name="kode" required="" autocomplete="off" disabled> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Customer </label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" name="customer" required="" autocomplete="off" disabled> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_transaksi" required=""> 
                                    </div>
                                </div>
                                

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Metode Pembayaran</label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">
                                            @if($row == 'tbd')
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                            @else
                                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="row mg-t-10 tbd">
                                    <label class="col-4 form-control-label">Sumber Kas<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="id_kas" class="form-control cash">
                                            @foreach ($kas as $row)
                                                <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                            @endforeach
                                        </select>

                                        <select name="id_rekening" class="form-control transfer">
                                            @foreach ($rekening as $row)
                                                <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Item yang diretur <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="item" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Alasan Retur <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="keterangan" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Total Tonase <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="number" class="form-control" name="total_tonase" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jumlah Bayar <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="jumlah_bayar" required="" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        searchAlign: 'left',
        toolbar: '#toolbar'
    });
    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    $('.select2').select2();
    
});

$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.tbd').show();
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.tbd').show();
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
        $('.tbd').hide();
    }
}

function ajukan(el){
    $.ajax({
        url: "<?= base_url('api/internal/penjualan/beras/faktur/json_get_detail_trx')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            console.log(result);
            $('#form-transaksi [name=kode]').val(result.kode);
            $('[name=id_penjualan]').val(result.id);
            $('#form-transaksi [name=customer]').val(result.customer).change();
            $("#modal_form").modal('show');
            $('.autonumeric').autoNumeric('destroy');
            $('.autonumeric').autoNumeric('init', {
                'mDec': 0
            });
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
}

</script>
@end