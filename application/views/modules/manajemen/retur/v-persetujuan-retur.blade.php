@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/retur/persetujuan?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($this->input->get('status')) && $this->input->get('status') == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{                                            
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pengajuan Transaksi Retur Penjualan </h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_beban">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kode Penjualan</th>
                    <th data-sortable="true">Nama Customer</th>
                    <th data-sortable="true">Item yang Diretur</th>
                    <th data-sortable="true">Alasan Retur</th>
                    <th data-sortable="true">Total Tonase</th>
                    <th data-sortable="true">Jumlah Bayar</th>
                    <th data-sortable="true">Sumber Kas</th>
                    <th data-sortable="true">Input by</th>
                    <th data-sortable="true">Acc by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($retur)){
                        foreach($retur as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['persetujuan'] == 0){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }else if($row['persetujuan'] == 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "</td>";
                            if($row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Menunggu Verifikasi</label></td>";
                            }
                            echo "<td>".$row['tanggal']."</td>";
                            // echo "<td><label class='badge badge-info'>".ucwords($row['kategori'])."</label></td>";
                            // echo "<td><label class='badge badge-light'>".ucwords($row['biaya'])."</label></td>";
                            echo "<td>".$row['kode_penjualan']."</td>";
                            echo "<td>".$row['customer']."</td>";
                            echo "<td>".$row['item_retur']."</td>";
                            echo "<td>".$row['keterangan']."</td>";
                            echo "<td>".monefy($row['total_tonase'], false)."</td>";
                            echo "<td>".monefy($row['jumlah_bayar'], false)."</td>";
                            if($row['id_kas'] != 0){
                                if($row['kas_is_rekening'] == 1){
                                    echo "<td><label class='badge badge-info'>".$row['kas_bank']." | ".$row['kas_no_rek']."</label></td>";
                                }else{
                                    echo "<td><label class='badge badge-primary'>".$row['kas_kas']."</label></td>";
                                }
                            }else{
                                echo "<td>-</td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            if($row['persetujuan'] != 1){
                                echo "<td>-</td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['acc_by']."</label><br><label class='badge badge-light'>".$row['timestamp_verifikasi']."</label></td>";
                            }
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end



@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_beban').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_metode_pembayaran();
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

// $('#tambah').click(function() {
//     $("#tabel_jenis_sak tbody").empty();
//     $('#form-transaksi').trigger('reset');
//     $("#modal_form").modal('show');
// });

$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.tbd').show();
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.tbd').show();
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
        $('.tbd').hide();
    }
}
function setuju(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan disetujui, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/retur/approve_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('manajemen/retur/decline_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end