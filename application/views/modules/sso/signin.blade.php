<!DOCTYPE html>
<html>
<head>
	<title>Halaman Masuk | </title>
	<!-- Bootstrap -->
	<link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
	<!-- Fontawesome -->
	<link href="<?= base_url('assets/plugins/fontawesome/css/fontawesome.min.css')?>" rel="stylesheet">
	<!-- css -->
	<link href="<?= base_url('assets/styles/login.css')?>" rel="stylesheet">
</head>
<body>
	<div class="ajaxloader" style="display:none;">
		<div class="loader">
			<img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
		</div>
	</div>
	<div class="wrapper">
		<div class="row">
			<div class="col-12 col-lg-5" id="left-side">
				<span class="title">
					<h4>SSO</h4>
					<h1>BERKAT<span class="grey">CORP</span></h1>
				</span>
				<form action="{{site_url('sso/server/auth')}}">
					<input type="hidden" name="source" value="{{isset($source)?urlencode($source):''}}">
					<div class="form-group">
						<label>Username :</label>
						<input type="text" class="form-control form-control-lg" name="username" placeholder="Masukkan username" required>
						<p class="form-text"></p>
					</div>
					<div class="form-group">
						<label>Password :</label>
						<input type="password" class="form-control form-control-lg" name="password" placeholder="Masukkan kata sandi" required>
						<p class="form-text"></p>
					</div>
					<div class="form-group mt-4">
						<button type="button" id="btn-submit" class="btn btn-secondary btn-lg btn-block">Masuk</button>
					</div>
				</form>
			</div>
			<div class="col-12 col-lg-7" id="right-side">
				<img src="{{base_url('assets/img/logo/berkat-group-white.png')}}" alt="Berkat Group">
				<h3 class="mb-3">Tentang Berkat Group</h3>
				<p>Berkat Group adalah Perusahaan yang berdiri sejak tahun1988, Perusahaan
					yang bergerak di bidang pangan seperti beras dan memiliki anak Perusahaan
					yang bergerak di bidang ternak ayam, sapi, domba, CV Berkat Farm yang sudah
					berdiri sejak tahun 2007, dan tambak udang, ikan, CV Berkat Marine yang akan
					sudah proses dalam tahun 2018 ini.
					Dasar visi Perusahaan Berkat Group adalah menjadi Perusahaan yang
					berkembang besar dan dapat menjadi berkat bagi orang lain melalui kualitas
				yang baik dari setiap produk yang disajikan.</p>
				<p class="text-small">______<br><br>copyright &copy {{date('Y')}}. Berkat Group
					<br>All Rights Reserved.</p>
				</div>
			</div>
		</div>

		<!-- jquery -->
		<script src="<?= base_url('assets/plugins/jquery/jquery.min.js')?>"></script>
		<!-- Bootstrap -->
		<script src="<?= base_url('assets/plugins/bootstrap/js/popper.js')?>"></script>
		<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>

<script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js')?>"></script>
		<!-- js -->
		<script src="<?= base_url('assets/scripts/app.js')?>"></script>
		<script src="<?= base_url('assets/scripts/login.js')?>"></script>
	</body>
	</html>