<!DOCTYPE html>
<html lang="en">

<head>
    @include('commons/head')
    
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>
    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
        </div>
    </div>

	<div class="am-header bg-midnightblack">
		<div class="am-header-left">
			<a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a> 
			<a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a> 
			<a href="#" class="am-logo"><img src="<?=base_url('assets/img/logo/berkat-group-horizontal.png')?>" class="img-fluid wd-40p"></a>
		</div>
		<div class="am-header-right">
			<div style="font-size: 150%">HELP CENTER</div>
		</div>
		
	</div>

	

<!-- @include('commons/sidebar') -->

    <div  style="padding-left: 0%" class="am-mainpanel">
        <div class="am-pagebody">
			<div class="row">
            <div class="column">
				<div class="pd-10 bd rounded">
				<ul class="nav nav-gray-600 flex-column" role="tablist">
				<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#" role="tab">Home</a></li>
				<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#" role="tab">About</a></li>
				<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#" role="tab">Features</a></li>
				</ul>
			</div>
			hello world
            <!-- @yield('content') -->
			</div>
			

			</div>
        </div>
    </div>
    <!-- @yield('modal') -->
    @include('commons/js')
</body>

</html>