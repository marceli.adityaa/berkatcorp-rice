@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Informasi
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('manajemen/piutang/kelola?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input class="form-control" value="{{$pembelian['customer']}}" readonly>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input class="form-control" value="{{ucwords($pembelian['tgl_tempo'])}}" readonly>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Nominal tagihan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><small>Rp</small></span>
                                </div>
                                <input type="text" class="form-control" value="{{monefy($pembelian['grandtotal'], false)}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"><b>Sisa Bayar</b></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text tx-bold" id="basic-addon1"><small>Rp</small></span>
                                </div>
                                <input type="text" class="form-control tx-bold" value="{{monefy($sisa_tempo, false)}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if($pembelian['persetujuan'] == 1 && $pembelian['is_lunas'] == 1)
                                <label class='badge badge-success'>Lunas</label>
                            @else
                                <label class='badge badge-warning'>Belum Lunas</label>
                            @endif
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Detail Pembayaran</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($pembelian['is_lunas'] == 0)
            <button class="btn btn-primary" id="tambah_data">+ Tambah Data</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Tgl Bayar</th>
                    <th data-sortable="true">Jenis Pembayaran</th>
                    <th data-sortable="true">Sumber Kas</th>
                    <th data-sortable="true">Jumlah Bayar</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Diajukan</th>
                    <th data-sortable="true">Disetujui</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($detail)){
                        foreach($detail as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['status_persetujuan'] != 1){
                                if($row['status_persetujuan'] != 2){
                                    echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit transaksi' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                                }
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['status_persetujuan'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu Verifikasi</label></td>";
                            }else if($row['status_persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            
                            echo "<td>".$row['tgl_pembayaran']."</td>";
                            if($row['is_paid_off'] == 1){
                                echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-success'>Bayar Lunas</label></td>";
                            }else{
                                echo "<td>".ucwords($row['jenis_pembayaran'])."</td>";
                            }
                            if($row['kas_is_rekening'] == 0){
                                echo "<td><label class='badge badge-primary'>".ucwords($row['kas_kas'])."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-info'>".ucwords($row['kas_bank'])." | ".$row['kas_no_rek']."</label></td>";
                            }
                            
                            echo "<td>".monefy($row['jumlah_bayar'], false)."</td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            if($row['status_persetujuan'] == 1){
                                echo "<td><label class='badge badge-light'>".$row['acc']."</label><br><label class='badge badge-light'>".$row['timestamp_acc']."</label></td>";
                            }else{
                                echo "<td>-</td>";
                            }
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
       
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_pembayaran" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Pembayaran</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-bayar" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('pembelian/lain/pembayaran_tempo/submit_pembayaran')}}" id="form-pembayaran" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_pembelian" value="{{$pembelian['id']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Bayar<span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tgl_pembayaran" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Metode Pembayaran <span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input" required>
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        </div>
                                        
                                        @endforeach
                                    </div>
                                </div>

                                <div class="row mg-t-10 tbd">
                                    <label class="col-4 form-control-label">Sumber Kas<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <select name="kas" class="form-control cash select2-modal" id="kas">
                                            <option value="">- Pilih Sumber Kas -</option>
                                            @foreach ($kas as $row)
                                                <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                            @endforeach
                                        </select>

                                        <select name="rekening" class="form-control transfer select2-modal" id="rekening">
                                        <option value="">- Pilih Rekening -</option>
                                            @foreach ($rekening as $row)
                                                <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jumlah Bayar <span class="tx-danger">*</span></label>
                                    <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="jumlah_bayar" autocomplete="off" required> 
                                        </div>
                                    </div>
                                    <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="is_paid_off" name="is_paid_off" value="1">
                                            <label class="custom-control-label" for="is_paid_off">Tandai Lunas</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sisa Bayar <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-1">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="hidden" name="sisa_bayar_val" value="">
                                            <input type="text" class="form-control" name="sisa_bayar" readonly=""> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" name="catatan" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();

    // $('.select2-modal').select2({
    //     dropdownParent: $('#modal_pembayaran')
    // })

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_metode_pembayaran();
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah_data').click(function() {
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/lain/pembayaran/get_sisa_tempo')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': {{$pembelian['id']}}
        },
        success: function(result) {
            $('#form-pembayaran [name=sisa_bayar]').val(result);
            $('#form-pembayaran [name=sisa_bayar_val]').val(result);
            $('#form-pembayaran [name=sisa_bayar]').autoNumeric('init', {
                'mDec': 0
            });
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    $('#form-pembayaran').trigger('reset');
    $("#modal_pembayaran").modal('show');
});

$('[name=jenis_pembayaran]').change(function(){
    cek_metode_pembayaran();
});

// $('#form-pembayaran [name=jumlah_bayar]').on('keyup change', function(){
//     hitung_sisa_bayar();
// });


$('#is_paid_off').change(function(){
    set_lunas();
});

function set_lunas(){
    if($('#is_paid_off').is(':checked')){
        let sisa_bayar = parseInt($('[name=sisa_bayar_val]').val());
        $('[name=jumlah_bayar]').val(sisa_bayar);
        $('[name=jumlah_bayar]').autoNumeric('destroy');
        $('[name=jumlah_bayar]').autoNumeric('init', {
            'mDec': 0
        });
        $('[name=jumlah_bayar]').prop('readonly', true);
    }else{
        $('[name=jumlah_bayar]').prop('readonly', false);
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.tbd').show();
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.tbd').show();
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
        $('.tbd').hide();
    }
}

function hitung_sisa_bayar(){
    let piutang = getNum(parseInt($('#form-pembayaran [name=sisa_bayar_val]').val()));
    let bayar = getNum(parseInt($('#form-pembayaran [name=jumlah_bayar]').autoNumeric('get')));
    let sisa = piutang - bayar;
    $('#form-pembayaran [name=sisa_bayar]').val(sisa);
    $('#form-pembayaran [name=sisa_bayar]').autoNumeric('destroy');
    $('#form-pembayaran [name=sisa_bayar]').autoNumeric('init', {
        'mDec': 0
    });
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/lain/pembayaran/get_detail_trx_pembayaran')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {               
                $.ajax({
                    url: "<?= base_url('api/internal/pembelian/lain/pembayaran/get_sisa_tempo')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': {{$pembelian['id']}}
                    },
                    success: function(sisa) {
                        $('[name=id]').val(result.id);
                        $('[name=tgl_pembayaran]').val(result.tgl_pembayaran);
                        $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
                        if(result.jenis_pembayaran == 'transfer'){
                            $('#rekening').val(result.id_kas).change();
                        }else{
                            $('#kas').val(result.id_kas).change();
                        }
                        $('[name=jumlah_bayar]').val(result.jumlah_bayar);
                        if(result.is_paid_off == 1){
                            $('#is_paid_off').prop('checked', true);
                        }
                        $('[name=sisa_bayar]').val(sisa);
                        $('[name=sisa_bayar_val]').val(sisa);
                        $('[name=catatan]').val(result.catatan);
                        $("#modal_pembayaran").modal('show');
                        cek_metode_pembayaran();
                        $('[name=jumlah_bayar], [name=sisa_bayar]').autoNumeric('destroy');
                        $('[name=jumlah_bayar], [name=sisa_bayar]').autoNumeric('init', {
                            'mDec': 0
                        });
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
                
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/lain/pembayaran/delete_data_pembayaran')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}
</script>
@end