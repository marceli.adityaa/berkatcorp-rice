@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/lain/pembayaran_tempo/persetujuan?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$customer as $row){
                                        if(!empty($this->input->get('customer')) && $this->input->get('customer') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pelunasan Pembelian Bahan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Kode Pembelian</th>
                    <th data-sortable="true">Tgl Bayar</th>
                    <th data-sortable="true">Jenis Pembayaran</th>
                    <th data-sortable="true">Sumber Kas</th>
                    <th data-sortable="true">Jumlah Bayar</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Diajukan</th>
                    <th data-sortable="true">Disetujui</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                $no = 1;
                if(!empty($pembayaran)){
                    foreach($pembayaran as $row){
                        echo "<tr>";
                        echo "<td class='text-center'>".$no++."</td>";
                        echo "<td class='text-nowrap'>";
                        if($row['status_persetujuan'] != 1){
                            echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            if($row['status_persetujuan'] != 2){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }
                        }
                        echo "</td>";
                        if($row['status_persetujuan'] == 0){
                            echo "<td><label class='badge badge-secondary'>Menunggu Verifikasi</label></td>";
                        }else if($row['status_persetujuan'] == 1){
                            echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                        }else{
                            echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                        }
                        echo "<td>".$row['customer']."</td>";
                        echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                        echo "<td>".$row['tgl_pembayaran']."</td>";
                        if($row['is_paid_off'] == 1){
                            echo "<td>".ucwords($row['jenis_pembayaran'])."<br><label class='badge badge-success'>Bayar Lunas</label></td>";
                        }else{
                            echo "<td>".ucwords($row['jenis_pembayaran'])."</td>";
                        }
                        if($row['kas_is_rekening'] == 0){
                            echo "<td><label class='badge badge-primary'>".ucwords($row['kas_kas'])."</label></td>";
                        }else{
                            echo "<td><label class='badge badge-info'>".ucwords($row['kas_bank'])." | ".$row['kas_no_rek']."</label></td>";
                        }
                        
                        echo "<td>".monefy($row['jumlah_bayar'], false)."</td>";
                        echo "<td>".$row['catatan']."</td>";
                        echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                        if($row['status_persetujuan'] == 1){
                            echo "<td><label class='badge badge-light'>".$row['acc']."</label><br><label class='badge badge-light'>".$row['timestamp_acc']."</label></td>";
                        }else{
                            echo "<td>-</td>";
                        }
                        echo "</tr>";
                    }
                }
            ?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian bahan</p>
        @endif
    </div>
</div>
@end

@section('modal')
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        // dropdownParent: $('#modal_form')
    });

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function setuju(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan disetujui, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/lain/pembayaran/setuju')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/lain/pembayaran/tolak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end