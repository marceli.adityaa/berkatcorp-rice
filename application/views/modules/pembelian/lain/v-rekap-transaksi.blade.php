@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/lain/rekap?')?>" id="form-filter">
                <?php $get = $this->input->get();?>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="customer" class="form-control select2">
                                    <option value="all">All</option>
                                        @foreach ($area as $ar)
                                            <optgroup label="{{ucwords($ar['nama'])}}">
                                            @foreach ($customer as $c)
                                                @if($ar['id'] == $c['id_area'])
                                                    @if (isset($get['customer']) && $get['customer'] == $c['id'])
                                                        <option value="{{$c['id']}}" selected>{{ucwords($c['nama'])}}</option>
                                                    @else
                                                        <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                            </optgroup>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Area</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="area" class="form-control select2">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$area as $row){
                                            if(!empty($get['area']) && $get['area'] == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Jenis Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="tempo" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$tempo as $key => $val){
                                            if(!empty($get['tempo']) && $get['tempo'] == $key){
                                                echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                            }else{
                                                echo '<option value="'.$key.'">'.$val.'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Status</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="status" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$status as $key => $val){
                                            if(isset($get['status']) && $get['status'] == $key){
                                                echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                            }else{
                                                echo '<option value="'.$key.'">'.$val.'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-8"></i>Filter</button>
                                @if(!empty($this->input->get()))
                                <a href="{{base_url('pembelian/lain/rekap')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-8"></i>Reset</button></a>
                                <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-8"></i>Export to Excel</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Rekap Transaksi Pembelian Lain</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-sortable="true" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Area</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Kode Transaksi</th>
                    <th data-sortable="true">Tempo</th>
                    <th data-sortable="true">Jumlah Tagihan</th>
                    <th data-sortable="true">Sisa Tagihan</th>
                    <th data-sortable="true">Umur (Hari)</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $now = date('Y-m-d');
                    $total = 0;
                    $sisa = 0;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['is_tempo'] == 0){
                                if($row['is_lunas'] == 1){
                                    echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Pembayaran' onclick='pembayaran(this)' data-id='".$row['id']."'><i class='fa fa-search'>&nbsp;</i></button> ";
                                }
                                    echo "<a href='".base_url('pembelian/lain/kelola/cetak_faktur/'.$row['id'])."' target='_blank'><button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Cetak Nota'><i class='fa fa-print'></i></button></a> ";
                            }else{
                                echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Pembayaran' onclick='pembayaran_tempo(this)' data-id='".$row['id']."'><i class='fa fa-search'>&nbsp;</i></button> ";
                                echo "<a href='".base_url('pembelian/lain/kelola/cetak_faktur/'.$row['id'])."' target='_blank'><button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Cetak Nota'><i class='fa fa-print'></i></button></a> ";
                            }
                            echo "</td>";
                            if($row['is_lunas'] == 0){
                                echo "<td><label class='badge badge-secondary'>Belum Lunas</label></td>";
                            }else if ($row['is_lunas'] == 1){
                                echo "<td><label class='badge badge-success'>Lunas</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['area']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['tanggal']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['customer']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            if($row['is_tempo']){
                                echo "<td class='text-center'><label class='badge badge-warning'>Ya</label></td>";
                            }else{
                                echo "<td class='text-center'><label class='badge badge-light'>-</label></td>";
                            }
                            echo "<td class='text-center'>".monefy($row['grandtotal'], false)."</td>";
                            
                            if($row['is_lunas'] == 1){
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($row['tgl_bayar']));
                            }else{
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($now));
                            }
                            if($row['is_tempo'] == 1){
                                if($row['tgl_tempo'] <= $now = date('Y-m-d') ){
                                    echo "<td class='text-center'><label class='tx-danger'>".monefy(($row['grandtotal']-$row['jumlah_bayar']), false)."</label></td>";
                                    echo "<td class='text-center'><label class='tx-danger'>".$interval->days."</label></td>";
                                }else{
                                    echo "<td class='text-center'>".monefy(($row['grandtotal']-$row['jumlah_bayar']), false)."</td>";
                                    echo "<td class='text-center'><label class='tx-primary'>- ".$interval->days."</label></td>";
                                }
                            }else{
                                echo "<td class='text-center'>".monefy(($row['grandtotal']-$row['jumlah_bayar']), false)."</td>";
                                echo "<td class='text-center'><label class='tx-primary'>-</label></td>";
                            }
                            
                            echo "</tr>";
                            $total += $row['grandtotal'];
                            $sisa += ($row['grandtotal']-$row['jumlah_bayar']);
                        }
                    }
				?>
                <tr class="tx-bold tx-16">
                    <td colspan="8">Total</td>
                    <td class="text-center"><?= monefy($total,false)?></td>
                    <td class="text-center"><?= monefy($sisa,false)?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
            <div class="modal-content">
                <form id="form-pembelian">
                    <div class="modal-header bg-midnightblack">
                        <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Pembayaran | <label id="kode" class="tx-white"></label></h6>
                        <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                        <div class="form-layout form-layout-4">
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" name="tanggal" class="form-control" value="" readonly="">
                                </div>
                            </div>
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Supplier</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" name="customer" class="form-control" readonly="">
                                </div>
                            </div>
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input" disabled="">
                                        <label class="custom-control-label" for="tempo-1">Tempo</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input" disabled="">
                                        <label class="custom-control-label" for="tempo-2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-12 my-3 tempo">
                                <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" name="tgl_tempo" class="form-control tempo" value="" disabled>
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo">
                                <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @foreach ($pembayaran as $key => $row)
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input" disabled>

                                        @if($row == 'tbd')
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                        @else
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo cash">
                                <label class="col-sm-4 form-control-label">Sumber Kas</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_kas" class="form-control select2" disabled>
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($kas as $row)
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-12 my-3 not-tempo transfer">
                                <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_rek_penerima" class="form-control select2" disabled>
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($rek_penerima as $row)
                                            <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-12 my-3 not-tempo">
                                <label class="col-sm-4 form-control-label">Tanggal Bayar</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" name="tgl_bayar" class="form-control" value="" disabled>
                                </div>
                            </div>

                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                        </div>
                                        <input type="hidden" name="grandtotal" value="">
                                        <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-12 my-3 not-tempo">
                                <label class="col-sm-4 form-control-label">Jumlah Bayar</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" name="jumlah_bayar" class="form-control not-tempo autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah bayar" disabled>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_pembayaran_tempo" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Pembayaran Tempo</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-home-tab">
                        <table class="table" id="tabel-detail">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th data-sortable="true">Tanggal Bayar</th>
                                    <th data-sortable="true">Jenis Pembayaran</th>
                                    <th data-sortable="true">Sumber</th>
                                    <th data-sortable="true">Jumlah Bayar</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    $('.select2').select2();
    
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function pembayaran(el){
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/lain/kelola/json_get_detail_trx')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            console.log(result);
            $('#kode').empty().html(result.kode);
            $('[name=tanggal]').val(result.tanggal);
            $('[name=id]').val(result.id);
            $('#form-pembelian [name=customer]').val(result.customer);
            if(result.is_tempo == 1){
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val(result.tgl_tempo);
            }else{
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val('');
                if(result.jenis_pembayaran == 'cash'){
                    if(result.id_kas == 0){
                        $('[name=id_kas]').val('').change();
                    }else{
                        $('[name=id_kas]').val(result.id_kas).change();
                    }
                    $('[name=id_rek_penerima]').val('').change();
                }else if(result.jenis_pembayaran == 'transfer'){
                    $('[name=id_kas]').val('').change();
                    if(result.id_kas == 0){
                        $('[name=id_rek_penerima]').val('').change();
                    }else{
                        $('[name=id_rek_penerima]').val(result.id_kas).change();
                    }
                }
            }
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);

            if(result.tgl_bayar != "0000-00-00"){
                $('[name=tgl_bayar]').val(result.tgl_bayar);
            }
            $('[name=grandtotal]').val(result.grandtotal);
            $('[name=jumlah_bayar]').val(result.jumlah_bayar);
            $('#grandtotal').html(parseInt(result.grandtotal).toLocaleString());
            $("#modal_form").modal('show');
            $('.autonumeric').autoNumeric('destroy');
            $('.autonumeric').autoNumeric('init', {
                'mDec': 0
            });
            cek_tempo();
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
}

function pembayaran_tempo(el){
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/lain/pembayaran/json_get_detail_pembayaran')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': $(el).data().id,
        },
        success: function(result) {
            $('#tabel-detail').bootstrapTable('destroy');
            $('#tabel-detail').find('tbody').empty();
            let no = 1;
            $(result).each(function(a,b){
                let label_lunas = "";    
                let kas = "";
                if(b.is_paid_off == 1){
                    label_lunas = '<br><label class="badge badge-success">Bayar Lunas</label>';
                }
                if(b.kas_is_rekening){
                    kas = b.kas_bank+'<br>'+b.kas_no_rek;
                }else{
                    kas = b.kas_kas;
                }

                $('#tabel-detail').find('tbody').append('\
                    <tr>\
                    <td>'+no+'</td>\
                    <td>'+b.tgl_pembayaran+'</td>\
                    <td>'+b.jenis_pembayaran+label_lunas+'</td>\
                    <td>'+kas+'</td>\
                    <td>'+parseInt(b.jumlah_bayar).toLocaleString()+'</td>\
                    </tr>\
                ');
                no++;
            });
            $('#tabel-detail').bootstrapTable({
                pagination: true,
                showToggle: true,
                showColumns: true,
                search:true,
                pageSize:10,
                striped: true,
                showFilter: true,
                toolbar: '#toolbar'
            });
            
            $('#modal_pembayaran_tempo').modal('show');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let customer = $("#form-filter [name=customer]").val();
    let area = $("#form-filter [name=area]").val();
    let status = $("#form-filter [name=status]").val();
    let tempo = $("#form-filter [name=tempo]").val();
    // if (start != '' && end != '') {
        var url = "<?= base_url('pembelian/lain/rekap/export?')?>"+'start='+start+'&end='+end+'&customer='+customer+'&area='+area+'&status='+status+'&tempo='+tempo;
        window.open(url, '_blank');
    // } else {
    //     Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}

</script>
@end