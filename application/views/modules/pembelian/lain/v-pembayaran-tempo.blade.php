@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/lain/pembayaran_tempo?')?>">
                <?php $get = $this->input->get()?>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="customer" class="form-control select2">
                                    <option value="all">All</option>
                                        @foreach ($area as $ar)
                                            <optgroup label="{{ucwords($ar['nama'])}}">
                                            @foreach ($customer as $c)
                                                @if($ar['id'] == $c['id_area'])
                                                    @if (isset($get['customer']) && $get['customer'] == $c['id'])
                                                        <option value="{{$c['id']}}" selected>{{ucwords($c['nama'])}}</option>
                                                    @else
                                                        <option value="{{$c['id']}}">{{ucwords($c['nama'])}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                            </optgroup>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Status</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="status" class="form-control">
                                    <option value="all">All</option>
                                    <?php
                                        foreach((array)$status as $key => $val){
                                            if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                                echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                            }else{
                                                echo '<option value="'.$key.'">'.$val.'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row col-md-6 mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                                <a href="{{base_url('pembelian/lain/pembayaran_tempo')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-10"></i>Refresh</button></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tempo Pembelian Bahan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Kode Transaksi</th>
                    <th data-sortable="true">Label</th>
                    <th data-sortable="true">Tgl Transaksi</th>
                    <th data-sortable="true">Tgl Jatuh Tempo</th>
                    <th data-sortable="true">Umur (Hari)</th>
                    <th data-sortable="true">Jumlah Tagihan</th>
                    <th data-sortable="true">Sisa Tagihan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($pembelian)){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('pembelian/lain/pembayaran_tempo/detail/'.$row['id'])."' class='btn btn-info active'><i class='fa fa-sign-in'>&nbsp;</i> Detail</a> ";
                            echo "</td>";
                            if($row['is_lunas'] == 0 && $row['tgl_tempo'] <= date('Y-m-d')){
                                echo "<td><label class='badge badge-danger'>Jatuh Tempo</label></td>";
                            }else if($row['is_lunas'] == 0 && $row['tgl_tempo'] > date('Y-m-d')){
                                echo "<td><label class='badge badge-primary'>Berlangsung</label></td>";                               
                            }else if ($row['is_lunas'] == 1){
                                echo "<td><label class='badge badge-success'>Lunas</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['customer']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['catatan']."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row['tanggal']."</label></td>";
                            echo "<td>";
                            if($row['tgl_tempo'] <= $now = date('Y-m-d') ){
                                echo "<label class='badge badge-danger'>".$row['tgl_tempo']."</label>";
                                echo " <label class='badge badge-danger' title='melewati tanggal'><i class='fa fa-bell-o'></i></label>";
                            }else{
                                echo "<label class='badge badge-primary'>".$row['tgl_tempo']."</label>";
                            }
                            echo "</td>";
                            echo "<td class='text-center'>";
                            if($row['is_lunas'] == 1){
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($row['tgl_bayar']));
                            }else{
                                $interval = date_diff(new DateTime($row['tgl_tempo']), new DateTime($now));
                            }
                            if($row['tgl_tempo'] <= $now = date('Y-m-d') ){
                                echo "<label class='tx-danger'>".$interval->days."</label>";
                            }else{
                                echo "<label class='tx-primary'>- ".$interval->days."</label>";
                            }
                            echo "</td>";
                            echo "<td class='text-center'>".monefy($row['grandtotal'], false)."</td>";
                            echo "<td class='text-center'>".monefy(($row['grandtotal']-$row['total_bayar']), false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.select2').select2();
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function setuju(el) {
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            console.log(result);
            $('#kode_pembelian').html(result.kode_pembelian);
            $('[name=tanggal_pembelian]').val(result.tanggal_pembelian);
            $('[name=id]').val(result.id);
            $('#form-pembelian [name=supplier]').val(result.id_supplier).change();
            if(result.is_tempo == 1){
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val(result.tgl_tempo);
            }else{
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val('');
            }
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
            if(result.jenis_pembayaran == 'cash'){
                $('[name=id_kas]').val(result.id_kas).change();
                $('[name=id_rek_pengirim]').val('').change();
                $('[name=id_rek_penerima]').val('').change();
            }else if(result.jenis_pembayaran == 'transfer'){
                $('[name=id_kas]').val('').change();
                $('[name=id_rek_pengirim]').val(result.id_rek_pengirim).change();
                $('[name=id_rek_penerima]').val(result.id_rek_penerima).change();
            }
            if(result.dibayar_oleh == "0"){
                $('[name=dibayar]').val('').change();
            }else{
                $('[name=dibayar]').val(result.dibayar_oleh).change();
            }
            if(result.checker == "0"){
                $('[name=checker]').val('').change();
            }else{
                $('[name=checker]').val(result.checker).change();
            }
            if(result.tgl_bayar != "0000-00-00"){
                $('[name=tgl_bayar]').val(result.tgl_bayar);
            }
            $('#grandtotal').html(parseInt(result.grandtotal).toLocaleString());
            $("#modal_form").modal('show');
            cek_tempo();
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/pembelian_gabah_tolak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end