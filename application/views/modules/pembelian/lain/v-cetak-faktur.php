<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .items td {
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }

    table thead td {
        /* background-color: #EEEEEE; */
        text-align: center;
        border: 0.1mm solid #000000;
        /* font-variant: small-caps; */
        font-weight:bold;
    }

    /* table tbody td{
        border-bottom: 0.05mm solid #aaa;
    } */

    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        font-weight:bold;
        text-align:center;
    }

    .items td.totals {
        /* text-align: right; */
        border: 0.1mm solid #000000;
    }

    .items td.cost {
        text-align: center;
    }

    .ttd{
        font-size:8pt;
        text-align:center;
    }
    .text-center{
        text-align:center;
    }
    .text-left{
        text-align:left;
    }
    .text-right{
        text-align:right;
    }
    .keterangan{
        word-wrap : break-word;
        text-align : left;
        width:30%;
        padding:5px;
    }
    </style>
</head>

<body>
<!--mpdf
<htmlpageheader name="myheader">
<table width="100%">
    <tr>
        <td width="34%" style="color:#000;padding-top:6px">
            <span style="font-weight: bold; font-size: 8pt;">PT. Berkat Anoegerah Sejahtera</span><br />
            <span style="font-size:7pt">Krajan, Plerean, Kec. Sumberjambe</span><br />
            <span style="font-size:7pt">Kab. Jember - Jawa Timur</span><br />
            <span style="font-family:dejavusanscondensed; font-size:7pt">&#9742;(0331) 566130</span> 
        </td>
        <td width="33%" style="text-align:center;line-height:1.5">
            <span style="font-weight: bold; font-size: 10pt;">NOTA PEMBELIAN</span><br>
            <span style="font-size:8pt;">Nomor : <?=$detail['kode']?></span><br>
            <span style="font-size:8pt;"><?= tanggal_indo($detail['tanggal'])?></span><br>
        </td>
        <td width="33%" style="text-align: right;">
            <img src="assets/img/logo/bas-black.png" style="width:115px">
        </td>
    </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
    <div style="border-top: 0.1mm solid #000000; font-size: 8pt; text-align: center; padding-top: 1mm; ">
        <table width="100%">
            <tr>
                <td width="50%" style="color:#444;text-align:left; font-size:7pt">
                    <b>Input by : <?= ucwords($detail['username_input'])?>, <?= $detail['timestamp_input']?></b>
                </td>
                <td width="50%" style="color:#444;text-align:right; font-size:7pt">
                    <b>Printed by : <?= ucwords($this->session->auth['nama'])?>, <?= date('Y-m-d H:i:s')?></b>
                </td>
            <tr>
        </table>
    </div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
    <hr style="margin-top:0px;height:0.1mm;color:#000;">
    <table border="none" width="100%">
        <tr>
            <td width="50%" class="text-left">
                <span style="font-size:7pt">Kepada Yth : <b><?= ucwords($detail['customer'])?></b></span>
            </td>
            <td width="50%" class="text-right">
            <?php 
                if($detail['tgl_tempo']!= '0000-00-00' && $detail['is_tempo']){
                    echo '<span style="font-size:7pt">Tanggal Tempo : <b>'.ucwords($detail["tgl_tempo"]).'</b>'; 
                }
                ?>
            </td>
        </tr>
        <tr>
            <td width="50%" class="text-left">
            <?php 
                if($detail['customer_alamat']!= ''){
                    echo '<span style="font-size:7pt">'.ucwords($detail["customer_alamat"]).' | '.$detail["customer_telp"].'</span>';
                }
            ?>
            </td>
            <td width="50%" class="text-right">
                
                
            </td>
        </tr>
    </table>
    <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse; margin-top:7px;" cellpadding="4">
            <thead>
                <tr>
                    <td class="text-center">No.</td>
                    <td style="text-align:left;">Nama Barang</td>
                    <td class="text-center">Kuantitas</td>
                    <td class="text-center">Harga Satuan</td>
                    <td class="text-center">Subtotal</td>
                    <td class="text-left keterangan">Keterangan</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                $total = 0;
                foreach($detail_trx as $row){
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td class="text-left">'.ucwords($row['jenis_bahan']).'</td>';
                    echo '<td class="text-center">'.monefy($row['kuantitas'],false).'</td>';
                    echo '<td class="text-center">'.monefy($row['harga_satuan'],false).'</td>';
                    echo '<td class="text-center">'.monefy($row['kuantitas']*$row['harga_satuan'],false).'</td>';
                    echo '<td class="keterangan">'.$row['catatan'].'</td>';
                    echo '</tr>';
                    $total += ($row['kuantitas']*$row['harga_satuan']);
                }
                
                if(!empty($detail['biaya_tambahan'])){
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td class="text-left">Biaya Tambahan</td>';
                    echo '<td class="text-center"></td>';
                    echo '<td class="text-center"></td>';
                    echo '<td class="text-center">'.monefy($detail['biaya_tambahan'],false).'</td>';
                    echo '<td class="text-center"></td>';
                    echo '</tr>';
                    $total += $detail['biaya_tambahan'];
                }

                if(!empty($detail['potongan'])){
                    echo '<tr>';
                    echo '<td class="text-center">'.$no++.'</td>';
                    echo '<td class="text-left">Potongan</td>';
                    echo '<td class="text-center"></td>';
                    echo '<td class="text-center"></td>';
                    echo '<td class="text-center">-'.monefy($detail['potongan'],false).'</td>';
                    echo '<td class="text-center"></td>';
                    echo '</tr>';
                    $total -= $detail['potongan'];
                }
                ?>
                <tr>
                    <td class="totals text-center" colspan="4"><b>Total</b></td>
                    <td class="totals text-center"><b><?= monefy($total, false)?></b></td>
                    <td class="totals"></td>
                </tr>
            </tbody>
        </table>
        <!-- <div style="font-size: 11px; text-align: center; margin-top : 10px">        
        <p># <?= $detail['terbilang']?> #</p>
            </div> -->
        <table class="" width="100%" style="font-size: 8pt; border-collapse: collapse; margin-top:0px" cellpadding="20">
        <tr>
            <td class="ttd">
                Penerima
            </td>    
            <!-- <td class="ttd">
                Pengirim
            </td>    -->
            <td class="ttd">
                Checker
            </td>
        </tr>
        <tr>
            <td class="ttd">
                <b>( _________________ )</b>
            </td>
            <!-- <td class="ttd">
                <b>( _________________ )</b>
            </td> -->
            <td class="ttd">
                <b>( _________________ )</b>
            </td>

        </tr>
    </table>
    <!-- <div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div> -->
</body>

</html>