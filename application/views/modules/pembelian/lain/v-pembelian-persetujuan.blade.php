@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/lain/persetujuan/index?')?>">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label" style="margin:auto;">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi Pembelian Lain</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))    
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Total</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['persetujuan'] != 1){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                if($row['persetujuan'] == 0){
                                    echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                                }
                            }
                            echo "<a href='".base_url('pembelian/lain/persetujuan/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Transaksi'><i class='fa fa-sign-in'></i></button></a> ";
                            echo "<a href='".base_url('pembelian/lain/kelola/cetak_faktur/'.$row['id'])."' target='_blank'><button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Cetak Nota'><i class='fa fa-print'></i></button></a> ";
                            echo "</td>";
                            if($row['persetujuan'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";
                            }else if($row['persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else if($row['persetujuan'] == 2){
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }
                            echo "<td>".$row['tanggal']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode']."</label></td>";
                            echo "<td>".$row['customer']."</td>";
                            echo "<td>".monefy($row['total'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pemesanan</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/lain/persetujuan/submit_approval')?>" id="form-pembelian">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Pembelian | <label id="kode" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <input type="hidden" name="kuantitas" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tanggal" class="form-control" value="" readonly="">
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select id="select_customer" name="customer" class="form-control">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($customer as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-1">Tempo</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-2">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row col-12 my-3 tempo">
                            <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_tempo" class="form-control tempo" value="">
                            </div>
                        </div>
                        <div class="row col-12 my-3 not-tempo">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 not-tempo cash">
                            <label class="col-sm-4 form-control-label">Sumber Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3 not-tempo transfer">
                            <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_penerima" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_penerima as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3 not-tempo">
                            <label class="col-sm-4 form-control-label">Tanggal Bayar <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_bayar" class="form-control" value="">
                            </div>
                        </div>
                       
                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp</span>
                                    </div>
                                    <input type="hidden" name="grandtotal" value="" id="total">
                                    <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal"></label>
                                    <input type="hidden" name="jumlah_bayar" class="form-control" value="" autocomplete="off" id="bayar">
                                </div>
                            </div>
                        </div>

                        <div class="row col-12 my-2 not-tempo">
                            <label class="col-sm-4 form-control-label">Potong Uang Muka</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" id="piutang" name="potong_piutang" class="form-control not-tempo autonumeric" value="" onblur="myFunction()">  
                                <p class="help-text mg-t-3 tx-success"> Saldo Uang Muka : <b><span id="balance"></span></b></p>
                                <p class="help-text mg-t-3 tx-danger"><b><span id="error_piutang"></span></b></p>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Setujui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end


@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {


    

    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    // $('.select_customer').disable();
      
    
});


function myFunction(){
    // var x = document.getElementById("piutang").value;
    // var y = document.getElementById("balance").innerHTML;
    // var z = document.getElementById("bayar").value;
    // var bon = parseInt(x.replaceAll(",", ""));
    // var saldo = parseInt(y.replaceAll(",", ""));
    // var bayar = parseInt(z.replaceAll(",", ""));
    
    // if (bon>bayar){
    //     Swal.fire({
    //     title: 'Error',
    //     text: 'Jumlah uang muka tidak diperbolehkan melebihi jumlah bayar',
    //     type: 'warning',
    //     confirmButtonText: 'Tutup'
    //     }).then(document.getElementById("piutang").value='0')
    // }else if(bon>saldo){
    //     Swal.fire({
    //     title: 'Error',
    //     text: 'Potongan uang muka tidak boleh melebihi saldo uang muka',
    //     type: 'warning',
    //     confirmButtonText: 'Tutup'
    //     }).then(document.getElementById("piutang").value='0')
    // }
}

// function cek_jumlahbayar(){
//     var x = document.getElementById("bayar").value;
//     var y = document.getElementById("total").value;
//     var bayar = parseInt(x.replaceAll(",", ""));
//     var total = parseInt(y.replaceAll(",", ""));


//     if (bayar>total){
//         Swal.fire({
//         title: 'Error',
//         text: 'Jumlah bayar tidak diperbolehkan melebihi jumlah grandtotal',
//         type: 'warning',
//         confirmButtonText: 'Tutup'
//         }).then(document.getElementById("bayar").value='0')
//     }
// }


$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(jenis_pembayaran == 'cash' && is_tempo == 0){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer' && is_tempo == 0){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

$('#is_paid_off').change(function(){
    set_lunas();
});

function set_lunas(){
    if($('#is_paid_off').is(':checked')){
        let sisa_bayar = parseInt($('[name=grandtotal]').val());
        $('[name=jumlah_bayar]').val(sisa_bayar);
        $('[name=jumlah_bayar]').autoNumeric('destroy');
        $('[name=jumlah_bayar]').autoNumeric('init', {
            'mDec': 0
        });
        $('[name=jumlah_bayar]').prop('readonly', true);
    }else{
        $('[name=jumlah_bayar]').prop('readonly', false);
    }
}






function setuju(el) {
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/lain/kelola/json_get_detail_trx')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            console.log(result);
            $('#kode').empty().html(result.kode);
            $('[name=tanggal]').val(result.tanggal);
            $('[name=id]').val(result.id);
            $('[name=kuantitas]').val(result.kuantitas);
            $('#form-pembelian [name=customer]').val(result.id_customer).change();
            if(result.is_tempo == 1){
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val(result.tgl_tempo);
            }else{
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val('');
                if(result.jenis_pembayaran == 'cash'){
                    if(result.id_kas == 0){
                        $('[name=id_kas]').val('').change();
                    }else{
                        $('[name=id_kas]').val(result.id_kas).change();
                    }
                    $('[name=id_rek_penerima]').val('').change();
                }else if(result.jenis_pembayaran == 'transfer'){
                    $('[name=id_kas]').val('').change();
                    if(result.id_kas == 0){
                        $('[name=id_rek_penerima]').val('').change();
                    }else{
                        $('[name=id_rek_penerima]').val(result.id_kas).change();
                    }
                }
            }
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);

            if(result.tgl_bayar != "0000-00-00"){
                $('[name=tgl_bayar]').val(result.tgl_bayar);
            }
            $('[name=grandtotal]').val(result.grandtotal);
            $('[name=potong_piutang]').val(result.potong_piutang)
            $('#balance').html(parseInt(result.balance).toLocaleString());
            $('[name=jumlah_bayar]').val(result.jumlah_bayar);
            $('#grandtotal').html(parseInt(result.grandtotal).toLocaleString());
            $('#bayar').val(result.grandtotal);
            $("#modal_form").modal('show');
            $('.autonumeric').autoNumeric('destroy');
            $('.autonumeric').autoNumeric('init', {
                'mDec': 0
            });
            
            cek_tempo();
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/lain/kelola/pembelian_lain_tolak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}


</script>
@end