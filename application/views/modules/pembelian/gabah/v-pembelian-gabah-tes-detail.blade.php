@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail Tes 
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="post" action="{{base_url('pembelian/gabah/quality_control/submit_form_detail')}}">
                    <input type="hidden" name="id_tes" value="{{$tes['id']}}">
                    <div class="row">
                        <div class="col-6">
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Kode Pembelian</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k1" type="text" class="form-control" value="{{ucwords($tes['kode_pembelian'])}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k2" type="text" class="form-control" value="{{$tes['tanggal_pembelian']}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10" hidden>
                                <label class="col-sm-4 form-control-label">Status Persetujuan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="status" type="text" class="form-control" value="{{$tes['status_persetujuan']}}" readonly>
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Supplier</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k3" type="text" class="form-control" value="{{ucwords($tes['supplier'])}}" readonly>
                                </div>
                            </div>

                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Timbang</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k4" type="text" class="form-control" value="#{{($tes['batch'])}}" readonly>
                                </div>
                            </div>

                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Jenis Gabah</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k5" type="text" class="form-control" value="{{'GABAH '.strtoupper($tes['jenis_gabah'])}}" readonly>
                                </div>
                            </div>

                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Jenis Padi</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input id="k6" type="text" class="form-control" value="{{strtoupper($tes['padi'])}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Tanggal Tes</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" name="tanggal_tes" class="form-control" value="{{$tes['tanggal_tes']}}" required="">
                                </div>
                            </div>

                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Jam Mulai</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="time" name="jam_mulai" class="form-control" value="{{$tes['jam_mulai']}}" required="">
                                </div>
                            </div>
                            
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Jam Selesai</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="time" name="jam_selesai" class="form-control" value="{{$tes['jam_selesai']}}" required="">
                                </div>
                            </div>

                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Petugas</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" name="petugas" class="form-control" value="{{$tes['petugas']}}" required="">
                                </div>
                            </div>

                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Hasil Akhir</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" name="hasil_akhir" class="form-control" value="{{$tes['hasil_akhir']}}" step="0,1" required="">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="row mg-t-5">
                        <div class="col-12 tx-center">
                            <button id="submit" type="submit" class="btn btn-success"><i class="fa fa-send"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Detail Tes</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table table-striped table-white" id="tabel-tes">
            <thead>
                <tr>
                    <th class="text-center">Aksi</th>
                    <th class="text-center">Gabah</th>
                    <th class="text-center">Padi</th>
                    <th class="text-center">Tes ke</th>
                    <th class="text-center">Hasil Tes (%)</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $jumlah = 0;
                $total = 0;
                $avg = 0;
                if(!empty($detail)){
                    foreach($detail as $row){
                        $subtotal = 0;
                        echo '<tr>';
                        echo '<td><button class="btn btn-danger" onclick="hapus(this)" data-id="'.$row['id'].'"><i class="fa fa-trash"></i></button></td>';
                        echo '<td class="text-center">GABAH '.strtoupper($row['jenis_gabah']).'</td>';
                        echo '<td class="text-center">'.strtoupper($row['padi']).'</td>';
                        echo '<td class="text-center">#'.$row['batch_tes'].'</td>';
                        echo '<td class="text-center">'.$row['hasil_tes'].'</td>';
                        echo '</tr>';
                        $total += $row['hasil_tes'];
                        $jumlah ++;
                    }
                    $avg = $total / $jumlah;
                }
                
                ?>
                <tr>
                    <td class="totals text-center" colspan="4"><b>Jumlah Tes</b></td>
                    <td class="totals text-center"><b><?= $jumlah?></b> kali</td>
                </tr>
                <tr>
                    <td class="totals text-center" colspan="4"><b>Rata-rata</b></td>
                    <td class="totals text-center"><b><?= monefy($avg, 1)?></b> %</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tes Gabah</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-layout form-layout-4">
                    <form method="post" action="{{base_url('pembelian/gabah/quality_control/submit_tes')}}" id="form-data">
                        <input type="hidden" name="id_tes" value="{{$tes['id']}}">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tes ke <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="number" class="form-control" name="batch_tes" autocomplete="off" required="" value=""> 
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Hasil Tes<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <div class="input-group">
                                    <input type="number" name="hasil_tes" class="form-control" value="" step="0,1" required="">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-tes').bootstrapTable({
        // pagination: true,
        // search: true,
        toolbar: '#toolbar'
    });

    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    if ($('#status').val() == "1"){
        $('#submit').prop('disabled', true); 
        $('#tambah').prop('disabled', true);
    }else{
        $('#submit').prop('disabled', false);
        $('#tambah').prop('disabled', false);

    }

});

$('#tambah').click(function() {
    $('#form-data').trigger('reset');
    $("#modal_form").modal('show');
});




function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function convert_to_number(input){
    return Number(input.replace(/[^0-9.-]+/g,""));
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}



function hapus(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Hapus data tes, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('pembelian/gabah/quality_control/hapus_detail_tes')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}

</script>
@end