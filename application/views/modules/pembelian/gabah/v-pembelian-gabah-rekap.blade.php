@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/rekap?')?>" id="form-filter">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button> 
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi Pembelian Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Plat Nomor</th>
                    <th data-sortable="true">Berat Netto</th>
                    <th data-sortable="true">Tempo</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Dari</th>
                    <!-- <th data-sortable="true">Kepada</th> -->
                    <th data-sortable="true">Grandtotal</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $grandtotal = 0;
                    $total_netto = 0;
                    if(!empty($pembelian)){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='data timbangan' onclick='timbangan(".$row['id'].")'><i class='fa fa-balance-scale'></i></button> ";
                            echo "<a href='".base_url('pembelian/gabah/kelola/detail_transaksi/'.$row['id'])."' class='btn btn-info active' target='_blank' data-toggle='tooltip' data-placement='top' title='detail transaksi'><i class='fa fa-sign-in'></i></a> ";
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode_pembelian']."</label></td>";
                            echo "<td>".$row['tanggal_pembelian']."</td>";
                            echo "<td>".$row['supplier']."</td>";
                            if(!empty($row['resi_pengiriman'])){
                                echo "<td><label class='badge badge-info' onclick='cek_resi(this)' data-resi='".$row['resi_pengiriman']."' data-toggle='tooltip' data-placement='top' title='Informasi Ekspedisi'>".$row['resi_pengiriman']."</label><br>
                                <label class='badge badge-light'>".$row['plat_kendaraan']."</label>
                                </td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['plat_kendaraan']."</label></td>";
                            }               
                            
                            echo "<td>".monefy($row['total_netto'], false)."</td>";
                            
                            if($row['is_tempo'] == 1){
                                echo "<td><label class='badge badge-warning'>Tempo</label><br>
                                <label class='badge badge-secondary'>Tanggal : ".$row['tgl_tempo']."</label>";
                                echo "</td>";
                            }else{
                                echo "<td><label class='badge badge-light'>Tidak</label></td>";
                            }

                            if($row['jenis_pembayaran'] == 'tbd'){
                                echo "<td><label class='badge badge-light'>-</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".ucwords($row['jenis_pembayaran'])."</label></td>";
                            }

                            if($row['jenis_pembayaran'] == 'transfer'){
                                echo "<td>";
                                echo "<label class='badge badge-primary'>".ucwords($row['rek_pengirim_bank'])."</label><br>";
                                echo "<label class='badge badge-light'>".ucwords($row['rek_pengirim_nama'])."</label><br>";
                                echo "<label class='badge badge-light'>".$row['rek_pengirim_no_rek']."</label>";
                                echo "</td>";
                               
                                // echo "<td>";
                                // echo "<label class='badge badge-primary'>".ucwords($row['rek_penerima_bank'])."</label><br>";
                                // echo "<label class='badge badge-light'>".ucwords($row['rek_penerima'])."</label><br>";
                                // echo "<label class='badge badge-light'>".$row['rek_penerima_no']."</label>";
                                // echo "</td>";
                            }else{
                                if(!empty($row['kas_kas'])){
                                    echo "<td><label class='badge badge-info'>".ucwords($row['kas_kas'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                                // echo "<td>-</td>";
                            }      
                            
                            echo "<td>Rp ".monefy($row['grandtotal'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_pengajuan']."</label></td>";
                            echo "</tr>";
                            $grandtotal += $row['grandtotal'];
                            $total_netto += $row['total_netto'];
                        }
                    }
				?>
                
            </tbody>
        </table>
        <table class="table">
            <tr class="tx-bold tx-16">
                <td>Total Netto</td>
                <td>:</td>
                <td>{{monefy($total_netto,false)}}</td>
            </tr>
            <tr class="tx-bold tx-16">
                <td>Total Pembelian</td>
                <td>:</td>
                <td>{{monefy($grandtotal,false)}}</td>
            </tr>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/kelola/submit_pembelian_gabah')?>" id="form-pembelian">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data Pembelian</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Supplier <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_supplier" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($supplier as $row)
                                    <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Ekspedisi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="resi_pengiriman" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <option value="0">Tanpa Ekspedisi</option>
                                    @foreach ($ekspedisi as $row)
                                    <option value="{{$row->no_resi}}" data-tanggal_jalan="{{$row->tanggal_jalan}}" data-plat="{{$row->no_pol}}">{{$row->no_resi.' | '.ucwords($row->sopir)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Plat Kendaraan<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="plat_kendaraan" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="tanggal_pembelian" value="" autocomplete="off" required="">
                            </div>
                        </div>
                        <hr>

                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Bruto<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_bruto" value="0" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Kosong<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_kosong" value="0" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Muatan<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_muatan" value="0" autocomplete="off" readonly="">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_timbangan" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Timbangan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="form-layout form-layout-4">
                                <table class="table table-bordered table-hover" id="tabel_timbangan">
                                    <thead class="bg-info">
                                        <th>#</th>
                                        <th nowrap>Label</th>
                                        <th>Berat Bruto</th>
                                        <th>Berat Kosong</th>
                                        <th>Potongan</th>
                                        <th>Berat Netto</th>
                                        <!-- <th>Manual</th> -->
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <div class="row mg-t-10">
                                    <div class="col-12 mg-t-10 mg-sm-t-0 text-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});

$(document).on('change keyup', '.hitung1', function(){
    let kotor = getNum($(this).closest('tr').find('[name=berat_kotor]').val());
    let kosong = getNum($(this).closest('tr').find('[name=berat_kosong]').val());
    let netto = kotor - kosong;
    $(this).closest('tr').find('[name=berat_netto]').val(netto);
});

$(document).on('change keyup', '.hitung2', function(){
    let kotor = getNum($(this).closest('tr').find('[name=berat_kotor]').val());
    let netto = getNum($(this).closest('tr').find('[name=berat_netto]').val());
    let kosong = kotor - netto;
    $(this).closest('tr').find('[name=berat_kosong]').val(kosong);
});


$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $('#form-pembelian').trigger('reset');
    $("#modal_form").modal('show');
});

$("#form-tambah-timbangan [name=berat_kotor], #form-tambah-timbangan [name=berat_kosong]").on('keyup', function(){
    let kotor = getNum($("#form-tambah-timbangan [name=berat_kotor]").val());
    let kosong = getNum($("#form-tambah-timbangan [name=berat_kosong]").val());
    let netto = kotor - kosong;
    $("#form-tambah-timbangan [name=berat_netto]").val(netto);
});

$("#form-tambah-timbangan [name=berat_netto]").on('keyup', function(){
    let kotor = getNum($("#form-tambah-timbangan [name=berat_kotor]").val());
    let netto = getNum($("#form-tambah-timbangan [name=berat_netto]").val());
    let kosong = kotor - netto;
    $("#form-tambah-timbangan [name=berat_kosong]").val(kosong);
});

$('#form-pembelian [name=berat_bruto], #form-pembelian [name=berat_kosong]').on('keyup', function(){
    hitungMuatan();
});

function cek_resi(el){
    var resi = $(el).data().resi;
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/info_resi')?>",
        type: "POST",
        dataType: "json",
        data: {
            'resi': resi,
        },
        success: function(result) {
            $('#form-pembelian').trigger('reset');
            $('#form-pembelian [name=id]').val(result.id);
            
            $('#modal_form').modal('show');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    
}


function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function hitungMuatan(){
    let bruto = parseInt($('#form-pembelian [name=berat_bruto]').val());
    let kosong = parseInt($('#form-pembelian [name=berat_kosong]').val());
    $('#form-pembelian [name=berat_muatan]').val(bruto-kosong);
}

function timbangan(id){
    if(id != ''){
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/get_data_timbangan')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                // Init data untuk form tambah
                $('#form-tambah-timbangan [name=id_pembelian]').val(result.pembelian.id);
                $('#form-tambah-timbangan [name=batch]').val(result.batch_idx);
                $('#form-tambah-timbangan [name=berat_kotor]').val(result.sisa_bruto);
                init_table(id);
                $('#modal_timbangan').modal('show');
                $('#tab1 a[href="#tab-timbangan"]').tab('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function init_table(id){
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/get_data_timbangan')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': id,
        },
        success: function(result) {
            $('#tabel_timbangan tbody').empty();
            if(result.timbangan != false){
                let row = '';
                // let is_manual = '';
                $(result.timbangan).each(function(a, b){
                    // if(b.is_manual == 1){
                    //     is_manual = 'checked=""';
                    // }else{
                    //     is_manual = '';
                    // }
                    row = '<tr>\
                    <td><input type="hidden" name="id" value="'+b.id+'"><label class="form-control" name="batch">'+b.batch+'</label></td>\
                    <td><label class="form-control" name="batch_label">'+b.batch_label+'</label></td>\
                    <td><input type="number" class="form-control hitung1" name="berat_kotor" value="'+b.berat_kotor+'" disabled></td>\
                    <td><input type="number" class="form-control hitung1" name="berat_kosong" value="'+b.berat_kosong+'" disabled></td>\
                    <td><input type="number" class="form-control hitung1" name="potongan_timbangan" value="'+b.potongan_timbangan+'" disabled></td>\
                    <td><input type="number" class="form-control hitung2" name="berat_netto" value="'+b.berat_netto+'" disabled></td>\
                    \
                    </tr>';
                    // console.log(b);
                    $('#tabel_timbangan tbody').append(row);
                });
            }
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let supplier = $("#form-filter [name=supplier]").val();
    // if (start != '' && end != '') {
        var url = "<?= base_url('pembelian/gabah/rekap/export?')?>"+'start='+start+'&end='+end+'&supplier='+supplier;
        window.open(url, '_blank');
    // } else {
        // Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}
</script>
@end