@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/persetujuan?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pembelian Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Tempo</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Dari</th>
                    <!-- <th data-sortable="true">Kepada</th> -->
                    <th data-sortable="true">Potong Uang Muka</th>
                    <th data-sortable="true">Grandtotal</th>
                    <th data-sortable="true">Diajukan Oleh</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($pembelian)){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['status_persetujuan'] == 0){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button> ";
                            }else if($row['status_persetujuan'] == 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "<a href='".base_url('pembelian/gabah/faktur/detail/'.$row['id'])."' class='btn btn-info active'>Detail</a> ";
                            echo "</td>";
                            if($row['status_persetujuan'] == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";
                            }else if($row['status_persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label><br>
                                        <label class='badge badge-light'>".ucwords($row['verifikasi'])." | ".$row['timestamp_verifikasi']."</label>
                                </td>";                               
                            }else{
                                echo "<td><label class='badge badge-danger'>Ditolak</label><br>
                                        <label class='badge badge-light'>".ucwords($row['verifikasi'])." | ".$row['timestamp_verifikasi']."</label>
                                </td>";        
                            }
                            echo "<td>".$row['tanggal_pembelian']."</td>";
                            echo "<td>".$row['supplier']."</td>";
                            if($row['is_tempo'] == 1){
                                echo "<td><label class='badge badge-warning'>Tempo</label>";
                                if($row['tgl_tempo'] <= date('Y-m-d') ){
                                    echo " <label class='badge badge-danger' title='melewati tanggal'><i class='fa fa-bell-o'></i></label>";
                                }
                                echo "<br>
                                <label class='badge badge-secondary'>Tanggal : ".$row['tgl_tempo']."</label>";
                                
                                echo "</td>";
                            }else{
                                echo "<td><label class='badge badge-light'>Tidak</label></td>";
                            }

                            if($row['jenis_pembayaran'] == 'tbd'){
                                echo "<td><label class='badge badge-light'>-</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".ucwords($row['jenis_pembayaran'])."</label></td>";
                            }

                            if($row['jenis_pembayaran'] == 'transfer'){
                                echo "<td>";
                                echo "<label class='badge badge-primary'>".ucwords($row['rek_pengirim_bank'])."</label><br>";
                                echo "<label class='badge badge-light'>".ucwords($row['rek_pengirim_nama'])."</label><br>";
                                echo "<label class='badge badge-light'>".$row['rek_pengirim_no_rek']."</label>";
                                echo "</td>";
                               
                                // echo "<td>";
                                // echo "<label class='badge badge-primary'>".ucwords($row['rek_penerima_bank'])."</label><br>";
                                // echo "<label class='badge badge-light'>".ucwords($row['rek_penerima'])."</label><br>";
                                // echo "<label class='badge badge-light'>".$row['rek_penerima_no']."</label>";
                                // echo "</td>";
                            }else{
                                if(!empty($row['kas_kas'])){
                                    echo "<td><label class='badge badge-info'>".ucwords($row['kas_kas'])."</label></td>";
                                }else{
                                    echo "<td>-</td>";
                                }
                                // echo "<td>-</td>";
                            }      
                            echo "<td>Rp ".monefy($row['potong_piutang'], false)."</td>";
                            echo "<td>Rp ".monefy($row['grandtotal'], false)."</td>";
                           

                            
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_pengajuan']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/persetujuan/submit_approval')?>" id="form-pembelian">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Pembelian | <label id="kode_pembelian" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <!-- <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Kode Pembelian</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="kode_pembelian" class="form-control" value="" readonly="">
                            </div>
                        </div> -->
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tanggal_pembelian" class="form-control" value="" readonly="">
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="supplier" class="form-control" readonly="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($supplier as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-1">Tempo</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input">
                                    <label class="custom-control-label" for="tempo-2">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row col-12 my-3 tempo">
                            <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_tempo" class="form-control tempo" value="">
                            </div>
                        </div>
                        <!-- <div class="row col-12 my-3 tempo">
                            <label class="col-sm-4 form-control-label">Bayar DP</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="bayar_dp" class="form-control tempo autonumeric" value="" autocomplete="off" placeholder="Jika ada pembayaran DP">
                            </div>
                        </div> -->
                        <div class="row col-12 my-3 langsung">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung cash">
                            <label class="col-sm-4 form-control-label">Sumber Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row col-12 my-3 langsung transfer">
                            <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_pengirim" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_pengirim as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- <div class="row col-12 my-3 langsung transfer">
                            <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_penerima" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_penerima as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> -->
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Potong Bon</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="potong_piutang" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan potongan piutang">
                            </div>
                        </div>
                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label">Dibayar oleh</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="dibayar">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label">Checker </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="checker">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Bayar</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_bayar" class="form-control" value="" required>
                            </div>
                        </div>
                        <div class="row col-12 my-2">
                            <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp</span>
                                    </div>
                                    <input type="hidden" name="grandtotal" value="">
                                    <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row col-12 my-3 not-tempo">
                            <label class="col-sm-4 form-control-label">Jumlah Bayar</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="jumlah_bayar" class="form-control not-tempo autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah bayar">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    
});

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function setuju(el) {
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id' : $(el).data().id,
        },
        success: function(result) {
            $('#kode_pembelian').html(result.kode_pembelian);
            $('[name=tanggal_pembelian]').val(result.tanggal_pembelian);
            $('[name=id]').val(result.id);
            $('#form-pembelian [name=supplier]').val(result.id_supplier).change();
            if(result.is_tempo == 1){
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val(result.tgl_tempo);
            }else{
                $('[name=is_tempo][value='+result.is_tempo+']').prop('checked', true);
                $('[name=tgl_tempo]').val('');
            }
            $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
            if(result.jenis_pembayaran == 'cash'){
                $('[name=id_kas]').val(result.id_kas).change();
                $('[name=id_rek_pengirim]').val('').change();
                $('[name=id_rek_penerima]').val('').change();
            }else if(result.jenis_pembayaran == 'transfer'){
                $('[name=id_kas]').val('').change();
                $('[name=id_rek_pengirim]').val(result.id_rek_pengirim).change();
                $('[name=id_rek_penerima]').val(result.id_rek_penerima).change();
            }
            if(result.dibayar_oleh == "0"){
                $('[name=dibayar]').val('').change();
            }else{
                $('[name=dibayar]').val(result.dibayar_oleh).change();
            }
            if(result.checker == "0"){
                $('[name=checker]').val('').change();
            }else{
                $('[name=checker]').val(result.checker).change();
            }
            if(result.tgl_bayar != "0000-00-00"){
                $('[name=tgl_bayar]').val(result.tgl_bayar);
            }
            $('[name=bayar_dp]').val(result.bayar_tempo);
            $('[name=potong_piutang]').val(result.potong_piutang);
            $('[name=grandtotal]').val(result.grandtotal);
            $('[name=jumlah_bayar]').val(result.grandtotal);
            $('#grandtotal').html(parseInt(result.grandtotal).toLocaleString());
            $("#modal_form").modal('show');
            $('.autonumeric').autoNumeric('destroy');
            $('.autonumeric').autoNumeric('init', {
                'mDec': 0
            });
            cek_tempo();
            cek_metode_pembayaran();
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {
        }
    });
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan ditolak, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/pembelian_gabah_tolak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end