<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .items td {
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
    }

    table thead td {
        /* background-color: #EEEEEE; */
        text-align: center;
        border: 0.1mm solid #000000;
        /* font-variant: small-caps; */
        font-weight:bold;
    }

    .items td.blanktotal {
        background-color: #EEEEEE;
        border: 0.1mm solid #000000;
        background-color: #FFFFFF;
        border: 0mm none #000000;
        border-top: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        font-weight:bold;
        text-align:center;
    }

    .items td.totals {
        text-align: right;
        border: 0.1mm solid #000000;
    }

    .items td.cost {
        text-align: center;
    }

    .ttd{
        font-size:9pt;
        text-align:center;
    }
    </style>
</head>

<body>
<!--mpdf
<htmlpageheader name="myheader">
<table width="100%">
    <tr>
        <td width="34%" style="color:#000;padding-top:6px">
            <span style="font-weight: bold; font-size: 10pt;">PT. Berkat Anoegerah Sejahtera</span><br />
            <span style="font-size:9pt">Krajan, Plerean, Kec. Sumberjambe</span><br />
            <span style="font-size:9pt">Kab. Jember - Jawa Timur</span><br />
            <span style="font-family:dejavusanscondensed; font-size:9pt">https://berkatgroup.com</span> <br>
            <span style="font-family:dejavusanscondensed; font-size:9pt">&#9742;(0331) 566130</span>
            
        </td>
        <td width="33%" style="text-align:center;">
            <span style="font-weight: bold; font-size: 12pt;">NOTA PEMBELIAN</span><br>
            <span style="font-size:9pt"><?= $tgl_pembelian?></span><br>
            <span style="font-size:9pt"><?= $pembelian['kode_pembelian']?></span>

        </td>
        <td width="33%" style="text-align: right;">
            <img src="assets/img/logo/bas-black.png" style="width:140px">
        </td>
    </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
    <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
        <table width="100%">
            <tr>
                <td width="50%" style="color:#444;text-align:left; font-size:7pt">
                    <i>Input by : <?= ucwords($pembelian['username_input'])?>, <?= $pembelian['timestamp_input']?></i>
                </td>
                <td width="50%" style="color:#444;text-align:right; font-size:7pt">
                    <i>Printed by : <?= ucwords($this->session->auth['nama'])?>, <?= date('Y-m-d H:i:s')?></i>
                </td>
            <tr>
        </table>
    </div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
    <hr style="margin-top:15px">
    <div style="text-align: left;">
        <span style="font-size:9pt">Kepada Yth : <b><?= ucwords($pembelian['supplier'])?></b> | <?= ucwords($pembelian['supplier_alamat'])?> | <?= $pembelian['supplier_telp']?></span></div>
    
    <br />
    <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="6">
        <thead>
            <tr>
                <td width="5%">No</td>
                <td width="20%" align="left">Nama Barang</td>
                <td width="10%">Qty<br>(Sak)</td>
                <td width="12%">Bruto <br>(Kg)</td>
                <td width="12%">Pot. Sak / Damen</td>
                <td width="12%">Pot. KA <br>(%)</td>
                <td width="10%">HP <br>(%)</td>
                <td width="12%">Netto <br>(Kg)</td>
                <td width="12%">Harga <br>(Rp)</td>
                <td width="15%">Subtotal <br>(Rp)</td>
            </tr>
        </thead>
        <tbody>
            <!-- ITEMS HERE -->
            <?php 
                $no = 1;
                $jml_sak = 0;
                $total_bruto = 0;
                $total_netto = 0;
                $grandtotal = 0;
                if(!empty($ks_jenis)){
                    foreach($ks_jenis as $row) { 
                        $berat_bruto = 0;
                        $berat_gabah = 0;
                        $berat_gabah_pot_KA = 0;
                        if(!empty($ks_detail)){
                            foreach($ks_detail as $a){
                                foreach($total_sak as $t){
                                    if($row['id_jenis'] == $a['id_jenis'] && $t['id_batch'] == $row['id_batch'] && $row['is_manual'] == $a['is_manual']){
                                        $berat_bruto = decimal_down($row['jumlah_sak'] / $t['jumlah'] * $t['berat_netto_nett']);
                                        $berat_gabah += decimal_down($a['jumlah_sak'] / $row['jumlah_sak'] * ((100 - $a['kadar_air']) / 100) * ($berat_bruto - $row['potong_lain'] - ($row['potong_sak'] * $a['jumlah_sak'])));
                                        $berat_gabah_pot_KA += decimal_down($a['jumlah_sak'] / $row['jumlah_sak'] * ((100 - $a['kadar_air']) / 100) * $berat_bruto);
                                    }
                                }
                            }
                        }
                        $netto = decimal_down($berat_gabah * ((100 - $row['hampa'])/100));
                        ?>
                        <tr>
                            <td align="center"><?= $no++?></td>
                            <td align="left"><?= '#'.$row['batch'].' GABAH '.strtoupper($row['jenis_gabah']).' '.strtoupper($row['padi'])?></td> -->
                            <td align="center"><?= monefy($row['jumlah_sak'], false)?></td>
                            <td class="cost"><?= monefy($berat_bruto, 1)?></td>
                            <td class="cost"><?= ($row['potong_sak'] * $row['jumlah_sak'])?> / <?= $row['potong_lain']?></td>
                            <td class="cost"><?= monefy(($berat_bruto - $berat_gabah_pot_KA) / $berat_bruto * 100, 1)?></td>
                            <td class="cost"><?= monefy($row['hampa'], 1)?></td>
                            <td align="center"><?= monefy($netto,1)?></td>
                            <td class='cost'><?= monefy($row['harga_nol_gabuk'], false)?></td>
                            <td class='cost'><?= monefy($row['subtotal'], false)?></td>
                        <?php 
                            $total_bruto += $berat_bruto;
                            $total_netto += $netto;
                            $grandtotal += $row['subtotal'];
                            $jml_sak += $row['jumlah_sak'];
                        ?>
                        </tr>
                        <?php
                        
                    }
                } 

                if(!empty($kg_jenis)){
                    foreach($kg_jenis as $row) { 
                        $berat_bruto = 0;
                        $berat_gabah = 0;
                        $berat_gabah_pot_KA = 0;
                        if(!empty($kg_detail)){
                            foreach($kg_detail as $a){
                                foreach($total_sak as $t){
                                    if($row['id_jenis'] == $a['id_jenis'] && $t['id_batch'] == $row['id_batch'] && $row['is_manual'] == $a['is_manual']){
                                        $berat_bruto = decimal_down($row['jumlah_sak'] / $t['jumlah'] * $t['berat_netto_nett']);
                                        $berat_gabah += decimal_down($a['jumlah_sak'] / $row['jumlah_sak'] * ((100 - $a['kadar_air']) / 100) * ($berat_bruto - $row['potong_lain'] - ($row['potong_sak'] * $a['jumlah_sak'])));
                                        $berat_gabah_pot_KA += decimal_down($a['jumlah_sak'] / $row['jumlah_sak'] * ((100 - $a['kadar_air']) / 100) * $berat_bruto);
                                    }
                                }
                            }
                        }
                        $netto = decimal_down($berat_gabah * ((100 - $row['hampa'])/100));
                        ?>
                        <tr>
                            <td align="center"><?= $no++?></td>
                            <td align="left"><?= '#'.$row['batch'].' GABAH '.strtoupper($row['jenis_gabah']).' '.strtoupper($row['padi'])?></td> -->
                            <td align="center"><?= monefy($row['jumlah_sak'], false)?></td>
                            <td class="cost"><?= monefy($berat_bruto, 1)?></td>
                            <td class="cost"><?= ($row['potong_sak'] * $row['jumlah_sak'])?> / <?= $row['potong_lain']?></td>
                            <td class="cost"><?= monefy(($berat_bruto - $berat_gabah_pot_KA) / $berat_bruto * 100, 1)?></td>
                            <td class="cost"><?= monefy($row['hampa'], 1)?></td>
                            <td align="center"><?= monefy($netto, 1)?></td>
                            <td class='cost'><?= monefy($row['harga_kg'], false)?></td>
                            <td class='cost'><?= monefy($row['subtotal'], false)?></td>
                        <?php 
                            $total_bruto += $berat_bruto;
                            $total_netto += $netto;
                            $grandtotal += $row['subtotal'];
                            $jml_sak += $row['jumlah_sak'];
                        ?>
                        </tr>
                        <?php
                        
                    }
                }
                ?>
            
            <!-- END ITEMS HERE -->
            <tr>
                <td class="blanktotal" colspan="2" rowspan="6">Total</td>
                <td class="blanktotal"><?= monefy($jml_sak, false)?></td>
                <td class="blanktotal"><?= monefy($total_bruto, false)?></td>
                <td class="blanktotal">-</td>
                <td class="blanktotal">-</td>
                <td class="blanktotal">-</td>
                <td class="blanktotal"><?= monefy($total_netto, false)?></td>
                <td class="blanktotal">-</td>
                <td class="blanktotal"><?= monefy($grandtotal, false)?></td>
            </tr>
            
        </tbody>
    </table>

    <table class="" width="100%" style="font-size: 9pt; border-collapse: collapse; margin-top:5px" cellpadding="20">
        <tr>    
            <td class="ttd">
                Supplier
            </td>
            <td class="ttd">
                Sopir/ Ekspedisi<br>( <?= $pembelian['plat_kendaraan']?> )
            </td>
            <td class="ttd">
                Dibayar Oleh
            </td>
            <td class="ttd">
                Checker
            </td>
        </tr>
        <tr>
            <td class="ttd">
                <b>( <?= ucwords($pembelian['supplier'])?> )</b>
            </td>
            <td class="ttd">
                <b><?php
                    if(!empty($ekspedisi)){
                        echo "( ".ucwords($ekspedisi->sopir)." )";
                    }else{
                        echo "( _________________ )";
                    }?>
                </b>
            </td>
            <td class="ttd">
                <b><?php
                    if(!empty($pembelian['pembayar'])){
                        echo "( ".ucwords($pembelian['pembayar'])." )";
                    }else{
                        echo "( _________________ )";
                    }
                ?></b>
            </td>
            <td class="ttd">
                <b><?php
                    if(!empty($pembelian['pengecek'])){
                        echo "( ".ucwords($pembelian['pengecek'])." )";
                    }else{
                        echo "( _________________ )";
                    }
                ?></b>
            </td>
        </tr>
    </table>
    <!-- <div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div> -->
</body>

</html>