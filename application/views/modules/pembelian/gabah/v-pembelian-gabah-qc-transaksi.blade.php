@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail Timbangan ID : {{$pembelian['kode_pembelian']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/quality_control?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Timbangan Ke</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['batch']}}</label>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Label</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['batch_label']}}</label>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Berat Bruto Timbangan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <label class="form-control">{{(!empty($pembelian['berat_kotor'])) ? monefy($pembelian['berat_kotor'], false) : '-'}}</label>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Berat Muatan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                @if($pembelian['berat_kosong'] != 0 && $pembelian['berat_netto'] != 0)
                                    <label class="form-control">{{monefy($pembelian['berat_netto'], false)}}</label>
                                @else
                                    <label class="form-control tx-gray-600"><i>Menunggu Update</i></label>
                                @endif
                                <div class="input-group-append">
                                    <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Timbangan Besar</h6>
        @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <a href="{{base_url('pembelian/gabah/quality_control/form/'.$pembelian['id_batch'].'?sap='.$sap)}}"><button class="btn btn-primary pos-absolute r-5" type="button">+ Tambah Data</button></a>
        @endif
    </div>
    <div id="toolbar" class="mg-y-10 mg-x-10">
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-striped table-white w-100" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">SAP</th>
                    <th data-sortable="true">Gabah</th>
                    <th data-sortable="true">Padi</th>
                    <th data-sortable="true">Pengeringan</th>
                    <th data-sortable="true">Proses</th>
                    <th data-sortable="true">Kadar Air</th>
                    <th data-sortable="true">Jumlah Sak</th>
                    <th data-sortable="true">Berat KS</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($detail))
                    <?php 
                        if(!empty($detail_kecil)){
                            $timbang_kecil = $detail_kecil[0]['berat_netto'];
                        }else{
                            $timbang_kecil = 0;
                        }
                        $total_sak = 0;
                        $total_ks = 0;
                        foreach($detail as $a)
                        {
                            $total_sak += $a['jumlah_sak'];
                        }

                        $berat_netto_sisa = $pembelian['berat_netto'] - $timbang_kecil;
                    ?>
                    @foreach ($detail as $row)
                        <tr>
                            <td nowrap>
                                @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                                <a href="{{base_url('pembelian/gabah/quality_control/edit_form/'.$row['id_batch'].'?sap='.$row['sap'])}}"><button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button></a>
                                
                                <a href="{{base_url('pembelian/gabah/quality_control/cetak_label/'.$row['id'])}}" target="_blank"><button type="button" class="btn btn-info" id="print" data-id="{{$row['id']}}"><i class="fa fa-print"></i></button></a>
                                <!-- <a href="{{base_url('pembelian/gabah/quality_control/cetak_label_portrait/'.$row['id'])}}" target="_blank"><button type="button" class="btn btn-info" id="print" data-id="{{$row['id']}}"><i class="fa fa-print"></i> Cetak-P</button></a> -->
                                <button type="button" class="btn btn-danger" onclick="hapus(this)" data-id="{{$row['id']}}" data-sap="{{$row['sap']}}"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                            <td>{{$row['sap']}}</td>
                            <td>{{strtoupper($row['jenis_gabah'])}}</td>
                            <td>{{$row['padi']}}</td>
                            <td>{{ucwords($row['pengeringan'])}}</td>
                            <td>{{ucwords($row['proses'])}}</td>
                            <td>{{$row['kadar_air_label']}}</td>
                            <td>{{$row['jumlah_sak']}}</td>
                            @if($pembelian['berat_kosong'] != 0 && $pembelian['berat_netto'] != 0)
                            <td>
                                <?php 
                                    $berat_ks = $row['jumlah_sak'] / $total_sak * ((100 - $row['kadar_air']) / 100) * $berat_netto_sisa;
                                    $total_ks += $berat_ks;
                                    echo monefy($berat_ks, false);
                                ?>
                            </td>
                            @else
                            <td><label class="badge badge-secondary">Menunggu update</label></td>
                            @endif
                            <td>{{$row['username_input']}}<br><label class="badge badge-light">{{$row['timestamp']}}</label></td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
        <hr>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Timbangan Kecil</h6>
        @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <a href="{{base_url('pembelian/gabah/quality_control/form_manual/'.$pembelian['id_batch'])}}"><button class="btn btn-primary pos-absolute r-5" type="button">+ Tambah Data</button></a>
        @endif
    </div>
    <div class="card-body">
        <h4>Timbangan Kecil</h4>
        <div class="table-responsive">
        <table class="table table-striped mg-t-10 table-white" id="tabel_manual">
            <thead>
                <tr>
                    <th>Aksi</th>
                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Berat Total</th>
                    <!-- <th data-sortable="true">Hampa (%)</th> -->
                </tr>
            </thead>
            <tbody>
            @if(!empty($detail_kecil))
                @foreach($detail_kecil as $row)
                    <tr>
                        <td>
                            <a href="{{base_url('pembelian/gabah/quality_control/cetak_label/'.$row['id_jenis'])}}" target="_blank"><button type="button" class="btn btn-primary" id="print" data-id="{{$row['id_jenis']}}"><i class="fa fa-print"></i> </button></a>

                            <button type="button" class="btn btn-danger" onclick="hapus_jenis(this)" data-id="{{$row['id_jenis']}}"><i class="fa fa-trash"></i></button>

                        </td>
                        <td>{{strtoupper($row['jenis_gabah'])}}</td>
                        <td>{{$row['padi']}}</td>
                        <!-- <td> -->
                            <input type="hidden" name="jenis_gabah" value="{{$row['jenis_gabah']}}">
                            <input type="hidden" name="id_jenis_padi" value="{{$row['id_jenis_padi']}}">
                            <!-- <input type="number" class="form-control jml-sak" name="jml_sak" value="{{$row['jumlah_sak']}}" readonly=""> -->
                        <!-- </td> -->
                        <td>
                            <input type="number" step="any" value="{{$row['total_berat_basah']}}" class="form-control" readonly="">
                        </td>
                        <!-- <td>
                            <input type="number" step="any" value="{{$row['hampa']}}" name="hampa" class="form-control broken">
                        </td> -->
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/submit_pembelian_gabah')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data Pembelian</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Supplier <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_supplier" class="form-control" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($supplier as $row)
                                    <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Pilih Resi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="resi_pengiriman" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <option value="0">Tanpa Resi</option>
                                    @foreach ($resi as $row)
                                    <option value="{{$row}}">{{$row}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Plat Kendaraan<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="plat_kendaraan" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="tanggal_pembelian" value="" autocomplete="off" required="">
                            </div>
                        </div>
                        <hr>

                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Bruto (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_bruto" value="0" autocomplete="off">
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Kosong (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_kosong" value="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Potongan /Sak<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="potong_sak" value="0.5" autocomplete="off">
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Potongan Lainnya (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="potong_lain" value="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Muatan<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_netto" value="0" autocomplete="off" readonly="">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});





function hapus(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('api/internal/pembelian/gabah/delete_data_jenis')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                        'sap' : $(el).data().sap
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function hapus_jenis(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('api/internal/pembelian/gabah/delete_data_jenis_kecil')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                        // 'sap' : $(el).data().sap
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}


</script>
@end