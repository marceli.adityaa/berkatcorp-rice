@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/quality_control?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pembelian Gabah</h6>
    </div>
    <div class="card-body">
        <table class="table mg-t-10 table-white w-100" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Resi/ Plat Nomor</th>
                    <th data-sortable="true">Berat Bruto</th>
                    <th data-sortable="true" data-searchable="false">Bongkar Muatan</th>
                    <th data-sortable="true" data-searchable="false">Input Harga</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($pembelian)){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('pembelian/gabah/quality_control/batch/'.$row['id'])."'><button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='left' title='Pilih'><i class='icon-control-play'></i></button></a>";
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode_pembelian']."</label></td>";
                            echo "<td>".$row['tanggal_pembelian']."</td>";
                            echo "<td>".$row['supplier']."</td>";
                            if(!empty($row['resi_kendaraan'])){
                                echo "<td><label class='badge badge-light'>".$row['resi_kendaraan']."</label><br>
                                <label class='badge badge-light'>".$row['plat_kendaraan']."</label>
                                </td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['plat_kendaraan']."</label></td>";
                            }               
                            
                            echo "<td>".monefy($row['berat_bruto'], false)."</td>";
                            
                            if($row['status_muatan'] == 1){
                                echo "<td><label class='badge badge-success'>OK</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Dalam Proses</label></td>";
                            }

                            if($row['status_selesai'] == 1){
                                echo "<td><label class='badge badge-success'>OK</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['username_input']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/kelola/submit_pembelian_gabah')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data Pembelian</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Supplier <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_supplier" class="form-control" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($supplier as $row)
                                    <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Pilih Resi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="resi_pengiriman" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <option value="0">Tanpa Resi</option>
                                    @foreach ($resi as $row)
                                    <option value="{{$row}}">{{$row}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Plat Kendaraan<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="plat_kendaraan" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="tanggal_pembelian" value="" autocomplete="off" required="">
                            </div>
                        </div>
                        <hr>

                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Bruto (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_bruto" value="0" autocomplete="off">
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Kosong (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_kosong" value="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Potongan /Sak<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="potong_sak" value="0.5" autocomplete="off">
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Potongan Lainnya (KG)<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="potong_lain" value="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Netto<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="berat_netto" value="0" autocomplete="off" readonly="">
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});



function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=id_supplier]').val(result.id_supplier).change();
                $('[name=resi_pengiriman]').val(result.resi_pengiriman).change();
                $('[name=plat_kendaraan]').val(result.plat_kendaraan);
                $('[name=tanggal_pembelian]').val(result.tanggal_pembelian);
                $('[name=berat_bruto]').val(result.berat_bruto);
                $('[name=berat_kosong]').val(result.berat_kosong);
                $('[name=potong_sak]').val(result.potong_sak);
                $('[name=potong_lain]').val(result.potong_lain);
                $('[name=berat_netto]').val(result.berat_netto);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end