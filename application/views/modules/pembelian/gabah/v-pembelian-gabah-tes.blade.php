@layout('commons/index')

@section('content')
<style>
    .nowrap{
        white-space:nowrap;
    }
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/quality_control/buku_tes?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && $this->input->get('status') == $key){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Tes</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table mg-t-10 table-white w-100" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false" >Aksi</th>
                    <th data-searchable="false">Status</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Tanggal Pembelian</th>
                    <th data-sortable="true">Tanggal Tes</th>
                    <th data-sortable="true">Waktu Tes</th>
                    <th data-sortable="true">Petugas</th>
                    <th data-sortable="true">Timbang</th>
                    <th data-sortable="true">Tes Gabah</th>
                    <th data-sortable="true">Hasil</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $temp2 = '';
                ?>
                    @if(!empty($tes))
                        @foreach($tes as $row)
                            @if($row['hasil_akhir'] == 0)
                                <?php $hasil = '<label class="badge badge-secondary">Menunggu</label>';?>
                            @else
                                <?php $hasil = '<label class="badge badge-light tx-14">'.$row['hasil_akhir'].'</label>%';?>
                            @endif

                            <td>{{$no++}}</td>
                            <td class="nowrap">
                                @if($row['hasil_akhir'] == 0 && $row['is_deleted'] == 0)
                                    <a href="{{base_url('pembelian/gabah/quality_control/detail_tes/'.$row['id'])}}"><button type="button" class="btn btn-info">Detail</button></a>
                                    <button type="button" class="btn btn-danger" title="hapus data?" onclick="hapus(this)" data-id="{{$row['id']}}"><i class="fa fa-trash"></i></button>
                                @elseif($row['hasil_akhir'] == 0 && $row['is_deleted'] == 1)
                                    <button type="button" class="btn btn-warning" title="aktifkan data?" onclick="aktif(this)" data-id="{{$row['id']}}"><i class="fa fa-refresh"></i></button>
                                @else 
                                    <a href="{{base_url('pembelian/gabah/quality_control/detail_tes/'.$row['id'])}}"><button type="button" class="btn btn-info">Detail</button></a>
                                @endif
                            </td>
                            <td>
                                @if($row['hasil_akhir'] == 0 && $row['is_deleted'] == 0)
                                <label class="badge badge-secondary">Menunggu</label>
                                @elseif($row['hasil_akhir'] == 0 && $row['is_deleted'] == 1)
                                <label class="badge badge-danger">Dihapus</label>
                                @else 
                                <label class="badge badge-success">Selesai</label>
                                @endif
                            </td>
                            <td><label class="badge badge-light">{{$row['kode_pembelian']}}</label></td>
                            <td><label class="badge badge-light">{{$row['supplier']}}</label></td>
                            <td><label class="badge badge-light">{{$row['tanggal_pembelian']}}</label></td>
                            <td><label class="badge badge-light">{{$row['tanggal_tes']}}</label></td>
                            <td><label class="badge badge-light">{{date("H:i", strtotime($row['jam_mulai'])).' - '.date("H:i", strtotime($row['jam_selesai']))}}</label></td>
                            <td><label class="badge badge-light">{{$row['petugas']}}</label></td>
                            <td><label class="badge badge-light">#{{$row['batch']}}</label></td>
                            <td>
                            @if ($row['jenis_gabah'] == 'ks')
                                <label class="badge badge-info">{{strtoupper($row['jenis_gabah'])}}</label> | {{$row['padi']}}
                            @else 
                                <label class="badge badge-warning">{{strtoupper($row['jenis_gabah'])}}</label> | {{$row['padi']}}
                            @endif
                            </td>
                            <td><b>{{$hasil}}</b></td>
                            </tr>
                        @endforeach
                    @endif
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Pembelian Gabah</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="">
                <div class="form-layout form-layout-4">
                    <table class="table table-white" id="tabel_data">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-searchable="false">Aksi</th>
                                <th data-sortable="true">Kode</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Supplier</th>
                                <th data-sortable="true">Jenis Gabah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1;
                                $temp = '';
                            ?>
                            @if(!empty($pembelian))
                                @foreach ($pembelian as $row)
                                    @if(empty($temp))
                                        <?php $temp = $row['id'];?>
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td class="text-nowrap">
                                                <button type="button" class="btn btn-primary" onclick="tambah_tes(this)" data-id="{{$row['id']}}">+ Tambah</button>
                                            </td>
                                            <td><label class="badge badge-light">{{$row['kode_pembelian']}}</label></td>
                                            <td><label class="badge badge-light">{{$row['tanggal_pembelian']}}</label></td>
                                            <td><label class="badge badge-light">{{$row['supplier']}}</label></td>
                                            <td>
                                                <label class="badge badge-light">#{{$row['batch']}}</label>
                                                @if ($row['jenis_gabah'] == 'ks')
                                                    <label class="badge badge-info">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                                @else 
                                                    <label class="badge badge-warning">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                                @endif
                                    @else
                                        @if($temp == $row['id'])
                                            @if ($row['jenis_gabah'] == 'ks')
                                            <br>
                                                <label class="badge badge-light">#{{$row['batch']}}</label>
                                                <label class="badge badge-info">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                            @else 
                                            <br>
                                                <label class="badge badge-light">#{{$row['batch']}}</label>
                                                <label class="badge badge-warning">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                            @endif
                                        @else
                                            <?php $temp = $row['id']?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="tambah_tes(this)" data-id="{{$row['id']}}">+ Tambah</button>
                                                </td>
                                                <td><label class="badge badge-light">{{$row['kode_pembelian']}}</label></td>
                                                <td><label class="badge badge-light">{{$row['tanggal_pembelian']}}</label></td>
                                                <td><label class="badge badge-light">{{$row['supplier']}}</label></td>
                                                <td>
                                                    <label class="badge badge-light">#{{$row['batch']}}</label>
                                                    @if ($temp == $row['id'])
                                                        @if ($row['jenis_gabah'] == 'ks')
                                                            <label class="badge badge-info">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                                        @else 
                                                            <label class="badge badge-warning">{{strtoupper($row['jenis_gabah'])}}</label> | <b>{{$row['padi']}}</b>
                                                        @endif
                                                    @endif
                                        @endif
                                    @endif
                                    
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('#tabel_data').bootstrapTable({
        pagination: true,
        search: true,
    });
    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.select2').select2();
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});

function tambah_tes(e){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menambahkan data berikut?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var url = "<?= base_url('pembelian/gabah/quality_control/init_data_tes?')?>"+'id='+$(e).data().id;
            window.location.href = url;
        }
    })
}

function hapus(el){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan dihapus, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('pembelian/gabah/quality_control/hapus_data_tesgabah')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}

function aktif(el){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan diaktifkan kembali, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('pembelian/gabah/quality_control/aktivasi_data_tesgabah')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end