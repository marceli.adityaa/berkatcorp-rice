@layout('commons/index')

@section('content')
<style>
    #tabel_jenis_ks, #tabel_jenis_kg input{
        min-width:100px;
    }
    th:first-child, td:first-child
    {
    position:sticky;
    left:0px;
    background-color:grey;
    }
    .hidden{
        visibility: hidden;
    }
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail ID Pembelian : {{$pembelian['kode_pembelian']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show mg-b-10" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/quality_control?')?>">
                    <div class="row">
                        <div class="col-6">
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Supplier</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <label class="form-control">{{$pembelian['supplier']}}</label>
                                </div>
                            </div>
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <label class="form-control">{{$pembelian['tanggal_pembelian']}}</label>
                                </div>
                            </div>
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Plat Kendaraan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <label class="form-control">{{$pembelian['plat_kendaraan']}}</label>
                                </div>
                            </div>
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Berat Bruto (KG)</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <label class="form-control">{{(!empty($pembelian['berat_bruto'])) ? monefy($pembelian['berat_bruto'], false) : '-'}}</label>
                                </div>
                            </div>
                            @if($pembelian['status_selesai'] == 1)
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Status Pengajuan</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @if($pembelian['status_persetujuan'] == 1)
                                        <label class="badge badge-success">Disetujui</label>
                                    @elseif($pembelian['status_persetujuan'] == 2)
                                        <label class="badge badge-success">Ditolak</label>
                                    @else
                                        <label class="badge badge-secondary">Menunggu Persetujuan</label>
                                        @if($pembelian['status_persetujuan'] != 1)
                                            <button type="button" class="btn btn-danger btn-sm float-right" onclick="batal_pengajuan()"><i class="fa fa-times"></i> Batalkan</button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            @if($pembelian['is_tempo'] == 1)
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label">Status Pembayaran</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @if($pembelian['is_tempo_dibayar'] == 1)
                                        <label class="badge badge-success">Sudah dibayar</label>
                                    
                                    @else
                                        <label class="badge badge-warning">Jatuh tempo {{$pembelian['tgl_tempo']}}</label>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @endif
                        </div>
                        <div class="col-6">
                            <div class="row col-12 my-3">
                                <label class="col-sm-4 form-control-label">Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($pembelian['is_tempo'] == 1)
                                        <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input" checked="">
                                        @else
                                        <input type="radio" id="tempo-1" name="is_tempo" value="1" class="custom-control-input">
                                        @endif
                                        <label class="custom-control-label" for="tempo-1">Tempo</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($pembelian['is_tempo'] == 0)
                                        <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input" checked="">
                                        @else
                                        <input type="radio" id="tempo-2" name="is_tempo" value="0" class="custom-control-input">
                                        @endif
                                        <label class="custom-control-label" for="tempo-2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-12 my-3 tempo">
                                <label class="col-sm-4 form-control-label">Tanggal Tempo</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="date" name="tgl_tempo" class="form-control tempo" value="{{$pembelian['tgl_tempo']}}">
                                </div>
                            </div>
                            <div class="row col-12 my-3 langsung">
                                <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    @foreach ($pembayaran as $key => $row)
                                    <div class="custom-control custom-radio custom-control-inline">
                                        @if($pembelian['jenis_pembayaran'] == $row)
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input" checked>
                                        @else
                                        <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input">
                                        @endif

                                        @if($row == 'tbd')
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                        @else
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row col-12 my-3 langsung cash">
                                <label class="col-sm-4 form-control-label">Sumber Kas</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_kas" class="form-control select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($kas as $row)
                                            @if($pembelian['id_kas'] == $row->id)
                                            <option value="{{$row->id}}" selected="">{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-12 my-3 langsung transfer">
                                <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_rek_pengirim" class="form-control select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($rek_pengirim as $row)
                                            @if($pembelian['id_rek_pengirim'] == $row->id)
                                            <option value="{{$row->id}}" selected>{{$row->label}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->label}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-12 my-3 langsung transfer">
                                <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select name="id_rek_penerima" class="form-control select3">
                                        <option value="">- Pilih Salah Satu -</option>
                                        @foreach ($rek_penerima as $row)
                                            @if($pembelian['id_rek_penerima'] == $row->id)
                                            <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @else
                                            <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-12 my-2">
                                <label class="col-sm-4 form-control-label tx-20 tx-bold">Grandtotal</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                        </div>
                                        <label class="form-control tx-30 tx-bold" aria-describedby="basic-addon1" id="grandtotal">{{monefy($pembelian['grandtotal'], false)}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @if(($pembelian['status_selesai'] != 1 && in_array('super', $role) || in_array('pembelian', $role)))
                            <button type="button" class="btn btn-success btn-lg" onclick="submit_form()"><i class="fa fa-send"></i> &nbsp;Selesai</button>
                            @endif
                            <button type="button" class="btn btn-warning float-right" id="cetak"><i class="fa fa-print"></i> &nbsp;Cetak</button>
                            @if(($pembelian['status_selesai'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                            <button type="button" class="btn btn-primary float-right mg-r-2" onclick="update_form(1)"><i class="fa fa-save"></i> &nbsp;Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">DATA PEMBELIAN GABAH KS</h6>
        @if(($pembelian['status_selesai'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <button class="btn btn-info pos-absolute r-5" type="button" onclick="update_form(1)"><i class="fa fa-save"></i> Simpan</button>
        @endif
    </div>
    <div class="card-body">
        <table class="table table-striped mg-t-10 table-white table-responsive" id="tabel_jenis_ks">
            <thead>
                <tr>
                    <th data-sortable="true">Timbangan</th>
                    <th data-sortable="true">Jumlah Sak</th>
                    <th data-sortable="true">Potong/ Sak</th>
                    <th data-sortable="true">Potongan Lain</th>
                    <th data-sortable="true">Hampa (%)</th>
                    <th data-sortable="true">Berat Muatan</th>
                    <th data-sortable="true">Berat Gabah</th>
                    <th data-sortable="true">Berat Netto</th>
                    <th data-sortable="true">Harga Nol Gabuk</th>
                    <th data-sortable="true">Tambahan</th>
                    <th data-sortable="true">Total</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($ks_jenis))
            <?php $total_ks = 0;?>
                @foreach ($ks_jenis as $row)
                    <tr>
                        <td nowrap>
                            <input type="hidden" name="id_jenis" value="{{$row['id_jenis']}}">
                            <label class="badge badge-light">#{{$row['batch']}} - {{$row['batch_label']}}</label><br>
                            <label class="badge badge-primary">{{strtoupper($row['jenis_gabah'])}}</label> <label class="badge badge-warning">{{$row['padi']}}</label><br>
                            @if($row['is_manual'] == 1)
                                <label class="badge badge-info">Manual</label>
                            @endif
                        </td>
                        <td>
                            <input type="hidden" name="jenis_gabah" value="{{$row['jenis_gabah']}}">
                            <input type="hidden" name="id_jenis_padi" value="{{$row['id_jenis_padi']}}">
                            <input type="number" class="form-control jml-sak" name="jml_sak" value="{{$row['jumlah_sak']}}" readonly=""></td>
                        <td>
                            <input type="number" step="any" value="{{$row['potong_sak']}}" name="potong_sak" class="form-control potong">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$row['potong_lain']}}" name="potong_lain" class="form-control">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$row['hampa']}}" name="hampa" class="form-control">
                        </td>
                            <?php 
                                $berat_gabah = 0;
                                $berat_bruto = 0;
                                foreach($total_sak as $t){
                                    if($row['id_batch'] == $t['id_batch']){
                                        $berat_bruto = $row['jumlah_sak'] / $t['jumlah'] * $t['berat_netto'];
                                        if($row['is_manual'] == 0){
                                            $berat_gabah += round($row['jumlah_sak'] / $ks_total_berat_standar[$row['id_batch']]['sak'] * $ks_total_berat_standar[$row['id_batch']]['value']);
                                        }else{
                                            $berat_gabah += round($row['jumlah_sak'] / $ks_total_berat_manual[$row['id_batch']]['sak'] * $ks_total_berat_manual[$row['id_batch']]['value']);
                                        }
                                    }
                                }
                            ?>
                        <td>
                            <input type="number" step="any" value="{{round($berat_bruto)}}" name="berat_muatan" class="form-control" readonly="">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{round($berat_gabah)}}" name="berat_gabah" class="form-control" readonly="" data-toggle="tooltip" title="Potongan KA : {{round($berat_bruto - $berat_gabah)}}">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$netto = round(($berat_gabah - (($row['potong_sak'] * $row['jumlah_sak']) + $row['potong_lain'])) * ((100-$row['hampa'])/100))}}" name="berat_netto" class="form-control" readonly="">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$row['harga_nol_gabuk']}}" name="harga_nol_gabuk" class="form-control">
                        </td>
                        <td>
                            <input type="text" step="any" value="{{$row['tambahan']}}" name="tambahan" class="form-control autonumeric">
                        </td>
                        <td>
                            <input type="hidden" name="total_ks" value="{{round(($netto * $row['harga_nol_gabuk']) + $row['tambahan'], 0)}}">
                            <input type="text" name="format_total_ks" value="{{monefy(round(($netto * $row['harga_nol_gabuk']) + $row['tambahan'], 0), false)}}" class="form-control" readonly="">
                        </td>
                        <?php 
                            $total_ks += ($netto * $row['harga_nol_gabuk']) + $row['tambahan'];
                        ?>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @if (!empty($total_ks))
        <h3 class="float-right"><small>Total</small> Rp <label id="total_ks">{{monefy($total_ks, false)}}</label></h2>
        @else
            <label id="total_ks" class="hidden">0</label>
        @endif
    </div>
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">DATA PEMBELIAN GABAH KG</h6>
        @if(($pembelian['status_selesai'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <button class="btn btn-info pos-absolute r-5" type="button" onclick="update_form(1)"><i class="fa fa-save"></i> Simpan</button>
        @endif
    </div>
    <div class="card-body">
        <table class="table table-striped mg-t-10 table-white table-responsive" id="tabel_jenis_kg">
            <thead>
                <tr>
                    <th data-sortable="true">Timbangan</th>
                    <th data-sortable="true">Jumlah Sak</th>
                    <th data-sortable="true">Potong/ Sak</th>
                    <th data-sortable="true">Potongan Lain</th>
                    <th data-sortable="true">Berat Muatan</th>
                    <th data-sortable="true">Berat Gabah</th>
                    <th data-sortable="true">Berat Netto</th>
                    <th data-sortable="true">Harga KG</th>
                    <th data-sortable="true">Tambahan</th>
                    <th data-sortable="true">Total</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($kg_jenis))
            <?php $total_kg = 0;?>
                @foreach ($kg_jenis as $row)
                    <tr>
                        <td nowrap>
                            <input type="hidden" name="id_jenis" value="{{$row['id_jenis']}}">
                            <label class="badge badge-light">#{{$row['batch']}} - {{$row['batch_label']}}</label><br>
                            <label class="badge badge-primary">{{strtoupper($row['jenis_gabah'])}}</label> <label class="badge badge-warning">{{$row['padi']}}</label><br>
                            @if($row['is_manual'] == 1)
                                <label class="badge badge-info">Manual</label>
                            @endif
                        </td>
                        <td>
                            <input type="hidden" name="jenis_gabah" value="{{$row['jenis_gabah']}}">
                            <input type="hidden" name="id_jenis_padi" value="{{$row['id_jenis_padi']}}">
                            <input type="number" class="form-control jml-sak" name="jml_sak" value="{{$row['jumlah_sak']}}" readonly=""></td>
                        <td>
                            <input type="number" step="any" value="{{$row['potong_sak']}}" name="potong_sak" class="form-control potong">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$row['potong_lain']}}" name="potong_lain" class="form-control">
                        </td>
                        <?php 
                            $berat_gabah = 0;
                            $berat_bruto = 0;
                            foreach($total_sak as $t){
                                if($row['id_batch'] == $t['id_batch']){
                                    $berat_bruto = $row['jumlah_sak'] / $t['jumlah'] * $t['berat_netto'];
                                    if($row['is_manual'] == 0){
                                        $berat_gabah += round($row['jumlah_sak'] / $kg_total_berat_standar[$row['id_batch']]['sak'] * $kg_total_berat_standar[$row['id_batch']]['value']);
                                    }else{
                                        $berat_gabah += round($row['jumlah_sak'] / $kg_total_berat_manual[$row['id_batch']]['sak'] * $kg_total_berat_manual[$row['id_batch']]['value']);
                                    }
                                }
                            }
                        ?>
                        <td>
                            <input type="number" step="any" value="{{round($berat_bruto)}}" name="berat_muatan" class="form-control" readonly="">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{round($berat_gabah)}}" name="berat_gabah" class="form-control" readonly="" data-toggle="tooltip" title="Potongan KA : {{round($berat_bruto - $berat_gabah)}}">
                        </td>
                        <td>
                            <input type="text" step="any" value="{{$netto = round($berat_gabah - (($row['potong_sak'] * $row['jumlah_sak']) + $row['potong_lain']))}}" name="berat_netto" class="form-control" readonly="">
                        </td>
                        <td>
                            <input type="number" step="any" value="{{$row['harga_kg']}}" name="harga_kg" class="form-control">
                        </td>
                        
                        <td>
                            <input type="text" step="any" value="{{$row['tambahan']}}" name="tambahan" class="form-control autonumeric">
                        </td>
                        <td>
                            <input type="hidden" name="total_kg" value="{{round(($netto * $row['harga_kg']) + $row['tambahan'])}}">
                            <input type="text" name="format_total_kg" value="{{monefy(round(($netto * $row['harga_kg']) + $row['tambahan']), false)}}" class="form-control" readonly="">
                        </td>
                        <?php 
                            $total_kg += ($netto * $row['harga_kg']) + $row['tambahan'];
                        ?>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @if(!empty($total_kg))
        <h3 class="float-right"><small>Total</small> Rp <label id="total_kg">{{monefy($total_kg, false)}}</label></h2>
        @else
        <label id="total_kg" class="hidden">0</label>
        @endif
    </div>
</div>

@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center" role="document" style="min-width:30%">
        <div class="modal-content">
            <form action="#">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Cetak Nota Pembelian</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="id_pembelian" value="{{$pembelian['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-6 form-control-label">Pilih Pembelian</label>
                            <div class="col-sm-6 mg-y-10">
                                <div class="col-sm-12 mg-y-10">
                                    <div class="custom-control custom-checkbox mg-y-5">
                                        <input type="checkbox" class="custom-control-input" id="check-gabah-ks" name="pilih_pembelian" value="ks">
                                        <label class="custom-control-label" for="check-gabah-ks">Gabah KS</label>
                                    </div><br>
                                    <div class="custom-control custom-checkbox mg-y-5">
                                        <input type="checkbox" class="custom-control-input" id="check-gabah-kg" name="pilih_pembelian" value="kg">
                                        <label class="custom-control-label" for="check-gabah-kg">Gabah KG</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-2">
                            <label class="col-sm-6 form-control-label">Dibayar oleh</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="dibayar">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        @if($pembelian['dibayar_oleh'] == $row['id'])
                                            <option value="{{$row['id']}}" selected>{{ucwords($row['nama'])}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row my-2">
                            <label class="col-sm-6 form-control-label">Checker </label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="checker">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($pic as $row)
                                        @if($pembelian['checker'] == $row['id'])
                                            <option value="{{$row['id']}}" selected>{{ucwords($row['nama'])}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row help-cetak">
                            <div class="col-sm-12">
                                <p class="text-muted tx-11" style="color:red !important">Harap pilih data yang akan dicetak.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-cetak"><i class="fa fa-print mr-2"></i>Cetak</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.set-tooltip').tooltip({ boundary: 'window' });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });

    $('.select3').select2();
    $('.help-cetak').hide();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_tempo();
    cek_metode_pembayaran();
});

$('#modal-cetak').on('shown.bs.modal', function(e) {
});

$('#cetak').click(function() {
    $('.help-cetak').hide();
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});

$("#tabel_jenis_ks [name=potong_sak], #tabel_jenis_ks [name=potong_lain], #tabel_jenis_ks [name=hampa], #tabel_jenis_ks [name=harga_nol_gabuk], #tabel_jenis_ks [name=tambahan]").on('change keyup', function(a,b){
    let berat_netto = hitung_berat_netto(this);
    let tambahan = parseInt($(this).closest('tr').find('[name=tambahan]').autoNumeric('get'));
    let total_ks = Math.round((berat_netto * getNum(parseInt($(this).closest('tr').find('[name=harga_nol_gabuk]').val()))) + tambahan);
    $(this).closest('tr').find('[name=berat_netto]').val(round(berat_netto, 1));
    $(this).closest('tr').find('[name=total_ks]').val(total_ks);
    $(this).closest('tr').find('[name=format_total_ks]').val(total_ks.toLocaleString());

    hitung_total_ks();
    hitung_grandtotal();
});

// $("#tabel_jenis_ks [name=tambahan]").on('change keyup', function(a,b){
//     if($(this).closest('tr').find('[name=tambahan]').val() != ""){
//         let berat_netto = hitung_berat_netto(this);
//         let total = parseInt($(this).closest('tr').find('[name=total_ks]').val());
//         let tambahan = parseInt($(this).closest('tr').find('[name=tambahan]').val());
//         let total_baru = total + tambahan;
//         let harga = total_baru / berat_netto;
//         $(this).closest('tr').find('[name=harga_nol_gabuk]').val(harga);
//         $(this).closest('tr').find('[name=total_ks]').val(total_baru);
//         $(this).closest('tr').find('[name=format_total_ks]').val(total_baru.toLocaleString());
//     }  
// });


$("#tabel_jenis_kg [name=potong_sak], #tabel_jenis_kg [name=potong_lain], #tabel_jenis_kg [name=harga_kg], #tabel_jenis_kg [name=tambahan]").on('change keyup', function(a,b){
    let berat_netto = hitung_berat_netto(this);
    let tambahan = parseInt($(this).closest('tr').find('[name=tambahan]').autoNumeric('get'));
    let harga_kg = getNum(parseInt($(this).closest('tr').find('[name=harga_kg]').val()));
    let total_kg = (berat_netto * harga_kg) + tambahan;
    $(this).closest('tr').find('[name=berat_netto]').val(round(berat_netto, 1));
    $(this).closest('tr').find('[name=total_kg]').val(total_kg);
    $(this).closest('tr').find('[name=format_total_kg]').val(total_kg.toLocaleString());

    hitung_total_kg();
    hitung_grandtotal();
})

$("#btn-cetak").click(function(){
    if($('[name=pilih_pembelian]').is(':checked')){
        send_data(1, 1);        
    }else{
        $('.help-cetak').show();
    }
})

function hitung_berat_netto(el){
    let potong = getNum(parseFloat($(el).closest('tr').find('[name=potong_sak]').val()));
    let potong_lain = getNum(parseInt($(el).closest('tr').find('[name=potong_lain]').val()));
    let sak = getNum(parseInt($(el).closest('tr').find('[name=jml_sak]').val()));
    let berat_gabah = getNum(parseInt($(el).closest('tr').find('[name=berat_gabah]').val()));
    let hampa = getNum(parseInt($(el).closest('tr').find('[name=hampa]').val()));
    return Math.round((berat_gabah - ((sak * potong) + potong_lain)) * ((100 - hampa)/100));
    
    
}

function hitung_total_ks(){
    let total_ks = 0;
    $('#tabel_jenis_ks tr').each(function(a, b) {
        if (a > 0) {
            total_ks += (getNum(parseInt($(b).find('[name=total_ks]').val())));
        }
    });
    $('#total_ks').html(total_ks.toLocaleString());
}

function hitung_total_kg(){
    let total_kg = 0;
    $('#tabel_jenis_kg tr').each(function(a, b) {
        if (a > 0) {
            total_kg += (getNum(parseInt($(b).find('[name=total_kg]').val())));
        }
    });
    $('#total_kg').html(total_kg.toLocaleString());
}

function hitung_grandtotal(){
    let ks = $('#total_ks').html();
    let kg = $('#total_kg').html();
    let grandtotal = convert_to_number(ks) + convert_to_number(kg);
    $('#grandtotal').html(grandtotal.toLocaleString());
}

$('[name=is_tempo], [name=jenis_pembayaran]').change(function(){
    cek_tempo();
    cek_metode_pembayaran();
});

function cek_tempo(){
    let is_tempo = $('[name=is_tempo]:checked').val();
    if(is_tempo == 1){
        $('.tempo').fadeIn();
        $('.not-tempo').hide();
    }else{
        $('.tempo').hide();
        $('.not-tempo').fadeIn();
    }
}

function cek_metode_pembayaran(){
    let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
    if(jenis_pembayaran == 'cash'){
        $('.cash').fadeIn();
        $('.transfer').hide();
    }else if(jenis_pembayaran == 'transfer'){
        $('.cash').hide();
        $('.transfer').fadeIn();
    }else{
        $('.cash').hide();
        $('.transfer').hide();
    }
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function convert_to_number(input){
    return Number(input.replace(/[^0-9.-]+/g,""))
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

function update_form(reload){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            send_data(reload, 0);
        }
    })    
}

function send_data(reload, print){
    
    var data_ks = [];
    var data_kg = [];
    var data_pembayaran = [];
    var status_selesai = "<?= $pembelian['status_selesai']?>";
    if(status_selesai == 0){
        $('#tabel_jenis_ks tr').each(function(a, b) {
            if (a > 0) {
                data_ks.push({
                    id_jenis: $(b).find("[name=id_jenis]").val(),
                    potong_sak: $(b).find("[name=potong_sak]").val(),
                    potong_lain: $(b).find("[name=potong_lain]").val(),
                    hampa: $(b).find("[name=hampa]").val(),
                    berat_beras_bruto: $(b).find("[name=berat_muatan]").val(),
                    berat_beras_netto: $(b).find("[name=berat_netto]").val(),
                    harga_nol_gabuk: $(b).find("[name=harga_nol_gabuk]").val(),
                    tambahan: $(b).find("[name=tambahan]").autoNumeric('get'),
                    subtotal : $(b).find("[name=total_ks]").val(),
                });
            }
        });

        $('#tabel_jenis_kg tr').each(function(a, b) {
            if (a > 0) {
                data_kg.push({
                    id_jenis: $(b).find("[name=id_jenis]").val(),
                    potong_sak: $(b).find("[name=potong_sak]").val(),
                    potong_lain: $(b).find("[name=potong_lain]").val(),
                    berat_beras_bruto: $(b).find("[name=berat_muatan]").val(),
                    berat_beras_netto: $(b).find("[name=berat_netto]").val(),
                    harga_kg: $(b).find("[name=harga_kg]").val(),
                    tambahan: $(b).find("[name=tambahan]").autoNumeric('get'),
                    subtotal : $(b).find("[name=total_kg]").val(),
                });
            }
        });
        
        data_pembayaran.push({
            is_tempo : $('[name=is_tempo]:checked').val(),
            tgl_tempo : $('[name=tgl_tempo]').val(),
            jenis_pembayaran : $('[name=jenis_pembayaran]:checked').val(),
            id_kas : $('[name=id_kas] option:selected').val(),
            id_rek_pengirim : $('[name=id_rek_pengirim]').find('option:selected').val(),
            id_rek_penerima : $('[name=id_rek_penerima]').find('option:selected').val(),
            dibayar_oleh : $('[name=dibayar]').val(),
            checker : $('[name=checker]').val(),
            grandtotal :convert_to_number($('#grandtotal').html())
        });

        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/submit_input_harga')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id_pembelian' : <?= $pembelian['id']?>,
                'data_ks': data_ks,
                'data_kg' : data_kg,
                'data_pembayaran' : data_pembayaran
            },
            success: function(result) {
                // window.location.href = result.redirect;
                if(print){
                    let str = "?";
                    $.each($("[name=pilih_pembelian]:checked"), function(a, b){
                        str += $(this).val()+"=true";
                        if(a < ($("[name=pilih_pembelian]:checked").length - 1)){
                            str += "&";
                        }
                    });
                    
                    str += "&dibayar="+$('[name=dibayar]').find(':selected').val()+"&checker="+$('[name=checker]').find(':selected').val()
                    let url = "{{base_url('pembelian/gabah/kelola/cetak_nota/'.$pembelian['id'])}}"+str;
                    var win = window.open(url);
                    if (win) {
                        win.focus();
                    } else {
                        alert('Please allow popups for this website');
                    }
                }
                if(reload){
                    reload_page();
                }

            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {
            }
        });
    }else{
        if(print){
            let str = "?";
            $.each($("[name=pilih_pembelian]:checked"), function(a, b){
                console.log(a);
                str += $(this).val()+"=true";
                if(a < ($("[name=pilih_pembelian]:checked").length - 1)){
                    str += "&";
                }
            });
            
            str += "&dibayar="+$('[name=dibayar]').find(':selected').val()+"&checker="+$('[name=checker]').find(':selected').val()
            let url = "{{base_url('pembelian/gabah/kelola/cetak_nota/'.$pembelian['id'])}}"+str;
            var win = window.open(url);
            if (win) {
                win.focus();
            } else {
                alert('Please allow popups for this website');
            }
        }else{
            Swal.fire("Info", "Sedang menunggu persetujuan pengajuan.", "warning");
        }
        
    }
}

function submit_form(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data akan diteruskan ke bagian Meja Utama, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            send_data(0,0);
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/pengajuan_persetujuan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : <?= $pembelian['id']?>,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}

function batal_pengajuan(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data pengajuan akan dibatalkan, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/pembatalan_persetujuan')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : <?= $pembelian['id']?>,
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end