@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail ID Pembelian : {{$pembelian['kode_pembelian']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <!-- <form method="get" action="<?= base_url('pembelian/gabah/quality_control?')?>"> -->
                <form>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['supplier']}}</label>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Pembelian</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['tanggal_pembelian']}}</label>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Plat Kendaraan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['plat_kendaraan']}}</label>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Berat Bruto</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <label class="form-control">{{(!empty($pembelian['berat_bruto'])) ? monefy($pembelian['berat_bruto'], false) : '-'}}</label>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="button" class="btn btn-success btn-lg" onclick="submit_form()"><i class="fa fa-send"></i> &nbsp;SUBMIT</button>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">DAFTAR TIMBANGAN</h6>
        @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <button class="btn btn-primary pos-absolute r-5" type="button" id="tambah">+ Tambah Data</button>
        @endif
    </div>
    <div id="toolbar" class="mg-y-10 mg-x-10">
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-striped mg-t-10 table-white w-100" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Timbangan</th>
                    <th data-sortable="true">Berat Bruto</th>
                    <th data-sortable="true">Berat Kosong</th>
                    <th data-sortable="true">Berat Muatan</th>
                    <th data-sortable="true">Jumlah Sak</th>
                    <th data-sortable="true">Jenis Sak</th>
                    <th data-sortable="true">Setor Sak</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($batch))
                @foreach ($batch as $row)
                    <tr>
                        <td nowrap>
                            <a href="{{base_url('pembelian/gabah/quality_control/transaksi/'.$row['id'])}}"><button class="btn btn-info set-tooltip" data-toggle="tooltip" data-placement="top" title="Detail"> Detail</button></a>
                            @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                            <button class="btn btn-warning set-tooltip" data-toggle="tooltip" data-placement="top" title="Edit" data-id="{{$row['id']}}" onclick="edit(this)"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-danger set-tooltip" data-toggle="tooltip" data-placement="top" title="Hapus" data-id="{{$row['id']}}" onclick="hapus(this)"><i class="fa fa-trash"></i></button>
                            @endif
                        </td>
                        <td>
                        <input type="hidden" name="id_batch" value="{{$row['id']}}">
                        <label class="badge badge-light">#{{$row['batch'].' '.$row['batch_label']}}</label>
                            @if($row['is_manual'] == 1)
                                <br>
                                <label class="badge badge-info">Manual</label>
                            @endif
                        </td>
                        <td>{{monefy($row['berat_kotor'], 0)}}</td>
                        <td>
                        @if($row['is_manual'] == 1)
                            {{monefy($row['berat_kotor'] - $row['berat_netto'], false)}}
                        @else
                            @if($row['berat_kosong'] != 0 && $row['berat_netto'] != 0)
                                {{monefy($row['berat_kosong'], false)}}
                            @else
                                <label class="tx-primary" data-id="{{$row['id']}}" onclick="edit(this)"><i>Menunggu Update</i></label>
                            @endif
                        @endif
                        </td>
                        <td>
                        @if($row['is_manual'] == 1)
                            {{monefy($row['berat_netto'], false)}}
                        @else
                            @if($row['berat_kosong'] != 0 && $row['berat_netto'] != 0)
                                {{monefy($row['berat_netto'], false)}}
                            @else
                                <label class="tx-primary" data-id="{{$row['id']}}" onclick="edit(this)"><i>Menunggu Update</i></label>
                            @endif
                        @endif
                        </td>
                        <td>
                        @if($row['is_manual'] == 1 && !empty(json_decode($row['sap_manual'])))
                            {{array_sum(json_decode($row['sap_manual']))}}
                        @else
                            {{monefy($row['sak'], false)}}
                        @endif
                        </td>
                        <td>
                            <select name="id_jenis_sak" class="form-control">
                                <option value="0">- Tidak -</option>
                                @foreach($jenis_sak as $a)
                                    @if($a['id'] == $row['id_jenis_sak'])
                                    <option value="{{$a['id']}}" selected>{{ucwords($a['nama'])}}</option>
                                    @else
                                    <option value="{{$a['id']}}">{{ucwords($a['nama'])}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <div class="custom-control custom-checkbox">
                                @if($row['is_record_supplier'] == 1)
                                <input type="checkbox" class="custom-control-input" id="sak-supplier-{{$row['id']}}" name="is_record_supplier" checked>
                                @else
                                <input type="checkbox" class="custom-control-input" id="sak-supplier-{{$row['id']}}" name="is_record_supplier">
                                @endif
                                <label class="custom-control-label" for="sak-supplier-{{$row['id']}}">+ Saldo Sak Supplier</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                @if($row['is_record_primary'] == 1)
                                <input type="checkbox" class="custom-control-input" id="sak-perusahaan-{{$row['id']}}" name="is_record_primary" checked>
                                @else
                                <input type="checkbox" class="custom-control-input" id="sak-perusahaan-{{$row['id']}}" name="is_record_primary">
                                @endif
                                <label class="custom-control-label" for="sak-perusahaan-{{$row['id']}}">+ Saldo Sak Perusahaan</label>
                            </div>
                        </td>
                        <td>
                            
                        @if($row['is_manual'] == 1)

                            @if($row['id_jenis_sak'] == 0 && $row['setor_sak'] == 0 && !empty(json_decode($row['sap_manual'])))
                            <input type="number" class="form-control" name="setor_sak" value="{{array_sum(json_decode($row['sap_manual']))}}">
                            @else
                            <input type="number" class="form-control" name="setor_sak" value="{{$row['setor_sak']}}">
                            @endif
                        @else
                            @if($row['id_jenis_sak'] == 0 && $row['setor_sak'] == 0)
                            <input type="number" class="form-control" name="setor_sak" value="{{$row['sak']}}">
                            @else
                            <input type="number" class="form-control" name="setor_sak" value="{{$row['setor_sak']}}">
                            @endif
                        @endif
                        </td>
                        <td>{{$row['username_input']}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">DATA JENIS PADI</h6>
        @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <button class="btn btn-info pos-absolute r-5" type="button" onclick="update()"><i class="fa fa-save"></i> Update</button>
        @endif
    </div>
    <div id="toolbar" class="mg-y-10 mg-x-10">
    </div>
    <div class="card-body">
        <h4>Timbangan Standar</h4>
        <div class="table-responsive">
        <table class="table table-striped mg-t-10 table-white" id="tabel_jenis">
            <thead>
                <tr>
                    <th>Timbangan</th>
                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Jumlah Sak</th>
                    <th data-sortable="true">Potong/ Sak</th>
                    <!-- <th data-sortable="true">Hampa (%)</th> -->
                    <th data-sortable="true">Total Potongan</th>
                </tr>
            </thead>
            <tbody>
            @if(!empty($jenis))
                @foreach ($jenis as $row)
                    <tr>
                        <td>   
                            <b>#{{$row['batch']}}</b> 
                            <label class="badge badge-light">{{$row['batch_label']}}</label>
                            <input type="hidden" name="id_batch" value="{{$row['id_batch']}}">
                        </td>
                        <td>{{strtoupper($row['jenis_gabah'])}}</td>
                        <td>{{$row['padi']}}</td>
                        <td>
                            <input type="hidden" name="jenis_gabah" value="{{$row['jenis_gabah']}}">
                            <input type="hidden" name="id_jenis_padi" value="{{$row['id_jenis_padi']}}">
                            <input type="number" class="form-control jml-sak" name="jml_sak" value="{{$row['jumlah_sak']}}" readonly=""></td>
                        <td>
                            <input type="number" step="any" value="{{$row['potong_sak']}}" name="potong_sak" class="form-control potong">
                        </td>
                        <!-- <td>
                            <input type="number" step="any" value="{{$row['hampa']}}" name="hampa" class="form-control">
                        </td> -->
                        <td>
                            <input type="text" name="total_potong" value="{{$row['potong_sak'] * $row['jumlah_sak']}}" class="form-control" readonly="">
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
        <hr>
        <h4>Timbangan Kecil</h4>
        <div class="table-responsive">
        <table class="table table-striped mg-t-10 table-white" id="tabel_manual">
            <thead>
                <tr>
                    <th data-sortable="true">Timbangan</th>
                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Berat total</th>
                    <!-- <th data-sortable="true">Hampa (%)</th> -->
                </tr>
            </thead>
            <tbody>
            @if(!empty($manual))
                @foreach ($manual as $row)
                    <tr>
                        <td>   
                            <b>#{{$row['batch']}}</b> 
                            <label class="badge badge-light">{{$row['batch_label']}}</label>
                            <input type="hidden" name="id_batch" value="{{$row['id_batch']}}">
                        </td>
                        <td>{{strtoupper($row['jenis_gabah'])}}</td>
                        <td>{{$row['padi']}}</td>
                        <!-- <td> -->
                            <input type="hidden" name="jenis_gabah" value="{{$row['jenis_gabah']}}">
                            <input type="hidden" name="id_jenis_padi" value="{{$row['id_jenis_padi']}}">
                            <!-- <input type="number" class="form-control jml-sak" name="jml_sak" value="{{$row['jumlah_sak']}}" readonly=""> -->
                        <!-- </td> -->
                        <td>
                            <input type="number" step="any" value="{{$row['total_berat_basah']}}" class="form-control" readonly="">
                        </td>
                        <!-- <td>
                            <input type="number" step="any" value="{{$row['hampa']}}" name="hampa" class="form-control broken">
                        </td> -->
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/quality_control/submit_pembelian_gabah_batch')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data Timbangan</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="id_pembelian" value="{{$pembelian['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Timbang ke <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="batch" value="{{$batch_idx}}" autocomplete="off" required="" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Label</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="batch_label" value="" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-4 form-control-label">Berat Bruto<span class="tx-danger"></span></label>
                            <div class="col-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <input type="number" class="form-control" name="berat_kotor" value="{{$sisa_bruto}}" autocomplete="off" min="0" readonly="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-4 form-control-label">Berat Kosong<span class="tx-danger"></span></label>
                            <div class="col-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <input type="number" class="form-control" name="berat_kosong" value="" autocomplete="off" min="0" readonly="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-4 form-control-label">Berat Muatan<span class="tx-danger"></span></label>
                            <div class="col-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <input type="number" class="form-control" name="berat_netto" value="" autocomplete="off" min="0" readonly="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="is_manual" value="1" name="is_manual">
                                    <label class="custom-control-label" for="is_manual">Hitung Manual</label>
                                </div>
                            </div>
                        </div> -->
                        

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('.set-tooltip').tooltip({ boundary: 'window' });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});

$("[name=berat_kotor], [name=berat_kosong]").on('keyup', function(){
    let kotor = getNum($("[name=berat_kotor]").val());
    let kosong = getNum($("[name=berat_kosong]").val());
    let netto = kotor - kosong;
    $("[name=berat_netto]").val(netto);
});

$("[name=berat_netto]").on('keyup', function(){
    let kotor = getNum($("[name=berat_kotor]").val());
    let netto = getNum($("[name=berat_netto]").val());
    let kosong = kotor - netto;
    $("[name=berat_kosong]").val(kosong);
});

$(".potong").on('change keyup', function(a,b){
    const parent = $(this).parent('tr');
    let potong = $(this, parent).val();
    let sak = $(this).closest('tr').find('[name=jml_sak]').val();
    let total = sak * potong;
    console.log(potong);
    $(this, parent).closest('tr').find('[name=total_potong]').val(total);
})

function update(){
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            var manual = [];
            var trx_sak = [];
            $('#tabel_pembelian tr').each(function(a, b) {
                var record_supplier = 0;
                var record_primary = 0;
                if (a > 0) {
                    if($("[name=is_record_supplier]", b).is(":checked")){
                        record_supplier = 1;
                    }
                    if($("[name=is_record_primary]", b).is(":checked")){
                        record_primary = 1;
                    }
                    trx_sak.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        id_jenis_sak: $("[name=id_jenis_sak]", b).val(),
                        setor_sak: $("[name=setor_sak]", b).val(),
                        is_record_supplier : record_supplier,
                        is_record_primary : record_primary
                    });
                }
            });

            $('#tabel_jenis tr').each(function(a, b) {
                if (a > 0) {
                    data.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        jenis_gabah: $("[name=jenis_gabah]", b).val(),
                        jenis_padi: $("[name=id_jenis_padi]", b).val(),
                        potong_sak: $("[name=potong_sak]", b).val(),
                    });
                }
            });

            $('#tabel_manual tr').each(function(a, b) {
                if (a > 0) {
                    manual.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        jenis_gabah: $("[name=jenis_gabah]", b).val(),
                        jenis_padi: $("[name=id_jenis_padi]", b).val(),
                    });
                }
            });
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/update_potongan_sak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'data': data,
                    'manual' : manual,
                    'trx_sak' : trx_sak
                },
                success: function(result) {
                    location.reload();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function submit_form() {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data akan diteruskan ke bagian Pembelian, lanjutkan?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            var manual = [];
            var trx_sak = [];

            $('#tabel_pembelian tr').each(function(a, b) {
                var record_supplier = 0;
                var record_primary = 0;
                if (a > 0) {
                    if($("[name=is_record_supplier]", b).is(":checked")){
                        record_supplier = 1;
                    }
                    if($("[name=is_record_primary]", b).is(":checked")){
                        record_primary = 1;
                    }
                    trx_sak.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        id_jenis_sak: $("[name=id_jenis_sak]", b).val(),
                        setor_sak: $("[name=setor_sak]", b).val(),
                        is_record_supplier : record_supplier,
                        is_record_primary : record_primary
                    });
                }
            });

            $('#tabel_jenis tr').each(function(a, b) {
                if (a > 0) {
                    data.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        jenis_gabah: $("[name=jenis_gabah]", b).val(),
                        jenis_padi: $("[name=id_jenis_padi]", b).val(),
                        potong_sak: $("[name=potong_sak]", b).val(),
                    });
                }
            });

            $('#tabel_manual tr').each(function(a, b) {
                if (a > 0) {
                    manual.push({
                        id_batch : $("[name=id_batch]", b).val(),
                        jenis_gabah: $("[name=jenis_gabah]", b).val(),
                        jenis_padi: $("[name=id_jenis_padi]", b).val(),
                    });
                }
            });
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/update_potongan_sak')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'data': data,
                    'manual' : manual,
                    'trx_sak' : trx_sak
                },
                success: function(result) {
                    $.ajax({
                        url: "<?= base_url('api/internal/pembelian/gabah/submit_qc_pembelian')?>",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': <?= $pembelian['id']?>,
                        },
                        success: function(result) {
                            window.location.href = result.redirect;
                        },
                        error: function(e) {
                            console.log(e);
                        },
                        complete: function(e) {}
                    });
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        
        }
    })
}



function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail_batch')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=id_pembelian]').val(result.id_pembelian);
                $('[name=batch]').val(result.batch).change();
                $('[name=batch_label]').val(result.batch_label);
                $('[name=berat_kotor]').val(result.berat_kotor);
                $('[name=berat_kosong]').val(result.berat_kosong);
                $('[name=berat_netto]').val(result.berat_netto);
                if(result.is_manual == 1){
                    $('[name=is_manual]').prop('checked', true);
                }else{
                    $('[name=is_manual]').prop('checked', false);
                }
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('api/internal/pembelian/gabah/delete_data_batch')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}
</script>
@end