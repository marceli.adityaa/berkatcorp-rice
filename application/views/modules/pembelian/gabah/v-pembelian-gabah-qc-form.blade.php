@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail ID Pembelian : {{$pembelian['kode_pembelian']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <div class="row">
                    <div class="col-8">
                        <form method="get">
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">SAP</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="sap" value="{{$sap}}">
                                </div>
                            </div>
                            <div class="row mg-t-10">
                                <label class="col-sm-4 form-control-label">Jumlah Sak</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="number" class="form-control" name="jumlah_sak" id="jumlah_sak" autocomplete="off">
                                </div>
                            </div>
                            <div id="toolbar" class="mg-y-10 pd-b-20">
                            </div>
                        </form>
                    </div>
                    <div class="col-4 pd-x-30">
                        <div class="row mg-t-10">
                            <button class="btn btn-dark pull-right w-100" type="button" onclick="submit({{$sap}})"><i class="icons icon-plus mg-r-10"></i>SIMPAN & TAMBAH SAP</button>
                        </div>
                        <div class="row mg-t-10">
                            <button class="btn btn-info pull-right w-100" type="button" onclick="submit()"><i class="icons icon-cursor mg-r-10"></i>SIMPAN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mg-t-10">
    <div class="card-header card-header-default bg-dark">
        <h6 class="mg-b-0 tx-white">Jenis Gabah : <b class="color-brown">GABAH KS</b></h6>
        <input type="hidden" id="jenis_gabah[]" value="">
    </div>
    <div class="card-body">
        <div class="row">
            <label class="col-sm-3 form-control-label">Pengeringan</label>
            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="jemur-1" class="custom-control-input" name="pengeringan_ks" value="jemur">
                    <label class="custom-control-label" for="jemur-1">Jemur</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="dryer-1" class="custom-control-input" name="pengeringan_ks" value="dryer">
                    <label class="custom-control-label" for="dryer-1">Dryer</label>
                </div>
            </div>
        </div>
        <button class="btn btn-primary mg-t-20" type="button" id="tambah_ka_ks">+ Tambah Kadar Air</button>
        <div class="table-responsive">
        <table class="table table-striped table-white" id="tabel_ks">
            <thead>
                <tr>
                    <th>Default</th>
                    <th>Jenis Padi</th>
                    <th>Kadar Air</th>
                    <th>Jumlah Sak</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        </div>
    </div>
</div>

<div class="card mg-t-10">
    <div class="card-header card-header-default bg-dark">
        <h6 class="mg-b-0 tx-white">Jenis Gabah : <b class="color-brown">GABAH KG</b></h6>
        <input type="hidden" id="jenis_gabah[]" value="">
    </div>
    <div class="card-body">
        <div class="row">
            <label class="col-sm-3 form-control-label">Proses</label>
            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="proses-tapel" class="custom-control-input" name="proses_kg" value="tapel">
                    <label class="custom-control-label" for="proses-tapel">Tapel</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="proses-giling" class="custom-control-input" name="proses_kg" value="giling">
                    <label class="custom-control-label" for="proses-giling">Giling</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="proses-jemur" class="custom-control-input" name="proses_kg" value="jemur">
                    <label class="custom-control-label" for="proses-jemur">Jemur</label>
                </div>
            </div>
        </div>
        <button class="btn btn-warning mg-t-20" type="button" id="tambah_ka_kg">+ Tambah Data</button>
        <div class="table-responsive">
        <table class="table table-striped table-white" id="tabel_kg">
            <thead>
                <tr>
                    <th>Default</th>
                    <th>Jenis Padi</th>
                    <th>Kadar Air</th>
                    <th>Jumlah Sak</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        </div>
    </div>
</div>

@end

@section('modal')

<div class="modal fade" tabindex="-1" role="dialog" id="modal_tambah_ka" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form id="form-tambah-ka">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-bold">Tambah Kadar Air</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" id="jenis_modal" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Jenis Padi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($jenis_padi as $row)
                                <div class="custom-control custom-radio custom-control-inline px-3 px-3">
                                    <input type="radio" id="jenis_padi_{{$row['id']}}" class="custom-control-input" name="jenis_padi" value="{{$row['id']}}" data-label="{{$row['nama']}}">
                                    <label class="custom-control-label label_jenis_padi" for="jenis_padi_{{$row['id']}}">{{$row['nama']}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Kadar Air<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($kadar_air as $row)
                                <div class="custom-control custom-radio custom-control-inline px-3 py-3">
                                    <input type="radio" id="kadar_air_{{$row['id']}}" class="custom-control-input" name="kadar_air" value="{{$row['id']}}" data-label="{{$row['nama']}}">
                                    <label class="custom-control-label label_kadar_air" for="kadar_air_{{$row['id']}}">{{$row['nama']}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <small id="tambah_ka_help" class="tx-danger" hidden>
                            *Harap pilih jenis padi dan kadar air.
                        </small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="tambah_ka(this)">+ Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<audio id="plusAudio">
    <source src="{{base_url('assets/audio/water-click.wav')}}">
</audio>
<audio id="minusAudio">
    <source src="{{base_url('assets/audio/back-click.wav')}}">
</audio>
@end

@section('js')
<script type="text/javascript">
let idx = 1;
$(document).ready(function() {

})
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah_ka_ks').click(function() {
    let jumlah_sak = $('#jumlah_sak').val();
    if (jumlah_sak != '') {
        $('#jenis_modal').val('ks');
        $('#tambah_ka_help').attr('hidden', true);
        $('#modal_tambah_ka form').trigger('reset');
        $("#modal_tambah_ka").modal('show');
    } else {
        $('#jumlah_sak').focus();
        swal.fire("Perhatian", "Harap isi jumlah sak terlebih dahulu.", "warning");
    }
});

$('#tambah_ka_kg').click(function() {
    let jumlah_sak = $('#jumlah_sak').val();
    if (jumlah_sak != '') {
        $('#jenis_modal').val('kg');
        $('#tambah_ka_help').attr('hidden', true);
        $('#modal_tambah_ka form').trigger('reset');
        $("#modal_tambah_ka").modal('show');
    } else {
        $('#jumlah_sak').focus();
        swal.fire("Perhatian", "Harap isi jumlah sak terlebih dahulu.", "warning");
    }
});

function tambah_ka(e) {

    var jns_padi = $('#form-tambah-ka input[name=jenis_padi]:checked').val();
    var jns_ka = $('#form-tambah-ka input[name=kadar_air]:checked').val();
    if (jns_padi != undefined && jns_ka != undefined) {
        var data = new Object();
        data = {
            val_jenis_padi: $('#form-tambah-ka input[name=jenis_padi]:checked').val(),
            label_jenis_padi: $('#form-tambah-ka input[name=jenis_padi]:checked').data('label'),
            val_jenis_ka: $('#form-tambah-ka input[name=kadar_air]:checked').val(),
            label_jenis_ka: $('#form-tambah-ka input[name=kadar_air]:checked').data('label')
        };
        var ref = $('#jenis_modal').val();
        if (ref == 'ks') {
            tambah_ks(data);
        } else {
            tambah_kg(data);
        }
    } else {
        $('#tambah_ka_help').attr('hidden', false);
    }

}

function tambah_ks(data) {
    add_row('tabel_ks', 'ks', data);
}

function tambah_kg(data) {
    add_row('tabel_kg', 'kg', data);
}

function add_row(table, ref, data) {
    // console.log(data);
    var element = '<tr>\
                    <td>\
                        <div class="custom-control custom-radio custom-control-inline">\
                            <input type="radio" id="ks-radio-' + idx + '" class="custom-control-input radio-is-default" name="is_default" onchange="trigger_sisa_sak(this)">\
                            <label class="custom-control-label" for="ks-radio-' + idx + '">Pilih</label>\
                        </div>\
                    </td>\
                    <td>\
                        <input type="text" class="form-control form_jenis_padi" value="' + data.label_jenis_padi + '" data-id="' + data.val_jenis_padi + '" readonly="" name="form_jenis_padi">\
                    </td>\
                    <td>\
                        <input type="text" class="form-control form_jenis_ka" value="' + data.label_jenis_ka + '" data-id="' + data.val_jenis_ka + '" readonly="" name="form_jenis_ka">\
                    </td>\
                    <td>\
                        <div class="input-group input-group-lg">\
                            <div class="input-group-prepend">\
                                <button type="button" class="btn btn-secondary button-action" onclick="minus(this)">-</button>\
                            </div>\
                            <input type="text" class="form-control text-center value_sak" value="0" readonly="" name="value_sak" id="value_sak' + idx + '">\
                            <div class="input-group-append">\
                                <button type="button" class="btn btn-primary button-action" onclick="plus(this)">+</button>\
                            </div>\
                        </div>\
                    </td>\
                    <td nowrap>\
                        <button type="button" class="btn btn-danger" onclick="hapus(this)"><i class="fa fa-trash"></i></button>\
                    </td>\
                </tr>';
    idx++;
    $("#" + table + " tbody").append(element);
    $("#modal_tambah_ka").modal('hide');
}

function plus(e) {
    let all = $('.value_sak').serializeArray();
    let current = parseInt($(e).closest('tr').find('.value_sak').val());
    let val = parseInt($('input[name=is_default]:checked').closest('tr').find('.value_sak').val());
    let jumlah_sak = parseInt($('#jumlah_sak').val());
    if (val > 0 || isNaN(val)) {
        let sisa = jumlah_sak;
        for (let i = 0; i < all.length; i++) {
            sisa -= parseInt(all[i].value);
        }
        if(sisa > 0){
            $(e).closest('tr').find('.value_sak').val(parseInt(current) + 1);
        }else{
            if(isNaN(val)){
                $(e).closest('tr').find('.value_sak').focus();
            }else{
                $(e).closest('tr').find('.value_sak').val(parseInt(current) + 1);
            }
        }
    } else {
        $(e).closest('tr').find('.value_sak').focus();
    }
    $("#plusAudio")[0].play();
    trigger_sisa_sak(e);
}

function minus(e) {
    let current = $(e).closest('tr').find('.value_sak').val();
    if (current > 0) {
        $(e).closest('tr').find('.value_sak').val(parseInt(current) - 1);
    } else {
        $(e).closest('tr').find('.value_sak').focus();
    }
    $("#minusAudio")[0].play();
    trigger_sisa_sak(e);
}

function trigger_sisa_sak(e) {
    let all = $('.value_sak').serializeArray();
    let jumlah_sak = parseInt($('#jumlah_sak').val());
    let current = parseInt($('input[name=is_default]:checked').closest('tr').find('.value_sak').val());
    if (jumlah_sak == '') {
        jumlah_sak = 0;
    }
    let sisa = jumlah_sak + current;
    for (let i = 0; i < all.length; i++) {
        sisa -= parseInt(all[i].value);
    }
    if(sisa >= 0){
        $('input[name=is_default]:checked').closest('tr').find('.value_sak').val(sisa);
        $('input[name=is_default]:checked').closest('tr').find('.button-action').attr('disabled', true);
        $('input[name=is_default]:not(:checked)').closest('tr').find('.button-action').attr('disabled', false);
    }
}

function submit(sap) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var new_sap = false;
            var check = true;
            if (sap > 0) {
                // Simpan data dan tambah SAP baru
                new_sap = true;
            }

            var data = [];
            $('#tabel_ks tr').each(function(a, b) {
                if (a > 0) {
                    if ($("[name=pengeringan_ks]:checked").val() == undefined) {
                        check = false;
                        swal.fire("Perhatian", "Harap pilih metode pengeringan terlebih dahulu.", "warning");
                    } else {
                        data.push({
                            id_batch: <?= $pembelian['id_batch'] ?> ,
                            jenis_gabah: 'ks',
                            jenis_padi: $(".form_jenis_padi", b).data().id,
                            pengeringan: $("[name=pengeringan_ks]:checked").val(),
                            proses_kg: '',
                            sap: $("[name=sap]").val(),
                            kadar_air: $(".form_jenis_ka", b).data().id,
                            jumlah_sak: $(".value_sak", b).val(),
                            default : $('input[name=is_default]', b).prop('checked')
                        });
                    }
                }
            });

            $('#tabel_kg tr').each(function(a, b) {
                if (a > 0) {
                    if ($("[name=proses_kg]:checked").val() == undefined) {
                        check = false;
                        swal.fire("Perhatian", "Harap pilih metode proses terlebih dahulu.", "warning");
                    }else{
                        data.push({
                            id_batch: <?= $pembelian['id_batch'] ?> ,
                            jenis_gabah: 'kg',
                            jenis_padi: $(".form_jenis_padi", b).data().id,
                            pengeringan: '',
                            proses_kg: $("[name=proses_kg]:checked").val(),
                            sap: $("[name=sap]").val(),
                            kadar_air: $(".form_jenis_ka", b).data().id,
                            jumlah_sak: $(".value_sak", b).val(),
                            default : $('input[name=is_default]', b).prop('checked')
                        });
                    }
                }
            });

            if (check == false) {
                swal.fire("Perhatian", "Harap pilih metode pengeringan terlebih dahulu.", "warning");
            } else {

                $.ajax({
                    url: "<?= base_url('api/internal/pembelian/gabah/insert_qc_form')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'new_sap': new_sap,
                        'sap': $("[name=sap]").val(),
                        'batch' : <?= $pembelian['id_batch']?>,
                        'data': data
                    },
                    success: function(result) {
                        window.location.href = result.redirect;
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        }
    })
    
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
            trigger_sisa_sak(e);
        }
    })
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=id_supplier]').val(result.id_supplier).change();
                $('[name=resi_pengiriman]').val(result.resi_pengiriman).change();
                $('[name=plat_kendaraan]').val(result.plat_kendaraan);
                $('[name=tanggal_pembelian]').val(result.tanggal_pembelian);
                $('[name=berat_bruto]').val(result.berat_bruto);
                $('[name=berat_kosong]').val(result.berat_kosong);
                $('[name=potong_sak]').val(result.potong_sak);
                $('[name=potong_lain]').val(result.potong_lain);
                $('[name=berat_netto]').val(result.berat_netto);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}
</script>
@end