@layout('commons/index')

@section('content')
<style>
.jumlah_sak{min-width:60px;}
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Detail Timbangan ID : {{$pembelian['kode_pembelian']}}
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form action="#">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Timbangan Ke</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['batch']}}</label>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Label</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <label class="form-control">{{$pembelian['batch_label']}}</label>
                        </div>
                    </div>

                    <!-- <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sap (Jumlah Sak)
                        </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="row col-12 div_sap">
                                <div class="col px-1 py-1">
                                    <button type="button" class="btn btn-dark" onclick="tambah_sap(this)">+ Tambah Sap</button>
                                </div>
                                <?php 
                                    if(!empty($sap_manual)){
                                        $sap_manual = json_decode($timbangan['sap_manual']);
                                    }else{
                                        $sap_manual = array();
                                    }
                                    if(empty($timbangan['berat_netto'])){
                                        $timbangan['berat_netto'] = 0;
                                    }

                                    $jml_sak = 0;
                                    if(!empty($sap_manual)){
                                        foreach($sap_manual as $row){
                                            echo '<div class="col px-1 py-1 float-left"> <input type="number" class="jumlah_sap form-control" value="'.$row.'" name="jumlah_sap"></input></div>';
                                            $jml_sak += $row;
                                        }
                                    }
                                    
                                ?>
                            </div>
                        </div>
                    </div> -->

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jumlah Sak</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="{{(!empty($detail))?count($detail):0}}" id="jml_sak">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Berat Total</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly="" value="{{monefy($timbangan['berat_netto'], false)}}" id="berat_total">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="button" id="save" class="btn btn-info btn-lg" onclick="simpan()"><i class="fa fa-save"></i> Simpan</button>  
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mg-t-10">
    <div class="card-header card-header-default bg-dark">
        <h6 class="mg-b-0 tx-white">Data Muatan</h6>
        <input type="hidden" id="jenis_gabah[]" value="">
    </div>
    <div class="card-body">
        <div class="row">
            
        </div>
        @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
        <button class="btn btn-primary mg-t-20" type="button" id="tambah_data">+ Tambah Kriteria</button>
        @endif
        <div class="table-responsive">
            <table class="table table-striped table-white w-100" id="tabel_timbangan">
                <thead>
                    <tr>
                        <th nowrap>Jenis Gabah</th>
                        <th style="min-width:120px" nowrap>Jenis Padi</th>
                        <th style="min-width:120px" nowrap>Kadar Air</th>
                        <th style="min-width:120px" nowrap>Pengeringan</th>
                        <th style="min-width:120px" nowrap>Proses</th>
                        <th style="min-width:180px">Berat</th>
                        <th nowrap>Subtotal</th>
                        <th nowrap>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($detail))
                        @foreach($detail as $key => $row)
                        <?php
                            if($key == 0){
                                $temp['gabah'] = $row['jenis_gabah'];
                                $temp['padi'] = $row['id_jenis_padi'];
                                $temp['pengeringan'] = $row['pengeringan'];
                                $temp['proses'] = $row['proses'];
                                $temp['ka'] = $row['id_ka'];
                            } 
                        ?>
                        @if($key == 0 || ($key > 0 && !($temp['gabah'] == $row['jenis_gabah'] && $temp['padi'] == $row['id_jenis_padi'] && $temp['pengeringan'] == $row['pengeringan'] && $temp['proses'] == $row['proses'] && $temp['ka'] == $row['id_ka'])))
                        <tr>
                            <td>
                                <input type="text" class="form-control" value="{{strtoupper($row['jenis_gabah'])}}" readonly="" name="jenis_gabah" readonly="">
                            </td>
                            <td>
                                <input type="text" class="form-control form_jenis_padi" value="{{$row['padi']}}" data-id="{{$row['id_jenis_padi']}}" readonly="" name="form_jenis_padi">
                            </td>
                            <td>
                                <input type="text" class="form-control form_jenis_ka" value="{{$row['kadar_air_label']}}" data-id="{{$row['id_ka']}}" readonly="" name="form_jenis_ka">
                            </td>

                            <td>
                                @if($row['jenis_gabah'] == 'ks')
                                <select class="form-control" name="pengeringan" id="d-pengeringan-{{$row['id']}}">
                                    <option value="jemur" {{($row['pengeringan'] == 'jemur') ? 'selected': ''}}>Jemur</option>
                                    <option value="dryer" {{($row['pengeringan'] == 'dryer') ? 'selected': ''}}>Dryer</option>
                                </select>
                                @endif
                            </td>
                            <td>
                                @if($row['jenis_gabah'] == 'kg')
                                <select class="form-control" name="proses" id="d-proses-{{$row['id']}}">
                                    <option value="tapel" {{($row['proses'] == 'tapel') ? 'selected': ''}}>Tapel</option>
                                    <option value="giling" {{($row['proses'] == 'giling') ? 'selected': ''}}>Giling</option>
                                    <option value="jemur" {{($row['proses'] == 'jemur') ? 'selected': ''}}>Jemur</option>
                                </select>
                                @endif
                            </td>
                            <td class="div_jumlah">
                                <?php $idx = 0; $subtotal = 0;?>
                                @foreach($detail as $row3)
                                    @if($row['jenis_gabah'] == $row3['jenis_gabah'] && $row['id_jenis_padi'] == $row3['id_jenis_padi'] && $row['pengeringan'] == $row3['pengeringan'] && $row['proses'] == $row3['proses'] && $row['id_ka'] == $row3['id_ka'])
                                        @if($idx == 0)
                                        <div class="row my-1">
                                            <div class="col-6 px-1">
                                                <input type="number" class="jumlah_sak form-control" value="{{$row3['berat_basah']}}"></input>
                                            </div>
                                        <?php $idx += 1;?>
                                        @else
                                            <div class="col-6 px-1">
                                                <input type="number" class="jumlah_sak form-control" value="{{$row3['berat_basah']}}"></input>
                                            </div>
                                            <?php $idx = 0;?>
                                        </div>
                                        @endif
                                        <?php $subtotal += $row3['berat_basah']?>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <input type="number" class="form-control" value="{{$subtotal}}" name="subtotal" readonly>
                            </td>
                            <td nowrap>
                            @if(($pembelian['status_muatan'] != 1 || in_array('super', $role) || in_array('pembelian', $role)))
                                <button type="button" class="btn btn-primary" onclick="tambah_jumlah(this)"><i class="fa fa-plus"></i></button>
                                <button type="button" class="btn btn-danger" onclick="hapus(this)"><i class="fa fa-trash"></i></button>
                            @endif
                            </td>
                        </tr>
                        <?php
                            $temp['gabah'] = $row['jenis_gabah'];
                            $temp['padi'] = $row['id_jenis_padi'];
                            $temp['pengeringan'] = $row['pengeringan'];
                            $temp['proses'] = $row['proses'];
                            $temp['ka'] = $row['id_ka'];
                        ?>
                        @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@end

@section('modal')

<div class="modal fade" tabindex="-1" role="dialog" id="modal_tambah_ka" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form id="form-tambah-ka" action="#">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-bold">Tambah Kriteria Timbangan</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Jenis Gabah<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline px-3 px-3">
                                    <input type="radio" id="jenis_padi_ks" class="custom-control-input" name="jenis_gabah" value="ks">
                                    <label class="custom-control-label label_jenis_padi" for="jenis_padi_ks">Gabah KS</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline px-3 px-3">
                                    <input type="radio" id="jenis_padi_kg" class="custom-control-input" name="jenis_gabah" value="kg">
                                    <label class="custom-control-label label_jenis_padi" for="jenis_padi_kg">Gabah KG</label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Jenis Padi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($jenis_padi as $row)
                                <div class="custom-control custom-radio custom-control-inline px-3 px-3">
                                    <input type="radio" id="jenis_padi_{{$row['id']}}" class="custom-control-input" name="jenis_padi" value="{{$row['id']}}" data-label="{{$row['nama']}}">
                                    <label class="custom-control-label label_jenis_padi" for="jenis_padi_{{$row['id']}}">{{$row['nama']}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Kadar Air<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($kadar_air as $row)
                                <div class="custom-control custom-radio custom-control-inline px-3 py-3">
                                    <input type="radio" id="kadar_air_{{$row['id']}}" class="custom-control-input" name="kadar_air" value="{{$row['id']}}" data-label="{{$row['nama']}}">
                                    <label class="custom-control-label label_kadar_air" for="kadar_air_{{$row['id']}}">{{$row['nama']}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row mg-t-10 pengeringan">
                            <label class="col-sm-4 form-control-label">Pengeringan<span class="tx-danger">*</span></label>

                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="jemur-1" class="custom-control-input" name="pengeringan" value="jemur">
                                    <label class="custom-control-label" for="jemur-1">Jemur</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="dryer-1" class="custom-control-input" name="pengeringan" value="dryer">
                                    <label class="custom-control-label" for="dryer-1">Dryer</label>
                                </div>
                            </div>
                        </div>
                        <hr class="pengeringan">
                        <div class="row mg-t-10 proses">
                            <label class="col-sm-4 form-control-label">Proses<span class="tx-danger">*</span></label>

                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="tapel" class="custom-control-input" name="proses" value="tapel">
                                    <label class="custom-control-label" for="tapel">Tapel</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="giling" class="custom-control-input" name="proses" value="giling">
                                    <label class="custom-control-label" for="giling">Giling</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="jemur-2" class="custom-control-input" name="proses" value="jemur">
                                    <label class="custom-control-label" for="jemur-2">Jemur</label>
                                </div>
                            </div>
                        </div>
                        <hr class="proses">
                        <!-- <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Berat<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="number" class="form-control" value="" name="berat_basah">
                            </div>
                        </div> -->
                        <small id="tambah_data_help" class="tx-danger" hidden="true">
                            *Harap lengkapi form terlebih dahulu.
                        </small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="tambah_data(this)">+ Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<audio id="plusAudio">
    <source src="{{base_url('assets/audio/water-click.wav')}}">
</audio>
<audio id="minusAudio">
    <source src="{{base_url('assets/audio/back-click.wav')}}">
</audio>
@end

@section('js')
<script type="text/javascript">
let idx = 1;
$(document).ready(function() {
    $('.pengeringan, .proses').hide();
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });

})

// $(document).on('change keyup', '.berat', function(){
//     hitung_total_berat();
// });

$(document).on('change keyup', '.jumlah_sak', function(){
   let subtotal = 0;
   $(this).closest('tr').find('.jumlah_sak').each(function(a, b){
    //    console.log(b);
        if($(b).val() == ''){
            subtotal += 0;
        }else{
            subtotal += parseInt($(b).val());
        }
   });   
   $(this).closest('tr').find('[name=subtotal]').val(subtotal);
   hitung_total_berat();
});

$(document).on('change keyup', '.jumlah_sap', function(){
   let sak = 0;
   $('.jumlah_sap').each(function(a, b){
        if($(b).val() == ''){
            sak += 0;
        }else{
            sak += parseInt($(b).val());
        }
   });   
   $('#jml_sak').val(sak);
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah_data').click(function() {
    $('#tambah_data_help').attr('hidden', true);
    $('#modal_tambah_ka form').trigger('reset');
    $('.pengeringan, .proses').hide();
    $("#modal_tambah_ka").modal('show');
});

$('#form-tambah-ka [name=jenis_gabah]').change(function(){
    if($('#form-tambah-ka [name=jenis_gabah]:checked').val() == 'ks'){
        $('.pengeringan').show(300);
        $('.proses').hide(300);
    }else{
        $('.pengeringan').hide(300);
        $('.proses').show(300);
    }
})

function tambah_data(e) {

    var jns_padi = $('#form-tambah-ka input[name=jenis_padi]:checked').val();
    var jns_ka = $('#form-tambah-ka input[name=kadar_air]:checked').val();
    var jns_gabah = $('#form-tambah-ka [name=jenis_gabah]:checked').val();
    var pengeringan = $('#form-tambah-ka [name=pengeringan]:checked').val();
    var proses = $('#form-tambah-ka [name=proses]:checked').val();
    if (jns_padi != undefined && jns_ka != undefined && jns_gabah != undefined) {
        var data = new Object();
        data = {
            jenis_gabah: jns_gabah,
            val_jenis_padi: $('#form-tambah-ka input[name=jenis_padi]:checked').val(),
            label_jenis_padi: $('#form-tambah-ka input[name=jenis_padi]:checked').data('label'),
            val_jenis_ka: $('#form-tambah-ka input[name=kadar_air]:checked').val(),
            label_jenis_ka: $('#form-tambah-ka input[name=kadar_air]:checked').data('label'),
            ks_pengeringan: pengeringan,
            kg_proses: proses
        }; 
        add_row(data);
    }else{
        $('#tambah_data_help').attr('hidden', false);
    }

}

function add_row(data) {
    var element = '<tr>\
                    <td>\
                        <input type="text" class="form-control" value="' + data.jenis_gabah + '" readonly="" name="jenis_gabah" readonly="">\
                    </td>\
                    <td>\
                        <input type="text" class="form-control form_jenis_padi" value="' + data.label_jenis_padi + '" data-id="' + data.val_jenis_padi + '" readonly="" name="form_jenis_padi">\
                    </td>\
                    <td>\
                        <input type="text" class="form-control form_jenis_ka" value="' + data.label_jenis_ka + '" data-id="' + data.val_jenis_ka + '" readonly="" name="form_jenis_ka">\
                    </td>\
                    <td>\
                        <select class="form-control" name="pengeringan" id="pengeringan-'+idx+'">\
                            <option value="jemur">Jemur</option>\
                            <option value="dryer">Dryer</option>\
                        </select>\
                    </td>\
                    <td>\
                        <select class="form-control" name="proses" id="proses-'+idx+'">\
                            <option value="tapel">Tapel</option>\
                            <option value="giling">Giling</option>\
                            <option value="jemur">Jemur</option>\
                        </select>\
                    </td>\
                    <td class="div_jumlah">\
                        <div class="row my-1">\
                            <div class="col-6 px-1">\
                                <input type="number" class="jumlah_sak form-control" value=""></input>\
                            </div>\
                            <div class="col-6 px-1">\
                                <input type="number" class="jumlah_sak form-control" value=""></input>\
                            </div>\
                        </div>\
                    </td>\
                    <td style="max-width:30%">\
                        <input type="number" class="form-control" value="" name="subtotal" readonly>\
                    </td>\
                    <td nowrap>\
                        <button type="button" class="btn btn-primary" onclick="tambah_jumlah(this)"><i class="fa fa-plus"></i></button>\
                        <button type="button" class="btn btn-danger" onclick="hapus(this)"><i class="fa fa-trash"></i></button>\
                    </td>\
                </tr>';
    
    $("#tabel_timbangan tbody").append(element);
    $("#modal_tambah_ka").modal('hide');
    if(data.jenis_gabah == 'ks'){
        $("#pengeringan-"+idx).val(data.ks_pengeringan).change();
        $("#proses-"+idx).remove();
    }else{
        $("#pengeringan-"+idx).remove();
        $("#proses-"+idx).val(data.kg_proses).change();
    }
    idx++;
    hitung_total_berat();
}

function tambah_jumlah(el){
    let field = '<div class="row my-1">\
                    <div class="col-6 px-1">\
                        <input type="number" class="jumlah_sak form-control" value=""></input>\
                    </div>\
                    <div class="col-6 px-1">\
                        <input type="number" class="jumlah_sak form-control" value=""></input>\
                    </div>\
                </div>';
    $(el).closest('tr').find('.div_jumlah').append(field);
}

function tambah_sap(el){
    let field =     '<div class="col px-1 py-1 float-left">\
                        <input type="number" class="jumlah_sap form-control" value="" name="jumlah_sap"></input>\
                    </div>';
    $('.div_sap').append(field);
}

function hitung_total_berat(){
    let total = 0;
    $('#tabel_timbangan tr').each(function(a, b) {
        if (a > 0) {
            $(this).closest('tr').find('.jumlah_sak').each(function(c, d){
                if($(d).val() == ''){
                    total += 0;
                }else{
                    total += parseInt($(d).val());
                }
            });
        }
    });
    $('#berat_total').val(total);
}

function simpan() {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Simpan data?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var data = [];
            var jumlah_sap = $('[name=jumlah_sap]').serializeArray();
            $('#tabel_timbangan tr').each(function(a, b) {
                if (a > 0) {
                    var list_berat = [];
                    $('.jumlah_sak', b).each(function(c, d){
                        if($(d).val() != ''){
                            list_berat.push($(d).val());
                        }
                    });

                    data.push({
                        id_pembelian : <?= $pembelian['id_pembelian']?>,
                        batch : <?= $pembelian['batch']?>,
                        id_batch: '<?= $timbangan['id_batch']?>',
                        jenis_gabah: $(b).find("[name=jenis_gabah]").val(),
                        jenis_padi: $(b).find("[name=form_jenis_padi]").data().id,
                        pengeringan: $(b).find("[name=pengeringan]").val(),
                        proses: $(b).find("[name=proses]").val(),
                        kadar_air: $(b).find("[name=form_jenis_ka]").data().id,
                        berat: list_berat,
                    });
                    
                }
            });
            
            $.ajax({
                url: "<?= base_url('api/internal/pembelian/gabah/submit_qc_form_manual')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id_pembelian' : <?= $pembelian['id_pembelian']?>,
                    'batch' : <?= $pembelian['batch']?>,
                    'id_batch': '<?= $timbangan['id_batch']?>',
                    'primary_batch' : <?= $pembelian['id_batch']?>,
                    'data': data,
                    'jumlah_sap' : jumlah_sap
                },
                success: function(result) {
                    // window.location.href = result.redirect;
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
            
        }
    })
    
}

function hapus(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $(e).closest('tr').remove();
            hitung_total_berat();
        }
    })
}
</script>
@end