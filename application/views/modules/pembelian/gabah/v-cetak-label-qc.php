<html>

<head>
    <style>
    body {
        font-family: sans-serif;
        font-size: 10pt;
    }

    span {
        font-size:10pt;
    }

    p {
        margin: 0pt;
    }

    table.items {
        border: 0.1mm solid #000000;
    }

    td {
        vertical-align: top;
    }

    .text-center{
        text-align:center;
    }
    .text-left{
        text-align:left;
    }
    .text-right{
        text-align:right;
    }
    </style>
</head>

<body>
    <!-- <hr style="margin-top:0px;height:0.1mm;color:#000;"> -->
    <table border="none" width="100%">
        <tr>
            <td width="30%" class="text-left">
                <span>Kode</span>
            </td>
            <td width="70%" class="text-left">
                <span>: <b><?= ($detail['kode_pembelian'])?></b></span>
            </td>
        </tr>
        <tr>
            <td width="30%" class="text-left">
                <span>Tanggal</span>
            </td>
            <td width="70%" class="text-left">
                <span>: <b><?= ($detail['tanggal_pembelian'])?></b></span>
            </td>
        </tr>
        <tr>
            <td width="30%" class="text-left">
                <span>Plat No</span>
            </td>
            <td width="70%" class="text-left">
                <span>: <b><?= strtoupper($detail['plat_kendaraan'])?></b></span>
            </td>
        </tr>
        <tr>
            <td width="30%" class="text-left">
                <span>Timbangan</span>
            </td>
            <td width="70%" class="text-left">
                <?php if($detail['is_manual']){?>
                    <span>: <b>#<?= $detail['batch']?></b> - TIMBANG KECIL</span>
                <?php }else{?>
                    <span>: <b>#<?= $detail['batch']?></b></span>
                <?php }?>
            </td>
        </tr>
        <tr>
            <td width="30%" class="text-left">
                <span>Gabah</span>
            </td>
            <td width="70%" class="text-left">
                <span>: <b><?= strtoupper($detail['jenis_gabah']).' '.strtoupper($detail['padi'])?></b></span>
            </td>
        </tr>
        <tr>
            <td width="30%" class="text-left">
                <span>Pengeringan</span>
            </td>
            <td width="70%" class="text-left">
                <span>: <b><?= strtoupper($detail['pengeringan'])?></b></span>
            </td>
        </tr>
    </table>
</body>

</html>