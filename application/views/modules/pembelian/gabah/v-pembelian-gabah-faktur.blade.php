@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pembelian/gabah/faktur?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pembelian Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Resi/ Plat Nomor</th>
                    <th data-sortable="true">Berat Bruto</th>
                    <th data-sortable="true" data-searchable="false">Bongkar Muatan</th>
                    <th data-sortable="true" data-searchable="false">Input Harga</th>
                    <th data-sortable="true" data-searchable="false">Persetujuan</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($pembelian)){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['status_muatan'] == 1 && $row['status_selesai'] == 0){
                                echo "<a href='".base_url('pembelian/gabah/faktur/detail/'.$row['id'])."' class='btn btn-danger active' data-toggle='tooltip' data-placement='top' title='Input Harga Gabah'><i class='fa fa-calculator'></i> set harga</a>";
                            }else if($row['status_muatan'] == 1 && $row['status_selesai'] == 1){
                                echo "<a href='".base_url('pembelian/gabah/faktur/detail/'.$row['id'])."' class='btn btn-info active'><i class='fa fa-sign-in'></i> detail</a> ";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode_pembelian']."</label></td>";
                            echo "<td>".$row['tanggal_pembelian']."</td>";
                            echo "<td>".$row['supplier']."</td>";
                            if(!empty($row['resi_pengiriman'])){
                                echo "<td><label class='badge badge-info' onclick='cek_resi(this)' data-resi='".$row['resi_pengiriman']."' data-toggle='tooltip' data-placement='top' title='Informasi Ekspedisi'>".$row['resi_pengiriman']."</label><br>
                                <label class='badge badge-light'>".$row['plat_kendaraan']."</label>
                                </td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['plat_kendaraan']."</label></td>";
                            }               
                            
                            echo "<td>".monefy($row['berat_bruto'], false)."</td>";
                            
                            if($row['status_muatan'] == 1){
                                echo "<td><label class='badge badge-success'>OK</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Dalam Proses</label></td>";
                            }

                            if($row['status_selesai'] == 1){
                                echo "<td><label class='badge badge-success'>OK</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";                               
                            }

                            if($row['status_persetujuan'] == 1){
                                echo "<td><label class='badge badge-success'>Disetujui</label></td>";
                            }else if($row['status_persetujuan'] == 2){
                                echo "<td><label class='badge badge-danger'>Ditolak</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";                               
                            }
                            echo "<td><label class='badge badge-light'>".$row['username_input']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembelian/gabah/kelola/submit_pembelian_gabah')?>" id="form-pembelian">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data Pembelian</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Supplier <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_supplier" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($supplier as $row)
                                    <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Ekspedisi<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="resi_pengiriman" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <option value="0">Tanpa Ekspedisi</option>
                                    @foreach ($ekspedisi as $row)
                                    <option value="{{$row->no_resi}}" data-tanggal_jalan="{{$row->tanggal_jalan}}" data-plat="{{$row->no_pol}}">{{$row->no_resi.' | '.ucwords($row->sopir)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Plat Kendaraan<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="plat_kendaraan" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Pembelian<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="tanggal_pembelian" value="" autocomplete="off" required="">
                            </div>
                        </div>
                        <hr>

                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Bruto<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_bruto" value="0" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Kosong<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_kosong" value="0" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mg-t-10">
                            <div class="row col-6">
                                <label class="col-6 form-control-label">Berat Muatan<span class="tx-danger"></span></label>
                                <div class="col-6 mg-t-10 mg-sm-t-0">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="berat_muatan" value="0" autocomplete="off" readonly="">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="inputGroup-sizing-default">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_timbangan" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Update Data Timbangan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <ul class="nav nav-pills mb-3" id="tab1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#tab-timbangan" role="tab" aria-controls="tab-timbangan" aria-selected="true"><b>Data Timbangan</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#tab-tambah" role="tab" aria-controls="tab-tambah" aria-selected="false"><b>+ Tambah Data</b></a>
                        </li>

                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="form-layout form-layout-4">
                                <table class="table table-bordered table-hover" id="tabel_timbangan">
                                    <thead class="bg-info">
                                        <th>Timbang Ke</th>
                                        <th>Label</th>
                                        <th>Berat Bruto</th>
                                        <th>Berat Kosong</th>
                                        <th>Berat Netto</th>
                                        <th>Manual</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <div class="row mg-t-10">
                                    <div class="col-12 mg-t-10 mg-sm-t-0 text-center">
                                        <button class="btn btn-info" type="button" onclick="simpan_timbangan()"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-tambah" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <form action="#" id="form-tambah-timbangan">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="id_pembelian" value="">
                                <div class="form-layout form-layout-4">
                                    <div class="row">
                                        <label class="col-sm-4 form-control-label">Timbang ke <span class="tx-danger"></span></label>
                                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                            <input type="text" class="form-control reset-form" name="batch" value="" autocomplete="off" required="" readonly>
                                        </div>
                                    </div>
                                    <div class="row mg-t-10">
                                        <label class="col-sm-4 form-control-label">Label</label>
                                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                            <input type="text" class="form-control reset-form" name="batch_label" value="" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="row mg-t-10">
                                        <label class="col-4 form-control-label">Berat Bruto<span class="tx-danger">*</span></label>
                                        <div class="col-8 mg-t-10 mg-sm-t-0">
                                            <input type="number" class="form-control reset-form" name="berat_kotor" value="" autocomplete="off" min="0" required="">
                                        </div>
                                    </div>
                                    <div class="row mg-t-10">
                                        <label class="col-4 form-control-label">Berat Kosong<span class="tx-danger">*</span></label>
                                        <div class="col-8 mg-t-10 mg-sm-t-0">
                                            <input type="number" class="form-control reset-form" name="berat_kosong" value="" autocomplete="off" min="0">
                                        </div>
                                    </div>
                                    <div class="row mg-t-10">
                                        <label class="col-4 form-control-label">Berat Netto<span class="tx-danger">*</span></label>
                                        <div class="col-8 mg-t-10 mg-sm-t-0">
                                            <input type="number" class="form-control reset-form" name="berat_netto" value="" autocomplete="off" min="0" >
                                        </div>
                                    </div>

                                    <div class="row mg-t-10">
                                        <label class="col-sm-4 form-control-label"></label>
                                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input reset-form" id="is_manual" value="1" name="is_manual">
                                                <label class="custom-control-label" for="is_manual">Hitung Manual</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mg-t-10">
                                        <label class="col-sm-4 form-control-label"></label>
                                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                            <button type="button" class="btn btn-info" onclick="tambah_timbangan()"><i class="fa fa-save"></i> Simpan</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        dropdownParent: $('#modal_form')
    });
});

$(document).on('change keyup', '.hitung1', function(){
    let kotor = getNum($(this).closest('tr').find('[name=berat_kotor]').val());
    let kosong = getNum($(this).closest('tr').find('[name=berat_kosong]').val());
    let netto = kotor - kosong;
    $(this).closest('tr').find('[name=berat_netto]').val(netto);
});

$(document).on('change keyup', '.hitung2', function(){
    let kotor = getNum($(this).closest('tr').find('[name=berat_kotor]').val());
    let netto = getNum($(this).closest('tr').find('[name=berat_netto]').val());
    let kosong = kotor - netto;
    $(this).closest('tr').find('[name=berat_kosong]').val(kosong);
});


$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $('#form-pembelian').trigger('reset');
    $("#modal_form").modal('show');
});

$("#form-tambah-timbangan [name=berat_kotor], #form-tambah-timbangan [name=berat_kosong]").on('keyup', function(){
    let kotor = getNum($("#form-tambah-timbangan [name=berat_kotor]").val());
    let kosong = getNum($("#form-tambah-timbangan [name=berat_kosong]").val());
    let netto = kotor - kosong;
    $("#form-tambah-timbangan [name=berat_netto]").val(netto);
});

$("#form-tambah-timbangan [name=berat_netto]").on('keyup', function(){
    let kotor = getNum($("#form-tambah-timbangan [name=berat_kotor]").val());
    let netto = getNum($("#form-tambah-timbangan [name=berat_netto]").val());
    let kosong = kotor - netto;
    $("#form-tambah-timbangan [name=berat_kosong]").val(kosong);
});

$('#form-pembelian [name=berat_bruto], #form-pembelian [name=berat_kosong]').on('keyup', function(){
    hitungMuatan();
});

$('[name=resi_pengiriman]').on('change', function(){
    set_ekspedisi();
});

function cek_resi(el){
    var resi = $(el).data().resi;
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/info_resi')?>",
        type: "POST",
        dataType: "json",
        data: {
            'resi': resi,
        },
        success: function(result) {
            $('#form-pembelian').trigger('reset');
            $('#form-pembelian [name=id]').val(result.id);
            
            $('#modal_form').modal('show');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    
}

function set_ekspedisi(){
    let resi = $('[name=resi_pengiriman] option:selected').data();
    $('[name=plat_kendaraan]').val(resi.plat);
    console.log(resi);
}


function getNum(val) {
   if (isNaN(val)) {
     return 0;
   }
   return val;
}

function hitungMuatan(){
    let bruto = parseInt($('#form-pembelian [name=berat_bruto]').val());
    let kosong = parseInt($('#form-pembelian [name=berat_kosong]').val());
    $('#form-pembelian [name=berat_muatan]').val(bruto-kosong);
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#form-pembelian').trigger('reset');
                $('#form-pembelian [name=id]').val(result.id);
                $('#form-pembelian [name=id_supplier]').val(result.id_supplier).change();
                $('#form-pembelian [name=resi_pengiriman]').val(result.resi_pengiriman).change();
                $('#form-pembelian [name=plat_kendaraan]').val(result.plat_kendaraan);
                $('#form-pembelian [name=tanggal_pembelian]').val(result.tanggal_pembelian);
                $('#form-pembelian [name=berat_bruto]').val(result.berat_bruto);
                $('#form-pembelian [name=berat_kosong]').val(result.berat_kosong);
                $('#form-pembelian [name=potong_sak]').val(result.potong_sak);
                $('#form-pembelian [name=potong_lain]').val(result.potong_lain);
                $('#form-pembelian [name=berat_muatan]').val(result.berat_muatan);
                $('#form-pembelian [name=resi_pengiriman]').val(result.resi_pengiriman);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function timbangan(id){
    if(id != ''){
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/get_data_timbangan')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                // Init data untuk form tambah
                $('#form-tambah-timbangan [name=id_pembelian]').val(result.pembelian.id);
                $('#form-tambah-timbangan [name=batch]').val(result.batch_idx);
                $('#form-tambah-timbangan [name=berat_kotor]').val(result.sisa_bruto);
                init_table(id);
                $('#modal_timbangan').modal('show');
                $('#tab1 a[href="#tab-timbangan"]').tab('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function tambah_timbangan(){
    let manual = 0;
    if($('#form-tambah-timbangan [name=is_manual]').is(':checked')){
        manual = 1;        
    }
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/insert_data_timbangan')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id_pembelian': $('#form-tambah-timbangan [name=id_pembelian]').val(),
            'batch': $('#form-tambah-timbangan [name=batch]').val(),
            'batch_label': $('#form-tambah-timbangan [name=batch_label]').val(),
            'berat_kotor': $('#form-tambah-timbangan [name=berat_kotor]').val(),
            'berat_kosong': $('#form-tambah-timbangan [name=berat_kosong]').val(),
            'berat_netto': $('#form-tambah-timbangan [name=berat_netto]').val(),
            'is_manual': manual,
        },
        success: function(result) {
            if(result){
                $('#tab1 a[href="#tab-timbangan"]').tab('show');
                init_table($('[name=id_pembelian]').val());
                $('.reset-form').trigger('reset');
                Swal.fire('Sukses', 'Berhasil menyimpan data', 'success');
            }else{
                Swal.fire('Gagal', 'Gagal menyimpan data, mohon periksa kembali data anda', 'danger');
            }
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}

function simpan_timbangan(){
    var data = [];
    $('#tabel_timbangan tr').each(function(a, b) {
        if (a > 0) {
            if($(b).find("[name=batch]").val() != ''){
                let manual = 0;
                if($(b).find("[name=is_manual]").is(':checked')){
                    manual = 1;        
                }
                data.push({
                    id: $(b).find("[name=id]").val(),
                    batch: $(b).find("[name=batch]").val(),
                    batch_label: $(b).find("[name=batch_label]").val(),
                    berat_kotor: $(b).find("[name=berat_kotor]").val(),
                    berat_kosong: $(b).find("[name=berat_kosong]").val(),
                    berat_netto: $(b).find("[name=berat_netto]").val(),
                    is_manual : manual
                });
            }
        }else{
            valid = false;
        }
    });

    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/update_data_timbangan')?>",
        type: "POST",
        dataType: "json",
        data: {
            'data': data
        },
        success: function(result) {
            if(result){
                Swal.fire('Sukses', 'Berhasil menyimpan data', 'success');
                init_table($('[name=id_pembelian]').val());
            }else{
                Swal.fire('Gagal', 'Gagal menyimpan data, mohon periksa kembali data anda', 'danger');
            }
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
     
}

function init_table(id){
    $.ajax({
        url: "<?= base_url('api/internal/pembelian/gabah/get_data_timbangan')?>",
        type: "POST",
        dataType: "json",
        data: {
            'id': id,
        },
        success: function(result) {
            $('#tabel_timbangan tbody').empty();
            if(result.timbangan != false){
                let row = '';
                let is_manual = '';
                $(result.timbangan).each(function(a, b){
                    if(b.is_manual == 1){
                        is_manual = 'checked=""';
                    }else{
                        is_manual = '';
                    }
                    row = '<tr>\
                    <td><input type="hidden" name="id" value="'+b.id+'"><input type="number" class="form-control" name="batch" value="'+b.batch+'"></td>\
                    <td><input type="text" class="form-control" name="batch_label" value="'+b.batch_label+'"></td>\
                    <td><input type="number" class="form-control hitung1" name="berat_kotor" value="'+b.berat_kotor+'"></td>\
                    <td><input type="number" class="form-control hitung1" name="berat_kosong" value="'+b.berat_kosong+'"></td>\
                    <td><input type="number" class="form-control hitung2" name="berat_netto" value="'+b.berat_netto+'" readonly></td>\
                    <td>\
                        <div class="custom-control custom-checkbox">\
                            <input type="checkbox" class="custom-control-input" id="is_manual_'+b.id+'" value="1" name="is_manual" '+is_manual+'>\
                            <label class="custom-control-label" for="is_manual_'+b.id+'">Manual</label>\
                        </div>\
                    </td>\
                    </tr>';
                    $('#tabel_timbangan tbody').append(row);
                });
            }
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
}


function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/pembelian/gabah/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end