@layout('commons/index')

@section('content')
<style>
.text-orange{
    color:#f48645;
}
</style>
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('aruskas/rekap?')?>" id="form-filter">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sumber Kas</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="kas" class="form-control select2">
                                <option value="all">All</option>
                                <optgroup label="Kas">
                                @foreach ($kas as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{ucwords($row->label)}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                                <optgroup label="Rekening">
                                @foreach ($rekening as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Transaksi</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <div class="col-sm-2 mg-t-10 mg-sm-t-0 tx-center" style="margin:auto;">
                        sampai dengan
                        </div>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Arus</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="arus" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$arus as $key => $val){
                                        if(!empty($this->input->get('arus')) && $this->input->get('arus') == $key){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi</h6>
    </div>
    <div class="card-body">
        @if(!empty($this->input->get()))
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
        <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kas</th>
                    <!-- <th data-sortable="true">Arus</th> -->
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Arus Masuk</th>
                    <th data-sortable="true">Arus Keluar</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Deskripsi</th>
                    <th data-sortable="true">Input by</th>
                    <th data-sortable="true">Acc by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $total_masuk = 0;
                    $total_keluar = 0;
                    if(!empty($transaksi->data)){
                        foreach($transaksi->data as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".date('Y-m-d', strtotime($row->tgl_transaksi))."</td>";
                            echo "<td><label class='badge badge-light'>".$row->kas."</label></td>";
                            // if($row->arus == 'in'){
                            //     echo "<td><label class='badge badge-primary'>in</label></td>";
                            // }else{
                            //     echo "<td><label class='badge badge-warning'>out</label></td>";
                            // }
                            echo "<td><label class='badge badge-light'>".$row->sumber."</label></td>";
                            if($row->arus == 'in'){
                                echo "<td><label class='tx-primary'>+ ".monefy($row->nominal, false)."</label></td>";
                                echo "<td></td>";
                                $total_masuk += $row->nominal;
                            }else{
                                echo "<td></td>";
                                echo "<td nowrap><label class='text-orange'>- ".monefy($row->nominal, false)."</label></td>";
                                $total_keluar += $row->nominal;
                            }
                            echo "<td>".$row->keterangan."</td>";
                            echo "<td>".$row->deskripsi."</td>";

                            echo "<td><label class='badge badge-light'>".$row->username_input."</label><br><label class='badge badge-light'>".$row->input_timestamp."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row->username_approval."</label><br><label class='badge badge-light'>".$row->approval_timestamp."</label></td>";
                            echo "</tr>";
                        }
                    }
                    echo "<tr>";
                    echo "<td class='text-right tx-bold tx-20' colspan='5'>Total</td>";
                    echo "<td class='tx-bold tx-20'><label class='tx-primary' style='white-space:nowrap'>+ ".monefy($total_masuk, false)."</label></td>";
                    echo "<td class='tx-bold tx-20'><label class='text-orange' style='white-space:nowrap'>- ".monefy($total_keluar, false)."</label></td>";
                    echo "<td colspan='3'></td>";
                    echo "</tr>";
				?>
            </tbody>
        </table>
        <div class="tx-center">
        <table class="table" style="vertical-align:middle;">
            <tr>
                <td class="tx-right">Saldo Awal</td>
                <td>:</td>
                <td class="tx-left tx-bold tx-20">
                {{monefy($total_in - $total_out, false)}}
                </td>
            </tr>
            <tr>
                <td class="tx-right">Saldo Akhir</td>
                <td>:</td>
                <td class="tx-left tx-bold tx-20">
                {{monefy(($total_in - $total_out) + ($total_masuk - $total_keluar), false)}}
                </td>
            </tr>
        </table>
        </div>

        @else
        <p class="text-center">Klik filter untuk menampilkan data transaksi</p>
        @endif
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let kas = $("#form-filter [name=kas]").val();
    let arus = $("#form-filter [name=arus]").val();
    var url = "<?= base_url('aruskas/export?')?>"+'start='+start+'&end='+end+'&kas='+kas+'&arus='+arus;
    window.open(url, '_blank');
}
</script>
@end