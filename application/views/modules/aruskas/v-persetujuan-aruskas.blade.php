@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('aruskas/persetujuan?')?>">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sumber Kas</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="kas" class="form-control select2">
                                <option value="all">All</option>
                                <optgroup label="Kas">
                                @foreach ($kas as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{ucwords($row->label)}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                                <optgroup label="Rekening">
                                @foreach ($rekening as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Transaksi</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <div class="col-sm-2 mg-t-10 mg-sm-t-0 tx-center" style="margin:auto;">
                        sampai dengan
                        </div>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Arus</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="arus" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$arus as $key => $val){
                                        if(!empty($this->input->get('arus')) && $this->input->get('arus') == $key){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Transaksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
            <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' id='setujuAll'><i class='fa fa-check'> Setujui yang Dipilih</i></button> 
            <button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' id='tolakAll'><i class='fa fa-times'> Tolak yang Dipilih</i></button> 

        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th><input class="form-check-input" type="checkbox" onchange="checkAll(this)" name="chk[]" ></th>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kas</th>
                    <th data-sortable="true">Arus</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Nominal</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            if($row->is_approval == 0){
                                echo '
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input multi-check" id="customCheck-'.$row->id.'" data-id="'.$row->id.'">
                                        <label class="custom-control-label" for="customCheck-'.$row->id.'">   </label>
                                    </div>
                                </td>';
                            }else{
                                echo "<td></td>";
                            }
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row->is_approval == 0){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row->id."'><i class='fa fa-check'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Tolak pengajuan?' onclick='tolak(this)' data-id='".$row->id."'><i class='fa fa-times'></i></button> ";
                            }else if($row->is_approval == 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Setujui pengajuan?' onclick='setuju(this)' data-id='".$row->id."'><i class='fa fa-check'></i></button> ";
                            }
                            echo "</td>";
                            if($row->is_approval == 0){
                                echo "<td><label class='badge badge-secondary'>Menunggu</label></td>";
                            }else if($row->is_approval == 1){
                                echo "<td><label class='badge badge-success' data-toggle='tooltip' data-placement='top' title='".$row->remark."'>Disetujui</label><br>
                                        <label class='badge badge-light'>".ucwords($row->username_approval)." | ".$row->approval_timestamp."</label>
                                </td>";                               
                            }else{
                                echo "<td><label class='badge badge-danger' data-toggle='tooltip' data-placement='top' title='".$row->remark."'>Ditolak</label><br>
                                        <label class='badge badge-light'>".ucwords($row->username_approval)." | ".$row->approval_timestamp."</label>
                                </td>";        
                            }
                            echo "<td>".date('Y-m-d', strtotime($row->tgl_transaksi))."</td>";
                            echo "<td><label class='badge badge-light'>".$row->kas."</label></td>";
                            if($row->arus == 'in'){
                                echo "<td><label class='badge badge-primary'>in</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>out</label></td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row->sumber."</label></td>";
                            echo "<td>".monefy($row->nominal, false)."</td>";
                            echo "<td>".$row->keterangan."</td>";
                            echo "<td>".$row->deskripsi."</td>";

                            echo "<td><label class='badge badge-light'>".$row->username_input."</label><br><label class='badge badge-light'>".$row->input_timestamp."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
        // dropdownParent: $('#modal_form')
    });
    
});

function checkAll(ele) {
       var checkboxes = document.getElementsByTagName('input');
       if (ele.checked) {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox'  && !(checkboxes[i].disabled) ) {
                   checkboxes[i].checked = true;
               }
           }
       } else {
           for (var i = 0; i < checkboxes.length; i++) {
               if (checkboxes[i].type == 'checkbox') {
                   checkboxes[i].checked = false;
               }
           }
       }
   }

$('#setujuAll').click(function(){
    let data_id = [];
    $('#tabel_pembelian').each(function(a, b) {
        $('.multi-check:checked', b).each(function(c,d){
            data_id.push($(d).data('id'));
            
        })
    
    });
    if(data_id.length > 0){
        console.log(data_id);
        Swal.fire({
            title: 'Setujui?',
            text: data_id.length+" data transaksi akan disetujui, lanjutkan?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (!result.dismiss) {
                $.ajax({
                    url: "<?= base_url('aruskas/approve_multi_transaksi')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'data_id' : data_id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {
                    }
                });
            }
        })
    }else{
    Swal.fire({
            title: 'Error!',
            text: "Silahkan tandai data yang ingin disetujui",
            type: 'error',
            confirmButtonText: 'Tutup',
        }).then()
    }
}); 

$('#tolakAll').click(function(){
    let data_id = [];
    $('#tabel_pembelian').each(function(a, b) {
        $('.multi-check:checked', b).each(function(c,d){
            data_id.push($(d).data('id'));
            
        })
    
    });
    if(data_id.length > 0){
        Swal.fire({
            title: 'Tolak?',
            text: data_id.length+" data transaksi akan ditolak, lanjutkan?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (!result.dismiss) {
                $.ajax({
                    url: "<?= base_url('aruskas/decline_multi_transaksi')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'data_id' : data_id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {
                    }
                });
            }
        })
    }else{
    Swal.fire({
            title: 'Error!',
            text: "Silahkan tandai data yang ingin disetujui",
            type: 'error',
            confirmButtonText: 'Tutup',
        }).then()
    }
}); 

function setuju(el) {
    Swal.fire({
        title: 'Setujui?',
        text: "Data transaksi akan disetujui, lanjutkan?",
        type: 'info',
        input: 'text',
        inputPlaceholder: 'Masukkan catatan jika ada',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (!result.dismiss) {
            $.ajax({
                url: "<?= base_url('aruskas/approve_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                    'remark' : result.value
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
    
}

function tolak(el) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Data transaksi akan ditolak, lanjutkan?",
        type: 'warning',
        input: 'text',
        inputPlaceholder: 'Masukkan catatan jika ada',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (!result.dismiss) {
            $.ajax({
                url: "<?= base_url('aruskas/decline_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id' : $(el).data().id,
                    'remark' : result.value
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                }
            });
        }
    })
}
</script>
@end