@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('stok/gabah/rekap?')?>" id="form-filter">
    
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Jenis Gabah</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="gabah" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    $get = $this->input->get();
                                    foreach($gabah as $row){
                                        if(isset($get['gabah']) && $get['gabah'] == $row){
                                            echo '<option value="'.$row.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Jenis Padi</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="padi" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($padi as $row){
                                        if(isset($get['padi']) && $get['padi'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Pengeringan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="pengeringan" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($pengeringan as $row){
                                        if(isset($get['pengeringan']) && $get['pengeringan'] == $row){
                                            echo '<option value="'.$row.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Proses (KG)</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="proses" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($proses as $row){
                                        if(isset($get['proses']) && $get['proses'] == $row){
                                            echo '<option value="'.$row.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$row.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" name="start" class="form-control" value="{{isset($get['start'])? $get['start']:''}}" required>
                        </div>
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" name="end" class="form-control" value="{{isset($get['end'])? $get['end']:''}}" required>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button> 
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Rekap Stok Gabah</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Jenis Gabah</th>
                    <th data-sortable="true">Jenis Padi</th>
                    <th data-sortable="true">Pengeringan</th>
                    <th data-sortable="true">Proses</th>
                    <th data-sortable="true">Harga Satuan</th>
                    <th data-sortable="true" class="text-center">Stok Awal</th>
                    <th data-sortable="true" class="text-center">Stok Bertambah</th>
                    <th data-sortable="true" class="text-center">Stok Berkurang</th>
                    <th data-sortable="true">Stok Sisa</th>
                    <th data-sortable="true">Nilai Sisa Stok</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $stok_awal = 0;
                    $stok_bertambah = 0;
                    $stok_berkurang = 0;
                    $stok_sisa = 0;
                    $total = 0;
                    if(!empty($stok)){
                        foreach($stok as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Detail Transaksi' onclick='detail(this)' data-id='".$row['id']."' data-gabah='".$row['jenis_gabah']."' data-padi='".$row['id_jenis_padi']."' data-pengeringan='".$row['jenis_pengeringan']."' data-proses='".$row['jenis_proses']."'><i class='fa fa-search'></i></button> 
                            <button type='button' class='btn btn-dark' data-toggle='tooltip' data-placement='top' title='Export Transaksi' onclick='export_detail(this)' data-id='".$row['id']."' data-gabah='".$row['jenis_gabah']."' data-padi='".$row['id_jenis_padi']."' data-pengeringan='".$row['jenis_pengeringan']."' data-proses='".$row['jenis_proses']."'><i class='fa fa-print'></i></button>";
                            echo "</td>";
                            if($row['jenis_gabah'] == 'ks'){
                                echo "<td><label class='badge badge-primary'>KS</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>KG</label></td>";
                            }
                            echo "<td>".$row['padi']."</td>";
                            echo "<td>".strtoupper($row['jenis_pengeringan'])."</td>";
                            echo "<td>".strtoupper($row['jenis_proses'])."</td>";
                            echo "<td>".monefy($row['avg_harga_pokok'], false)."</td>";
                            echo "<td>".$row['jml_stok_awal']."</td>";
                            if($row['jml_stok_bertambah'] > 0){
                                echo "<td><label class='tx-success'>+ ".$row['jml_stok_bertambah']."</label></td>";
                            }else{
                                echo "<td></td>";
                            }
                            if($row['jml_stok_terpakai'] > 0){
                                echo "<td><label class='tx-danger'>- ".$row['jml_stok_terpakai']."</label></td>";
                            }else{
                                echo "<td></td>";
                            }
                            echo "<td><b>".$row['jml_sisa_stok']."<b></td>";
                            echo "<td>".monefy($row['jml_sisa_stok'] * $row['avg_harga_pokok'], false)."</td>";
                            echo "</tr>";
                            $stok_awal += $row['jml_stok_awal'];
                            $stok_bertambah += $row['jml_stok_bertambah'];
                            $stok_berkurang += $row['jml_stok_terpakai'];
                            $stok_sisa += $row['jml_sisa_stok'];
                            $total += ($row['jml_sisa_stok'] * $row['avg_harga_pokok']);
                        }
                    }
				?>
                <tr class="tx-bold tx-16">
                    <td colspan="7">Total</td>
                    <td>{{monefy($stok_awal, false)}}</td>
                    <td><label class='tx-success'>{{monefy($stok_bertambah, false)}}</label></td>
                    <td><label class='tx-danger'>{{monefy($stok_berkurang, false)}}</label></td>
                    <td>{{monefy($stok_sisa, false)}}</td>
                    <td>Rp. {{monefy($total, false)}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Transaksi Stok</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-home-tab">
                        <table class="table" id="tabel-detail">
                            <thead>
                                <tr>
                                    <th data-sortable="true">Tanggal</th>
                                    <th data-sortable="true">Arus</th>
                                    <th data-sortable="true">Sumber</th>
                                    <th data-sortable="true">Supplier</th>
                                    <th data-sortable="true">Gabah</th>
                                    <th data-sortable="true">Padi</th>
                                    <th data-sortable="true">Kuantitas</th>
                                    <th data-sortable="true">Harga</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
		toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

$('#form-transaksi [name=jenis_gabah]').change(function(){
    cek_jenis_gabah();
})

function detail(el) {
    $.ajax({
        url: "<?= base_url('api/internal/stok/gabah/json_get_detail_transaksi_stok')?>",
        type: "POST",
        dataType: "json",
        data: {
            'gabah': $(el).data().gabah,
            'padi': $(el).data().padi,
            'pengeringan': $(el).data().pengeringan,
            'proses': $(el).data().proses,
            'start' : $('[name=start]').val(),
            'end' : $('[name=end]').val()
        },
        success: function(result) {
            $('#tabel-detail').bootstrapTable('destroy');
            $('#tabel-detail').find('tbody').empty();
            var sisa = 0;
            let supplier = "";
            $(result).each(function(a,b){
                var label_arus = "";
                var label_qty = "";
                if(b.arus == 'in'){
                    label_arus = '<label class="badge badge-primary">in</label>';
                    label_qty = '<label class="tx-success">+'+b.kuantitas+'</label>';
                    sisa += parseInt(b.kuantitas);
                }else{
                    label_arus = '<label class="badge badge-warning">out</label>';
                    label_qty = '<label class="tx-danger">-'+b.kuantitas+'</label>';
                    sisa -= parseInt(b.kuantitas);
                }
                if(b.supplier == null){
                    supplier = "master";
                }else{
                    supplier = b.supplier;
                }
                $('#tabel-detail').find('tbody').append('\
                    <tr>\
                    <td>'+b.tgl+'</td>\
                    <td>'+label_arus+'</td>\
                    <td>'+b.sumber+'</td>\
                    <td>'+supplier+'</td>\
                    <td>'+b.jenis_gabah+'</td>\
                    <td>'+b.padi+'</td>\
                    <td>'+label_qty+'</td>\
                    <td>'+b.harga+'</td>\
                    </tr>\
                ');
            });
            $('#tabel-detail').bootstrapTable({
                pagination: true,
                showToggle: true,
                showColumns: true,
                search:true,
                pageSize:10,
                striped: true,
                showFilter: true,
                toolbar: '#toolbar'
            });
            $('#modal_form').modal('show');
        },
        error: function(e) {
            console.log(e);
        },
        complete: function(e) {}
    });
    
}

function cek_jenis_gabah(){
    if($('#form-transaksi [name=jenis_gabah]:checked').val() == 'ks'){
        $('.pengeringan').show(300);
        $('.proses').hide(300);
        $('#form-transaksi [name=jenis_proses]').prop('checked', false);
    }else{
        $('.pengeringan').hide(300);
        $('.proses').show(300);
        $('#form-transaksi [name=jenis_pengeringan]').prop('checked', false);
    }
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let gabah = $("#form-filter [name=gabah]").val();
    let padi = $("#form-filter [name=padi]").val();
    let pengeringan = $("#form-filter [name=pengeringan]").val();
    let proses = $("#form-filter [name=proses]").val();
    if (start != '' && end != '') {
        var url = "<?= base_url('stok/gabah/export?')?>"+'start='+start+'&end='+end+'&gabah='+gabah+'&padi='+padi+'&pengeringan='+pengeringan+'&proses='+proses;
        window.open(url, '_blank');
    } else {
        Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    }
}

function export_detail(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let gabah = $(el).data().gabah;
    let padi = $(el).data().padi;
    let pengeringan = $(el).data().pengeringan;
    let proses = $(el).data().proses;
    if (start != '' && end != '') {
        var url = "<?= base_url('stok/gabah/export_detail?')?>"+'start='+start+'&end='+end+'&gabah='+gabah+'&padi='+padi+'&pengeringan='+pengeringan+'&proses='+proses;
        window.open(url, '_blank');
    } else {
        Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    }
}
</script>
@end