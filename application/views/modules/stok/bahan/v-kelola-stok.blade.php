@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('stok/bahan/kelola?')?>">
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Transaksi</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Jenis Bahan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="bahan" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    $get = $this->input->get();
                                    foreach($bahan as $row){
                                        if(isset($get['bahan']) && $get['bahan'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Status Stok</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Stok Bahan</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Stok</button>
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Sumber</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Bahan</th>
                    <th data-sortable="true" class="text-center">Stok Awal</th>
                    <th data-sortable="true" class="text-center">Stok Sisa</th>
                    <th data-sortable="true">Harga Satuan</th>
                    <th data-sortable="true">Nilai Stok</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['sumber'] == 'master-transaksi'){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Edit transaksi' onclick='edit(this)' data-id='".$row['id']."'><i class='fa fa-edit'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                            }
                            echo "</td>";
                            if($row['id_supplier'] == 0){
                                echo "<td><label class='badge badge-light'>".$row['sumber']."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['sumber']."</label><br>
                                <label class='badge badge-dark'>".ucwords($list_supplier[$row['id_supplier']]['nama'])."</label>
                                </td>";
                            }
                            echo "<td>".$row['tanggal']."</td>";
                            echo "<td>".strtoupper($row['bahan'])."</td>";
                            echo "<td>".monefy($row['stok'], false)."</td>";
                            echo "<td>".monefy($row['stok_sisa'], false)."</td>";
                            echo "<td>".monefy($row['harga_satuan'], false)."</td>";
                            echo "<td>".monefy($row['stok_sisa'] * $row['harga_satuan'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Stok Bahan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('stok/bahan/submit_stok_bahan')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="id" value="">
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="date" class="form-control" name="tanggal" required=""> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sumber <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_supplier" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            <option value="0">Master Transaksi</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jenis Bahan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_jenis_bahan" class="form-control reset-form select2-modal" required="">
                                            <option value="">- Plih Salah Satu -</option>
                                            @foreach ($bahan as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Stok Awal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="stok" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Stok Sisa <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="stok_sisa" required=""> 
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon1">kg</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label">Harga Satuan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="number" class="form-control" name="harga_satuan" required=""> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });
    $('.pengeringan, .proses').hide();
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $("#tabel_jenis_sak tbody").empty();
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/stok/bahan/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                console.log(result);
                $('#form-transaksi').trigger('reset');
                $('#form-transaksi [name=id]').val(result.id);
                $('#form-transaksi [name=tanggal]').val(result.tanggal);
                $('#form-transaksi [name=id_supplier]').val(result.id_supplier).change();
                $('#form-transaksi [name=id_jenis_bahan').val(result.id_jenis_bahan).change();
                $('#form-transaksi [name=stok]').val(result.stok);
                $('#form-transaksi [name=stok_sisa]').val(result.stok_sisa);
                $('#form-transaksi [name=harga_satuan]').val(result.harga_satuan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}


function hapus_data(e) {
    Swal.fire({
        title: 'Konfirmasi',
        text: "Apakah anda yakin ingin menghapus data ini?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('api/internal/stok/bahan/delete_data_transaksi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}
</script>
@end