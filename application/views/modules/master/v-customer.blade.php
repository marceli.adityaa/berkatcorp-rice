@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('master/customer?')?>">
                    <?php $get = $this->input->get()?>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jenis</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="jenis" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($jenis as $key => $val){
                                        if(isset($get['jenis']) && $get['jenis'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($val).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($val).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Area</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="area" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($area as $row){
                                        if(isset($get['area']) && $get['area'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Master Customer</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table w-100 table-striped mg-t-10 table-white" id="tabel_customer">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                    <th data-sortable="true">Area</th>
                    <th data-sortable="true">Nama</th>
                    <th data-sortable="true">Inisial</th>
                    <th data-sortable="true">Peranan</th>
                    <th data-sortable="true">Telp</th>
                    <!-- <th data-sortable="true">Saldo Sak</th>
                    <th data-sortable="true">Saldo Bon</th>
                    <th data-sortable="true">Limit Bon</th>
                    <th data-sortable="true">Penyuplai</th> -->
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($customer)){
					foreach($customer as $row){
						echo "<tr>";
						echo "<td class='text-center'>".$no++."</td>";
						echo "<td class='text-nowrap'>";
						echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
						if($row['status'] == 1){
							echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set Nonaktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button>";
						}else{
							echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set Aktif' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
						}
                        echo "</td>";
                        if($row['status'] == 1){
							echo "<td><label class='badge badge-success'>Aktif</label></td>";
						}else{
							echo "<td><label class='badge badge-secondary'>Nonaktif</label></td>";
						}
                        echo "<td>".$row['area']."</td>";
                        echo "<td>".$row['nama']."</td>";
                        echo "<td>".strtoupper($row['initial'])."</td>";

                        echo "<td>";
                            if($row['is_supplier'] == 1){
                                echo "<label class='badge badge-light'>Supplier</label><br>";
                            }
                            if($row['is_customer'] == 1){
                                echo "<label class='badge badge-light'>Customer</label><br>";
                            }
                        echo "</td>";
                        echo "<td>".$row['telp']."</td>";

                        // echo "<td>".$row['saldo_sak']."</td>";
                        // echo "<td>Rp ".monefy($row['saldo_bon'], false)."</td>";
                        // echo "<td>Rp ".monefy($row['limit_bon'], false)."</td>";
                        // echo "<td>";
                        //     if($row['supply_gabah'] == 1){
                        //         echo "<label class='badge badge-light'>Gabah</label><br>";
                        //     }
                        //     if($row['supply_beras'] == 1){
                        //         echo "<label class='badge badge-light'>Beras</label><br>";
                        //     }
                        // echo "</td>";
                        
						echo "</tr>";
                    }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('master/customer/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data Customer</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Nama Customer <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="nama" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Initial <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="initial" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Peranan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <label class='ckbox mg-l-5'><input type='checkbox' name='is_supplier' value='1'><span>Supplier</span></label>
                                <label class='ckbox mg-l-5'><input type='checkbox' name='is_customer' value='1'><span>Customer</span></label>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Area <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_area" class="form-control" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($area as $row)
                                    <option value="{{$row['id']}}">{{$row['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Alamat <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="alamat" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Telpon <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control numeric" name="telp" autocomplete="off">
                            </div>
                        </div>
                        <!-- <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Saldo Sak <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control numeric" name="saldo_sak" autocomplete="off" value="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Saldo Bon <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control autonumeric" name="saldo_bon" autocomplete="off" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Limit Bon <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control autonumeric" name="limit_bon" autocomplete="off" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Penyuplai <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <label class='ckbox mg-l-5'><input type='checkbox' name='supply_beras' value='1'><span>Beras</span></label>
                                <label class='ckbox mg-l-5'><input type='checkbox' name='supply_gabah' value='1'><span>Gabah</span></label>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_customer').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
		toolbar: '#toolbar'
    });
    $('[name=saldo_bon], [name=limit_bon]').autoNumeric('init', {
        'mDec': 0
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/customer/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=id_area]').val(result.id_area).change();
                $('[name=alamat]').val(result.alamat);
                $('[name=telp]').val(result.telp);
                $('[name=initial]').val(result.initial);
                $('[name=saldo_bon]').val(result.saldo_bon);
                $('[name=limit_bon]').val(result.limit_bon);
                $('[name=saldo_bon], [name=limit_bon]').autoNumeric('destroy');
                $('[name=saldo_bon], [name=limit_bon]').autoNumeric('init', {
                    'mDec': 0
                });
                if (result.supply_beras == 1) {
                    $('[name=supply_beras]').prop('checked', true);
                }
                if (result.supply_gabah == 1) {
                    $('[name=supply_gabah]').prop('checked', true);
                }
                if (result.is_supplier == 1) {
                    $('[name=is_supplier]').prop('checked', true);
                }
                if (result.is_customer == 1) {
                    $('[name=is_customer]').prop('checked', true);
                }
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/customer/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/customer/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end