@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('master/merk_beras/kemasan?')?>">
                    <?php $get = $this->input->get()?>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Nama Merk</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="merk" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($merk as $val){
                                        if(isset($get['merk']) && $get['merk'] == $val['id']){
                                            echo '<option value="'.$val['id'].'" selected>'.strtoupper($val['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$val['id'].'">'.strtoupper($val['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php 
                                    foreach($status as $key => $row){
                                        if(isset($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.strtoupper($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.strtoupper($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Kemasan Beras</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
		</div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_merk_beras">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Nama Merk</th>
                    <th data-sortable="true">Kemasan</th>
                    <th data-sortable="true">Harga Terbaru</th>
                    <th data-sortable="true">Tanggal Berlaku</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($kemasan)){
                        foreach($kemasan as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='left' title='Riwayat Harga' onclick='riwayat(this)' data-id='".$row['id']."'><i class='fa fa-history'></i></button> 
                            <button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
                            if($row['status'] == 1){
                                echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set Nonaktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button>";
                            }else{
                                echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set Aktif' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
                            }
                            
                            echo "</td>";
                            echo "<td>".$row['merk']."</td>";
                            echo "<td>".$row['kemasan']."</td>";
                            echo "<td>".monefy($row['harga'],false)."</td>";
                            echo "<td>".$row['last_update']."</td>";
                            if($row['status'] == 1){
                                echo "<td><label class='badge badge-success'>Aktif</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Nonaktif</label></td>";
                            }
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('master/merk_beras/submit_kemasan_beras')?>" id="form-kemasan">
                <div class="modal-header bg-midnightblack">
					<h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Kemasan Beras</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4">Merk Beras <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_merk" class="form-control" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($merk as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4">Kemasan<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="number" class="form-control" name="kemasan" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4">Harga<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="number" class="form-control" name="harga" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4">Tanggal Berlaku<span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" class="form-control" name="last_update" autocomplete="off" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_riwayat" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Riwayat Kemasan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped mg-t-10 table-white" id="tabel_riwayat">
                    <thead>
                        <tr>
                            <th data-sortable="true" data-filterable="true">Merk</th>
                            <th data-sortable="true" data-filterable="true">Kemasan</th>
                            <th data-sortable="true" data-filterable="true">Tanggal</th>
                            <th data-sortable="true" data-filterable="true">Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_merk_beras').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
		toolbar: '#toolbar'
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/merk_beras/json_get_detail_kemasan')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#form-kemasan').trigger('reset');
                $('#form-kemasan [name=id]').val(result.id);
                $('#form-kemasan [name=id_merk]').val(result.id_merk).change();
                $('#form-kemasan [name=kemasan]').val(result.kemasan);
                $('#form-kemasan [name=harga]').val(result.harga);
                $('#form-kemasan [name=last_update]').val(result.last_update);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function riwayat(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/merk_beras/json_get_riwayat_kemasan')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#tabel_riwayat').bootstrapTable('destroy');
                $('#tabel_riwayat').find('tbody').empty();
                $(result).each(function(a,b){
                    $('#tabel_riwayat').find('tbody').append('\
                        <tr>\
                        <td>'+b.merk+'</td>\
                        <td>'+b.kemasan+'</td>\
                        <td>'+b.tanggal+'</td>\
                        <td>'+b.harga+'</td>\
                        </tr>\
                    ');
                });
                init_table_riwayat();
                $("#modal_riwayat").modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/merk_beras/set_kemasan_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('api/internal/merk_beras/set_kemasan_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function init_table_riwayat(){
    $('#tabel_riwayat').bootstrapTable({
        pagination: true,
        showToggle: true,
        showColumns: true,
        search:true,
        pageSize:10,
        striped: true,
        showFilter: true,
    });
}
</script>
@end