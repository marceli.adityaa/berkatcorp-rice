@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Master Proses Produksi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
		</div>
        <table class="table table-striped mg-t-10 table-white" id="tabel-data">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Nama Proses</th>
                    <th data-sortable="true" class="text-center">Arus</th>
                    <th data-sortable="true" class="text-center">Cor</th>
                    <th data-sortable="true" class="text-center">Return</th>
                    <th data-sortable="true" class="text-center">Sisa Produksi</th>
                    <th data-sortable="true" class="text-center">Urutan</th>
                    <th data-sortable="true" data-searchable="false" class="text-center">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($list)){
                        foreach($list as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
                            if($row['status'] == 1){
                                echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set Nonaktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button>";
                            }else{
                                echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set Aktif' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
                            }
                            
                            echo "</td>";
                            echo "<td>".$row['nama']."</td>";
                            if($row['arus'] == 'in'){
                                echo "<td><label class='badge badge-primary'>in</label></td>";
                            }else{
                                echo "<td><label class='badge badge-warning'>out</label></td>";
                            }
                            
                            if($row['is_multiply_cor']){
                                echo "<td><label class='badge badge-success'>ya</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>tidak</label></td>";
                            }

                            if($row['is_return']){
                                echo "<td><label class='badge badge-success'>ya</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>tidak</label></td>";
                            }

                            if($row['is_sisa_produksi']){
                                echo "<td><label class='badge badge-success'>ya</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>tidak</label></td>";
                            }

                            echo "<td>#".$row['urutan']."</td>";
    
                            if($row['status'] == 1){
                                echo "<td><label class='badge badge-success'>Aktif</label></td>";
                            }else{
                                echo "<td><label class='badge badge-secondary'>Nonaktif</label></td>";
                            }
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('master/proses_produksi/submit_form')?>">
                <div class="modal-header bg-midnightblack">
					<h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Data</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Nama Proses <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="nama" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Arus <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="arus" class="custom-control-input" value="in" required="">
                                    <label class="custom-control-label" for="customRadioInline1">in</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="arus" class="custom-control-input" value="out" required="">
                                    <label class="custom-control-label" for="customRadioInline2">out</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Cor <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline3" name="is_multiply_cor" class="custom-control-input" value="1" required="">
                                    <label class="custom-control-label" for="customRadioInline3">Ya</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline4" name="is_multiply_cor" class="custom-control-input" value="0" required="">
                                    <label class="custom-control-label" for="customRadioInline4">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Return <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline5" name="is_return" class="custom-control-input" value="1" required="">
                                    <label class="custom-control-label" for="customRadioInline5">Ya</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline6" name="is_return" class="custom-control-input" value="0" required="">
                                    <label class="custom-control-label" for="customRadioInline6">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Sisa Produksi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline7" name="is_sisa_produksi" class="custom-control-input" value="1" required="">
                                    <label class="custom-control-label" for="customRadioInline7">Ya</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline8" name="is_sisa_produksi" class="custom-control-input" value="0" required="">
                                    <label class="custom-control-label" for="customRadioInline8">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Urutan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="number" class="form-control" name="urutan" autocomplete="off" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel-data').bootstrapTable({
        pagination: true,
		search: true,
		toolbar: '#toolbar'
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/proses_produksi/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=arus][value='+result.arus+']').prop('checked', true);
                $('[name=is_multiply_cor][value='+result.is_multiply_cor+']').prop('checked', true);
                $('[name=is_return][value='+result.is_return+']').prop('checked', true);
                $('[name=is_sisa_produksi][value='+result.is_sisa_produksi+']').prop('checked', true);
                $('[name=urutan]').val(result.urutan);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/proses_produksi/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('master/proses_produksi/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end