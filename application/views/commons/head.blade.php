<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Meta -->
<meta name="description" content="Menjadi Perusahaan yang berkembang besar dan dapat menjadi berkat bagi orang lain melalui kualitas yang baik dari setiap produk yang disajikan.">
<meta name="author" content="Berkat Group">

<title>Berkat Rice Mills | <?=(!empty(get_session('title')) ? ucwords(get_session('title')) : 'Dashboard')?></title>

<!-- Bootstrap -->
<link href="{{base_url('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- ICON -->
<link href="{{base_url('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
<link href="{{base_url('assets/plugins/simple-line/css/simple-line.css')}}" rel="stylesheet">
<link href="{{base_url('assets/plugins/theme/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
<!-- Theme -->
<link href="{{base_url('assets/plugins/theme/lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
<link href="{{base_url('assets/plugins/theme/lib/jquery-toggles/toggles-full.css')}}" rel="stylesheet">
<!-- Bootstrap Table -->
<link rel="stylesheet" type="text/css" href="{{base_url('assets/plugins/bootstrap-table/bootstrap-table.css')}}">
<!-- Select2 -->
<link rel="stylesheet" type="text/css" href="{{base_url('assets/plugins/select2/css/select2.min.css')}}">
<!-- Datetime -->
<link rel="stylesheet" type="text/css" href="{{base_url('assets/plugins/datetime/datetime.css')}}">
<!-- App -->
<link rel="stylesheet" href="{{base_url('assets/plugins/theme/css/amanda.min.css')}}">
<link rel="stylesheet" href="{{base_url('assets/styles/app.css')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{base_url('assets/img/logo/small/berkat-group.png')}}">
@yield('style')