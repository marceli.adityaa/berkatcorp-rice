<!-- jquery -->
<script src="<?= base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap -->
<script src="<?= base_url('assets/plugins/bootstrap/js/popper.js') ?>"></script>
@yield('js')
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- Theme -->
<script src="<?= base_url('assets/plugins/theme/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/lib/jquery-toggles/toggles.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/js/amanda.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/js/ResizeSensor.js') ?>"></script>
<!-- Bootstrap Table -->
<script src="<?= base_url('assets/plugins/bootstrap-table/bootstrap-table.js') ?>"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{base_url('assets/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- datetime -->
<script type="text/javascript" src="{{base_url('assets/plugins/datetime/datetime.js')}}"></script>
<!-- moment -->
<script type="text/javascript" src="{{base_url('assets/plugins/moment/moment.js')}}"></script>
<!-- Sweet Alert -->
<script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js') ?>"></script>
<!-- App -->
<script src="<?= base_url('assets/scripts/app.js') ?>"></script>
<!-- Terbilang js -->
<script src="<?= base_url('assets/scripts/index.js') ?>"></script>
<!--masking curency-->
<script src="<?= base_url('assets/scripts/index.min.js') ?>"></script>

<!--loading screen-->
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>


<script>
@if($msg = $this -> session -> flashdata('message'))
Toast.fire('Pesan: ', '{{$msg[0]}}', '{{$msg[1]}}');
@endif

var submenu = "{{get_session('submenu')}}";
var menu = "{{get_session('menu')}}";

if (submenu != '') {
    $("." + submenu).next().slideToggle();
    // $("." + submenu).toggleClass('show-sub');
    $("." + submenu).toggleClass('active');
}

if (menu != '') {
    $("." + menu).toggleClass('active');
}
</script>
