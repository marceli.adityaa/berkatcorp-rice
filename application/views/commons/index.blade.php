<!DOCTYPE html>
<html lang="en">

<head>
    @include('commons/head')
    
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>
    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
        </div>
    </div>

    @include('commons/topbar')
    @include('commons/sidebar')

    <div class="am-mainpanel">
        <div class="am-pagebody">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <?php 
                        $breadcrumb = get_session('breadcrumb');
                        foreach($breadcrumb as $key => $val){
                            if($val != 'active'){
                                echo '
 
                                <li class="breadcrumb-item"><a href="'.$val.'">'.ucfirst($key).'</a></li>
                           
                                ';
                            }else{
                                echo '
                                
                                <li class="breadcrumb-item active" aria-current="page">'.ucfirst($key).'</li>
                                
                                ';
                            }
                        }
                    ?>
                </ol>
            </nav>
            @yield('content')

        </div>
    </div>
    @yield('modal')
    @include('commons/js')
</body>

</html>