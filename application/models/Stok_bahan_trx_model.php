<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_bahan_trx_model extends MY_Model {
	public $_table = 'stok_bahan_trx';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            return $this->db->get('stok_bahan_trx')->result();
        }else{
            return $this->db->get('stok_bahan_trx');
        }
    }

    public function hapus_trx_stok($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            $this->db->where('sumber', 'master-transaksi');
            return $this->db->delete('stok_bahan_trx');
        }else{
            return false;
        }
    }

    public function get_detail_rekap_stok($param){
        if(!isset($param['start']) || $param['start'] == ''){
            $param['start'] = '0000-00-00';
        }
        if(!isset($param['end']) || $param['end'] == ''){
            $param['end'] = date('Y-m-d');
        }

        $this->db->select('b.*, DATE_FORMAT(b.timestamp, "%Y-%m-%d") as tanggal, UPPER(c.nama) as bahan, d.nama as supplier');
        if(!empty($param['bahan']) && $param['bahan'] != 'all'){
            $this->db->where('a.id_jenis_bahan', $param['bahan']);
        }
        $this->db->where('b.tgl >=', $param['start']);
        $this->db->where('b.tgl <=', $param['end']);
        $this->db->join('stok_bahan_trx b', 'a.id = b.id_stok');
        $this->db->join('jenis_bahan c', 'a.id_jenis_bahan = c.id');
        $this->db->join('customer d','a.id_supplier = d.id', 'left');
        $this->db->order_by('b.tgl', 'desc');
        $result = $this->db->get('stok_bahan a');
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
    }
}