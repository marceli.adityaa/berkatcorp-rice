<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_akun_biaya_detail_model extends MY_Model {
	public $_table = 'master_akun_biaya_detail';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($id = null)
	{
		$this->db->order_by('status', 'desc');
		$this->db->order_by('nama');
		if(!empty($id)){
			$this->db->where('id', $id);
		}
		$result = $this->db->get('master_akun_biaya_detail');
        if ($result->num_rows() > 0) {
			if(!empty($id)){
				return $result->row_array();
			}else{
				return $result->result_array();
			}
        } else {
            return false;
        }
	}

	public function get_data_detail($kategori){
        $this->db->select('b.*, a.nama as kategori');
		$this->db->join('master_akun_biaya_detail b', 'a.id = b.id_biaya');
		$this->db->order_by('b.status', 'desc');
		$this->db->order_by('b.nama');
		if(!empty($kategori)){
			$this->db->where('b.id_biaya', $kategori);
		}
		$result = $this->db->get('master_akun_biaya a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_active()
	{
		$this->db->select('b.*, a.nama as kategori');
		$this->db->join('master_akun_biaya_detail b', 'a.id = b.id_biaya');
		$this->db->where('a.status', 1);
		$this->db->where('b.status', 1);
		$this->db->order_by('a.nama');
		$this->db->order_by('b.nama');
		$result = $this->db->get('master_akun_biaya a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}