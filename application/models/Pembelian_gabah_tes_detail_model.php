<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_tes_detail_model extends MY_Model
{
    public $_table = 'pembelian_gabah_tes_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_detail($id){
        $this->db->select('a.jenis_gabah, d.nama as padi, c.*');
        $this->db->join('pembelian_gabah_tes_detail c', 'a.id = c.id_tes');
        $this->db->join('jenis_padi d', 'a.id_jenis_padi = d.id');
        $this->db->where('c.id_tes', $id);
        $this->db->order_by('c.batch_tes');
        $results = $this->db->get('pembelian_gabah_tes a');
        if ($results->num_rows() > 0) {
            return $results->result_array();
        } else {
            return false;
        }
    }
}
