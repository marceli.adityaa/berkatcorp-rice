<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_jenis_model extends MY_Model
{
    public $_table = 'pembelian_gabah_jenis';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        // $this->db->join('')
        $this->db->where('a.id_pembelian', $id);
        $this->db->order_by('a.batch');
        $result = $this->db->get('pembelian_gabah_batch a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_jenis_pembelian($pembelian, $manual = 0, $gabah = null){
        if($manual == 1){
            $this->db->select('c.id as id_jenis, b.id_pembelian, c.id_batch, b.batch_label, b.batch, b.berat_netto, c.jenis_gabah, c.id_jenis_padi, e.nama as padi, sum(jumlah_sak) as jumlah_sak, sum(berat_basah) as total_berat_basah, c.potong_sak, c.potong_lain, c.hampa, c.persen_broken, c.harga_kepala, c.harga_broken, c.rendemen_kg, c.harga_beras_medium');
        }else{
            $this->db->select('c.id as id_jenis, b.id_pembelian, c.id_batch, b.batch_label, b.batch, b.berat_netto, c.jenis_gabah, c.id_jenis_padi, e.nama as padi, sum(jumlah_sak) as jumlah_sak, c.potong_sak, c.potong_lain, c.hampa, c.persen_broken, c.harga_kepala, c.harga_broken, c.rendemen_kg, c.harga_beras_medium');
        }
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $this->db->join('jenis_padi e', 'c.id_jenis_padi = e.id');
        $this->db->where('a.id', $pembelian);
        $this->db->where('b.is_manual', $manual);
        if(!empty($gabah)){
            $this->db->where('c.jenis_gabah', $gabah);
        }
        $this->db->group_by('b.id');
        $this->db->group_by('c.jenis_gabah');
        $this->db->group_by('c.id_jenis_padi');
        $this->db->order_by('batch');
        $this->db->order_by('jenis_gabah');
        $this->db->order_by('e.nama');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_jenis_timbang_kecil($id_batch,  $gabah = null){

        $this->db->select('c.id as id_jenis, b.id_pembelian, c.id_batch, b.batch_label, b.batch, b.berat_netto, c.jenis_gabah, c.id_jenis_padi, e.nama as padi, sum(jumlah_sak) as jumlah_sak, sum(berat_basah) as total_berat_basah, c.potong_sak, c.potong_lain, c.hampa, c.persen_broken, c.harga_kepala, c.harga_broken, c.rendemen_kg, c.harga_beras_medium');
        
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $this->db->join('jenis_padi e', 'c.id_jenis_padi = e.id');
        $this->db->where('b.id', $id_batch);
        $this->db->where('b.is_manual', 1);
        if(!empty($gabah)){
            $this->db->where('c.jenis_gabah', $gabah);
        }
        $this->db->group_by('b.id');
        $this->db->group_by('c.jenis_gabah');
        $this->db->group_by('c.id_jenis_padi');
        $this->db->order_by('batch');
        $this->db->order_by('jenis_gabah');
        $this->db->order_by('e.nama');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_input_harga($pembelian, $gabah = null){
        $this->db->select('c.id as id_jenis, c.id_batch, b.batch_label, b.batch, b.id_pembelian, b.berat_netto, c.jenis_gabah, c.id_jenis_padi, e.nama as padi, sum(jumlah_sak) as jumlah_sak, c.potong_sak, c.potong_lain, c.hampa, c.harga_nol_gabuk, b.is_manual, harga_kg, tambahan, subtotal, berat_beras_netto');
        
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $this->db->join('jenis_padi e', 'c.id_jenis_padi = e.id');
        $this->db->where('a.id', $pembelian);
        if(!empty($gabah)){
            $this->db->where('c.jenis_gabah', $gabah);
        }
        $this->db->group_by('b.id');
        if(empty($gabah)){
            $this->db->group_by('c.id');
        }
        $this->db->group_by('c.id_jenis_padi');
        $this->db->order_by('b.batch');
        $this->db->order_by('is_manual');
        $this->db->order_by('e.nama');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_detail_input_harga($id, $gabah = null)
    {
        $this->db->select('a.id, b.id as id_batch, d.id_jenis, b.batch, b.batch_label, b.berat_netto, (b.berat_netto - berat_timbang_kecil) as berat_netto_nett, c.jenis_gabah, c.id_jenis_padi, e.nama as padi, c.pengeringan, c.proses, d.sap, f.id as id_ka, f.nama as label_ka, f.kadar_air, d.jumlah_sak, d.is_default, b.is_manual, harga_kg');
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $this->db->join('jenis_padi e', 'c.id_jenis_padi = e.id');
        $this->db->join('kadar_air f', 'd.kadar_air = f.id');
        $this->db->where('a.id', $id);
        if(!empty($gabah)){
            $this->db->where('jenis_gabah', $gabah);
        }
        $this->db->order_by('is_manual');
        $this->db->order_by('b.batch');
        $this->db->order_by('sap');
        $this->db->order_by('e.nama');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_berat_beras($pembelian)
    {
        $this->db->select('b.id_pembelian, c.jenis_gabah, c.id_jenis_padi, f.kadar_air, d.jumlah_sak');
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $this->db->join('jenis_padi e', 'c.id_jenis_padi = e.id');
        $this->db->join('kadar_air f', 'd.kadar_air = f.id');
        $this->db->where('a.id', $pembelian);
        $this->db->order_by('jenis_gabah');
        $this->db->order_by('e.nama');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_jenis_ks($id = null)
    {
        $this->db->where('a.id_batch', $id);
        $this->db->where('jenis_gabah', 'ks');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_jenis_kg($id = null)
    {
        $this->db->where('a.id_batch', $id);
        $this->db->where('jenis_gabah', 'kg');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function is_available($batch, $gabah, $padi, $sap = null, $pengeringan = null, $proses = null)
    {
        $this->db->select('a.*');
        $this->db->where('id_batch', $batch);
        $this->db->where('jenis_gabah', $gabah);
        $this->db->where('id_jenis_padi', $padi);
        if(!empty($sap)){
            $this->db->where('sap', $sap);
        }
        if(!empty($pengeringan)){
            $this->db->where('pengeringan', $pengeringan);
        }
        if(!empty($proses)){
            $this->db->where('proses', $proses);
        }
        $this->db->join('pembelian_gabah_detail b', 'a.id = b.id_jenis');
        $this->db->group_by('a.id');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function get_index_sap($id){
        $this->db->select('sap, id_batch');
        $this->db->where('a.id', $id);
        $this->db->order_by('sap', 'desc');
        $this->db->limit(1);
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->row_array()['sap']+1;
        } else {
            return 1;
        }
    }

    public function get_detail_muatan($batch, $order)
    {
        $this->db->select('a.id, a.id_batch, b.id as id_detail, jenis_gabah, id_jenis_padi, c.nama as padi, pengeringan, proses, d.id as id_ka, d.nama as kadar_air_label, d.kadar_air, sap, jumlah_sak, b.input_by, b.timestamp, b.berat_basah');
        $this->db->where('a.id_batch', $batch);
        $this->db->join('pembelian_gabah_detail b', 'a.id = b.id_jenis');
        $this->db->join('jenis_padi c', 'a.id_jenis_padi = c.id');
        $this->db->join('kadar_air d', 'b.kadar_air = d.id');
        if($order){
            $this->db->order_by('sap');
            $this->db->order_by('c.nama');
            $this->db->order_by('d.nama');
        }
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_label_muatan($batch)
    {
        $this->db->select('a.id, a.id_batch, b.id as id_detail, jenis_gabah, id_jenis_padi, c.nama as padi, f.kode_pembelian, f.tanggal_pembelian');
        $this->db->where('a.id_batch', $batch);
        $this->db->join('pembelian_gabah_detail b', 'a.id = b.id_jenis');
        $this->db->join('jenis_padi c', 'a.id_jenis_padi = c.id');
        $this->db->join('pembelian_gabah_batch e', 'a.id_batch = e.id');
        $this->db->join('pembelian_gabah f', 'e.id_pembelian = f.id');
        $this->db->group_by('a.jenis_gabah');
        $this->db->group_by('c.id');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_detail_sap($batch)
    {
        $this->db->select('sap');
        $this->db->where('a.id_batch', $batch);
        $this->db->join('pembelian_gabah_detail b', 'a.id = b.id_jenis');
        $this->db->order_by('sap');
        $this->db->group_by('sap');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_by($batch, $sap = null)
    {
        $this->db->select('a.id, a.id_batch, sap');
        $this->db->where('a.id_batch', $batch);
        if(!empty($sap)){
            $this->db->where('sap', $sap);
        }
        $this->db->join('pembelian_gabah_detail b', 'a.id = b.id_jenis');
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_label($id)
    {
        $this->db->select('a.*, b.nama as padi, c.batch, d.kode_pembelian, d.plat_kendaraan, d.tanggal_pembelian, d.berat_bruto, e.nama as supplier, c.is_manual');
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('pembelian_gabah_batch c', 'a.id_batch = c.id');
        $this->db->join('pembelian_gabah d', 'c.id_pembelian = d.id');
        $this->db->join('customer e', 'd.id_supplier = e.id');
        $this->db->where('a.id', $id);
        $result = $this->db->get('pembelian_gabah_jenis a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function update_hampa($data, $where)
    {
        $this->db->where($where);
        return $this->db->update('pembelian_gabah_jenis', $data);
    }

    public function delete_by_batch($batch){
        $this->db->where('id_batch', $batch);
        return $this->db->delete('pembelian_gabah_jenis');
    }

}
