<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_retur_model extends MY_Model {
	public $_table = 'penjualan_retur';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id)
    {
        $this->db->select('a.*, b.nama as customer, b.alamat as customer_alamat, b.telp as customer_telp');
        $this->db->where('a.id', $id);
        $this->db->join('customer b', 'a.id_customer = b.id');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
			return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_trx()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer,c.nama as area, ifnull(sum(d.kuantitas*e.kemasan),0) as tonase, ifnull(sum(d.kuantitas*d.harga_satuan),0) as nominal');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);  
        }
        if(isset($get['area']) && $get['area'] != 'all'){
            $this->db->where('b.id_area', $get['area']);  
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # belum diisi
                $this->db->where('a.status', 0);
            }else if($get['status'] == 2){
                # sudah diisi
                $this->db->where('a.status', 1);
            }
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('area c', 'b.id_area = c.id');
        $this->db->join('penjualan_retur_detail d', 'a.id = d.id_retur', 'left');
        $this->db->join('merk_beras_kemasan e', 'd.id_kemasan = e.id', 'left');
        $this->db->group_by('a.id');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer,c.nama as area, d.*, e.kemasan, f.nama as merk');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);  
        }
        if(isset($get['area']) && $get['area'] != 'all'){
            $this->db->where('b.id_area', $get['area']);  
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # belum diisi
                $this->db->where('a.status', 0);
            }else if($get['status'] == 2){
                # sudah diisi
                $this->db->where('a.status', 1);
            }
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('area c', 'b.id_area = c.id');
        $this->db->join('penjualan_retur_detail d', 'a.id = d.id_retur', 'left');
        $this->db->join('merk_beras_kemasan e', 'd.id_kemasan = e.id', 'left');
        $this->db->join('merk_beras f', 'e.id_merk = f.id', 'left');
        $this->db->order_by('c.nama');
        $this->db->order_by('a.tanggal');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}