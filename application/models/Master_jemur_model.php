<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jemur_model extends MY_Model {
	public $_table = 'master_jemur';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active()
	{
		$this->db->where('status', 1);
		$this->db->order_by('nama');
		$result = $this->db->get('master_jemur');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}