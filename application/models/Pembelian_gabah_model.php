<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_model extends MY_Model
{
    public $_table = 'pembelian_gabah';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        $this->db->select('a.*, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != ''){
            if($get['status'] == 1){
                $this->db->where('status_muatan', 0);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 2){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 3){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 1);
            }      
        }
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_muatan($id = null)
    {
        $this->db->select('max(berat_kotor) as berat_kotor, min(berat_kosong) as berat_kosong, (berat_kotor - berat_kosong) as berat_muatan');
        $this->db->where('id_pembelian', $id);
        $result = $this->db->get('pembelian_gabah_batch a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_rekap($id = null)
    {
        $this->db->select('a.*, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp, ifnull(sum(c.berat_netto), 0) as total_netto');
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        
        $this->db->where('status_muatan', 1);
        $this->db->where('status_selesai', 1);
        $this->db->where('status_persetujuan', 1);
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('pembelian_gabah_batch c', 'a.id = c.id_pembelian', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal_pembelian');
        $this->db->order_by('a.id');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_rekap_detail($id = null)
    {
        $this->db->select('a.id, a.tanggal_pembelian, c.id as id_batch, batch_label, batch, c.is_manual, a.kode_pembelian, a.plat_kendaraan, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp, ifnull(sum(c.berat_netto), 0) as total_netto, jenis_gabah, f.nama as padi, sum(jumlah_sak) as sak, potong_sak, potong_lain, hampa, persen_broken, g.kadar_air, d.berat_beras_bruto, d.berat_beras_netto, d.harga_nol_gabuk, d.harga_kg, d.subtotal, d.tambahan');
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        
        $this->db->where('a.status_muatan', 1);
        $this->db->where('a.status_selesai', 1);
        $this->db->where('a.status_persetujuan', 1);
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('pembelian_gabah_batch c', 'a.id = c.id_pembelian', 'left');
        $this->db->join('pembelian_gabah_jenis d', 'c.id = d.id_batch', 'left');
        $this->db->join('pembelian_gabah_detail e', 'd.id = e.id_jenis', 'left');
        $this->db->join('jenis_padi f', 'd.id_jenis_padi = f.id', 'left');
        $this->db->join('kadar_air g', 'e.kadar_air = g.id', 'left');
        $this->db->group_by('a.id');
        $this->db->group_by('c.id');
        $this->db->group_by('d.jenis_gabah');
        $this->db->group_by('d.id_jenis_padi');
        $this->db->order_by('a.tanggal_pembelian');
        $this->db->order_by('a.id');
        $this->db->order_by('batch');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_total_sak($id = null){
        $get = $this->input->get();
        $this->db->select('b.id as id_batch, sum(jumlah_sak) as jumlah, b.berat_netto, sum(c.berat_beras_netto) as jumlah_beras_netto, is_manual');
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        $this->db->group_by('a.id');
        $this->db->group_by('b.id');
        $this->db->order_by('a.tanggal_pembelian');
        $this->db->order_by('a.id');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_cetak_nota($id = null)
    {
        $this->db->select('a.*, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp, ifnull(c.nama, "") as pembayar, ifnull(d.nama, "") as pengecek');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('penanggung_jawab c', 'a.dibayar_oleh = c.id', 'left');
        $this->db->join('penanggung_jawab d', 'a.checker = d.id', 'left');
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != ''){
            if($get['status'] == 1){
                $this->db->where('status_muatan', 0);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 2){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 3){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 1);
            }      
        }
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_persetujuan($id = null)
    {
        $this->db->select('a.*, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != ''){
            if($get['status'] == 1){
                # Menunggu
                $this->db->where('status_selesai', 1);
                $this->db->where('status_persetujuan', 0);
            }else if($get['status'] == 2){
                # Disetujui
                $this->db->where('status_selesai', 1);
                $this->db->where('status_persetujuan', 1);
            }else if($get['status'] == 3){
                # Ditolak
                $this->db->where('status_selesai', 1);
                $this->db->where('status_persetujuan', 2);
            }else{
                $this->db->where('status_selesai', 1);
            }      
        }
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_pembayaran_tempo($id = null)
    {
        $this->db->select('a.*, b.nama as supplier, b.alamat as supplier_alamat, b.telp as supplier_telp, c.tgl_pembayaran');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('pembelian_gabah_pembayaran c', 'a.id = c.id_pembelian', 'left');
        $this->db->where('a.status_selesai', 1);
        $this->db->where('a.is_tempo', 1);
        $this->db->where('a.status_persetujuan', 1);
        $get = $this->input->get();
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != ''){
            if($get['status'] == 1){
                # Menunggu
                $this->db->where('a.is_tempo_dibayar', 0);
            }else if($get['status'] == 2){
                # Lunas
                $this->db->where('a.is_tempo_dibayar', 1);
            }
        }
        $this->db->order_by('a.tanggal_pembelian');
        $this->db->order_by('c.tgl_pembayaran');
        $this->db->group_by('a.id');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_qc($id = null)
    {
        $this->db->select('a.*, b.nama as supplier');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->order_by('a.status_muatan');
        $this->db->order_by('tanggal_pembelian', 'desc');

        $get = $this->input->get();
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_pembelian >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_pembelian <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != ''){
            if($get['status'] == 1){
                $this->db->where('status_muatan', 0);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 2){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 0);
            }else if($get['status'] == 3){
                $this->db->where('status_muatan', 1);
                $this->db->where('status_selesai', 1);
            }      
        }
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_qc_detail($id = null)
    {
        $this->db->select('a.kode_pembelian, a.status_muatan, plat_kendaraan, tanggal_pembelian, b.nama as supplier, c.id as id_batch, c.batch, c.batch_label, c.id_pembelian, c.berat_kotor, c.berat_kosong, c.berat_netto, is_manual, sap_manual');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('pembelian_gabah_batch c', 'a.id = c.id_pembelian');
        $this->db->where('c.id', $id);
        $this->db->order_by('a.status_muatan');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_qc_detail_manual($pembelian, $batch){
        $this->db->select('a.kode_pembelian, a.status_muatan, plat_kendaraan, tanggal_pembelian, b.nama as supplier, c.id as id_batch, c.batch, c.batch_label, c.id_pembelian, c.berat_kotor, c.berat_kosong, c.berat_netto, is_manual, sap_manual');
        $this->db->join('customer b', 'a.id_supplier = b.id');
        $this->db->join('pembelian_gabah_batch c', 'a.id = c.id_pembelian');
        $this->db->where('c.id_pembelian', $pembelian);
        $this->db->where('c.batch', $batch);
        $this->db->where('is_manual', 1);
        $this->db->order_by('a.status_muatan');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_kode_pembelian($tgl)
    {
        $this->db->where('year(tanggal_pembelian) = ', date('Y', strtotime($tgl)));
        $this->db->where('month(tanggal_pembelian) = ', date('m', strtotime($tgl)));
        $this->db->order_by('timestamp_input', 'desc');
        $this->db->order_by('kode_pembelian', 'desc');
        $result = $this->db->get('pembelian_gabah');
        if ($result->num_rows() > 0) {
            return $result->row_array()['kode_pembelian'];
        } else {
            return false;
        }
    }

    public function get_data_nota($id, $param)
    {
        $this->db->select('a.tanggal_pembelian, a.id_supplier, b.batch, b.batch_label, jenis_gabah, c.id_jenis_padi, d.nama as padi, (berat_beras_bruto-potong_lain) as berat_bruto, hampa, berat_beras_netto, (b.berat_netto - berat_timbang_kecil) as berat_netto_nett, harga_nol_gabuk, harga_kg, e.jumlah_sak, e.id_jenis, f.kadar_air, pengeringan');
        if(isset($param['ks']) && isset($param['kg'])){
            $this->db->where_in('jenis_gabah', array('ks', 'kg'));
        }else if(isset($param['ks'])){
            $this->db->where('jenis_gabah', 'ks');
        }else if(isset($param['kg'])){
            $this->db->where('jenis_gabah', 'kg');
        }
        $this->db->where('a.id', $id);
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail e', 'c.id = e.id_jenis');
        $this->db->join('jenis_padi d', 'c.id_jenis_padi = d.id');
        $this->db->join('kadar_air f', 'e.kadar_air = f.id');
        $this->db->order_by('batch');
        $this->db->order_by('jenis_gabah', 'desc');
        $this->db->order_by('d.nama');
        $results = $this->db->get('pembelian_gabah a');
        if ($results->num_rows() > 0) {
            return $results->result_array();
        } else {
            return false;
        }
    }

    public function get_data_tes_gabah($id = null){
        $this->db->select('a.id, a.kode_pembelian, e.nama as supplier, a.tanggal_pembelian, b.id as id_batch, b.batch, c.jenis_gabah, d.nama as padi, id_jenis_padi, id_supplier');
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('jenis_padi d', 'c.id_jenis_padi = d.id');
        $this->db->join('customer e', 'a.id_supplier = e.id');
        $this->db->where('a.status_muatan', 1);
        $this->db->where('a.status_tes', 0);
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->group_by('a.id');
        $this->db->group_by('b.batch');
        $this->db->group_by('c.jenis_gabah');
        $this->db->group_by('c.id_jenis_padi');
        $this->db->order_by('a.tanggal_pembelian');
        $this->db->order_by('b.batch');
        $results = $this->db->get('pembelian_gabah a');
        if ($results->num_rows() > 0) {
            return $results->result_array();
        } else {
            return false;
        }
    }
}
