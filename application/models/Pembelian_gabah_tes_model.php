<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_tes_model extends MY_Model
{
    public $_table = 'pembelian_gabah_tes';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_tes($id = null){
        $get = $this->input->get();
        $this->db->select('b.kode_pembelian, b.tanggal_pembelian, b.status_persetujuan, a.*, c.nama as supplier, d.nama as padi');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_tes >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_tes <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu update
                $this->db->where('a.hasil_akhir', 0);
                $this->db->where('a.is_deleted', 0);
            }else if($get['status'] == 2){
                # Selesai
                $this->db->where('a.hasil_akhir >', 0);
            }else if($get['status'] == 3){
                # Dibatalkan
                $this->db->where('a.hasil_akhir', 0);
                $this->db->where('a.is_deleted', 1);
            }
        }
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
    
        $this->db->join('pembelian_gabah b', 'a.id_pembelian = b.id');
        $this->db->join('customer c', 'b.id_supplier = c.id');
        $this->db->join('jenis_padi d', 'a.id_jenis_padi = d.id');
        $this->db->order_by('a.tanggal_tes');
        $results = $this->db->get('pembelian_gabah_tes a');
        if ($results->num_rows() > 0) {
            return $results->result_array();
        } else {
            return false;
        }
    }

    public function get_data_tes_detail($id){
        $get = $this->input->get();
        $this->db->select('b.kode_pembelian, b.status_persetujuan, b.tanggal_pembelian, a.*, c.nama as supplier, d.nama as padi');
        $this->db->where('a.id', $id);
        $this->db->join('pembelian_gabah b', 'a.id_pembelian = b.id');
        $this->db->join('customer c', 'b.id_supplier = c.id');
        $this->db->join('jenis_padi d', 'a.id_jenis_padi = d.id');
        $this->db->order_by('a.tanggal_tes');
        $results = $this->db->get('pembelian_gabah_tes a');
        if ($results->num_rows() > 0) {
            return $results->result_array();
        } else {
            return false;
        }
    }
}
