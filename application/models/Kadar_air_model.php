<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kadar_air_model extends MY_Model {
	public $_table = 'kadar_air';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active()
	{
		$this->db->where('status', 1);
		$this->db->order_by('kadar_air');
		$result = $this->db->get('kadar_air');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}