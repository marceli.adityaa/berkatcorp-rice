<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_produksi_detail_model extends MY_Model {
	public $_table = 'penjualan_produksi_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('penjualan_produksi_detail')->row();
    }

    public function get_data_po($id){
        $this->db->select('a.*, b.kode, b.tanggal, c.nama as customer, d.nama as sales, b.catatan, e.nama as area, sum(kuantitas * kemasan) as tonase');
        $this->db->where('a.id_produksi', $id);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->join('master_sales d', 'b.id_sales = d.id');
        $this->db->join('area e', 'c.id_area = e.id');
        $this->db->join('penjualan_po_detail f', 'b.id = f.id_po');
        $this->db->join('merk_beras_kemasan g', 'f.id_kemasan = g.id');
        $this->db->group_by('b.id');
        $this->db->order_by('b.kode');
        $result = $this->db->get('penjualan_produksi_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail($id){
        $this->db->select('a.id_produksi, d.id_merk, e.nama as merk, sum(c.kuantitas*d.kemasan) as tonase');
        $this->db->where('a.id_produksi', $id);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('penjualan_po_detail c', 'b.id = c.id_po');
        $this->db->join('merk_beras_kemasan d', 'c.id_kemasan = d.id');
        $this->db->join('merk_beras e', 'd.id_merk = e.id');
        $this->db->group_by('d.id_merk');
        $this->db->order_by('e.nama');
        $result = $this->db->get('penjualan_produksi_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_summary($id){
        $this->db->select('a.id_produksi, d.id_merk, c.id_kemasan, e.nama as merk, d.kemasan, sum(c.kuantitas) as qty');
        $this->db->where('a.id_produksi', $id);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('penjualan_po_detail c', 'b.id = c.id_po');
        $this->db->join('merk_beras_kemasan d', 'c.id_kemasan = d.id');
        $this->db->join('merk_beras e', 'd.id_merk = e.id');
        $this->db->group_by('e.id');
        $this->db->group_by('d.kemasan');
        $this->db->order_by('e.nama');
        $this->db->order_by('d.kemasan');
        $result = $this->db->get('penjualan_produksi_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_produksi', $id);
        return $this->db->delete('penjualan_produksi_detail');
    }
    
}