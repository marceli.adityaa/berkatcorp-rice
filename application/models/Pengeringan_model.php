<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeringan_model extends MY_Model {
	public $_table = 'pengeringan';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('pengeringan')->row();
    }

    public function get_data($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as padi, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }
        if(isset($get['padi']) && $get['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $get['padi']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu Input
                $this->db->having('total_kuantitas', 0);
            }else if($get['status'] == 2){
                # Proses Pengeringan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Selesai
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan >', 1);
            }

        }
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('pengeringan_detail c', 'a.id = c.id_pengeringan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('pengeringan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_hasil_pengeringan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as padi, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }
        if(isset($get['padi']) && $get['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $get['padi']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }

        $this->db->where('a.kuantitas >', 0);
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu Input
                $this->db->having('total_kuantitas', 0);
            }else if($get['status'] == 2){
                # Proses Pengeringan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Selesai
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan >', 1);
            }

        }
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('pengeringan_detail c', 'a.id = c.id_pengeringan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('pengeringan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_persetujuan_pengeringan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as padi, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }
        if(isset($get['padi']) && $get['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $get['padi']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }

        $this->db->where('a.kuantitas >', 0);
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Acc
                $this->db->where('a.persetujuan', 2);
            }else if($get['status'] == 5){
                # Reject
                $this->db->where('a.persetujuan', 3);
            }

        }
        $this->db->where('persetujuan >', 0);
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('pengeringan_detail c', 'a.id = c.id_pengeringan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('pengeringan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap_pengeringan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as padi, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }
        $this->db->where('persetujuan', 2);
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('pengeringan_detail c', 'a.id = c.id_pengeringan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('pengeringan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_generate_kode($tanggal)
    {
        $this->db->select('count(*) as total');
        $this->db->where('DATE_FORMAT(tanggal_mulai, "%Y-%m-%d") =', date('Y-m-d', strtotime($tanggal)));
        $result = $this->db->get('pengeringan');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'] + 1;
        } else {
            return 1;
        }
    }
    
}