<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_model extends MY_Model
{
    public $_table = 'piutang';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('piutang');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_piutang_awal($id)
    {
        $this->db->select('a.*, b.id as id_detail, b.jenis_pembayaran, b.id_kas, b.is_verifikasi as verif_detail');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('piutang_detail b', 'a.id = b.id_piutang');
        $this->db->order_by('b.tgl_transaksi');
        $this->db->limit(1);
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_piutang($id = null)
    {
        $this->db->select('a.*, b.nama as debitur');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_kelola_piutang($id = null)
    {
        $get = $this->input->get();
        // dump($get);
        $this->db->select('a.*, b.nama as debitur, max(c.tgl_transaksi) as tgl_terakhir_bayar, sum(if((bayar_pokok != 0 or bayar_bunga != 0) and c.is_verifikasi = 1, 1, 0)) as angsuran, (sum(if(c.is_verifikasi = 1, tambah_piutang, 0)) - sum(if(c.is_verifikasi = 1,bayar_pokok, 0))) as sisa_pokok, sum(if(c.is_verifikasi = 1, tambah_piutang, 0)) as total_piutang');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['client']) && $get['client'] != 'all'){
            $this->db->where('a.id_customer', $get['client']);
        }
        if(!empty($get['start'])){
            $this->db->where('a.tgl_piutang >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_piutang <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum lunas
                $this->db->where('a.is_paid_off', 0);
            }else if($get['status'] == 2){
                # lunas
                $this->db->where('a.is_paid_off', 1);
            }   
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('piutang_detail c', 'a.id = c.id_piutang');
        $this->db->group_by('a.id');
        $this->db->order_by('c.tgl_transaksi');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_piutang()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as debitur, max(c.tgl_transaksi) as tgl_terakhir_bayar, sum(if((bayar_pokok != 0 or bayar_bunga != 0) and c.is_verifikasi = 1, 1, 0)) as angsuran, (sum(if(c.is_verifikasi = 1, tambah_piutang, 0)) - sum(if(c.is_verifikasi = 1,bayar_pokok, 0))) as sisa_pokok, sum(if(c.is_verifikasi = 1, tambah_piutang, 0)) as total_piutang');

        if(!empty($get['start'])){
            $this->db->where('a.tgl_piutang >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_piutang <=', $get['end']);
        }

        if(isset($get['client']) && $get['client']!= 'all'){
            $this->db->where('a.id_customer', $get['client']);
        }
        
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum lunas
                $this->db->where('a.is_paid_off', 0);
            }else if($get['status'] == 2){
                # lunas
                $this->db->where('a.is_paid_off', 1);
            }   
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('piutang_detail c', 'a.id = c.id_piutang');
        $this->db->group_by('a.id');
        $this->db->having('total_piutang >', 0);
        $this->db->order_by('c.tgl_transaksi');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
        // dump($result);
    }

    public function get_active_balance($id = null){
        # Belum selesai
        $this->db->select('a.*, ifnull(SUM(b.tambah_piutang - bayar_pokok),0) as balance');
        if(!empty($id)){
            $this->db->where('a.id_customer', $id);
        }
        $this->db->where('a.is_paid_off', 0);
        $this->db->where('b.is_verifikasi', 1);
        $this->db->join('piutang_detail b', 'b.id_piutang = a.id', 'left');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }
    
    public function get_data_by_customer($id){
        $this->db->where('id_customer', $id);
        $this->db->where('is_paid_off', 0);
        $result = $this->db->get('piutang');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }
}
