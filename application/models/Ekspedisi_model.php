<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekspedisi_model extends MY_Model {
	public $_table = 'supplier';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($id = null){
        $this->db->select('a.*, b.nama as area');
        $this->db->order_by('a.status desc');
        $this->db->join('area b', 'a.id_area = b.id');
		$result = $this->db->get('supplier a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
	}
}