<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kas_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $result = ['total' => 0, 'rows' => []];
        $url = $this->config->item('url_master') . "/api/external/kas/get_many/" . $this->session->auth['token'] . "?";
        $result = xcurl(array(CURLOPT_URL => $url));

        if (!$result['err']) {
            $result = (Array) json_decode($result['response']);
        }

        return $result;
    }

    public function join($data = [], $name, $target)
    {
        $kas = $this->format($this->get());
        if (!empty($data)) {
            if (array_key_exists(0, $data)){
                foreach ($data as $index => $value) {
                    if (isset($value[$target]) && $value[$target] != 0) {
                        $data[$index][$name.'_is_rekening'] = $kas[$value[$target]]['is_rekening'];
                        if($kas[$value[$target]]['is_rekening'] == 1){
                            $data[$index][$name.'_bank'] = $kas[$value[$target]]['bank'];
                            $data[$index][$name.'_nama'] = $kas[$value[$target]]['nama'];
                            $data[$index][$name.'_no_rek'] = $kas[$value[$target]]['no_rek'];
                            $data[$index][$name.'_label'] = $kas[$value[$target]]['bank'].' | '.$kas[$value[$target]]['no_rek'];
                        }else{
                            $data[$index][$name.'_kas'] = $kas[$value[$target]]['nama'];
                            $data[$index][$name.'_label'] = $kas[$value[$target]]['nama'];
                        }
                    }
                }
            }else{
                $data[$name] = $kas[$data[$target]];
            }
        }
        return $data;
    }

    private function format($data = array())
    {
        $result = array();
        foreach ($data as $d) {
            if($d->is_rekening == 1){
                $result[$d->id]['is_rekening'] = $d->is_rekening;
                $result[$d->id]['bank'] = $d->bank;
                $result[$d->id]['nama'] = $d->nama;
                $result[$d->id]['no_rek'] = $d->no_rekening;
            }else{
                $result[$d->id]['is_rekening'] = $d->is_rekening;
                $result[$d->id]['nama'] = $d->label;
            }
        }
        return $result;
    }
}
