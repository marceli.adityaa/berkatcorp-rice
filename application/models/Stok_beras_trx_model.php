<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_beras_trx_model extends MY_Model {
	public $_table = 'stok_beras_trx';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            return $this->db->get('stok_beras_trx')->result();
        }else{
            return $this->db->get('stok_beras_trx');
        }
    }

    public function hapus_trx_stok($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            $this->db->where('sumber', 'master-transaksi');
            return $this->db->delete('stok_beras_trx');
        }else{
            return false;
        }
    }

    public function get_detail_rekap_stok($param){
        if(!isset($param['start']) || $param['start'] == ''){
            $param['start'] = '0000-00-00';
        }
        if(!isset($param['end']) || $param['end'] == ''){
            $param['end'] = date('Y-m-d');
        }

        $this->db->select('d.*, c.nama as merk, b.kemasan');
        if(!empty($param['id'])){
            $this->db->where('b.id', $param['id']);
        }
        $this->db->where('d.tgl >=', $param['start']);
        $this->db->where('d.tgl <=', $param['end']);
        $this->db->join('merk_beras_kemasan b', 'a.id_kemasan = b.id');
        $this->db->join('merk_beras c', 'b.id_merk = c.id');
        $this->db->join('stok_beras_trx d', 'a.id = d.id_stok');
        $this->db->order_by('d.tgl', 'desc');
        $result = $this->db->get('stok_beras a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    
}