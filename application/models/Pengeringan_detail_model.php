<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeringan_detail_model extends MY_Model {
	public $_table = 'pengeringan_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('pengeringan')->row();
    }

    public function get_data_detail($pengeringan){
        $this->db->select('a.*, b.jenis_pengeringan, b.jenis_gabah, b.id_supplier, c.nama as padi');
        $this->db->where('a.id_pengeringan', $pengeringan);
        $this->db->join('stok_gabah b', 'a.id_stok = b.id');
        $this->db->join('jenis_padi c', 'b.id_jenis_padi = c.id');
        $result = $this->db->get('pengeringan_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail_waiting_exclude($pengeringan){
        $this->db->select('b.*');
        $this->db->where('a.id <>', $pengeringan);
        $this->db->where('a.persetujuan <', 2);
        // $this->db->where('a.kuantitas >', 0);
        $this->db->join('pengeringan_detail b', 'b.id_pengeringan = a.id');
        $result = $this->db->get('pengeringan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_pengeringan', $id);
        return $this->db->delete('pengeringan_detail');
    }
    
}