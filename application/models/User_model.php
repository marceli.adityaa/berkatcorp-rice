<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $result = ['total' => 0, 'rows' => []];
        $url = $this->config->item('url_master') . "/api/external/user/get_many/" . $this->session->auth['token'] . "?";
        $result = xcurl(array(CURLOPT_URL => $url));

        if (!$result['err']) {
            $result = (Array) json_decode($result['response']);
        }

        return $result;
    }

    public function join($data = [], $name, $target)
    {
        $user = $this->format($this->get());
        if (!empty($data)) {
            if (array_key_exists(0, $data)){
                foreach ($data as $index => $value) {
                    if (isset($value[$target]) && $value[$target] != 0) {
                        $data[$index][$name] = $user[$value[$target]];
                    }
                }
            }else{
                $data[$name] = $user[$data[$target]];
            }
        }
        return $data;
    }

    public function join_object($data = [], $name, $target)
    {
        $user = $this->format($this->get());
        if (!empty($data)) {
            if (array_key_exists(0, $data)){
                foreach ($data as $index => $value) {
                    if (isset($value->$target) && $value->$target != 0) {
                        $data[$index]->$name = $user[$value->$target];
                    }
                }
            }else{
                $data->$name = $user[$data->$target];
            }
        }
        return $data;
    }

    private function format($data = array())
    {
        $result = array();
        foreach ($data as $d) {
            $result[$d->id] = $d->nama;
        }
        return $result;
    }
}
