<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekening_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_perusahaan = $this->config->item('id_perusahaan');
    }

    public function get()
    {
        $result = ['total' => 0, 'rows' => []];
        $url = $this->config->item('url_master') . "/api/external/rekening/get_active?token=" . $this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan;
        $result = xcurl(array(CURLOPT_URL => $url));

        if (!$result['err']) {
            $result = (Array) json_decode($result['response']);
        }
        return $result;
    }

    public function join($data = [], $name, $target)
    {
        $rekening = $this->format($this->get());
        if (!empty($data)) {
            if (array_key_exists(0, $data)){
                foreach ($data as $index => $value) {
                    if (isset($value[$target]) && $value[$target] != 0) {
                        $data[$index][$name] = $rekening[$value[$target]]['nama'];
                        $data[$index][$name.'_no'] = $rekening[$value[$target]]['no_rekening'];
                        $data[$index][$name.'_bank'] = $rekening[$value[$target]]['bank'];
                    }
                }
            }else{
                $data[$name] = $rekening[$data[$target]]['nama'];
                $data[$name.'_no'] = $rekening[$data[$target]['no_rekening']];
                $data[$name.'_bank'] = $rekening[$data[$target]['bank']];
            }
        }
        return $data;
    }

    private function format($data = array())
    {
        $result = array();
        foreach ($data as $d) {
            $result[$d->id]['nama'] = $d->nama;
            $result[$d->id]['no_rekening'] = $d->no_rekening;
            $result[$d->id]['bank'] = $d->bank;
        }
        return $result;
    }
}
