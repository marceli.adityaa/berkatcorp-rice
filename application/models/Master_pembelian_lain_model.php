<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_pembelian_lain_model extends MY_Model {
	public $_table = 'master_pembelian_lain';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($id = null)
	{
		$this->db->order_by('status', 'desc');
		$this->db->order_by('nama');
		if(!empty($id)){
			$this->db->where('id', $id);
		}
		$result = $this->db->get('master_pembelian_lain');
        if ($result->num_rows() > 0) {
			if(!empty($id)){
				return $result->row_array();
			}else{
				return $result->result_array();
			}
        } else {
            return false;
        }
	}

	public function get_active()
	{
		$this->db->where('status', 1);
		$this->db->order_by('nama');
		$result = $this->db->get('master_pembelian_lain');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}