<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan_produksi_proses_model extends MY_Model {
	public $_table = 'penjualan_produksi_proses';

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_data_detail($id)
	{
		$this->db->select('a.*, b.nama as merk');
		$this->db->where('a.id_produksi', $id);
		$this->db->join('merk_beras b', 'a.id_merk = b.id');
        $result = $this->db->get('penjualan_produksi_proses a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function delete_transaksi($id){
        $this->db->where('id_produksi', $id);
        return $this->db->delete('penjualan_produksi_proses');
    }
    
}