<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_bahan_model extends MY_Model {
	public $_table = 'produksi_bahan';

	public function __construct()
	{
		parent::__construct();
	}

	public function delete_transaksi($id){
        $this->db->where('id_produksi', $id);
        return $this->db->delete('produksi_bahan');
    }

	public function get_data_produksi($id){
		$this->db->select('b.*, c.nama as proses, c.is_return, c.arus, c.is_multiply_cor, c.is_sisa_produksi, d.nama as bahan');
		$this->db->where('a.id', $id);
		$this->db->where('c.is_sisa_produksi', '0');
		$this->db->join('produksi_bahan b', 'a.id = b.id_produksi');
		$this->db->join('produksi_proses c', 'b.id_proses = c.id', 'left');
		$this->db->join('jenis_bahan d', 'b.id_bahan = d.id', 'left');
		$this->db->order_by('c.urutan');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_sisa($id){
		$this->db->select('b.*, c.nama as proses, c.is_sisa_produksi, d.nama as bahan');
		$this->db->where('a.id', $id);
		$this->db->where('c.is_sisa_produksi', '1');
		$this->db->join('produksi_bahan b', 'a.id = b.id_produksi');
		$this->db->join('produksi_proses c', 'b.id_proses = c.id');
		$this->db->join('jenis_bahan d', 'b.id_bahan = d.id');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_bahan_dipakai($id_produksi){
		$this->db->select('a.tanggal, a.jumlah_cor, a.id_merk, b.*, c.nama as proses, c.*, d.nama as bahan');
		$this->db->where('a.id', $id_produksi);
		$this->db->where('c.arus', 'in');
		$this->db->where('c.is_return', '0');
		$this->db->join('produksi_bahan b', 'a.id = b.id_produksi');
		$this->db->join('produksi_proses c', 'b.id_proses = c.id');
		$this->db->join('jenis_bahan d', 'b.id_bahan = d.id');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_bahan_keluaran($id_produksi){
		$this->db->select('a.tanggal, a.jumlah_cor, a.id_merk, b.*, c.nama as proses, c.*, d.nama as bahan');
		$this->db->where('a.id', $id_produksi);
		$this->db->where('c.arus', 'out');
		$this->db->where('c.is_sisa_produksi', '0');
		$this->db->join('produksi_bahan b', 'a.id = b.id_produksi');
		$this->db->join('produksi_proses c', 'b.id_proses = c.id');
		$this->db->join('jenis_bahan d', 'b.id_bahan = d.id');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_bahan_sisa_produksi($id_produksi){
		$this->db->select('a.tanggal, a.jumlah_cor, a.id_merk, b.*, c.nama as proses, c.*, d.nama as bahan');
		$this->db->where('a.id', $id_produksi);
		$this->db->where('c.arus', 'out');
		$this->db->where('c.is_sisa_produksi', '1');
		$this->db->join('produksi_bahan b', 'a.id = b.id_produksi');
		$this->db->join('produksi_proses c', 'b.id_proses = c.id');
		$this->db->join('jenis_bahan d', 'b.id_bahan = d.id');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}