<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur_model extends MY_Model
{
    public $_table = 'penjualan_retur';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('penjualan_retur');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }


    public function get_data_retur($id = null)
    {
        $this->db->select('a.*, b.kode as kode_penjualan');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('penjualan_po b', 'a.id_penjualan = b.id');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_kelola_retur($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.kode as kode_penjualan, c.nama as customer');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        // if(isset($get['kategori']) && $get['kategori']!= 'all'){
        //     $this->db->where('b.id_biaya', $get['kategori']);
        // }

        // if(isset($get['biaya']) && $get['biaya']!= 'all'){
        //     $this->db->where('a.id_beban', $get['biaya']);
        // }

        if(!empty($get['start'])){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tanggal <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum diotorisasi
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 2){
                # acc
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 3){
                # tolak
                $this->db->where('a.persetujuan', 2);
            }   
        }
        $this->db->join('penjualan_po b', 'a.id_penjualan = b.id');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->order_by('a.tanggal', 'desc');
        // $this->db->order_by('c.nama');
        // $this->db->order_by('b.nama');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_retur($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.kode as kode_penjualan, c.nama as customer');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        

        if(!empty($get['start'])){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tanggal <=', $get['end']);
        }
        
        $this->db->where('a.persetujuan !=', 0);

        $this->db->join('penjualan_po b', 'a.id_penjualan = b.id');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->order_by('a.tanggal');
        // $this->db->order_by('c.nama');
        // $this->db->order_by('b.nama');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_persetujuan_retur($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.kode as kode_penjualan, c.nama as customer');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        
        if(!empty($get['start'])){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tanggal <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum diotorisasi
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 2){
                # acc
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 3){
                # tolak
                $this->db->where('a.persetujuan', 2);
            }   
        }
        $this->db->join('penjualan_po b', 'a.id_penjualan = b.id');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->order_by('a.tanggal');
        $result = $this->db->get('penjualan_retur a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    
}
