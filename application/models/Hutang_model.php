<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang_model extends MY_Model
{
    public $_table = 'hutang';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('hutang');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_hutang_awal($id)
    {
        $this->db->select('a.*, b.id as id_detail, b.jenis_pembayaran, b.id_kas, b.is_verifikasi as verif_detail');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('hutang_detail b', 'a.id = b.id_hutang');
        $this->db->order_by('b.tgl_transaksi');
        $this->db->limit(1);
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_hutang($id = null)
    {
        $this->db->select('a.*, b.nama as kreditur');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_kelola_hutang($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kreditur, max(c.tgl_transaksi) as tgl_terakhir_bayar, sum(if(bayar_pokok != 0 or bayar_bunga != 0 and c.is_verifikasi = 1, 1, 0)) as angsuran, (sum(if(c.is_verifikasi = 1, tambah_hutang, 0)) - sum(if(c.is_verifikasi = 1,bayar_pokok, 0))) as sisa_pokok, sum(if(c.is_verifikasi = 1, tambah_hutang, 0)) as total_hutang');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['client']) && $get['client']!= 'all'){
            $this->db->where('a.id_customer', $get['client']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_hutang >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_hutang <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum lunas
                $this->db->where('a.is_paid_off', 0);
            }else if($get['status'] == 2){
                # lunas
                $this->db->where('a.is_paid_off', 1);
            }   
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('hutang_detail c', 'a.id = c.id_hutang');
        $this->db->group_by('a.id');
        $this->db->order_by('c.tgl_transaksi');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_hutang($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kreditur, max(c.tgl_transaksi) as tgl_terakhir_bayar, sum(if((bayar_pokok != 0 or bayar_bunga) != 0 and c.is_verifikasi = 1, 1, 0)) as angsuran, (sum(if(c.is_verifikasi = 1, tambah_hutang, 0)) - sum(if(c.is_verifikasi = 1,bayar_pokok, 0))) as sisa_pokok, sum(if(c.is_verifikasi = 1, tambah_hutang, 0)) as total_hutang');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['client']) && $get['client']!= 'all'){
            $this->db->where('a.id_customer', $get['client']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_hutang >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_hutang <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum lunas
                $this->db->where('a.is_paid_off', 0);
            }else if($get['status'] == 2){
                # lunas
                $this->db->where('a.is_paid_off', 1);
            }   
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('hutang_detail c', 'a.id = c.id_hutang');
        $this->db->group_by('a.id');
        $this->db->having('total_hutang >', 0);
        $this->db->order_by('c.tgl_transaksi');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    
}
