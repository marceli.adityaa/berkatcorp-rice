<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_model extends MY_Model
{
    public $_table = 'customer';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as area');
        
        if(isset($get['jenis']) && $get['jenis'] != 'all'){
            if($get['jenis'] == 2){
                # Supplier
                $this->db->where('is_supplier', 1);
            }else if($get['jenis'] == 3){
                # Customer
                $this->db->where('is_customer', 1);
            }
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Aktif
                $this->db->where('a.status', 1);
            }else if($get['status'] == 2){
                # Non-aktif
                $this->db->where('a.status', 0);
            }
        }

        if(isset($get['area']) && $get['area'] != 'all'){
            $this->db->where('a.id_area', $get['area']);
        }
        
        $this->db->join('area b', 'a.id_area = b.id');
        $this->db->order_by('a.status', 'desc');
        $this->db->order_by('b.nama', 'asc');
        $this->db->order_by('a.nama', 'asc');
        $result = $this->db->get('customer a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_active()
    {
        $this->db->where('status', '1');
        $this->db->order_by('nama');
        $result = $this->db->get('customer');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_primary(){
        $this->db->where('status', 1);
        $this->db->where('is_primary', '1');
        $result = $this->db->get('customer');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_non_primary(){
        $this->db->where('status', 1);
        $this->db->where('is_primary', '0');
        $result = $this->db->get('customer');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_active_supplier()
    {
        $this->db->where('status', 1);
        $this->db->where('is_supplier', 1);
        $this->db->order_by('nama');
        $result = $this->db->get('customer');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_active_customer()
    {
        $this->db->where('status', 1);
        $this->db->where('is_customer', 1);
        $this->db->order_by('nama');
        $result = $this->db->get('customer');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_customer_penjualan_beras()
    {
        $this->db->select('a.*');
        $this->db->where('a.status', 1);
        $this->db->where('a.is_customer', 1);
        $this->db->order_by('a.nama');
        $this->db->join('penjualan_po b', 'a.id = b.id_customer');
        $this->db->group_by('a.id');
        $result = $this->db->get('customer a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}
