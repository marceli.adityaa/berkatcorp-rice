<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan_lain_model extends MY_Model {
	public $_table = 'penjualan_lain';

	public function __construct()
	{
		parent::__construct();
	}

	public function get($id)
    {
        $this->db->select('a.*, b.nama as customer, b.alamat as customer_alamat, b.telp as customer_telp');
        $this->db->where('a.id', $id);
        $this->db->join('customer b', 'a.id_customer = b.id');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
			return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->get('penjualan_lain');
        if ($result->num_rows() > 0) {
			return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_kode($tgl)
    {
        $this->db->where('year(tanggal) = ', date('Y', strtotime($tgl)));
        $this->db->where('month(tanggal) = ', date('m', strtotime($tgl)));
        $this->db->order_by('timestamp_input', 'desc');
        $this->db->order_by('kode', 'desc');
        $result = $this->db->get('penjualan_lain');
        if ($result->num_rows() > 0) {
            return $result->row_array()['kode'];
        } else {
            return false;
        }
    }

    public function get_data_trx()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer, d.nama as area');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);  
        }

        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # input data
                $this->db->where('a.status', 0);
            }else if($get['status'] == 2){
                # menunggu
                $this->db->where('a.status', 1);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # disetujui
                $this->db->where('a.status', 1);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # ditolak
                $this->db->where('a.status', 1);
                $this->db->where('a.persetujuan', 2);
            }
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('area d', 'b.id_area = d.id');
        $this->db->order_by('a.status');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer, d.nama as area, if(a.jumlah_bayar = 0, sum(if(e.status_persetujuan = 1, e.jumlah_bayar, 0)), 0) as jumlah_bayar');
        $this->db->where('persetujuan', 1);
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);  
        }
        if(isset($get['area']) && $get['area'] != 'all'){
            $this->db->where('b.id_area', $get['area']);  
        }
        if(isset($get['sales']) && $get['sales'] != 'all'){
            $this->db->where('a.id_sales', $get['sales']);  
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # lunas 
                $this->db->where('a.is_lunas', 1);
            }else if($get['status'] == 2){
                # belum lunas
                $this->db->where('a.is_lunas', 0);
            }
        }

        if(isset($get['tempo']) && $get['tempo'] != 'all'){
            if($get['tempo'] == 1){
                # Berlangsung 
                $this->db->where('is_lunas', 0);
                $this->db->where('a.tgl_tempo >', date('Y-m-d'));
            }else if($get['tempo'] == 2){
                # Jatuh Tempo
                $this->db->where('is_lunas', 0);
                $this->db->where('a.tgl_tempo <=', date('Y-m-d'));
            }
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('area d', 'b.id_area = d.id');
        $this->db->join('penjualan_lain_pembayaran e', 'a.id = e.id_penjualan', 'left');
        $this->db->order_by('d.nama');
        $this->db->order_by('a.tanggal');
        $this->db->order_by('a.grandtotal');
        $this->db->group_by('a.id');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_persetujuan()
    {
        $get = $this->input->get();
        if(!isset($get['status'])){
            $get['status'] == 2;
        }

        $this->db->select('a.*, b.nama as customer, (ifnull(sum(c.kuantitas*c.harga_satuan), 0) - a.potongan + a.biaya_tambahan) as total, d.nama as kategori');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        $this->db->where('a.status', 1);
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 2){
                # menunggu persetujuan
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # disetujui
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # ditolak
                $this->db->where('a.persetujuan', 2);
            }
        }

        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('penjualan_lain_detail c', 'a.id = c.id_penjualan', 'left');
        $this->db->join('master_penjualan_lain d', 'd.id = c.id_kategori', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_nota()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer, c.nama as sales, d.nama as area');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);  
        }
        if(isset($get['area']) && $get['area'] != 'all'){
            $this->db->where('b.id_area', $get['area']);  
        }
        if(isset($get['sales']) && $get['sales'] != 'all'){
            $this->db->where('a.id_sales', $get['sales']);  
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # belum diisi
                $this->db->where('a.status_po', 0);
            }else if($get['status'] == 2){
                # input harga
                $this->db->where('a.status_po', 1);
                $this->db->where('a.status_pengiriman', 0);
            }else if($get['status'] == 3){
                # atur pengiriman
                $this->db->where('a.status_po', 1);
                $this->db->where('a.status_pengiriman', 1);
                $this->db->where('a.status_selesai', 0);
            }else if($get['status'] == 4){
                # selesai
                $this->db->where('a.status_po', 1);
                $this->db->where('a.status_pengiriman', 1);
                $this->db->where('a.status_selesai', 1);
            }
        }else{
            $this->db->where('a.status_po', 1);
            $this->db->where('a.status_pengiriman', 0);
        }
        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('master_sales c', 'a.id_sales = c.id');
        $this->db->join('area d', 'b.id_area = d.id');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function cek_transaksi_kontan($tanggal = null, $sales = null){
        if(!empty($tanggal)){
            $this->db->where('tanggal', $tanggal);
        }
        if(!empty($sales)){
            $this->db->where('id_sales', $sales);
        }
        $this->db->where('status_kontan', 1);
        $result = $this->db->get('penjualan_lain');
        
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_data_pembayaran_tempo($id = null){
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as customer, ifnull(sum(c.kuantitas*c.harga_satuan), 0) as total, d.nama as kategori, sum(if(e.status_persetujuan=1, e.jumlah_bayar, 0)) as total_bayar');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->where('a.status', 1);
        $this->db->where('a.is_tempo', 1);
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Jatuh tempo
                $this->db->where('is_lunas', 0);
                $this->db->where('tgl_tempo <=', date('Y-m-d'));
            }else if($get['status'] == 2){
                # Berlangsung
                $this->db->where('is_lunas', 0);
                // $this->db->where('tgl_tempo >', date('Y-m-d'));
            }else if($get['status'] == 3){
                # Lunas
                $this->db->where('is_lunas', 1);
            }
        }

        $this->db->join('customer b', 'a.id_customer = b.id');
        $this->db->join('penjualan_lain_detail c', 'a.id = c.id_penjualan', 'left');
        $this->db->join('master_penjualan_lain d', 'd.id = c.id_kategori', 'left');
        $this->db->join('penjualan_lain_pembayaran e', 'a.id = e.id_penjualan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('penjualan_lain a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}