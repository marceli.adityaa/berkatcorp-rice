<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendapatan_model extends MY_Model
{
    public $_table = 'pendapatan';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('pendapatan');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }


    public function get_data_pendapatan($id = null)
    {
        $this->db->select('a.*, b.nama as kategori');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('master_akun_pendapatan b', 'a.id_kategori = b.id');
        $result = $this->db->get('pendapatan a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_kelola_pendapatan($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kategori');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['kategori']) && $get['kategori']!= 'all'){
            $this->db->where('a.id_kategori', $get['kategori']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum diotorisasi
                $this->db->where('a.is_verifikasi', 0);
            }else if($get['status'] == 2){
                # acc
                $this->db->where('a.is_verifikasi', 1);
            }else if($get['status'] == 2){
                # tolak
                $this->db->where('a.is_verifikasi', 2);
            }   
        }
        $this->db->join('master_akun_pendapatan b', 'a.id_kategori = b.id');
        $this->db->order_by('a.tgl_transaksi');
        $this->db->order_by('b.nama');
        $result = $this->db->get('pendapatan a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_pendapatan($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kategori');
        $this->db->where('a.is_verifikasi', 1);
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['kategori']) && $get['kategori']!= 'all'){
            $this->db->where('a.id_kategori', $get['kategori']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }

        $this->db->join('master_akun_pendapatan b', 'a.id_kategori = b.id');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $this->db->order_by('b.nama');
        $result = $this->db->get('pendapatan a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_persetujuan_pendapatan($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kategori');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['kategori']) && $get['kategori']!= 'all'){
            $this->db->where('a.id_kategori', $get['kategori']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum diotorisasi
                $this->db->where('a.is_verifikasi', 0);
            }else if($get['status'] == 2){
                # acc
                $this->db->where('a.is_verifikasi', 1);
            }else if($get['status'] == 2){
                # tolak
                $this->db->where('a.is_verifikasi', 2);
            }   
        }
        $this->db->join('master_akun_pendapatan b', 'a.id_kategori = b.id');
        $this->db->order_by('a.tgl_transaksi');
        $result = $this->db->get('pendapatan a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    
}
