<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_pembayaran_model extends MY_Model
{
    public $_table = 'pembelian_gabah_pembayaran';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('pembelian_gabah_pembayaran');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_detail_pembayaran($id = null)
    {
        if(!empty($id)){

            $this->db->select('b.*');
            $this->db->join('pembelian_gabah_pembayaran b', 'a.id = b.id_pembelian');
            $this->db->where('a.id', $id);
            $this->db->order_by('b.tgl_pembayaran');
            $result = $this->db->get('pembelian_gabah a');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public function get_data_pembayaran_verified($pembelian, $id = null)
    {
        $this->db->select('b.*');   
        $this->db->where('a.id', $pembelian);
        if(!empty($id)){
            $this->db->where('b.id <>', $id);
        }
        $this->db->where('b.status_persetujuan', 1);
        $this->db->join('pembelian_gabah_pembayaran b', 'a.id = b.id_pembelian');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_otorisasi_pembayaran()
    {
        $get = $this->input->get();
        $this->db->select('c.nama as supplier, a.kode_pembelian, b.*');
        $this->db->join('pembelian_gabah_pembayaran b', 'a.id = b.id_pembelian');
        $this->db->join('customer c', 'a.id_supplier = c.id');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('b.tgl_pembayaran >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('b.tgl_pembayaran <=', $get['end']);
        }
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu
                $this->db->where('b.status_persetujuan', 0);
            }else if($get['status'] == 2){
                # Lunas
                $this->db->where('b.status_persetujuan', 1);
            }else{
                # Ditolak
                $this->db->where('b.status_persetujuan', 2);
            }
        }
        $this->db->order_by('b.tgl_pembayaran');
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_deleteable($id){
        $this->db->where('id', $id);
        $this->db->where('status_persetujuan >', 0);
        $result = $this->db->get('pembelian_gabah_pembayaran');
        if ($result->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }


}
