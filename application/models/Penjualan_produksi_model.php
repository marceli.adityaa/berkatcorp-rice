<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_produksi_model extends MY_Model {
	public $_table = 'penjualan_produksi';

	public function __construct()
	{
		parent::__construct();
	}

	public function get($id)
    {
        $this->db->select('a.*');
        $this->db->where('a.id', $id);
        $result = $this->db->get('penjualan_produksi a');
        if ($result->num_rows() > 0) {
			return $result->row_array();
        } else {
            return false;
        }
    }

    public function get_kode_order($tgl)
    {
        $this->db->where('year(tanggal) = ', date('Y', strtotime($tgl)));
        $this->db->where('month(tanggal) = ', date('m', strtotime($tgl)));
        $this->db->order_by('kode', 'desc');
        $result = $this->db->get('penjualan_produksi');
        if ($result->num_rows() > 0) {
            return $result->row_array()['kode'];
        } else {
            return false;
        }
    }

    public function get_data_trx()
    {
        $get = $this->input->get();

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal <=', $get['end']);  
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            $this->db->where('a.status', ($get['status'] - 1));
        }
        $result = $this->db->get('penjualan_produksi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}