<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggilingan_detail_model extends MY_Model {
	public $_table = 'penggilingan_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('penggilingan')->row();
    }

    public function get_data_detail($penggilingan){
        $this->db->select('a.*, b.sumber, b.jenis_proses, b.jenis_gabah, b.id_supplier, b.tanggal, c.nama as padi');
        $this->db->where('a.id_penggilingan', $penggilingan);
        $this->db->join('stok_gabah b', 'a.id_stok = b.id');
        $this->db->join('jenis_padi c', 'b.id_jenis_padi = c.id');
        $result = $this->db->get('penggilingan_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail_waiting_exclude($penggilingan){
        $this->db->select('b.*, c.jenis_gabah, c.jenis_proses');
        $this->db->where('a.id', $penggilingan);
        $this->db->where('a.persetujuan <', 2);
        $this->db->where('a.kuantitas >', 0);
        $this->db->join('penggilingan_detail b', 'b.id_penggilingan = a.id');
        $this->db->join('stok_gabah c', 'b.id_stok = c.id');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_penggilingan', $id);
        return $this->db->delete('penggilingan_detail');
    }
    
}