<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_sak_detail_model extends MY_Model
{
    public $_table = 'transaksi_sak_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id_transaksi', $id);
        }
        $this->db->order_by('id_transaksi', 'desc');
        $this->db->order_by('id_jenis_sak');
        $this->db->order_by('id_customer');
        $result = $this->db->get('transaksi_sak_detail');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_edit_transaksi($id = null)
    {
        if(!empty($id)){
            $this->db->where('id_transaksi', $id);
        }
        $this->db->order_by('id_transaksi', 'desc');
        $this->db->order_by('id_jenis_sak');
        $this->db->order_by('id_customer');
        $this->db->group_by('id_jenis_sak');
        $result = $this->db->get('transaksi_sak_detail');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function reset_detail($id = null)
    {
        if(!empty($id)){
            $this->db->where('id_transaksi', $id);
            return $this->db->delete('transaksi_sak_detail');
        }
    }

    public function delete_by_transaksi($id)
    {
        $this->db->where('id_transaksi', $id);
        return $this->db->delete('transaksi_sak_detail');
    }
}
