<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_proses_model extends MY_Model {
	public $_table = 'produksi_proses';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active()
	{
		$this->db->where('status', 1);
		$this->db->order_by('urutan');
		$result = $this->db->get('produksi_proses');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}


}