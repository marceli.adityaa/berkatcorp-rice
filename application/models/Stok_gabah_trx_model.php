<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_gabah_trx_model extends MY_Model {
	public $_table = 'stok_gabah_trx';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            return $this->db->get('stok_gabah_trx')->result();
        }else{
            return $this->db->get('stok_gabah_trx');
        }
    }

    public function hapus_trx_stok($id = null){
        if(!empty($id)){
            $this->db->where('id_stok', $id);
            $this->db->where('sumber', 'master-transaksi');
            return $this->db->delete('stok_gabah_trx');
        }else{
            return false;
        }
    }

    public function get_detail_rekap_stok($param){
        $this->db->select('UPPER(a.jenis_gabah) as jenis_gabah, a.jenis_pengeringan, a.jenis_proses, b.*, UPPER(c.nama) as padi, d.nama as supplier');
        if(!empty($param['gabah']) && $param['gabah'] != 'all'){
            $this->db->where('a.jenis_gabah', $param['gabah']);
        }
        if(!empty($param['padi']) && $param['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $param['padi']);
        }
        if(!empty($param['pengeringan']) && $param['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $param['pengeringan']);
        }
        if(!empty($param['proses']) && $param['proses'] != 'all'){
            $this->db->where('a.jenis_proses', $param['proses']);
        }
        $this->db->where('b.tgl >=', $param['start']);
        $this->db->where('b.tgl <=', $param['end']);
        $this->db->join('stok_gabah_trx b', 'a.id = b.id_stok');
        $this->db->join('jenis_padi c', 'a.id_jenis_padi = c.id');
        $this->db->join('customer d','a.id_supplier = d.id', 'left');
        $this->db->order_by('b.timestamp', 'desc');
        $result = $this->db->get('stok_gabah a');
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
    }
}