<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_po_pembayaran_model extends MY_Model
{
    public $_table = 'penjualan_po_pembayaran';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('penjualan_po_pembayaran');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }
    
    public function jumlah_pembayaran($id){
        $this->db->where('id_penjualan', $id);
        $this->db->where('status_persetujuan', 1);
        $result = $this->db->get('penjualan_po_pembayaran');
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return false;
        }
    }

    public function get_detail_pembayaran($id = null)
    {
        if(!empty($id)){

            $this->db->select('b.*');
            $this->db->join('penjualan_po_pembayaran b', 'a.id = b.id_penjualan');
            $this->db->where('a.id', $id);
            $this->db->order_by('b.tgl_pembayaran');
            $result = $this->db->get('penjualan_po a');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public function get_detail_pembayaran_approved($id = null)
    {
        if(!empty($id)){

            $this->db->select('b.*');
            $this->db->join('penjualan_po_pembayaran b', 'a.id = b.id_penjualan');
            $this->db->where('a.id', $id);
            $this->db->where('b.status_persetujuan', 1);
            $this->db->order_by('b.tgl_pembayaran');
            $result = $this->db->get('penjualan_po a');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public function get_data_pembayaran_verified($penjualan, $id = null)
    {
        $this->db->select('b.*');   
        $this->db->where('a.id', $penjualan);
        if(!empty($id)){
            $this->db->where('b.id <>', $id);
        }
        $this->db->where('b.status_persetujuan', 1);
        $this->db->join('penjualan_po_pembayaran b', 'a.id = b.id_penjualan');
        $result = $this->db->get('penjualan_po a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_otorisasi_pembayaran()
    {
        $get = $this->input->get();
        $this->db->select('c.nama as customer, a.kode, b.*');
        $this->db->join('penjualan_po_pembayaran b', 'a.id = b.id_penjualan');
        $this->db->join('customer c', 'a.id_customer = c.id');
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('b.tgl_pembayaran >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('b.tgl_pembayaran <=', $get['end']);
        }
        if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.id_customer', $get['customer']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu
                $this->db->where('b.status_persetujuan', 0);
            }else if($get['status'] == 2){
                # Lunas
                $this->db->where('b.status_persetujuan', 1);
            }else{
                # Ditolak
                $this->db->where('b.status_persetujuan', 2);
            }
        }
        $this->db->order_by('b.tgl_pembayaran');
        $result = $this->db->get('penjualan_po a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_deleteable($id){
        $this->db->where('id', $id);
        $this->db->where('status_persetujuan', 1);
        $result = $this->db->get('penjualan_po_pembayaran');
        if ($result->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
