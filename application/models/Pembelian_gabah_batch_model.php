<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_batch_model extends MY_Model
{
    public $_table = 'pembelian_gabah_batch';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        $this->db->select('a.*, b.nama as jenis_sak');
        $this->db->join('jenis_sak b', 'a.id_jenis_sak = b.id', 'left');
        $this->db->where('a.id_pembelian', $id);
        $this->db->where('is_manual', 0);
        $this->db->order_by('a.batch');
        $result = $this->db->get('pembelian_gabah_batch a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function is_deleteable($id){
        $this->db->where('id_batch', $id);
        $result = $this->db->get('pembelian_gabah_jenis');
        if ($result->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function get_data_last($id = null)
    {
        $this->db->where('a.id_pembelian', $id);
        $this->db->order_by('a.batch', 'desc');
        $result = $this->db->get('pembelian_gabah_batch a');
        if ($result->num_rows() > 0) {
            return $result->row_array()['batch']+1;
        } else {
            return 1;
        }
    }

    public function get_data_jenis_bukutes($id_batch, $id_pembelian, $jenis_gabah, $jenis_padi){
        $this->db->select('b.*');
        $this->db->join('pembelian_gabah_jenis b', 'a.id = b.id_batch');
        $this->db->where('a.id', $id_batch);
        $this->db->where('a.id_pembelian', $id_pembelian);
        $this->db->where('b.jenis_gabah', $jenis_gabah);
        $this->db->where('b.id_jenis_padi', $jenis_padi);
        $this->db->order_by('a.batch');
        $result = $this->db->get('pembelian_gabah_batch a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_pembelian($pembelian){
        $this->db->where('id_pembelian', $pembelian);
        return $this->db->delete('pembelian_gabah_batch');
    }
}
