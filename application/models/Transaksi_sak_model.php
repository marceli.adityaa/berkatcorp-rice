<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_sak_model extends MY_Model
{
    public $_table = 'transaksi_sak';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('transaksi_sak');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_sak(){
        $get = $this->input->get();
        $this->db->select('a.kode_transaksi, a.tgl_transaksi, a.source, a.input_by, a.timestamp_input, b.*, c.nama as customer, c.is_primary, d.nama as jenis_sak');
        if(isset($get['client']) && $get['client'] != 'all'){
            $this->db->where('b.id_customer', $get['client']);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['arus']) && $get['arus'] != 'all'){
            $this->db->where('b.arus', $get['arus']);
        }
        $this->db->where('b.status_acc', 2);
        $this->db->join('transaksi_sak_detail b', 'a.id = b.id_transaksi');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->join('jenis_sak d', 'b.id_jenis_sak = d.id');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('kode_transaksi', 'desc');
        $result = $this->db->get('transaksi_sak a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_stok()
    {
        if(isset($_GET['start']) && $_GET['start'] != ''){
            $range = '';
            $range .= "and tgl_transaksi >= '".$_GET['start']."'";
            if(isset($_GET['end']) && $_GET['end'] != ''){
                $range .= " and tgl_transaksi <= '".$_GET['end']."' ";
            }
            $this->db->select('a.id, a.nama, ifnull((awal_masuk - awal_keluar), 0) as stok_awal, ifnull(total_masuk, 0) as total_masuk, ifnull(total_keluar, 0) as total_keluar');
            $this->db->join('(
                SELECT id_jenis_sak, sum(if(a.arus="in", jumlah, 0)) as total_masuk, sum(if(a.arus="out", jumlah, 0)) as total_keluar 
                FROM transaksi_sak_detail a 
                JOIN transaksi_sak b ON a.id_transaksi = b.id
                WHERE id_customer ='.$_GET['akun'].' and is_record = 1 '.$range.'
                GROUP BY id_jenis_sak
                ) b', 'a.id = b.id_jenis_sak', 'left');
            $this->db->join('(
                SELECT id_jenis_sak, sum(if(a.arus="in", jumlah, 0)) as awal_masuk, sum(if(a.arus="out", jumlah, 0)) as awal_keluar 
                FROM transaksi_sak_detail a 
                JOIN transaksi_sak b ON a.id_transaksi = b.id
                WHERE id_customer ='.$_GET['akun'].' and is_record = 1 and tgl_transaksi < "'.$_GET['start'].'"
                GROUP BY id_jenis_sak
                ) c', 'a.id = c.id_jenis_sak', 'left');    
            $this->db->group_by('a.id');
            $this->db->order_by('a.nama');
            $result = $this->db->get('jenis_sak a');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }else{
            $this->db->select('a.id, a.nama, ifnull(total_masuk, 0) as total_masuk, ifnull(total_keluar, 0) as total_keluar');
            $this->db->join('(
                SELECT id_jenis_sak, sum(if(a.arus="in", jumlah, 0)) as total_masuk, sum(if(a.arus="out", jumlah, 0)) as total_keluar 
                FROM transaksi_sak_detail a 
                JOIN transaksi_sak b ON a.id_transaksi = b.id
                WHERE id_customer ='.$_GET['akun'].' and is_record = 1
                GROUP BY id_jenis_sak
                ) b', 'a.id = b.id_jenis_sak', 'left');
            $this->db->group_by('a.id');
            $this->db->order_by('a.nama');
            $result = $this->db->get('jenis_sak a');
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        }
    }

    public function get_data_pengajuan()
    {
        $get = $this->input->get();
        $this->db->select('a.kode_transaksi, a.tgl_transaksi, a.source, a.input_by, a.timestamp_input, b.*, c.nama as customer, c.is_primary, d.nama as jenis_sak');
        if(isset($get['client']) && $get['client'] != 'all'){
            $this->db->where('b.id_customer', $get['client']);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            $this->db->where('b.status_acc', $get['status']);
        }
        $this->db->join('transaksi_sak_detail b', 'a.id = b.id_transaksi');
        $this->db->join('customer c', 'b.id_customer = c.id');
        $this->db->join('jenis_sak d', 'b.id_jenis_sak = d.id');
        $result = $this->db->get('transaksi_sak a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_by($src = null, $id = null)
    {
        if(!empty($src)){
            $this->db->where('source', $src);
        }

        if(!empty($id)){
            $this->db->where('id_source', $id);
        }
        $result = $this->db->get('transaksi_sak');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_last_kode($tgl){
        $this->db->order_by('kode_transaksi', 'desc');
        $this->db->where('tgl_transaksi', $tgl);
        $this->db->limit(1);
        $result = $this->db->get('transaksi_sak');
        if ($result->num_rows() > 0) {
            return $result->row_array()['kode_transaksi'];
        } else {
            return false;
        }
    }

    public function is_exist($src, $id){
        $this->db->where('source', $src);
        $this->db->where('id_source', $id);
        $result = $this->db->get('transaksi_sak');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}
