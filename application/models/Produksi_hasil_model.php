<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_hasil_model extends MY_Model {
	public $_table = 'produksi_hasil';

	public function __construct()
	{
		parent::__construct();
	}

	public function delete_transaksi($id){
        $this->db->where('id_produksi', $id);
        return $this->db->delete('produksi_hasil');
	}
	
	public function get_data_hasil($id){
		$this->db->select('a.id_merk, a. tanggal, b.*, c.nama as merk');
		$this->db->where('a.id', $id);
		$this->db->join('produksi_hasil b', 'a.id = b.id_produksi');
		$this->db->join('merk_beras c', 'a.id_merk = c.id');
		$result = $this->db->get('produksi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_rekap_hasil($id = null){
		$get = $this->input->get();
        $this->db->select('a.*, sum(b.kemasan*b.kuantitas) as tonase_hasil');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") <=', $get['end']);  
        }

        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('a.id_merk', $get['merk']);
        }

        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->where('a.status', 1);
        $this->db->where('a.persetujuan', 1);
        $this->db->join('produksi_hasil b', 'a.id = b.id_produksi', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('produksi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function get_data_rekap_detail($id = null){
        $get = $this->input->get();
        $this->db->select('a.kode, a.tanggal, b.*, c.nama as merk');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") <=', $get['end']);  
        }

        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('a.id_merk', $get['merk']);
        }

        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->where('a.status', 1);
        $this->db->where('a.persetujuan', 1);
        $this->db->join('produksi_hasil b', 'a.id = b.id_produksi');
        $this->db->join('merk_beras c', 'a.id_merk = c.id');
        $this->db->order_by('a.tanggal', 'desc');
        $this->db->order_by('a.id');
        $result = $this->db->get('produksi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}