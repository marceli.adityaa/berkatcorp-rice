<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_gabah_model extends MY_Model {
	public $_table = 'jenis_gabah';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active()
	{
		$this->db->where('status', 1);
		$this->db->order_by('nama');
		$result = $this->db->get('jenis_gabah');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}