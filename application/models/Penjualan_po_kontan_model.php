<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_po_kontan_model extends MY_Model {
	public $_table = 'penjualan_po_kontan';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        $result = $this->db->get('penjualan_po_kontan');
        if ($result->num_rows() > 0) {
			return $result->row_array();
        } else {
            return false;
        }
    }

	public function get_data_transaksi($id)
    {
        $this->db->select('a.*, ifnull(sum(c.kemasan*b.kuantitas*b.harga_satuan), 0) as subtotal');
        $this->db->where('a.id_po', $id);
        $this->db->join('penjualan_po_detail b', 'a.id = b.id_kontan', 'left');
        $this->db->join('merk_beras_kemasan c', 'b.id_kemasan = c.id', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.timestamp_input');
        $result = $this->db->get('penjualan_po_kontan a');
        if ($result->num_rows() > 0) {
			return $result->result_array();
        } else {
            return false;
        }
    }
    public function get_kode($tgl)
    {
        $this->db->where('c.year(tanggal) = ', date('Y', strtotime($tgl)));
        $this->db->where('c.month(tanggal) = ', date('m', strtotime($tgl)));
        $this->db->order_by('b.timestamp_input', 'desc');
        $this->db->order_by('b.kode', 'desc');
        $this->db->join('penjualan_po_detail c', 'a.id = c.id_kontan', 'left');
        $this->db->join('penjualan_po b', 'b.id = c.id_po', 'left');
        $result = $this->db->get('penjualan_po_kontan a');
        dump($result);
        if ($result->num_rows() > 0) {
            return $result->row_array()['kode'];
        } else {
            return false;
        }
    }
}