<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_po_detail_model extends MY_Model {
	public $_table = 'penjualan_po_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('penjualan_po_detail')->row();
    }

    public function get_data_detail($id){
        $this->db->select('a.*, c.kemasan, d.nama as merk');
        $this->db->where('a.id_po', $id);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('merk_beras_kemasan c', 'a.id_kemasan = c.id');
        $this->db->join('merk_beras d', 'c.id_merk = d.id');
        $result = $this->db->get('penjualan_po_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail_faktur_kontan($id){
        $this->db->select('a.*, c.kemasan, d.nama as merk');
        $this->db->where('a.id_kontan', $id);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('merk_beras_kemasan c', 'a.id_kemasan = c.id');
        $this->db->join('merk_beras d', 'c.id_merk = d.id');
        $result = $this->db->get('penjualan_po_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail_kontan($trx){
        $this->db->select('a.*, c.kemasan, d.nama as merk');
        $this->db->where('a.id_kontan', $trx);
        $this->db->join('penjualan_po b', 'a.id_po = b.id');
        $this->db->join('merk_beras_kemasan c', 'a.id_kemasan = c.id');
        $this->db->join('merk_beras d', 'c.id_merk = d.id');
        $this->db->order_by('a.timestamp_input');
        $result = $this->db->get('penjualan_po_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_detail_waiting_exclude($penggilingan){
        $this->db->select('b.*, c.jenis_gabah, c.jenis_proses');
        $this->db->where('a.id', $penggilingan);
        $this->db->where('a.persetujuan <', 2);
        $this->db->where('a.kuantitas >', 0);
        $this->db->join('penggilingan_detail b', 'b.id_penggilingan = a.id');
        $this->db->join('stok_gabah c', 'b.id_stok = c.id');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_po', $id);
        return $this->db->delete('penjualan_po_detail');
    }

    public function delete_detail_kontan($id){
        $this->db->where('id_kontan', $id);
        return $this->db->delete('penjualan_po_detail');
    }
    
}