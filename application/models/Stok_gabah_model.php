<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_gabah_model extends MY_Model {
	public $_table = 'stok_gabah';

	public function __construct()
	{
		parent::__construct();
	}

	// public function get_active()
	// {
	// 	$this->db->where('status', 1);
	// 	$this->db->order_by('nama');
	// 	$result = $this->db->get('jenis_padi');
    //     if ($result->num_rows() > 0) {
    //         return $result->result_array();
    //     } else {
    //         return false;
    //     }
    // }

    public function get_data($id = null){
        if(!empty($id)){
            $this->db->where('id', $id);
            return $this->db->get('stok_gabah')->row_array();
        }else{
            return $this->db->get('stok_gabah');
        }
    }

    public function get_data_source($sumber = 'pembelian-gabah', $id = null){
        if(!empty($id)){
            $this->db->where('sumber', $sumber);
            $this->db->where('id_sumber', $id);
            $result = $this->db->get('stok_gabah');
            if($result->num_rows() > 0){
                return $result->result();
            }else{
                return false;
            }
        }else{
            return $this->db->get('stok_gabah');
        }
    }

    public function get_data_trx(){
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as padi');
        if(isset($get['supplier']) && $get['supplier'] != 'all'){
            $this->db->where('a.id_supplier', $get['supplier']);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal <=', $get['end']);  
        }
        if(isset($get['gabah']) && $get['gabah'] != 'all'){
            $this->db->where('a.jenis_gabah', $get['gabah']);  
        }
        if(isset($get['padi']) && $get['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $get['padi']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }
        if(isset($get['proses']) && $get['proses'] != 'all'){
            $this->db->where('a.jenis_proses', $get['proses']);  
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Tersedia
                $this->db->where('stok_sisa >', 0);
            }else if($get['status'] == 2){
                # Kosong
                $this->db->where('stok_sisa', 0);
            }

        }
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->order_by('tanggal', 'desc');
        $result = $this->db->get('stok_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap(){
        $get = $this->input->get();
        if(!isset($get['start']) || $get['start'] == ''){
            $get['start'] = '0000-00-00';
        }
        if(!isset($get['end']) || $get['end'] == ''){
            $get['end'] = date('Y-m-d');
        }
        $stok_masuk_awal = '(sum(if(d.tgl < "'.$get['start'].'" and arus="in", kuantitas, 0)))';
        $stok_keluar_awal = '(sum(if(d.tgl < "'.$get['start'].'" and arus="out", kuantitas, 0)))';
        $stok_masuk_now = '(sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", kuantitas, 0)))';
        $stok_keluar_now = '(sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", kuantitas, 0)))';

        $this->db->select('a.*, b.nama as padi, '.$stok_masuk_awal.' - '.$stok_keluar_awal.' as jml_stok_awal, '.$stok_masuk_awal.' - '.$stok_keluar_awal.' + '.$stok_masuk_now.' - '.$stok_keluar_now.' as jml_sisa_stok, '.$stok_keluar_now.' as jml_stok_terpakai, '.$stok_masuk_now.' as jml_stok_bertambah, round(((sum(if(d.tgl < "'.$get['start'].'" and arus="in", (kuantitas*d.harga), 0))) - (sum(if(d.tgl < "'.$get['start'].'" and arus="out", (kuantitas*d.harga), 0))) + (sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", (kuantitas*d.harga), 0)) - sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", (kuantitas*d.harga), 0)))) / ('.$stok_masuk_awal.' - '.$stok_keluar_awal.' + sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", kuantitas, 0)) - sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", kuantitas, 0)))) as avg_harga_pokok');

        // $this->db->select('a.*, b.nama as padi, sum(stok_sisa) as jml_stok_sisa, sum(stok) as jml_stok_awal, round(sum(stok*harga_satuan)/ sum(stok)) as harga_pokok');

        if(isset($get['gabah']) && $get['gabah'] != 'all'){
            $this->db->where('a.jenis_gabah', $get['gabah']);  
        }
        if(isset($get['padi']) && $get['padi'] != 'all'){
            $this->db->where('a.id_jenis_padi', $get['padi']);  
        }
        if(isset($get['pengeringan']) && $get['pengeringan'] != 'all'){
            $this->db->where('a.jenis_pengeringan', $get['pengeringan']);  
        }
        if(isset($get['proses']) && $get['proses'] != 'all'){
            $this->db->where('a.jenis_proses', $get['proses']);  
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Tersedia
                $this->db->where('stok_sisa >', 0);
            }else if($get['status'] == 2){
                # Kosong
                $this->db->where('stok_sisa', 0);
            }

        }
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $this->db->join('stok_gabah_trx d', 'a.id = d.id_stok');
        $this->db->group_by('jenis_gabah');
        $this->db->group_by('id_jenis_padi');
        $this->db->group_by('jenis_pengeringan');
        $this->db->group_by('jenis_proses');
        $this->db->order_by('tanggal', 'desc');
        $result = $this->db->get('stok_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function hapus_pembelian($id = null){
        if(!empty($id)){
            $this->db->where('sumber', 'pembelian-gabah');
            $this->db->where('id_sumber', $id);
            return $this->db->delete('stok_gabah');
        }else{
            return false;
        }
    }

    public function get_stok_pengeringan($pengeringan){
        $this->db->select('a.*, b.nama as padi');
        $this->db->where('a.jenis_pengeringan', $pengeringan);
        $this->db->where('a.stok_sisa > ', 0);
        $this->db->order_by('a.tanggal', 'desc');
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $result = $this->db->get('stok_gabah a');
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_bahan_gilingan(){
        $this->db->select('a.*, b.nama as padi');
        $this->db->where('a.stok_sisa > ', 0);
        $this->db->where('jenis_gabah', 'kg');
        $this->db->order_by('jenis_proses');
        $this->db->order_by('a.id_jenis_padi', 'desc');
        $this->db->order_by('a.tanggal', 'desc');
        $this->db->join('jenis_padi b', 'a.id_jenis_padi = b.id');
        $result = $this->db->get('stok_gabah a');
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
    }

    
}