<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_model extends MY_Model {
	public $_table = 'produksi';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id)
    {
        $this->db->select('a.*, b.nama as merk');
        $this->db->where('a.id', $id);
        $this->db->join('merk_beras b', 'a.id_merk = b.id');
        return $this->db->get('produksi a')->row();
    }

    public function get_data($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, (sum(if(d.arus="in", if(is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas), 0)) - sum(if(d.arus="out" and is_sisa_produksi = "0", if(is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas), 0))) as total_kuantitas, (sum(if(d.arus="out" and is_sisa_produksi="1", if(is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas ), 0))) as sisa_produksi, c.nama as merk');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") <=', $get['end']);  
        }

        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('a.id_merk', $get['merk']);
        }

        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu Input
                $this->db->having('total_kuantitas', 0);
                $this->db->where('a.status', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 2){
                # Proses produksi
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.status', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.status', 1);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 4){
                # Selesai
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.status', 1);
                $this->db->where('a.persetujuan', 1);
            }

        }
        $this->db->join('produksi_bahan b', 'a.id = b.id_produksi', 'left');
        $this->db->join('merk_beras c', 'a.id_merk = c.id');
        $this->db->join('produksi_proses d', 'b.id_proses = d.id', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('produksi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, (sum(if(d.arus="in", if(d.is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas), 0)) - sum(if(d.arus="out" and d.is_sisa_produksi = "0", if(d.is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas), 0))) as total_kuantitas, (sum(if(d.arus="out" and d.is_sisa_produksi="1", if(d.is_multiply_cor="1", (b.kuantitas * jumlah_cor), b.kuantitas ), 0))) as sisa_produksi, c.nama as merk');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal, "%Y-%m-%d") <=', $get['end']);  
        }

        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('a.id_merk', $get['merk']);
        }

        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->where('a.status', 1);
        $this->db->where('a.persetujuan', 1);
        $this->db->join('produksi_bahan b', 'a.id = b.id_produksi', 'left');
        $this->db->join('merk_beras c', 'a.id_merk = c.id', 'left');
        $this->db->join('produksi_proses d', 'b.id_proses = d.id', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('a.tanggal', 'desc');
        $result = $this->db->get('produksi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_generate_kode($tanggal)
    {
        $this->db->select('count(*) as total');
        $this->db->where('DATE_FORMAT(tanggal, "%Y-%m-%d") =', date('Y-m-d', strtotime($tanggal)));
        $result = $this->db->get('produksi');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'] + 1;
        } else {
            return 1;
        }
    }
}