<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_model extends MY_Model {
	public $_table = 'customer';

	public function __construct()
	{
		parent::__construct();
	}

	public function generate_kode($fitur, $tgl, $cust, $last)
    {
        $index = 1;
        if ($last != false) {
            $temp = explode('-', $last);
            $temp2 = explode('.', $temp[2]);
            $index = intval($temp2[1]) + 1;
        }
        $kode = strtoupper($fitur) . '-' . strtoupper($cust) .  '-' . date("Ym", strtotime($tgl)) . '.' . zerofy($index, 3);
        return $kode;

        //usage
        //construct
        //$this->load->model('generate_model', 'generate'); // model generate kode //
        ## generate kode ##
        // $fitur = 'BB';
        // $tgl = $post['tanggal'];
        // $cust = $this->generate->get_customer_init($post['id_customer']);
        //$last = $this->pembelian_lain->get_kode($tgl);
        // $post['kode'] = $this->generate->generate_kode($fitur, $tgl, $cust, $last);
        ## end generate kode ##
    }
    public function get_customer_init($id)
    {
        $this->db->select('initial');
        $this->db->where('id', $id);
        $result = $this->db->get('customer');
        return $result->row()->initial;
    }
}