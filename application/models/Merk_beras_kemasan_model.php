<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk_beras_kemasan_model extends MY_Model {
	public $_table = 'merk_beras_kemasan';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_data($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.nama as merk, b.*');
        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('a.id', $get['merk']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Aktif
                $this->db->where('b.status', 1);
            }else if($get['status'] == 2){
                # Nonaktif
                $this->db->where('b.status', 0);
            }
        }
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }

        $this->db->order_by('a.nama');
        $this->db->order_by('b.kemasan', 'desc');
        $this->db->join('merk_beras_kemasan b', 'a.id = b.id_merk');
		$result = $this->db->get('merk_beras a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_by($merk, $kemasan){
        $this->db->where('id_merk', $merk);
        $this->db->where('kemasan', $kemasan);
        $result = $this->db->get('merk_beras_kemasan');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

	public function get_active()
	{
		$this->db->select('a.nama as merk, b.*');
        $this->db->order_by('a.nama');
        $this->db->order_by('b.kemasan', 'desc');
        $this->db->join('merk_beras_kemasan b', 'a.id = b.id_merk');
        $this->db->where('b.status', 1);
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
		$result = $this->db->get('merk_beras a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }
    
    public function get_jenis_kemasan()
    {
        $this->db->select('kemasan');
        $this->db->distinct();
        $this->db->order_by('kemasan');
        $result = $this->db->get('merk_beras_kemasan')->result_array();
        return $result;
    }

    public function create_data_kemasan($merk, $kemasan){
        $post = array(
            'id_merk' => $merk,
            'kemasan' => $kemasan,
            'status' => 1,
            'timestamp_update' => setNewDateTime()
        );
        $result = $this->db->insert('merk_beras_kemasan', $post);
        return $this->db->insert_id();
    }
}