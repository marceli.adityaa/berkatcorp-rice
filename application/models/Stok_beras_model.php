<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_beras_model extends MY_Model {
	public $_table = 'stok_beras';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
            return $this->db->get('stok_beras')->row_array();
        }else{
            return $this->db->get('stok_beras');
        }
    }
    
    public function get_data_trx()
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.kemasan, c.nama as merk');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal <=', $get['end']);  
        }
        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('b.id_merk', $get['merk']);  
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Tersedia
                $this->db->where('stok_sisa >', 0);
            }else if($get['status'] == 2){
                # Kosong
                $this->db->where('stok_sisa', 0);
            }

        }
        $this->db->join('merk_beras_kemasan b', 'a.id_kemasan = b.id');
        $this->db->join('merk_beras c', 'b.id_merk = c.id');
        $this->db->order_by('tanggal', 'desc');
        $result = $this->db->get('stok_beras a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap()
    {
        $get = $this->input->get();
        if(!isset($get['start']) || $get['start'] == ''){
            $get['start'] = '0000-00-00';
        }
        if(!isset($get['end']) || $get['end'] == ''){
            $get['end'] = date('Y-m-d');
        }

        $stok_masuk_awal = '(sum(if(d.tgl < "'.$get['start'].'" and arus="in", kuantitas, 0)))';
        $stok_keluar_awal = '(sum(if(d.tgl < "'.$get['start'].'" and arus="out", kuantitas, 0)))';
        $stok_masuk_now = '(sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", kuantitas, 0)))';
        $stok_keluar_now = '(sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", kuantitas, 0)))';

        $this->db->select('a.*, b.kemasan, c.nama as merk, '.$stok_masuk_awal.' - '.$stok_keluar_awal.' as jml_stok_awal, '.$stok_masuk_awal.' - '.$stok_keluar_awal.' + '.$stok_masuk_now.' - '.$stok_keluar_now.' as jml_sisa_stok, '.$stok_keluar_now.' as jml_stok_terpakai, '.$stok_masuk_now.' as jml_stok_bertambah, round(((sum(if(d.tgl < "'.$get['start'].'" and arus="in", (kuantitas*d.harga), 0))) - (sum(if(d.tgl < "'.$get['start'].'" and arus="out", (kuantitas*d.harga), 0))) + (sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", (kuantitas*d.harga), 0)) - sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", (kuantitas*d.harga), 0)))) / ('.$stok_masuk_awal.' - '.$stok_keluar_awal.' + sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="in", kuantitas, 0)) - sum(if(d.tgl >= "'.$get['start'].'" and d.tgl <= "'.$get['end'].'" and arus="out", kuantitas, 0)))) as avg_harga_pokok');

        if(isset($get['merk']) && $get['merk'] != 'all'){
            $this->db->where('b.id_merk', $get['merk']);  
        }
        
        // if(isset($get['status']) && $get['status'] != 'all'){
        //     if($get['status'] == 1){
        //         # Tersedia
        //         $this->db->where('stok_sisa >', 0);
        //     }else if($get['status'] == 2){
        //         # Kosong
        //         $this->db->where('stok_sisa', 0);
        //     }
        // }
        $this->db->join('merk_beras_kemasan b', 'a.id_kemasan = b.id');
        $this->db->join('merk_beras c', 'b.id_merk = c.id');
        $this->db->join('stok_beras_trx d', 'a.id = d.id_stok');
        $this->db->group_by('b.id');
        $this->db->order_by('c.nama', 'asc');
        $this->db->order_by('b.kemasan', 'desc');
        $result = $this->db->get('stok_beras a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_by_kemasan($kemasan)
    {
        $this->db->where('id_kemasan', $kemasan);
        $this->db->where('stok_sisa >',0);
        $this->db->order_by('tanggal');
        return $this->db->get('stok_beras')->result_array();
    }
}