<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_gabah_detail_model extends MY_Model
{
    public $_table = 'pembelian_gabah_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_detail($batch = null, $sap = 1)
    {
        $this->db->select('a.*, b.id_jenis_padi, d.nama as padi, c.nama as ka_label, b.pengeringan, b.proses, jenis_gabah');
        $this->db->join('pembelian_gabah_jenis b', 'a.id_jenis = b.id');
        $this->db->join('kadar_air c', 'a.kadar_air = c.id');
        $this->db->join('jenis_padi d', 'b.id_jenis_padi = d.id');
        $this->db->where('b.id_batch', $batch);
        $this->db->where('a.sap', $sap);
        $this->db->order_by('a.sap');
        $this->db->order_by('c.nama');
        $result = $this->db->get('pembelian_gabah_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_total_sak($pembelian = null){
        $this->db->select('b.id as id_batch, sum(jumlah_sak) as jumlah, b.berat_netto, (b.berat_netto - berat_timbang_kecil) as berat_netto_nett , sum(c.berat_beras_netto) as jumlah_beras_netto, is_manual, c.potong_lain, c.potong_sak');
        $this->db->join('pembelian_gabah_batch b', 'a.id = b.id_pembelian');
        $this->db->join('pembelian_gabah_jenis c', 'b.id = c.id_batch');
        $this->db->join('pembelian_gabah_detail d', 'c.id = d.id_jenis');
        if(!empty($pembelian)){
            $this->db->where('a.id', $pembelian);
        }
        $this->db->group_by('a.id');
        $this->db->group_by('b.id');
        // $this->db->group_by('is_manual', 0);
        $result = $this->db->get('pembelian_gabah a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_by_jenis($jenis){
        $this->db->where('id_jenis', $jenis);
        return $this->db->delete('pembelian_gabah_detail');
    }

    public function delete_by_jenis_sap($jenis, $sap){
        $this->db->where('id_jenis', $jenis);
        $this->db->where('sap', $sap);
        return $this->db->delete('pembelian_gabah_detail');
    }

    public function is_available_detail_by_jenis($jenis){
        $this->db->where('id_jenis', $jenis);
        $result = $this->db->get('pembelian_gabah_detail');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
