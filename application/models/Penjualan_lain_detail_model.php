<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_lain_detail_model extends MY_Model {
	public $_table = 'penjualan_lain_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('penjualan_lain_detail')->row();
    }

    public function get_data_detail($id){
        $this->db->select('a.*, c.nama as kategori');
        $this->db->where('a.id_penjualan', $id);
        $this->db->join('penjualan_lain b', 'a.id_penjualan = b.id');
        $this->db->join('master_penjualan_lain c', 'a.id_kategori = c.id');
        $this->db->order_by('a.timestamp_input');
        $result = $this->db->get('penjualan_lain_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_penjualan', $id);
        return $this->db->delete('penjualan_lain_detail');
    }
    
}