<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_lain_detail_model extends MY_Model {
	public $_table = 'pembelian_lain_detail';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id){
        $this->db->where('id', $id);
        return $this->db->get('pembelian_lain_detail')->row();
    }

    public function get_data_detail($id){
        $this->db->select('a.*, c.nama as jenis_bahan');
        $this->db->where('a.id_pembelian', $id);
        $this->db->join('pembelian_lain b', 'a.id_pembelian = b.id');
        $this->db->join('jenis_bahan c', 'a.id_jenis_bahan = c.id');
        $this->db->order_by('a.timestamp_input');
        $result = $this->db->get('pembelian_lain_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete_transaksi($id){
        $this->db->where('id_pembelian', $id);
        return $this->db->delete('pembelian_lain_detail');
    }
    
}