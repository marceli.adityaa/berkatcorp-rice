<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggilingan_model extends MY_Model {
	public $_table = 'penggilingan';

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('penggilingan')->row();
    }

    public function get_data($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }

        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu Input
                $this->db->having('total_kuantitas', 0);
            }else if($get['status'] == 2){
                # Proses penggilingan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Selesai
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan >', 1);
            }

        }
        $this->db->join('penggilingan_detail c', 'a.id = c.id_penggilingan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_hasil_penggilingan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, c.jenis_gabah, d.nama as padi, coalesce(sum(b.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }

        $this->db->where('a.kuantitas >', 0);
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu Input
                $this->db->having('total_kuantitas', 0);
            }else if($get['status'] == 2){
                # Proses penggilingan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 0);
            }else if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Selesai
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan >', 1);
            }

        }
        $this->db->join('penggilingan_detail b', 'a.id = b.id_penggilingan', 'left');
        $this->db->join('stok_gabah c', 'b.id_stok = c.id');
        $this->db->join('jenis_padi d', 'c.id_jenis_padi = d.id');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_persetujuan_penggilingan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, coalesce(sum(c.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }

        if(isset($get['penggilingan']) && $get['penggilingan'] != 'all'){
            $this->db->where('a.jenis_penggilingan', $get['penggilingan']);  
        }

        $this->db->where('a.kuantitas >', 0);
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 3){
                # Menunggu Persetujuan
                $this->db->having('total_kuantitas >', 0);
                $this->db->where('a.persetujuan', 1);
            }else if($get['status'] == 4){
                # Acc
                $this->db->where('a.persetujuan', 2);
            }else if($get['status'] == 5){
                # Reject
                $this->db->where('a.persetujuan', 3);
            }

        }
        $this->db->where('persetujuan >', 0);
        $this->db->join('penggilingan_detail c', 'a.id = c.id_penggilingan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_rekap_penggilingan()
    {
        $get = $this->input->get();
        $this->db->select('a.*, coalesce(sum(b.kuantitas),0) as total_kuantitas');

        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('DATE_FORMAT(a.tanggal_mulai, "%Y-%m-%d") <=', $get['end']);  
        }
        $this->db->where('persetujuan', 2);
        $this->db->join('penggilingan_detail b', 'a.id = b.id_penggilingan', 'left');
        $this->db->group_by('a.id');
        $this->db->order_by('tanggal_mulai', 'desc');
        $result = $this->db->get('penggilingan a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_data_generate_kode($tanggal)
    {
        $this->db->select('count(*) as total');
        $this->db->where('DATE_FORMAT(tanggal_mulai, "%Y-%m-%d") =', date('Y-m-d', strtotime($tanggal)));
        $result = $this->db->get('penggilingan');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'] + 1;
        } else {
            return 1;
        }
    }
    
}