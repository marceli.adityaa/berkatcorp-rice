<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller_api extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function get($token = '')
	{
		# Autentikasi Token
		if(!$this->validateToken($token)) {
            show_404();
        }
		# Get Param
		$id = $this->input->get('id');
		$callback = $this->input->get('callback');
		# If ID is empty
		if (empty($id)) {
			die(json_encode(array()));
		}
		# Get data from db
		$data = $this->model->get($id);
		# If there is callback
		if($callback && method_exists($this, $callback)) {
			$data = $this->{$callback}($data);
		}
		# Return
		echo json_encode($data);
	}

	public function get_many($token = '')
	{
		# Autentikasi Token
		if(!$this->validateToken($token)) {
            show_404();
        }
		# Get param from db
		$param = $this->input->get();
		# Check if there is callback
		$callback = isset($param['callback']) ? $param['callback'] : '';
		unset($param['callback']);
		# Get data from DB
		$result = $this->model->get_data($param);
		# Check if there is callback
		if ($callback && method_exists($this, $callback)) {
			$result = $this->{$callback}($result);
		}

		echo json_encode($result);
	}

	public function save($token = '')
	{
		# Autentikasi Token
		if(!$this->validateToken($token)) {
            show_404();
        }

		$data = $this->input->post();

		if (!$data['id']) {
			# Insert
			$this->model->insert($data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
		} else {
			# Update
			$id = $data['id'];
			unset($data['id']);
			$this->model->update($id, $data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
		}
	}

	public function disable($token = '')
	{
		if(!$this->validateToken($token)) {
            show_404();
        }

		$id  = $this->input->get('id');
		$par = $this->input->get('par');
		$par = $par == 'true' ? 1 : 0;

		$this->model->update($id, array('is_disabled' => $par));
		die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di' . ($par ? 'nonaktifkan' : 'aktifkan'))));
	}
}
