<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller_admin extends MY_Controller
{
    public $auth_type = 'external';
    public $app_id = 'bas';
    public $class_id  = '';
    public $bypass_auth = FALSE;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model', 'menu');
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method)) {
            if ($this->session->auth && $this->validateToken($this->session->auth['token'])) {
                $hak = json_decode($this->session->auth['hak_akses']);
                // dump($this);
                if(!empty($hak->{$this->app_id})){
                    if($this->bypass_auth || in_array($this->class_id, $hak->{$this->app_id}) || $hak->{$this->app_id}[0] == '*') {
                        return call_user_func_array(array($this, $method), $params);
                    } else {
                        show_error("Anda tidak memiliki hak akses untuk halaman ini.");
                    }
                }else{
                    // show_error("Anda tidak memiliki hak akses untuk halaman ini.");
                }
                
            } else {
                $this->go('sso/client/deauth');
            }
        } else {
            show_404();
        }
    }

    protected function render($view, $data = array())
    {
        $data['menu'] = $this->menu->get_sidebar_menu();
        $this->blade->render('modules/' . $view, $data);
    }
}
