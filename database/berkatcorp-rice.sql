-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2021 at 05:46 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp-rice`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Jember', 1, 0),
(2, 'Surabaya', 1, 0),
(3, 'Bondowoso', 0, 0),
(4, 'Kalimantan', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `arus_kas`
--

CREATE TABLE `arus_kas` (
  `id` int(11) NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_kas` int(11) NOT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `arus` enum('in','out','') NOT NULL,
  `sumber` varchar(255) NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `nominal` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `input_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_approval` tinyint(1) NOT NULL,
  `approval_by` int(11) NOT NULL,
  `approval_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `arus_kas`
--

INSERT INTO `arus_kas` (`id`, `id_perusahaan`, `id_kas`, `tgl_transaksi`, `arus`, `sumber`, `id_sumber`, `keterangan`, `nominal`, `deskripsi`, `input_by`, `input_timestamp`, `is_approval`, `approval_by`, `approval_timestamp`) VALUES
(4, 5, 2, '2020-02-23 00:00:00', 'out', 'master-transaksi-piutang', 1, 'Tambah Piutang', 15000000, 'cash - transaksi awal', 1, '2020-03-02 12:51:17', 0, 0, '0000-00-00 00:00:00'),
(5, 5, 2, '2020-03-01 00:00:00', 'in', 'master-transaksi-piutang', 3, 'Bayar Pokok', 5000000, 'cash - cicilan pertama', 1, '2020-03-05 04:15:24', 0, 0, '0000-00-00 00:00:00'),
(6, 5, 2, '2020-03-01 00:00:00', 'in', 'master-transaksi-piutang', 3, 'Bayar Bunga', 500000, 'cash - cicilan pertama', 1, '2020-03-05 04:15:25', 0, 0, '0000-00-00 00:00:00'),
(7, 5, 2, '2020-03-03 00:00:00', 'in', 'master-transaksi-piutang', 4, 'Bayar Pokok', 2000000, 'cash - ', 1, '2020-03-05 07:05:07', 0, 0, '0000-00-00 00:00:00'),
(8, 5, 2, '2020-03-03 00:00:00', 'in', 'master-transaksi-piutang', 4, 'Bayar Bunga', 100000, 'cash - ', 1, '2020-03-05 07:05:07', 0, 0, '0000-00-00 00:00:00'),
(9, 5, 4, '2020-03-04 00:00:00', 'out', 'master-transaksi-piutang', 5, 'Tambah Piutang', 10000000, 'transfer - ', 1, '2020-03-05 07:08:06', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Gabah', 1, 0),
(2, 'Beras', 0, 0),
(3, 'Broken', 1, 0),
(4, 'Big Broken', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `beban`
--

CREATE TABLE `beban` (
  `id` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `id_beban` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beban`
--

INSERT INTO `beban` (`id`, `tgl_transaksi`, `id_beban`, `nominal`, `catatan`, `jenis_pembayaran`, `id_kas`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, '2020-10-08', 5, 5000000, '123', 'cash', 2, 1, 1, '2020-10-08 16:26:53', 1, '2020-10-13 17:21:01'),
(2, '2020-10-06', 2, 10000000, 'sewa A', 'transfer', 4, 1, 1, '2020-10-08 16:28:26', 1, '2020-10-13 17:01:38'),
(3, '2021-06-26', 2, 40000000, 'gaji borongan juni 2021', 'transfer', 4, 1, 1, '2021-06-26 15:22:57', 1, '2021-06-26 15:33:19'),
(4, '2021-09-06', 3, 1200000, 'gaji rudi', 'transfer', 4, 1, 1, '2021-09-06 16:13:28', 1, '2021-09-06 16:13:55'),
(5, '2021-09-10', 7, 200001, 'Beli Token Listrik Mess 3', 'transfer', 4, 1, 1, '2021-09-10 15:17:52', 1, '2021-09-10 15:19:05'),
(6, '2021-09-20', 2, 120000, 'Bayar Borongan', 'cash', 2, 0, 1, '2021-09-20 15:05:25', 0, '0000-00-00 00:00:00'),
(7, '2021-09-20', 10, 2000000, 'Retur BK@25', 'cash', 1, 1, 1, '2021-09-22 17:46:59', 1, '0000-00-00 00:00:00'),
(8, '2021-09-23', 10, 1000000, 'Pembayaran tempo untuk retur Manna@25', 'cash', 2, 1, 1, '2021-09-23 11:44:12', 1, '0000-00-00 00:00:00'),
(9, '2021-09-22', 10, 2000000, 'Retur Manna@5, MM@5, Berkat@5', 'cash', 2, 1, 1, '2021-09-23 13:55:45', 1, '2021-09-23 13:55:45'),
(10, '2021-09-23', 10, 1000006, 'Pembayaran tempo untuk retur Beras6', 'transfer', 5, 1, 1, '2021-09-23 14:26:52', 1, '2021-09-23 14:26:52'),
(11, '2021-09-24', 4, 150000, 'Biaya tambahan pembelian PBN-004/09/21', 'cash', 2, 1, 1, '2021-09-25 16:52:15', 1, '2021-09-25 16:52:15'),
(12, '2021-09-25', 9, 200000, 'Potongan penjualan PJL-003/09/21', 'cash', 2, 1, 1, '2021-09-25 17:52:10', 1, '2021-09-25 17:52:10'),
(13, '2021-09-27', 9, 50000, 'Potongan penjualan PJL-002/09/21', 'transfer', 4, 1, 1, '2021-09-25 18:19:03', 1, '2021-09-25 18:19:03'),
(14, '2021-09-27', 9, 93000, 'Potongan penjualan beras PO-016/09/21', 'transfer', 4, 1, 1, '2021-09-27 16:22:51', 1, '2021-09-27 16:22:51'),
(15, '2021-09-28', 9, 291000, 'Potongan penjualan beras PO-017/09/21', 'transfer', 5, 1, 1, '2021-09-27 17:01:55', 1, '2021-09-27 17:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `initial` varchar(3) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `id_area` int(11) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `saldo_sak` int(11) NOT NULL,
  `saldo_bon` int(11) NOT NULL,
  `limit_bon` int(11) NOT NULL,
  `supply_gabah` tinyint(1) NOT NULL,
  `supply_beras` tinyint(1) NOT NULL,
  `is_primary` tinyint(1) NOT NULL,
  `is_supplier` tinyint(2) NOT NULL,
  `is_customer` tinyint(2) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `initial`, `alamat`, `id_area`, `telp`, `saldo_sak`, `saldo_bon`, `limit_bon`, `supply_gabah`, `supply_beras`, `is_primary`, `is_supplier`, `is_customer`, `input_by`, `timestamp`, `status`, `is_deleted`) VALUES
(1, 'Supplier A', 'SPA', 'Banyuwangi Kota', 1, '08123232323', 300, 10000000, 100000000, 1, 1, 0, 1, 1, 1, '2019-09-19 11:38:34', 1, 0),
(2, 'Supplier B', 'SPB', '', 2, '', 0, 0, 0, 0, 0, 0, 1, 0, 1, '2019-09-25 10:47:03', 1, 0),
(3, 'Pak Lia', 'LIA', 'Jember', 1, '', 0, 0, 0, 1, 0, 0, 1, 0, 1, '2019-11-26 16:19:57', 1, 0),
(4, 'Ivon', 'IVN', 'Jember', 1, '', 0, 0, 0, 1, 1, 0, 1, 0, 1, '2019-12-13 14:09:08', 1, 0),
(5, 'Seke', 'SKE', 'Jember', 1, '', 0, 0, 0, 1, 1, 0, 1, 0, 1, '2019-12-13 14:32:19', 1, 0),
(6, 'Alex', 'ALX', 'Jember', 1, '', 0, 0, 0, 1, 0, 0, 1, 0, 1, '2020-01-11 16:49:11', 1, 0),
(7, 'Magda', 'MGD', '', 1, '', 0, 0, 0, 1, 1, 0, 1, 1, 1, '2020-01-11 16:49:21', 1, 0),
(8, 'Gapur', 'GPR', 'Jember', 1, '088838232', 0, 0, 0, 1, 1, 0, 1, 0, 1, '2020-01-15 11:26:17', 1, 0),
(9, 'PT. Berkat Anoegerah Sejahtera', 'BAS', '', 1, '', 0, 0, 0, 0, 0, 1, 1, 1, 1, '2020-02-15 12:09:08', 1, 0),
(10, 'Ruko', 'RKO', 'Ruko Surabaya', 2, '12321321', 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-06-29 18:46:42', 1, 0),
(11, 'Denny', 'DNI', 'Jl. Raya Bondowoso 14, Bondowoso', 1, '081232323444', 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-07-01 14:34:27', 1, 0),
(12, 'Indra', 'IRA', '', 1, '', 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-07-16 15:01:16', 1, 0),
(13, 'H. Kus', 'KUS', '', 1, '', 0, 0, 0, 1, 0, 0, 1, 0, 1, '2021-06-04 13:59:34', 1, 0),
(14, 'Sibro', 'SBR', '', 1, '', 0, 0, 0, 1, 1, 0, 1, 0, 1, '2021-07-07 17:15:31', 1, 0),
(15, 'Adam', 'adm', 'Jember', 1, '', 0, 0, 0, 0, 0, 0, 1, 0, 1, '2021-08-06 14:31:05', 1, 0),
(16, 'Abdullah', 'Abd', 'Jl Kupang Jaya', 2, '082554927811', 0, 0, 0, 0, 0, 0, 0, 1, 1, '2021-09-27 17:21:04', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_hutang` date NOT NULL,
  `nominal_hutang` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hutang`
--

INSERT INTO `hutang` (`id`, `id_customer`, `tgl_hutang`, `nominal_hutang`, `keterangan`, `is_paid_off`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, 1, '2020-10-07', 50000000, 'tes1', 0, 0, 1, '2020-10-07 10:14:01', 0, '0000-00-00 00:00:00'),
(2, 15, '2021-09-06', 200000000, 'Hutang', 0, 0, 1, '2021-09-06 15:58:34', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hutang_detail`
--

CREATE TABLE `hutang_detail` (
  `id` int(11) NOT NULL,
  `id_hutang` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `id_source` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tambah_hutang` bigint(20) NOT NULL,
  `bayar_pokok` bigint(20) NOT NULL,
  `bayar_bunga` bigint(20) NOT NULL,
  `pokok_awal` bigint(20) NOT NULL,
  `sisa_pokok` bigint(20) NOT NULL,
  `remark` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hutang_detail`
--

INSERT INTO `hutang_detail` (`id`, `id_hutang`, `source`, `id_source`, `tgl_transaksi`, `tambah_hutang`, `bayar_pokok`, `bayar_bunga`, `pokok_awal`, `sisa_pokok`, `remark`, `jenis_pembayaran`, `id_kas`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, 1, 'master-transaksi-hutang', 0, '2020-10-07', 50000000, 0, 0, 0, 50000000, 'transaksi awal', 'transfer', 4, 1, 1, '2020-10-08 14:43:03', 1, '2020-10-07 13:51:50'),
(2, 1, 'master-transaksi-hutang', 0, '2020-10-07', 0, 10000000, 500000, 50000000, 40000000, '', 'transfer', 5, 1, 1, '2020-10-07 13:55:22', 1, '2020-10-07 13:56:39'),
(3, 1, 'master-transaksi-hutang', 0, '2020-11-24', 0, 30000000, 0, 40000000, 10000000, 'bayar utang', 'transfer', 5, 1, 1, '2020-11-24 14:23:02', 1, '2020-11-24 14:23:02'),
(4, 2, 'master-transaksi-hutang', 0, '2021-09-06', 200000000, 0, 0, 0, 200000000, 'transaksi awal', 'transfer', 4, 1, 1, '2021-09-06 15:59:07', 1, '2021-09-06 15:59:07'),
(5, 2, 'master-transaksi-hutang', 0, '2021-09-06', 0, 100000000, 12000000, 200000000, 100000000, '', 'transfer', 4, 1, 1, '2021-09-06 16:00:34', 1, '2021-09-06 16:00:34'),
(6, 1, 'master-transaksi-hutang', 0, '2021-09-07', 0, 10000000, 0, 10000000, 0, '', 'transfer', 4, 1, 1, '2021-09-07 11:05:33', 1, '2021-09-07 11:05:33');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_bahan`
--

CREATE TABLE `jenis_bahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gilingan` tinyint(4) NOT NULL,
  `kebian` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_bahan`
--

INSERT INTO `jenis_bahan` (`id`, `nama`, `gilingan`, `kebian`, `status`, `is_deleted`) VALUES
(1, 'Beras Medium', 1, 0, 1, 0),
(2, 'Menir', 1, 0, 1, 0),
(3, 'Sekam', 1, 0, 1, 0),
(4, 'Katul', 1, 0, 1, 0),
(5, 'Beras Kepala', 1, 1, 1, 0),
(6, 'Beras Broken', 1, 1, 1, 0),
(7, 'Beras KK', 0, 1, 1, 0),
(8, 'Broken A', 0, 0, 1, 0),
(9, 'Broken B', 0, 0, 1, 0),
(10, 'TAP Manna', 0, 0, 1, 0),
(11, 'BB Manna', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_gabah`
--

CREATE TABLE `jenis_gabah` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_gabah`
--

INSERT INTO `jenis_gabah` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Gabah KS', 1, 0),
(2, 'Gabah KG', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_padi`
--

CREATE TABLE `jenis_padi` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_padi`
--

INSERT INTO `jenis_padi` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'IR-64', 1, 0),
(2, 'IR-66', 1, 0),
(3, 'IR-74', 1, 0),
(4, 'GH', 1, 0),
(5, 'IR-32', 1, 0),
(6, 'Legowo', 1, 0),
(7, 'PW', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_sak`
--

CREATE TABLE `jenis_sak` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_sak`
--

INSERT INTO `jenis_sak` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Polar', 1, 0),
(2, 'Polos @50', 1, 0),
(3, 'Manna @25', 1, 0),
(4, 'Manna @10', 1, 0),
(5, 'Manna @5', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kadar_air`
--

CREATE TABLE `kadar_air` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kadar_air` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kadar_air`
--

INSERT INTO `kadar_air` (`id`, `nama`, `kadar_air`, `status`, `is_deleted`) VALUES
(1, 'B1', 1, 1, 0),
(2, 'B2', 2, 1, 0),
(3, 'B3', 3, 1, 0),
(4, 'B4', 4, 1, 0),
(5, 'B5', 5, 1, 0),
(6, 'B6', 6, 1, 0),
(7, 'B7', 7, 1, 0),
(8, 'B8', 8, 1, 0),
(9, 'B9', 9, 1, 0),
(10, 'B10', 10, 1, 0),
(11, 'B0', 0, 1, 0),
(12, 'B11', 11, 1, 0),
(13, 'B12', 12, 1, 0),
(14, 'B13', 13, 1, 0),
(15, 'B14', 14, 1, 0),
(16, 'B15', 15, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_biaya`
--

CREATE TABLE `master_akun_biaya` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_biaya`
--

INSERT INTO `master_akun_biaya` (`id`, `nama`, `status`, `is_deleted`) VALUES
(2, 'Beban Sewa ', 1, 0),
(3, 'Beban Rumah', 1, 0),
(5, 'Beban Gaji', 1, 0),
(6, 'Beban Penjualan', 1, 0),
(7, 'Beban Pembelian', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_biaya_detail`
--

CREATE TABLE `master_akun_biaya_detail` (
  `id` int(11) NOT NULL,
  `id_biaya` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_biaya_detail`
--

INSERT INTO `master_akun_biaya_detail` (`id`, `id_biaya`, `nama`, `status`) VALUES
(1, 5, 'Gaji Kantor', 1),
(2, 5, 'Gaji Borongan', 1),
(3, 5, 'Gaji Harian', 1),
(4, 3, 'Listrik Mess 1', 1),
(5, 0, 'Listrik Mess 2', 1),
(6, 3, 'Listrik Mess 2', 1),
(7, 3, 'Listrik Mess 3', 1),
(8, 2, 'Sewa Tanah', 1),
(9, 6, 'Potongan Penjualan', 1),
(10, 6, 'Retur Penjualan', 1),
(11, 7, 'Biaya Tambahan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_pendapatan`
--

CREATE TABLE `master_akun_pendapatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_pendapatan`
--

INSERT INTO `master_akun_pendapatan` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Penjualan Menir', 1, 0),
(2, 'Penjualan Sekam', 1, 0),
(3, 'Penjualan Ternak', 1, 0),
(4, 'Penjualan Sak', 1, 0),
(5, 'Tambahan Penjualan', 1, 0),
(6, 'Potongan Pembelian', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_dryer`
--

CREATE TABLE `master_dryer` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_dryer`
--

INSERT INTO `master_dryer` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Dryer 1', 1, 0),
(2, 'Dryer 2', 1, 0),
(3, 'Dryer 3', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_jemur`
--

CREATE TABLE `master_jemur` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_jemur`
--

INSERT INTO `master_jemur` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'A-1', 1, 0),
(2, 'A-2', 1, 0),
(3, 'A-3', 1, 0),
(4, 'A-4', 1, 0),
(5, 'B-1', 1, 0),
(6, 'B-2', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_pembelian_lain`
--

CREATE TABLE `master_pembelian_lain` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_pembelian_lain`
--

INSERT INTO `master_pembelian_lain` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Pembelian Jagung', 1, 0),
(2, 'Pembelian Katul', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_penjualan_lain`
--

CREATE TABLE `master_penjualan_lain` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_penjualan_lain`
--

INSERT INTO `master_penjualan_lain` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Penjualan Jagung', 1, 0),
(2, 'Penjualan Menir', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_sales`
--

CREATE TABLE `master_sales` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_sales`
--

INSERT INTO `master_sales` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Tino', 1, 0),
(2, 'Adi', 1, 0),
(3, 'Bhakti', 1, 0),
(4, 'Rani', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT 0,
  `urutan` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `query_badge` text NOT NULL,
  `is_curl` tinyint(1) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `status`, `query_badge`, `is_curl`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', 'icon-home', 'menu-dashboard', 'dashboard', 0, 1, 1, '', 0, 0),
(2, 'kms', 'data master', 'icon-book-open', 'sub-master', '', 0, 2, 1, '', 0, 0),
(3, 'bhn', 'bahan', 'icon-arrow-right', 'menu-bahan', 'master/bahan', 2, 3, 1, '', 0, 1),
(6, 'cust', 'customer / supplier', 'icon-arrow-right', 'menu-customer', 'master/customer', 2, 2, 1, '', 0, 0),
(7, 'xpd', 'ekspedisi', 'fa fa-truck', 'sub-ekspedisi', '', 0, 7, 1, '', 0, 1),
(8, 'xpda', 'pengambilan', 'icon-arrow-right', 'menu-pengambilan', 'ekspedisi/pengambilan', 7, 8, 1, '', 0, 1),
(9, 'xpdj', 'penjemputan', 'icon-arrow-left', 'menu-penjemputan', 'ekspedisi/penjemputan', 7, 9, 1, '', 0, 1),
(10, 'jn-gbh', 'jenis gabah', 'icon-arrow-right', 'menu-jenis-gabah', 'master/jenis_gabah', 2, 4, 1, '', 0, 0),
(11, 'kda', 'kadar air', 'icon-arrow-right', 'menu-kadar-air', 'master/kadar_air', 2, 7, 1, '', 0, 0),
(12, 'pbl', 'pembelian gabah', 'icon icon-basket-loaded', 'sub-pembelian', '', 0, 11, 1, '', 0, 0),
(13, 'pbl-gbh', 'kelola transaksi', 'icon-arrow-right', 'menu-pembelian-gabah', 'pembelian/gabah/kelola', 12, 1, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where status_muatan = 0 and status_selesai = 0', 0, 0),
(14, 'qc', 'quality control', 'icon-arrow-right', 'menu-pembelian-gabah-qc', 'pembelian/gabah/quality_control', 12, 2, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where status_muatan = 0', 0, 0),
(15, 'jn-pdi', 'jenis padi', 'icon-arrow-right', 'menu-jenis-padi', 'master/jenis_padi', 2, 5, 1, '', 0, 0),
(16, 'pic', 'penanggung jawab', 'icon-arrow-right', 'menu-penanggung-jawab', 'master/pic', 2, 7, 1, '', 0, 0),
(17, 'acc', 'persetujuan', 'fa fa-check-square-o', 'sub-persetujuan', '', 0, 12, 1, '', 0, 1),
(18, 'acc-pbl-gb', 'otorisasi transaksi', 'icon-arrow-right', 'menu-persetujuan-pembelian-gabah', 'pembelian/gabah/persetujuan', 12, 5, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where status_selesai = 1 and status_persetujuan = 0\n', 0, 0),
(19, 'pby', 'pembayaran tempo', 'fa fa-money', 'sub-pembayaran', '', 0, 14, 1, '', 0, 1),
(20, 'pby-gbh', 'pelunasan tempo', 'icon-arrow-right', 'menu-pembayaran-pembelian-gabah', 'pembelian/gabah/pembayaran_tempo', 12, 6, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where is_tempo = 1 and is_tempo_dibayar = 0 and status_selesai = 1 and status_persetujuan = 1', 0, 0),
(21, 'jn-sak', 'jenis sak', 'icon-arrow-right', 'menu-jenis-sak', 'master/jenis_sak', 2, 6, 1, '', 0, 0),
(22, 'm-sak', 'manajemen sak', 'icon icon-puzzle', 'sub-manajemen-sak', '', 0, 30, 1, '', 0, 0),
(23, 'acc-sak', 'otorisasi transaksi', 'icon-arrow-right', 'menu-persetujuan-sak', 'manajemen/sak/persetujuan', 22, 2, 1, 'SELECT COUNT(*) as total FROM transaksi_sak_detail where status_acc = 1', 0, 0),
(24, 'kll-sak', 'kelola transaksi sak', 'icon-arrow-right', 'menu-kelola-sak', 'manajemen/sak/kelola', 22, 1, 1, '', 0, 0),
(25, 'krt-sak', 'kartu sak', 'icon-arrow-right', 'menu-kartu-sak', 'manajemen/sak/kartu', 22, 4, 1, '', 0, 0),
(26, 'rkp-sak', 'stok sak', 'icon-arrow-right', 'menu-stok-sak', 'manajemen/sak/stok', 22, 3, 1, '', 0, 0),
(27, 'piut', 'uang muka', 'icon icon-calculator', 'sub-piutang', '', 0, 33, 1, '', 0, 0),
(28, 'piut-kll', 'kelola transaksi', 'icon-arrow-right', 'menu-piutang-kelola', 'manajemen/piutang/kelola', 27, 1, 1, '', 0, 0),
(29, 'piut-acc', 'otorisasi uang muka', 'icon-arrow-right', 'menu-piutang-persetujuan', 'manajemen/piutang/persetujuan', 27, 2, 1, 'SELECT COUNT(*) as total FROM piutang_detail where is_verifikasi = 0', 0, 0),
(30, 'piut-rkp', 'rekap uang muka', 'icon-arrow-right', 'menu-piutang-rekap', 'manajemen/piutang/rekap', 27, 3, 1, '', 0, 0),
(31, 'arsks', 'buku kas', 'fa fa-exchange', 'sub-aruskas', '', 0, 40, 1, '', 0, 0),
(32, 'kll-arsks', 'kelola transaksi', 'icon-arrow-right', 'menu-aruskas-kelola', 'aruskas/kelola', 31, 1, 1, '', 0, 0),
(33, 'acc-arsks', 'otorisasi transaksi', 'icon-arrow-right', 'menu-aruskas-persetujuan', 'aruskas/persetujuan', 31, 3, 1, 'api/external/arus_kas/get_jumlah_pengajuan', 1, 0),
(34, 'rkp-arsks', 'rekap buku kas', 'icon-arrow-right', 'menu-aruskas-rekap', 'aruskas/rekap', 31, 4, 1, '', 0, 0),
(35, 'acc-pby-gb', 'otorisasi pelunasan', 'icon-arrow-right', 'menu-persetujuan-pembayaran-gabah', 'pembelian/gabah/pembayaran_tempo/persetujuan', 12, 7, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah_pembayaran where status_persetujuan = 0\n', 0, 0),
(36, 'jn-bhn', 'jenis bahan', 'icon-arrow-right', 'menu-jenis-bahan', 'master/jenis_bahan', 2, 3, 1, '', 0, 0),
(37, 'stk-gbh', 'stok gabah', 'icon icon-social-dropbox', 'sub-stok-gabah', '', 0, 12, 1, '', 0, 0),
(38, 'kll-stk-gb', 'kelola stok gabah', 'icon-arrow-right', 'menu-kelola-stok-gabah', 'stok/gabah/kelola', 37, 1, 1, '', 0, 0),
(40, 'rkp-stk-gb', 'rekap stok', 'icon-arrow-right', 'menu-rekap-stok-gabah', 'stok/gabah/rekap', 37, 3, 1, '', 0, 0),
(41, 'dry', 'pengeringan', 'fa fa-sun-o', 'sub-pengeringan', '', 0, 15, 1, '', 0, 0),
(42, 'dry-kll', 'kelola pengeringan', 'icon-arrow-right', 'menu-kelola-pengeringan', 'produksi/pengeringan/kelola', 41, 1, 1, '', 0, 0),
(43, 'dry-res', 'Hasil pengeringan', 'icon-arrow-right', 'menu-hasil-pengeringan', 'produksi/pengeringan/hasil', 41, 2, 1, '', 0, 0),
(44, 'dry-acc', 'persetujuan proses', 'icon-arrow-right', 'menu-persetujuan-pengeringan', 'produksi/pengeringan/persetujuan', 41, 3, 1, 'SELECT COUNT(*) as total FROM pengeringan where persetujuan = 0', 0, 0),
(45, 'dry-rkp', 'rekap pengeringan', 'icon-arrow-right', 'menu-rekap-pengeringan', 'produksi/pengeringan/rekap', 41, 4, 1, '', 0, 0),
(46, 'stk-bhn', 'stok bahan', 'icon icon-social-dropbox', 'sub-stok-bahan', '', 0, 13, 1, '', 0, 0),
(47, 'm-dry', 'master dryer', 'icon-arrow-right', 'menu-master-dryer', 'master/dryer', 2, 8, 1, '', 0, 0),
(48, 'm-jemur', 'master lokasi jemur', 'icon-arrow-right', 'menu-master-jemur', 'master/jemur', 2, 9, 1, '', 0, 0),
(49, 'kll-stk-bh', 'kelola stok bahan', 'icon-arrow-right', 'menu-kelola-stok-bahan', 'stok/bahan/kelola', 46, 1, 1, '', 0, 0),
(50, 'rkp-stk-bh', 'rekap stok', 'icon-arrow-right', 'menu-rekap-stok-bahan', 'stok/bahan/rekap', 46, 3, 1, '', 0, 0),
(51, 'grnd', 'penggilingan', 'fa fa-recycle', 'sub-penggilingan', '', 0, 16, 1, '', 0, 0),
(52, 'grnd-kll', 'kelola penggilingan', 'icon-arrow-right', 'menu-kelola-penggilingan', 'produksi/penggilingan/kelola', 51, 1, 1, '', 0, 0),
(53, 'grnd-res', 'Hasil Penggilingan', 'icon-arrow-right', 'menu-hasil-penggilingan', 'produksi/penggilingan/hasil', 51, 2, 1, 'SELECT COUNT(*) as total FROM penggilingan where persetujuan = 0', 0, 0),
(54, 'grnd-acc', 'persetujuan proses', 'icon-arrow-right', 'menu-persetujuan-penggilingan', 'produksi/penggilingan/persetujuan', 51, 3, 1, 'SELECT COUNT(*) as total FROM penggilingan where persetujuan = 0', 0, 0),
(55, 'grnd-rkp', 'rekap penggilingan', 'icon-arrow-right', 'menu-rekap-penggilingan', 'produksi/penggilingan/rekap', 51, 4, 1, '', 0, 0),
(56, 'm-merk', 'merk beras', 'icon-arrow-right', 'menu-merk-beras', 'master/merk_beras', 2, 10, 1, '', 0, 0),
(58, 'm-kmsn', 'kemasan beras', 'icon-arrow-right', 'menu-kemasan-beras', 'master/merk_beras/kemasan', 2, 13, 1, '', 0, 0),
(59, 'stk-bs', 'stok beras', 'icon icon-grid', 'sub-stok-beras', '', 0, 14, 1, '', 0, 0),
(60, 'kll-stk-bs', 'kelola stok beras', 'icon-arrow-right', 'menu-kelola-stok-beras', 'stok/beras/kelola', 59, 1, 1, '', 0, 0),
(61, 'rkp-stk-bs', 'rekap stok', 'icon-arrow-right', 'menu-rekap-stok-beras', 'stok/beras/rekap', 59, 1, 1, '', 0, 0),
(62, 'area', 'area', 'icon-arrow-right', 'menu-area', 'master/area', 2, 1, 1, '', 0, 0),
(63, 'm-sales', 'master sales', 'icon-arrow-right', 'menu-sales', 'master/sales', 2, 12, 1, '', 0, 0),
(64, 'pj', 'penjualan beras', 'fa fa-book', 'sub-penjualan', '', 0, 30, 1, '', 0, 0),
(65, 'pj-po', 'surat pemesanan', 'icon-arrow-right', 'menu-pj-po', 'penjualan/beras/surat_pemesanan', 64, 2, 1, '', 0, 0),
(66, 'pj-op', 'order produksi', 'icon-arrow-right', 'menu-pj-op', 'penjualan/beras/order_produksi', 64, 3, 1, '', 0, 0),
(68, 'pj-np', 'nota penjualan', 'icon-arrow-right', 'menu-pj-fp', 'penjualan/beras/faktur', 64, 5, 1, '', 0, 0),
(69, 'pj-kt', 'penjualan kontan', 'icon-arrow-right', 'menu-pj-kt', 'penjualan/beras/kontan', 64, 6, 1, '', 0, 0),
(70, 'pj-rkp', 'rekap transaksi', 'icon-arrow-right', 'menu-pj-rkp', 'penjualan/beras/rekap', 64, 11, 1, '', 0, 0),
(71, 'pj-acc', 'otorisasi transaksi', 'icon-arrow-right', 'menu-pj-acc', 'penjualan/beras/persetujuan', 64, 7, 1, 'SELECT COUNT(*) as total FROM penjualan_po where persetujuan = 0 and status_selesai = 1', 0, 0),
(72, 'pj-tmp', 'pelunasan tempo', 'icon-arrow-right', 'menu-pj-tempo', 'penjualan/beras/pembayaran_tempo', 64, 8, 1, 'SELECT COUNT(*) as total FROM penjualan_po where persetujuan = 1 and is_tempo= 1 and is_lunas = 0', 0, 0),
(73, 'pj-tmp-acc', 'otorisasi pembayaran', 'icon-arrow-right', 'menu-pj-tempo-acc', 'penjualan/beras/pembayaran_tempo/persetujuan', 64, 9, 1, 'SELECT COUNT(*) as total FROM penjualan_po_pembayaran where status_persetujuan = 0', 0, 0),
(74, 'm-ak-bbn', 'akun biaya', 'icon-arrow-right', 'menu-akun-biaya', 'master/akun/biaya', 2, 14, 1, '', 0, 0),
(75, 'm-ak-pdp', 'akun pendapatan', 'icon-arrow-right', 'menu-akun-pendapatan', 'master/akun/pendapatan', 2, 15, 1, '', 0, 0),
(76, 'm-itm-pb', 'item pembelian lain', 'icon-arrow-right', 'menu-item-pembelian-lain', 'master/item/pembelian_lain', 2, 16, 1, '', 0, 0),
(77, 'm-itm-pj', 'item penjualan lain', 'icon-arrow-right', 'menu-item-penjualan-lain', 'master/item/penjualan_lain', 2, 17, 1, '', 0, 0),
(78, 'htg', 'Hutang', 'icon icon-fire', 'sub-hutang', '', 0, 33, 1, '', 0, 0),
(79, 'htg-kll', 'kelola transaksi', 'icon-arrow-right', 'menu-hutang-kelola', 'manajemen/hutang/kelola', 78, 1, 1, '', 0, 0),
(80, 'htg-acc', 'otorisasi hutang', 'icon-arrow-right', 'menu-hutang-persetujuan', 'manajemen/hutang/persetujuan', 78, 2, 1, 'SELECT COUNT(*) as total FROM hutang_detail where is_verifikasi = 0', 0, 0),
(81, 'htg-rkp', 'rekap hutang', 'icon-arrow-right', 'menu-hutang-rekap', 'manajemen/hutang/rekap', 78, 3, 1, '', 0, 0),
(82, 'bbn', 'Biaya / Beban', 'icon icon-basket', 'sub-beban', '', 0, 34, 1, '', 0, 0),
(83, 'bbn-kll', 'kelola beban', 'icon-arrow-right', 'menu-beban-kelola', 'manajemen/beban/kelola', 82, 1, 1, '', 0, 0),
(84, 'bbn-acc', 'otorisasi beban', 'icon-arrow-right', 'menu-beban-persetujuan', 'manajemen/beban/persetujuan', 82, 2, 1, 'SELECT COUNT(*) as total FROM beban where is_verifikasi = 0', 0, 0),
(85, 'bbn-rkp', 'rekap beban', 'icon-arrow-right', 'menu-beban-rekap', 'manajemen/beban/rekap', 82, 3, 1, '', 0, 0),
(86, 'pdt', 'pendapatan lain', 'icon icon-share-alt', 'sub-pendapatan', '', 0, 35, 1, '', 0, 0),
(87, 'pdt-kll', 'kelola pendapatan', 'icon-arrow-right', 'menu-pendapatan-kelola', 'manajemen/pendapatan/kelola', 86, 1, 1, '', 0, 0),
(88, 'pdt-acc', 'otorisasi pendapatan', 'icon-arrow-right', 'menu-pendapatan-persetujuan', 'manajemen/pendapatan/persetujuan', 86, 2, 1, 'SELECT COUNT(*) as total FROM pendapatan where is_verifikasi = 0', 0, 0),
(89, 'pdt-rkp', 'rekap pendapatan', 'icon-arrow-right', 'menu-pendapatan-rekap', 'manajemen/pendapatan/rekap', 86, 3, 1, '', 0, 0),
(90, 'pjl', 'penjualan lain', 'fa fa-bookmark-o', 'sub-penjualan-lain', '', 0, 31, 1, '', 0, 0),
(91, 'pjl-kll', 'kelola transaksi', 'icon-arrow-right', 'menu-pjl-kll', 'penjualan/lain/kelola', 90, 1, 1, '', 0, 0),
(92, 'pjl-rkp', 'rekap transaksi', 'icon-arrow-right', 'menu-pjl-rkp', 'penjualan/lain/rekap', 90, 6, 1, '', 0, 0),
(93, 'pjl-acc', 'otorisasi transaksi', 'icon-arrow-right', 'menu-pjl-acc', 'penjualan/lain/persetujuan', 90, 3, 1, 'SELECT COUNT(*) as total FROM penjualan_lain where persetujuan = 0 and status = 1', 0, 0),
(94, 'pjl-tmp', 'pelunasan tempo', 'icon-arrow-right', 'menu-pjl-tempo', 'penjualan/lain/pembayaran_tempo', 90, 4, 1, 'SELECT COUNT(*) as total FROM penjualan_lain where persetujuan = 1 and is_tempo= 1 and is_lunas = 0', 0, 0),
(95, 'pjl-tmp-ac', 'otorisasi pembayaran', 'icon-arrow-right', 'menu-pjl-tempo-acc', 'penjualan/lain/pembayaran_tempo/persetujuan', 90, 5, 1, 'SELECT COUNT(*) as total FROM penjualan_lain_pembayaran where status_persetujuan = 0', 0, 0),
(96, 'mts-arsks', 'mutasi akun', 'icon-arrow-right', 'menu-aruskas-mutasi', 'aruskas/mutasi', 31, 2, 1, '', 0, 0),
(97, 'pj-rtr', 'Retur Penjualan', 'fa fa-bookmark-o', 'menu-penjualan-retur', 'penjualan/beras/retur', 64, 12, 1, '', 0, 0),
(98, 'rkp-pb-gb', 'rekap pembelian', 'icon-arrow-right', 'menu-rekap-pembelian-gabah', 'pembelian/gabah/rekap', 12, 8, 1, '', 0, 0),
(99, 'pbg-np', 'Nota Pembelian', 'icon-arrow-right', 'menu-pembelian-gabah-faktur', 'pembelian/gabah/faktur', 12, 4, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where status_muatan = 1 and status_selesai = 0', 0, 0),
(100, 'm-prd', 'proses produksi', 'icon-arrow-right', 'menu-proses-produksi', 'master/proses_produksi', 2, 11, 1, '', 0, 0),
(101, 'prd', 'produksi', 'icon icon-speedometer', 'sub-produksi', '', 0, 17, 1, '', 0, 0),
(102, 'prd-kll', 'kelola produksi', 'icon-arrow-right', 'menu-kelola-produksi', 'produksi/beras/kelola', 101, 1, 1, 'SELECT COUNT(*) as total FROM produksi where status = 0', 0, 0),
(103, 'prd-hsl', 'hasil produksi', 'icon-arrow-right', 'menu-hasil-produksi', 'produksi/beras/hasil', 101, 2, 1, '', 0, 1),
(104, 'prd-acc', 'persetujuan proses', 'icon-arrow-right', 'menu-persetujuan-produksi', 'produksi/beras/persetujuan', 101, 3, 1, 'SELECT COUNT(*) as total FROM produksi where status = 1 and persetujuan = 0', 0, 0),
(105, 'prd-rkp', 'rekap produksi', 'icon-arrow-right', 'menu-rekap-produksi', 'produksi/beras/rekap', 101, 4, 1, '', 0, 0),
(106, 'ts-gbh', 'buku tes', 'icon-arrow-right', 'menu-pembelian-gabah-tes', 'pembelian/gabah/quality_control/buku_tes', 12, 3, 1, 'SELECT COUNT(*) as total FROM pembelian_gabah where status_tes = 0 and status_muatan = 1', 0, 0),
(107, 'pj-xpd', 'ekspedisi', 'icon-arrow-right', 'menu-pj-ekspedisi', 'penjualan/beras/ekspedisi', 64, 6, 1, 'SELECT COUNT(*) as total FROM penjualan_po WHERE `status_po` = 1 AND `status_pengiriman` = 0', 0, 0),
(108, 'pbl', 'pembelian bahan', 'fa fa-shopping-basket', 'sub-pembelian-lain', '', 0, 11, 1, '', 0, 0),
(109, 'pbl-kll', 'kelola transaksi', 'icon-arrow-right', 'menu-pbl-kll', 'pembelian/lain/kelola', 108, 1, 1, '', 0, 0),
(110, 'pbl-acc', 'otorisasi transaksi', 'icon-arrow-right', 'menu-pbl-acc', 'pembelian/lain/persetujuan', 108, 2, 1, 'SELECT COUNT(*) as total FROM pembelian_lain where status = 1 and persetujuan = 0', 0, 0),
(111, 'pbl-tmp', 'pelunasan tempo', 'icon-arrow-right', 'menu-pbl-tempo', 'pembelian/lain/pembayaran_tempo', 108, 3, 1, 'SELECT COUNT(*) as total FROM pembelian_lain where is_tempo = 1 and persetujuan = 1 and is_lunas = 0', 0, 0),
(112, 'pbl-tmp-ac', 'otorisasi pelunasan', 'icon-arrow-right', 'menu-pbl-tempo-acc', 'pembelian/lain/pembayaran_tempo/persetujuan', 108, 4, 1, 'SELECT COUNT(*) as total FROM pembelian_lain_pembayaran where status_persetujuan = 0;', 0, 0),
(113, 'pbl-rkp', 'rekap transaksi', 'icon-arrow-right', 'menu-pbl-rkp', 'pembelian/lain/rekap', 108, 5, 1, '', 0, 0),
(114, 'rtp', 'retur penjualan', 'fa fa-undo', 'sub-retur-penjualan', '', 0, 32, 1, '', 0, 0),
(115, 'rtp-kll', 'kelola retur', 'icon-arrow-right', 'menu-rtp-kll', 'manajemen/retur/kelola', 114, 1, 1, '', 0, 0),
(116, 'rtp-acc', 'otorisasi retur', 'icon-arrow-right', 'menu-rtp-acc', 'manajemen/retur/persetujuan', 114, 2, 1, 'SELECT COUNT(*) as total FROM `penjualan_retur` WHERE status = 1 AND persetujuan = 0', 0, 0),
(117, 'rtp-rkp', 'rekap retur', 'icon-arrow-right', 'menu-rtp-rkp', 'manajemen/retur/rekap', 114, 5, 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `merk_beras`
--

CREATE TABLE `merk_beras` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merk_beras`
--

INSERT INTO `merk_beras` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Manna', 1, 0),
(2, 'Putra BK', 1, 0),
(3, 'Timor BK', 1, 0),
(4, 'Berkat', 1, 0),
(5, 'Bakung', 1, 0),
(6, 'Bless', 1, 0),
(7, 'Kukuruyuk', 1, 0),
(8, 'Murah Meriah', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `merk_beras_harga`
--

CREATE TABLE `merk_beras_harga` (
  `id` int(11) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `harga` int(11) NOT NULL,
  `timestamp_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merk_beras_harga`
--

INSERT INTO `merk_beras_harga` (`id`, `id_kemasan`, `tanggal`, `harga`, `timestamp_update`, `update_by`) VALUES
(1, 2, '2020-06-22', 11000, '2020-06-22 11:06:13', 1),
(2, 1, '2020-06-22', 11500, '2020-06-22 17:31:34', 1),
(3, 1, '2020-06-22', 11300, '2020-06-22 17:34:02', 1),
(4, 3, '2020-06-22', 11500, '2020-06-22 17:42:15', 1),
(5, 4, '2020-07-01', 9300, '2020-07-01 14:02:28', 1),
(6, 5, '2020-07-01', 9800, '2020-07-01 14:02:51', 1),
(7, 6, '2020-07-01', 8600, '2020-07-01 14:03:06', 1),
(8, 7, '2020-07-01', 9100, '2020-07-01 14:03:19', 1),
(9, 8, '2020-07-01', 8100, '2020-07-01 14:03:32', 1),
(10, 9, '2020-07-01', 10200, '2020-07-01 14:03:47', 1),
(11, 10, '2020-07-01', 10000, '2020-07-01 14:04:09', 1),
(12, 2, '2020-07-01', 11100, '2020-07-01 14:17:11', 1),
(13, 3, '2020-07-01', 11800, '2020-07-01 14:18:57', 1),
(14, 11, '2020-07-02', 9500, '2020-07-08 15:40:19', 1),
(15, 12, '2020-07-07', 9400, '2020-07-08 17:16:37', 1),
(16, 13, '2020-07-03', 8300, '2020-07-08 17:19:31', 1),
(17, 14, '2020-07-09', 10200, '2020-07-09 12:18:28', 1),
(18, 2, '2020-07-09', 11300, '2020-07-09 12:18:59', 1),
(19, 10, '2021-09-15', 10100, '2021-09-15 15:13:58', 1),
(20, 15, '2021-09-15', 9100, '2021-09-15 15:16:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `merk_beras_kemasan`
--

CREATE TABLE `merk_beras_kemasan` (
  `id` int(11) NOT NULL,
  `id_merk` int(11) NOT NULL,
  `kemasan` tinyint(4) NOT NULL,
  `harga` int(11) NOT NULL,
  `last_update` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merk_beras_kemasan`
--

INSERT INTO `merk_beras_kemasan` (`id`, `id_merk`, `kemasan`, `harga`, `last_update`, `status`, `timestamp_update`) VALUES
(1, 1, 10, 11300, '2020-06-22', 1, '0000-00-00 00:00:00'),
(2, 1, 25, 11300, '2020-07-09', 1, '2020-07-09 12:18:59'),
(3, 1, 5, 11800, '2020-07-01', 1, '2020-07-01 14:18:57'),
(4, 5, 25, 9300, '2020-07-01', 1, '2020-07-01 14:02:28'),
(5, 4, 25, 9800, '2020-07-01', 1, '2020-07-01 14:02:51'),
(6, 6, 25, 8600, '2020-07-01', 1, '2020-07-01 14:03:06'),
(7, 7, 25, 9100, '2020-07-01', 1, '2020-07-01 14:03:19'),
(8, 8, 25, 8100, '2020-07-01', 1, '2020-07-01 14:03:32'),
(9, 2, 25, 10200, '2020-07-01', 1, '2020-07-01 14:03:47'),
(10, 3, 25, 10100, '2021-09-15', 1, '2021-09-15 15:13:58'),
(11, 5, 5, 9500, '2020-07-02', 1, '2020-07-08 15:40:19'),
(12, 5, 10, 9400, '2020-07-07', 1, '2020-07-08 17:16:37'),
(13, 8, 5, 8300, '2020-07-03', 1, '2020-07-08 17:19:31'),
(14, 4, 5, 10200, '2020-07-09', 1, '2020-07-09 12:18:28'),
(15, 3, 5, 9100, '2021-09-15', 1, '2021-09-15 15:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah`
--

CREATE TABLE `pembelian_gabah` (
  `id` int(11) NOT NULL,
  `kode_pembelian` varchar(100) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `resi_pengiriman` varchar(100) NOT NULL,
  `plat_kendaraan` varchar(10) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `berat_bruto` float NOT NULL,
  `berat_kosong` float NOT NULL,
  `berat_muatan` float NOT NULL,
  `berat_netto` float NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') DEFAULT NULL,
  `is_tempo` tinyint(1) NOT NULL,
  `is_tempo_dibayar` tinyint(1) NOT NULL,
  `tgl_tempo` date NOT NULL,
  `id_kas` int(11) NOT NULL,
  `id_rek_pengirim` int(11) NOT NULL,
  `id_rek_penerima` int(11) NOT NULL,
  `bayar_dp` int(11) NOT NULL,
  `potong_piutang` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `status_muatan` tinyint(1) NOT NULL,
  `status_tes` tinyint(1) NOT NULL,
  `status_selesai` tinyint(1) NOT NULL,
  `status_persetujuan` tinyint(1) NOT NULL,
  `dibayar_oleh` int(11) NOT NULL,
  `checker` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `diajukan_oleh` int(11) NOT NULL,
  `verifikasi_oleh` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `timestamp_input` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `timestamp_pengajuan` datetime NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `timestamp_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah`
--

INSERT INTO `pembelian_gabah` (`id`, `kode_pembelian`, `id_supplier`, `resi_pengiriman`, `plat_kendaraan`, `tanggal_pembelian`, `berat_bruto`, `berat_kosong`, `berat_muatan`, `berat_netto`, `jenis_pembayaran`, `is_tempo`, `is_tempo_dibayar`, `tgl_tempo`, `id_kas`, `id_rek_pengirim`, `id_rek_penerima`, `bayar_dp`, `potong_piutang`, `grandtotal`, `jumlah_bayar`, `status_muatan`, `status_tes`, `status_selesai`, `status_persetujuan`, `dibayar_oleh`, `checker`, `input_by`, `diajukan_oleh`, `verifikasi_oleh`, `tgl_bayar`, `timestamp_input`, `timestamp_pengajuan`, `timestamp_verifikasi`, `update_by`, `timestamp_update`) VALUES
(5, 'PB.005-191213.1', 5, '0', 'P8584Q', '2019-12-13', 13040, 0, 13040, 0, 'cash', 1, 1, '2021-07-17', 2, 0, 0, 0, 14000000, 34779120, 34779120, 1, 1, 1, 1, 2, 7, 1, 1, 1, '2021-07-08', '2021-07-08 08:47:15', '2021-07-08 15:40:35', '2021-07-08 15:42:27', 0, '0000-00-00 00:00:00'),
(8, 'PB.005-200106.1', 5, '0', 'P777BK', '2020-01-06', 10000, 0, 10000, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 0, 41886100, 41886100, 1, 1, 1, 1, 2, 1, 1, 1, 1, '2020-03-20', '2021-08-10 08:44:55', '2020-11-23 16:58:00', '2021-07-06 19:30:33', 0, '0000-00-00 00:00:00'),
(10, 'PB.008-200114.1', 8, 'RS191031-FVX', 'P 7777 BK', '2020-01-14', 11000, 0, 11000, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 0, 42203000, 0, 1, 1, 1, 1, 5, 5, 1, 1, 1, '2020-06-11', '2021-01-05 10:15:19', '2020-06-12 17:19:50', '2020-06-12 17:29:42', 0, '0000-00-00 00:00:00'),
(11, 'PB.005-200114.1', 5, 'RS191012-ZTP', 'P 3098 AB', '2020-01-14', 15910, 0, 15910, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 15000000, 95577700, 95577700, 1, 1, 1, 1, 2, 4, 1, 1, 1, '2020-11-21', '2021-08-12 05:22:43', '2020-11-19 19:29:44', '2020-11-21 17:24:07', 0, '0000-00-00 00:00:00'),
(12, 'PB.006-200610.1', 6, 'RS191008-EHF', 'P 3098 AB', '2020-06-10', 4500, 0, 4500, 0, 'cash', 0, 0, '0000-00-00', 1, 0, 0, 0, 0, 13868400, 0, 1, 1, 1, 1, 7, 2, 1, 1, 1, '2020-06-10', '2021-01-05 10:16:58', '2020-06-10 14:50:26', '2020-06-10 14:51:09', 0, '0000-00-00 00:00:00'),
(13, 'PBG-013-001/06/21', 13, '0', 'P8827UR', '2021-06-01', 2740, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 0, 8271000, 8271000, 1, 1, 1, 1, 2, 3, 1, 1, 1, '2021-08-12', '2021-08-12 05:23:57', '2021-08-12 12:23:39', '2021-08-12 12:23:57', 0, '0000-00-00 00:00:00'),
(15, 'PBG-014-001/07/03', 14, '0', 'P8760K', '2021-07-03', 2430, 0, 0, 0, 'transfer', 0, 0, '0000-00-00', 0, 4, 1, 0, 3127000, 3760000, 3760000, 1, 1, 1, 1, 2, 4, 1, 1, 1, '2021-07-03', '2021-07-07 18:31:26', '2021-07-07 17:23:02', '2021-07-08 01:31:26', 0, '0000-00-00 00:00:00'),
(19, 'PBG-015-001/07/27', 15, '0', 'Z8202f', '2021-07-27', 3090, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 7747000, 7747000, 7747000, 1, 1, 1, 1, 2, 4, 1, 1, 1, '2021-07-27', '2021-08-06 10:10:21', '2021-08-06 17:09:05', '2021-08-06 17:10:21', 0, '0000-00-00 00:00:00'),
(20, 'PBG-015-002/07/27', 15, '0', 'z8202fz', '2021-07-27', 3220, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 7253000, 8513000, 8513000, 1, 1, 1, 1, 5, 4, 1, 1, 1, '2021-08-09', '2021-08-10 09:34:13', '2021-08-09 16:08:45', '2021-08-09 16:22:37', 0, '0000-00-00 00:00:00'),
(21, 'PBG-004-001/08/10', 4, '0', 'P9175VS', '2021-08-10', 6310, 0, 0, 0, 'transfer', 0, 0, '2021-08-11', 0, 4, 0, 0, 5911000, 5911000, 5911000, 1, 1, 1, 1, 2, 8, 1, 1, 1, '2021-08-10', '2021-08-10 08:34:02', '2021-08-10 15:19:25', '2021-08-10 15:34:02', 0, '0000-00-00 00:00:00'),
(23, 'PBG-004-002/08/03', 4, '0', '8294', '2021-08-03', 5800, 0, 0, 0, 'cash', 1, 1, '2021-08-14', 2, 0, 0, 0, 0, 7312682, 7312682, 1, 1, 1, 1, 2, 4, 1, 1, 1, '2021-08-13', '2021-08-13 08:29:38', '2021-08-13 15:27:08', '2021-08-13 15:28:14', 0, '0000-00-00 00:00:00'),
(24, 'PBG-004-003/08/14', 4, '0', 'p9003hjk', '2021-08-14', 7800, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 0, 14524000, 14524000, 1, 1, 1, 1, 7, 5, 1, 1, 1, '2021-08-14', '2021-08-14 09:19:05', '2021-08-14 16:12:00', '2021-08-14 16:19:05', 0, '0000-00-00 00:00:00'),
(25, 'PBG-001-001/08/24', 1, '0', 'p9908g', '2021-08-24', 7080, 0, 0, 0, 'transfer', 0, 0, '0000-00-00', 0, 4, 0, 0, 9740000, 9740400, 9740400, 1, 1, 1, 1, 5, 6, 1, 1, 1, '2021-08-26', '2021-08-27 07:58:51', '2021-08-26 14:33:52', '2021-08-26 14:34:45', 0, '0000-00-00 00:00:00'),
(26, 'PBG-015-001/08/31', 15, '0', 'p8882pk', '2021-08-31', 5444, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 333333, 888126, 888126, 1, 1, 1, 1, 2, 6, 1, 1, 1, '2021-08-31', '2021-08-31 05:27:04', '2021-08-31 11:56:42', '2021-08-31 12:27:04', 0, '0000-00-00 00:00:00'),
(27, 'PBG-004-001/09/01', 4, '0', 'p4434kg', '2021-09-01', 7800, 0, 0, 0, 'transfer', 0, 0, '0000-00-00', 0, 4, 0, 0, 7567000, 11135000, 11135000, 1, 1, 1, 1, 5, 7, 1, 1, 1, '2021-09-02', '2021-09-02 02:48:46', '2021-09-02 09:46:45', '2021-09-02 09:48:46', 0, '0000-00-00 00:00:00'),
(28, 'PBG-006-001/09/11', 6, 'RS210911-RPH', 'P999BK', '2021-09-11', 5424, 0, 0, 0, NULL, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 9400000, 9400000, 1, 1, 1, 1, 7, 4, 1, 1, 1, '2021-09-13', '2021-09-14 08:18:07', '2021-09-11 17:12:40', '2021-09-14 15:18:07', 0, '0000-00-00 00:00:00'),
(29, 'PBG-015-001/09/13', 15, '0', 'P3431zs', '2021-09-13', 5466, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 7187222, 7718500, 7718500, 1, 1, 1, 1, 7, 4, 1, 1, 1, '2021-09-13', '2021-09-13 07:39:23', '2021-09-13 14:31:28', '2021-09-13 14:39:23', 0, '0000-00-00 00:00:00'),
(30, 'BG-IVN-202109.001', 4, '0', 'p3321ws', '2021-09-27', 4500, 0, 0, 0, NULL, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '0000-00-00', '2021-09-27 07:51:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(33, 'BG-ADM-202109.002', 15, 'RS210925-4HR', 'P999BK', '2021-09-27', 4500, 0, 0, 0, NULL, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '0000-00-00', '2021-09-27 08:12:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_batch`
--

CREATE TABLE `pembelian_gabah_batch` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `batch_label` varchar(100) NOT NULL,
  `berat_kotor` float NOT NULL,
  `berat_kosong` float NOT NULL,
  `potongan_timbangan` int(11) NOT NULL,
  `berat_netto` float NOT NULL,
  `berat_timbang_kecil` float NOT NULL,
  `is_manual` tinyint(4) NOT NULL,
  `sap_manual` text NOT NULL,
  `setor_sak` int(11) NOT NULL,
  `id_jenis_sak` int(11) NOT NULL,
  `is_record_supplier` tinyint(1) NOT NULL,
  `is_record_primary` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_batch`
--

INSERT INTO `pembelian_gabah_batch` (`id`, `id_pembelian`, `batch`, `batch_label`, `berat_kotor`, `berat_kosong`, `potongan_timbangan`, `berat_netto`, `berat_timbang_kecil`, `is_manual`, `sap_manual`, `setor_sak`, `id_jenis_sak`, `is_record_supplier`, `is_record_primary`, `input_by`) VALUES
(14, 5, 1, 'Seke 1', 13040, 9460, 0, 3580, 919, 0, '', 75, 2, 1, 1, 3),
(15, 5, 2, 'Seke 2', 9460, 5240, 0, 4220, 0, 0, '', 88, 2, 0, 1, 3),
(19, 6, 1, 'Timbang 1', 5000, 1500, 0, 3500, 0, 0, '', 0, 0, 0, 0, 1),
(20, 6, 2, 'Timbang Kecil', 1500, 0, 0, 125, 0, 1, '', 0, 0, 0, 0, 1),
(22, 6, 3, 'tessss', 1410, 0, 0, 0, 0, 1, '', 0, 0, 0, 0, 1),
(23, 8, 1, 'KS 64 1', 10000, 5000, 0, 5000, 0, 0, '', 0, 0, 0, 0, 1),
(24, 8, 2, 'KS 66', 5000, 3750, 0, 1250, 0, 0, '', 0, 0, 0, 0, 1),
(25, 8, 3, 'KS GH', 3750, 0, 0, 393, 0, 1, '', 0, 0, 0, 0, 1),
(28, 10, 1, 'timbang 1', 11000, 7960, 0, 3040, 0, 0, '', 0, 0, 0, 0, 1),
(29, 10, 2, '2', 7960, 3860, 0, 4100, 0, 0, '', 0, 0, 0, 0, 1),
(30, 11, 1, 'timbang 1 ks64 jemur', 15910, 11170, 0, 4740, 0, 0, '', 0, 0, 0, 0, 1),
(31, 11, 2, 'timbang 2 ks64 dryer', 11170, 4310, 0, 6860, 0, 0, '', 0, 0, 0, 0, 1),
(35, 12, 1, 'Timbang KS 64', 4500, 1500, 0, 3000, 0, 0, '', 0, 0, 0, 0, 1),
(36, 5, 1, 'Timbangan Kecil', 0, 0, 0, 919, 0, 1, '[\"5\",\"8\",\"6\"]', 0, 0, 0, 0, 1),
(37, 13, 1, 'timbang 1', 2740, 890, 0, 1850, 173, 0, '', 35, 0, 0, 0, 1),
(40, 0, 2, '', 5430, 0, 0, 0, 0, 1, '', 0, 0, 0, 0, 1),
(41, 15, 1, '1', 2430, 1250, 9, 1171, 0, 0, '', 20, 0, 0, 0, 1),
(45, 19, 1, 'KS IR64', 3090, 1100, 15, 1975, 0, 0, '', 29, 2, 0, 0, 1),
(46, 19, 1, 'Timbangan Kecil', 0, 0, 0, 0, 0, 1, '[]', 0, 0, 0, 0, 1),
(48, 20, 1, 'KS PW', 3220, 2400, 7, 813, 0, 0, '', 11, 0, 0, 0, 1),
(49, 20, 2, 'KS 64', 2400, 1090, 10, 1300, 0, 0, '', 20, 0, 0, 0, 1),
(50, 0, 1, 'KS 64', 6310, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 1),
(51, 21, 1, 'KS 64', 6310, 4790, 12, 1508, 0, 0, '', 33, 0, 0, 0, 1),
(52, 21, 2, 'KS 64', 4790, 2810, 15, 1965, 0, 0, '', 0, 0, 0, 0, 1),
(54, 13, 1, 'Timbangan Kecil', 0, 0, 0, 173, 0, 1, '[]', 0, 0, 0, 0, 1),
(58, 0, 1, 'KS LGW', 5800, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 1),
(59, 23, 1, 'KS LGW', 5800, 3650, 172, 1978, 0, 0, '', 32, 0, 0, 0, 1),
(60, 0, 1, 'KS LGW', 7800, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 1),
(61, 24, 1, 'KS LGW', 7800, 4100, 32, 3668, 335, 0, '', 2, 0, 0, 0, 1),
(62, 24, 1, 'Timbangan Kecil', 0, 0, 0, 335, 0, 1, '[]', 0, 0, 0, 0, 1),
(63, 25, 1, 'KS 46', 7080, 3000, 21, 4059, 0, 0, '', 20, 0, 0, 0, 1),
(64, 26, 1, 'KYP', 5444, 2450, 29, 2965, 0, 0, '', 56, 0, 0, 0, 1),
(65, 27, 1, 'LBL', 7800, 4000, 29, 3771, 0, 0, '', 23, 0, 0, 0, 1),
(66, 28, 1, 'GBH', 5424, 3000, 29, 2395, 0, 0, '', 26, 0, 0, 0, 1),
(67, 29, 1, 'GBH', 5466, 3010, 29, 2427, 125, 0, '', 33, 0, 0, 0, 1),
(68, 29, 1, 'Timbangan Kecil', 0, 0, 0, 125, 0, 1, '[]', 0, 0, 0, 0, 1),
(69, 28, 2, '2', 3000, 1000, 0, 2000, 0, 0, '', 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_detail`
--

CREATE TABLE `pembelian_gabah_detail` (
  `id` int(11) NOT NULL,
  `sap` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `kadar_air` int(11) NOT NULL,
  `jumlah_sak` int(11) NOT NULL,
  `berat_basah` float NOT NULL,
  `berat_kering` float NOT NULL,
  `is_default` varchar(10) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_detail`
--

INSERT INTO `pembelian_gabah_detail` (`id`, `sap`, `id_jenis`, `kadar_air`, `jumlah_sak`, `berat_basah`, `berat_kering`, `is_default`, `input_by`, `timestamp`) VALUES
(1, 1, 1, 6, 4, 0, 0, '0', 1, '2019-11-21 17:00:38'),
(2, 1, 1, 1, 11, 0, 0, '1', 1, '2019-11-21 17:00:38'),
(40, 2, 18, 11, 4, 0, 0, '1', 3, '2019-12-13 14:44:48'),
(41, 2, 18, 7, 5, 0, 0, '0', 3, '2019-12-13 14:44:48'),
(42, 1, 18, 11, 1, 0, 0, '0', 3, '2019-12-13 14:50:16'),
(43, 1, 18, 3, 15, 0, 0, '1', 3, '2019-12-13 14:50:16'),
(44, 3, 18, 11, 16, 0, 0, '1', 3, '2019-12-13 14:50:38'),
(45, 4, 18, 11, 16, 0, 0, '1', 3, '2019-12-13 14:50:51'),
(46, 5, 18, 11, 18, 0, 0, '1', 3, '2019-12-13 14:51:05'),
(48, 1, 20, 4, 16, 0, 0, '1', 3, '2019-12-13 14:56:55'),
(49, 2, 20, 11, 10, 0, 0, '1', 3, '2019-12-13 14:59:08'),
(50, 2, 20, 4, 1, 0, 0, '0', 3, '2019-12-13 14:59:08'),
(51, 3, 20, 11, 21, 0, 0, '1', 3, '2019-12-13 14:59:30'),
(52, 4, 20, 11, 20, 0, 0, '1', 3, '2019-12-13 14:59:57'),
(53, 5, 20, 11, 20, 0, 0, '1', 3, '2019-12-13 15:00:16'),
(110, 2, 63, 11, 6, 0, 0, '0', 1, '2019-12-23 13:34:22'),
(111, 2, 64, 2, 5, 0, 0, '0', 1, '2019-12-23 13:34:22'),
(112, 2, 63, 2, 5, 0, 0, '0', 1, '2019-12-23 13:34:22'),
(113, 2, 65, 11, 3, 0, 0, '0', 1, '2019-12-23 13:34:22'),
(114, 2, 66, 11, 11, 0, 0, '1', 1, '2019-12-23 13:34:22'),
(115, 3, 67, 2, 11, 0, 0, '1', 1, '2019-12-23 13:35:16'),
(116, 3, 67, 5, 4, 0, 0, '0', 1, '2019-12-23 13:35:16'),
(119, 1, 69, 11, 24, 0, 0, '1', 1, '2019-12-24 13:38:13'),
(120, 1, 69, 3, 6, 0, 0, '0', 1, '2019-12-24 13:38:13'),
(121, 1, 69, 5, 5, 0, 0, '0', 1, '2019-12-24 13:38:13'),
(122, 2, 69, 11, 29, 0, 0, '1', 1, '2019-12-24 13:39:01'),
(123, 2, 69, 2, 6, 0, 0, '0', 1, '2019-12-24 13:39:01'),
(124, 2, 69, 6, 5, 0, 0, '0', 1, '2019-12-24 13:39:01'),
(128, 1, 73, 11, 4, 0, 0, '0', 1, '2020-01-06 10:20:23'),
(129, 1, 73, 2, 26, 0, 0, '1', 1, '2020-01-06 10:20:23'),
(130, 2, 73, 2, 7, 0, 0, '0', 1, '2020-01-06 10:21:10'),
(131, 2, 73, 4, 5, 0, 0, '0', 1, '2020-01-06 10:21:10'),
(132, 2, 73, 11, 18, 0, 0, '1', 1, '2020-01-06 10:21:10'),
(133, 3, 73, 11, 14, 0, 0, '1', 1, '2020-01-06 10:23:00'),
(134, 3, 73, 9, 3, 0, 0, '0', 1, '2020-01-06 10:23:00'),
(135, 3, 73, 2, 8, 0, 0, '0', 1, '2020-01-06 10:23:00'),
(136, 4, 74, 11, 3, 0, 0, '1', 1, '2020-01-06 10:27:14'),
(137, 4, 74, 3, 1, 0, 0, '0', 1, '2020-01-06 10:27:14'),
(138, 4, 74, 2, 1, 0, 0, '0', 1, '2020-01-06 10:27:14'),
(139, 5, 74, 11, 16, 0, 0, '1', 1, '2020-01-06 10:29:41'),
(140, 5, 74, 3, 4, 0, 0, '0', 1, '2020-01-06 10:29:41'),
(242, 1, 100, 1, 1, 23, 0, '0', 1, '2020-01-09 14:52:38'),
(243, 1, 100, 1, 1, 40, 0, '0', 1, '2020-01-09 14:52:38'),
(244, 1, 100, 1, 1, 12, 0, '0', 1, '2020-01-09 14:52:38'),
(245, 1, 100, 1, 1, 30, 0, '0', 1, '2020-01-09 14:52:38'),
(246, 1, 100, 1, 1, 44, 0, '0', 1, '2020-01-09 14:52:38'),
(247, 1, 100, 1, 1, 32, 0, '0', 1, '2020-01-09 14:52:38'),
(248, 1, 100, 1, 1, 33, 0, '0', 1, '2020-01-09 14:52:39'),
(249, 1, 100, 1, 1, 44, 0, '0', 1, '2020-01-09 14:52:39'),
(250, 1, 100, 1, 1, 47, 0, '0', 1, '2020-01-09 14:52:39'),
(251, 1, 100, 5, 1, 9, 0, '0', 1, '2020-01-09 14:52:39'),
(252, 1, 100, 5, 1, 12, 0, '0', 1, '2020-01-09 14:52:39'),
(253, 1, 100, 5, 1, 11, 0, '0', 1, '2020-01-09 14:52:39'),
(254, 1, 101, 1, 1, 33, 0, '0', 1, '2020-01-09 14:52:39'),
(255, 1, 101, 1, 1, 23, 0, '0', 1, '2020-01-09 14:52:39'),
(266, 1, 104, 11, 40, 0, 0, '1', 1, '2020-01-15 11:29:21'),
(267, 2, 105, 11, 10, 0, 0, '1', 1, '2020-01-15 11:31:21'),
(268, 3, 105, 11, 10, 0, 0, '1', 1, '2020-01-15 11:31:44'),
(269, 4, 105, 11, 10, 0, 0, '1', 1, '2020-01-15 11:32:11'),
(270, 5, 105, 11, 10, 0, 0, '1', 1, '2020-01-15 11:32:26'),
(271, 6, 105, 11, 14, 0, 0, '1', 1, '2020-01-15 11:32:40'),
(272, 1, 106, 3, 5, 0, 0, '0', 1, '2020-01-15 11:42:22'),
(273, 1, 106, 11, 12, 0, 0, '1', 1, '2020-01-15 11:42:22'),
(274, 2, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:44:28'),
(275, 3, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:44:43'),
(276, 4, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:44:51'),
(277, 5, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:44:58'),
(278, 6, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:45:07'),
(279, 7, 106, 11, 15, 0, 0, '1', 1, '2020-01-15 11:45:15'),
(280, 1, 107, 11, 11, 0, 0, '1', 1, '2020-01-15 11:48:13'),
(281, 1, 107, 3, 5, 0, 0, '0', 1, '2020-01-15 11:48:13'),
(282, 2, 107, 11, 5, 0, 0, '1', 1, '2020-01-15 11:48:35'),
(283, 2, 107, 3, 2, 0, 0, '0', 1, '2020-01-15 11:48:35'),
(284, 3, 107, 11, 21, 0, 0, '1', 1, '2020-01-15 11:48:47'),
(285, 4, 107, 11, 19, 0, 0, '1', 1, '2020-01-15 11:48:56'),
(286, 5, 107, 11, 18, 0, 0, '1', 1, '2020-01-15 11:49:07'),
(287, 6, 107, 11, 21, 0, 0, '1', 1, '2020-01-15 11:49:18'),
(288, 7, 107, 11, 20, 0, 0, '1', 1, '2020-01-15 11:49:30'),
(289, 8, 107, 11, 21, 0, 0, '1', 1, '2020-01-15 11:49:49'),
(290, 1, 108, 11, 1, 10, 0, '0', 1, '2020-02-10 14:18:27'),
(291, 1, 108, 11, 1, 5, 0, '0', 1, '2020-02-10 14:18:27'),
(292, 1, 108, 11, 1, 20, 0, '0', 1, '2020-02-10 14:18:28'),
(293, 1, 109, 1, 1, 35, 0, '0', 1, '2020-02-10 14:18:28'),
(294, 1, 110, 2, 1, 55, 0, '0', 1, '2020-02-10 14:18:28'),
(350, 1, 134, 10, 1, 0, 0, '0', 1, '2020-02-15 11:27:09'),
(351, 1, 135, 5, 1, 0, 0, '0', 1, '2020-02-15 11:27:28'),
(352, 1, 136, 2, 1, 350, 0, '0', 1, '2020-04-18 14:46:12'),
(353, 1, 136, 2, 1, 140, 0, '0', 1, '2020-04-18 14:46:12'),
(354, 1, 136, 3, 1, 101, 0, '0', 1, '2020-04-18 14:46:12'),
(355, 1, 136, 3, 1, 220, 0, '0', 1, '2020-04-18 14:46:12'),
(356, 1, 136, 5, 1, 170, 0, '0', 1, '2020-04-18 14:46:13'),
(357, 1, 136, 5, 1, 200, 0, '0', 1, '2020-04-18 14:46:13'),
(358, 1, 137, 11, 40, 0, 0, '1', 1, '2020-06-10 14:35:06'),
(359, 2, 137, 2, 10, 0, 0, '1', 1, '2020-06-10 14:36:12'),
(451, 1, 168, 1, 1, 111, 0, '0', 1, '2021-05-01 13:58:35'),
(452, 1, 168, 1, 1, 221, 0, '0', 1, '2021-05-01 13:58:35'),
(453, 1, 168, 3, 1, 233, 0, '0', 1, '2021-05-01 13:58:35'),
(454, 1, 168, 3, 1, 244, 0, '0', 1, '2021-05-01 13:58:35'),
(455, 1, 169, 10, 1, 50, 0, '0', 1, '2021-05-01 13:58:36'),
(456, 1, 169, 10, 1, 60, 0, '0', 1, '2021-05-01 13:58:36'),
(459, 1, 171, 11, 25, 0, 0, '1', 1, '2021-06-04 16:22:15'),
(460, 1, 171, 5, 10, 0, 0, '0', 1, '2021-06-04 16:22:15'),
(463, 1, 173, 4, 1, 76, 0, '0', 1, '2021-06-07 16:25:03'),
(464, 1, 173, 4, 1, 73, 0, '0', 1, '2021-06-07 16:25:03'),
(465, 1, 173, 4, 1, 70, 0, '0', 1, '2021-06-07 16:25:03'),
(466, 1, 173, 4, 1, 71, 0, '0', 1, '2021-06-07 16:25:03'),
(467, 1, 173, 4, 1, 66, 0, '0', 1, '2021-06-07 16:25:04'),
(468, 1, 173, 4, 1, 41, 0, '0', 1, '2021-06-07 16:25:04'),
(469, 1, 173, 4, 1, 37, 0, '0', 1, '2021-06-07 16:25:04'),
(470, 1, 173, 4, 1, 60, 0, '0', 1, '2021-06-07 16:25:04'),
(471, 1, 173, 4, 1, 78, 0, '0', 1, '2021-06-07 16:25:04'),
(472, 1, 173, 4, 1, 38, 0, '0', 1, '2021-06-07 16:25:04'),
(477, 1, 176, 11, 20, 0, 0, '1', 1, '2021-07-07 17:21:34'),
(478, 1, 177, 11, 29, 0, 0, '1', 1, '2021-08-06 15:19:27'),
(494, 1, 186, 1, 6, 0, 0, '0', 1, '2021-08-06 16:36:23'),
(495, 1, 186, 6, 49, 0, 0, '1', 1, '2021-08-06 16:36:23'),
(496, 2, 187, 11, 10, 0, 0, '1', 1, '2021-08-06 16:36:32'),
(498, 2, 186, 11, 22, 0, 0, '1', 1, '2021-08-06 16:45:06'),
(505, 1, 190, 8, 1, 50, 0, '0', 1, '2021-08-07 15:54:31'),
(506, 1, 190, 8, 1, 55, 0, '0', 1, '2021-08-07 15:54:31'),
(507, 1, 190, 8, 1, 13, 0, '0', 1, '2021-08-07 15:54:31'),
(509, 1, 192, 11, 11, 0, 0, '1', 1, '2021-08-09 15:54:50'),
(510, 2, 193, 11, 20, 0, 0, '1', 1, '2021-08-09 16:00:05'),
(513, 1, 195, 1, 15, 0, 0, '1', 1, '2021-08-10 15:07:13'),
(514, 2, 195, 1, 18, 0, 0, '1', 1, '2021-08-10 15:08:21'),
(515, 1, 196, 2, 20, 0, 0, '1', 1, '2021-08-11 15:04:59'),
(516, 2, 196, 4, 21, 0, 0, '1', 1, '2021-08-11 15:05:22'),
(531, 1, 206, 11, 1, 68, 0, '0', 1, '2021-08-13 14:53:05'),
(532, 1, 206, 11, 1, 87, 0, '0', 1, '2021-08-13 14:53:05'),
(533, 1, 207, 11, 15, 0, 0, '1', 1, '2021-08-13 15:17:14'),
(534, 2, 207, 11, 15, 0, 0, '1', 1, '2021-08-13 15:17:31'),
(535, 3, 208, 11, 1, 0, 0, '1', 1, '2021-08-13 15:18:04'),
(536, 4, 208, 11, 1, 0, 0, '0', 1, '2021-08-13 15:18:21'),
(541, 1, 213, 11, 2, 0, 0, '0', 1, '2021-08-14 15:09:51'),
(542, 1, 214, 11, 1, 223, 0, '0', 1, '2021-08-14 15:36:55'),
(543, 1, 214, 11, 1, 112, 0, '0', 1, '2021-08-14 15:36:55'),
(544, 1, 215, 1, 20, 0, 0, '1', 1, '2021-08-24 16:08:24'),
(545, 1, 216, 1, 34, 0, 0, '1', 1, '2021-08-31 11:53:36'),
(546, 2, 217, 1, 22, 0, 0, '1', 1, '2021-08-31 11:53:50'),
(547, 1, 218, 1, 23, 0, 0, '1', 1, '2021-09-01 16:40:56'),
(548, 1, 219, 1, 26, 0, 0, '1', 1, '2021-09-11 16:59:24'),
(549, 1, 220, 11, 21, 0, 0, '1', 1, '2021-09-13 10:32:09'),
(550, 2, 221, 1, 12, 0, 0, '1', 1, '2021-09-13 10:32:30'),
(551, 1, 222, 11, 1, 60, 0, '0', 1, '2021-09-13 10:52:38'),
(552, 1, 222, 11, 1, 65, 0, '0', 1, '2021-09-13 10:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_jenis`
--

CREATE TABLE `pembelian_gabah_jenis` (
  `id` int(11) NOT NULL,
  `id_batch` int(11) NOT NULL,
  `jenis_gabah` varchar(100) NOT NULL,
  `id_jenis_padi` int(11) NOT NULL,
  `pengeringan` enum('jemur','dryer','') NOT NULL,
  `proses` enum('tapel','giling','jemur','') NOT NULL,
  `potong_sak` float NOT NULL DEFAULT 0.5,
  `potong_lain` float NOT NULL,
  `hampa` float NOT NULL,
  `persen_broken` float NOT NULL,
  `berat_beras_bruto` float NOT NULL,
  `berat_beras_netto` float NOT NULL,
  `harga_nol_gabuk` int(11) NOT NULL,
  `harga_kepala` int(11) NOT NULL,
  `harga_broken` int(11) NOT NULL,
  `rendemen_kg` decimal(10,0) NOT NULL,
  `harga_beras_medium` int(11) NOT NULL,
  `harga_kg` int(11) NOT NULL,
  `tambahan` int(11) NOT NULL COMMENT 'bonus atau potongan',
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_jenis`
--

INSERT INTO `pembelian_gabah_jenis` (`id`, `id_batch`, `jenis_gabah`, `id_jenis_padi`, `pengeringan`, `proses`, `potong_sak`, `potong_lain`, `hampa`, `persen_broken`, `berat_beras_bruto`, `berat_beras_netto`, `harga_nol_gabuk`, `harga_kepala`, `harga_broken`, `rendemen_kg`, `harga_beras_medium`, `harga_kg`, `tambahan`, `subtotal`) VALUES
(18, 14, 'ks', 1, 'jemur', '', 0.5, 36, 14, 12, 2661, 2201.4, 5400, 0, 0, '0', 0, 0, 0, 11887560),
(20, 15, 'ks', 1, 'dryer', '', 0.6, 42, 18, 12, 4220, 3356.3, 5300, 0, 0, '0', 0, 0, 0, 17788390),
(69, 19, 'ks', 1, 'jemur', '', 0.5, 0, 15, 0, 3500, 2909.4, 5600, 0, 0, '0', 0, 0, -880, 16291760),
(73, 23, 'ks', 1, 'dryer', '', 0.5, 0, 15, 0, 5000, 4149, 7600, 0, 0, '0', 0, 0, 0, 31532400),
(74, 24, 'ks', 2, 'jemur', '', 0.5, 0, 15, 0, 1250, 1045, 7500, 0, 0, '0', 0, 0, 0, 7837500),
(75, 25, 'ks', 4, 'jemur', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(76, 25, 'ks', 4, 'jemur', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(77, 25, 'ks', 4, 'jemur', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(78, 25, 'ks', 4, 'dryer', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(79, 25, 'ks', 4, 'dryer', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(100, 25, 'ks', 4, 'jemur', '', 0.5, 0, 11, 0, 337, 288, 7400, 0, 0, '0', 0, 0, 0, 2131200),
(101, 25, 'kg', 1, '', '', 0.5, 0, 0, 0, 56, 55, 0, 0, 0, '0', 0, 7000, 0, 385000),
(104, 28, 'ks', 1, 'jemur', '', 0.5, 0, 15, 0, 3040, 2567, 7000, 0, 0, '0', 0, 0, 0, 17969000),
(105, 29, 'ks', 1, 'dryer', '', 0.5, 0, 15, 0, 4100, 3462, 7000, 0, 0, '0', 0, 0, 0, 24234000),
(106, 30, 'ks', 1, 'jemur', '', 0.5, 0, 0, 0, 4740, 4680, 8400, 0, 0, '0', 0, 0, 0, 39312000),
(107, 31, 'ks', 1, 'dryer', '', 0.5, 0, 0, 0, 6860, 6779, 8300, 0, 0, '0', 0, 0, 0, 56265700),
(108, 20, 'ks', 1, 'jemur', '', 0, 0, 0, 0, 100, 75, 5700, 0, 0, '0', 0, 0, -500, 427000),
(109, 20, 'ks', 1, 'dryer', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(110, 20, 'kg', 1, '', '', 0, 0, 0, 0, 25, 24.5, 0, 0, 0, '0', 0, 6700, -150, 164000),
(134, 16, 'ks', 1, 'jemur', '', 0, 0, 2, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(135, 17, 'ks', 1, 'jemur', '', 0, 0, 2, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(137, 35, 'ks', 1, 'dryer', '', 0.5, 0, 10, 0, 3000, 2667, 5200, 0, 0, '0', 0, 0, 0, 13868400),
(168, 36, 'ks', 1, 'dryer', '', 0, 0, 0, 0, 612.6, 600.2, 5450, 0, 0, '0', 0, 0, 0, 3271090),
(169, 36, 'ks', 2, 'dryer', '', 0, 0, 0, 0, 306.3, 275.6, 5500, 0, 0, '0', 0, 0, 0, 1515800),
(171, 37, 'ks', 5, 'dryer', '', 0.5, 10, 7, 0, 1677, 1512, 4950, 0, 0, '0', 0, 0, 2025, 7486425),
(176, 41, 'ks', 6, 'dryer', '', 0.5, 0, 21, 0, 1171, 917.1, 4100, 0, 0, '0', 0, 0, -110, 3760000),
(177, 45, 'ks', 1, 'dryer', '', 0.5, 0, 15, 0, 1975, 1666.4, 4650, 0, 0, '0', 0, 0, -1760, 7747000),
(190, 47, 'ks', 3, 'jemur', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(192, 48, 'ks', 7, 'dryer', '', 0.5, 0, 10, 0, 813, 726.7, 4700, 0, 0, '0', 0, 0, -490, 3415000),
(193, 49, 'ks', 1, 'dryer', '', 0.5, 0, 15, 0, 1300, 1096.5, 4650, 0, 0, '0', 0, 0, -725, 5098000),
(195, 51, 'ks', 1, 'dryer', '', 0.5, 0, 9, 0, 1508, 1343.6, 4400, 0, 0, '0', 0, 0, -840, 5911000),
(196, 52, 'ks', 1, 'dryer', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(197, 54, 'kg', 7, '', 'giling', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(206, 57, 'kg', 1, '', 'giling', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0),
(207, 59, 'ks', 6, 'dryer', '', 0.5, 0, 8, 0, 1854.3, 1692, 3956, 0, 0, '0', 0, 0, 0, 6693552),
(208, 59, 'kg', 1, '', 'giling', 0.5, 0, 0, 0, 123.6, 122.6, 0, 0, 0, '0', 0, 5050, 0, 619130),
(213, 61, 'kg', 1, '', 'giling', 0.5, 0, 9, 0, 3333, 3032.1, 0, 0, 0, '0', 0, 4000, 0, 12128400),
(214, 62, 'ks', 7, 'jemur', '', 0.5, 0, 8, 0, 335, 307.2, 3894, 0, 0, '0', 0, 0, -237, 1196000),
(215, 63, 'ks', 5, 'dryer', '', 0.5, 0, 19, 0, 4059, 3246.8, 3000, 0, 0, '0', 0, 0, 0, 9740400),
(216, 64, 'ks', 6, 'jemur', '', 0.5, 0, 12, 0, 1800.1, 1553.3, 433, 0, 0, '0', 0, 0, 0, 672579),
(217, 64, 'ks', 5, 'jemur', '', 0.5, 0, 19, 0, 1164.8, 925.1, 233, 0, 0, '0', 0, 0, 0, 215548),
(218, 65, 'ks', 7, 'jemur', '', 0.5, 0, 12, 0, 3771, 3275.2, 3400, 0, 0, '0', 0, 0, -680, 11135000),
(219, 66, 'ks', 6, 'jemur', '', 0.5, 10, 11, 0, 2395, 2089.8, 4500, 0, 0, '0', 0, 0, -4100, 9400000),
(220, 67, 'ks', 4, 'jemur', '', 0.5, 0, 3, 0, 1464.9, 1410.7, 3000, 0, 0, '0', 0, 0, -2100, 4230000),
(221, 67, 'ks', 7, 'dryer', '', 0.5, 0, 12, 0, 837, 723.8, 4000, 0, 0, '0', 0, 0, -200, 2895000),
(222, 68, 'kg', 7, '', 'giling', 0, 0, 5, 0, 125, 118.7, 0, 0, 0, '0', 0, 5000, 0, 593500);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_pembayaran`
--

CREATE TABLE `pembelian_gabah_pembayaran` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `id_rek_penerima` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `status_persetujuan` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_pembayaran`
--

INSERT INTO `pembelian_gabah_pembayaran` (`id`, `id_pembelian`, `tgl_pembayaran`, `jenis_pembayaran`, `id_kas`, `id_rek_penerima`, `jumlah_bayar`, `catatan`, `is_paid_off`, `status_persetujuan`, `input_by`, `timestamp_input`, `acc_by`, `timestamp_acc`) VALUES
(1, 1, '2020-09-24', 'transfer', 4, 0, 73975700, '', 1, 2, 1, '2021-08-10 16:37:24', 1, '2021-07-08 15:46:20'),
(3, 5, '2021-07-08', 'transfer', 4, 0, 34779120, 'bayar gabah tempo', 1, 1, 1, '2021-07-08 15:46:42', 1, '2021-07-08 15:47:15'),
(4, 1, '2021-08-10', 'transfer', 4, 0, 73975700, '', 0, 1, 1, '2021-08-10 16:38:42', 1, '2021-08-10 16:38:54'),
(5, 23, '2021-08-13', 'cash', 2, 0, 7312682, 'Lunas', 1, 1, 1, '2021-08-13 15:29:10', 1, '2021-08-13 15:29:38'),
(6, 1, '2021-09-02', 'transfer', 4, 0, 0, '', 1, 1, 1, '2021-09-02 09:49:31', 1, '2021-09-02 09:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_tes`
--

CREATE TABLE `pembelian_gabah_tes` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_batch` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `tanggal_tes` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `jenis_gabah` varchar(50) NOT NULL,
  `id_jenis_padi` int(11) NOT NULL,
  `hasil_akhir` double NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_tes`
--

INSERT INTO `pembelian_gabah_tes` (`id`, `id_pembelian`, `id_batch`, `batch`, `tanggal_tes`, `jam_mulai`, `jam_selesai`, `id_supplier`, `jenis_gabah`, `id_jenis_padi`, `hasil_akhir`, `petugas`, `is_deleted`, `input_by`, `timestamp`) VALUES
(1, 6, 19, 1, '2021-06-09', '00:00:00', '00:00:00', 3, 'ks', 1, 15, 'asdsa', 0, 1, '2021-06-09 11:08:42'),
(2, 6, 20, 2, '0000-00-00', '00:00:00', '00:00:00', 3, 'kg', 1, 0, '', 1, 1, '2021-06-09 11:08:42'),
(3, 6, 20, 2, '0000-00-00', '00:00:00', '00:00:00', 3, 'ks', 1, 0, '', 1, 1, '2021-06-09 11:08:42'),
(4, 15, 41, 1, '2021-07-03', '15:00:00', '15:30:00', 14, 'ks', 6, 21, 'steven', 0, 1, '2021-07-07 17:21:56'),
(5, 19, 45, 1, '2021-07-27', '15:00:00', '15:20:00', 15, 'ks', 1, 19, 'Steven, Rio', 0, 1, '2021-08-06 16:59:29'),
(6, 21, 51, 1, '2021-08-10', '07:05:00', '07:33:00', 4, 'ks', 1, 9, 'Steven, Rio', 0, 1, '2021-08-10 10:43:04'),
(7, 8, 23, 1, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 1, 0, '', 1, 1, '2021-08-10 15:44:55'),
(8, 8, 24, 2, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 2, 0, '', 1, 1, '2021-08-10 15:44:55'),
(9, 8, 25, 3, '0000-00-00', '00:00:00', '00:00:00', 5, 'kg', 1, 0, '', 1, 1, '2021-08-10 15:44:55'),
(10, 8, 25, 3, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 4, 0, '', 1, 1, '2021-08-10 15:44:55'),
(11, 20, 48, 1, '0000-00-00', '00:00:00', '00:00:00', 15, 'ks', 7, 0, '', 1, 1, '2021-08-10 16:34:13'),
(12, 20, 49, 2, '0000-00-00', '00:00:00', '00:00:00', 15, 'ks', 1, 0, '', 1, 1, '2021-08-10 16:34:13'),
(13, 11, 30, 1, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 1, 0, '', 1, 1, '2021-08-12 12:22:43'),
(14, 11, 31, 2, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 1, 0, '', 1, 1, '2021-08-12 12:22:43'),
(15, 14, 39, 1, '0000-00-00', '00:00:00', '00:00:00', 5, 'ks', 1, 0, '', 1, 1, '2021-08-12 12:22:47'),
(16, 13, 54, 1, '0000-00-00', '00:00:00', '00:00:00', 13, 'kg', 7, 0, '', 1, 1, '2021-08-12 12:22:51'),
(17, 13, 37, 1, '0000-00-00', '00:00:00', '00:00:00', 13, 'ks', 5, 0, '', 1, 1, '2021-08-12 12:22:51'),
(18, 23, 59, 1, '2021-08-03', '00:00:00', '00:00:00', 4, 'kg', 1, 0, 'Steven', 1, 1, '2021-08-13 15:20:44'),
(19, 23, 59, 1, '2021-08-03', '00:00:00', '00:00:00', 4, 'ks', 6, 8, 'Steven', 0, 1, '2021-08-13 15:20:44'),
(20, 24, 61, 1, '2021-08-14', '15:00:00', '16:00:00', 4, 'kg', 1, 9, 'Steven', 0, 1, '2021-08-14 15:41:52'),
(21, 24, 62, 1, '2021-08-14', '15:30:00', '16:00:00', 4, 'ks', 7, 8, 'Steven', 0, 1, '2021-08-14 15:41:52'),
(22, 25, 63, 1, '0000-00-00', '00:00:00', '00:00:00', 1, 'ks', 5, 0, '', 1, 1, '2021-08-27 14:58:51'),
(23, 26, 64, 1, '2021-08-31', '00:00:00', '00:00:00', 15, 'ks', 5, 19, 'Steven', 0, 1, '2021-08-31 11:54:21'),
(24, 26, 64, 1, '2021-08-31', '00:00:00', '00:00:00', 15, 'ks', 6, 12, 'Steven', 0, 1, '2021-08-31 11:54:21'),
(25, 27, 65, 1, '2021-09-01', '15:03:00', '15:31:00', 4, 'ks', 7, 12, 'Steven', 0, 1, '2021-09-01 16:41:48'),
(26, 28, 66, 1, '2021-09-11', '00:00:00', '00:00:00', 6, 'ks', 6, 11, 'Steven', 0, 1, '2021-09-11 17:10:15'),
(27, 29, 68, 1, '2021-09-13', '00:00:00', '00:00:00', 15, 'kg', 7, 5, 'Steven', 0, 1, '2021-09-13 11:00:00'),
(28, 29, 67, 1, '2021-09-13', '00:00:00', '00:00:00', 15, 'ks', 4, 3, 'Steven', 0, 1, '2021-09-13 11:00:00'),
(29, 29, 67, 1, '2021-09-13', '00:00:00', '00:00:00', 15, 'ks', 7, 12, 'Steven', 0, 1, '2021-09-13 11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_gabah_tes_detail`
--

CREATE TABLE `pembelian_gabah_tes_detail` (
  `id` int(11) NOT NULL,
  `id_tes` int(11) NOT NULL,
  `batch_tes` int(11) NOT NULL,
  `hasil_tes` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_gabah_tes_detail`
--

INSERT INTO `pembelian_gabah_tes_detail` (`id`, `id_tes`, `batch_tes`, `hasil_tes`) VALUES
(1, 5, 1, 12),
(2, 5, 2, 14),
(3, 20, 1, 5),
(4, 20, 2, 9),
(5, 20, 3, 4),
(6, 20, 4, 8),
(7, 20, 5, 13);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_lain`
--

CREATE TABLE `pembelian_lain` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `is_tempo` tinyint(4) NOT NULL,
  `is_tempo_dibayar` tinyint(4) NOT NULL,
  `tgl_tempo` date NOT NULL,
  `id_kas` int(11) NOT NULL,
  `potong_piutang` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `biaya_tambahan` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `terbilang` varchar(225) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `persetujuan` tinyint(4) NOT NULL,
  `is_lunas` tinyint(4) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_lain`
--

INSERT INTO `pembelian_lain` (`id`, `kode`, `tanggal`, `id_customer`, `catatan`, `jenis_pembayaran`, `is_tempo`, `is_tempo_dibayar`, `tgl_tempo`, `id_kas`, `potong_piutang`, `potongan`, `biaya_tambahan`, `grandtotal`, `terbilang`, `jumlah_bayar`, `tgl_bayar`, `status`, `persetujuan`, `is_lunas`, `input_by`, `timestamp_input`, `verif_by`, `timestamp_verif`) VALUES
(1, 'PBL-001/08/21', '2021-08-18', 3, 'Katul', 'cash', 1, 1, '2021-08-31', 4, 0, 0, 0, 120000, '', 0, '2021-08-21', 1, 1, 1, 1, '2021-08-18 14:44:16', 1, '2021-08-18 15:47:56'),
(2, 'PBL-002/08/21', '2021-08-19', 4, 'Katul', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 306000, '', 306000, '2021-08-19', 1, 1, 1, 1, '2021-08-19 09:57:03', 1, '2021-08-19 11:20:07'),
(3, 'PBL-003/08/21', '2021-08-19', 15, 'Katul', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 2700000, '', 2700000, '2021-08-19', 1, 1, 1, 1, '2021-08-19 15:00:43', 1, '2021-08-19 15:01:46'),
(4, 'PBL-004/08/21', '2021-08-19', 6, 'Beras', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 3000000, '', 3000000, '0000-00-00', 1, 1, 1, 1, '2021-08-19 15:09:43', 1, '2021-08-20 13:16:58'),
(5, 'PBL-005/08/21', '2021-08-19', 6, 'Jagung', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 600000, '', 600000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 13:22:04', 1, '2021-08-20 13:23:06'),
(6, 'PBL-006/08/21', '2021-08-20', 13, 'Katul', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 4000000, '', 4000000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 13:38:29', 1, '2021-08-20 13:46:51'),
(7, 'PBL-007/08/21', '2021-08-20', 6, 'S+', 'cash', 0, 0, '0000-00-00', 1, 0, 0, 0, 800000, '', 800000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 13:48:27', 1, '2021-08-20 14:03:23'),
(8, 'PBL-008/08/21', '2021-08-20', 4, 'Broken', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 2000000, '', 2000000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 14:07:22', 1, '2021-08-20 14:08:20'),
(9, 'PBL-009/08/21', '2021-08-20', 15, 'Jiwa', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 2000000, '', 2000000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 14:22:02', 1, '2021-08-20 14:24:10'),
(10, 'PBL-010/08/21', '2021-08-20', 3, 'lBWL', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 306000, '', 306000, '2021-08-20', 1, 1, 1, 1, '2021-08-20 14:27:41', 1, '2021-08-20 14:49:35'),
(11, 'PBL-011/08/21', '2021-08-18', 6, 'Label', 'transfer', 0, 0, '0000-00-00', 5, 0, 0, 0, 7843020, '', 7843020, '2021-08-19', 1, 1, 1, 1, '2021-08-20 14:51:12', 1, '2021-08-20 14:52:21'),
(12, 'PBL-012/08/21', '2021-08-20', 15, 'lbl', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 561600, '', 561600, '2021-08-20', 1, 1, 1, 1, '2021-08-20 14:59:34', 1, '2021-08-20 15:00:29'),
(13, 'PBL-013/08/21', '2021-08-20', 15, 'lbkk', 'tbd', 1, 1, '2021-08-21', 4, 0, 0, 0, 405000, '', 0, '2021-08-21', 1, 1, 1, 1, '2021-08-20 16:37:09', 1, '2021-08-20 16:37:54'),
(14, 'PBL-014/08/21', '2021-08-21', 15, 'Jagung', 'tbd', 1, 1, '2021-08-22', 2, 0, 0, 0, 3000000, '', 0, '2021-08-21', 1, 1, 1, 1, '2021-08-21 14:27:32', 1, '2021-08-21 14:30:43'),
(15, 'PBL-015/08/21', '2021-08-21', 15, 'nnn', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 11207000, '', 11207000, '2021-08-21', 1, 1, 1, 1, '2021-08-21 19:08:18', 1, '2021-08-21 19:17:03'),
(16, 'PBL-017/08/21', '2021-08-23', 4, 'PBH-KK', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 4550000, '', 4550000, '2021-08-23', 1, 1, 1, 1, '2021-08-23 15:59:41', 1, '2021-08-23 17:02:24'),
(17, 'PBL-018/08/21', '2021-08-23', 15, 'ddd', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 2360000, '', 2360000, '2021-08-23', 1, 1, 1, 1, '2021-08-23 17:06:06', 1, '2021-08-23 17:09:44'),
(18, 'PBL-019/08/21', '2021-08-23', 9, 'LPB', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 140000, '', 140000, '2021-08-23', 1, 1, 1, 1, '2021-08-23 17:14:10', 1, '2021-08-23 17:14:55'),
(19, 'PBL-021/08/21', '2021-08-24', 4, 'Lbl', 'cash', 0, 0, '0000-00-00', 2, 0, 1000, 3000, 1322000, '', 0, '2021-08-28', 1, 1, 1, 1, '2021-08-24 16:02:13', 1, '2021-08-28 09:14:53'),
(20, 'PBL-022/08/21', '2021-08-28', 4, 'CB22', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 11400000, '', 100000, '2021-08-28', 1, 1, 1, 1, '2021-08-28 14:30:03', 1, '2021-08-28 14:33:24'),
(21, 'PBL-023/08/21', '2021-08-28', 4, 'gtw', 'tbd', 1, 1, '2021-08-28', 4, 0, 0, 0, 4800000, '', 0, '2021-08-28', 1, 1, 1, 1, '2021-08-28 14:42:29', 1, '2021-08-28 16:23:51'),
(22, 'PBL-024/08/21', '2021-08-28', 1, 'lbl', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 4444400, '', 0, '0000-00-00', 1, 1, 1, 1, '2021-08-28 15:09:43', 1, '2021-08-28 15:43:42'),
(23, 'PBL-025/08/21', '2021-08-28', 4, 'hsjjj', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 1200000, '', 1000000, '2021-08-28', 1, 1, 1, 1, '2021-08-28 17:11:26', 1, '2021-08-28 17:12:28'),
(24, 'PBL-027/08/21', '2021-08-28', 5, 'kkk', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 0, 5196601, '', 5196601, '0000-00-00', 1, 1, 1, 1, '2021-08-28 18:17:41', 1, '2021-08-28 18:30:02'),
(25, 'PBN-028/08/21', '2021-08-30', 15, 'PBADAM', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 1200000, '', 1200000, '0000-00-00', 1, 1, 1, 1, '2021-08-30 16:58:21', 1, '2021-08-31 10:20:11'),
(26, 'PBN-029/08/21', '2021-08-31', 15, 'LBL', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 1000000, '', 1000000, '0000-00-00', 1, 1, 1, 1, '2021-08-31 10:22:26', 1, '2021-08-31 10:23:01'),
(27, 'PBN-030/08/21', '2021-08-31', 15, 'www', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 8021168, '', 8021168, '2021-08-31', 1, 1, 1, 1, '2021-08-31 11:05:20', 1, '2021-08-31 11:50:51'),
(28, 'PBN-031/08/21', '2021-08-31', 1, 'fff', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 1110208, '', 1110208, '2021-09-01', 1, 1, 1, 1, '2021-08-31 12:33:26', 1, '2021-09-01 12:16:28'),
(29, 'PBN-001/09/21', '2021-09-01', 5, '', '', 0, 0, '0000-00-00', 0, 0, 0, 0, 10266000, '', 0, '0000-00-00', 1, 2, 0, 1, '2021-09-01 16:50:06', 0, '0000-00-00 00:00:00'),
(30, 'PBN-002/09/21', '2021-09-04', 15, 'JAGUNG', 'transfer', 0, 0, '0000-00-00', 4, 0, 50000, 120000, 6070000, 'Enam Juta Tujuh Puluh Ribu', 6070000, '2021-09-04', 1, 1, 1, 1, '2021-09-04 08:58:37', 1, '2021-09-04 12:59:48'),
(31, 'PBN-003/09/21', '2021-09-04', 1, 'KATUL', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 120000, 4720000, 'Empat Juta Tujuh Ratus Dua Puluh Ribu Rupiah', 0, '0000-00-00', 1, 2, 0, 1, '2021-09-04 15:35:25', 0, '0000-00-00 00:00:00'),
(32, 'PBN-004/09/21', '2021-09-24', 15, 'cek um', 'cash', 0, 0, '0000-00-00', 2, 0, 100000, 150000, 8050000, '', 8050000, '2021-09-24', 1, 1, 1, 1, '2021-09-24 16:55:24', 1, '2021-09-25 16:52:15'),
(36, 'BB-ADM-202109.005', '2021-09-27', 15, 'Cek kode', 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 100000, '', 100000, '2021-09-27', 1, 1, 1, 1, '2021-09-27 12:29:54', 1, '2021-09-27 14:25:19'),
(37, 'BB-ALX-202109.006', '2021-09-27', 6, '233', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '', 0, '0000-00-00', 0, 0, 0, 1, '2021-09-27 12:54:27', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_lain_detail`
--

CREATE TABLE `pembelian_lain_detail` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_jenis_bahan` int(11) NOT NULL,
  `item` varchar(255) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_lain_detail`
--

INSERT INTO `pembelian_lain_detail` (`id`, `id_pembelian`, `id_jenis_bahan`, `item`, `kuantitas`, `harga_satuan`, `catatan`, `input_by`, `timestamp_input`) VALUES
(1, 1, 2, 'Katul hijau', 60, 2000, 'per kilo', 1, '2021-08-18 14:45:04'),
(2, 2, 6, 'Pembelian beras broken', 90, 3400, 'Jika ada', 1, '2021-08-19 10:35:42'),
(3, 3, 4, 'Keterangan', 900, 3000, '', 1, '2021-08-19 15:01:07'),
(4, 4, 6, 'Broken 2', 1000, 3000, '', 1, '2021-08-19 15:10:44'),
(5, 5, 4, 'Katul Jagung', 200, 3000, '', 1, '2021-08-20 13:22:36'),
(6, 6, 4, 'Katul 64', 1000, 4000, '', 1, '2021-08-20 13:46:10'),
(7, 7, 9, 'plus', 200, 4000, '', 1, '2021-08-20 13:49:41'),
(8, 8, 6, 'Broken ABC', 1000, 2000, '', 1, '2021-08-20 14:07:47'),
(9, 9, 5, 'Beras', 2000, 1000, '', 1, '2021-08-20 14:22:25'),
(10, 10, 7, 'kk', 102, 3000, '', 1, '2021-08-20 14:28:00'),
(11, 11, 6, 'Broken', 3902, 2010, '', 1, '2021-08-20 14:51:33'),
(12, 12, 6, 'Broken', 312, 1800, '', 1, '2021-08-20 14:59:56'),
(13, 13, 11, 'manna', 100, 4050, '', 1, '2021-08-20 16:37:31'),
(14, 14, 4, 'Katul Jagung', 1000, 3000, '', 1, '2021-08-21 14:28:01'),
(15, 15, 1, 'aaa', 1000, 6900, '', 1, '2021-08-21 19:09:26'),
(16, 15, 7, 'asada', 590, 7300, '', 1, '2021-08-21 19:09:50'),
(17, 16, 7, 'KK', 1000, 2000, '', 1, '2021-08-23 15:52:08'),
(18, 16, 1, 'BM', 1020, 2500, '', 1, '2021-08-23 15:52:27'),
(20, 17, 5, 'pembelian bahan', 1000, 2000, '', 1, '2021-08-23 17:06:17'),
(21, 17, 2, 'pembelian bahan', 300, 1200, '', 1, '2021-08-23 17:06:30'),
(22, 18, 4, 'pembelian bahan', 200, 100, '', 1, '2021-08-23 17:14:23'),
(23, 18, 3, 'pembelian bahan', 400, 300, '', 1, '2021-08-23 17:14:37'),
(24, 19, 11, 'pembelian bahan', 200, 2000, '', 1, '2021-08-24 16:02:57'),
(25, 19, 7, 'pembelian bahan', 230, 4000, '', 1, '2021-08-24 16:03:14'),
(26, 20, 6, 'pembelian bahan', 1000, 3000, '', 1, '2021-08-28 14:30:43'),
(27, 20, 9, 'pembelian bahan', 4200, 2000, '', 1, '2021-08-28 14:31:05'),
(28, 21, 11, 'pembelian bahan', 600, 8000, '', 1, '2021-08-28 14:42:46'),
(29, 22, 6, 'pembelian bahan', 22222, 200, '', 1, '2021-08-28 15:09:57'),
(30, 23, 7, 'pembelian bahan', 400, 3000, '', 1, '2021-08-28 17:11:40'),
(31, 24, 5, 'pembelian bahan', 1000, 4000, '', 1, '2021-08-28 18:13:28'),
(32, 24, 2, 'pembelian bahan', 399, 2999, '', 1, '2021-08-28 18:13:55'),
(33, 25, 11, 'pembelian bahan', 400, 3000, '', 1, '2021-08-30 16:58:40'),
(34, 26, 8, 'pembelian bahan', 1000, 1000, '', 1, '2021-08-31 10:22:39'),
(35, 27, 5, 'pembelian bahan', 2344, 3422, '', 1, '2021-08-31 11:05:38'),
(36, 28, 9, 'pembelian bahan', 332, 3344, '', 1, '2021-08-31 12:33:44'),
(37, 29, 6, 'pembelian bahan', 3422, 3000, '', 1, '2021-09-01 16:50:27'),
(38, 30, 4, 'pembelian bahan', 1200, 5000, '', 1, '2021-09-04 09:03:09'),
(39, 31, 4, 'pembelian bahan', 2300, 2000, '', 1, '2021-09-04 15:35:50'),
(40, 32, 8, 'pembelian bahan', 2000, 4000, '', 1, '2021-09-24 16:55:42'),
(41, 36, 8, 'pembelian bahan', 100, 1000, '', 1, '2021-09-27 12:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_lain_pembayaran`
--

CREATE TABLE `pembelian_lain_pembayaran` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `status_persetujuan` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_lain_pembayaran`
--

INSERT INTO `pembelian_lain_pembayaran` (`id`, `id_pembelian`, `tgl_pembayaran`, `jenis_pembayaran`, `id_kas`, `jumlah_bayar`, `catatan`, `is_paid_off`, `status_persetujuan`, `input_by`, `timestamp_input`, `acc_by`, `timestamp_acc`) VALUES
(1, 13, '2021-08-21', 'transfer', 4, 405000, '', 1, 1, 1, '2021-08-21 11:57:37', 1, '2021-08-21 12:00:14'),
(2, 1, '2021-08-21', 'transfer', 4, 120000, '', 1, 1, 1, '2021-08-21 13:37:37', 1, '2021-08-21 13:57:37'),
(3, 14, '2021-08-21', 'cash', 2, 3000000, '', 1, 1, 1, '2021-08-21 14:33:49', 1, '2021-08-21 14:34:01'),
(4, 21, '2021-08-28', 'transfer', 4, 4800000, '', 1, 1, 1, '2021-08-28 16:24:18', 1, '2021-08-28 16:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `penanggung_jawab`
--

CREATE TABLE `penanggung_jawab` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penanggung_jawab`
--

INSERT INTO `penanggung_jawab` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Samuel', 1, 0),
(2, 'Iik', 1, 0),
(3, 'Magda', 1, 0),
(4, 'Hana', 1, 0),
(5, 'Alex', 1, 0),
(6, 'Indra', 1, 0),
(7, 'B. Evie', 1, 0),
(8, 'Rani', 1, 0),
(9, 'Shannen', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan`
--

CREATE TABLE `pendapatan` (
  `id` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan`
--

INSERT INTO `pendapatan` (`id`, `tgl_transaksi`, `id_kategori`, `nominal`, `catatan`, `jenis_pembayaran`, `id_kas`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, '2020-10-14', 4, 800000, 'Penjualan sak jumbo', 'cash', 2, 1, 1, '2020-10-14 15:06:23', 1, '2020-10-14 18:01:31'),
(2, '2021-09-07', 3, 15000000, 'Jual Kambing 7 biji', 'tbd', 0, 1, 1, '2021-09-07 10:41:14', 1, '2021-09-07 10:41:32'),
(3, '2021-09-10', 2, 20102010, 'Jual Sekam', 'cash', 1, 1, 1, '2021-09-10 15:24:06', 1, '2021-09-10 15:24:42'),
(4, '2021-09-24', 5, 100000, 'Potongan pembelian PBN-004/09/21', 'cash', 2, 1, 1, '2021-09-25 16:52:15', 1, '2021-09-25 16:52:15'),
(5, '2021-09-25', 5, 150000, 'Biaya tambahan penjualan PJL-003/09/21', 'cash', 2, 1, 1, '2021-09-25 17:52:10', 1, '2021-09-25 17:52:10'),
(6, '2021-09-27', 5, 100000, 'Biaya tambahan penjualan PJL-002/09/21', 'transfer', 4, 1, 1, '2021-09-25 18:19:04', 1, '2021-09-25 18:19:04'),
(7, '2021-09-27', 5, 100000, 'Biaya tambahan penjualan beras PO-016/09/21', 'transfer', 4, 1, 1, '2021-09-27 16:22:51', 1, '2021-09-27 16:22:51'),
(8, '2021-09-28', 5, 100000, 'Biaya tambahan penjualan beras PO-017/09/21', 'transfer', 5, 1, 1, '2021-09-27 17:01:55', 1, '2021-09-27 17:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `pengeringan`
--

CREATE TABLE `pengeringan` (
  `id` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `tanggal_mulai` datetime NOT NULL,
  `jenis_pengeringan` enum('dryer','jemur','') NOT NULL,
  `jenis_proses` varchar(100) NOT NULL,
  `id_jenis_padi` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `hpp_beli` int(11) NOT NULL,
  `tanggal_selesai` datetime NOT NULL,
  `berat_kering` float NOT NULL,
  `hpp_kering` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `persetujuan` tinyint(1) NOT NULL COMMENT '0=menunggu, 1=diajukan, 2=acc, 3=reject',
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeringan`
--

INSERT INTO `pengeringan` (`id`, `kode`, `tanggal_mulai`, `jenis_pengeringan`, `jenis_proses`, `id_jenis_padi`, `kuantitas`, `hpp_beli`, `tanggal_selesai`, `berat_kering`, `hpp_kering`, `keterangan`, `petugas`, `persetujuan`, `input_by`, `timestamp_input`, `verif_by`, `timestamp_verif`) VALUES
(1, 'JMR-20200510-001', '2020-05-10 18:00:00', 'jemur', 'tapel', 2, 1500, 7329, '2020-05-18 05:00:00', 1200, 9161, 'Jemur Gabah', 'Jumasan', 2, 1, '2020-05-18 11:23:19', 1, '2020-06-01 00:13:59'),
(3, 'DRY-20200519-001', '2020-05-19 17:00:00', 'dryer', 'giling', 1, 3000, 6500, '2020-05-20 01:00:00', 2550, 7647, '', 'ARI', 2, 1, '2020-05-19 14:45:38', 1, '2020-06-10 13:32:20'),
(6, 'DRY-20200610-001', '2020-06-10 12:00:00', 'dryer', 'tapel', 1, 8000, 6188, '2020-06-10 21:00:00', 6800, 7280, '', 'Ari', 2, 1, '2020-06-10 13:01:55', 1, '2020-06-10 13:32:13'),
(7, 'DRY-20200610-002', '2020-06-10 17:00:00', 'dryer', 'giling', 1, 2667, 5200, '2020-06-10 23:00:00', 2267, 6118, 'Dryer KS 64 tanggal 2020-06-10', 'Ari', 2, 1, '2020-06-10 14:55:18', 1, '2020-06-10 14:56:40'),
(9, 'DRY-20200613-001', '2020-06-13 05:00:00', 'dryer', 'giling', 1, 0, 0, '2020-06-13 23:00:00', 1777.5, 0, '', 'Ari', 1, 1, '2020-06-12 15:26:54', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pengeringan_detail`
--

CREATE TABLE `pengeringan_detail` (
  `id` int(11) NOT NULL,
  `id_pengeringan` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `kuantitas` float NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `id_dryer` int(11) NOT NULL,
  `id_jemur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeringan_detail`
--

INSERT INTO `pengeringan_detail` (`id`, `id_pengeringan`, `id_stok`, `kuantitas`, `harga_satuan`, `id_dryer`, `id_jemur`) VALUES
(18, 1, 22, 1000, 6993, 0, 1),
(19, 1, 26, 500, 8000, 0, 1),
(22, 3, 0, 4000, 0, 1, 0),
(24, 3, 29, 3000, 6500, 1, 0),
(25, 6, 0, 5000, 0, 1, 0),
(26, 6, 0, 2000, 0, 1, 0),
(27, 6, 28, 5000, 6000, 1, 0),
(28, 6, 29, 3000, 6500, 1, 0),
(29, 7, 33, 2667, 5200, 1, 0),
(31, 9, 27, 2000, 6000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penggilingan`
--

CREATE TABLE `penggilingan` (
  `id` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `tanggal_mulai` datetime NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `hpp_awal` int(11) NOT NULL,
  `tanggal_selesai` datetime NOT NULL,
  `hasil_beras` int(11) NOT NULL,
  `hasil_menir` int(11) NOT NULL,
  `hasil_katul` int(11) NOT NULL,
  `hasil_sekam` int(11) NOT NULL,
  `broken` int(11) NOT NULL,
  `harga_medium` int(11) NOT NULL,
  `harga_kepala` int(11) NOT NULL,
  `harga_broken` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `persetujuan` tinyint(4) NOT NULL COMMENT '0=menunggu, 1=diajukan, 2=acc, 3=reject	',
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggilingan`
--

INSERT INTO `penggilingan` (`id`, `kode`, `tanggal_mulai`, `petugas`, `kuantitas`, `hpp_awal`, `tanggal_selesai`, `hasil_beras`, `hasil_menir`, `hasil_katul`, `hasil_sekam`, `broken`, `harga_medium`, `harga_kepala`, `harga_broken`, `keterangan`, `persetujuan`, `input_by`, `timestamp_input`, `verif_by`, `timestamp_verif`) VALUES
(1, 'GIL-20200603-002', '2020-06-03 07:00:00', 'Jumasan', 1319, 9219, '2020-06-04 13:00:00', 1000, 300, 0, 0, 20, 0, 0, 0, 'hasil dryer ', 0, 1, '2020-06-10 14:19:24', 0, '0000-00-00 00:00:00'),
(2, 'GIL-20200610-002', '2020-06-10 01:00:00', 'Jumasan', 4069, 8231, '2020-06-11 01:00:00', 1759, 26, 0, 0, 20, 19040, 3004, 83184, 'hasil dryer tgl 10-06-2020', 2, 1, '2020-06-10 14:20:55', 1, '2021-08-20 14:55:02'),
(3, 'GIL-20200611-001', '2020-06-11 08:00:00', 'Jumasan', 2267, 6118, '2020-06-11 23:00:00', 1564, 23, 0, 0, 20, 8868, 9200, 7540, 'Giling KG Alex 10 Juni 2020', 2, 1, '2020-06-10 15:05:49', 1, '2020-06-13 16:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `penggilingan_detail`
--

CREATE TABLE `penggilingan_detail` (
  `id` int(11) NOT NULL,
  `id_penggilingan` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggilingan_detail`
--

INSERT INTO `penggilingan_detail` (`id`, `id_penggilingan`, `id_stok`, `kuantitas`, `harga_satuan`) VALUES
(1, 1, 30, 1000, 9161),
(2, 1, 24, 319, 9400),
(3, 2, 32, 2550, 7647),
(4, 3, 34, 2267, 6118),
(5, 1, 0, 1170, 0),
(8, 1, 23, 1170, 9000),
(9, 2, 30, 1200, 9161),
(10, 2, 24, 319, 9400);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_lain`
--

CREATE TABLE `penjualan_lain` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `is_tempo` tinyint(4) NOT NULL,
  `is_tempo_dibayar` tinyint(4) NOT NULL,
  `tgl_tempo` date NOT NULL,
  `id_kas` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `biaya_tambahan` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `persetujuan` tinyint(4) NOT NULL,
  `is_lunas` tinyint(4) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_lain`
--

INSERT INTO `penjualan_lain` (`id`, `kode`, `tanggal`, `id_customer`, `catatan`, `jenis_pembayaran`, `is_tempo`, `is_tempo_dibayar`, `tgl_tempo`, `id_kas`, `potongan`, `biaya_tambahan`, `grandtotal`, `jumlah_bayar`, `tgl_bayar`, `status`, `persetujuan`, `is_lunas`, `input_by`, `timestamp_input`, `verif_by`, `timestamp_verif`) VALUES
(1, 'PJL-003/10/20', '2020-10-19', 11, 'Penjualan Menir dan Jagung', 'transfer', 0, 0, '0000-00-00', 4, 10000, 0, 2023000, 2023000, '2020-10-26', 1, 1, 1, 1, '2020-10-24 16:02:49', 1, '2020-10-26 15:48:14'),
(2, 'PJL-005/10/20', '2020-10-27', 12, 'Pembelian Jagung', 'tbd', 1, 0, '2020-11-05', 4, 0, 0, 2750000, 0, '2020-10-27', 1, 1, 1, 1, '2020-10-27 10:57:53', 1, '2020-10-27 10:59:45'),
(3, 'PJL-006/10/20', '2020-10-28', 11, 'tes', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 2876589, 0, '0000-00-00', 0, 0, 0, 1, '2020-10-28 13:39:19', 0, '0000-00-00 00:00:00'),
(4, 'PJL-001/08/21', '2021-08-01', 7, 'Penjualan sak', 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 100000, 100000, '0000-00-00', 1, 1, 1, 1, '2021-08-17 11:15:41', 1, '2021-08-17 11:18:50'),
(5, 'PJL-001/09/21', '2021-09-24', 12, 'Jual sak', 'transfer', 1, 0, '2021-09-30', 0, 0, 0, 95000, 0, '0000-00-00', 1, 1, 0, 1, '2021-09-25 09:34:06', 1, '2021-09-25 09:36:14'),
(6, 'PJL-002/09/21', '2021-09-25', 10, 'Dengan TEMPO', 'transfer', 1, 1, '2021-09-30', 4, 50000, 100000, 250000, 250000, '2021-09-29', 1, 1, 1, 1, '2021-09-25 17:07:21', 1, '2021-09-25 18:01:44'),
(7, 'PJL-003/09/21', '2021-09-25', 9, 'TANPA TEMPO', 'cash', 0, 0, '0000-00-00', 2, 200000, 150000, 2250000, 2250000, '2021-09-25', 1, 1, 1, 1, '2021-09-25 17:09:04', 1, '2021-09-25 17:52:10'),
(8, 'JL-DNY-202109.001', '2021-09-27', 11, 'Cek kode', 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 1, '2021-09-27 15:21:16', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_lain_detail`
--

CREATE TABLE `penjualan_lain_detail` (
  `id` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `item` varchar(255) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_lain_detail`
--

INSERT INTO `penjualan_lain_detail` (`id`, `id_penjualan`, `id_kategori`, `item`, `kuantitas`, `harga_satuan`, `catatan`, `input_by`, `timestamp_input`) VALUES
(1, 1, 1, 'Jagung', 11, 3000, 'ini adalah contoh catatan, ini adalah contoh catatan', 1, '2020-10-21 12:36:53'),
(2, 1, 2, 'Menir gula', 1000, 2000, '', 1, '2020-10-24 16:02:21'),
(3, 2, 1, 'Jagung', 1000, 2750, '', 1, '2020-10-27 10:57:31'),
(4, 3, 1, 'Jagung A', 2333, 1233, '', 1, '2020-10-28 13:39:33'),
(5, 4, 1, 'Sak jagung', 20, 5000, 'Sak dari pak gofur', 1, '2021-08-17 11:16:24'),
(6, 5, 1, 'Sak jagung', 190, 500, 'sak 25kg', 1, '2021-09-25 09:34:49'),
(7, 6, 2, 'Menir', 200, 1000, '', 1, '2021-09-25 17:07:39'),
(8, 7, 1, 'Jagung', 2300, 1000, '', 1, '2021-09-25 17:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_lain_pembayaran`
--

CREATE TABLE `penjualan_lain_pembayaran` (
  `id` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `status_persetujuan` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_lain_pembayaran`
--

INSERT INTO `penjualan_lain_pembayaran` (`id`, `id_penjualan`, `tgl_pembayaran`, `jenis_pembayaran`, `id_kas`, `jumlah_bayar`, `catatan`, `is_paid_off`, `status_persetujuan`, `input_by`, `timestamp_input`, `acc_by`, `timestamp_acc`) VALUES
(1, 2, '2020-10-27', 'transfer', 4, 2750000, '', 1, 1, 1, '2020-10-27 14:01:20', 1, '2020-10-27 14:53:36'),
(2, 5, '2021-09-25', 'cash', 1, 95000, '', 1, 0, 1, '2021-09-25 09:38:50', 0, '0000-00-00 00:00:00'),
(3, 6, '2021-09-27', 'transfer', 4, 150000, 'cicil pertama', 0, 1, 1, '2021-09-25 18:04:57', 1, '2021-09-25 18:19:03'),
(4, 6, '2021-09-29', 'transfer', 4, 100000, 'cicil kedua', 1, 1, 1, '2021-09-25 18:24:36', 1, '2021-09-25 18:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_po`
--

CREATE TABLE `penjualan_po` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_surat_jalan` varchar(100) NOT NULL,
  `no_resi` varchar(50) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_sales` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=menunggu, 1=diajukan, 2=proses produksi, 3=selesai, 4=batal',
  `status_kontan` tinyint(1) NOT NULL,
  `status_po` tinyint(4) NOT NULL,
  `status_produksi` tinyint(4) NOT NULL,
  `status_pengiriman` tinyint(4) NOT NULL,
  `status_selesai` tinyint(4) NOT NULL,
  `persetujuan` tinyint(1) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL DEFAULT 'tbd',
  `is_tempo` tinyint(1) NOT NULL,
  `is_tempo_dibayar` tinyint(1) NOT NULL,
  `tgl_tempo` date NOT NULL,
  `id_kas` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `biaya_tambahan` int(11) NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `is_lunas` tinyint(1) NOT NULL,
  `verifikasi_oleh` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_po`
--

INSERT INTO `penjualan_po` (`id`, `kode`, `tanggal`, `kode_surat_jalan`, `no_resi`, `tgl_kirim`, `id_customer`, `id_sales`, `catatan`, `status`, `status_kontan`, `status_po`, `status_produksi`, `status_pengiriman`, `status_selesai`, `persetujuan`, `jenis_pembayaran`, `is_tempo`, `is_tempo_dibayar`, `tgl_tempo`, `id_kas`, `potongan`, `biaya_tambahan`, `grandtotal`, `jumlah_bayar`, `tgl_bayar`, `is_lunas`, `verifikasi_oleh`, `timestamp_verifikasi`, `input_by`, `timestamp_input`) VALUES
(1, 'PO-003/07/20', '2020-07-06', 'SJ-001/07/20', 'RS191030-2HB', '2020-07-10', 7, 3, '', 1, 0, 1, 1, 1, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 18440000, 18440000, '2021-09-14', 1, 1, '2021-09-14 11:00:16', 1, '2020-07-08 17:17:16'),
(2, 'PO-004/07/20', '2020-07-06', 'SJ-002/07/20', 'RS191010-XFH', '2020-07-10', 11, 3, '', 1, 0, 1, 1, 1, 1, 1, 'tbd', 1, 0, '2020-08-12', 5, 0, 0, 33880000, 0, '2020-09-29', 1, 0, '0000-00-00 00:00:00', 1, '2020-07-13 14:27:24'),
(5, 'PO-003/08/20', '2020-08-21', '', '', '2020-08-21', 9, 4, 'tes', 0, 1, 0, 0, 0, 1, 1, 'cash', 0, 0, '2020-09-30', 2, 0, 0, 1790000, 1790000, '2020-09-16', 1, 1, '2020-09-16 17:20:42', 1, '2020-08-22 15:45:30'),
(6, 'PO-004/08/20', '2020-08-27', '', '', '2020-08-27', 9, 4, '', 0, 1, 0, 0, 0, 1, 1, 'transfer', 0, 0, '2020-10-10', 4, 0, 0, 1275000, 1275000, '2020-09-14', 1, 1, '2020-09-14 16:42:09', 1, '2020-08-27 15:06:28'),
(9, 'PO-001/10/20', '2020-10-24', '', '', '2020-10-24', 9, 3, '', 0, 1, 0, 0, 0, 1, 1, 'transfer', 0, 0, '0000-00-00', 4, 0, 0, 282500, 282500, '2021-09-03', 1, 1, '2021-09-03 14:53:33', 1, '2020-10-24 14:30:16'),
(10, 'PO-001/06/21', '2021-06-18', '', '14', '0000-00-00', 7, 2, '', 0, 0, 1, 1, 1, 1, 1, 'cash', 1, 0, '2021-09-30', 0, 0, 0, 0, 1860000, '0000-00-00', 0, 1, '2021-09-14 11:09:28', 1, '2021-09-03 14:51:08'),
(11, 'PO-001/09/21', '2021-09-10', '', '14', '0000-00-00', 9, 3, 'Beras', 0, 0, 1, 0, 1, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 43645500, '0000-00-00', 1, 1, '2021-09-14 11:08:34', 1, '2021-09-10 15:13:31'),
(12, 'PO-002/09/21', '2021-09-11', '', '', '2021-09-11', 9, 2, 'Jual manna', 0, 1, 0, 0, 0, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 16700000, 16700000, '2021-09-11', 1, 1, '2021-09-11 15:45:05', 1, '2021-09-11 15:03:32'),
(14, 'PO-003/09/21', '2021-09-15', '', '', '0000-00-00', 10, 4, 'PO', 0, 0, 1, 1, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-15 12:21:51'),
(15, 'PO-004/09/21', '2021-09-15', '', '', '2021-09-15', 9, 2, '', 0, 1, 0, 0, 0, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 2325000, 2325000, '2021-09-15', 1, 1, '2021-09-15 14:43:18', 1, '2021-09-15 14:37:58'),
(16, 'PO-005/09/21', '2021-09-14', '', '', '2021-09-14', 9, 3, 'Coba Stok', 0, 1, 0, 0, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-15 15:11:08'),
(17, 'PO-006/09/21', '2021-09-15', 'SJ-016/09/21', 'RS210915-4DK', '2021-09-18', 1, 4, 'PO-1', 0, 0, 1, 1, 1, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 3802500, 3802500, '2021-09-18', 1, 1, '2021-09-18 16:17:41', 1, '2021-09-15 15:18:53'),
(18, 'PO-007/09/21', '2021-09-15', 'SJ-017/09/21', 'RS210915-4DK', '2021-09-17', 1, 3, 'PO-2', 0, 0, 1, 1, 0, 1, 1, 'cash', 1, 0, '2021-09-30', 1, 0, 0, 9572000, 9572000, '2021-09-23', 0, 1, '2021-09-23 09:08:59', 1, '2021-09-15 15:19:43'),
(19, 'PO-008/09/21', '2021-09-15', 'SJ-019/09/21', 'RS210915-4DK', '2021-09-17', 7, 2, 'PO-3', 0, 0, 1, 1, 0, 0, 0, 'tbd', 1, 0, '2021-09-24', 0, 49500, 1000, 31300000, 31300000, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-15 15:20:19'),
(20, 'PO-009/09/21', '2021-09-15', '', '16', '0000-00-00', 1, 3, 'tes 1', 0, 0, 1, 1, 1, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 0, 0, 0, 7945000, '2021-09-15', 1, 1, '2021-09-15 16:49:26', 1, '2021-09-15 16:36:54'),
(21, 'PO-010/09/21', '2021-09-16', '', '', '0000-00-00', 7, 2, 'Cek tempo', 0, 0, 1, 1, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-16 13:39:49'),
(22, 'PO-011/09/21', '2021-09-16', '', '', '0000-00-00', 1, 4, 'Coba tempo note', 0, 0, 0, 0, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-16 14:41:27'),
(23, 'PO-012/09/21', '2021-09-16', 'SJ-005/09/21', '16', '2021-09-16', 9, 2, 'Check kode SJ', 0, 0, 1, 0, 1, 1, 1, 'cash', 0, 0, '0000-00-00', 2, 1000, 500, 0, 569500, '2021-09-17', 1, 1, '2021-09-17 15:12:39', 1, '2021-09-17 14:02:50'),
(24, 'PO-013/09/21', '2021-09-17', '', '', '0000-00-00', 7, 2, 'Tempo2', 0, 0, 1, 0, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-17 16:14:12'),
(25, 'PO-014/09/21', '2021-09-18', '', 'RS210915-4DK', '0000-00-00', 7, 3, '', 0, 0, 1, 0, 1, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-18 14:25:35'),
(26, 'PO-015/09/21', '2021-09-24', '', '', '0000-00-00', 9, 3, 'Testing', 0, 0, 1, 0, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-24 16:02:24'),
(27, 'PO-016/09/21', '2021-09-25', 'SJ-021/09/21', 'RS210925-4HR', '2021-09-25', 10, 4, 'TEST BIAYA NON TEMPO', 0, 0, 1, 0, 0, 1, 1, 'transfer', 0, 0, '0000-00-00', 4, 93000, 100000, 83000000, 83000000, '2021-09-27', 1, 1, '2021-09-27 16:22:51', 1, '2021-09-25 17:03:17'),
(28, 'PO-017/09/21', '2021-09-25', 'SJ-022/09/21', 'RS210925-4HR', '2021-09-26', 9, 3, 'TEST BIAYA DENGAN TEMPO', 0, 0, 1, 0, 0, 1, 1, 'transfer', 1, 1, '2021-09-30', 5, 291000, 100000, 18000000, 18000000, '2021-09-28', 1, 1, '2021-09-27 16:21:29', 1, '2021-09-25 17:05:10'),
(29, 'JB-MGD-202109.010', '2021-09-27', 'SJ-MGD-202109.010', '', '0000-00-00', 7, 3, 'Cek kode', 0, 0, 1, 0, 0, 0, 0, 'tbd', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-27 11:54:40'),
(31, 'BK-BAS-202109.011', '2021-09-28', '', '', '2021-09-28', 9, 3, 'Eceran', 0, 1, 0, 0, 0, 0, 0, 'cash', 0, 0, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', 1, '2021-09-28 10:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_po_detail`
--

CREATE TABLE `penjualan_po_detail` (
  `id` int(11) NOT NULL,
  `id_po` int(11) NOT NULL,
  `id_kontan` int(11) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_po_detail`
--

INSERT INTO `penjualan_po_detail` (`id`, `id_po`, `id_kontan`, `id_kemasan`, `kuantitas`, `harga_satuan`, `catatan`, `input_by`, `timestamp_input`) VALUES
(132, 2, 0, 11, 50, 9500, '', 0, '2020-08-25 17:25:06'),
(133, 2, 0, 12, 10, 9400, '', 0, '2020-08-25 17:25:06'),
(134, 2, 0, 4, 15, 9300, '', 0, '2020-08-25 17:25:06'),
(135, 2, 0, 14, 55, 10200, '', 0, '2020-08-25 17:25:06'),
(136, 2, 0, 3, 20, 11800, '', 0, '2020-08-25 17:25:06'),
(137, 2, 0, 1, 10, 11300, '', 0, '2020-08-25 17:25:06'),
(138, 2, 0, 13, 100, 8300, '', 0, '2020-08-25 17:25:06'),
(139, 2, 0, 8, 25, 8100, '', 0, '2020-08-25 17:25:06'),
(140, 2, 0, 9, 50, 10200, '', 0, '2020-08-25 17:25:06'),
(141, 3, 0, 11, 10, 0, '', 0, '2020-08-25 17:25:06'),
(142, 3, 0, 12, 20, 0, '', 0, '2020-08-25 17:25:06'),
(143, 3, 0, 4, 40, 0, '', 0, '2020-08-25 17:25:06'),
(144, 3, 0, 5, 40, 0, '', 0, '2020-08-25 17:25:06'),
(145, 3, 0, 6, 15, 0, '', 0, '2020-08-25 17:25:06'),
(146, 3, 0, 3, 10, 0, '', 0, '2020-08-25 17:25:06'),
(147, 3, 0, 1, 10, 0, '', 0, '2020-08-25 17:25:06'),
(148, 3, 0, 2, 20, 0, '', 0, '2020-08-25 17:25:06'),
(149, 3, 0, 13, 100, 0, '', 0, '2020-08-25 17:25:06'),
(150, 3, 0, 8, 200, 0, '', 0, '2020-08-25 17:25:06'),
(157, 5, 0, 2, 5, 11300, '', 1, '2020-08-25 17:31:47'),
(158, 5, 0, 2, 1, 11300, '', 1, '2020-08-25 17:41:01'),
(159, 5, 0, 11, 2, 9500, '', 1, '2020-08-26 16:39:44'),
(163, 6, 2, 4, 1, 9300, '', 1, '2020-08-29 14:49:25'),
(164, 6, 2, 3, 10, 11800, '', 1, '2020-08-29 14:55:01'),
(165, 6, 2, 13, 5, 8300, '', 1, '2020-08-29 14:55:56'),
(166, 6, 4, 5, 1, 9800, '', 1, '2020-09-05 16:09:17'),
(167, 9, 5, 2, 1, 11300, '', 1, '2020-10-24 14:30:50'),
(168, 1, 0, 11, 50, 9500, '', 0, '2021-01-15 14:06:15'),
(169, 1, 0, 4, 10, 9300, '', 0, '2021-01-15 14:06:15'),
(170, 1, 0, 6, 15, 8600, '', 0, '2021-01-15 14:06:15'),
(171, 1, 0, 3, 60, 11800, '', 0, '2021-01-15 14:06:15'),
(172, 1, 0, 1, 25, 11300, '', 0, '2021-01-15 14:06:15'),
(173, 1, 0, 13, 100, 8300, '', 0, '2021-01-15 14:06:15'),
(175, 10, 0, 4, 8, 9300, '', 0, '2021-09-03 14:57:14'),
(176, 12, 6, 3, 100, 11800, '', 1, '2021-09-11 15:04:37'),
(177, 12, 6, 3, 200, 10800, '', 1, '2021-09-11 15:05:11'),
(187, 11, 0, 11, 3, 9500, '', 0, '2021-09-14 10:18:46'),
(188, 11, 0, 14, 234, 10200, '', 0, '2021-09-14 10:18:46'),
(189, 11, 0, 5, 4, 9800, '', 0, '2021-09-14 10:18:46'),
(190, 11, 0, 6, 5, 8600, '', 0, '2021-09-14 10:18:46'),
(191, 11, 0, 7, 44, 9100, '', 0, '2021-09-14 10:18:46'),
(192, 11, 0, 3, 6, 11800, '', 0, '2021-09-14 10:18:46'),
(193, 11, 0, 8, 12, 8100, '', 0, '2021-09-14 10:18:46'),
(194, 11, 0, 9, 44, 10200, '', 0, '2021-09-14 10:18:46'),
(195, 11, 0, 10, 22, 10000, '', 0, '2021-09-14 10:18:46'),
(205, 13, 0, 12, 44, 0, '', 0, '2021-09-14 11:31:39'),
(206, 13, 0, 4, 5, 0, '', 0, '2021-09-14 11:31:39'),
(207, 13, 0, 14, 55, 0, '', 0, '2021-09-14 11:31:39'),
(208, 13, 0, 5, 55, 0, '', 0, '2021-09-14 11:31:39'),
(209, 13, 0, 6, 55, 0, '', 0, '2021-09-14 11:31:39'),
(210, 13, 0, 7, 44, 0, '', 0, '2021-09-14 11:31:39'),
(211, 13, 0, 3, 55, 0, '', 0, '2021-09-14 11:31:39'),
(212, 13, 0, 2, 54, 0, '', 0, '2021-09-14 11:31:39'),
(213, 13, 0, 13, 45, 0, '', 0, '2021-09-14 11:31:39'),
(217, 14, 0, 12, 20, 0, '', 0, '2021-09-15 12:22:35'),
(218, 14, 0, 13, 37, 0, '', 0, '2021-09-15 12:22:35'),
(219, 14, 0, 10, 10, 0, '', 0, '2021-09-15 12:22:35'),
(221, 15, 7, 4, 10, 9300, '', 1, '2021-09-15 14:41:47'),
(224, 17, 0, 11, 21, 9500, '', 0, '2021-09-15 15:19:18'),
(225, 17, 0, 9, 11, 10200, '', 0, '2021-09-15 15:19:18'),
(230, 18, 0, 14, 22, 10200, '', 0, '2021-09-15 15:19:57'),
(231, 18, 0, 5, 11, 9800, '', 0, '2021-09-15 15:19:57'),
(232, 18, 0, 6, 11, 8600, '', 0, '2021-09-15 15:19:57'),
(233, 18, 0, 2, 12, 11300, '', 0, '2021-09-15 15:19:57'),
(240, 19, 0, 11, 12, 9500, '', 0, '2021-09-15 15:22:35'),
(241, 19, 0, 1, 2, 11300, '', 0, '2021-09-15 15:22:35'),
(242, 19, 0, 10, 121, 10100, '', 0, '2021-09-15 15:22:35'),
(246, 20, 0, 3, 10, 11700, '', 0, '2021-09-15 16:37:53'),
(247, 20, 0, 1, 15, 11400, '', 0, '2021-09-15 16:37:53'),
(248, 20, 0, 2, 20, 11300, '', 0, '2021-09-15 16:37:53'),
(254, 21, 0, 12, 12, 0, '', 0, '2021-09-16 16:14:10'),
(255, 21, 0, 7, 12, 0, '', 0, '2021-09-16 16:14:10'),
(256, 21, 0, 2, 34, 0, '', 0, '2021-09-16 16:14:10'),
(257, 21, 0, 8, 343, 0, '', 0, '2021-09-16 16:14:10'),
(258, 21, 0, 9, 3, 0, '', 0, '2021-09-16 16:14:10'),
(260, 23, 0, 11, 12, 9500, '', 0, '2021-09-17 14:05:37'),
(263, 24, 0, 11, 23, 0, '', 0, '2021-09-17 16:15:04'),
(264, 24, 0, 12, 22, 0, '', 0, '2021-09-17 16:15:04'),
(265, 25, 0, 11, 12, 0, '', 0, '2021-09-18 14:47:11'),
(266, 16, 8, 4, 100, 9300, 'Cek tempo', 1, '2021-09-23 09:06:55'),
(269, 26, 0, 11, 24, 0, '', 0, '2021-09-24 16:02:42'),
(270, 26, 0, 6, 22, 0, '', 0, '2021-09-24 16:02:42'),
(275, 27, 0, 11, 12, 9500, '', 0, '2021-09-25 17:03:32'),
(276, 27, 0, 5, 323, 9800, '', 0, '2021-09-25 17:03:32'),
(277, 27, 0, 6, 12, 8600, '', 0, '2021-09-25 17:03:32'),
(278, 27, 0, 3, 12, 11800, '', 0, '2021-09-25 17:03:32'),
(279, 28, 0, 11, 212, 9500, '', 0, '2021-09-25 17:05:21'),
(280, 28, 0, 5, 23, 9800, '', 0, '2021-09-25 17:05:21'),
(281, 28, 0, 1, 22, 11300, '', 0, '2021-09-25 17:05:21'),
(282, 29, 0, 11, 222, 0, '', 0, '2021-09-27 11:54:55'),
(283, 29, 0, 12, 23, 0, '', 0, '2021-09-27 11:54:55'),
(284, 29, 0, 4, 23, 0, '', 0, '2021-09-27 11:54:55'),
(287, 31, 10, 12, 100, 9400, '', 1, '2021-09-28 10:42:23');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_po_kontan`
--

CREATE TABLE `penjualan_po_kontan` (
  `id` int(11) NOT NULL,
  `id_po` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_po_kontan`
--

INSERT INTO `penjualan_po_kontan` (`id`, `id_po`, `nama`, `catatan`, `input_by`, `timestamp_input`) VALUES
(2, 6, 'DENY', 'tes', 1, '2020-08-29 14:31:34'),
(4, 6, 'ECERAN', '', 1, '2020-09-05 16:09:00'),
(5, 9, 'ECERAN', '', 1, '2020-10-24 14:30:25'),
(6, 12, 'ECERAN', 'Manna', 1, '2021-09-11 15:04:08'),
(7, 15, 'ECERAN', '', 1, '2021-09-15 14:38:05'),
(8, 16, 'ECERAN', '', 1, '2021-09-15 15:14:21'),
(9, 0, 'ECERAN', 'Ada', 1, '2021-09-28 10:34:08'),
(10, 31, 'ECERAN', '', 1, '2021-09-28 10:40:11');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_po_pembayaran`
--

CREATE TABLE `penjualan_po_pembayaran` (
  `id` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `status_persetujuan` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_po_pembayaran`
--

INSERT INTO `penjualan_po_pembayaran` (`id`, `id_penjualan`, `tgl_pembayaran`, `jenis_pembayaran`, `id_kas`, `jumlah_bayar`, `catatan`, `is_paid_off`, `status_persetujuan`, `input_by`, `timestamp_input`, `acc_by`, `timestamp_acc`) VALUES
(3, 2, '2020-09-29', 'transfer', 5, 33880000, '', 1, 1, 1, '2020-09-25 17:40:50', 1, '2020-10-27 15:02:21'),
(4, 18, '2021-09-23', 'cash', 1, 100000, 'Bayar 1', 0, 1, 1, '2021-09-23 09:09:55', 1, '2021-09-23 09:12:12'),
(5, 18, '2021-09-23', 'transfer', 4, 0, 'Bayar tempo untuk retur Beras5', 0, 1, 1, '2021-09-23 14:17:07', 1, '2021-09-23 14:17:07'),
(6, 18, '2021-09-23', 'transfer', 5, 1000006, 'Bayar tempo untuk retur Beras6', 0, 1, 1, '2021-09-23 14:26:52', 1, '2021-09-23 14:26:52'),
(7, 28, '2021-09-28', 'transfer', 5, 10000000, 'cicil 1', 0, 1, 1, '2021-09-27 16:25:52', 1, '2021-09-27 17:01:54'),
(8, 28, '2021-09-28', 'transfer', 5, 8000000, 'cicil 2', 1, 1, 1, '2021-09-27 17:03:29', 1, '2021-09-27 17:10:29');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produksi`
--

CREATE TABLE `penjualan_produksi` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `catatan` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=input data, 1=selesai',
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_produksi`
--

INSERT INTO `penjualan_produksi` (`id`, `kode`, `tanggal`, `catatan`, `status`, `input_by`, `timestamp_input`) VALUES
(3, 'OP-001/07/20', '2020-07-06', 'test', 1, 1, '2020-07-24 18:08:25'),
(4, 'OP-001/09/21', '2021-09-14', 'Prod', 1, 1, '2021-09-14 09:13:17'),
(5, 'OP-002/09/21', '2021-09-14', 'PO BERAS RUKO', 1, 1, '2021-09-14 11:33:07'),
(8, 'OP-005/09/21', '2021-09-15', 'po 15/0/21', 0, 1, '2021-09-15 15:34:26'),
(9, 'OP-006/09/21', '2021-09-16', '', 1, 1, '2021-09-17 11:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produksi_detail`
--

CREATE TABLE `penjualan_produksi_detail` (
  `id` int(11) NOT NULL,
  `id_produksi` int(11) NOT NULL,
  `id_po` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_produksi_detail`
--

INSERT INTO `penjualan_produksi_detail` (`id`, `id_produksi`, `id_po`) VALUES
(25, 3, 1),
(26, 3, 2),
(27, 4, 10),
(28, 5, 13),
(32, 8, 19),
(33, 8, 18),
(34, 8, 14),
(35, 8, 17),
(36, 8, 20),
(37, 9, 21);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_produksi_proses`
--

CREATE TABLE `penjualan_produksi_proses` (
  `id` int(11) NOT NULL,
  `id_do` int(11) NOT NULL,
  `id_merk` int(11) NOT NULL,
  `tonase_po` int(11) NOT NULL,
  `tonase_ready` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_retur`
--

CREATE TABLE `penjualan_retur` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `item_retur` varchar(500) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL,
  `id_kas` int(11) NOT NULL,
  `persetujuan` tinyint(1) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `total_tonase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_retur`
--

INSERT INTO `penjualan_retur` (`id`, `tanggal`, `id_penjualan`, `keterangan`, `item_retur`, `status`, `verifikasi_by`, `input_by`, `timestamp_input`, `timestamp_verifikasi`, `id_kas`, `persetujuan`, `tgl_bayar`, `jumlah_bayar`, `jenis_pembayaran`, `total_tonase`) VALUES
(3, '2021-09-20', 12, 'Beras Kapangen', 'BK@25', 1, 1, 1, '2021-09-22 16:34:16', '2021-09-22 17:46:59', 1, 1, '2021-09-20', 2000000, 'cash', 1000),
(5, '2021-09-22', 23, 'Kemasan Bocor', 'Manna@5, MM@5, Berkat@5', 1, 1, 1, '2021-09-22 14:38:09', '2021-09-23 13:55:45', 2, 1, '0000-00-00', 2000000, 'cash', 100),
(6, '2021-09-23', 18, 'Kapangen', 'Manna@25', 1, 1, 1, '2021-09-23 11:43:38', '2021-09-23 11:44:12', 2, 1, '0000-00-00', 1000000, 'cash', 100),
(7, '2021-09-23', 18, 'Kapang kapang', 'Bakung@5', 1, 1, 1, '2021-09-23 12:07:37', '2021-09-23 12:07:55', 4, 1, '0000-00-00', 1000000, 'transfer', 100),
(8, '2021-09-23', 18, 'Bocor', 'Cek@1', 1, 1, 1, '2021-09-23 13:28:42', '2021-09-23 13:30:07', 4, 1, '0000-00-00', 1000001, 'transfer', 101),
(9, '2021-09-23', 18, 'Bocorr', 'Manna@25', 1, 1, 1, '2021-09-23 13:51:55', '2021-09-23 13:52:16', 4, 1, '0000-00-00', 1000002, 'transfer', 102),
(10, '2021-09-23', 18, 'Bocor', 'Beras', 1, 1, 1, '2021-09-23 14:05:44', '2021-09-23 14:09:32', 4, 1, '0000-00-00', 1000003, 'transfer', 103),
(11, '2021-09-23', 18, 'Bocor4', 'Beras4', 1, 1, 1, '2021-09-23 14:11:06', '2021-09-23 14:11:22', 4, 1, '0000-00-00', 1000004, 'transfer', 104),
(12, '2021-09-23', 18, 'Bocor5', 'Beras5', 1, 1, 1, '2021-09-23 14:16:47', '2021-09-23 14:17:07', 4, 1, '0000-00-00', 1000005, 'transfer', 105),
(14, '2021-09-23', 18, 'Bocor6', 'Beras6', 1, 1, 1, '2021-09-23 14:26:36', '2021-09-23 14:26:52', 5, 1, '0000-00-00', 1000006, 'transfer', 106);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_retur_detail`
--

CREATE TABLE `penjualan_retur_detail` (
  `id` int(11) NOT NULL,
  `id_retur` int(11) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_piutang` date NOT NULL,
  `jenis_piutang` enum('panjang','pendek','') NOT NULL,
  `nominal_piutang` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`id`, `id_customer`, `tgl_piutang`, `jenis_piutang`, `nominal_piutang`, `keterangan`, `is_paid_off`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, 5, '2020-02-23', 'panjang', 15000000, '', 0, 0, 1, '2020-02-26 18:34:50', 0, '0000-00-00 00:00:00'),
(2, 7, '2020-02-27', 'panjang', 50000000, '', 0, 0, 1, '2020-02-29 15:45:41', 0, '0000-00-00 00:00:00'),
(5, 14, '2021-07-01', 'pendek', 3127000, 'uang muka gabah', 1, 0, 1, '2021-07-07 23:53:54', 0, '0000-00-00 00:00:00'),
(6, 15, '2021-07-27', 'panjang', 15000000, 'Uang Muka Gabah', 0, 0, 1, '2021-08-06 14:33:28', 0, '0000-00-00 00:00:00'),
(7, 4, '2021-07-06', 'panjang', 15000000, 'Uang Muka', 1, 0, 1, '2021-08-10 10:02:03', 0, '0000-00-00 00:00:00'),
(8, 1, '2021-08-23', 'panjang', 35000000, '', 0, 0, 1, '2021-08-24 16:05:49', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `piutang_detail`
--

CREATE TABLE `piutang_detail` (
  `id` int(11) NOT NULL,
  `id_piutang` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `id_source` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tambah_piutang` bigint(20) NOT NULL,
  `bayar_pokok` bigint(20) NOT NULL,
  `bayar_bunga` bigint(20) NOT NULL,
  `pokok_awal` bigint(20) NOT NULL,
  `sisa_pokok` bigint(20) NOT NULL,
  `remark` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piutang_detail`
--

INSERT INTO `piutang_detail` (`id`, `id_piutang`, `source`, `id_source`, `tgl_transaksi`, `tambah_piutang`, `bayar_pokok`, `bayar_bunga`, `pokok_awal`, `sisa_pokok`, `remark`, `jenis_pembayaran`, `id_kas`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, 1, 'master-transaksi-uangmuka', 0, '2020-02-23', 15000000, 0, 0, 0, 15000000, 'transaksi awal', 'cash', 2, 1, 1, '2020-03-06 18:16:56', 1, '2020-03-02 19:51:17'),
(2, 2, 'master-transaksi-uangmuka', 0, '2020-02-27', 50000000, 0, 0, 0, 50000000, 'transaksi awal', 'transfer', 4, 1, 1, '2021-09-06 15:49:18', 1, '2021-09-06 15:49:18'),
(3, 1, 'master-transaksi-uangmuka', 0, '2020-03-01', 0, 5000000, 500000, 15000000, 10000000, 'cicilan pertama', 'cash', 2, 1, 1, '2020-03-05 10:55:36', 1, '2020-03-05 11:15:24'),
(4, 1, 'master-transaksi-uangmuka', 0, '2020-03-03', 0, 2000000, 100000, 10000000, 8000000, '', 'cash', 2, 1, 1, '2020-03-05 11:21:07', 1, '2020-03-05 14:05:07'),
(5, 1, 'master-transaksi-uangmuka', 0, '2020-03-04', 10000000, 0, 0, 8000000, 18000000, '', 'transfer', 4, 1, 1, '2020-03-05 13:15:27', 1, '2020-03-05 14:08:04'),
(6, 1, 'master-transaksi-uangmuka', 0, '2020-03-06', 0, 4000000, 400000, 18000000, 14000000, 'tes 123', 'cash', 1, 1, 1, '2020-11-23 18:19:19', 1, '2020-11-23 18:19:19'),
(7, 1, 'master-transaksi-uangmuka', 0, '2020-03-06', 9000000, 0, 0, 14000000, 23000000, 'test', 'transfer', 5, 1, 1, '2021-08-28 15:08:24', 1, '2021-08-28 15:08:24'),
(12, 4, 'master-transaksi-uangmuka', 0, '2021-07-01', 3127000, 0, 0, 0, 3127000, 'transaksi awal', 'transfer', 4, 1, 1, '2021-07-07 19:48:35', 1, '2021-07-07 19:48:36'),
(14, 5, 'master-transaksi-uangmuka', 0, '2021-07-01', 3127000, 0, 0, 0, 3127000, 'uang muka gabah', 'transfer', 4, 1, 1, '2021-07-08 00:04:34', 1, '2021-07-08 00:04:34'),
(16, 5, 'pembelian-gabah', 15, '2021-07-03', 0, 3127000, 0, 3127000, 0, 'Potong Bon PBG-014-001/07/03', 'transfer', 4, 1, 1, '2021-07-08 01:34:26', 1, '2021-07-08 01:34:26'),
(17, 6, 'master-transaksi-uangmuka', 0, '2021-07-27', 15000000, 0, 0, 0, 15000000, 'Uang Muka Gabah - transaksi awal', 'transfer', 4, 1, 1, '2021-08-06 14:34:04', 1, '2021-08-06 14:34:04'),
(18, 6, 'pembelian-gabah', 19, '2021-07-27', 0, 7747000, 0, 15000000, 7253000, 'Potong Bon - PBG-015-001/07/27', 'transfer', 4, 1, 1, '2021-08-06 17:14:37', 1, '2021-08-06 17:14:37'),
(19, 6, 'pembelian-gabah', 20, '2021-08-09', 0, 7253000, 0, 7253000, 0, 'Potong Bon - PBG-015-002/07/27', 'transfer', 4, 1, 1, '2021-08-09 16:23:00', 1, '2021-08-09 16:23:00'),
(20, 7, 'master-transaksi-uangmuka', 0, '2021-07-06', 15000000, 0, 0, 0, 15000000, 'Uang Muka - transaksi awal', 'transfer', 4, 1, 1, '2021-08-10 10:02:39', 1, '2021-08-10 10:02:39'),
(21, 7, 'pembelian-gabah', 21, '2021-08-10', 0, 5911000, 0, 13678000, 7767000, 'Potong Bon - PBG-004-001/08/10', 'transfer', 4, 1, 1, '2021-08-28 15:08:17', 1, '2021-08-28 15:08:17'),
(22, 8, 'master-transaksi-uangmuka', 0, '2021-08-23', 35000000, 0, 0, 0, 35000000, ' - transaksi awal', 'transfer', 4, 1, 1, '2021-08-24 16:06:10', 1, '2021-08-24 16:06:10'),
(23, 8, 'pembelian-gabah', 25, '2021-08-26', 0, 9740000, 0, 35000000, 25260000, 'Potong Bon - PBG-001-001/08/24', 'transfer', 4, 1, 1, '2021-08-26 14:35:30', 1, '2021-08-26 14:35:30'),
(24, 7, 'pembelian-bahan', 19, '2021-08-28', 0, 1322000, 0, 15000000, 13678000, 'Potong Bon - PBL-021/08/21', 'cash', 2, 1, 1, '2021-08-28 09:16:40', 1, '2021-08-28 09:16:40'),
(25, 8, 'pembelian-bahan', 22, '0000-00-00', 0, 5000000, 0, 25260000, 20260000, 'Potong Bon - PBL-024/08/21', 'tbd', 0, 1, 1, '2021-08-28 16:21:01', 1, '2021-08-28 16:21:01'),
(26, 7, 'pembelian-bahan', 23, '2021-08-28', 0, 200000, 0, 7767000, 7567000, 'Potong Bon - PBL-025/08/21', 'cash', 2, 1, 1, '2021-08-28 17:12:50', 1, '2021-08-28 17:12:50'),
(27, 8, 'pembelian-bahan', 28, '2021-09-01', 0, 1100000, 0, 20260000, 19160000, 'Potong Bon - PBN-031/08/21', 'cash', 2, 1, 1, '2021-09-01 12:16:54', 1, '2021-09-01 12:16:54'),
(28, 7, 'pembelian-gabah', 27, '2021-09-02', 0, 7567000, 0, 7567000, 0, 'Potong Bon - PBG-004-001/09/01', 'transfer', 4, 1, 1, '2021-09-02 09:58:41', 1, '2021-09-02 09:58:41'),
(29, 6, 'master-transaksi-uangmuka', 0, '2021-09-13', 12000000, 0, 0, 0, 12000000, '', 'transfer', 4, 1, 1, '2021-09-13 11:33:08', 1, '2021-09-13 11:33:08'),
(30, 6, 'pembelian-gabah', 29, '2021-09-13', 0, 7187222, 0, 12000000, 4812778, 'Potong Bon - PBG-015-001/09/13', 'cash', 2, 1, 1, '2021-09-14 11:24:10', 1, '2021-09-14 11:24:11'),
(31, 6, 'pembelian-bahan', 32, '2021-09-24', 0, 100000, 0, 0, 0, 'Potong UM - PBN-004/09/21', 'cash', 2, 0, 1, '2021-09-25 16:52:15', 0, '0000-00-00 00:00:00'),
(32, 6, 'pembelian-bahan', 36, '2021-09-27', 0, 100000, 0, 0, 0, 'Potong UM - BB-ADM-202109.005', 'cash', 2, 0, 1, '2021-09-27 14:25:19', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE `produksi` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `id_merk` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `jumlah_cor` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `persetujuan` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi`
--

INSERT INTO `produksi` (`id`, `kode`, `tanggal`, `id_merk`, `keterangan`, `jumlah_cor`, `status`, `persetujuan`, `input_by`, `timestamp_input`, `verif_by`, `timestamp_verif`) VALUES
(1, 'PRD-20201201-001', '2020-12-01', 1, 'manna ', 1.5, 1, 0, 1, '2020-12-02 18:31:12', 0, '0000-00-00 00:00:00'),
(2, 'PRD-20201205-001', '2020-12-05', 1, 'Test perhitungan manna ', 3.25, 1, 1, 1, '2020-12-05 16:38:44', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produksi_bahan`
--

CREATE TABLE `produksi_bahan` (
  `id` int(11) NOT NULL,
  `id_produksi` int(11) NOT NULL,
  `id_proses` int(11) NOT NULL,
  `id_bahan` int(11) NOT NULL,
  `nama_return` varchar(255) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi_bahan`
--

INSERT INTO `produksi_bahan` (`id`, `id_produksi`, `id_proses`, `id_bahan`, `nama_return`, `kuantitas`, `harga`, `input_by`, `timestamp`) VALUES
(2, 1, 7, 5, '', 2000, 8800, 1, '0000-00-00 00:00:00'),
(3, 1, 7, 7, '', 500, 7700, 1, '0000-00-00 00:00:00'),
(4, 1, 3, 1, '', 50, 7000, 1, '0000-00-00 00:00:00'),
(5, 2, 7, 5, '', 1000, 8960, 1, '2020-12-05 16:46:53'),
(6, 2, 7, 6, '', 450, 6500, 1, '2020-12-05 16:47:08'),
(7, 2, 7, 7, '', 150, 8300, 1, '2020-12-05 16:47:32'),
(8, 2, 3, 8, '', 447, 5800, 1, '2020-12-05 17:05:36'),
(9, 2, 5, 10, '', 32, 8500, 1, '2020-12-05 17:07:47'),
(10, 2, 6, 11, '', 339, 4500, 1, '2020-12-05 17:08:12');

-- --------------------------------------------------------

--
-- Table structure for table `produksi_hasil`
--

CREATE TABLE `produksi_hasil` (
  `id` int(11) NOT NULL,
  `id_produksi` int(11) NOT NULL,
  `kemasan` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `biaya_sak` int(11) NOT NULL,
  `biaya_transport` int(11) NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi_hasil`
--

INSERT INTO `produksi_hasil` (`id`, `id_produksi`, `kemasan`, `kuantitas`, `biaya_sak`, `biaya_transport`, `harga_pokok`, `input_by`, `timestamp`) VALUES
(2, 1, 25, 50, 140, 50, 0, 1, '2020-12-05 16:08:06'),
(3, 1, 10, 120, 200, 50, 0, 1, '2020-12-05 16:08:53'),
(5, 1, 10, 100, 200, 50, 0, 1, '2020-12-05 16:11:07'),
(6, 2, 25, 128, 140, 50, 9305, 1, '2020-12-05 17:09:43'),
(7, 2, 5, 200, 320, 50, 9485, 1, '2020-12-05 17:10:04');

-- --------------------------------------------------------

--
-- Table structure for table `produksi_proses`
--

CREATE TABLE `produksi_proses` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `is_multiply_cor` tinyint(4) NOT NULL,
  `is_return` tinyint(4) NOT NULL,
  `is_sisa_produksi` int(11) NOT NULL,
  `urutan` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produksi_proses`
--

INSERT INTO `produksi_proses` (`id`, `nama`, `arus`, `is_multiply_cor`, `is_return`, `is_sisa_produksi`, `urutan`, `status`) VALUES
(1, 'Tambahan', 'in', 0, 0, 0, 2, 1),
(2, 'Return', 'in', 0, 1, 0, 3, 1),
(3, 'Ayak I', 'out', 0, 0, 0, 4, 1),
(4, 'Ayak II', 'out', 0, 0, 0, 5, 1),
(5, 'TAP', 'out', 0, 0, 1, 6, 1),
(6, 'BB', 'out', 0, 0, 1, 7, 1),
(7, 'Bahan Baku', 'in', 1, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stok_bahan`
--

CREATE TABLE `stok_bahan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `sumber` enum('master-transaksi','hasil-giling','produksi','pembelian-bahan') NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_jenis_bahan` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `broken` float NOT NULL,
  `harga_kepala` float NOT NULL,
  `harga_broken` float NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_bahan`
--

INSERT INTO `stok_bahan` (`id`, `tanggal`, `sumber`, `id_sumber`, `id_supplier`, `id_jenis_bahan`, `stok`, `stok_sisa`, `harga_satuan`, `broken`, `harga_kepala`, `harga_broken`, `input_by`, `timestamp_input`) VALUES
(1, '2020-06-01', 'master-transaksi', 0, 0, 6, 5000, 3538, 6200, 0, 0, 0, 1, '2020-06-02 15:48:25'),
(2, '2020-06-02', 'master-transaksi', 0, 0, 5, 7000, 3750, 9500, 0, 0, 0, 1, '2020-06-02 15:48:42'),
(3, '2020-06-02', 'master-transaksi', 0, 0, 5, 3300, 3300, 9900, 0, 0, 0, 1, '2020-06-02 16:39:43'),
(5, '2020-06-11', 'hasil-giling', 3, 0, 1, 1564, 1564, 8868, 20, 9200, 7540, 1, '2020-06-13 16:10:39'),
(6, '2020-06-11', 'hasil-giling', 3, 0, 2, 23, 23, 0, 0, 0, 0, 1, '2020-06-13 16:10:39'),
(55, '2021-08-24', 'pembelian-bahan', 19, 4, 11, 200, 200, 2000, 0, 0, 0, 1, '2021-08-28 09:14:53'),
(56, '2021-08-24', 'pembelian-bahan', 19, 4, 7, 230, 230, 4000, 0, 0, 0, 1, '2021-08-28 09:14:53'),
(57, '2021-08-28', 'pembelian-bahan', 22, 1, 6, 22222, 22222, 200, 0, 0, 0, 1, '2021-08-28 15:43:43'),
(58, '2021-08-28', 'pembelian-bahan', 21, 4, 11, 600, 600, 8000, 0, 0, 0, 1, '2021-08-28 16:23:51'),
(59, '2021-08-28', 'pembelian-bahan', 23, 4, 7, 400, 400, 3000, 0, 0, 0, 1, '2021-08-28 17:12:28'),
(60, '2021-08-30', 'pembelian-bahan', 25, 15, 11, 400, 400, 3000, 0, 0, 0, 1, '2021-08-31 10:20:12'),
(61, '2021-08-31', 'pembelian-bahan', 26, 15, 8, 1000, 1000, 1000, 0, 0, 0, 1, '2021-08-31 10:23:01'),
(62, '2021-08-31', 'pembelian-bahan', 27, 15, 5, 2344, 2344, 3422, 0, 0, 0, 1, '2021-08-31 11:50:51'),
(63, '2021-08-31', 'pembelian-bahan', 28, 1, 9, 332, 332, 3344, 0, 0, 0, 1, '2021-09-01 12:16:28'),
(64, '2021-09-04', 'pembelian-bahan', 30, 15, 4, 1200, 1200, 5000, 0, 0, 0, 1, '2021-09-04 12:59:48'),
(65, '2021-09-07', 'master-transaksi', 0, 0, 2, 0, 400, 2300, 0, 0, 0, 1, '2021-09-07 09:39:22'),
(66, '2021-09-24', 'pembelian-bahan', 32, 15, 8, 2000, 2000, 4000, 0, 0, 0, 1, '2021-09-25 16:52:15'),
(67, '2021-09-27', 'pembelian-bahan', 36, 15, 8, 100, 100, 1000, 0, 0, 0, 1, '2021-09-27 14:25:20');

-- --------------------------------------------------------

--
-- Table structure for table `stok_bahan_old`
--

CREATE TABLE `stok_bahan_old` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_jenis_bahan` int(11) NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `sumber` enum('master-transaksi','hasil-giling','produksi') NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `broken` float NOT NULL,
  `harga_kepala` float NOT NULL,
  `harga_broken` float NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stok_bahan_trx`
--

CREATE TABLE `stok_bahan_trx` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_stok` int(11) NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `sumber` varchar(255) NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_bahan_trx`
--

INSERT INTO `stok_bahan_trx` (`id`, `tgl`, `id_stok`, `arus`, `kuantitas`, `harga`, `sumber`, `id_sumber`, `timestamp`) VALUES
(4, '2020-06-01', 1, 'in', 5000, 6200, 'master-transaksi', 0, '2020-06-02 15:48:25'),
(5, '2020-06-02', 2, 'in', 7000, 9500, 'master-transaksi', 0, '2020-06-02 15:48:42'),
(6, '2020-06-02', 3, 'in', 3300, 9900, 'master-transaksi', 0, '2020-06-02 16:39:43'),
(7, '2020-06-11', 5, 'in', 1564, 8868, 'hasil-giling', 3, '2020-06-13 16:10:39'),
(8, '2020-06-11', 6, 'in', 23, 0, 'hasil-giling', 3, '2020-06-13 16:10:39'),
(65, '2021-08-24', 55, 'in', 200, 2000, 'pembelian-bahan', 0, '2021-08-28 09:14:53'),
(66, '2021-08-24', 56, 'in', 230, 4000, 'pembelian-bahan', 0, '2021-08-28 09:14:53'),
(67, '2021-08-28', 57, 'in', 22222, 200, 'pembelian-bahan', 0, '2021-08-28 15:43:43'),
(68, '2021-08-28', 58, 'in', 600, 8000, 'pembelian-bahan', 0, '2021-08-28 16:23:51'),
(69, '2021-08-28', 59, 'in', 400, 3000, 'pembelian-bahan', 0, '2021-08-28 17:12:28'),
(70, '2021-08-30', 60, 'in', 400, 3000, 'pembelian-bahan', 0, '2021-08-31 10:20:12'),
(71, '2021-08-31', 61, 'in', 1000, 1000, 'pembelian-bahan', 0, '2021-08-31 10:23:01'),
(72, '2021-08-31', 62, 'in', 2344, 3422, 'pembelian-bahan', 0, '2021-08-31 11:50:51'),
(73, '2021-08-31', 63, 'in', 332, 3344, 'pembelian-bahan', 0, '2021-09-01 12:16:28'),
(74, '2021-09-04', 64, 'in', 1200, 5000, 'pembelian-bahan', 0, '2021-09-04 12:59:48'),
(75, '2021-09-07', 65, 'in', 0, 2300, 'master-transaksi', 0, '2021-09-07 09:39:22'),
(76, '2021-09-24', 66, 'in', 2000, 4000, 'pembelian-bahan', 0, '2021-09-25 16:52:15'),
(77, '2021-09-27', 67, 'in', 100, 1000, 'pembelian-bahan', 0, '2021-09-27 14:25:20');

-- --------------------------------------------------------

--
-- Table structure for table `stok_beras`
--

CREATE TABLE `stok_beras` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `sumber` enum('master-transaksi','produksi','beli-beras','') NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_beras`
--

INSERT INTO `stok_beras` (`id`, `tanggal`, `sumber`, `id_sumber`, `id_kemasan`, `stok`, `stok_sisa`, `harga_pokok`, `input_by`, `timestamp_input`) VALUES
(2, '2020-06-24', 'master-transaksi', 0, 2, 50, 18, 12000, 1, '2020-06-24 17:00:09'),
(3, '2020-06-23', 'master-transaksi', 0, 1, 200, 138, 10900, 1, '2020-06-25 10:21:21'),
(4, '2020-06-23', 'master-transaksi', 0, 3, 800, 412, 11100, 1, '2020-06-25 10:22:01'),
(5, '2020-09-15', 'master-transaksi', 0, 11, 250, 0, 8900, 1, '2020-09-16 17:19:57'),
(6, '2020-12-05', 'produksi', 2, 2, 128, 128, 9305, 1, '2020-12-15 17:48:11'),
(7, '2020-12-05', 'produksi', 2, 3, 200, 200, 9485, 1, '2020-12-15 17:48:12'),
(8, '2021-08-20', 'master-transaksi', 0, 4, 2000, 272, 12300, 1, '2021-08-21 11:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `stok_beras_trx`
--

CREATE TABLE `stok_beras_trx` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_stok` int(11) NOT NULL,
  `arus` enum('in','out','') NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `sumber` varchar(255) NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_beras_trx`
--

INSERT INTO `stok_beras_trx` (`id`, `tgl`, `id_stok`, `arus`, `kuantitas`, `harga`, `sumber`, `id_sumber`, `timestamp`) VALUES
(3, '2020-06-24', 2, 'in', 50, 12000, 'master-transaksi', 0, '2020-06-24 17:00:09'),
(4, '2020-06-24', 2, 'out', 20, 12000, 'master-transaksi', 0, '2020-06-24 17:00:09'),
(5, '2020-06-23', 3, 'in', 200, 10900, 'master-transaksi', 0, '2020-06-25 10:21:21'),
(6, '2020-06-23', 4, 'in', 800, 11100, 'master-transaksi', 0, '2020-06-25 10:22:01'),
(9, '2020-09-15', 5, 'in', 250, 8900, 'master-transaksi', 0, '2020-09-16 17:19:57'),
(10, '2020-09-15', 1, 'out', 5, 10500, 'penjualan-beras', 5, '2020-09-16 17:20:43'),
(11, '2020-09-15', 1, 'out', 1, 10500, 'penjualan-beras', 5, '2020-09-16 17:20:43'),
(12, '2020-09-15', 5, 'out', 2, 8900, 'penjualan-beras', 5, '2020-09-16 17:20:43'),
(13, '2020-12-05', 6, 'in', 128, 9305, 'produksi', 2, '2020-12-15 17:48:12'),
(14, '2020-12-05', 7, 'in', 200, 9485, 'produksi', 2, '2020-12-15 17:48:12'),
(15, '2021-08-20', 8, 'in', 2000, 12300, 'master-transaksi', 0, '2021-08-21 11:56:47'),
(16, '2020-10-24', 1, 'out', 1, 10500, 'penjualan-beras', 9, '2021-09-03 14:53:33'),
(17, '2021-09-11', 4, 'out', 100, 11100, 'penjualan-beras', 12, '2021-09-11 15:45:05'),
(18, '2021-09-11', 4, 'out', 200, 11100, 'penjualan-beras', 12, '2021-09-11 15:45:05'),
(19, '2020-07-10', 5, 'out', 50, 8900, 'penjualan-beras', 1, '2021-09-14 11:00:17'),
(20, '2020-07-10', 8, 'out', 10, 12300, 'penjualan-beras', 1, '2021-09-14 11:00:17'),
(21, '2020-07-10', 4, 'out', 60, 11100, 'penjualan-beras', 1, '2021-09-14 11:00:17'),
(22, '2020-07-10', 3, 'out', 25, 10900, 'penjualan-beras', 1, '2021-09-14 11:00:17'),
(23, '0000-00-00', 4, 'out', 6, 11100, 'penjualan-beras', 11, '2021-09-14 11:08:34'),
(24, '0000-00-00', 5, 'out', 3, 8900, 'penjualan-beras', 11, '2021-09-14 11:08:34'),
(25, '0000-00-00', 8, 'out', 8, 12300, 'penjualan-beras', 10, '2021-09-14 11:09:28'),
(26, '2021-09-15', 8, 'out', 10, 12300, 'penjualan-beras', 15, '2021-09-15 14:43:18'),
(27, '0000-00-00', 4, 'out', 10, 11100, 'penjualan-beras', 20, '2021-09-15 16:49:26'),
(28, '0000-00-00', 3, 'out', 15, 10900, 'penjualan-beras', 20, '2021-09-15 16:49:26'),
(29, '0000-00-00', 2, 'out', 20, 12000, 'penjualan-beras', 20, '2021-09-15 16:49:26'),
(30, '2021-09-16', 5, 'out', 12, 8900, 'penjualan-beras', 23, '2021-09-17 15:12:39'),
(31, '2021-09-18', 5, 'out', 21, 8900, 'penjualan-beras', 17, '2021-09-18 16:17:41'),
(32, '2021-09-17', 2, 'out', 12, 12000, 'penjualan-beras', 18, '2021-09-23 09:08:59'),
(33, '2021-09-26', 5, 'out', 162, 8900, 'penjualan-beras', 28, '2021-09-27 16:21:29'),
(34, '2021-09-26', 3, 'out', 22, 10900, 'penjualan-beras', 28, '2021-09-27 16:21:30'),
(35, '2021-09-25', 4, 'out', 12, 11100, 'penjualan-beras', 27, '2021-09-27 16:22:51');

-- --------------------------------------------------------

--
-- Table structure for table `stok_gabah`
--

CREATE TABLE `stok_gabah` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `sumber` enum('pembelian-gabah','master-transaksi','hasil-dryer','hasil-jemur') NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `jenis_gabah` varchar(100) NOT NULL,
  `id_jenis_padi` int(11) NOT NULL,
  `jenis_pengeringan` varchar(100) NOT NULL,
  `jenis_proses` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_gabah`
--

INSERT INTO `stok_gabah` (`id`, `tanggal`, `sumber`, `id_sumber`, `id_supplier`, `jenis_gabah`, `id_jenis_padi`, `jenis_pengeringan`, `jenis_proses`, `stok`, `stok_sisa`, `harga_satuan`, `input_by`, `timestamp_input`) VALUES
(6, '2020-04-27', 'master-transaksi', 0, 0, 'kg', 1, '', 'tapel', 3500, 3500, 7500, 1, '2020-05-06 14:00:39'),
(22, '2019-10-15', 'pembelian-gabah', 1, 1, 'ks', 1, 'jemur', '', 6329, 5329, 6993, 1, '2020-05-06 13:51:24'),
(23, '2019-10-15', 'pembelian-gabah', 1, 1, 'kg', 1, '', 'tapel', 1170, 1170, 9000, 1, '2020-05-06 13:51:24'),
(24, '2019-10-15', 'pembelian-gabah', 1, 1, 'kg', 2, '', 'tapel', 319, 0, 9400, 1, '2020-05-06 13:51:24'),
(25, '2019-10-15', 'pembelian-gabah', 1, 1, 'ks', 2, 'jemur', '', 532, 532, 8000, 1, '2020-05-06 13:51:24'),
(26, '2019-10-15', 'pembelian-gabah', 1, 1, 'ks', 3, 'jemur', '', 1596, 1096, 8000, 1, '2020-05-06 13:51:24'),
(27, '2020-05-12', 'master-transaksi', 0, 0, 'ks', 1, 'dryer', '', 4000, 4000, 6000, 1, '2020-05-12 17:30:11'),
(28, '2020-05-18', 'master-transaksi', 0, 0, 'ks', 1, 'dryer', '', 5000, 0, 6000, 1, '2020-05-19 14:46:38'),
(29, '2020-05-18', 'master-transaksi', 0, 0, 'ks', 4, 'dryer', '', 3000, -3000, 6500, 1, '2020-05-19 14:47:06'),
(30, '2020-05-18', 'hasil-jemur', 1, 0, 'kg', 2, '', 'tapel', 1200, 0, 9161, 1, '2020-06-01 00:13:59'),
(31, '2020-06-10', 'hasil-dryer', 6, 0, 'kg', 1, '', 'tapel', 6800, 6800, 7280, 1, '2020-06-10 13:32:13'),
(32, '2020-05-20', 'hasil-dryer', 3, 0, 'kg', 1, '', 'giling', 2550, 0, 7647, 1, '2020-06-10 13:32:20'),
(33, '2020-06-10', 'pembelian-gabah', 12, 6, 'ks', 1, 'dryer', '', 2667, 0, 5200, 1, '2020-06-10 14:50:26'),
(34, '2020-06-10', 'hasil-dryer', 7, 0, 'kg', 1, '', 'giling', 2267, -4534, 6118, 1, '2020-06-10 14:56:40'),
(35, '2020-01-14', 'pembelian-gabah', 10, 8, 'ks', 1, 'jemur', '', 2567, 2567, 7000, 1, '2020-06-12 17:19:50'),
(36, '2020-01-14', 'pembelian-gabah', 10, 8, 'ks', 1, 'dryer', '', 3462, 3462, 7000, 1, '2020-06-12 17:19:50'),
(41, '2020-01-14', 'pembelian-gabah', 11, 5, 'ks', 1, 'jemur', '', 4680, 4680, 8400, 1, '2020-11-19 19:29:44'),
(42, '2020-01-14', 'pembelian-gabah', 11, 5, 'ks', 1, 'dryer', '', 6780, 6780, 8300, 1, '2020-11-19 19:29:44'),
(43, '2020-01-06', 'pembelian-gabah', 8, 5, 'ks', 1, 'dryer', '', 4148, 4148, 7600, 1, '2020-11-23 16:58:00'),
(44, '2020-01-06', 'pembelian-gabah', 8, 5, 'ks', 2, 'jemur', '', 1045, 1045, 7500, 1, '2020-11-23 16:58:00'),
(45, '2020-01-06', 'pembelian-gabah', 8, 5, 'ks', 4, 'jemur', '', 300, 300, 7400, 1, '2020-11-23 16:58:00'),
(46, '2020-01-06', 'pembelian-gabah', 8, 5, 'kg', 1, '', '', 50, 50, 7000, 1, '2020-11-23 16:58:00'),
(47, '2021-07-03', 'pembelian-gabah', 15, 14, 'ks', 6, 'dryer', '', 917, 917, 4100, 1, '2021-07-07 17:23:02'),
(48, '2019-12-13', 'pembelian-gabah', 5, 5, 'ks', 1, 'jemur', '', 2201, 2201, 5400, 1, '2021-07-08 15:40:34'),
(49, '2019-12-13', 'pembelian-gabah', 5, 5, 'ks', 1, 'dryer', '', 3940, 3940, 5322, 1, '2021-07-08 15:40:34'),
(50, '2019-12-13', 'pembelian-gabah', 5, 5, 'ks', 2, 'dryer', '', 292, 292, 5500, 1, '2021-07-08 15:40:34'),
(51, '2021-07-27', 'pembelian-gabah', 19, 15, 'ks', 1, 'dryer', '', 1666, 1666, 4649, 1, '2021-08-06 17:09:05'),
(52, '2021-07-27', 'pembelian-gabah', 20, 15, 'ks', 7, 'dryer', '', 727, 727, 4699, 1, '2021-08-09 16:08:45'),
(53, '2021-07-27', 'pembelian-gabah', 20, 15, 'ks', 1, 'dryer', '', 1097, 1097, 4649, 1, '2021-08-09 16:08:45'),
(54, '2021-08-10', 'pembelian-gabah', 21, 4, 'ks', 1, 'dryer', '', 1344, 1344, 4399, 1, '2021-08-10 15:19:25'),
(55, '2021-06-01', 'pembelian-gabah', 13, 13, 'ks', 5, 'dryer', '', 1512, 1512, 4951, 1, '2021-08-12 12:23:39'),
(56, '2021-04-17', 'pembelian-gabah', 14, 5, 'ks', 1, 'dryer', '', 470, 470, 4400, 1, '2021-08-12 12:24:07'),
(57, '2021-08-03', 'pembelian-gabah', 23, 4, 'ks', 6, 'dryer', '', 1702, 1702, 3956, 1, '2021-08-13 15:27:08'),
(58, '2021-08-03', 'pembelian-gabah', 23, 4, 'kg', 1, '', 'giling', 114, 114, 5050, 1, '2021-08-13 15:27:08'),
(59, '2021-08-14', 'pembelian-gabah', 24, 4, 'kg', 1, '', 'giling', 3032, 3032, 4000, 1, '2021-08-14 16:12:00'),
(60, '2021-08-14', 'pembelian-gabah', 24, 4, 'ks', 7, 'jemur', '', 308, 308, 3893, 1, '2021-08-14 16:12:00'),
(61, '2021-08-21', 'master-transaksi', 0, 0, 'kg', 1, '', 'giling', 3000, 3200, 4500, 1, '2021-08-21 14:58:03'),
(62, '2021-08-24', 'pembelian-gabah', 25, 1, 'ks', 5, 'dryer', '', 3247, 3247, 3000, 1, '2021-08-26 14:33:52'),
(63, '2021-08-31', 'pembelian-gabah', 26, 15, 'ks', 6, 'jemur', '', 1505, 1505, 433, 1, '2021-08-31 11:56:42'),
(64, '2021-08-31', 'pembelian-gabah', 26, 15, 'ks', 5, 'jemur', '', 974, 974, 233, 1, '2021-08-31 11:56:42'),
(65, '2021-09-01', 'pembelian-gabah', 27, 4, 'ks', 7, 'jemur', '', 3275, 3275, 3400, 1, '2021-09-02 09:46:45'),
(66, '2021-09-07', 'master-transaksi', 0, 0, 'ks', 4, 'dryer', '', 1000, 1000, 4600, 1, '2021-09-07 16:46:04'),
(67, '2021-09-11', 'pembelian-gabah', 28, 6, 'ks', 6, 'jemur', '', 2090, 2090, 4498, 1, '2021-09-11 17:12:40'),
(68, '2021-09-13', 'pembelian-gabah', 29, 15, 'ks', 4, 'jemur', '', 1358, 1358, 2999, 1, '2021-09-13 14:31:28'),
(69, '2021-09-13', 'pembelian-gabah', 29, 15, 'ks', 7, 'dryer', '', 776, 776, 4000, 1, '2021-09-13 14:31:28'),
(70, '2021-09-13', 'pembelian-gabah', 29, 15, 'kg', 7, '', 'giling', 118, 118, 5000, 1, '2021-09-13 14:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `stok_gabah_trx`
--

CREATE TABLE `stok_gabah_trx` (
  `id` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_stok` int(11) NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `sumber` varchar(255) NOT NULL,
  `id_sumber` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_gabah_trx`
--

INSERT INTO `stok_gabah_trx` (`id`, `tgl`, `id_stok`, `arus`, `kuantitas`, `harga`, `sumber`, `id_sumber`, `timestamp`) VALUES
(11, '2019-10-15', 22, 'in', 6329, 6993, 'pembelian-gabah', 0, '2020-05-06 13:51:24'),
(12, '2019-10-15', 23, 'in', 1170, 9000, 'pembelian-gabah', 0, '2020-05-06 13:51:24'),
(13, '2019-10-15', 24, 'in', 319, 9400, 'pembelian-gabah', 0, '2020-05-06 13:51:24'),
(14, '2019-10-15', 25, 'in', 532, 8000, 'pembelian-gabah', 0, '2020-05-06 13:51:25'),
(15, '2019-10-15', 26, 'in', 1596, 8000, 'pembelian-gabah', 0, '2020-05-06 13:51:25'),
(16, '2020-04-27', 6, 'in', 3500, 7500, 'master-transaksi', 0, '2020-05-06 14:00:39'),
(17, '2020-05-12', 27, 'in', 4000, 6000, 'master-transaksi', 0, '2020-05-12 17:30:11'),
(18, '2020-05-18', 28, 'in', 5000, 6000, 'master-transaksi', 0, '2020-05-19 14:46:38'),
(19, '2020-05-18', 29, 'in', 3000, 6500, 'master-transaksi', 0, '2020-05-19 14:47:06'),
(20, '2020-05-18', 22, 'out', 1000, 6993, 'pengeringan', 1, '2020-06-01 00:13:58'),
(21, '2020-05-18', 26, 'out', 500, 8000, 'pengeringan', 1, '2020-06-01 00:13:59'),
(22, '2020-05-18', 30, 'in', 1200, 9161, 'pengeringan', 1, '2020-06-01 00:13:59'),
(23, '2020-06-10', 28, 'out', 5000, 6000, 'hasil-dryer', 6, '2020-06-10 13:32:11'),
(24, '2020-06-10', 29, 'out', 3000, 6500, 'hasil-dryer', 6, '2020-06-10 13:32:13'),
(25, '2020-06-10', 31, 'in', 6800, 7280, 'hasil-dryer', 6, '2020-06-10 13:32:13'),
(27, '2020-05-20', 32, 'in', 2550, 7647, 'hasil-dryer', 3, '2020-06-10 13:32:20'),
(28, '2020-06-10', 33, 'in', 2667, 5200, 'pembelian-gabah', 0, '2020-06-10 14:50:26'),
(29, '2020-06-10', 33, 'out', 2667, 5200, 'hasil-dryer', 7, '2020-06-10 14:56:40'),
(30, '2020-06-10', 34, 'in', 2267, 6118, 'hasil-dryer', 7, '2020-06-10 14:56:40'),
(31, '2020-01-14', 35, 'in', 2567, 7000, 'pembelian-gabah', 0, '2020-06-12 17:19:50'),
(32, '2020-01-14', 36, 'in', 3462, 7000, 'pembelian-gabah', 0, '2020-06-12 17:19:50'),
(34, '2020-06-11', 34, 'out', 2267, 6118, 'hasil-giling', 3, '2020-06-13 16:10:38'),
(35, '2019-12-13', 37, 'in', 2949, 5400, 'pembelian-gabah', 0, '2020-11-19 19:26:56'),
(36, '2019-12-13', 38, 'in', 3600, 8400, 'pembelian-gabah', 0, '2020-11-19 19:26:56'),
(37, '2020-01-14', 39, 'in', 4680, 8400, 'pembelian-gabah', 0, '2020-11-19 19:29:02'),
(38, '2020-01-14', 40, 'in', 6780, 8300, 'pembelian-gabah', 0, '2020-11-19 19:29:03'),
(39, '2020-01-14', 41, 'in', 4680, 8400, 'pembelian-gabah', 0, '2020-11-19 19:29:44'),
(40, '2020-01-14', 42, 'in', 6780, 8300, 'pembelian-gabah', 0, '2020-11-19 19:29:44'),
(41, '2020-01-06', 43, 'in', 4148, 7600, 'pembelian-gabah', 0, '2020-11-23 16:58:00'),
(42, '2020-01-06', 44, 'in', 1045, 7500, 'pembelian-gabah', 0, '2020-11-23 16:58:00'),
(43, '2020-01-06', 45, 'in', 300, 7400, 'pembelian-gabah', 0, '2020-11-23 16:58:00'),
(44, '2020-01-06', 46, 'in', 50, 7000, 'pembelian-gabah', 0, '2020-11-23 16:58:00'),
(45, '2021-07-03', 47, 'in', 917, 4100, 'pembelian-gabah', 0, '2021-07-07 17:23:02'),
(46, '2019-12-13', 48, 'in', 2201, 5400, 'pembelian-gabah', 0, '2021-07-08 15:40:34'),
(47, '2019-12-13', 49, 'in', 3940, 5322, 'pembelian-gabah', 0, '2021-07-08 15:40:34'),
(48, '2019-12-13', 50, 'in', 292, 5500, 'pembelian-gabah', 0, '2021-07-08 15:40:35'),
(49, '2021-07-27', 51, 'in', 1666, 4649, 'pembelian-gabah', 0, '2021-08-06 17:09:05'),
(50, '2021-07-27', 52, 'in', 727, 4699, 'pembelian-gabah', 0, '2021-08-09 16:08:45'),
(51, '2021-07-27', 53, 'in', 1097, 4649, 'pembelian-gabah', 0, '2021-08-09 16:08:45'),
(52, '2021-08-10', 54, 'in', 1344, 4399, 'pembelian-gabah', 0, '2021-08-10 15:19:25'),
(53, '2021-06-01', 55, 'in', 1512, 4951, 'pembelian-gabah', 0, '2021-08-12 12:23:39'),
(54, '2021-04-17', 56, 'in', 470, 4400, 'pembelian-gabah', 0, '2021-08-12 12:24:07'),
(55, '2021-08-03', 57, 'in', 1702, 3956, 'pembelian-gabah', 0, '2021-08-13 15:27:08'),
(56, '2021-08-03', 58, 'in', 114, 5050, 'pembelian-gabah', 0, '2021-08-13 15:27:08'),
(57, '2021-08-14', 59, 'in', 3032, 4000, 'pembelian-gabah', 0, '2021-08-14 16:12:00'),
(58, '2021-08-14', 60, 'in', 308, 3893, 'pembelian-gabah', 0, '2021-08-14 16:12:00'),
(59, '2020-06-11', 32, 'out', 2550, 7647, 'hasil-giling', 2, '2021-08-20 14:55:02'),
(60, '2020-06-11', 30, 'out', 1200, 9161, 'hasil-giling', 2, '2021-08-20 14:55:02'),
(61, '2020-06-11', 24, 'out', 319, 9400, 'hasil-giling', 2, '2021-08-20 14:55:02'),
(62, '2021-08-21', 61, 'in', 3000, 4500, 'master-transaksi', 0, '2021-08-21 14:58:03'),
(63, '2021-08-24', 62, 'in', 3247, 3000, 'pembelian-gabah', 0, '2021-08-26 14:33:52'),
(64, '2021-08-31', 63, 'in', 1505, 433, 'pembelian-gabah', 0, '2021-08-31 11:56:42'),
(65, '2021-08-31', 64, 'in', 974, 233, 'pembelian-gabah', 0, '2021-08-31 11:56:42'),
(66, '2021-09-01', 65, 'in', 3275, 3400, 'pembelian-gabah', 0, '2021-09-02 09:46:45'),
(67, '2021-09-07', 66, 'in', 1000, 4600, 'master-transaksi', 0, '2021-09-07 16:46:04'),
(68, '2021-09-11', 67, 'in', 2090, 4498, 'pembelian-gabah', 0, '2021-09-11 17:12:40'),
(69, '2021-09-13', 68, 'in', 1358, 2999, 'pembelian-gabah', 0, '2021-09-13 14:31:28'),
(70, '2021-09-13', 69, 'in', 776, 4000, 'pembelian-gabah', 0, '2021-09-13 14:31:28'),
(71, '2021-09-13', 70, 'in', 118, 5000, 'pembelian-gabah', 0, '2021-09-13 14:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_sak`
--

CREATE TABLE `transaksi_sak` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(50) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `source` varchar(255) NOT NULL,
  `id_source` int(11) NOT NULL,
  `id_customer_from` int(11) NOT NULL,
  `id_customer_to` int(11) NOT NULL,
  `is_record_from` tinyint(1) NOT NULL DEFAULT 1,
  `is_record_to` tinyint(1) NOT NULL DEFAULT 1,
  `arus` enum('in','out','') NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 = wait, 2 = acc, 3 = declined',
  `input_by` int(11) NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_sak`
--

INSERT INTO `transaksi_sak` (`id`, `kode_transaksi`, `tgl_transaksi`, `source`, `id_source`, `id_customer_from`, `id_customer_to`, `is_record_from`, `is_record_to`, `arus`, `is_verifikasi`, `input_by`, `verifikasi_by`, `timestamp_input`, `timestamp_verifikasi`) VALUES
(1, 'SAK.191213.001', '2019-12-13', 'pembelian-gabah', 5, 5, 9, 1, 1, 'in', 1, 1, 0, '2020-02-17 13:34:24', '0000-00-00 00:00:00'),
(5, 'SAK.200201.001', '2020-02-01', 'master-transaksi', 0, 5, 9, 1, 1, 'in', 1, 1, 0, '2020-02-21 16:57:38', '0000-00-00 00:00:00'),
(6, 'SAK.200210.001', '2020-02-10', 'master-transaksi', 0, 9, 5, 1, 1, 'out', 1, 1, 0, '2020-02-21 18:43:25', '0000-00-00 00:00:00'),
(12, 'SAK.200219.001', '2020-02-19', 'master-transaksi', 0, 5, 9, 0, 1, 'in', 1, 1, 0, '2020-02-21 18:44:03', '0000-00-00 00:00:00'),
(13, 'SAK.200610.001', '2020-06-10', 'pembelian-gabah', 12, 6, 9, 1, 1, 'in', 0, 1, 0, '2020-06-10 14:46:27', '0000-00-00 00:00:00'),
(14, 'SAK.210601.001', '2021-06-01', 'pembelian-gabah', 13, 13, 9, 1, 1, 'in', 0, 1, 0, '2021-06-04 16:19:53', '0000-00-00 00:00:00'),
(15, 'SAK.210417.001', '2021-04-17', 'pembelian-gabah', 14, 5, 9, 1, 1, 'in', 0, 1, 0, '2021-06-07 16:31:46', '0000-00-00 00:00:00'),
(16, 'SAK.210703.001', '2021-07-03', 'pembelian-gabah', 15, 14, 9, 1, 1, 'in', 0, 1, 0, '2021-07-07 17:21:49', '0000-00-00 00:00:00'),
(17, 'SAK.210806.001', '2021-08-06', 'pembelian-gabah', 18, 8, 9, 1, 1, 'in', 0, 1, 0, '2021-08-06 13:33:26', '0000-00-00 00:00:00'),
(18, 'SAK.210727.001', '2021-07-27', 'pembelian-gabah', 19, 15, 9, 1, 1, 'in', 0, 1, 0, '2021-08-06 16:59:03', '0000-00-00 00:00:00'),
(19, 'SAK.210727.002', '2021-07-27', 'pembelian-gabah', 20, 15, 9, 1, 1, 'in', 0, 1, 0, '2021-08-09 16:00:48', '0000-00-00 00:00:00'),
(20, 'SAK.210810.001', '2021-08-10', 'pembelian-gabah', 21, 4, 9, 1, 1, 'in', 0, 1, 0, '2021-08-10 10:42:31', '0000-00-00 00:00:00'),
(21, 'SAK.210803.001', '2021-08-03', 'pembelian-gabah', 23, 4, 9, 1, 1, 'in', 0, 1, 0, '2021-08-13 15:20:33', '0000-00-00 00:00:00'),
(22, 'SAK.210814.001', '2021-08-14', 'pembelian-gabah', 24, 4, 9, 1, 1, 'in', 0, 1, 0, '2021-08-14 15:39:25', '0000-00-00 00:00:00'),
(23, 'SAK.210824.001', '2021-08-24', 'pembelian-gabah', 25, 1, 9, 1, 1, 'in', 0, 1, 0, '2021-08-24 16:08:41', '0000-00-00 00:00:00'),
(24, 'SAK.210831.001', '2021-08-31', 'pembelian-gabah', 26, 15, 9, 1, 1, 'in', 0, 1, 0, '2021-08-31 11:54:14', '0000-00-00 00:00:00'),
(25, 'SAK.210901.001', '2021-09-01', 'pembelian-gabah', 27, 4, 9, 1, 1, 'in', 0, 1, 0, '2021-09-01 16:41:36', '0000-00-00 00:00:00'),
(26, 'SAK.210910.001', '2021-09-10', 'master-transaksi', 0, 15, 6, 1, 1, 'out', 1, 1, 0, '2021-09-10 15:09:32', '0000-00-00 00:00:00'),
(27, 'SAK.210910.002', '2021-09-10', 'master-transaksi', 0, 6, 15, 1, 1, 'out', 1, 1, 0, '2021-09-10 15:08:08', '0000-00-00 00:00:00'),
(28, 'SAK.210909.001', '2021-09-09', 'master-transaksi', 0, 8, 14, 0, 0, 'out', 1, 1, 0, '2021-09-10 15:10:50', '0000-00-00 00:00:00'),
(29, 'SAK.210911.001', '2021-09-11', 'pembelian-gabah', 28, 6, 9, 1, 1, 'in', 0, 1, 0, '2021-09-11 17:10:07', '0000-00-00 00:00:00'),
(30, 'SAK.210904.001', '2021-09-04', 'master-transaksi', 0, 15, 13, 1, 1, 'out', 1, 1, 0, '2021-09-11 17:33:13', '0000-00-00 00:00:00'),
(31, 'SAK.210913.001', '2021-09-13', 'pembelian-gabah', 29, 15, 9, 1, 1, 'in', 0, 1, 0, '2021-09-13 10:58:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_sak_detail`
--

CREATE TABLE `transaksi_sak_detail` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_jenis_sak` int(11) NOT NULL,
  `is_record` tinyint(1) NOT NULL,
  `arus` enum('in','out','') NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status_acc` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 = wait, 2 = acc, 3 = declined',
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_sak_detail`
--

INSERT INTO `transaksi_sak_detail` (`id`, `id_transaksi`, `id_customer`, `id_jenis_sak`, `is_record`, `arus`, `jumlah`, `status_acc`, `acc_by`, `timestamp_acc`) VALUES
(7, 1, 5, 2, 1, 'in', 75, 2, 1, '2020-02-18 15:58:08'),
(8, 1, 9, 2, 1, 'in', 75, 2, 1, '2020-02-18 16:01:19'),
(9, 1, 9, 2, 1, 'in', 88, 2, 1, '2021-09-04 16:32:25'),
(10, 1, 9, 2, 1, 'in', 77, 2, 1, '2021-09-07 11:07:41'),
(11, 1, 9, 2, 1, 'in', 53, 2, 1, '2021-09-07 11:07:37'),
(22, 5, 5, 1, 1, 'in', 55, 2, 0, '0000-00-00 00:00:00'),
(23, 5, 9, 1, 1, 'in', 55, 2, 0, '0000-00-00 00:00:00'),
(24, 5, 5, 2, 1, 'in', 44, 2, 0, '0000-00-00 00:00:00'),
(25, 5, 9, 2, 1, 'in', 44, 2, 0, '0000-00-00 00:00:00'),
(42, 6, 9, 1, 1, 'out', 15, 2, 0, '0000-00-00 00:00:00'),
(43, 6, 5, 1, 1, 'out', 15, 2, 0, '0000-00-00 00:00:00'),
(44, 12, 5, 1, 0, 'in', 23, 2, 0, '0000-00-00 00:00:00'),
(45, 12, 9, 1, 1, 'in', 23, 2, 0, '0000-00-00 00:00:00'),
(48, 27, 6, 5, 1, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(49, 27, 15, 5, 1, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(50, 26, 15, 3, 1, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(51, 26, 6, 3, 1, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(52, 28, 8, 3, 0, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(53, 28, 14, 3, 0, 'out', 12, 2, 0, '0000-00-00 00:00:00'),
(54, 30, 15, 3, 1, 'out', 121, 2, 0, '0000-00-00 00:00:00'),
(55, 30, 13, 3, 1, 'out', 121, 2, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trx_stok_gabah`
--

CREATE TABLE `trx_stok_gabah` (
  `id` int(11) NOT NULL,
  `kode_trx` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=menunggu, 1=acc, 2=tolak',
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_stok_gabah_detail`
--

CREATE TABLE `trx_stok_gabah_detail` (
  `id` int(11) NOT NULL,
  `id_trx` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `arus_kas`
--
ALTER TABLE `arus_kas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_perusahaan` (`id_perusahaan`),
  ADD KEY `id_kas` (`id_kas`),
  ADD KEY `approval_by` (`approval_by`),
  ADD KEY `id_sumber` (`id_sumber`);

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beban`
--
ALTER TABLE `beban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_beban` (`id_beban`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `initial` (`initial`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `area_id` (`id_area`);

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `hutang_detail`
--
ALTER TABLE `hutang_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_piutang` (`id_hutang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_gabah`
--
ALTER TABLE `jenis_gabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_padi`
--
ALTER TABLE `jenis_padi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_sak`
--
ALTER TABLE `jenis_sak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kadar_air`
--
ALTER TABLE `kadar_air`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_akun_biaya`
--
ALTER TABLE `master_akun_biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_akun_biaya_detail`
--
ALTER TABLE `master_akun_biaya_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biaya` (`id_biaya`);

--
-- Indexes for table `master_akun_pendapatan`
--
ALTER TABLE `master_akun_pendapatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_dryer`
--
ALTER TABLE `master_dryer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_jemur`
--
ALTER TABLE `master_jemur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_pembelian_lain`
--
ALTER TABLE `master_pembelian_lain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_penjualan_lain`
--
ALTER TABLE `master_penjualan_lain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_sales`
--
ALTER TABLE `master_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk_beras`
--
ALTER TABLE `merk_beras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk_beras_harga`
--
ALTER TABLE `merk_beras_harga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kemasan` (`id_kemasan`);

--
-- Indexes for table `merk_beras_kemasan`
--
ALTER TABLE `merk_beras_kemasan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_merk` (`id_merk`);

--
-- Indexes for table `pembelian_gabah`
--
ALTER TABLE `pembelian_gabah`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_pembelian` (`kode_pembelian`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `id_supplier_kendaraan` (`resi_pengiriman`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_rek_pengirim` (`id_rek_pengirim`),
  ADD KEY `id_rek_penerima` (`id_rek_penerima`),
  ADD KEY `id_kas` (`id_kas`),
  ADD KEY `dibayar_oleh` (`dibayar_oleh`),
  ADD KEY `checker` (`checker`),
  ADD KEY `diajukan_oleh` (`diajukan_oleh`),
  ADD KEY `verifikasi_oleh` (`verifikasi_oleh`);

--
-- Indexes for table `pembelian_gabah_batch`
--
ALTER TABLE `pembelian_gabah_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_jenis_sak` (`id_jenis_sak`);

--
-- Indexes for table `pembelian_gabah_detail`
--
ALTER TABLE `pembelian_gabah_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `pembelian_gabah_jenis`
--
ALTER TABLE `pembelian_gabah_jenis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_batch`),
  ADD KEY `id_jenis_gabah` (`jenis_gabah`),
  ADD KEY `id_jenis_padi` (`id_jenis_padi`);

--
-- Indexes for table `pembelian_gabah_pembayaran`
--
ALTER TABLE `pembelian_gabah_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`);

--
-- Indexes for table `pembelian_gabah_tes`
--
ALTER TABLE `pembelian_gabah_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`),
  ADD KEY `id_jenis_padi` (`id_jenis_padi`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_batch` (`id_batch`);

--
-- Indexes for table `pembelian_gabah_tes_detail`
--
ALTER TABLE `pembelian_gabah_tes_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tes` (`id_tes`);

--
-- Indexes for table `pembelian_lain`
--
ALTER TABLE `pembelian_lain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `pembelian_lain_detail`
--
ALTER TABLE `pembelian_lain_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_penjualan` (`id_pembelian`),
  ADD KEY `id_kategori` (`id_jenis_bahan`);

--
-- Indexes for table `pembelian_lain_pembayaran`
--
ALTER TABLE `pembelian_lain_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`);

--
-- Indexes for table `penanggung_jawab`
--
ALTER TABLE `penanggung_jawab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendapatan`
--
ALTER TABLE `pendapatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_beban` (`id_kategori`);

--
-- Indexes for table `pengeringan`
--
ALTER TABLE `pengeringan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jenis_padi` (`id_jenis_padi`);

--
-- Indexes for table `pengeringan_detail`
--
ALTER TABLE `pengeringan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pengeringan` (`id_pengeringan`),
  ADD KEY `id_stok` (`id_stok`),
  ADD KEY `id_dryer` (`id_dryer`),
  ADD KEY `id_jemur` (`id_jemur`);

--
-- Indexes for table `penggilingan`
--
ALTER TABLE `penggilingan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `penggilingan_detail`
--
ALTER TABLE `penggilingan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_penggilingan` (`id_penggilingan`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indexes for table `penjualan_lain`
--
ALTER TABLE `penjualan_lain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `penjualan_lain_detail`
--
ALTER TABLE `penjualan_lain_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_penjualan` (`id_penjualan`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `penjualan_lain_pembayaran`
--
ALTER TABLE `penjualan_lain_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_penjualan`);

--
-- Indexes for table `penjualan_po`
--
ALTER TABLE `penjualan_po`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_sales` (`id_sales`);

--
-- Indexes for table `penjualan_po_detail`
--
ALTER TABLE `penjualan_po_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_po` (`id_po`),
  ADD KEY `id_kemasan` (`id_kemasan`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `penjualan_po_kontan`
--
ALTER TABLE `penjualan_po_kontan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_po` (`id_po`);

--
-- Indexes for table `penjualan_po_pembayaran`
--
ALTER TABLE `penjualan_po_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_penjualan`);

--
-- Indexes for table `penjualan_produksi`
--
ALTER TABLE `penjualan_produksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_produksi_detail`
--
ALTER TABLE `penjualan_produksi_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_do` (`id_produksi`),
  ADD KEY `id_po` (`id_po`);

--
-- Indexes for table `penjualan_produksi_proses`
--
ALTER TABLE `penjualan_produksi_proses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_do` (`id_do`),
  ADD KEY `id_merk` (`id_merk`);

--
-- Indexes for table `penjualan_retur`
--
ALTER TABLE `penjualan_retur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_penjualan`);

--
-- Indexes for table `penjualan_retur_detail`
--
ALTER TABLE `penjualan_retur_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_retur` (`id_retur`),
  ADD KEY `id_kemasan` (`id_kemasan`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `piutang_detail`
--
ALTER TABLE `piutang_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_piutang` (`id_piutang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_merk` (`id_merk`);

--
-- Indexes for table `produksi_bahan`
--
ALTER TABLE `produksi_bahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produksi` (`id_produksi`),
  ADD KEY `id_proses` (`id_proses`),
  ADD KEY `id_bahan` (`id_bahan`);

--
-- Indexes for table `produksi_hasil`
--
ALTER TABLE `produksi_hasil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produksi` (`id_produksi`);

--
-- Indexes for table `produksi_proses`
--
ALTER TABLE `produksi_proses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_bahan`
--
ALTER TABLE `stok_bahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jenis_bahan` (`id_jenis_bahan`);

--
-- Indexes for table `stok_bahan_old`
--
ALTER TABLE `stok_bahan_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jenis_bahan` (`id_jenis_bahan`);

--
-- Indexes for table `stok_bahan_trx`
--
ALTER TABLE `stok_bahan_trx`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indexes for table `stok_beras`
--
ALTER TABLE `stok_beras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kemasan` (`id_kemasan`);

--
-- Indexes for table `stok_beras_trx`
--
ALTER TABLE `stok_beras_trx`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indexes for table `stok_gabah`
--
ALTER TABLE `stok_gabah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sumber` (`id_sumber`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_jenis_padi` (`id_jenis_padi`);

--
-- Indexes for table `stok_gabah_trx`
--
ALTER TABLE `stok_gabah_trx`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indexes for table `transaksi_sak`
--
ALTER TABLE `transaksi_sak`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer_from` (`id_customer_from`),
  ADD KEY `id_customer_to` (`id_customer_to`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `transaksi_sak_detail`
--
ALTER TABLE `transaksi_sak_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaksi` (`id_transaksi`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_jenis_sak` (`id_jenis_sak`),
  ADD KEY `acc_by` (`acc_by`);

--
-- Indexes for table `trx_stok_gabah`
--
ALTER TABLE `trx_stok_gabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trx_stok_gabah_detail`
--
ALTER TABLE `trx_stok_gabah_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_stok` (`id_stok`),
  ADD KEY `id_trx` (`id_trx`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `arus_kas`
--
ALTER TABLE `arus_kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `beban`
--
ALTER TABLE `beban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hutang_detail`
--
ALTER TABLE `hutang_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jenis_bahan`
--
ALTER TABLE `jenis_bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `jenis_gabah`
--
ALTER TABLE `jenis_gabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jenis_padi`
--
ALTER TABLE `jenis_padi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jenis_sak`
--
ALTER TABLE `jenis_sak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kadar_air`
--
ALTER TABLE `kadar_air`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `master_akun_biaya`
--
ALTER TABLE `master_akun_biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_akun_biaya_detail`
--
ALTER TABLE `master_akun_biaya_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `master_akun_pendapatan`
--
ALTER TABLE `master_akun_pendapatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_dryer`
--
ALTER TABLE `master_dryer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_jemur`
--
ALTER TABLE `master_jemur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_pembelian_lain`
--
ALTER TABLE `master_pembelian_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_penjualan_lain`
--
ALTER TABLE `master_penjualan_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_sales`
--
ALTER TABLE `master_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `merk_beras`
--
ALTER TABLE `merk_beras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `merk_beras_harga`
--
ALTER TABLE `merk_beras_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `merk_beras_kemasan`
--
ALTER TABLE `merk_beras_kemasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pembelian_gabah`
--
ALTER TABLE `pembelian_gabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `pembelian_gabah_batch`
--
ALTER TABLE `pembelian_gabah_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `pembelian_gabah_detail`
--
ALTER TABLE `pembelian_gabah_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=553;

--
-- AUTO_INCREMENT for table `pembelian_gabah_jenis`
--
ALTER TABLE `pembelian_gabah_jenis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `pembelian_gabah_pembayaran`
--
ALTER TABLE `pembelian_gabah_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pembelian_gabah_tes`
--
ALTER TABLE `pembelian_gabah_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `pembelian_gabah_tes_detail`
--
ALTER TABLE `pembelian_gabah_tes_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pembelian_lain`
--
ALTER TABLE `pembelian_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `pembelian_lain_detail`
--
ALTER TABLE `pembelian_lain_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pembelian_lain_pembayaran`
--
ALTER TABLE `pembelian_lain_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penanggung_jawab`
--
ALTER TABLE `penanggung_jawab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendapatan`
--
ALTER TABLE `pendapatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pengeringan`
--
ALTER TABLE `pengeringan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pengeringan_detail`
--
ALTER TABLE `pengeringan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `penggilingan`
--
ALTER TABLE `penggilingan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penggilingan_detail`
--
ALTER TABLE `penggilingan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `penjualan_lain`
--
ALTER TABLE `penjualan_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penjualan_lain_detail`
--
ALTER TABLE `penjualan_lain_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penjualan_lain_pembayaran`
--
ALTER TABLE `penjualan_lain_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penjualan_po`
--
ALTER TABLE `penjualan_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `penjualan_po_detail`
--
ALTER TABLE `penjualan_po_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `penjualan_po_kontan`
--
ALTER TABLE `penjualan_po_kontan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `penjualan_po_pembayaran`
--
ALTER TABLE `penjualan_po_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penjualan_produksi`
--
ALTER TABLE `penjualan_produksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `penjualan_produksi_detail`
--
ALTER TABLE `penjualan_produksi_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `penjualan_produksi_proses`
--
ALTER TABLE `penjualan_produksi_proses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_retur`
--
ALTER TABLE `penjualan_retur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `penjualan_retur_detail`
--
ALTER TABLE `penjualan_retur_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `piutang_detail`
--
ALTER TABLE `piutang_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `produksi`
--
ALTER TABLE `produksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `produksi_bahan`
--
ALTER TABLE `produksi_bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produksi_hasil`
--
ALTER TABLE `produksi_hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `produksi_proses`
--
ALTER TABLE `produksi_proses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stok_bahan`
--
ALTER TABLE `stok_bahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `stok_bahan_old`
--
ALTER TABLE `stok_bahan_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stok_bahan_trx`
--
ALTER TABLE `stok_bahan_trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `stok_beras`
--
ALTER TABLE `stok_beras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `stok_beras_trx`
--
ALTER TABLE `stok_beras_trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `stok_gabah`
--
ALTER TABLE `stok_gabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `stok_gabah_trx`
--
ALTER TABLE `stok_gabah_trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `transaksi_sak`
--
ALTER TABLE `transaksi_sak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `transaksi_sak_detail`
--
ALTER TABLE `transaksi_sak_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `trx_stok_gabah`
--
ALTER TABLE `trx_stok_gabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trx_stok_gabah_detail`
--
ALTER TABLE `trx_stok_gabah_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
